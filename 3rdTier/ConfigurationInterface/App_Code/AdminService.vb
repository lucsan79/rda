﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class DialogAdminService
    Inherits System.Web.Services.WebService

#Region "VARIABILI"
    Private currentConnection As eConnection
    Dim myEPLMS_HOME As String
#End Region


#Region "Account Manager"


    <WebMethod(Description:="Autenticate user in current session", EnableSession:=True)>
    Public Function login(ByVal given_username As String, ByVal given_password As String, ByRef statusMessage As String) As Integer

        Dim rc As Integer = ePLMS_OK
        Try
            myEPLMS_HOME = System.Configuration.ConfigurationManager.AppSettings("ePLMSConfigPath")


            If Not Session("currentConnection") Is Nothing Then
                currentConnection = DirectCast(Session("currentConnection"), eConnection)
            Else
                currentConnection = New eConnection(myEPLMS_HOME)
                rc = currentConnection.internalDbLogIn("eplms")
                currentConnection.pushMessageFlag = True
                'rc = currentConnection.logIn(given_username, given_password)
                'currentConnection.pushMessageFlag = False
                If currentConnection.isLoggedIn Then
                    Session.Add("currentConnection", currentConnection)
                Else
                    statusMessage = "User: " & given_username & " not connected. " & rc
                End If
            End If


        Catch ex As Exception
            rc = ePLMS_ERROR
            statusMessage = ex.Message
        End Try

        Return rc
    End Function


    '</summary>
    <WebMethod(Description:="Disconnect current session", EnableSession:=True)>
    Public Function logout() As Integer
        Dim rc As Integer = ePLMS_OK
        Try
            If Not currentConnection Is Nothing Then
                If currentConnection.isLoggedIn Then
                    rc = currentConnection.logOut()
                End If
                If Not Session("currentConnection") Is Nothing Then
                    Session.Remove("currentConnection")
                End If
                currentConnection.Dispose()
            End If
        Catch ex As Exception
            rc = ePLMS_ERROR
        End Try

        Return rc
    End Function
#End Region

#Region "Dialog Manager"

    <WebMethod(Description:="Dialog Create", EnableSession:=True)>
    Public Function CreateDialogo(ByVal givenFamiglia As String,
                                  ByVal givenProdotto As String,
                                  ByVal givenCodifica As String,
                                  ByVal givenCadReferement As String,
                                  ByVal givenTipologia As String,
                                  ByRef statusMessage As String,
                                  ByRef returnDialogID As Integer, ByRef returnDialogName As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim myeBusinessData As eBusinessData
        Dim attributeList As New ArrayList
        Dim myeBDT As eBusinessDataType
        Dim eBDTDialogo As String = "" & System.Configuration.ConfigurationManager.AppSettings("DialogoBDT")

        Try
            currentConnection = Session("currentConnection")
            myeBDT = New eBusinessDataType(currentConnection)
            If myeBDT.Load(eBDTDialogo) = ePLMS_OK Then
                myeBDT.getAttributeList(attributeList)
                If attributeList IsNot Nothing Then
                    For idx As Integer = 0 To attributeList.Count - 1
                        Dim edata As eDataListElement = attributeList(idx)
                        Select Case edata.Name
                            Case System.Configuration.ConfigurationManager.AppSettings("AttDLG-Codifica")
                                edata.Value = givenCodifica
                            Case System.Configuration.ConfigurationManager.AppSettings("AttDLG-Prodotto")
                                edata.Value = givenProdotto
                            Case System.Configuration.ConfigurationManager.AppSettings("AttDLG-Famiglia")
                                edata.Value = givenFamiglia
                        End Select
                        attributeList(idx) = edata
                    Next
                End If
                myeBusinessData = New eBusinessData(currentConnection)
                myeBusinessData.addObjectToWorkbenchFolder = False
                myeBusinessData.Name = "#"
                myeBusinessData.businessDataType = myeBDT.Name
                myeBusinessData.Project = System.Configuration.ConfigurationManager.AppSettings("DialogoProject")
                myeBusinessData.Workflow = System.Configuration.ConfigurationManager.AppSettings("DialogoWorkflow")

                myeBusinessData.Description = String.Format("Cofinguration of {0}", givenFamiglia)

                myeBusinessData.Description = myeBusinessData.Description.Trim
                currentConnection.pushMessageFlag = True
                retValue = myeBusinessData.Create(attributeList)
                currentConnection.pushMessageFlag = False
                If retValue <> ePLMS_OK Then
                    statusMessage = currentConnection.messageStack.Peek.text
                Else
                    returnDialogID = myeBusinessData.Id
                    returnDialogName = myeBusinessData.Name
                End If

                myeBusinessData = Nothing
            Else
                statusMessage = String.Format("BusinessDataType {0} inesistente", eBDTDialogo)

            End If
        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message

        End Try

        Return retValue
    End Function
    <WebMethod(Description:="List of Dialogs Stored", EnableSession:=True)>
    Public Function GetExternalTable(givenExternalTable As String, ByVal givenParam() As String, givenOperator() As String, queryParamsValue() As String, givenColumnsToShow As String, givenMAxRow As Integer, ByRef statusMessage As String) As System.Data.DataTable


        Dim myBusinessDataType As eBusinessDataType
        Dim myAttributeList As ArrayList
        Dim myBaseAttributeQuery As String = "SELECT eBusinessDataId FROM eplms.eBusinessDataAttribute WHERE (eAttributeId = {0}) AND (eValue {1} {2})"
        Dim myQuery As String = ""
        Dim retValue As Integer = ePLMS_ERROR
        Dim myeFind As eFind
        Dim returnList As System.Data.DataTable
        Dim myEntry As eBusinessData

        currentConnection = Session("currentConnection")
        myBusinessDataType = New eBusinessDataType(currentConnection)
        If myBusinessDataType.Load(givenExternalTable) = ePLMS_OK Then
            myAttributeList = New ArrayList
            myBusinessDataType.getAttributeList(myAttributeList)

            For idx As Integer = 0 To givenParam.Length - 1
                Dim param As String = givenParam(idx)
                Dim op As String = givenOperator(idx)
                Dim val As String = queryParamsValue(idx)
                Dim paramId As String = ""
                If param = "" Or op = "" Or val = "" Then
                    Continue For
                End If
                If myAttributeList IsNot Nothing Then
                    For Each edata As eDataListElement In myAttributeList
                        If edata.Name.ToUpper = param.ToUpper Then
                            paramId = edata.Key
                            Exit For
                        End If
                    Next
                End If

                If param <> "" And op <> "" And val <> "" And paramId <> "" Then
                    If myQuery <> "" Then
                        myQuery &= " INTERSECT "
                    End If

                    If op.ToUpper = "LIKE" Then
                        ' If Not IsNumeric(val) Then
                        If Not val.Contains("*") And Not val.Contains("%") Then
                            If Not val.StartsWith("'") Then
                                val = "'%" & val
                            Else
                                val = "'%" & val.Substring(1)
                            End If
                            If Not val.EndsWith("'") Then
                                val &= "%'"
                            Else
                                val = val.Substring(0, val.ToString.Length - 1) & "%'"
                            End If
                        Else
                            If Not val.StartsWith("'") Then
                                val = "'" & val
                            End If
                            If Not val.EndsWith("'") Then
                                val &= "'"
                            End If
                        End If


                        val = val.Replace("*", "%")
                        'End If
                    ElseIf Not IsNumeric(val) Then
                        val = "'" & val & "'"

                    End If
                    myQuery &= String.Format(myBaseAttributeQuery, paramId, op, val)
                End If

            Next

            Dim queryMax As String = " "
            If givenMAxRow > 0 Then
                queryMax = " TOP(" & givenMAxRow & ") "
            End If

            If myQuery = "" Then
                myQuery = "select" & queryMax & "eId from eplms.eBusinessData where eBusinessDataType = '" & givenExternalTable & "'"
            Else
                myQuery = "select" & queryMax & "eId from eplms.eBusinessData where eBusinessDataType = '" & givenExternalTable & "' and eId in (" & myQuery & ")"
            End If

        End If

        If myQuery <> "" Then

            myeFind = New eFind(currentConnection)
            myeFind.queryString = "$(" & myQuery & ")"
            myeFind.selectAttributeListString = "id"
            myeFind.resultInWorkbench = ePLMSResultInWorkbenchNone
            currentConnection.pushMessageFlag = True
            retValue = myeFind.Prepare(Nothing)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
            Else
                returnList = New System.Data.DataTable
                currentConnection.pushMessageFlag = True
                retValue = myeFind.Execute(returnList)
                currentConnection.pushMessageFlag = False
                If retValue <> ePLMS_OK Then
                    statusMessage = currentConnection.messageStack.Peek.text
                    returnList = Nothing
                Else
                    myEntry = New eBusinessData(currentConnection)
                    returnList.TableName = "EXTERNALTABLE"
                    For Each col As String In givenColumnsToShow.Split(",".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
                        returnList.Columns.Add(col.ToUpper)
                    Next
                    For Each row As System.Data.DataRow In returnList.Rows
                        If myEntry.Load(row("eId")) = ePLMS_OK Then
                            myAttributeList = New ArrayList
                            myEntry.getAttributeList(myAttributeList)
                            If myAttributeList IsNot Nothing Then
                                For Each edata As eDataListElement In myAttributeList
                                    If givenColumnsToShow.ToUpper.Contains(edata.Name.ToUpper()) Then
                                        row(edata.Name.ToUpper) = edata.Value

                                    End If
                                Next

                            End If
                            row.AcceptChanges()
                        End If
                    Next
                End If
            End If
        End If

        Return returnList
    End Function


    <WebMethod(Description:="List of Dialogs Stored", EnableSession:=True)>
    Public Function GetListaDialoghi(ByRef returnList As System.Data.DataTable, bypassStatus As Boolean, ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim myeFind As eFind
        Dim queryString As String = "BusinessDataType={1} and Level={2} and key1<>'bkp'"
        Dim queryParams() As String = Nothing

        If bypassStatus Then
            queryParams = New String() {System.Configuration.ConfigurationManager.AppSettings("DialogoBDT"), "*"}
        Else
            queryParams = New String() {System.Configuration.ConfigurationManager.AppSettings("DialogoBDT"), System.Configuration.ConfigurationManager.AppSettings("DLGLevelEnable")}
        End If



        currentConnection = Session("currentConnection")

        myeFind = New eFind(currentConnection)
        myeFind.getLastRevision = True
        myeFind.queryString = queryString
        myeFind.selectAttributeListString = "Id;Name;Description;Level"

        currentConnection.pushMessageFlag = True
        retValue = myeFind.Prepare(queryParams)
        currentConnection.pushMessageFlag = False
        If retValue <> ePLMS_OK Then
            statusMessage = currentConnection.messageStack.Peek.text
        Else
            returnList = New System.Data.DataTable
            currentConnection.pushMessageFlag = True
            retValue = myeFind.Execute(returnList)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
                returnList = Nothing
            Else
                returnList.TableName = "DIALGOS"
            End If
        End If

        Return retValue
    End Function

    <WebMethod(Description:="List of Dialogs Stored", EnableSession:=True)>
    Public Function GetFamiglieProdotti(ByRef returnList As System.Data.DataTable, ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim myeFind As eFind
        Dim queryString As String = "BusinessDataType={1} and Level={2} "
        Dim myStandardProject As eProject
        Dim subProjectFamiglia() As eProject
        Dim subProjectProdotto() As eProject

        currentConnection = Session("currentConnection")

        myStandardProject = New eProject(currentConnection)
        If myStandardProject.Load("Standard") = ePLMS_OK Then
            returnList = New System.Data.DataTable("PRODOTTI-FAMIGLIA")
            returnList.Columns.Add("Famiglia")
            returnList.Columns.Add("Prodotto")

            myStandardProject.getProjectList(subProjectFamiglia)
            If subProjectFamiglia IsNot Nothing Then
                For Each famigliaProject As eProject In subProjectFamiglia
                    famigliaProject.getProjectList(subProjectProdotto)
                    If subProjectProdotto IsNot Nothing Then

                        For Each prodottoProject As eProject In subProjectProdotto
                            Dim myNewRow As System.Data.DataRow = returnList.NewRow
                            myNewRow(0) = famigliaProject.Name
                            myNewRow(1) = prodottoProject.Name
                            returnList.Rows.Add(myNewRow)
                        Next

                    End If

                Next
            End If
        End If

        Return ePLMS_OK

    End Function


    <WebMethod(Description:="List of Dialogs Stored", EnableSession:=True)>
    Public Function GetBOM(ByRef returnList As System.Data.DataTable, ByVal givenFamiglia As String, ByVal givenProdotto As String, ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim myeFind As eFind
        Dim queryString As String = "BusinessDataType={1} and CADType={2} "
        Dim tmpTAble As System.Data.DataTable
        Dim mySubProjects() As eProject
        Dim queryProject As String = ""
        currentConnection = Session("currentConnection")

        Dim queryParams() As String = New String() {"CADModel", "Top-Assembly"}
        Dim myTopProject As eProject

        myTopProject = New eProject(currentConnection)
        If myTopProject.Load(givenFamiglia) = ePLMS_OK Then

            myTopProject.getProjectList(mySubProjects)
            If mySubProjects IsNot Nothing Then
                For Each project As eProject In mySubProjects
                    If queryProject <> "" Then
                        queryProject &= " OR "
                    End If
                    queryProject &= "Project='" & project.Name & "'"

                Next
            End If
        End If
        If queryProject <> "" Then
            queryString = queryString & " and ( " & queryProject & " )"
        End If


        myeFind = New eFind(currentConnection)
        myeFind.getLastRevision = True
        myeFind.queryString = queryString
        myeFind.selectAttributeListString = "Id;Name;Description;Project"
        currentConnection.pushMessageFlag = True
        retValue = myeFind.Prepare(queryParams)
        currentConnection.pushMessageFlag = False
        If retValue <> ePLMS_OK Then
            statusMessage = currentConnection.messageStack.Peek.text
        Else
            tmpTAble = New System.Data.DataTable
            currentConnection.pushMessageFlag = True
            retValue = myeFind.Execute(tmpTAble)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
                returnList = Nothing
            Else
                If tmpTAble IsNot Nothing Then
                    If tmpTAble.Rows.Count > 0 Then
                        For Each topAssRow As System.Data.DataRow In tmpTAble.Rows
                            EstrazioneDistinta(topAssRow("eId"), Nothing, returnList, "", givenFamiglia)
                        Next


                    Else
                        statusMessage = "Non è stato individuato alcun top assembly per la serie " & givenProdotto
                        returnList = Nothing
                        Return ePLMS_ERROR
                    End If
                End If

            End If
        End If



        Return ePLMS_OK

    End Function


    <WebMethod(Description:="List of Features Stored", EnableSession:=True)>
    Public Function GetListFeatures(ByRef returnList As System.Data.DataTable, ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim myeFind As eFind
        Dim queryString As String = "BusinessDataType={1} and Level={2}"
        Dim queryParams() As String = {System.Configuration.ConfigurationManager.AppSettings("FeatureBDT"), System.Configuration.ConfigurationManager.AppSettings("FeatureLevelEnable")}

        currentConnection = Session("currentConnection")

        myeFind = New eFind(currentConnection)
        myeFind.getLastRevision = True
        myeFind.queryString = queryString
        myeFind.selectAttributeListString = "Id;Name;Description;key1"

        currentConnection.pushMessageFlag = True
        retValue = myeFind.Prepare(queryParams)
        currentConnection.pushMessageFlag = False
        If retValue <> ePLMS_OK Then
            statusMessage = currentConnection.messageStack.Peek.text
        Else
            currentConnection.pushMessageFlag = True
            retValue = myeFind.Execute(returnList)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
                returnList = Nothing
            Else

                returnList.Columns.Add("CDVAVR")
                returnList.Columns.Add("DAVAVR")
                returnList.Columns.Add("DSVAVR")
                returnList.Columns.Add("FS01VR")
                returnList.Columns.Add("GRVAVR")
                returnList.Columns.Add("TIPOVR")
                returnList.TableName = "Caratteristiche"

                If returnList IsNot Nothing Then
                    Dim myCar As New eBusinessData(currentConnection)
                    Dim myAttribute As ArrayList
                    For Each row As System.Data.DataRow In returnList.Rows
                        If myCar.Load(row("eId")) = ePLMS_OK Then
                            myAttribute = New ArrayList
                            myCar.getAttributeList(myAttribute)
                            If myAttribute IsNot Nothing Then
                                For Each edata As eDataListElement In myAttribute
                                    If returnList.Columns.Contains(edata.Name) Then
                                        row(edata.Name) = edata.Value
                                    End If
                                Next
                            End If
                            row.AcceptChanges()
                        End If

                    Next
                End If
            End If
        End If

        Return retValue
    End Function



    <WebMethod(Description:="Get Dialog File", EnableSession:=True)>
    Public Function GetFileDialogo(ByVal givenDialogoId As Integer, ByRef returnFileName As String, ByRef returnFileContent As Byte(), ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim downloadFilePath As String
        Dim myeBusinessData As eBusinessData
        Dim myeFileList() As eBusinessDataFile
        Dim returnFileURI As String
        Dim myWebClient As Net.WebClient

        Try
            currentConnection = Session("currentConnection")

            myeBusinessData = New eBusinessData(currentConnection)
            currentConnection.pushMessageFlag = True
            retValue = myeBusinessData.Load(givenDialogoId)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
            Else
                myeBusinessData.getBusinessDataFileList(True, myeFileList)
                If (myeFileList IsNot Nothing) Then
                    For Each file As eBusinessDataFile In myeFileList
                        If file.Description = "Dialog Configuration" And file.Name.EndsWith(".xml") Then
                            downloadFilePath = currentConnection.ePLMS_HOME & "\tmp\" & file.Name
                            currentConnection.pushMessageFlag = True
                            returnFileName = file.Name
                            retValue = file.copyOut(returnFileURI)
                            currentConnection.pushMessageFlag = False
                            If retValue <> ePLMS_OK Then
                                statusMessage = currentConnection.messageStack.Peek.text
                            Else
                                myWebClient = New Net.WebClient
                                myWebClient.DownloadFile(returnFileURI, downloadFilePath)
                                returnFileContent = System.IO.File.ReadAllBytes(downloadFilePath)
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message

        End Try

        Return retValue
    End Function


    <WebMethod(Description:="Get Dialog File", EnableSession:=True)>
    Public Function GetFileFunctionDialogo(ByVal givenDialogoId As Integer, ByRef returnFileName As System.Data.DataTable, ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim downloadFilePath As String
        Dim myeBusinessData As eBusinessData
        Dim myeFileList() As eBusinessDataFile
        Dim returnFileURI As String
        Dim myWebClient As Net.WebClient

        Try
            returnFileName = New System.Data.DataTable("filesFunction")
            returnFileName.Columns.Add("FileName")
            returnFileName.Columns.Add("FileContent")
            currentConnection = Session("currentConnection")

            myeBusinessData = New eBusinessData(currentConnection)
            currentConnection.pushMessageFlag = True
            retValue = myeBusinessData.Load(givenDialogoId)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
            Else
                myeBusinessData.getBusinessDataFileList(True, myeFileList)
                If (myeFileList IsNot Nothing) Then
                    For Each file As eBusinessDataFile In myeFileList
                        If file.Name.ToLower.EndsWith(".vb") Then
                            Dim newRow As System.Data.DataRow = returnFileName.NewRow

                            downloadFilePath = currentConnection.ePLMS_HOME & "\tmp\" & file.Name
                            currentConnection.pushMessageFlag = True
                            newRow(0) = file.Name
                            retValue = file.copyOut(returnFileURI)
                            currentConnection.pushMessageFlag = False
                            If retValue <> ePLMS_OK Then
                                statusMessage = currentConnection.messageStack.Peek.text
                            Else
                                myWebClient = New Net.WebClient
                                myWebClient.DownloadFile(returnFileURI, downloadFilePath)
                                Dim cont As String = ""
                                cont = New UTF8Encoding().GetString(System.IO.File.ReadAllBytes(downloadFilePath))
                                newRow(1) = cont
                                returnFileName.Rows.Add(newRow)
                            End If
                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message

        End Try

        Return retValue
    End Function


    <WebMethod(Description:="Get Dialog File", EnableSession:=True)>
    Public Function GetFileCADConfigDialogo(ByVal givenDialogoId As Integer, ByRef returnFileName As String, ByRef returnFileContent As Byte(), ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim downloadFilePath As String
        Dim myeBusinessData As eBusinessData
        Dim myeFileList() As eBusinessDataFile
        Dim returnFileURI As String
        Dim myWebClient As Net.WebClient

        Try
            currentConnection = Session("currentConnection")

            myeBusinessData = New eBusinessData(currentConnection)
            currentConnection.pushMessageFlag = True
            retValue = myeBusinessData.Load(givenDialogoId)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
            Else
                myeBusinessData.getBusinessDataFileList(True, myeFileList)
                If (myeFileList IsNot Nothing) Then
                    For Each file As eBusinessDataFile In myeFileList
                        If file.Description = "CAD Features Configuration" And file.Name.EndsWith(".config") Then
                            downloadFilePath = currentConnection.ePLMS_ROOT & "\tmp\" & file.Name
                            currentConnection.pushMessageFlag = True
                            returnFileName = file.Name
                            retValue = file.copyOut(returnFileURI)
                            currentConnection.pushMessageFlag = False
                            If retValue <> ePLMS_OK Then
                                statusMessage = currentConnection.messageStack.Peek.text
                            Else
                                myWebClient = New Net.WebClient
                                myWebClient.DownloadFile(returnFileURI, downloadFilePath)
                                returnFileContent = System.IO.File.ReadAllBytes(downloadFilePath)
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message

        End Try

        Return retValue
    End Function



    <WebMethod(Description:="Add File to a Dialog", EnableSession:=True)>
    Public Function AddFileDialogo(ByVal businessDataId As Integer,
                                              ByVal fileContent() As Byte, ByVal fileName As String, ByVal fileDescription As String,
                                              ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim tempFilePath As String = ""
        Dim myeBusinessData As eBusinessData
        Dim tempFileName As String
        'Dim tempFileDescription As String = "Dialog Configuration"
        Dim myeFileService As eFileService.eFileService
        Dim myeVault As eVault
        Dim myeBDFile As eBusinessDataFile

        currentConnection = Session("currentConnection")

        Try
            myeBusinessData = New eBusinessData(currentConnection)
            currentConnection.pushMessageFlag = True
            retValue = myeBusinessData.Load(businessDataId)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
            Else
                tempFileName = myeBusinessData.Name & ".xml"

            End If
            If fileName <> "" Then
                tempFileName = fileName
            End If

            tempFilePath = currentConnection.ePLMS_ROOT & "\tmp\" & tempFileName

            If IO.File.Exists(tempFilePath) Then IO.File.Delete(tempFilePath)

            myeFileService = New eFileService.eFileService
            myeVault = New eVault(currentConnection)
            myeBusinessData.getCandidateVault(myeVault)
            myeFileService.Url = myeVault.fileServiceURL & "/eFileService.asmx"
            myeFileService.UploadSingleFile(fileContent, tempFileName)
            myeBDFile = New eBusinessDataFile(currentConnection, myeBusinessData)
            myeBDFile.Name = System.IO.Path.GetFileName(tempFilePath)
            myeBDFile.Description = fileDescription
            myeBDFile.Type = ePLMSFileTypeSource
            myeBDFile.Source = tempFileName
            myeBDFile.businessDataFileType = "XML"
            If System.IO.Path.GetExtension(fileName).ToLower = ".vb" Then
                myeBDFile.businessDataFileType = "VB.NET"
            ElseIf System.IO.Path.GetExtension(fileName).ToLower = ".config" Then
                myeBDFile.businessDataFileType = "CONFIG"
            End If

            currentConnection.pushMessageFlag = True
            retValue = myeBusinessData.addBusinessDataFile(myeBDFile, New ArrayList)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
            Else


            End If
        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message

        End Try

        Try
            If IO.File.Exists(tempFilePath) Then IO.File.Delete(tempFilePath)
        Catch ex As Exception

        End Try

        Return retValue
    End Function

    <WebMethod(Description:="Set Dialog Public", EnableSession:=True)>
    Public Function SetDialogPublic(ByVal businessDataId As Integer, ByRef message As String) As Integer
        Dim myeBusinessData As eBusinessData
        Dim retValue As Integer = ePLMS_ERROR
        Dim newBD As eBusinessData
        Dim mySaveAsCriteria As New eSaveasCriteria
        Dim myWorkflowEngine As eWorkflowEngine

        currentConnection = Session("currentConnection")

        myeBusinessData = New eBusinessData(currentConnection)
        retValue = myeBusinessData.Load(businessDataId)
        currentConnection.pushMessageFlag = False
        If retValue = ePLMS_OK Then
            myWorkflowEngine = New eWorkflowEngine(currentConnection, myeBusinessData.Id)
            retValue = myWorkflowEngine.reassignLevel("Public", "", True)
            If retValue <> ePLMS_OK Then
                message = myWorkflowEngine.getLastErrorMessage

            Else
                message = ""
            End If
        End If

        Return retValue

    End Function

    <WebMethod(Description:="Set Dialog Working", EnableSession:=True)>
    Public Function SetDialogWorking(ByVal businessDataId As Integer, ByRef message As String) As Integer
        Dim myeBusinessData As eBusinessData
        Dim retValue As Integer = ePLMS_ERROR
        Dim newBD As eBusinessData
        Dim mySaveAsCriteria As New eSaveasCriteria
        Dim myWorkflowEngine As eWorkflowEngine

        currentConnection = Session("currentConnection")

        myeBusinessData = New eBusinessData(currentConnection)
        retValue = myeBusinessData.Load(businessDataId)
        currentConnection.pushMessageFlag = False
        If retValue = ePLMS_OK Then
            myWorkflowEngine = New eWorkflowEngine(currentConnection, myeBusinessData.Id)
            retValue = myWorkflowEngine.reassignLevel("Working", "", True)
            If retValue <> ePLMS_OK Then
                message = myWorkflowEngine.getLastErrorMessage

            Else
                message = ""
            End If
        End If

        Return retValue
    End Function
    <WebMethod(Description:="bkp per update Dialog", EnableSession:=True)>
    Public Function PrepareToSave(ByVal businessDataId As Integer) As Integer
        Dim myeBusinessData As eBusinessData
        Dim retValue As Integer = ePLMS_ERROR
        Dim newBD As eBusinessData
        Dim mySaveAsCriteria As New eSaveasCriteria
        Dim myNowDate As String = ""
        currentConnection = Session("currentConnection")
        myeBusinessData = New eBusinessData(currentConnection)
        retValue = myeBusinessData.Load(businessDataId)
        currentConnection.pushMessageFlag = False
        If retValue = ePLMS_OK Then


            newBD = New eBusinessData(currentConnection)

            currentConnection.Core.dateToString(Now, myNowDate)
            newBD.Name = String.Format("{0}_{1}", myeBusinessData.Name, myNowDate)
            newBD.Description = "backup " & myeBusinessData.Name & " : " & Now.ToString
            newBD.businessDataType = myeBusinessData.businessDataType
            newBD.Revision = myeBusinessData.Revision
            newBD.Project = myeBusinessData.Project
            newBD.Workflow = myeBusinessData.Workflow
            newBD.Key1 = "bkp"
            mySaveAsCriteria.businessDataAttributeFlag = True
            mySaveAsCriteria.businessDataFileFlag = True
            mySaveAsCriteria.businessDataRelationFlag = False



            currentConnection.pushMessageFlag = True
            retValue = myeBusinessData.Saveas(mySaveAsCriteria, newBD)

        End If

    End Function

    <WebMethod(Description:="Add File to a Dialog", EnableSession:=True)>
    Public Function UpdateFileDialogo(ByVal businessDataId As Integer,
                                              ByVal fileContent() As Byte, ByVal filename As String, ByVal fileDescription As String,
                                              ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim tempFilePath As String = ""
        Dim myeBusinessData As eBusinessData
        Dim tempFileDescription As String = "Dialog Configuration"
        Dim myeFileService As eFileService.eFileService
        Dim myeVault As eVault
        Dim myeBDFile As eBusinessDataFile
        Dim myeFileList() As eBusinessDataFile
        Dim downloadFilePath As String
        Dim newFileId As Integer

        Try
            currentConnection = Session("currentConnection")

            myeBusinessData = New eBusinessData(currentConnection)
            currentConnection.pushMessageFlag = True
            retValue = myeBusinessData.Load(businessDataId)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
            Else
                If filename = "" Then
                    filename = myeBusinessData.Name & ".xml"
                End If
                myeBusinessData.getBusinessDataFileList(True, myeFileList)
                If (myeFileList IsNot Nothing) Then
                    For Each myeBDFile In myeFileList
                        If myeBDFile.Description.ToLower = fileDescription.ToLower And myeBDFile.Name.ToLower = filename.ToLower Then
                            newFileId = myeBDFile.Id
                            Exit For
                            'myeBusinessData.removeBusinessDataFile(myeBDFile)
                        End If
                    Next

                    'AddFileDialogo(myeBusinessData.Id, fileContent, filename, fileDescription, statusMessage)

                End If
            End If
            If newFileId > 0 Then
                myeBDFile = Nothing
                myeBDFile = New eBusinessDataFile(currentConnection, myeBusinessData)
                myeBusinessData.getBusinessDataFile(newFileId, myeBDFile)
                myeBDFile.checkOut("Update of " & Now.ToString, True, "")
                myeFileService = New eFileService.eFileService
                myeVault = New eVault(currentConnection)
                myeBusinessData.getCandidateVault(myeVault)
                myeFileService.Url = myeVault.fileServiceURL & "/eFileService.asmx"
                myeFileService.UploadSingleFile(fileContent, downloadFilePath)
                retValue = myeBDFile.checkIn(downloadFilePath, False, newFileId)
                If retValue <> ePLMS_OK Then
                    statusMessage = currentConnection.messageStack.Peek.text
                End If
            Else
                AddFileDialogo(myeBusinessData.Id, fileContent, filename, fileDescription, statusMessage)
            End If

        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message

        End Try




        Return retValue
    End Function

    <WebMethod(Description:="Remove Function File to a Dialog", EnableSession:=True)> _
    Public Function RemoveFileFunction(ByVal businessDataId As Integer, ByVal fileDescription As String, ByRef statusMessage As String) As Integer

        Dim retValue As Integer = ePLMS_ERROR
        Dim tempFilePath As String = ""
        Dim myeBusinessData As eBusinessData
        Dim tempFileDescription As String = "Dialog Configuration"
        Dim myeFileService As eFileService.eFileService
        Dim myeVault As eVault
        Dim myeBDFile As eBusinessDataFile
        Dim myeFileList() As eBusinessDataFile
        Dim downloadFilePath As String
        Dim newFileId As Integer

        Try
            currentConnection = Session("currentConnection")

            myeBusinessData = New eBusinessData(currentConnection)
            currentConnection.pushMessageFlag = True
            retValue = myeBusinessData.Load(businessDataId)
            currentConnection.pushMessageFlag = False
            If retValue <> ePLMS_OK Then
                statusMessage = currentConnection.messageStack.Peek.text
            Else

                myeBusinessData.getBusinessDataFileList(True, myeFileList)
                If (myeFileList IsNot Nothing) Then
                    For Each myeBDFile In myeFileList
                        If myeBDFile.Description.ToLower = fileDescription.ToLower Then
                            myeBusinessData.removeBusinessDataFile(myeBDFile)
                        End If
                    Next


                End If
            End If


        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message

        End Try




        Return retValue
    End Function



    <WebMethod(Description:="Get Dialog File", EnableSession:=True)> _
    Public Function GetTabelleEsterne(ByRef statusMessage As String) As System.Data.DataSet

        Dim retValue As Integer = ePLMS_ERROR
        Dim myAttributeList As ArrayList
        Dim myBusinessDataTypeList As ArrayList
        Dim dummyBDT As eBusinessDataType
        Dim retDataSet As New System.Data.DataSet("TabelleEsterne")

        Try
            currentConnection = Session("currentConnection")
            myBusinessDataTypeList = New ArrayList
            currentConnection.Core.getBusinessDataTypeList("ENABLED", False, myBusinessDataTypeList)
            If myBusinessDataTypeList IsNot Nothing Then
                dummyBDT = New eBusinessDataType(currentConnection)
                For Each edata As eDataListElement In myBusinessDataTypeList
                    If edata.Name.StartsWith("TE_") Then

                        If dummyBDT.Load(edata.Name) = ePLMS_OK Then
                            Dim myTable As New System.Data.DataTable(edata.Name)
                            myAttributeList = New ArrayList
                            dummyBDT.getAttributeList(myAttributeList)
                            If myAttributeList IsNot Nothing Then
                                For Each edata2 As eDataListElement In myAttributeList
                                    myTable.Columns.Add(edata2.Name.ToUpper)
                                Next
                            End If
                            retDataSet.Tables.Add(myTable)
                        End If
                    End If

                Next
            End If


        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message
            retDataSet = Nothing
        End Try

        Return retDataSet
    End Function


    <WebMethod(Description:="Get Dialog File", EnableSession:=True)> _
    Public Function GetTabellaEsternaWithData(ByVal givenTableName As String, ByRef statusMessage As String) As System.Data.DataTable

        Dim retValue As Integer = ePLMS_ERROR
        Dim myAttributeList As ArrayList
        Dim dummyBDT As eBusinessDataType
        Dim retDataSet As New System.Data.DataTable(givenTableName)
        Dim myeFind As eFind
        Try
            currentConnection = Session("currentConnection")
            If currentConnection Is Nothing Then
                login("ConfigAdmin", "Config01011990Admin", "")
            ElseIf Not currentConnection.isLoggedIn Then
                login("ConfigAdmin", "Config01011990Admin", "")
            End If
            dummyBDT = New eBusinessDataType(currentConnection)
            If dummyBDT.Load(givenTableName) = ePLMS_OK Then
                myAttributeList = New ArrayList
                dummyBDT.getAttributeList(myAttributeList)
                If myAttributeList IsNot Nothing Then
                    retDataSet.Columns.Add("eid")
                    For Each edata2 As eDataListElement In myAttributeList
                        retDataSet.Columns.Add(edata2.Name.ToUpper)
                    Next
                End If


                Dim queryString As String = "BusinessDataType={1}"
                Dim queryParams() As String = {givenTableName}

                myeFind = New eFind(currentConnection)
                myeFind.getLastRevision = True
                myeFind.queryString = queryString
                myeFind.selectAttributeListString = "Id"

                currentConnection.pushMessageFlag = True
                retValue = myeFind.Prepare(queryParams)
                currentConnection.pushMessageFlag = False
                If retValue <> ePLMS_OK Then
                    statusMessage = currentConnection.messageStack.Peek.text
                Else
                    Dim tmpReturnList = New System.Data.DataTable
                    currentConnection.pushMessageFlag = True
                    retValue = myeFind.Execute(tmpReturnList)
                    currentConnection.pushMessageFlag = False
                    If retValue = ePLMS_OK Then
                        Dim myDummyBD As New eBusinessData(currentConnection)
                        For Each row As System.Data.DataRow In tmpReturnList.Rows

                            If myDummyBD.Load(row("eId")) = ePLMS_OK Then
                                myAttributeList = New ArrayList
                                myDummyBD.getAttributeList(myAttributeList)
                                If myAttributeList IsNot Nothing Then
                                    Dim myNewRow As System.Data.DataRow = retDataSet.NewRow
                                    myNewRow("eId") = myDummyBD.Id
                                    For Each edata As eDataListElement In myAttributeList
                                        myNewRow(edata.Name.ToUpper) = edata.Value
                                    Next
                                    retDataSet.Rows.Add(myNewRow)
                                End If

                            End If
                        Next
                    End If
                End If



            End If


        Catch ex As Exception
            retValue = ePLMS_ERROR
            statusMessage = ex.Message
            retDataSet = Nothing
        End Try

        Return retDataSet
    End Function
#End Region

#Region "User Functions"
    <WebMethod(Description:="Get Groups List", EnableSession:=True)> _
    Public Function GetGroupsList(ByRef returnTable As System.Data.DataTable, ByRef exitStatus As String) As Integer

        Dim myArrayGroup As New ArrayList
        Dim rc As Integer = ePLMS_OK
        Dim myeGroup As eGroup
        If returnTable Is Nothing Then
            returnTable = New System.Data.DataTable("Groups")
        End If
        returnTable.Columns.Add("Name")
        returnTable.Columns.Add("Description")

        currentConnection = Session("currentConnection")
        rc = currentConnection.Core.getGroupList("ENABLED", myArrayGroup)
        If myArrayGroup IsNot Nothing Then
            myeGroup = New eGroup(currentConnection)
            For Each dummyGroup As eDataListElement In myArrayGroup
                If myeGroup.Load(dummyGroup.Name) = ePLMS_OK Then
                    Dim myNewGroup As System.Data.DataRow = returnTable.NewRow
                    myNewGroup("Name") = myeGroup.Name
                    myNewGroup("Description") = myeGroup.Description
                    returnTable.Rows.Add(myNewGroup)
                End If

            Next
        End If
        Return rc
    End Function
#End Region

#Region "PRIVATE FUNCTIONS"
    Private Sub EstrazioneDistinta(ByVal idCadModel As String, ByVal idParentCadModel As String, ByRef givenTable As System.Data.DataTable, ByVal parentFile As String, ByVal givenFamiglia As String)

        Dim myTemp As String = ""
        Dim myCadModel As eBusinessData

        myCadModel = New eBusinessData(currentConnection)

        If givenTable Is Nothing Then
            givenTable = New System.Data.DataTable("BOM")
            givenTable.Columns.Add("eParentId")
            givenTable.Columns.Add("eId")
            givenTable.Columns.Add("eCADName")
            givenTable.Columns.Add("eFileName")
            givenTable.Columns.Add("eFileNameParent")
            givenTable.Columns.Add("Ref")
        End If


        myCadModel.Load(idCadModel)
        myCadModel.getAttribute("NumeroParte", myTemp)
        If myTemp = "" Then
            myTemp = myCadModel.Name
        End If
        If myTemp.ToUpper = "PARAMETRI BASE" Then
            Exit Sub
        End If


        Dim myCadFiles() As eBusinessDataFile
        myCadModel.getBusinessDataFileList(True, myCadFiles)
        If myCadFiles IsNot Nothing Then

            For Each myCadFile As eBusinessDataFile In myCadFiles
                If myCadFile.Name.ToUpper.EndsWith(".IAM") Or myCadFile.Name.ToUpper.EndsWith(".IPT") Then
                    Dim myRow As System.Data.DataRow = givenTable.NewRow
                    myRow("eParentId") = idParentCadModel
                    myRow("eId") = idCadModel
                    myRow("eCADName") = myTemp
                    myRow("eFileName") = myCadFile.Name
                    myRow("eFileNameParent") = parentFile
                    myRow("Ref") = givenFamiglia & "-" & myCadModel.Project
                    givenTable.Rows.Add(myRow)

                    Dim returnBusinessDataDependencyList As New Generic.List(Of businessDataFileDependency)
                    ' Dim localNFIG As Integer = 1
                    myCadFile.getBusinessDataDependencyList(returnBusinessDataDependencyList)
                    For Each myDep As businessDataFileDependency In returnBusinessDataDependencyList
                        If myDep.originalFileName.ToUpper <> "Parametri Base.ipt".ToUpper Then
                            EstrazioneDistinta(myDep.businessDataId, myCadModel.Id, givenTable, myCadFile.Name, givenFamiglia)
                        End If


                        'localNFIG += 1
                    Next
                End If
            Next

        End If

    End Sub

#End Region

End Class