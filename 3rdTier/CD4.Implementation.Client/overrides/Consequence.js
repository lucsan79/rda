Ext.define('overrides.Consequence',{
	override : 'Ext.promise.Consequence',	
	
	 /**
     * Transform and propagate the specified value using the
     * optional callback and propagate the transformed result.
     *
     * @param {Mixed} value Value to transform and/or propagate.
     * @param {Function} [callback] Callback to use to transform the value.
     * @param {Function} deferred Deferred to use to propagate the value, if no callback
     * was specified.
     * @param {Function} deferredMethod Deferred method to call to propagate the value,
     * if no callback was specified.
     *
     * @private
     */
    propagate: function(value, callback, deferred, deferredMethod) {
        if (Ext.isFunction(callback)) {
            this.schedule(function() {
                try {
                    deferred.resolve(callback(value));
                }
                catch (e) {		
					console.log ('----------------------------------------------------------');
					console.error(e);
					console.log('');
					Ext.log.error(e); 	/* ADD LOG ERROR TO FACILITATE DEBUGGING*/
					console.log ('----------------------------------------------------------');
					
                    deferred.reject(e);
                }
            });
        }
        else {
            deferredMethod.call(this.deferred, value);
        }
    }
},function() {	
	console.log ('overriding Ext.promise.Consequence...');
});