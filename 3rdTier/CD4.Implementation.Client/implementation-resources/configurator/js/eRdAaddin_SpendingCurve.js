﻿var SpendingCurve = {
    messageError: "",
    datasource: [],
    
    ControlUniqueId: function () {
        var input = $("textarea[code='rdaw1_spendingcurve']");
        if (input.length == 1)
            return input[0].id;
        else
            return 'undefined';
    },

    ReplacedTextbox: function () {
        var input = $("textarea[code='rdaw1_spendingcurve']");
        if (input.length== 1)
            return input[0];
        else
            return null;
    },
    Amount: function () {
        var input = $("input[code='Amount']");
        if (input.length == 1)
            return input[0];
        else
            return null;
    },
    DefaultItems: function () {
       
        //if (this.datasource.length == 0)
        //{
        //    var today = new Date();
        //    var dd = today.getDate();
        //    var mm = today.getMonth() + 1; //January is 0!
        //    var yyyy = today.getFullYear();
        //    if (dd < 10) {
        //        dd = '0' + dd;
        //    }
        //    if (mm < 10) {
        //        mm = '0' + mm;
        //    }
        //    var today = dd + ' / ' + mm + ' / ' + yyyy;
        //    var baseItem = {
        //        invoicedate: today,
        //        rate: 100,
        //        index: 0
        //    };
        //    this.datasource.push(baseItem);
        //}
        
        return this.datasource;
    },
    Container: function () {
        var content = $("#iFrameSpendingCurve");
        if (content.length == 1)
            return content[0];
        else
            return null;
    },
    MandatoryError: function (actualMessage) {
    
        if (SpendingCurve.ReplacedTextbox()) { 
        if (SpendingCurve.ReplacedTextbox().value == "")
            if (SpendingCurve.messageError!="")
                actualMessage = SpendingCurve.messageError;
        }
        return actualMessage;
    },
    ValidateError: function (actualMessage) {
        if (SpendingCurve.ReplacedTextbox() != null)
        {
            if (SpendingCurve.ReplacedTextbox().value != "")
                if (SpendingCurve.messageError != "")
                    actualMessage = SpendingCurve.messageError;
        }
    return actualMessage;
}
};


SpendingCurve.InitElements = function () {
    var currentTextBox = SpendingCurve.ReplacedTextbox();
    var currentAmount = SpendingCurve.Amount();
    var myContainer = null;
    var myTableContainer = null;
    var dummyObject = null;
    if (currentTextBox != null) {
        var viewMode = 'edit';
        if (currentTextBox.disabled)
            viewMode = "view";
        var tr = $(currentTextBox).parentsUntil("div");
        tr = tr[tr.length - 1];
        $(tr).hide();
        var parent = $(tr).parent().get(0);
        var first = $(parent).children(":first");
        var myFrame = document.createElement("div");
        $(myFrame).attr("id", "frameSpendingCurve");
        $(myFrame).attr("itemId", "frameSpendingCurve");
        $(myFrame).attr("class", "frameSpendingCurve");
        $(myFrame).insertAfter(first);
        var prePanel = Ext.getCmp("myPanelTranche");
        if (prePanel != null) prePanel.destroy();
        var myPanel = Ext.create('Ext.form.Panel', {
            title: false,
            viewMode: viewMode,
            tranche: 0,
            maxValue:100,
            data:[],
            id: 'myPanelTranche',
            itemId: 'myPanelTranche',
            listeners: {
                beforerender: function (thisForm, options) {
                    thisForm.addHeader();
                    thisForm.addRowMaster('New tranche', 100.00, 100.00, new Date())
                }
            },
            cls: 'panelframeSpendingCurve',
            renderTo: 'frameSpendingCurve',
            addHeader: function () {
                this.add({
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    cls: 'scHeader',
                    defaults: {
                        xtype: 'label',
                        style: {
                            'width': '130px',
                            'padding-right': '20px'
                        }
                    },
                    items: [
                        {
                            text: 'No.',
                            style: {
                                'width': '100px',
                            }
                        },
                        {
                            text: '% of amount',
                        },
                        {
                            text: 'Invoice Date',
                        }
                        
                    ]
                });
            },
            addRowMaster: function (rowLabel, percValue, maxValue, dateValue) {
                this.add({
                    xtype: 'fieldcontainer',
                    cls: 'scRowMaster',
                    layout: 'hbox',
                    id: 'rowMaster',
                    itemId:'rowMaster',
                    defaults: {
                        style: {
                            'width': '130px',
                            'padding-right': '20px'
                        }
                    },
                    items: [
                        {
                            xtype: 'label',
                            text: rowLabel,
                            cls: 'newTrancheLabel',
                            style: {
                                'width': '100px',
                            }
                        },
                        {
                            xtype: 'numberfield',
                            id:'percTranche',
                            value: percValue,
                            decimalPrecision: 2,
                            minValue: 0.01,
                            maxValue: maxValue,
                        },
                        {
                            xtype: 'datefield',
                            id: 'dateTranche',
                            format:'d/m/Y',
                            style: {
                                'width': '150px',
                                'padding-right': '20px'
                            },
                            value: dateValue
                        },
                        {
                           
                            html: '<i  class="fas fa-plus-circle" title="Add New Tranche"></i>',
                            id: 'addTranche',
                            listeners: {
                                render: function (c) {
                                    c.getEl().on({
                                        click: function () {
                                            var myPanel = this.up("#myPanelTranche").component;
                                            myPanel.addNewRowOfTranche();
                                        }
                                    });
                                }
                            },
                            cls:'btnAdd',
                            style: {
                                'width': '50px',
                            }
                        }
                    ]
                });
            },
            getLabelTranche: function (numTranche) {
                var rowLabel = numTranche;
                var j = numTranche % 10,
                    k = numTranche % 100;
                if (j == 1 && k != 11) {
                    rowLabel = numTranche + "st tranche";
                }
                else if (j == 2 && k != 12) {
                    rowLabel = numTranche + "nd tranche";
                }
                else if (j == 3 && k != 13) {
                    rowLabel = numTranche + "rd tranche";
                }
                else
                    rowLabel = numTranche + "th tranche";
                return rowLabel;
            },


            updateTrancheNumber: function () {
                this.tranche = 1;
                if (this.data == null)
                    return;

                this.tranche = this.data.length+1;
            },
            updateRowMaster: function () {
                var self = this;
                var percField = self.down("#percTranche");
                var dateField = self.down("#dateTranche");
                var button = self.down("#addTranche")
                self.maxValue = 100;
                var result = "";
                if (self.data != null) {
                    for (var index = 0; index < self.data.length; index++) {
                        self.maxValue = self.maxValue - self.data[index].percValue;
                        if (result != "")
                            result = result + ";";
                        
                        result = result + self.data[index].tranche + "-" + self.data[index].percValue + "-" + Ext.Date.format(self.data[index].dateValue, 'd/m/Y');
                    }
                }
                percField.maxValue = self.maxValue;
                percField.setValue(self.maxValue);
                percField.setReadOnly(false);
                if (self.tranche >= 4 || self.maxValue<=0) {
                    percField.setReadOnly(true);
                }
                if (self.tranche >= 5 || self.maxValue <= 0)
                    button.update("<i  class=\"fas fa-plus-circle fa-plus-circle-disabled\" title=\"Tranche limit reached\"></i>")
                else
                    button.update("<i  class=\"fas fa-plus-circle\" title=\"Add New Tranche\"></i>")

                var message = CDI.service.Translate.data['dialogconfig-rda-spendingcurve-no-100perc'];
                if (self.maxValue == 0)
                    message = "";
                SpendingCurve.OnTrancheSelected(result, message);
            },
            addNewRowOfTranche() {
                var self = this;
                if (self.maxValue <= 0)
                    return;
                var percField = self.down("#percTranche");
                var dateField = self.down("#dateTranche");
                self.updateTrancheNumber();
                var newTranche = self.tranche;
                var myIdRow = 'rt_' + newTranche;
                var myNewData = new Object();
                myNewData.id = myIdRow;
                myNewData.tranche = newTranche;
                myNewData.percValue = percField.value;
                myNewData.dateValue = dateField.value;
                self.drawTranche(myNewData);
                if (self.data == null)
                    self.data = [myNewData];
                else
                    self.data.push(myNewData);
                self.updateTrancheNumber();
                self.updateRowMaster();
            },
            removeRowOfTranche(currentRow) {
                var self = this;
                var newArray = self.data.slice(0);
                var itemRemoved = self.data[currentRow.tranche - 1];
                for (var index = 1; index <= 4; index++) {
                    var row = self.down("#rt_" + index);
                    if (row) row.destroy();
                }
                self.data = null;
                for (var index = 0; index < newArray.length; index++) {
                    if (newArray[index].tranche != currentRow.tranche) {
                        self.updateTrancheNumber();
                        var newTranche = self.tranche;
                        var myIdRow = 'rt_' + newTranche;
                        var myNewData = new Object();
                        myNewData.id = myIdRow;
                        myNewData.tranche = newTranche;
                        myNewData.percValue = newArray[index].percValue;
                        myNewData.dateValue = newArray[index].dateValue;
                        self.drawTranche(myNewData);
                        if (self.data == null)
                            self.data = [myNewData];
                        else
                            self.data.push(myNewData);
                    }
                }
                self.updateTrancheNumber();
                self.updateRowMaster();
            },
            drawTranche(objectData) {
                var self = this;
                self.add({
                    xtype: 'fieldcontainer',
                    cls: 'scRowTranche',
                    id: objectData.id,
                    itemId: objectData.id,
                    tranche: objectData.tranche,
                    layout: 'hbox',
                    defaults: {
                        style: {
                            'width': '130px',
                            'padding-right': '20px'
                        }
                    },
                    items: [
                        {
                            xtype: 'label',
                            id: 'lt_' + objectData.tranche,
                            itemId: 'lblrt' + objectData.tranche,
                            text: myPanel.getLabelTranche(objectData.tranche),
                            cls: 'newTrancheLabel',
                            style: {
                                'width': '100px',
                            }
                        },
                        {
                            xtype: 'numberfield',
                            id: 'ptr_' + objectData.tranche,
                            itemId: 'ptr_' + objectData.tranche,
                            value: objectData.percValue,
                            decimalPrecision: 2,
                            minValue: 0.01,
                            listeners: {
                                change: function (field, newVal, oldVal) {
                                    var myPanel = this.up("#myPanelTranche");
                                    var currentRow = this.up();
                                    if (myPanel.data != null) {
                                        for (var idx = 0; idx < myPanel.data.length; idx++) {
                                            if (myPanel.data[idx].tranche == currentRow.tranche) {
                                                myPanel.data[idx].percValue = newVal;
                                                break;
                                            }
                                        }
                                        myPanel.updateTrancheNumber();
                                        myPanel.updateRowMaster();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'datefield',
                            id: 'dtr_' + objectData.tranche,
                            itemId: 'dtr_' + objectData.tranche,
                            format: 'd/m/Y',
                            style: {
                                'width': '150px',
                                'padding-right': '20px'
                            },
                            value: objectData.dateValue
                        },
                        {

                            html: '<i class="fas fa-trash-alt" title="Remove tranche"></i>',
                            listeners: {
                                render: function (c) {
                                    c.getEl().on({
                                        click: function () {
                                            var row = this.up().component;
                                            this.up("#myPanelTranche").component.removeRowOfTranche(row);
                                        }
                                    });
                                }
                            },
                            cls: 'btnAdd',
                            style: {
                                'width': '50px',
                            }
                        }
                    ]
                });

            }

        });
        if (viewMode == "view") {
            var data = currentTextBox.value.split(";");
            for (var index = 0; index < data.length; index++) {
                var items = data[index].split("-");
                var parts = items[2].split('/');
                var mydate = new Date(parts[2], parts[1] - 1, parts[0]); 
                var myIdRow = 'rt_' + items[0];
                var myNewData = new Object();
                myNewData.id = myIdRow;
                myNewData.tranche = items[0];
                myNewData.percValue = items[1];
                myNewData.dateValue = mydate;
                myPanel.drawTranche(myNewData);
                if (myPanel.data == null)
                    myPanel.data = [myNewData];
                else
                    myPanel.data.push(myNewData);
            }
            myPanel.updateTrancheNumber();
            myPanel.updateRowMaster();
            SpendingCurve.EnableDisableControl("disable");
        }
        
    }
};

SpendingCurve.EnableDisableControl = function (flag) {
    if (flag === undefined) flag = "enable";
    try {
        var spending = SpendingCurve.ReplacedTextbox();
        if (spending != null) {
            var myPanel = Ext.getCmp("myPanelTranche");
            if (myPanel != null) {
                var rowMaster = myPanel.down("#rowMaster");
                if (rowMaster) rowMaster.setDisabled(flag == "disable");
                for (var index = 1; index <= 4; index++) {
                    var myRow = myPanel.down("#rt_" + index);
                    if (myRow) {
                        myRow.setDisabled(flag == "disable");
                    }
                }
            }
        }
        //if (spending && spending.disabled)
        //    myFrame.contentWindow.disableControls();
        //else if (spending && !spending.disabled)
        //    myFrame.contentWindow.enableControls();
    }
    catch (err) { };
}

SpendingCurve.SetResult = function (givenValue,givenMessage) {
    SpendingCurve.ReplacedTextbox.value = givenValue;
    SpendingCurve.MessageError = givenMessage;
}

SpendingCurve.EnableControl = function (controlId) {
    SpendingCurve.EnableDisableControl("enable");
    return true;
    var spending = SpendingCurve.ReplacedTextbox();
    if (spending) {
       
        if (spending.id == controlId) {
            var currentAmount = SpendingCurve.Amount();
            var link = "../SpendingCurve/default.aspx?viewMode=edit&amount=" + $(currentAmount).val();
            var myFrame = SpendingCurve.Container();
            if (myFrame) {
                $(myFrame).attr("src", link);
                return true;
            }
        }


    }

    
    return false;

};


jQuery.fn.insertAt = function (index, element) {
    var lastIndex = this.children().size();
    if (index < 0) {
        index = Math.max(0, lastIndex + 1 + index);
    }
    this.append(element);
    if (index < lastIndex) {
        this.children().eq(index).before(this.children().last());
    }
    return this;
}

function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}


SpendingCurve.OnTrancheSelected = function (result, resultError) {

    if (SpendingCurve.ReplacedTextbox())
    {
        SpendingCurve.ReplacedTextbox().value = result;
        SpendingCurve.messageError = resultError;
    }
    
}

function getIframeWindow(iframe_object) {
    var doc;

    if (iframe_object.contentWindow) {
        return iframe_object.contentWindow;
    }

    if (iframe_object.window) {
        return iframe_object.window;
    }

    if (!doc && iframe_object.contentDocument) {
        doc = iframe_object.contentDocument;
    }

    if (!doc && iframe_object.document) {
        doc = iframe_object.document;
    }

    if (doc && doc.defaultView) {
        return doc.defaultView;
    }

    if (doc && doc.parentWindow) {
        return doc.parentWindow;
    }

    return undefined;
}

