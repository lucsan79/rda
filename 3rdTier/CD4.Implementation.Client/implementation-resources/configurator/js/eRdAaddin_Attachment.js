﻿
var RDAAttachment = {
    messageError: "",
    datasource: [],
    
    OfferAttachmentTextbox: function () {
        var input = $("input[code='rdaw1_offerattachment']");
        if (input.length== 1)
            return input[0];
        else
            return null;
    },
    SSAttachmentTextbox: function () {
        var input = $("input[code='rdaw1_ssattachment']");
        if (input.length == 1)
            return input[0];
        else
            return null;
    },
    DefaultItems: function () {
        return this.datasource;
    },
    ContainerOffer: function () {
        var content = $("#iFrameOfferAttachment");
        if (content.length == 1)
            return content[0];
        else
            return null;
    },
    ContainerSS: function () {
        var content = $("#iFrameSSAttachmnet");
        if (content.length == 1)
            return content[0];
        else
            return null;
    }
};


RDAAttachment.InitElements = function () {
    
    var currentTextBox = RDAAttachment.OfferAttachmentTextbox();
    var myContainer = null;
    var myTableContainer = null;
    var dummyObject = null;
    if (currentTextBox != null) {
        var table = $(currentTextBox).parentsUntil("div");
        table = table[table.length - 1];
        $(table).hide();
        var tdLabel = $(table).find("td").get(0);
        var mandatory = false;
        tdLabel = $(tdLabel).html();
        if (tdLabel.indexOf("<FONT title=\"IS MANDATORY\" color=red>*</FONT>") != -1)
            mandatory = true;
        tdLabel = tdLabel.replace("<FONT title=\"IS MANDATORY\" color=red>*</FONT>", "");
        var parent = $(table).parent().get(0);
        var first = $(parent).children(":first");
        
        var viewMode = 'edit';
        if (currentTextBox.disabled)
            viewMode = "view";
        filename = $(currentTextBox).val();

        var myDiv = document.createElement("div");
        $(myDiv).attr("id", "dialogAttach");
        $(myDiv).attr("itemId", "dialogAttach");
        $(myDiv).attr("class", "dialogAttach");
        $(myDiv).attr("width", "100%");
        $(myDiv).attr("height", "40px");
        $(myDiv).insertAfter(first);
      
        var filename = $(currentTextBox).val();
        var old = Ext.getCmp("fileAttachment");
        if (old != null) {
            old.destroy();
        }
        var fibasic = Ext.create('Ext.form.field.File', {
            id: 'fileAttachment',
            itemId: 'fileAttachment',
            cls: 'extattachment',
            hideLabel: true,
            listeners: {
                afterrender: function (fld) {
                    fld.setRawValue(filename);
                    if (currentTextBox.disabled)
                        fld.disable();
                },
                change: function (fld, value) {
                    var newValue = value.replace(/C:\\fakepath\\/g, '');
                    fld.setRawValue(newValue);
                    RDAAttachment.OfferUploaded(newValue);
                    CDI.service.erdaAction.cacheFileData(fld, 'erda_std_attach');
                }
            }
        });
        setTimeout(function () { fibasic.render("dialogAttach"); }, 200);
    }
   
};

RDAAttachment.InitSSElements = function () {

    var currentTextBox = RDAAttachment.SSAttachmentTextbox();
    var myContainer = null;
    var myTableContainer = null;
    var dummyObject = null;
    if (currentTextBox != null) {
        var table = $(currentTextBox).parentsUntil("div");
        table = table[table.length - 1];
        $(table).hide();
        var tdLabel = $(table).find("td").get(0);
        var mandatory = false;
        tdLabel = $(tdLabel).html();
        if (tdLabel.indexOf("<FONT title=\"IS MANDATORY\" color=red>*</FONT>") != -1)
            mandatory = true;
        tdLabel = tdLabel.replace("<FONT title=\"IS MANDATORY\" color=red>*</FONT>", "");
        var parent = $(table).parent().get(0);
        var first = $(parent).children(":first");

        var viewMode = 'edit';
        if (currentTextBox.disabled)
            viewMode = "view";
        
        var myDiv = document.createElement("div");
        $(myDiv).attr("id", "dialogSSAttach");
        $(myDiv).attr("itemId", "dialogSSAttach");
        $(myDiv).attr("class", "dialogSSAttach");
        $(myDiv).attr("width", "100%");
        $(myDiv).attr("height", "40px");
        $(myDiv).insertAfter(first);

        
        var filename = $(currentTextBox).val();
        var old = Ext.getCmp("fileSSAttachment");
        if (old != null) {
            old.destroy();
        }
        var fibasic = Ext.create('Ext.form.field.File', {
            id: 'fileSSAttachment',
            itemId: 'fileSSAttachment',
            cls: 'extattachment',
            hideLabel: true,
            listeners: {
                afterrender: function (fld) {
                    fld.setRawValue(filename);
                    if (currentTextBox.disabled)
                        fld.disable();
                },
                change: function (fld, value) {
                    var newValue = value.replace(/C:\\fakepath\\/g, '');
                    fld.setRawValue(newValue);
                    RDAAttachment.SSUploaded(newValue);
                    CDI.service.erdaAction.cacheFileData(fld, 'erda_ss_attach');
                }
            }
        });
        setTimeout(function () { fibasic.render("dialogSSAttach"); }, 200);


    }


};



RDAAttachment.EnableControl = function (controlId) {

    try {
        var button = null;
        var offer = RDAAttachment.OfferAttachmentTextbox();
        var ss = RDAAttachment.SSAttachmentTextbox();
        var myFrame;
        if (offer != null && offer.id == controlId) {
            myFrame = Ext.getCmp('fileAttachment');
        }
        else if (ss != null && ss.id == controlId) {
            myFrame = Ext.getCmp('fileSSAttachment');
        }
        if (myFrame != null ) {
            myFrame.enable();
        }
    }
    catch (err) { };
    
    //return false;



    //debugger;
        //var file = Ext.getCmp('fileAttachment');
        //if (file != null)
        //    if (value) {
        //        file.disable();
        //    }
        //    else {
        //        file.enable();
        //    }


};



RDAAttachment.OfferUploaded = function (givenValue) {
    if (RDAAttachment.OfferAttachmentTextbox() != null)
        RDAAttachment.OfferAttachmentTextbox().value = givenValue;
        
}

RDAAttachment.SSUploaded = function (givenValue) {
    if (RDAAttachment.SSAttachmentTextbox() != null)
        RDAAttachment.SSAttachmentTextbox().value = givenValue;
}



RDAAttachment.OfferValidate = function (givenValue) {
    if (RDAAttachment.OfferAttachmentTextbox() != null) 
        if (RDAAttachment.SSAttachmentTextbox() != null && RDAAttachment.SSAttachmentTextbox().value != "" && RDAAttachment.OfferAttachmentTextbox().value!="")
            if (RDAAttachment.OfferAttachmentTextbox().value.toLowerCase() == RDAAttachment.SSAttachmentTextbox().value.toLowerCase())
                return ("Attention! Offer file the same of Single Source attachment");
    

    return "";
}

RDAAttachment.SSValidate = function (givenValue) {
    if (RDAAttachment.SSAttachmentTextbox() != null)
        if (RDAAttachment.OfferAttachmentTextbox() != null && RDAAttachment.OfferAttachmentTextbox().value != "" && RDAAttachment.SSAttachmentTextbox().value!="")
            if (RDAAttachment.OfferAttachmentTextbox().value.toLowerCase() == RDAAttachment.SSAttachmentTextbox().value.toLowerCase())
                return "Attention! Single Source file is the same of Offer attachment";

    return "";
}