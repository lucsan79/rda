﻿var jsController = null;
var RemedyIdentify = {
    Supplier: {
        Name:function(){
            return "SupplierRemedy"
        },
        selector: function () {
            var input = $("input[code='SupplierRemedy']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        istances: function (searchParam) {
            return RemedyIdentify.GetSupplierList(searchParam);
        }
    }
   
};

RemedyIdentify.InitElements = function (givenController) {
    jsController = givenController;

    var currentSelector = RemedyIdentify.Supplier.selector();
    var listOfItems = null;
    var mySelection = null;
    if (currentSelector != null) {

        var currentValue = currentSelector.value;
        mySelection = document.createElement("select");
        $.each(currentSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;// RdAProjectSettings.Brand.istances();
        CommonPrepareComboBoxData(listOfItems, mySelection, currentSelector);

      
    }
    
};




RemedyIdentify.GetSupplierList = function (searchParam) {
    var selectedBrand = "";
    try {
        //selectedBrand = RdABrand.GetSelection();
        selectedBrand = RdAWBSSettings.Brand.selector().value;
    } catch (err) { }
    if (selectedBrand == "") {
        try {
            if (RdAWBSSettings.Competence.Name().toLowerCase() == "transversal")
                selectedBrand = "maserati";
        } catch (err) { };
    }
    var currentSelector = RemedyIdentify.Supplier.selector();
    var currentValue = currentSelector.value;
    var currentCBS = RdAWBSSettings.CBS.actualvalue();
    var currentWBS = RdAWBSSettings.WBS.actualvalue();
    currentValue = encodeURIComponent(currentValue);
    if (searchParam != null)
        currentValue = searchParam;


    var data = {
        reqtype: 'GetRemedySupplierList',
        basicList: '',
        brand: selectedBrand,
        lvl1: null,
        lvl2: null,
        searchItem: currentValue,
        competence: null,
        cbs: currentCBS,
        wbs: currentWBS,
    };
    return RemedyIdentify.GetData(currentValue, data);

};

RemedyIdentify.SetControlRed = function (controlId) {
    var objectType = "";
    if (RemedyIdentify.Supplier.selector() != null && controlId == RemedyIdentify.Supplier.selector().id)
        objectType = RemedyIdentify.Supplier.Name();
    if (objectType != "") {
        var className = "." + objectType + ".custom-combobox";
        var myInput = $(className).find(".custom-combobox-input");
        $(myInput).addClass('inputred');
        return true;
    }
    return false;
};
RemedyIdentify.SetControlWhite = function (controlId) {
    var objectType = "";
    if (RemedyIdentify.Supplier.selector() != null && controlId == RemedyIdentify.Supplier.selector().id)
        objectType = RemedyIdentify.Supplier.Name();
    if (objectType != "") {
        var className = "." + objectType + ".custom-combobox";
        var myInput = $(className).find(".custom-combobox-input");
        $(myInput).removeClass('inputred');
        return true;
    }
    return false;
};


RemedyIdentify.EnableControl = function (controlId) {
    var currentControl = RemedyIdentify.Supplier.Name();
    var selector = RemedyIdentify.Supplier.selector();
    if (selector != null && controlId == selector.id) {
        var controName = "#" + currentControl + "combobox";
        if ($(controName) != null) {
            CommonInitComboBox(selector);
            $(controName).combobox();
            //controName = "#" + currentControl + "image";
            //if ($(controName) != null && $(controName).length == 1) {
            //    $(controName)[0].style.cursor = "hand";
            //    $(controName)[0].onclick = RemedyIdentify.GetSupplierAdvanced;
            //}

            return true;
        }

    }
    return false;
};
RemedyIdentify.DisableControl = function (controlId) {
    //if (RemedyIdentify.Supplier.selector().id == controlId)
        //alert('Disable');
};


RemedyIdentify.ValidateBudget = function (control) {
    var input = $("input[code='Amount']");
    if (input != null)
        if (input.length > 0)
            input = input[0];
    if (control != null && input != null) {
        if (control.id == input.id) {
            var supplier = RemedyIdentify.Supplier.selector();
            if (supplier)
            {
                var http = GetSuppliersXmlHttpObject();
                var ssid = Math.random();
                var url = "../Remedy/AjaxRequest.aspx?reqtype=VerifyBudget&value=" + input.value + "&supplier=" + supplier.value + "&ssid=" + ssid;
                try {
                    http.open("GET", url, false);
                    http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    http.send(null);
                    var autoSelection = http.responseText;
                    if (autoSelection != "")
                        return autoSelection.split(";");
                    else
                        return null;
                }
                catch (err) { alert(err.message); }

                return "error budget";
            }
        }
    }
    return "";
};
RemedyIdentify.GetData = function (currentValue, data, url) {
    if (url === undefined || url === "") url = "erda/dialog/getdialogitems";
    if (jsController) {
        var response = jsController.performRequest(url, 'POST', data)
        if (response === undefined || response === "") return "";

        return response.split(";");
    }
    return null;

};

CommonPrepareComboBoxData = function (listOfItems, mySelection, currentSelector) {
    var id = currentSelector.id;
    var currentValue = currentSelector.value;
    var isDisabled = $(currentSelector).is(':disabled');
    var objectType = currentSelector.attributes["code"].value;


    var parent = $(currentSelector).parent().get(0);
    $(currentSelector).hide();
    $(mySelection).attr("id", objectType + "combobox");
    if (currentSelector.value != "") {
        option = "<option selected=\"selected\" value=\"" + currentSelector.value + "\">" + currentSelector.value + "</option>";
        $(mySelection).append(option);
    }


    $(parent).append(mySelection);

    CommonInitComboBox(currentSelector);

    if (isDisabled) {
        $(mySelection).prop('disabled', 'disabled');
    }
    else {
        $("#" + mySelection.id).combobox();
    }
};



var SupplierIdentify = {
    Supplier: {
        Name: function () {
            return "Supplier";
        },
        selector: function () {
            var input = $("input[code='Supplier']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        istances: function (searchParam) {
            return SupplierIdentify.GetSupplierList(searchParam);
        }
    }

};

SupplierIdentify.InitElements = function () {

    var currentSelector = SupplierIdentify.Supplier.selector();
    var listOfItems = null;
    var mySelection = null;
    if (currentSelector != null) {

        var currentValue = currentSelector.value;
        mySelection = document.createElement("select");
        $.each(currentSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;// RdAProjectSettings.Brand.istances();
        CommonPrepareComboBoxData(listOfItems, mySelection, currentSelector);
    }
};



SupplierIdentify.SetControlValue = function (controlText, controlValue) {

    var objectType = "";
    if (SupplierIdentify.Supplier.selector() != null && controlId == SupplierIdentify.Supplier.selector().id)
        objectType = SupplierIdentify.Supplier.Name();
    if (objectType != "") {
        var className = "." + objectType + ".custom-combobox";
        var myInput = $(className).find(".custom-combobox-input");
        $(myInput).addClass('inputred');
        return true;
    }

    return false;
};
SupplierIdentify.GetSupplierList = function (searchParam) {
    var selectedBrand = "";
    try {
        //selectedBrand = RdABrand.GetSelection();
        selectedBrand = RdAWBSSettings.Brand.selector().value;
    } catch (err) { }
    if (selectedBrand == "") {
        try {
            if (RdAWBSSettings.Competence.Name().toLowerCase() == "transversal")
                selectedBrand = "maserati";
        } catch (err) { };
    }
    var currentSelector = SupplierIdentify.Supplier.selector();
    var currentValue = currentSelector.value;
    var currentCBS = RdAWBSSettings.CBS.actualvalue();
    var currentWBS = RdAWBSSettings.WBS.actualvalue();;

    currentValue = encodeURIComponent(currentValue);
    if (searchParam != null)
        currentValue = searchParam;

    var data = {
        reqtype: 'GetSupplierList',
        basicList: '',
        brand: selectedBrand,
        lvl1: null,
        lvl2: null,
        searchItem: currentValue,
        competence: null,
        cbs: currentCBS,
        wbs: currentWBS,
    };
    return SupplierIdentify.GetData(currentValue, data);

};

SupplierIdentify.SetControlRed = function (controlId) {

    var objectType = "";
    if (SupplierIdentify.Supplier.selector() != null && controlId == SupplierIdentify.Supplier.selector().id)
        objectType = SupplierIdentify.Supplier.Name();
    if (objectType != "") {
        var className = "." + objectType + ".custom-combobox";
        var myInput = $(className).find(".custom-combobox-input");
        $(myInput).addClass('inputred');
        return true;
    }

    return false;
};
SupplierIdentify.SetControlWhite = function (controlId) {
    var objectType = "";
    if (SupplierIdentify.Supplier.selector() != null && controlId == SupplierIdentify.Supplier.selector().id)
        objectType = SupplierIdentify.Supplier.Name();
    if (objectType != "") {
        var className = "." + objectType + ".custom-combobox";
        var myInput = $(className).find(".custom-combobox-input");
        $(myInput).removeClass('inputred');
        return true;
    }

    return false;
};


SupplierIdentify.EnableControl = function (controlId) {

    var currentControl = SupplierIdentify.Supplier.Name();
    var selector = SupplierIdentify.Supplier.selector();
    if (selector != null && controlId == selector.id) {
        var controName = "#" + currentControl + "combobox";
        if ($(controName) != null && $(controName).length == 1) {
            CommonInitComboBox(selector);
            $(controName).combobox();
            //controName = "#" + currentControl + "image";
            //if ($(controName) != null && $(controName).length == 1) {
            //    $(controName)[0].style.cursor = "hand";
            //    $(controName)[0].onclick = SupplierIdentify.GetSupplierAdvanced;
            //}

            return true;
        }
       
    }
    return false;
};
SupplierIdentify.DisableControl = function (controlId) {

   
    //if (SupplierIdentify.Supplier.selector().id == controlId)
    //alert('Disable');
};


SupplierIdentify.ValidateBudget = function (control) {
    var input = $("input[code='Amount']");
    if (input != null)
        if (input.length > 0)
            input = input[0];
    if (control != null && input != null) {
        if (control.id == input.id) {
            var supplier = SupplierIdentify.Supplier.selector();
            if (supplier) {
                var http = GetSuppliersXmlHttpObject();
                var ssid = Math.random();
                var url = "../Remedy/AjaxRequest.aspx?reqtype=VerifyBudget&value=" + input.value + "&supplier=" + supplier.value + "&ssid=" + ssid;
                try {
                    http.open("GET", url, false);
                    http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    http.send(null);
                    var autoSelection = http.responseText;
                    if (autoSelection != "")
                        return autoSelection.split(";");
                    else
                        return null;
                }
                catch (err) { alert(err.message); }

                return "error budget";
            }
        }
    }
    return "";
};


SupplierIdentify.GetData = function (currentValue, data, url) {
    if (url === undefined || url === "") url = "erda/dialog/getdialogitems";
    if (jsController) {
        var response = jsController.performRequest(url, 'POST', data)
        if (response === undefined || response === "") return "";

        
        return response.split(";");
    }
    return null;

};
GetSuppliersXmlHttpObject = function () {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari8.  
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE512.  
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
};

CommonInitComboBox = function (currentSelector) {
    $.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<span>")
                .addClass(this.element[0].id.replace("combobox", "") + " custom-combobox")
                .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
                value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
                .appendTo(this.wrapper)
                .val(value)
                .attr("title", "")
                .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy(this, "_source2"),
                    open: function (event, ui) {
                        var autocomplete = $(".ui-autocomplete");
                        var oldTop = $(this).position().top;
                        var newTop = $(window).height();
                        if ((newTop - oldTop) < 350) {
                            newTop = newTop - oldTop - 20;
                            newTop = newTop + 'px';
                            autocomplete.css("max-height", newTop);
                        }
                    }
                    
                })

                .tooltip({
                    classes: {
                        "ui-tooltip": "ui-state-highlight"
                    }
                });

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                },
               
                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function () {
            var input = this.input,
                wasOpen = false;

            $("<a>")
                .attr("tabIndex", -1)
                .attr("title", "Show All Items")
                .tooltip()
                .appendTo(this.wrapper)
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass("ui-corner-all")
                .addClass("custom-combobox-toggle ui-corner-right")
                .on("mousedown", function () {
                    wasOpen = input.autocomplete("widget").is(":visible");
                })
                .on("click", function () {
                    input.trigger("focus");

                    // Close if already visible
                    if (wasOpen) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    input.autocomplete("search", "");
                });
        },

        _source: function (request, response) {

            if (request.term.trim == "") {
                return null;
            }
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text =  $(this).text();
                if (this.value && (!request.term || matcher.test(text)))

                    return {
                        label: text,
                        value: text,
                        option: this
                    };

            }));
        },

        _source2: function (request, response) {
            var ssid = Math.random();
            var currentName = this.element[0].id.replace("combobox", "");

            //var myurl = "../Admin/AjaxRequest.aspx?reqtype=GetList&list=" + this.element[0].id.replace("combobox","") + "&searchItem=" + request.term + "&ssid=" + ssid;
            var myData = null;
            if (currentName == RemedyIdentify.Supplier.Name())
                myData = RemedyIdentify.Supplier.istances(request.term);
            else if (currentName == SupplierIdentify.Supplier.Name())
                myData = SupplierIdentify.Supplier.istances(request.term);


            if (myData != null) {
                response($.map(myData, function (item) {
                    if (item != "") {
                        if (typeof item === "string") {
                            return {
                                label: item.split("|")[1].trim(),
                                value: item.split("|")[1].trim().split("$")[0],
                                option: this
                            };
                        }
                        return $.extend({
                            label: item.label.trim(),
                            value: item.value.trim(),
                            option: this
                        }, item);
                    }
                }));
            }



        },
        _removeIfInvalid: function (event, ui) {

            document.getElementById(currentSelector.id).value = "";
            // Selected an item, nothing to do
            if (ui.item) {
                //RdAProjectSettings.OnSelectItemChanged(currentSelector, ui.item.value, document.getElementById(currentSelector.id).value);
                document.getElementById(currentSelector.id).value = ui.item.value;
                return;
            }

            // Search for a match (case-insensitive)
            var matchText = "";
            var matchValue = "";
            var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
            valueLowerCase = valueLowerCase.trim();
            if (valueLowerCase != "") {
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        matchText = $(this).text();
                        matchValue = $(this).val();

                        return false;
                    }
                });
            }

            // Found a match, nothing to do
            if (valid) {
                document.getElementById(currentSelector.id).value = matchValue;
                this.input.val(matchText);

                return;
            }

            // Remove invalid value
            this.input
                .val("")
                .attr("title", value + " didn't match any item")
                .tooltip("open");
            this.element.val("");
            document.getElementById(currentSelector.id).value = "";
            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input.autocomplete("instance").term = "";
            
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });

};

