﻿var jsController=null;
var RdAWBSSettings = {
    
    Competence: {
        Name: function () {
            return  jsController.getFeatureValue("RDACompetence");
        }
       
    },
    Brand: {
        Name: function () {
            return "Brand"
        },
        selector: function () {
            var input = $("input[code='Brand']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        
        istances: function (searchParam) {
            return RdAWBSSettings.GetBrand(searchParam);
        }
    },
    Project: {
        Name: function () {
            return "ProjectName"
        },
        selector: function () {
            var input = $("input[code='ProjectName']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='ProjectName']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetProject(searchParam);
        }
    },
    ActivityNPI: {
        Name: function () {
            return "ActivityNPI"
        },
        selector: function () {
            var input = $("input[code='ActivityNPI']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='ActivityNPI']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetActivityNPI(searchParam);
        }
    },
    Content: {
        Name: function () {
            return "Contents"
        },
        selector: function () {
            var input = $("input[code='Contents']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='Contents']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetContent(searchParam);
        }
    },
    FuelSupply: {
        Name: function () {
            return "FuelSupply"
        },
        selector: function () {
            var input = $("input[code='FuelSupply']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='FuelSupply']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetFuelSupply(searchParam);
        }
    },
    Engine: {
        Name: function () {
            return "Engine"
        },
        selector: function () {
            var input = $("input[code='Engine']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='Engine']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetEngine(searchParam);
        }
    },
    RequestType: {
        Name: function () {
            return "RequestType"
        },
        selector: function () {
            var input = $("input[code='RequestType']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='RequestType']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetRequestType(searchParam);
        }
    },
    CDC: {
        Name: function () {
            return "CDC"
        },
        selector: function () {
            var input = $("input[code='CDC']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='CDC']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetWBSWizard('CDC',searchParam);
        }
    },
    Area: {
        Name: function () {
            return "Area"
        },
        selector: function () {
            var input = $("input[code='Area']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='Area']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetWBSWizard('AREA', searchParam);
        }
    },
    System: {
        Name: function () {
            return "System"
        },
        selector: function () {
            var input = $("input[code='System']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='System']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetWBSWizard('SYSTEM', searchParam);
        }
    },
    
    CBS: {
        Name: function () {
            return "CBS"
        },
        selector: function () {
            var input = $("input[code='CBS']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='CBS']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetWBSWizard('CBS', searchParam);
        }
    },
    WBS: {
        Name: function () {
            return "WBS"
        },
        selector: function () {
            var input = $("input[code='WBS']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
            return null;
        },
        updateValue: function (givenValue) {
            var input = $("input[code='WBS']");
            if (input.length == 1)
                input[0].value = givenValue;
        },
        istances: function (searchParam) {
            return RdAWBSSettings.GetWBSWizard('WBS', searchParam);
        }
    },
    HOURS: {
       
        selector: function () {
            var input = $("input[code='Hours']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        actualvalue: function () {
            if (this.selector())
                return this.selector().value;
        },
        setLabel: function () {
            var textBox = RdAWBSSettings.HOURS.selector();
            if (textBox) {
                try {
                    var parent = textBox.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.childNodes[0];
                    if (parent) {
                        if (parent.innerHTML.toLowerCase().indexOf("remedy days #")!=-1) {
                            parent.innerHTML = parent.innerHTML.toLowerCase().replace("remedy days #", "Days #");
                            var brand = "";
                            var CBS = "";
                            var WBS = "";
                            try {
                                brand = RdAWBSSettings.Brand.selector().value;
                            } catch (err) { };
                            if (brand == "") {
                                try {
                                    if (RdAWBSSettings.Competence.Name().toLowerCase() == "transversal")
                                        brand = "maserati";
                                } catch (err) { };
                            }
                            try {
                                CBS = RdAWBSSettings.CBS.actualvalue();
                            } catch (err) { };

                            try {
                                WBS = RdAWBSSettings.WBS.actualvalue();
                            } catch (err) { };


                            if (brand.toLowerCase() == "maserati") {
                                parent.innerText = "Days # (Hours ONLY for FCA suppliers)";
                            }
                            if (CBS != null && WBS != null)
                                if (CBS.toLowerCase() == "test/tool proto/outsource&facilities" && WBS.toLowerCase().indexOf("test") != -1)
                                    parent.innerText = "Quantity";
                        }
                    }
                }
                catch (err) { }
            }
        }
    }
};

RdAWBSSettings.InitElements = function (givenController) {
    jsController = givenController;
    var listOfItems = null;
    var mySelection = null;

    RdAWBSSettings.HOURS.setLabel();
    var BrandSelector = RdAWBSSettings.Brand.selector();
    if (BrandSelector != null) {

        var currentValue = BrandSelector.value;
        mySelection = document.createElement("span");
        $.each(BrandSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = RdAWBSSettings.Brand.istances("");
        RdAWBSSettings.PrepareComboBoxData2(listOfItems, mySelection, BrandSelector, "", BrandSelector.value,"OnBrandSelction");
    }


    var ProjectSelector = RdAWBSSettings.Project.selector();
    if (ProjectSelector != null) {

        var currentValue = ProjectSelector.value;
        mySelection = document.createElement("select");
        $.each(ProjectSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, ProjectSelector);
        if (BrandSelector != null) {
            if (BrandSelector.value == null || BrandSelector.value == "") {
                RdAWBSSettings.ResetControls(RdAWBSSettings.Project.Name());
            }
        }
    }
    
    var ActivityNPISelector = RdAWBSSettings.ActivityNPI.selector();
    if (ActivityNPISelector != null) {

        var currentValue = ActivityNPISelector.value;
        mySelection = document.createElement("select");
        $.each(ActivityNPISelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, ActivityNPISelector);
        if (ProjectSelector.value == null || ProjectSelector.value == "") {
            RdAWBSSettings.ResetControls(RdAWBSSettings.ActivityNPI.Name());
        }
    }

    var ContentSelector = RdAWBSSettings.Content.selector();
    if (ContentSelector != null) {

        var currentValue = ContentSelector.value;
        mySelection = document.createElement("select");
        $.each(ContentSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, ContentSelector);
        if (ProjectSelector.value == null || ProjectSelector.value == "") {
            RdAWBSSettings.ResetControls(RdAWBSSettings.Content.Name());
        }

    }

    var FuelSupplySelector = RdAWBSSettings.FuelSupply.selector();
    if (FuelSupplySelector != null) {

        var currentValue = FuelSupplySelector.value;
        mySelection = document.createElement("span");
        listOfItems = RdAWBSSettings.FuelSupply.istances();;
        RdAWBSSettings.PrepareComboBoxData2(listOfItems, mySelection, FuelSupplySelector, "", FuelSupplySelector.value, "OnFuelSupplySelection");
        if (FuelSupplySelector.value == null || FuelSupplySelector.value == "") {
            RdAWBSSettings.ResetControls(RdAWBSSettings.FuelSupply.Name());
        }

    }

    var EngineSelector = RdAWBSSettings.Engine.selector();
    if (EngineSelector != null) {

        var currentValue = EngineSelector.value;
        mySelection = document.createElement("select");
        $.each(EngineSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;// RdAWBSSettings.Area.istances();
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, EngineSelector);
        if (ProjectSelector.value == null || ProjectSelector.value == "") {
            RdAWBSSettings.ResetControls(RdAWBSSettings.Engine.Name());
        }
    }



    var CDCSelector = RdAWBSSettings.CDC.selector();
    if (CDCSelector != null) {

        var currentValue = CDCSelector.value;
        mySelection = document.createElement("select");
        $.each(CDCSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;// RdAWBSSettings.Area.istances();
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, CDCSelector);
        if (ProjectSelector.value == null || ProjectSelector.value == "") {
            RdAWBSSettings.ResetControls(RdAWBSSettings.CDC.Name());
        }
    }

    var AreaSelector = RdAWBSSettings.Area.selector();
    if (AreaSelector != null) {

        var currentValue = AreaSelector.value;
        mySelection = document.createElement("select");
        $.each(AreaSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;// RdAWBSSettings.Area.istances();
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, AreaSelector);
        if (CDCSelector != null)
        {
            if (CDCSelector.value == null || CDCSelector.value == "") {
                RdAWBSSettings.ResetControls(RdAWBSSettings.Area.Name());
            }
        }
        else {
            if (ProjectSelector.value == null || ProjectSelector.value == "") {
                RdAWBSSettings.ResetControls(RdAWBSSettings.Area.Name(), "Project");
            }
        }
        
        
        
    }
    var SystemSelector = RdAWBSSettings.System.selector();
    if (SystemSelector != null) {

        var currentValue = SystemSelector.value;
        mySelection = document.createElement("select");
        listOfItems = null;
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, SystemSelector);
        if (AreaSelector.value == null || AreaSelector.value == "") {
            RdAWBSSettings.ResetControls(RdAWBSSettings.System.Name());
        }

    }
    var RequestTypeSelector = RdAWBSSettings.RequestType.selector();
    if (RequestTypeSelector != null) {

        var currentValue = RequestTypeSelector.value;
        mySelection = document.createElement("span");
        listOfItems = RdAWBSSettings.RequestType.istances();;
        RdAWBSSettings.PrepareComboBoxData2(listOfItems, mySelection, RequestTypeSelector,"Tooling Buy","R&D");
        if (RequestTypeSelector.value == null || RequestTypeSelector.value == "") {
            RdAWBSSettings.ResetControls(RdAWBSSettings.RequestType.Name());
        }

    }

    var CBSSelector = RdAWBSSettings.CBS.selector();
    if (CBSSelector != null) {

        var currentValue = CBSSelector.value;
        mySelection = document.createElement("select");
        listOfItems = null;
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, CBSSelector);
        
        if (SystemSelector != null) {
            if (SystemSelector.value == null || SystemSelector.value == "") {
                RdAWBSSettings.ResetControls(RdAWBSSettings.CBS.Name(), "System");
            }
        }
        else {
            if (AreaSelector.value == null || AreaSelector.value == "") {
                RdAWBSSettings.ResetControls(RdAWBSSettings.CBS.Name(),"Area");
            }
        }
        

    }

    var WBSSelector = RdAWBSSettings.WBS.selector();
    if (WBSSelector != null) {

        var currentValue = WBSSelector.value;
        mySelection = document.createElement("select");
        listOfItems = null;
        RdAWBSSettings.PrepareComboBoxData(listOfItems, mySelection, WBSSelector);
        if (CBSSelector.value == null || CBSSelector.value == "") {
            RdAWBSSettings.ResetControls(RdAWBSSettings.WBS.Name());
        }

    }
};


RdAWBSSettings.GetData = function (data, url) {
    if (url === undefined || url === "") url = "erda/dialog/getdialogitems";
    if (jsController) {
        var response = jsController.performRequest(url, 'POST', data)
        if (response === undefined || response === "") return "";

        return response.split("|");
    }
    return null;

};


RdAWBSSettings.GetBrand = function (searchParam) {

    
    var currentValue = RdAWBSSettings.Brand.selector().value;
    if (searchParam != null)
        currentValue = searchParam;
    var data = {
        reqtype: 'GetNestedList',
        basicList: 'BRAND_PROJECT',
        lvl1: null,
        lvl2: null,
        searchItem: currentValue,
        competence: null
    };
    return RdAWBSSettings.GetData(data);
};

RdAWBSSettings.GetProject = function (searchParam) {
    
    var currentValue = RdAWBSSettings.Project.selector().value;
    currentValue = encodeURIComponent(currentValue);
    if (searchParam != null)
        currentValue = searchParam;
    var myBaseList = "BRAND_PROJECT";
    var returnValue;
    var actualValueBrand = "";
    if (RdAWBSSettings.Brand.selector()) {
        actualValueBrand = RdAWBSSettings.Brand.selector().value;
        if (actualValueBrand == null || actualValueBrand == "") {
            return null;
        }
    }
    else
        if (RdAWBSSettings.Competence.Name().toLowerCase() == "transversal")
        {
            myBaseList = "COMPETENCE_PROJECT";
            actualValueBrand = RdAWBSSettings.Competence.Name().toUpperCase();
        }
            
    var data = {
        reqtype: 'GetNestedList',
        basicList: myBaseList,
        lvl1: actualValueBrand,
        lvl2: null,
        searchItem: currentValue,
        competence: RdAWBSSettings.Competence.Name()
    };
    return RdAWBSSettings.GetData(data);
};

RdAWBSSettings.GetActivityNPI = function (searchParam) {
    var currentValue = RdAWBSSettings.ActivityNPI.selector().value;
    currentValue = encodeURIComponent(currentValue);
    if (searchParam != null)
        currentValue = searchParam;

    var returnValue;
    var actualValueProject = RdAWBSSettings.Project.selector().value;
    if (actualValueProject == null || actualValueProject == "") {
        return null;
    }

    var data= {
        reqtype: 'GetNestedList',
        basicList: 'PROJECT_ACTIVITYNPI',
        lvl1: actualValueProject,
        lvl2: null,
        searchItem: currentValue,
        competence: null
    };
    return RdAWBSSettings.GetData(data);
};
RdAWBSSettings.GetContent = function (searchParam) {

    var currentValue = RdAWBSSettings.Content.selector().value;
    currentValue = encodeURIComponent(currentValue);
    if (searchParam != null)
        currentValue = searchParam;

    var returnValue;
    var actualValueProject = RdAWBSSettings.Project.selector().value;
    if (actualValueProject == null || actualValueProject == "") {
        return null;
    }
    var data = {
        reqtype: 'GetNestedList',
        basicList: 'PROJECT_CONTENTS',
        lvl1: actualValueProject,
        lvl2: null,
        searchItem: currentValue,
        competence: null
    };
    return RdAWBSSettings.GetData(data);
    
};
RdAWBSSettings.GetEngine = function (searchParam) {

    var currentValue = RdAWBSSettings.Engine.selector().value;
    currentValue = encodeURIComponent(currentValue);
    if (searchParam != null)
        currentValue = searchParam;

    var returnValue;
    var actualValueProject = RdAWBSSettings.Project.selector().value;
    if (actualValueProject == null || actualValueProject == "") {
        return null;
    }
    var data= {
        reqtype: 'GetNestedList',
        basicList: 'PROJECT_ENGINES',
        lvl1: actualValueProject,
        lvl2: null,
        searchItem: currentValue,
        competence: null
    };

    return RdAWBSSettings.GetData(data);

    
};

RdAWBSSettings.SetDefaultSystem = function (actualValueArea) {

    var returnValue;
    var data= {
        reqtype: 'GetNestedList',
        basicList: 'AREA_SUBSYSTEMS',
        lvl1: actualValueArea,
        lvl2: null,
        searchItem: null,
        competence: null
    };
    returnValue= RdAWBSSettings.GetData(data);
   
    if (returnValue != null && returnValue.length == 1) {
        var parentControl1 = $("span[class='" + RdAWBSSettings.System.Name() + " custom-combobox']");
        $(parentControl1).find('input').val(returnValue[0]);
        document.getElementById(RdAWBSSettings.System.selector().id).value = returnValue[0];
    }


};

RdAWBSSettings.GetFuelSupply = function (searchParam) {
    var returnValue = "GAS (Gasoline);DS (Diesel)".split(";");
    return returnValue;
};


RdAWBSSettings.GetWBSWizard = function (givenparam, searchParam) {
        
    var currentValue = searchParam;
    var data = {
        reqtype: 'GetWBSWizard',
        competence: RdAWBSSettings.Competence.Name(),
        project: RdAWBSSettings.Project.actualvalue(),
        cdc: RdAWBSSettings.CDC.actualvalue(),
        area: RdAWBSSettings.Area.actualvalue(),
        system: RdAWBSSettings.System.actualvalue(),
        cbs: RdAWBSSettings.CBS.actualvalue(),
        wbs: RdAWBSSettings.WBS.actualvalue(),
        param: givenparam,
        searchItem: currentValue
    };
    return RdAWBSSettings.GetData(data);
    
};

RdAWBSSettings.SetDefaultWBSWizard = function (givenparam, searchParam, searchParamName, controlName, controlId,currentSelector) {

    
    var currentValue = searchParam;
    var ssid = Math.random();
    var givenproject= RdAWBSSettings.Project.actualvalue();
    var givencdc= RdAWBSSettings.CDC.actualvalue();
    var givenarea= RdAWBSSettings.Area.actualvalue();
    var givensystem= RdAWBSSettings.System.actualvalue();
    var givencbs= RdAWBSSettings.CBS.actualvalue();
    var givenwbs = RdAWBSSettings.WBS.actualvalue();

    if (searchParamName == "PROJECT")
        givenproject = searchParam;
    else if (searchParamName == "CDC")
        givencdc = searchParam;
    else if (searchParamName == "AREA")
        givenarea = searchParam;
    else if (searchParamName == "SYSTEM")
        givensystem = searchParam;
    else if (searchParamName == "CBS")
        givencbs = searchParam;
    else if (searchParamName == "WBS")
        givenwbs = searchParam;

    var data= {
        reqtype: 'GetDefaultWBSWizard',
        competence: RdAWBSSettings.Competence.Name(),
        project: givenproject,
        cdc: givencdc,
        area: givenarea,
        system: givensystem,
        cbs: givencbs,
        wbs: givenwbs,
        param: givenparam,
        searchItem: currentValue
    };
    returnValue = RdAWBSSettings.GetData(data);
    if (returnValue != null && returnValue.length == 1) {
        var parentControl1 = $("span[class='" + controlName + " custom-combobox']");
        $(parentControl1).find('input').val(returnValue[0]);
        var currentValue = document.getElementById(controlId).value
        document.getElementById(controlId).value = returnValue[0];
        RdAWBSSettings.OnSelectItemChanged(currentSelector, returnValue[0], currentValue);
    }
    
};


RdAWBSSettings.GetRequestType = function (searchParam) {


    var currentValue = searchParam;
    var data = {
        reqtype: 'GetNestedList',
        basicList: 'REQUEST_TYPE',
        lvl1: null,
        lvl2: null,
        searchItem: currentValue,
        competence: null
    };
    return RdAWBSSettings.GetData(data);
    
};


RdAWBSSettings.GetControlName = function (controlId) {
    var objectType = "";
    if (RdAWBSSettings.Brand.selector() != null && controlId == RdAWBSSettings.Brand.selector().id)
        objectType = RdAWBSSettings.Brand.Name();
    else if (RdAWBSSettings.Project.selector() != null && controlId == RdAWBSSettings.Project.selector().id)
        objectType = RdAWBSSettings.Project.Name();
    else if (RdAWBSSettings.ActivityNPI.selector() != null && controlId == RdAWBSSettings.ActivityNPI.selector().id)
        objectType = RdAWBSSettings.ActivityNPI.Name();
    else if (RdAWBSSettings.Content.selector() != null && controlId == RdAWBSSettings.Content.selector().id)
        objectType = RdAWBSSettings.Content.Name();
    else if (RdAWBSSettings.FuelSupply.selector() != null && controlId == RdAWBSSettings.FuelSupply.selector().id)
        objectType = RdAWBSSettings.FuelSupply.Name();
    else if (RdAWBSSettings.Engine.selector() != null && controlId == RdAWBSSettings.Engine.selector().id)
        objectType = RdAWBSSettings.Engine.Name();
    else if (RdAWBSSettings.RequestType.selector() != null && controlId == RdAWBSSettings.RequestType.selector().id)
        objectType = RdAWBSSettings.RequestType.Name();
    else if (RdAWBSSettings.CDC.selector() != null && controlId == RdAWBSSettings.CDC.selector().id)
        objectType = RdAWBSSettings.CDC.Name();
    else if (RdAWBSSettings.Area.selector() != null && controlId == RdAWBSSettings.Area.selector().id)
        objectType = RdAWBSSettings.Area.Name();
    else if (RdAWBSSettings.System.selector() != null && controlId == RdAWBSSettings.System.selector().id)
        objectType = RdAWBSSettings.System.Name();
    else if (RdAWBSSettings.CBS.selector() != null && controlId == RdAWBSSettings.CBS.selector().id)
        objectType = RdAWBSSettings.CBS.Name();
    else if (RdAWBSSettings.WBS.selector() != null && controlId == RdAWBSSettings.WBS.selector().id)
        objectType = RdAWBSSettings.WBS.Name();

    return objectType;
}
RdAWBSSettings.GetControlSelector = function (controlId) {
    var objectType;
    if (RdAWBSSettings.Brand.selector() != null && controlId == RdAWBSSettings.Brand.selector().id)
        objectType = RdAWBSSettings.Brand.selector();
    else if (RdAWBSSettings.Project.selector() != null && controlId == RdAWBSSettings.Project.selector().id)
        objectType = RdAWBSSettings.Project.selector();
    else if (RdAWBSSettings.ActivityNPI.selector() != null && controlId == RdAWBSSettings.ActivityNPI.selector().id)
        objectType = RdAWBSSettings.ActivityNPI.selector();
    else if (RdAWBSSettings.Content.selector() != null && controlId == RdAWBSSettings.Content.selector().id)
        objectType = RdAWBSSettings.Content.selector();
    else if (RdAWBSSettings.FuelSupply.selector() != null && controlId == RdAWBSSettings.FuelSupply.selector().id)
        objectType = RdAWBSSettings.FuelSupply.selector();
    else if (RdAWBSSettings.Engine.selector() != null && controlId == RdAWBSSettings.Engine.selector().id)
        objectType = RdAWBSSettings.Engine.selector();
    else if (RdAWBSSettings.RequestType.selector() != null && controlId == RdAWBSSettings.RequestType.selector().id)
        objectType = RdAWBSSettings.RequestType.selector();
    else if (RdAWBSSettings.CDC.selector() != null && controlId == RdAWBSSettings.CDC.selector().id)
        objectType = RdAWBSSettings.CDC.selector();
    else if (RdAWBSSettings.Area.selector() != null && controlId == RdAWBSSettings.Area.selector().id)
        objectType = RdAWBSSettings.Area.selector();
    else if (RdAWBSSettings.System.selector() != null && controlId == RdAWBSSettings.System.selector().id)
        objectType = RdAWBSSettings.System.selector();
    else if (RdAWBSSettings.CBS.selector() != null && controlId == RdAWBSSettings.CBS.selector().id)
        objectType = RdAWBSSettings.CBS.selector
    else if (RdAWBSSettings.WBS.selector() != null && controlId == RdAWBSSettings.WBS.selector().id)
        objectType = RdAWBSSettings.WBS.selector();

    return objectType;
}


RdAWBSSettings.SetControlRed = function (controlId) {
    var objectType = RdAWBSSettings.GetControlName(controlId);

    if (objectType!=""){
            var className = "." + objectType + ".custom-combobox";
            var myInput = $(className).find(".custom-combobox-input");
            $(myInput).addClass('inputred');
            return true;
    }

    return false;
};

RdAWBSSettings.SetControlWhite = function (controlId) {
    var objectType = RdAWBSSettings.GetControlName(controlId);

    if (objectType != "") {
        var className = "." + objectType + ".custom-combobox";
        var myInput = $(className).find(".custom-combobox-input");
        $(myInput).removeClass('inputred');
        return true;
    }
    
    return false;
};


RdAWBSSettings.EnableControl = function (controlId) {

    var currentControl = RdAWBSSettings.GetControlName(controlId);
    var selector = RdAWBSSettings.GetControlSelector(controlId);

    if (RdAWBSSettings.Brand.selector() != null && controlId == RdAWBSSettings.Brand.selector().id) {
        
        return true;
    }
    if (RdAWBSSettings.FuelSupply.selector() != null && controlId == RdAWBSSettings.FuelSupply.selector().id) {

        return true;
    }
    if (RdAWBSSettings.RequestType.selector() != null && controlId == RdAWBSSettings.RequestType.selector().id) {
        var request = RdAWBSSettings.RequestType.Name();
        var controName = "#" + request + "combobox";
        if ($(controName) != null && $(controName).length == 1) {
            var controlDisable= $(controName).children('input[name="Tooling Buy"]');
            $(controlDisable).prop('disabled', true);
            return true;
        }
    }

    if (currentControl != null) {
        
        var controName = "#" + currentControl + "combobox";
        if ($(controName) != null && $(controName).length == 1) {
            RdAWBSSettings.InitComboBox(selector);
            $(controName).combobox();
            return true;
        }

    }
    
    return false;

};


RdAWBSSettings.DisableControl = function (controlId) {

};



RdAWBSSettings.PrepareComboBoxData = function (listOfItems, mySelection, currentSelector) {
    var id = currentSelector.id;
    var currentValue = currentSelector.value;
    var isDisabled = $(currentSelector).is(':disabled');
    var objectType = currentSelector.attributes["code"].value;
    

    var parent = $(currentSelector).parent().get(0);
    $(currentSelector).hide();
    $(mySelection).attr("id", objectType + "combobox");
    if (currentSelector.value != "")
    {
        option = "<option selected=\"selected\" value=\"" + currentSelector.value + "\">" + currentSelector.value + "</option>";
        $(mySelection).append(option);
    }
        

    $(parent).append(mySelection);

    RdAWBSSettings.InitComboBox(currentSelector);

    if (isDisabled) {
        $(mySelection).prop('disabled', 'disabled');
    }
    else {
        $("#" + mySelection.id).combobox();
    }
}

RdAWBSSettings.empty = function(obj) 
{ };

RdAWBSSettings.PrepareComboBoxData2 = function (listOfItems, mySelection, currentSelector ,disableItems, defaultvalue,callbackFunction) {
    var id = currentSelector.id;
    var currentValue = currentSelector.value;
    var isDisabled = $(currentSelector).is(':disabled');
    var objectType = currentSelector.attributes["code"].value;

    var parent = $(currentSelector).parent().get(0);
    $(currentSelector).hide();
    if (callbackFunction==null || callbackFunction == "")
        callbackFunction = "empty";

    for (i = 0; i < listOfItems.length; i++) {
        var name = listOfItems[i];
        if (name == defaultvalue)
        {
            document.getElementById(currentSelector.id).value = name;
            option = '<input onclick="javascript:RdAWBSSettings.' + callbackFunction + '(this);"  type="radio" checked="checked" name="' + name + '">' + name + '</input>';
        }
        else
        {
            if (disableItems.indexOf(name) != -1)
                option = '<input onclick="javascript:RdAWBSSettings.' + callbackFunction + '(this);"  type="radio" disabled=true name="' + name + '">' + name + '</input>';
            else
                option = '<input onclick="javascript:RdAWBSSettings.' + callbackFunction + '(this);"  type="radio" name="' + name + '">' + name + '</input>';
            
        }
        
        $(mySelection).append(option);
    }

    $(mySelection).attr("id", objectType + "combobox");

    $(parent).append(mySelection);

    RdAWBSSettings.InitComboBox(currentSelector);

    if (isDisabled) {
        $(mySelection).prop('disabled', 'disabled');
    }
}

RdAWBSSettings.ResetControlsInCascade = function (itemToReset,refItem,startOnCurrent) {
    if(startOnCurrent)RdAWBSSettings.ResetControls(itemToReset,refItem);
    if (RdAWBSSettings.Project.selector()) {
        if (itemToReset == RdAWBSSettings.Project.Name()) {
            RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.Content.Name(),refItem,true);
            if (RdAWBSSettings.CDC.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.CDC.Name(), refItem, true);
            else if (RdAWBSSettings.Area.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.Area.Name(), refItem, true);
        }

    }

    if (RdAWBSSettings.CDC.selector()) {
        if (itemToReset == RdAWBSSettings.CDC.Name()) {
            if (RdAWBSSettings.Area.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.Area.Name(), refItem, true);
            else if (RdAWBSSettings.System.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.System.Name(), refItem, true);
            else if (RdAWBSSettings.CBS.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.CBS.Name(), refItem, true);
        }
    }

    if (RdAWBSSettings.Area.selector()) {
        if (itemToReset == RdAWBSSettings.Area.Name()) {
            if (RdAWBSSettings.System.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.System.Name(), refItem, true);
            else if (RdAWBSSettings.CBS.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.CBS.Name(), refItem, true);

        }
    }
    if (RdAWBSSettings.System.selector()) {
        if (itemToReset == RdAWBSSettings.System.Name()) {
            if (RdAWBSSettings.CBS.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.CBS.Name(), refItem, true);
        }
    }
    if (RdAWBSSettings.CBS.selector()) {
        if (itemToReset == RdAWBSSettings.CBS.Name()) {
            if (RdAWBSSettings.WBS.selector())
                RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.WBS.Name(), refItem, true);
        }
    }
};

RdAWBSSettings.ResetControls = function (itemToReset, refItem) {
    if (RdAWBSSettings.Project.selector())
    {
        if (itemToReset == RdAWBSSettings.Project.Name()) {
            var parentControl1 = $("span[class='" + RdAWBSSettings.Project.Name() + " custom-combobox']");
            $(parentControl1).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl1).find('input').val('Select Brand...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.Project.selector().id).value = "";
        }
    }
    if (RdAWBSSettings.ActivityNPI.selector()) {
        if (itemToReset == RdAWBSSettings.ActivityNPI.Name()) {
            var parentControl1 = $("span[class='" + RdAWBSSettings.ActivityNPI.Name() + " custom-combobox']");
            $(parentControl1).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl1).find('input').val('Select Project...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.ActivityNPI.selector().id).value = "";
        }
    }
    if (RdAWBSSettings.Content.selector()) {
        if (itemToReset == RdAWBSSettings.Content.Name()) {
            var parentControl3 = $("span[class='" + RdAWBSSettings.Content.Name() + " custom-combobox']");
            $(parentControl3).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl3).find('input').val('Select Project...');
            $(parentControl3).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.Content.selector().id).value = "";
        }
    }
    

    if (RdAWBSSettings.Engine.selector()) {
        if (itemToReset == RdAWBSSettings.Engine.Name()) {
            var parentControl1 = $("span[class='" + RdAWBSSettings.Engine.Name() + " custom-combobox']");
            $(parentControl1).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl1).find('input').val('Select Project...');
            if (refItem != null && refItem != "")
                $(parentControl1).find('input').val('Select ' + refItem + '...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.Engine.selector().id).value = "";
        }
    }

    if (RdAWBSSettings.CDC.selector())
    {
        if (itemToReset == RdAWBSSettings.CDC.Name()) {
            var parentControl1 = $("span[class='" + RdAWBSSettings.CDC.Name() + " custom-combobox']");
            $(parentControl1).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl1).find('input').val('Select Project...');
            if (refItem != null && refItem != "")
                $(parentControl1).find('input').val('Select ' + refItem + '...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.CDC.selector().id).value = "";
        }
    }
    
    if (RdAWBSSettings.Area.selector())
    {
        if (itemToReset == RdAWBSSettings.Area.Name()) {
            var parentControl1 = $("span[class='" + RdAWBSSettings.Area.Name() + " custom-combobox']");
            $(parentControl1).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl1).find('input').val('Select CDC...');
            if (refItem != null && refItem != "")
                $(parentControl1).find('input').val('Select ' + refItem + '...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.Area.selector().id).value = "";
        }
    }
    if (RdAWBSSettings.System.selector()) {
        if (itemToReset == RdAWBSSettings.System.Name()) {
            var parentControl1 = $("span[class='" + RdAWBSSettings.System.Name() + " custom-combobox']");
            $(parentControl1).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl1).find('input').val('Select Area...');
            if (refItem != null && refItem != "")
                $(parentControl1).find('input').val('Select ' + refItem + '...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.System.selector().id).value = "";
        }
    }
    if (RdAWBSSettings.CBS.selector())
    {
        if (itemToReset == RdAWBSSettings.CBS.Name()) {
            var parentControl1 = $("span[class='" + RdAWBSSettings.CBS.Name() + " custom-combobox']");
            $(parentControl1).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl1).find('input').val('Select Area...');
            if (refItem != null && refItem != "")
                $(parentControl1).find('input').val('Select ' + refItem + '...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.CBS.selector().id).value = "";
        }
    }
    if (RdAWBSSettings.WBS.selector())
    {
        if (itemToReset == RdAWBSSettings.WBS.Name()) {
            var parentControl1 = $("span[class='" + RdAWBSSettings.WBS.Name() + " custom-combobox']");
            $(parentControl1).find('input, textarea, button, select, a').attr('disabled', 'disabled');
            $(parentControl1).find('input').val('Select CBS...');
            if (refItem != null && refItem != "")
                $(parentControl1).find('input').val('Select ' + refItem + '...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(RdAWBSSettings.WBS.selector().id).value = "";
        }
    }
    

}
RdAWBSSettings.SetReadyControl = function (itemToReady,itemIdToReady) {
    var parentControl1 = $("span[class='" + itemToReady + " custom-combobox']");
    $(parentControl1).find('input, textarea, button, select, a').attr('disabled', false);
    $(parentControl1).find('input').val('');
    $(parentControl1).find('input').css({ 'background-color': '#FFF' });
    document.getElementById(itemIdToReady).value = "";

}

RdAWBSSettings.setSupplySelction = function (givenItem) {
    var currentSelector = RdAWBSSettings.FuelSupply.selector();
    var currentValue = document.getElementById(currentSelector.id).value;
    document.getElementById(currentSelector.id).value = givenItem.name;
    var parentControl1 = $(givenItem).parent();
    var inputs = $(parentControl1).find('input');
    for (var idx = 0; idx < inputs.length; idx++) {
        if (inputs[idx].name != givenItem.name)
            $(inputs[idx]).prop('checked', false);
    }
    RdAWBSSettings.OnSelectItemChanged(currentSelector, givenItem.name, currentValue);
}


RdAWBSSettings.ActiveWBSWizardSteps = function (currentSelector) {
    

}

RdAWBSSettings.OnSelectItemChanged = function (currentSelector, actualValue, currentValue) {
    
    var resetControl = false;
    if (actualValue == "" || actualValue != currentValue)
        resetControl = true;
    var contentid = -1;
    var areaid = -1;
    var cdcid = -1;
    var systemid = -1;
    if (RdAWBSSettings.Content.selector())
        contentid = RdAWBSSettings.Content.selector().id;
    if (RdAWBSSettings.Area.selector())
        areaid = RdAWBSSettings.Area.selector().id;
    if (RdAWBSSettings.CDC.selector())
        cdcid = RdAWBSSettings.CDC.selector().id;
    if (RdAWBSSettings.System.selector())
        systemid = RdAWBSSettings.System.selector().id;

    var RdAWBSSettingsBrandSelectorId = "-1";
    if (RdAWBSSettings.Brand.selector())
        RdAWBSSettingsBrandSelectorId = RdAWBSSettings.Brand.selector().id;
    switch (currentSelector.id) {
        case RdAWBSSettingsBrandSelectorId:
            if (actualValue != currentValue) {
                RdAWBSSettings.SetReadyControl(RdAWBSSettings.Project.Name(), RdAWBSSettings.Project.selector().id);
            }
            break;
        case RdAWBSSettings.Project.selector().id:
            if (resetControl) RdAWBSSettings.ResetControlsInCascade(RdAWBSSettings.Project.Name());
            if (actualValue != currentValue) {
                RdAWBSSettings.Project.updateValue(actualValue);
                
                if (RdAWBSSettings.Content.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.Content.Name(), RdAWBSSettings.Content.selector().id);
                    RdAWBSSettings.SetDefaultContent(actualValue);
                }
                if (RdAWBSSettings.ActivityNPI.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.ActivityNPI.Name(), RdAWBSSettings.ActivityNPI.selector().id);
                    RdAWBSSettings.SetDefaultActivityNPI(actualValue);
                }
                if (RdAWBSSettings.Engine.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.Engine.Name(), RdAWBSSettings.Engine.selector().id);
                    RdAWBSSettings.SetDefaultEngine(actualValue);
                }

                if (RdAWBSSettings.CDC.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.CDC.Name(), RdAWBSSettings.CDC.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("CDC", actualValue, "PROJECT", RdAWBSSettings.CDC.Name(), RdAWBSSettings.CDC.selector().id, RdAWBSSettings.CDC.selector());
                }
                else {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.Area.Name(), RdAWBSSettings.Area.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("AREA", actualValue, "PROJECT", RdAWBSSettings.Area.Name(), RdAWBSSettings.Area.selector().id, RdAWBSSettings.Area.selector());
                }
            }
            break;
       // case RdAWBSSettings.Content.selector().id:
        //    break;
        case cdcid:
            if (actualValue != currentValue) {
                RdAWBSSettings.CDC.updateValue(actualValue);

                if (RdAWBSSettings.Area.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.Area.Name(), RdAWBSSettings.Area.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("AREA", actualValue, "CDC", RdAWBSSettings.Area.Name(), RdAWBSSettings.Area.selector().id, RdAWBSSettings.Area.selector());
                }
                else if (RdAWBSSettings.System.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.System.Name(), RdAWBSSettings.System.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("SYSTEM", actualValue, "CDC", RdAWBSSettings.System.Name(), RdAWBSSettings.System.selector().id, RdAWBSSettings.System.selector());
                }
                else if (RdAWBSSettings.CBS.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.CBS.Name(), RdAWBSSettings.CBS.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("CBS", actualValue, "CDC", RdAWBSSettings.CBS.Name(), RdAWBSSettings.CBS.selector().id, RdAWBSSettings.CBS.selector());
                }
            }
           
            break;
        case areaid:
            
            if (actualValue != currentValue) {
                RdAWBSSettings.Area.updateValue(actualValue);
                if (RdAWBSSettings.System.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.System.Name(), RdAWBSSettings.System.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("SYSTEM", actualValue, "AREA", RdAWBSSettings.System.Name(), RdAWBSSettings.System.selector().id, RdAWBSSettings.System.selector());
                }
                else {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.CBS.Name(), RdAWBSSettings.CBS.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("CBS", actualValue, "AREA", RdAWBSSettings.CBS.Name(), RdAWBSSettings.CBS.selector().id, RdAWBSSettings.CBS.selector());
                }

            }
            break;
        case systemid:
            if (actualValue != currentValue) {
                RdAWBSSettings.System.updateValue(actualValue);
                if (RdAWBSSettings.CBS.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.CBS.Name(), RdAWBSSettings.CBS.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("CBS", actualValue, "SYSTEM", RdAWBSSettings.CBS.Name(), RdAWBSSettings.CBS.selector().id, RdAWBSSettings.CBS.selector());
                }
            }
            break;
        case RdAWBSSettings.CBS.selector().id:
            if (actualValue != currentValue) {
                RdAWBSSettings.ResetControls(RdAWBSSettings.WBS.Name());
                RdAWBSSettings.CBS.updateValue(actualValue);
                if (RdAWBSSettings.WBS.selector()) {
                    RdAWBSSettings.SetReadyControl(RdAWBSSettings.WBS.Name(), RdAWBSSettings.WBS.selector().id);
                    RdAWBSSettings.SetDefaultWBSWizard("WBS", actualValue, "CBS", RdAWBSSettings.WBS.Name(), RdAWBSSettings.WBS.selector().id, RdAWBSSettings.WBS.selector());
                }
            }
            break;
    }
   

   
};
RdAWBSSettings.InitComboBox = function (currentSelector) {

    if (typeof currentSelector === "function")
        currentSelector = currentSelector();
    $.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<span>")
                .addClass(this.element[0].id.replace("combobox", "") + " custom-combobox")
                .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
                value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
                .appendTo(this.wrapper)
                .val(value)
                .attr("title", "")
                .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy(this, "_source2"),
                    open: function (event, ui) {
                        var autocomplete = $(".ui-autocomplete");
                        var oldTop = $(this).position().top;
                        var newTop = $(window).height();
                        if ((newTop - oldTop) < 350) {
                            newTop = newTop - oldTop - 20;
                            newTop = newTop + 'px';
                            autocomplete.css("max-height", newTop);
                        }
                    }
                })
                .tooltip({
                    classes: {
                        "ui-tooltip": "ui-state-highlight"
                    }
                });

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                    
                    RdAWBSSettings.OnSelectItemChanged(currentSelector, ui.item.value, document.getElementById(currentSelector.id).value);

                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function () {
            var input = this.input,
                wasOpen = false;

            $("<a>")
                .attr("tabIndex", -1)
                .attr("title", "Show All Items")
                .tooltip()
                .appendTo(this.wrapper)
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass("ui-corner-all")
                .addClass("custom-combobox-toggle ui-corner-right")
                .on("mousedown", function () {
                    wasOpen = input.autocomplete("widget").is(":visible");
                })
                .on("click", function () {
                    input.trigger("focus");

                    // Close if already visible
                    if (wasOpen) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    input.autocomplete("search", "");
                });
        },

        _source: function (request, response) {

            if (request.term.trim == "") {
                return null;
            }
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))

                    return {
                        label: text,
                        value: text,
                        option: this
                    };

            }));
        },

        _source2: function (request, response) {
            var ssid = Math.random();
           // var obj = this;
            var currentName = this.element[0].id.replace("combobox", "");

            //var myurl = "../Admin/AjaxRequest.aspx?reqtype=GetList&list=" + this.element[0].id.replace("combobox","") + "&searchItem=" + request.term + "&ssid=" + ssid;
            var myData = null;
            if (currentName == RdAWBSSettings.Project.Name())
                myData = RdAWBSSettings.Project.istances(request.term);
            else if (currentName == RdAWBSSettings.ActivityNPI.Name())
                myData = RdAWBSSettings.ActivityNPI.istances(request.term);
            else if (currentName == RdAWBSSettings.Content.Name())
                myData = RdAWBSSettings.Content.istances(request.term);
            else if (currentName == RdAWBSSettings.CDC.Name())
                myData = RdAWBSSettings.CDC.istances(request.term);
            else if (currentName == RdAWBSSettings.Area.Name())
                myData = RdAWBSSettings.Area.istances(request.term);
            else if (currentName == RdAWBSSettings.System.Name())
                myData = RdAWBSSettings.System.istances(request.term);
            else if (currentName == RdAWBSSettings.CBS.Name())
                myData = RdAWBSSettings.CBS.istances(request.term);
            else if (currentName == RdAWBSSettings.WBS.Name())
                myData = RdAWBSSettings.WBS.istances(request.term);
            else if (currentName == RdAWBSSettings.Engine.Name())
                myData = RdAWBSSettings.Engine.istances(request.term);

            if (myData != null) {
                response($.map(myData, function (item) {
                    if (item != "") {
                        if (typeof item === "string") {
                            return {
                                label: item.trim(),
                                value: item.trim(),
                                option: this
                            };
                        }
                        return $.extend({
                            label: item.label.trim(),
                            value: item.value.trim(),
                            option: this
                        }, item);
                    }
                }));
            }

        },
        _removeIfInvalid: function (event, ui) {

            document.getElementById(currentSelector.id).value = "";
            // Selected an item, nothing to do
            if (ui.item) {
                //RdAWBSSettings.OnSelectItemChanged(currentSelector, ui.item.value, document.getElementById(currentSelector.id).value);
                document.getElementById(currentSelector.id).value = ui.item.value;
                return;
            }

            // Search for a match (case-insensitive)
            var matchText = "";
            var matchValue = "";
            var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
            valueLowerCase = valueLowerCase.trim();
            if (valueLowerCase != "") {
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        matchText = $(this).text();
                        matchValue = $(this).val();

                        return false;
                    }
                });
            }

            // Found a match, nothing to do
            if (valid) {
                RdAWBSSettings.OnSelectItemChanged(currentSelector, matchValue, document.getElementById(currentSelector.id).value);
                document.getElementById(currentSelector.id).value = matchValue;
                this.input.val(matchText);

                return;
            }

            // Remove invalid value
            this.input
                .val("")
                .attr("title", value + " didn't match any item")
                .tooltip("open");
            this.element.val("");
            document.getElementById(currentSelector.id).value = "";
            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input.autocomplete("instance").term = "";
            RdAWBSSettings.OnSelectItemChanged(currentSelector, "", "");
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });

};
RdAWBSSettings.SetDefaultActivityNPI = function (actualValueProject) {

    var returnValue;
    var data =  {
        reqtype: 'GetNestedList',
        basicList: 'PROJECT_ACTIVITYNPI',
        lvl1: actualValueProject,
        lvl2: null,
        searchItem: null,
        competence: null
    };
    returnValue = RdAWBSSettings.GetData(data);
    if (returnValue != null && returnValue.length == 1) {
        var parentControl1 = $("span[class='" + RdAWBSSettings.ActivityNPI.Name() + " custom-combobox']");
        $(parentControl1).find('input').val(returnValue[0]);
        document.getElementById(RdAWBSSettings.ActivityNPI.selector().id).value = returnValue[0];
    }
};


RdAWBSSettings.SetDefaultContent = function (actualValueProject) {

    var returnValue;
    var data = {
        reqtype: 'GetNestedList',
        basicList: 'PROJECT_CONTENTS',
        lvl1: actualValueProject,
        lvl2: null,
        searchItem: null,
        competence: null
    };
    returnValue = RdAWBSSettings.GetData(data);
    if (returnValue != null && returnValue.length == 1) {
        var parentControl1 = $("span[class='" + RdAWBSSettings.Content.Name() + " custom-combobox']");
        $(parentControl1).find('input').val(returnValue[0]);
        document.getElementById(RdAWBSSettings.Content.selector().id).value = returnValue[0];
    }
};

RdAWBSSettings.SetDefaultEngine = function (actualValueProject) {

    var returnValue;
    var data = {
        reqtype: 'GetNestedList',
        basicList: 'PROJECT_ENGINES',
        lvl1: actualValueProject,
        lvl2: null,
        searchItem: null,
        competence: null
    };
    returnValue = RdAWBSSettings.GetData(data);
    if (returnValue != null && returnValue.length == 1) {
        var parentControl1 = $("span[class='" + RdAWBSSettings.Engine.Name() + " custom-combobox']");
        $(parentControl1).find('input').val(returnValue[0]);
        document.getElementById(RdAWBSSettings.Engine.selector().id).value = returnValue[0];
    }
};

RdAWBSSettings.OnBrandSelction = function (givenItem) {
    var currentSelector = RdAWBSSettings.Brand.selector();
    var currentValue = document.getElementById(currentSelector.id).value;
    document.getElementById(currentSelector.id).value = givenItem.name;
    var parentControl1 = $(givenItem).parent();
    var inputs = $(parentControl1).find('input');
    for (var idx = 0; idx < inputs.length; idx++) {
        if (inputs[idx].name != givenItem.name)
            $(inputs[idx]).prop('checked', false);
    }
    RdAWBSSettings.OnSelectItemChanged(currentSelector, givenItem.name, currentValue);
};

RdAWBSSettings.OnFuelSupplySelection = function (givenItem) {
    var currentSelector = RdAWBSSettings.FuelSupply.selector();
    var currentValue = document.getElementById(currentSelector.id).value;
    document.getElementById(currentSelector.id).value = givenItem.name;
    var parentControl1 = $(givenItem).parent();
    var inputs = $(parentControl1).find('input');
    for (var idx = 0; idx < inputs.length; idx++) {
        if (inputs[idx].name != givenItem.name)
            $(inputs[idx]).prop('checked', false);
    }
    
};