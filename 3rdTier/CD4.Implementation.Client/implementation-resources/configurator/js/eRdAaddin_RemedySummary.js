﻿
var RdARemedySummary = {
    RemedySupplier: {
        selector: function () {
            var input = $("select[code='SupplierRemedy']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },
    RemedyDays: {
        selector: function () {
            var input = $("input[code='Hours']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },
    RemedyActivityStart: {
        selector: function () {
            var input = $("input[code='ActivityStart']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },

    RemedyActivityEnd: {
        selector: function () {
            var input = $("input[code='ActivityEnd']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },
    RemedyProfessionalFigure: {
        selector: function () {
            var input = $("select[code='remedy_rdaw1_professionalrole']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },
    RemedyTestDescription: {
        selector: function () {
            var input = $("select[code='remedy_rdaw1_professionalrole']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },
    RemedySite: {
        selector: function () {
            var input = $("select[code='Site']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },

    SummarySupplier: {
        Name: function () {
            return "SummarySupplier"
        },
        selector: function () {
            var input = $("input[code='SummarySupplier']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        
        istances: function (searchParam) {
            return null;
        }
    },
    SummaryDays: {
        Name: function () {
            return "SummaryDays"
        },
        selector: function () {
            var input = $("input[code='SummaryDays']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },

        istances: function (searchParam) {
            return null;
        }
    },
    SummaryProfessionalFigure: {
        Name: function () {
            return "SummaryProfessionalFigure"
        },
        selector: function () {
            var input = $("input[code='SummaryProfessionalFigure']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },

        istances: function (searchParam) {
            return null;
        }
    },
    SummaryTestDescription: {
        Name: function () {
            return "SummaryTestDescription"
        },
        selector: function () {
            var input = $("input[code='SummaryTestDescription']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },

        istances: function (searchParam) {
            return null;
        }
    },
    SummarySite: {
        Name: function () {
            return "SummarySite"
        },
        selector: function () {
            var input = $("input[code='SummarySite']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },

        istances: function (searchParam) {
            return null;
        }
    },
    SummaryAmount: {
        Name: function () {
            return "SummaryAmount"
        },
        selector: function () {
            var input = $("input[code='SummaryAmount']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },

        istances: function (searchParam) {
            return null;
        }
    },
    SummaryDailyCost: {
        Name: function () {
            return "SummaryDailyCost"
        },
        selector: function () {
            var input = $("input[code='SummaryDailyCost']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },

        istances: function (searchParam) {
            return null;
        }
    },
    SummaryItemCode: {
        Name: function () {
            return "SummaryItemCode"
        },
        selector: function () {
            var input = $("input[code='SummaryItemCode']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },

        istances: function (searchParam) {
            return null;
        }
    }


};

RdARemedySummary.InitElements = function () {
    
    debugger;
    var supplier = RdARemedySummary.SummarySupplier.selector();
    var days = RdARemedySummary.SummaryDays.selector();
    var professionalFigure = RdARemedySummary.SummaryProfessionalFigure.selector();
    var testDescription = RdARemedySummary.SummaryTestDescription.selector();
    var site = RdARemedySummary.SummarySite.selector();
    var amount = RdARemedySummary.SummaryAmount.selector();
    var dailycost = RdARemedySummary.SummaryDailyCost.selector();
    var itemCode = RdARemedySummary.SummaryItemCode.selector();
    var activityStart = RdARemedySummary.RemedyActivityStart.selector();
    var activityEnd = RdARemedySummary.RemedyActivityEnd.selector();
    var isPWT = false;
    var currentCBS = RdAWBSSettings.CBS.actualvalue();
    var currentWBS = RdAWBSSettings.WBS.actualvalue();
    var isTest = false;
    if (currentCBS != null && currentWBS != null)
        if (currentCBS.toLowerCase() == "test/tool proto/outsource&facilities" && currentCBS.toLowerCase().indexOf("test") != -1)
            isTest = true;
    if (isTest) {
        if (supplier != null && testDescription != null && days != null && amount != null && dailycost != null && activityEnd != null) {
        //if (supplier != null && testDescription != null && amount != null && activityEnd != null) {
            supplier.style.width = "100%";
            testDescription.style.width = "100%";

            if (supplier)supplier.disabled = true;
            if (testDescription) testDescription.disabled = true;
            if (days) days.disabled = true;
            if (dailycost) dailycost.disabled = true;
            if (amount)amount.disabled = true;
            if (itemCode)itemCode.disabled = true;


            var definedSupplier = RdARemedySummary.RemedySupplier.selector();
            var definedDays = RdARemedySummary.RemedyDays.selector();
            var definedTestDescr = RdARemedySummary.RemedyTestDescription.selector();

            if (definedSupplier != null && definedDays != null && definedTestDescr != null) {
                supplier.value = definedSupplier.value;
                testDescription.value = definedTestDescr.value;

                var competence = "VEHICLE";
                var context = "STANDARD";
                var supplier = "";
                var myStart = activityStart.value;
                var myEnd = activityEnd.value;
               
                var ssid = Math.random();
                var supplierCode = definedSupplier.value;
                supplierCode = supplierCode.replace("[", "").replace("]", "").split("cod.forn.")[1].toString().trim();

                days.value = definedDays.value;

                var data = {
                    reqtype: 'GetPrice',
                    competence: encodeURIComponent(competence),
                    context: encodeURIComponent(context),
                    suppliercode: encodeURIComponent(supplierCode),
                    professionalrole: encodeURIComponent(testDescription.value),
                    startdate: myStart,
                    enddate: myEnd,
                    days: days.value,
                    cbs: currentCBS,
                    wbs: currentWBS,
                };

                var returnValue = RdAWBSSettings.GetData(data);
                amount.value = returnValue[0];
                dailycost.value = returnValue[1];
                itemCode.value = returnValue[2];
            }
            try {
                var SummaryControlButton = "";
                if (RdARemedySummary.SummaryAmount.selector() != null)
                    SummaryControlButton = RdARemedySummary.SummaryAmount.selector().parent;
                var button = document.getElementById("editCA_" + SummaryControlButton);
                if (button != null && button.parentNode != null) {
                    $(button.parentNode).hide();//prop('disabled', true);
                }
            }
            catch (err) { };
        }


    }
    else {
        if (supplier != null && days != null && professionalFigure != null && site != null && amount != null && dailycost != null && activityEnd != null) {
            supplier.style.width = "100%";
            professionalFigure.style.width = "100%";

            supplier.disabled = true;
            days.disabled = true;
            professionalFigure.disabled = true;
            site.disabled = true;
            amount.disabled = true;
            dailycost.disabled = true;
            itemCode.disabled = true;

            var definedSupplier = RdARemedySummary.RemedySupplier.selector();
            var definedDays = RdARemedySummary.RemedyDays.selector();
            var definedProfRoles = RdARemedySummary.RemedyProfessionalFigure.selector();
            var definedSite = RdARemedySummary.RemedySite.selector();

            if (definedSupplier != null && definedDays != null && definedProfRoles != null && definedSite != null) {
                supplier.value = definedSupplier.value;
                supplier.disabled = true;
                days.value = definedDays.value;
                professionalFigure.value = definedProfRoles.value;
                site.value = definedSite.value;

                if (supplier)
                    if (supplier.parentNode.innerHTML.indexOf("PWT Professional Figure") != -1)
                        isPWT = true;

                var competence = "VEHICLE";
                var context = "STANDARD";
                var supplier = "";
                var myStart = activityStart.value;
                var myEnd = activityEnd.value;
                if (isPWT)
                    competence = "POWERTRAIN";
                var ssid = Math.random();
                var supplierCode = definedSupplier.value;
                supplierCode = supplierCode.replace("[", "").replace("]", "").split("cod.forn.")[1].toString().trim();


                var data = {
                    reqtype: 'GetPrice',
                    competence: encodeURIComponent(competence),
                    context: encodeURIComponent(context),
                    suppliercode: encodeURIComponent(supplierCode),
                    professionalrole: encodeURIComponent(professionalFigure.value),
                    site: site.value,
                    startdate: myStart,
                    enddate: myEnd,
                    days: days.value
                };

                var returnValue = RdAWBSSettings.GetData(data);
                amount.value = returnValue[0];
                dailycost.value = returnValue[1];
                itemCode.value = returnValue[2];
            }
            try {
                var SummaryControlButton = "";
                if (RdARemedySummary.SummaryAmount.selector() != null)
                    SummaryControlButton = RdARemedySummary.SummaryAmount.selector().parent;
                var button = document.getElementById("editCA_" + SummaryControlButton);
                if (button != null && button.parentNode != null) {
                    $(button.parentNode).hide();//prop('disabled', true);
                }
            }
            catch (err) { };
        }


    }
    
};





GetSiteXmlHttpObject = function () {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari8.  
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE512.  
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
};
