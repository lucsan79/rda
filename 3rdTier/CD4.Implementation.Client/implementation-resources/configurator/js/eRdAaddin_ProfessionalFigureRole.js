﻿var standardCompName = "rdaw1_professionalrole";
var remedyCompName = "remedy_rdaw1_professionalrole";
var componentName = "";
var isTest = false;

var ProfessionalObjectIdentify = {
    Roles: {
        Name: function () {
            componentName = standardCompName;
            var input=$("input[code='" + remedyCompName + "']");
            if (input!=null && input.length > 0)
                componentName = remedyCompName;
            
            return componentName;
        },
        selector: function () {
            var input = null;
            input = $("input[code='" + this.Name() + "']");
            if (input.length == 1)
                return input[0];
            else
            {
                input = $("input[code='" + remedyCompName + "']");
                if (input.length == 1)
                {
                    componentName = remedyCompName;
                    return input[0];
                }
                    
            }
            return null;
        },
        selectedSupplier: function () {
            var code = "";
            var input = $("input[code='SupplierRemedy']");
            if (input.length == 1)
                code = input[0].value;

            return code;
        },
        istances: function (searchParam) {
            return ProfessionalObjectIdentify.GetList(this.Name(), searchParam);
        }
    },
    Levels: {
        Name: function () {
            return "rdaw1_professionallevel";
        },
        selector: function () {
            var input = $("input[code='rdaw1_professionallevel']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        istances: function (searchParam) {
            return ProfessionalObjectIdentify.GetList('rdaw1_professionallevel', searchParam);
        }
    },
    Site: {
        Name: function () {
            return "Site";
        },
        selector: function () {
            var input = $("input[code='Site']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        istances: function (searchParam) {
            return ProfessionalObjectIdentify.GetListOfSite(searchParam);
        }
    }
   
};

ProfessionalObjectIdentify.InitElements = function () {
    //debugger;
    var currentCBS = RdAWBSSettings.CBS.actualvalue();
    var currentWBS = RdAWBSSettings.WBS.actualvalue();

    if (currentCBS != null && currentWBS != null)
        if (currentCBS.toLowerCase() == "test/tool proto/outsource&facilities" && currentCBS.toLowerCase().indexOf("test") != -1)
            isTest = true;

    var RolesSelector = ProfessionalObjectIdentify.Roles.selector();
    var listOfItems = null;
    var mySelection = null;
    if (RolesSelector != null) {

        var currentValue = RolesSelector.value;
        mySelection = document.createElement("select");
        $.each(RolesSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;// RdAProjectSettings.Brand.istances();
        ProfessionalObjectIdentify.PrepareComboBoxData(listOfItems, mySelection, RolesSelector);
    }
    var LevelsSelector = ProfessionalObjectIdentify.Levels.selector();
    if (LevelsSelector != null) {

        var currentValue = LevelsSelector.value;
        mySelection = document.createElement("select");
        $.each(RolesSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;
        ProfessionalObjectIdentify.PrepareComboBoxData(listOfItems, mySelection, LevelsSelector);

    }
    var SiteSelector = ProfessionalObjectIdentify.Site.selector();
    if (SiteSelector != null) {

        var currentValue = SiteSelector.value;
        mySelection = document.createElement("select");
        $.each(SiteSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });
        listOfItems = null;
        ProfessionalObjectIdentify.PrepareComboBoxData(listOfItems, mySelection, SiteSelector);

        if (SiteSelector.value == null || SiteSelector.value == "") {
            ProfessionalObjectIdentify.ResetControls(ProfessionalObjectIdentify.Site.Name());
        }

    }
    //

};

ProfessionalObjectIdentify.ResetControls = function (itemToReset, refItem) {
  
    if (ProfessionalObjectIdentify.Site.selector()) {
        if (itemToReset == ProfessionalObjectIdentify.Site.Name()) {
            var parentControl1 = $("span[class='" + ProfessionalObjectIdentify.Site.Name() + " custom-combobox']");
            $(parentControl1).find('input').val('Select Professional Figure...');
            if (isTest)
                $(parentControl1).find('input').val('Select Test Description...');
            $(parentControl1).find('input').css({ 'background-color': '#FFFFEEE' });
            document.getElementById(ProfessionalObjectIdentify.Site.selector().id).value = "";
        }
    }
};

ProfessionalObjectIdentify.GetList = function (objectType, searchItem) {
    var currentSelector = null;

    if (objectType == ProfessionalObjectIdentify.Roles.Name())
        currentSelector = ProfessionalObjectIdentify.Roles.selector();
    else if (objectType == ProfessionalObjectIdentify.Levels.Name())
        currentSelector = ProfessionalObjectIdentify.Levels.selector();

    var currentValue = currentSelector.value;
    currentValue = encodeURIComponent(currentValue);
    if (searchItem != "")
        currentValue = searchItem;
    
    var http = GetProfessionalXmlHttpObject();
    var ssid = Math.random();
    if (objectType == ProfessionalObjectIdentify.Roles.Name())
        if (currentSelector.parentNode.innerHTML.indexOf("PWT Professional Figure") != -1)
            objectType = objectType + "_pwt";
    var currentCBS = RdAWBSSettings.CBS.actualvalue();
    var currentWBS = RdAWBSSettings.WBS.actualvalue();

    var returnValue;
    var data = {
        reqtype: 'GetList',
        basicList: objectType,
        lvl1: null,
        lvl2: null,
        searchItem: currentValue,
        competence: null,
        cbs: currentCBS,
        wbs: currentWBS,
    };

    if (objectType.replace("_pwt", "") == ProfessionalObjectIdentify.Roles.Name()) {

        var competence = "VEHICLE";
        var context = "STANDARD";
        var supplier = "";
        if (objectType.indexOf("_pwt") != -1)
            competence = "POWERTRAIN";

        if (objectType.indexOf("remedy_") != -1) {
            context = "REMEDY";
            supplier = ProfessionalObjectIdentify.Roles.selectedSupplier();
            if (supplier != null && supplier!="")
                supplier = supplier.replace("[", "").replace("]", "").split("cod.forn.")[1].toString().trim();
        }
        data.reqtype = 'GetProfessionalRoles';
        data.context = context;
        data.competence = competence;
        data.suppliercode = supplier;
        data.searchItem = currentValue;
    }

    
    returnValue = RdAWBSSettings.GetData(data);
    return returnValue;

};

ProfessionalObjectIdentify.SetControlRed = function (controlId) {
    var objectType = "";
    if (ProfessionalObjectIdentify.Roles.selector() != null && controlId == ProfessionalObjectIdentify.Roles.selector().id)
        objectType = ProfessionalObjectIdentify.Roles.Name();
    else if (ProfessionalObjectIdentify.Levels.selector() != null && controlId == ProfessionalObjectIdentify.Levels.selector().id)
        objectType = ProfessionalObjectIdentify.Levels.Name();
    else if (ProfessionalObjectIdentify.Site.selector() != null && controlId == ProfessionalObjectIdentify.Site.selector().id)
        objectType = ProfessionalObjectIdentify.Site.Name();
    if (objectType != "") {
        var className = "." + objectType + ".custom-combobox";
        var myInput = $(className).find(".custom-combobox-input");
        $(myInput).addClass('inputred');
        return true;
    }

    return false;
};

ProfessionalObjectIdentify.SetControlWhite = function (controlId) {

    var objectType = "";
    if (ProfessionalObjectIdentify.Roles.selector() != null && controlId == ProfessionalObjectIdentify.Roles.selector().id)
        objectType = ProfessionalObjectIdentify.Roles.Name();
    else if (ProfessionalObjectIdentify.Levels.selector() != null && controlId == ProfessionalObjectIdentify.Levels.selector().id)
        objectType = ProfessionalObjectIdentify.Levels.Name();
    else if (ProfessionalObjectIdentify.Site.selector() != null && controlId == ProfessionalObjectIdentify.Site.selector().id)
        objectType = ProfessionalObjectIdentify.Site.Name();

    if (objectType != "") {
        var className = "." + objectType + ".custom-combobox";
        var myInput = $(className).find(".custom-combobox-input");
        $(myInput).removeClass('inputred');
        return true;
    }

    return false;


  
};


ProfessionalObjectIdentify.EnableControl = function (controlId) {
    
    var selector = null;
    var currentControl;
    if (ProfessionalObjectIdentify.Roles.selector() != null && controlId == ProfessionalObjectIdentify.Roles.selector().id) {
        currentControl = ProfessionalObjectIdentify.Roles.Name();
        selector = ProfessionalObjectIdentify.Roles.selector();
    }
    else if (ProfessionalObjectIdentify.Levels.selector() != null && controlId == ProfessionalObjectIdentify.Levels.selector().id) {
        currentControl = ProfessionalObjectIdentify.Levels.Name();
        selector = ProfessionalObjectIdentify.Levels.selector();
    }
    else if (ProfessionalObjectIdentify.Site.selector() != null && controlId == ProfessionalObjectIdentify.Site.selector().id) {
        currentControl = ProfessionalObjectIdentify.Site.Name();
        selector = ProfessionalObjectIdentify.Site.selector();
    }
    if (currentControl != null) {
        var controName = "#" + currentControl + "combobox";
        if ($(controName) != null && $(controName).length == 1) {
            ProfessionalObjectIdentify.InitComboBox(selector);
            $(controName).combobox();
            return true;
        }

    }

    return false;
        
};


ProfessionalObjectIdentify.DisableControl = function (controlId) {
    
};

GetProfessionalItemsXmlHttpObject = function () {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari8.  
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE512.  
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
};

ProfessionalObjectIdentify.PrepareComboBoxData = function (listOfItems, mySelection, currentSelector) {
    var id = currentSelector.id;
    var currentValue = currentSelector.value;
    var isDisabled = $(currentSelector).is(':disabled');
    var objectType = currentSelector.attributes["code"].value;


    var parent = $(currentSelector).parent().get(0);
    $(currentSelector).hide();
    $(mySelection).attr("id", objectType + "combobox");
    if (currentSelector.value != "") {
        option = "<option selected=\"selected\" value=\"" + currentSelector.value + "\">" + currentSelector.value + "</option>";
        $(mySelection).append(option);
    }

    $(parent).append(mySelection);

    ProfessionalObjectIdentify.InitComboBox(currentSelector);

    if (isDisabled) {
        $(mySelection).prop('disabled', 'disabled');
    }
    else {
        $("#" + mySelection.id).combobox();
    }

};


ProfessionalObjectIdentify.InitComboBox = function (currentSelector) {
    $.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<span>")
                .addClass(this.element[0].id.replace("combobox", "") + " custom-combobox")
                .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
                value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
                .appendTo(this.wrapper)
                .val(value)
                .attr("title", "")
                .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy(this, "_source2"),
                    open: function (event, ui) {
                        var autocomplete = $(".ui-autocomplete");
                        var oldTop = $(this).position().top;
                        var newTop = $(window).height();
                        if ((newTop - oldTop) < 350) {
                            newTop = newTop - oldTop - 20;
                            newTop = newTop + 'px';
                            autocomplete.css("max-height", newTop);
                        }
                    }
                })
                .tooltip({
                    classes: {
                        "ui-tooltip": "ui-state-highlight"
                    }
                });

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                   
                    ProfessionalObjectIdentify.OnSelectItemChanged(currentSelector, ui.item.value, document.getElementById(currentSelector.id).value);
                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function () {
            var input = this.input,
                wasOpen = false;

            $("<a>")
                .attr("tabIndex", -1)
                .attr("title", "Show All Items")
                .tooltip()
                .appendTo(this.wrapper)
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass("ui-corner-all")
                .addClass("custom-combobox-toggle ui-corner-right")
                .on("mousedown", function () {
                    wasOpen = input.autocomplete("widget").is(":visible");
                })
                .on("click", function () {
                    input.trigger("focus");

                    // Close if already visible
                    if (wasOpen) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    input.autocomplete("search", "");
                });
        },

        _source: function (request, response) {

            if (request.term.trim == "") {
                return null;
            }
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))

                    return {
                        label: text,
                        value: text,
                        option: this
                    };

            }));
        },

        _source2: function (request, response) {
            var ssid = Math.random();
            var currentName = this.element[0].id.replace("combobox", "");

            //var myurl = "../Admin/AjaxRequest.aspx?reqtype=GetList&list=" + this.element[0].id.replace("combobox","") + "&searchItem=" + request.term + "&ssid=" + ssid;
            var myData = null;
            if (currentName == ProfessionalObjectIdentify.Roles.Name())
                myData = ProfessionalObjectIdentify.Roles.istances(request.term);
            else if (currentName == ProfessionalObjectIdentify.Levels.Name())
                myData = ProfessionalObjectIdentify.Levels.istances(request.term);
            else if (currentName == ProfessionalObjectIdentify.Site.Name())
                myData = ProfessionalObjectIdentify.Site.istances(request.term);

            if (myData != null) {
                response($.map(myData, function (item) {
                    if (item != "") {
                        if (typeof item === "string") {
                            return {
                                label: item.trim(),
                                value: item.trim(),
                                option: this
                            };
                        }
                        return $.extend({
                            label: item.label.trim(),
                            value: item.value.trim(),
                            option: this
                        }, item);
                    }
                }));
            }



        },
        _removeIfInvalid: function (event, ui) {

            document.getElementById(currentSelector.id).value = "";
            // Selected an item, nothing to do
            if (ui.item) {
                //RdAProjectSettings.OnSelectItemChanged(currentSelector, ui.item.value, document.getElementById(currentSelector.id).value);
                document.getElementById(currentSelector.id).value = ui.item.value;
                return;
            }

            // Search for a match (case-insensitive)
            var matchText = "";
            var matchValue = "";
            var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
            valueLowerCase = valueLowerCase.trim();
            if (valueLowerCase != "") {
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        matchText = $(this).text();
                        matchValue = $(this).val();

                        return false;
                    }
                });
            }

            // Found a match, nothing to do
            if (valid) {
                ProfessionalObjectIdentify.OnSelectItemChanged(currentSelector, matchValue, document.getElementById(currentSelector.id).value);
                
                document.getElementById(currentSelector.id).value = matchValue;
                this.input.val(matchText);

                return;
            }

            // Remove invalid value
            this.input
                .val("")
                .attr("title", value + " didn't match any item")
                .tooltip("open");
            this.element.val("");
            document.getElementById(currentSelector.id).value = "";
            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input.autocomplete("instance").term = "";
            ProfessionalObjectIdentify.OnSelectItemChanged(currentSelector, "", "");
            
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });

};
ProfessionalObjectIdentify.OnSelectItemChanged = function (currentSelector, actualValue, currentValue) {
    //debugger;
    var resetControl = false;
    if (actualValue == "" || actualValue != currentValue)
        resetControl = true;
    
    
    if (currentSelector.id == ProfessionalObjectIdentify.Roles.selector().id)
    {
        if (ProfessionalObjectIdentify.Site.selector() != null)
        {
            if (resetControl) ProfessionalObjectIdentify.ResetControls(ProfessionalObjectIdentify.Site.Name());
            ProfessionalObjectIdentify.SetReadyControl(ProfessionalObjectIdentify.Site.Name(), ProfessionalObjectIdentify.Site.selector().id);
            ProfessionalObjectIdentify.SetDefaultSite(actualValue);
        }
        
        
    }

};

ProfessionalObjectIdentify.SetReadyControl = function (itemToReady, itemIdToReady) {

    var parentControl1 = $("span[class='" + itemToReady + " custom-combobox']");
    $(parentControl1).find('input, textarea, button, select, a').attr('disabled', false);
    $(parentControl1).find('input').val('');
    $(parentControl1).find('input').css({ 'background-color': '#FFF' });
    document.getElementById(itemIdToReady).value = "";

}


ProfessionalObjectIdentify.SetDefaultSite = function (actualProfessionalRole) {


    
    var http = GetProfessionalXmlHttpObject();
    var ssid = Math.random();

    var currentSelector = ProfessionalObjectIdentify.Roles.selector();
    var objectType = ProfessionalObjectIdentify.Roles.Name();
    if (currentSelector.parentNode.innerHTML.indexOf("PWT Professional Figure") != -1)
        objectType = objectType + "_pwt";

    var url = "";


    var competence = "VEHICLE";
    var context = "STANDARD";
    var supplier = "";
    if (objectType.indexOf("_pwt") != -1)
        competence = "POWERTRAIN";

    if (objectType.indexOf("remedy_") != -1) {
        context = "REMEDY";
        supplier = ProfessionalObjectIdentify.Roles.selectedSupplier();
        if (supplier != null && supplier!="")
            supplier = supplier.replace("[", "").replace("]", "").split("cod.forn.")[1].toString().trim();
    }

    var data = {
        reqtype: 'GetSite',
        basicList: objectType,
        lvl1: null,
        lvl2: null,
        searchItem: '',
        competence: competence,
        context: context,
        suppliercode: supplier,
        professionalrole: actualProfessionalRole
    };


    var returnValue = RdAWBSSettings.GetData(data);

    if (returnValue != null && returnValue.length == 1)
    {
        var siteControlName = ProfessionalObjectIdentify.Site.Name();
        var siteControlId = ProfessionalObjectIdentify.Site.selector().id;
        var parentControl1 = $("span[class='" + siteControlName + " custom-combobox']");
        $(parentControl1).find('input').val(returnValue[0]);
        document.getElementById(siteControlId).value = returnValue[0];
    }
   
};

ProfessionalObjectIdentify.GetListOfSite = function (searchParam) {


    
    var http = GetProfessionalXmlHttpObject();
    var ssid = Math.random();
    
    var currentSelector = ProfessionalObjectIdentify.Roles.selector();
    var objectType = ProfessionalObjectIdentify.Roles.Name();
    if (currentSelector.parentNode.innerHTML.indexOf("PWT Professional Figure") != -1)
        objectType = objectType + "_pwt";

    var url = "";

    debugger;
    var competence = "VEHICLE";
    var context = "STANDARD";
    var supplier = "";
    if (objectType.indexOf("_pwt") != -1)
        competence = "POWERTRAIN";

    if (objectType.indexOf("remedy_") != -1) {
        context = "REMEDY";
        supplier = ProfessionalObjectIdentify.Roles.selectedSupplier();
        if (supplier != null && supplier!="")
            supplier = supplier.replace("[", "").replace("]", "").split("cod.forn.")[1].toString().trim();
    }
    var role = encodeURIComponent(ProfessionalObjectIdentify.Roles.selector().value);

    var currentValue = searchParam;
    var data = {
        reqtype: 'GetSite',
        basicList: objectType,
        lvl1: null,
        lvl2: null,
        searchItem: currentValue,
        competence: competence,
        context: context,
        suppliercode : supplier,
        professionalrole: role
    };

    return RdAWBSSettings.GetData(data);
};


GetProfessionalXmlHttpObject = function () {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari8.  
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE512.  
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
};
