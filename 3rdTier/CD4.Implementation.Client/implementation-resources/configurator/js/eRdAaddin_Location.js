﻿
var Locationdentify = {
    Location: {
        selector: function () {
            var input = $("input[code='Location']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        istances: function () {
            return Locationdentify.GetLocationList();
        }
    }

};

Locationdentify.InitElements = function () {

    var currentSelector = Locationdentify.Location.selector();
    var listOfLocation = null;
    var mySelection = null;
    if (currentSelector != null) {

        var currentValue = currentSelector.value;

        mySelection = document.createElement("div");
        $(mySelection).attr('class', 'locationContainer');
        $.each(currentSelector.attributes, function () {
            if (this.specified) {
                $(mySelection).attr(this.name, this.value);
            }
        });

        listOfLocation = Locationdentify.Location.istances();
        if (listOfLocation != null) {
            if (listOfLocation.length > 0) {
                $(mySelection).append(new Option("", ""));
                for (var i = 0; i < listOfLocation.length; i++) {
                    var optionName = listOfLocation[i];
                    var option = "";
                    if (currentValue.indexOf(optionName) != -1)
                        option = "<input onclick='locationHandleClick(this);' type=\"checkbox\" checked=\"checked\" value=\"" + optionName + "\">" + optionName + "</input><br/>";
                    else
                        option = "<input onclick='locationHandleClick(this);' type=\"checkbox\" value=\"" + optionName + "\">" + optionName + "</input><br/>";
                    $(mySelection).append(option);
                }
            }
        }

        var id = currentSelector.id;

        var isDisabled = $(currentSelector).is(':disabled');

        var parent = $(currentSelector).parent().get(0);
        $(currentSelector).hide();
        $(mySelection).attr("id", "locationListBox");
        $(parent).append(mySelection);

        //$(currentSelector).replaceWith(mySelection.outerHTML);

        //INITALL();

        if (isDisabled) {
            // $("#" + mySelection.id).attr('disabled', 'disabled');
            // $(mySelection).attr('disabled', 'disabled');
            $(mySelection).prop('disabled', 'disabled');
        }
        else {
            //$("#" + mySelection.id).combobox();

        }
        //setTimeout("test();", 1000);
        //$(currentSelector).autocomplete({
        //    source: source,
        //    minLength: 3
        //});
        //$(currentSelector).focus().s;
    }
};


Locationdentify.GetLocationList = function () {
    var currentSelector = Locationdentify.Location.selector();
    var currentValue = currentSelector.value;
    currentValue = encodeURIComponent(currentValue);


    var data = {
        reqtype: 'GetLocationList',
        searchItem: currentValue,
    };
    return RdAWBSSettings.GetData(data);
    
};

Locationdentify.SetControlRed = function (controlId) {
    var supplier = Locationdentify.Location.selector();
    if (supplier)
        if (supplier.id == controlId) {
            $(".locationContainer").addClass('inputred');
            return true;
        }

    return false;
};
Locationdentify.SetControlWhite = function (controlId) {
    var supplier = Locationdentify.Location.selector();
    if (supplier)
        if (supplier.id == controlId) {
            $(".locationContainer").removeClass('inputred');
            return true;
        }
    return false;
};


Locationdentify.EnableControl = function (controlId) {

    var location = Locationdentify.Location.selector();
    if (location)
        if (location.id == controlId) {
            $(".locationContainer").removeAttr('disabled');
            return true;
        }
    return false;

};
Locationdentify.DisableControl = function (controlId) {
    //if (Locationdentify.Location.selector().id == controlId)
    //alert('Disable');
};

Locationdentify.GetXmlHttpObject = function () {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari8.  
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE512.  
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
};
locationHandleClick = function (eventObj) {

    var currentSelector = Locationdentify.Location.selector();
    if (currentSelector == null)
        return;
    var returnValue = "";
    var listOfLocation = $(".locationContainer").find($("input:checked"));
    if (listOfLocation) {
        for (var i = 0; i < listOfLocation.length; i++) {
            returnValue =returnValue + listOfLocation[i].value + ';';
        }
    }
    document.getElementById(currentSelector.id).value = returnValue;

}
