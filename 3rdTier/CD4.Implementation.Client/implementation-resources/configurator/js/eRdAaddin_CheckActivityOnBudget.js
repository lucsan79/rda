﻿var CheckActivityOnBudget = {
    RangeDate: {
        StartDate: function () {
            var input = $("input[code='ActivityStart']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        EndDate: function () {
            var input = $("input[code='ActivityEnd']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        },
        MaxActivityEnd: function () {
            var input = $("input[code='MaxActivityEnd']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },
    RemedySupplier: {
        selector: function () {
            var input = $("select[code='SupplierRemedy']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },
    RemedyProfessionalFigure: {
        selector: function () {
            var input = $("select[code='remedy_rdaw1_professionalrole']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    },
    RemedySite: {
        selector: function () {
            var input = $("select[code='Site']");
            if (input.length == 1)
                return input[0];
            else
                return null;
        }
    }
};

CheckActivityOnBudget.InitElements = function () {

    var maxDate = CheckActivityOnBudget.RangeDate.MaxActivityEnd();
    if (maxDate != null) {
        $(maxDate.parentElement.parentElement).find("input,button,textarea,select").attr("disabled", "disabled");
        $(maxDate.parentElement.parentElement).find("img").hide();
        maxDate.value = "";
        var currentStart = CheckActivityOnBudget.RangeDate.StartDate();
        var inputStart = $(currentStart.parentElement.parentElement).find("input");
        if (inputStart) {
            inputStart[0].onDateSelected = function () {
                maxDate.value = "";
                var myMaxDate = CheckActivityOnBudget.GetMaxActivityDate(inputStart[0].value);
                if (myMaxDate.length == 8) {
                    maxDate.value = myMaxDate.substring(6, 8) + "." + myMaxDate.substring(4, 6) + "." + myMaxDate.substring(0, 4);
                }
            };
            if (inputStart[0].value.length > 0) {
                maxDate.value = "";
                var myMaxDate = CheckActivityOnBudget.GetMaxActivityDate(inputStart[0].value);
                if (myMaxDate.length == 8) {
                    maxDate.value = myMaxDate.substring(6, 8) + "." + myMaxDate.substring(4, 6) + "." + myMaxDate.substring(0, 4);
                }
            }
        }
        

    }
    return "";

    
};


CheckActivityOnBudget.VerifyRangeDateBudget = function () {
    
    var maxDate = CheckActivityOnBudget.RangeDate.MaxActivityEnd();
    if (maxDate) {
        //maxDate.value = "";
        var currentStart = CheckActivityOnBudget.RangeDate.StartDate();
        var currentEnd = CheckActivityOnBudget.RangeDate.EndDate();
        if ((currentStart != null && currentEnd != null) && (!currentStart.disabled && !currentEnd.disabled)) {
            if (currentStart.value != "" && currentEnd.value != "") {
                return CheckActivityOnBudget.ValidateRange(currentStart.value, currentEnd.value)
            }
        }
    }
    return "";

};

CheckActivityOnBudget.ValidateRange = function (startdate, enddate) {

    var definedSupplier = CheckActivityOnBudget.RemedySupplier.selector();
    var definedProfRoles = CheckActivityOnBudget.RemedyProfessionalFigure.selector();
    var definedSite = CheckActivityOnBudget.RemedySite.selector();
    var isPWT = false;
    var competence = "VEHICLE";
    var returnValue = "";
    if (definedSupplier != null && definedProfRoles != null && definedSite != null) {


        if (definedProfRoles.value.indexOf("PWT Professional Figure") != -1)
            competence = "POWERTRAIN";

        var ssid = Math.random();
        var supplierCode = definedSupplier.value;
        supplierCode = supplierCode.replace("[", "").replace("]", "").split("cod.forn.")[1].toString().trim();

        var data = {
            CheckType: 'verifyactivityrange',
            PropertyName: ['startdate', 'enddate', 'competence','supplier', 'professionalrole', 'site'],
            PropertyValue: [startdate, enddate, encodeURIComponent(competence), encodeURIComponent(supplierCode), encodeURIComponent(definedProfRoles.value), definedSite.value],
        };

        var returnValue = CheckActivityOnBudget.GetData( data,"erda/dialog/check");
        
        return returnValue;

    }

};
CheckActivityOnBudget.GetData = function (data, url) {
    if (url === undefined || url === "") url = "erda/dialog/getdialogitems";
    if (jsController) {
        var response = jsController.performRequest(url, 'POST', data)
        if (response === undefined || response === "") return "";

        return response;
    }
    return null;

};
CheckActivityOnBudget.GetMaxActivityDate = function (startdate) {

    
    var definedSupplier = CheckActivityOnBudget.RemedySupplier.selector();
    var definedProfRoles = CheckActivityOnBudget.RemedyProfessionalFigure.selector();
    var definedSite = CheckActivityOnBudget.RemedySite.selector();
    var isPWT = false;
    var competence = "VEHICLE";
    var returnValue = "";
    if (definedSupplier != null && definedProfRoles != null && definedSite != null) {


        if (definedProfRoles.value.indexOf("PWT Professional Figure") != -1)
            competence = "POWERTRAIN";

        var ssid = Math.random();
        var supplierCode = definedSupplier.value;
        supplierCode = supplierCode.replace("[", "").replace("]", "").split("cod.forn.")[1].toString().trim();


        var data = {
            CheckType: 'getactivityrange',
            PropertyName: ['startdate', 'competence', 'supplier', 'professionalrole', 'site'],
            PropertyValue: [startdate,  encodeURIComponent(competence), encodeURIComponent(supplierCode), encodeURIComponent(definedProfRoles.value), definedSite.value],
        };

        var returnValue = CheckActivityOnBudget.GetData(data, "erda/dialog/check");

        return returnValue;

    }




};
