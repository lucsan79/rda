/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */

Ext.application({
    
    name: 'CDI',

    extend: 'CDI.Application',

    //requires: [
    //    'CDI.view.main.Main'
    //],

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //
    //mainView: 'CDI.view.main.Main'
	
    //-------------------------------------------------------------------------
    // Most customizations should be made to CDI.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
    init: function () {
        
        var cdConfigFile = {
            cdUrl: '//localhost:65004/'
        };
        Ext.Ajax.request({
            url: 'endpoint.json',
            async:false,
            success: function (response, options) {
                var data = Ext.decode(response.responseText);
                cdConfigFile.cdUrl = data.connection.url;
            }
        });

        CD.apiUrl = cdConfigFile.cdUrl;
        Ext.USE_NATIVE_JSON = false;
        addSignalR();
        Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider());
        
        Ext.util.Format.thousandSeparator = ',';
        Ext.util.Format.decimalSeparator = '.';
        Ext.Ajax.setTimeout(300000);

        function addSignalR() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = CD.apiUrl + "signalr/hubs/";

            document.getElementsByTagName('head')[0].appendChild(script);
        }
    }
});
