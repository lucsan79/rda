﻿Ext.define('CDI.store.rdaSimpleStore', {});
Ext.deferDefine('CDI.store.rdaSimpleStore', function () {
    return {
        extend: 'Ext.data.Store',
        alias: 'store.simplestore',
        storeId: 'simplestore',
        fields: [],
        data: [],
        pageSize: 50,
        remoteFilter: true,
        proxy: {
            type: 'memory',
            enablePaging: true
        }
    }
});
