/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('CDI.Application', {
    extend: 'CD.Application',
    
    name: 'CDI',

    requires: [
        'CDI.service.Translate',
        'CDI.service.erdaAction',
        'CDI.model.CurrencyField',
        'CDI.view.main.MainView',
        'CDI.view.main.MainViewController',
        'CDI.view.login.LoginViewController',
        'CDI.view.popup.wfRouteView',
        'CDI.view.popup.wfRouteViewController',
        'CDI.view.popup.sendToBaanView',
        'CDI.view.popup.sendToBaanViewController',
        'CDI.view.popup.sendMaterialsView',
        'CDI.view.popup.sendMaterialsViewController',
        'CDI.view.popup.exportDataView',
        'CDI.view.popup.exportDataViewController',
        'CDI.view.popup.changeCostView',
        'CDI.view.popup.changeCostViewController',
        'CDI.view.popup.updatePriceList',
        'CDI.view.popup.updatePriceListController',
        'CDI.view.popup.multiUserProjectAssignment',
        'CDI.view.popup.multiUserProjectAssignmentController',

        'CDI.view.popup.fastClosure',
        'CDI.view.popup.fastClosureController',

        'CDI.store.rdaSimpleStore',
        'CDI.view.businessdata.BusinessDataWrapper',
        'CDI.view.components.contentSection.workflow.WorkflowItemViewController',
        'CDI.view.components.contentSection.erda.spendingCurve',
        'CDI.view.components.contentSection.erda.spendingCurveController',
        'CDI.view.components.dialogSection.createRdAView',
        'CDI.view.components.dialogSection.dialogConfigurationManager',
        'CDI.view.components.menuSection.info.InfoViewController',
        'CDI.view.components.menuSection.multimaterial.mainView',
        'CDI.view.components.menuSection.multimaterial.mainViewController',
        'CDI.view.components.menuSection.multimaterial.objectListView',

        'CDI.view.components.applicationSection.search.SearchViewController',
        'CDI.view.components.applicationSection.search.SearchResultView',
        'CDI.view.components.applicationSection.search.SearchResultViewController',
        'CDI.view.components.applicationSection.search.data.gridView',
        'CDI.view.components.applicationSection.search.data.gridViewController',

        'CDI.view.dashboard.DashboardHome',
        'CDI.view.dashboard.common.atcView',
        'CDI.view.dashboard.common.atcViewController',
        'CDI.view.dashboard.common.OverviewRda',
        'CDI.view.dashboard.common.OverviewRdaController',
        'CDI.view.dashboard.common.OverviewRdaFilter',
        'CDI.view.dashboard.home.rdaHomeView',
        'CDI.view.dashboard.home.rdaHomeFilter',
        'CDI.view.dashboard.home.rdaHomeViewController',

       
        'CDI.view.dashboard.work.base.objectListView',
        'CDI.view.dashboard.work.base.projectListView',
        'CDI.view.dashboard.work.mainView',
        'CDI.view.dashboard.work.mainViewController',

        'CDI.view.dashboard.sync.base.objectListView_prp',
        'CDI.view.dashboard.sync.base.objectListView_pes',
        'CDI.view.dashboard.sync.base.objectListView_ar',
        'CDI.view.dashboard.sync.base.objectListView_sc',
        'CDI.view.dashboard.sync.base.objectUpdateView_pes',
        'CDI.view.dashboard.sync.base.objectUpdateView_prp',
        'CDI.view.dashboard.sync.base.projectListView',
        'CDI.view.dashboard.sync.mainView_pes',
        'CDI.view.dashboard.sync.mainView_prp',
        'CDI.view.dashboard.sync.mainView_ar',
        'CDI.view.dashboard.sync.mainView_sc',
        'CDI.view.dashboard.sync.mainViewController',


        'CDI.view.pickers.eRdaSearchParameter',
        'CDI.view.pickers.eRdaSearchParameterController',

        //ADMIN
        'CDI.view.components.rdaAdminCenter.common.baanConfigView',
        'CDI.view.components.rdaAdminCenter.common.baanConfigViewController',
        'CDI.view.components.rdaAdminCenter.rdaAdminCenterWrapper',
        'CDI.view.components.rdaAdminCenter.rdaAdminCenterWrapperController',
         'CDI.view.components.rdaAdminCenter.common.adminGrid',
        'CDI.view.components.rdaAdminCenter.common.adminGridController',


        'CDI.view.components.rdaAdminCenter.dialogConfig.dialogConfigWrapper',
        'CDI.view.components.rdaAdminCenter.dialogConfig.dialogConfigWrapperController',
        'CDI.view.components.rdaAdminCenter.dialogConfig.panels.brandView',
        'CDI.view.components.rdaAdminCenter.dialogConfig.panels.competenceView',
        'CDI.view.components.rdaAdminCenter.dialogConfig.panels.projectView',
        'CDI.view.components.rdaAdminCenter.dialogConfig.panels.wbsGrid',
        

        'CDI.view.components.rdaAdminCenter.dialogContent.dialogContentWrapper',
        'CDI.view.components.rdaAdminCenter.dialogContent.dialogContentWrapperController',
        'CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalLevels',
        'CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalLocation',
        'CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalRoleView',
        

        'CDI.view.components.rdaAdminCenter.supplierManagement.supplierGrid',

        'CDI.view.components.rdaAdminCenter.roleManager.roleManagerWrapper',
        'CDI.view.components.rdaAdminCenter.roleManager.roleManagerWrapperController',
        'CDI.view.components.rdaAdminCenter.roleManager.reqControllersView',
        'CDI.view.components.rdaAdminCenter.roleManager.reqControllersViewController',
        'CDI.view.components.rdaAdminCenter.roleManager.reqDriversView',
        'CDI.view.components.rdaAdminCenter.roleManager.reqDriversViewController',
        'CDI.view.components.rdaAdminCenter.roleManager.reqReadersView',
        'CDI.view.components.rdaAdminCenter.roleManager.reqReadersViewController',
        'CDI.view.components.rdaAdminCenter.roleManager.roleFullListView',

        'CDI.view.components.menuSection.info.businessdata.BusinessDataView',
        'CDI.view.components.menuSection.info.businessdata.BusinessDataViewController',
        'CDI.view.components.menuSection.info.businessdata.BusinessDataToolbar',
        'CDI.view.components.menuSection.info.businessdata.BusinessDataToolbarController',

        'CDI.view.components.rdaAdminCenter.roleManager.roleGridSimply',
        'CDI.view.components.rdaAdminCenter.roleManager.roleGrid',
        'CDI.view.components.rdaAdminCenter.roleManager.roleGridController',
        'CDI.view.components.rdaAdminCenter.roleManager.roleGroupingGrid',
        'CDI.view.components.rdaAdminCenter.roleManager.roleGridWithOutBBar',

        
        'CDI.view.dashboard.archive.mainView',
        'CDI.view.dashboard.archive.mainViewwController',
        'CDI.view.dashboard.archive.data.gridView',
        'CDI.view.dashboard.archive.data.gridViewController',
        'CDI.view.components.rdaAdminCenter.supplierManagement.supplierWrapper',
        'CDI.view.components.rdaAdminCenter.supplierManagement.supplierWrapperController',
        'CDI.view.components.rdaAdminCenter.supplierManagement.popupSaveDiscardView',
        'CDI.view.components.rdaAdminCenter.supplierManagement.popupSaveDiscardViewController',
        
    ],

    
    //stores: [
    //    'CDI.store.RdAUsers'
    //]
    //constructor: function () {
    //    this.requires = this.superclass.requires.concat(this.implementationRequires);

    //    this.callSuper();
    //}

    //stores: [
    //    // TODO: add global / shared stores here
    //],
    
    //launch: function () {
    //    // TODO - Launch the application
    //},

    //onAppUpdate: function () {
    //    Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
    //        function (choice) {
    //            if (choice === 'yes') {
    //                window.location.reload();
    //            }
    //        }
    //    );
    //}
},
function () {
    
    //register attribute triggers
    CD.AttributeTriggers = {
        'trigger-searchparam': {
            picker: {
                cls: 'erda x-form-search-trigger',
                handler: function () {
                    var textbox = this;
                    var picker = Ext.create('CDI.view.pickers.eRdaSearchParameter', {
                        inputText: textbox
                    });
                    picker.show();
                }
            }
        },


    };
});

