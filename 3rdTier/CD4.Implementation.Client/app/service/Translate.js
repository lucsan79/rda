﻿Ext.define('CDI.service.Translate',
{
    singleton: true,

    data: {},

    successFunction: function () { },
    errorFunction: function () { },

    init: function (lang) {
        var self = this;

        var urlPrefix = 'implementation-resources/translations/';

        // Fetch translation
        Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json'
                },
                url: urlPrefix + lang + '.json'
            })
            .then(function (data) {
                    self.data = Ext.util.JSON.decode(data.responseText);

                    self.successFunction();
                },
                function () {
                    self.errorFunction();
                });

        return this;
    },

    done: function (success, error) {
        this.successFunction = success;
        this.errorFunction = error;
    }

});