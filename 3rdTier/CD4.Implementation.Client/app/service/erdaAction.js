﻿Ext.define('CDI.service.erdaAction',
{
    singleton: true,

    status:null,
    SetPanelVisibility: function (panels) {
        
        var userProfile = this.getUserProfile();
        if (panels != null) {
            for (var i = 0; i < panels.length; i++) {
                switch (panels[i].itemId) {
                    case "worklist":
                        if (userProfile.OnlyBaaN) panels[i].hidden = true;
                        else {
                            
                            if (userProfile.PM) {
                                var pm = true;
                                
                                for (var key in userProfile) {
                                    var value = userProfile[key];
                                    if (key != "PM" && value == true) {
                                        pm = false;
                                    }
                                }
                                if (pm)
                                    panels[i].hidden = true;
                            }
                            
                        }
                        break;
                    case "tobaanpes":
                        if (userProfile.SuperVisor == false && userProfile.PESController == false)
                            panels[i].hidden = true;
                        break;
                    case "tobaanprp":
                        if (userProfile.SuperVisor == false && userProfile.PRPController == false)
                            panels[i].hidden = true;
                        break;
                    case "folderar":
                        if (userProfile.SuperVisor == false && userProfile.PESController == false && userProfile.PRPController == false)
                            panels[i].hidden = true;
                        break;
                    case "spendingcurve":
                        //if (userProfile.OnlyBaaN) panels[i].hidden = true;
                        if (userProfile.REQUESTER == false && userProfile.CMS == false)
                            panels[i].hidden = true;
                        break;
                    case "rdainfastmaterial":
                        if (userProfile.REQUESTER == false  && userProfile.CMS == false)
                            panels[i].hidden = true;
                        break;
                    case "rdamulticreation":
							
                        if (userProfile.REQUESTER == false && userProfile.CMS == false)
                            panels[i].hidden = true;
                        break;

                }
            }
        }

    },

    replaceAll(givenText, valueToMatch, valueToReplace) {
        var value = givenText;
        while (value.indexOf(valueToMatch) != -1) {
            value = value.replace(valueToMatch, valueToReplace);
        }
        return value;
    },
    getUserProfile: function () {
        var userProfile = null;
        Ext.Ajax.request({
            url: CD.apiUrl + 'erda/userprofile',
            method: 'GET',
            withCredentials: true,
            async: false,
            success: function (jsonData) {
                userProfile = Ext.decode(jsonData.responseText);
            }
        });
        return userProfile;
    },

    open: function (id) {

        Ext.Ajax.request({
            url: CD.apiUrl + 'erda/' + id,
            method: 'GET',
            withCredentials: true,
            async: false,
            failure: function (response) {
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-security-alert'], "", response);
            },
            success: function (jsonData) {
                var rda = Ext.decode(jsonData.responseText);
                Ext.fireEvent('addTab', rda.Name, 'businessdatawrapper',
                    {
                        businessDataType: rda.BusinessDataType,
                        itemName: rda.Name,
                        businessDataId: rda.Id,
                        description: rda.Description,
                        revision: rda.Revision,
                        level: rda.Level,
                        project: rda.Project,
                        workflow: rda.Workflow,
                        owner: rda.Owner,
                        createUser: rda.CreateUser
                    }, true, null, 'fa-donate');


            }
        });
    },

    open2: function (special,index) {

        Ext.fireEvent('addTab', special.itemName, 'businessdatawrapper', special, true, null, 'fa-donate', index);
    },


    showWorkflowAction: function (viewId, actionType, itemId, itemNumber, itemLevel, extraParam) {
		
        var win = new Ext.Window({
            header: false,
            border: false,
            closable: false,
            draggable: false,
            itemId: 'WfgWin',
            title: false,
            width: '500px',
            height: '375px',
            closeAction: 'hide',
            buttonAlign: 'center',
            closable: false,
            modal: true,
            animShow: function () {
                this.el.slideIn('t', {
                    duration: 1, callback: function () {
                        this.afterShow(true);
                    }, scope: this
                });
            },
            animHide: function () {
                this.el.disableShadow();
                this.el.slideOut('t', {
                    duration: 1, callback: function () {
                        this.el.hide();
                        this.afterHide();
                    }, scope: this
                });
            },
            layout: {
                align: 'stretch',
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'wfrouteview',
                    config: {
                        actiontype: actionType,
                        businessDataId: itemId,
                        businessDataName: itemNumber,
                        businessDataLevel: itemLevel,
                        viewId: viewId,
                        extraParam: extraParam,
                    },
                    
                    flex: 1,
                }
            ],


        });
        win.show(Ext.getBody());
    },

    showSendBaaNAction: function (viewId, actionType, itemId, itemNumber) {
        var win = new Ext.Window({
            header: false,
            border: false,
            closable: false,
            draggable: false,
            itemId: 'WfgWin',
            title: false,
            width: '500px',
            height: '300px', closeAction: 'hide', buttonAlign: 'center',
            closable: false,
            modal: true,
            animShow: function () {
                this.el.slideIn('t', {
                    duration: 1, callback: function () {
                        this.afterShow(true);
                    }, scope: this
                });
            },
            animHide: function () {
                this.el.disableShadow();
                this.el.slideOut('t', {
                    duration: 1, callback: function () {
                        this.el.hide();
                        this.afterHide();
                    }, scope: this
                });
            },
            layout: {
                align: 'stretch',
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'sendtobaanview',
                    config: {
                        actiontype: actionType,
                        businessDataId: itemId,
                        businessDataName: itemNumber,
                        viewId: viewId
                    },
                    flex: 1,
                }
            ],


        });
        win.show(Ext.getBody());
    },

    showFastClosureAction: function (viewId, actionType, itemId, itemNumber) {
        var win = new Ext.Window({
            header: false,
            border: false,
            closable: false,
            draggable: false,
            itemId: 'WfgWin',
            title: false,
            width: '500px',
            height: '350px', closeAction: 'hide', buttonAlign: 'center',
            closable: false,
            modal: true,
            animShow: function () {
                this.el.slideIn('t', {
                    duration: 1, callback: function () {
                        this.afterShow(true);
                    }, scope: this
                });
            },
            animHide: function () {
                this.el.disableShadow();
                this.el.slideOut('t', {
                    duration: 1, callback: function () {
                        this.el.hide();
                        this.afterHide();
                    }, scope: this
                });
            },
            layout: {
                align: 'stretch',
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'fastclosure',
                    config: {
                        actiontype: actionType,
                        businessDataId: itemId,
                        businessDataName: itemNumber,
                        viewId: viewId
                    },
                    flex: 1,
                }
            ],


        });
        win.show(Ext.getBody());
    },


    showSaveMaterialsAction: function (viewId, materialContent) {
        var win = new Ext.Window({
            header: false,
            border: false,
            closable: false,
            draggable: false,
            itemId: 'WfgWin',
            title: false,
            width: '500px',
            height: '300px', closeAction: 'hide', buttonAlign: 'center',
            closable: false,
            modal: true,
            animShow: function () {
                this.el.slideIn('t', {
                    duration: 1, callback: function () {
                        this.afterShow(true);
                    }, scope: this
                });
            },
            animHide: function () {
                this.el.disableShadow();
                this.el.slideOut('t', {
                    duration: 1, callback: function () {
                        this.el.hide();
                        this.afterHide();
                    }, scope: this
                });
            },
            layout: {
                align: 'stretch',
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'sendmaterialsview',
                    config: {
                        businessDataId: materialContent.businessDataId,
                        rows: materialContent.rows,
                        viewId: viewId
                    },
                    flex: 1,
                }
            ],


        });
        win.show(Ext.getBody());
    },

    showExportPanel: function (exporttype,options) {
        var win = new Ext.Window({
            header: false,
            border: false,
            closable: false,
            draggable: false,
            itemId: 'WfgWin',
            title: false,
            width: '500px',
            height: '300px', closeAction: 'hide', buttonAlign: 'center',
            closable: false,
            modal: true,
            animShow: function () {
                this.el.slideIn('t', {
                    duration: 1, callback: function () {
                        this.afterShow(true);
                    }, scope: this
                });
            },
            animHide: function () {
                this.el.disableShadow();
                this.el.slideOut('t', {
                    duration: 1, callback: function () {
                        this.el.hide();
                        this.afterHide();
                    }, scope: this
                });
            },
            layout: {
                align: 'stretch',
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'exportdataview',
                    config: {
                        exporttype: exporttype,
                        options: options
                    },
                    flex: 1,
                }
            ],


        });
        win.show(Ext.getBody());
    },

    showImportPanel: function (importtype, parentViewId, options) {
        var win = new Ext.Window({
            header: false,
            border: false,
            closable: false,
            draggable: false,
            itemId: 'WfgWin',
            title: false,
            width: '500px',
            height: '350px', closeAction: 'hide', buttonAlign: 'center',
            closable: false,
            modal: true,
            animShow: function () {
                this.el.slideIn('t', {
                    duration: 1, callback: function () {
                        this.afterShow(true);
                    }, scope: this
                });
            },
            animHide: function () {
                this.el.disableShadow();
                this.el.slideOut('t', {
                    duration: 1, callback: function () {
                        this.el.hide();
                        this.afterHide();
                    }, scope: this
                });
            },
            layout: {
                align: 'stretch',
                type: 'vbox'
            },
            items: [
                {
                    xtype: 'updatepricelist',
                    config: {
                        viewId: parentViewId,
                        importtype: importtype,
                        options: options
                    },
                    flex: 1,
                }
            ],


        });
        win.show(Ext.getBody());
    },

    performTask: function (callbackController, wfActionConfig, progressBar) {
        //var actionWF = {
        //    actiontype: wfActionConfig.actionType,
        //    businessDataId: itemId,
        //    reason: reason
        //};
        var data;
        var itemsid = [];
        var itemsName = [];
        if (Array.isArray(wfActionConfig.businessDataId)) {
            itemsid = wfActionConfig.businessDataId;
            itemsName = wfActionConfig.businessDataName;
        }
        else {
            itemsid.push(wfActionConfig.businessDataId);
            itemsName.push(wfActionConfig.businessDataName);
        }
        this.recursiveperform(callbackController, wfActionConfig, progressBar, 0, itemsid, itemsName);
    },

    recursiveperform: async function (callbackController, wfActionConfig, progressBar, index, itemsId, itemsName,data)
    {
        
        var self = this;
        if (index < itemsId.length) {
            var actionWF = {
                actiontype: wfActionConfig.actiontype,
                businessDataId: itemsId[index],
                businessDataName: itemsName[index],
                reason: wfActionConfig.reason
            };
            if (actionWF.actiontype.toLowerCase()=="reject" && wfActionConfig.backCMS != null && wfActionConfig.backCMS == true)
                actionWF.backCMS = true;

            if (progressBar != null) {
                progressBar.updateText(itemsName[index]);
            }
            var urlAction = CD.apiUrl + 'erda/workflow/execute';
            if (wfActionConfig.extraParam != null && wfActionConfig.extraParam.url != "" && !actionWF.backCMS) {
                urlAction = wfActionConfig.extraParam.url;
            }
            Ext.Ajax.request({
                url: urlAction,//CD.apiUrl + 'erda/workflow/execute',
                method: 'POST',
                jsonData: actionWF,
                withCredentials: true,
                //async: false,
                failure: function (response) {
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-workflow-title'],"",response);
                    callbackController.donewWithError();
                },
                success: function (jsonData) {
                    data = Ext.decode(jsonData.responseText);
                    index += 1;
                    self.recursiveperform(callbackController, wfActionConfig, progressBar, index, itemsId, itemsName,data);

                }
            });
        }
        else {
            callbackController.doneSuccess(data);
        }
            
    },


    sendeRdA: function (callbackController, wfActionConfig, progressBar) {
        //var actionWF = {
        //    actiontype: wfActionConfig.actionType,
        //    businessDataId: itemId,
        //    reason: reason
        //};
        var data;
        var itemsId = [];
        if (Array.isArray(wfActionConfig.businessDataId)) {
            itemsId = wfActionConfig.businessDataId;
        }
        else {
            itemsId.push(wfActionConfig.businessDataId);
        }
        var actionBaan = {
            actiontype: wfActionConfig.actiontype,
            businessDataId: itemsId,
        };
        if (actionBaan.actiontype == "close") {
            actionBaan.baancode = wfActionConfig.baancode;
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/close',
                method: 'POST',
                jsonData: actionBaan,
                withCredentials: true,
                //async: false,
                failure: function (response) {
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-baansync-title'], "", response);
                    callbackController.donewWithError();
                },
                success: function (jsonData) {
                    data = Ext.decode(jsonData.responseText);
                    callbackController.doneSuccess(data);

                }
            });

        }
        else {
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/baan/sync',
                method: 'POST',
                jsonData: actionBaan,
                withCredentials: true,
                //async: false,
                failure: function (response) {
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-baansync-title'], "", response);
                    callbackController.donewWithError();
                },
                success: function (jsonData) {
                    data = Ext.decode(jsonData.responseText);
                    callbackController.doneSuccess(data);

                }
            });
        }
        

        
    },

    creatematerials: function (callbackController, matActionConfig, progressBar) {
        
        Ext.Ajax.request({
            url: CD.apiUrl + 'erda/materials/save',
            method: 'POST',
            jsonData: matActionConfig,
            withCredentials: true,
            //async: false,
            failure: function (response) {
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-baansync-title'], "", response);
                callbackController.donewWithError();
            },
            success: function (jsonData) {
                data = Ext.decode(jsonData.responseText);
                callbackController.doneSuccess(data);

            }
        });


    },

    exportdata: function (callbackController, config, progressBar) {

        var url = CD.apiUrl + 'erda/export/' + config.exporttype;

        Ext.Ajax.request({
            timeout: 240000,
            url: url,
            method: 'POST',
            jsonData: config.options,
            withCredentials: true,
            //async: false,
            failure: function (response) {
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-export-title'], "", response);
                callbackController.donewWithError();
            },
            success: function (jsonData) {
                data = Ext.decode(jsonData.responseText);
               
                callbackController.doneSuccess(data);

            }
        });


    },


    importData: function (callbackController, inputdata, progressBar) {

        var url = CD.apiUrl + 'erda/update/remedypricelist';

        Ext.Ajax.request({
            timeout: 240000,
            url: url,
            method: 'POST',
            jsonData: inputdata,
            withCredentials: true,
            //async: false,
            failure: function (response) {
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-update-title'], "", response);
                callbackController.donewWithError();
            },
            success: function (jsonData) {
                callbackController.doneSuccess(null);

            }
        });


    },


    showErrorMessage: function (title, message, response) {
        
        if (message == "") {
            var dummy = null;

            if (dummy == null) {
                try {
                    dummy = Ext.decode(Ext.decode(response.responseText).ExceptionMessage).ErrorMessage;
                } catch (err) { }
            }
            if (dummy == null) {
                try {
                    dummy = Ext.decode(response.responseText).ExceptionMessage;
                } catch (err) { }
            }
            if (dummy == null) {
                try {
                    dummy = Ext.decode(response.responseText).Message;
                } catch (err) { }
            }

            if (dummy != null && dummy != "")
                message = dummy;
            else
                message = CDI.service.Translate.data["generic-error-internal-error"];
        }
       
        Ext.MessageBox.show({
            title: title,
            msg: message,
            buttons: Ext.MessageBox.OK,
            cls: "messageboxforerror",
            //animateTarget: btn,
            icon: Ext.MessageBox.ERROR,
        });
    },

    showCornfrmOnWarning: function (message) {
        var title = CDI.service.Translate.data["generic-error-warning-title"];
        Ext.MessageBox.confirm(title, message,
            function (id) {
                if (id === 'yes')
                    return true;

            });
        return false;
    },


    showToastError: function (message) {
        //Calculation is based on the average reading speed that around 200/250 words per minute (wpm)
        if (message === undefined || message == "")
            return;
        var timeout = 2000;
        var words = message.split(" ");
        timeout = (60000 * words.length) / 240;
        if (timeout < 2000) timeout = 2000;

        Ext.toast({
            html: '<span class="toasterror">' + message + '</span>',
            slideDUration: 800,
            closable: false,
            align: 't',
            cls: 'toastforerror',
            ui: 'warn',
            timeout: 2500
        });
    },
	
	showToastMessage: function (message) {
        Ext.toast({
            html: message,
            slideDUration: 800,
            closable: false,
            bodyStyle: 'background-color: green; color: white',
			align: 'tr',
			anchor: 'contentPanel'
        });
    },

    onFailureRequest: function (jqXHR, exception, messageHaader) {
        var message;
        var statusErrorMap = {
            '400': "Server understood the request, but request content was invalid.",
            '401': "Unauthorized access.",
            '403': "Forbidden resource can't be accessed.",
            '500': "Internal server error.",
            '503': "Service unavailable."
        };
        if (jqXHR.status) {
            message = statusErrorMap[x.status];
            if (!message) {
                message = "Unknown Error \n.";
            }
        }
        else if (exception == 'parsererror') {
            message = "Error.\nParsing JSON Request failed.";
        }
        else if (exception == 'timeout') {
            message = "Request Time out.";
        }
        else if (exception == 'abort') {
            message = "Request was aborted by the server";
        }
        else {
            message = "Unknown Error \n.";
        }
        if (messageHaader === undefined || messageHaader == "")
            messageHaader = CDI.service.Translate.data['rda-generic-error'];
        CDI.service.erdaAction.showErrorMessage(messageHaader, message);
    },

    getFileData: async function (file) {
            let result_base64 = await new Promise((resolve) => {
                let fileReader = new FileReader();
                fileReader.onload = (e) => resolve(fileReader.result);
                fileReader.readAsArrayBuffer(file);
            });

            return result_base64;
    },
    cacheFileData: function (file, keyserver) {
        var uploader = file.el.down('input[type=file]').dom.files[0];
        var reader = new FileReader();
        reader.onload = function () {
            var dataURL = reader.result;
            var response = Ext.Ajax.request({
                url: CD.apiUrl + 'erda/cachefile',
                method: "POST",
                jsonData: {
                    cacheName: keyserver,
                    Name: uploader.name,
                    data: dataURL
                },
                withCredentials: true,
                async: false,
                callback: function (opt, success, response) {
                    if (!success) {
                        self.showError(self.getMessageError(response));
                        return null;
                    }
                }
            });
        };
        reader.readAsDataURL(uploader);
        
    },

    formatMoney: function (amount, sign, decimals, end) {
        // Save the thousand separator
        
        var value = amount;
        value = amount.toString().replace(",", ".");
        var format = Ext.util.Format;
        format.thousandSeparator = '.';
        format.decimalSeparator = ',';
        var formatted = format.currency(value, sign, decimals, end);
        return formatted;
    },

    panelbuttonview: function (view) {


        var btnEdit = view.down('#btnEdit');
        var btnSaveAs = view.down("#btnSaveAs");
        var btnDelete = view.down("#btnDelete");
        var btnChangeCost = view.down("#btnChangeCost");

        if (!CD.service.UserData.userData.IsAdmin) {
            if (view.special.level.toLowerCase() != "working" && view.special.level.toLowerCase() != "cms validation")
                btnChangeCost.hide();

            btnSaveAs.setDisabled(true);
            if (view.special.workflow != "WF_RDA_01" || view.special.level.toLowerCase() == "closed") {
                btnEdit.setDisabled(true);
            }
            else if (view.special.workflow == "WF_RDA_01") {
                btnSaveAs.setDisabled(true);
                if (CD.service.UserData.userData.Groups != null) {
                    if (CD.service.UserData.userData.Groups.includes('Requester'))
                        btnSaveAs.setDisabled(false);
                }
            }
        }

        if (view.special.workflow == "WF_CANCEL" || view.special.level.toLowerCase() == "cancelled")
            btnDelete.setDisabled(true);
        else {
            btnDelete.setDisabled(true);
            if (view.special.workflow == "WF_RDA_01" && view.special.level.toLowerCase() == "working")
                if (view.special.owner == CD.service.UserData.userData.Username)
                    btnDelete.setDisabled(false);
        }
        if (CD.service.UserData.userData.IsAdmin)
            if (view.special.workflow != "WF_CANCEL")
                btnDelete.setDisabled(false);
       

    }

});