﻿Ext.define('CDI.view.popup.changeCostView', {});

Ext.deferDefine('CDI.view.popup.changeCostView', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'changecostview',
        controller: 'changecostviewcontroller',
        cls: 'easyeditview changecost',
        title: '<i class="fas fa-coins"></i> ' + CDI.service.Translate.data["rda-admincenter-changecostbtn-title"],
        buttonAlign: 'center',
        listeners: {
            beforerender: 'onBeforeRender',
        },
        bodyPadding: 10,

        config: {
            //actiontype: actionType,
            //businessDataId: itemId,
            //businessDataName: itemNumber,
            //viewId: viewId
        },
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },

        defaults: {
            //cls: 'modal-content',
            xtype: 'label',
        },
        fieldDefaults: {
            labelAlign: 'right',
            labelWidth: 110,
            msgTarget: 'side'
        },

        items: [
            {
                xtype: 'fieldset',
                title: CDI.service.Translate.data["rda-changecost-title1"],
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },

                items: [
                    {
                        allowBlank: false,
                        disabled:true,
                        fieldLabel: CDI.service.Translate.data["rda-supplier-grid-supplier"],
                        itemId: 'supplier',
                        text: CDI.service.Translate.data["rda-supplier-grid-supplier-tobesourced"],
                    },
                    {
                        allowBlank: false,
                        disabled: true,
                        fieldLabel: CDI.service.Translate.data["rda-changecost-professionalrole"],
                        itemId: 'professionalrole',
                    }
                ]
            },
            {
                xtype: 'fieldset',
                itemId:'rdhours',
                title: CDI.service.Translate.data["rda-changecost-title2"],
                defaultType: 'numberfield',
                layout: {
                    type: 'table',
                    columns: 2,
                },
                defaults: {
                    anchor: '100%',
                    cls: 'tdcontent'
                },

                items:
                    [
                        {
                            fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-hours"],
                            itemId: 'hours',
                            minValue: 1,
                            listeners: {
                                change: function (field, newVal, oldVal) {

                                    var amount = Ext.getCmp('amount');
                                    if (amount.disabled) {
                                        var daily = Ext.getCmp('dailycost').getValue();
                                        var total = daily * newVal;
                                        amount.setValue(total);
                                        amount.fireEvent('blur', amount);
                                    }
                                    
                                }
                            }
                        },
                        {
                            fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-dailycost"],
                            itemId: 'dailycost',
                            id: 'dailycost',
                            minValue: 0,
                            xtype: 'erdacurrencyfield',
                            disabled: true,
                        },
                        {
                            fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-amount"],
                            itemId: 'amount',
                            id: 'amount',
                            
                            xtype: 'erdacurrencyfield',
                            minValue: 0,
                            //disabled: true,
                        },
                        {
                            xtype: 'combobox',
                            itemId: 'amountformat',
                            fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-piecepriceformat"],
                            displayField: 'Value',
                            valueField: 'Value',
                            listeners: {
                                select: function (combo, records, eOpts) {
                                    var panel = combo.up();
                                    var amount = panel.down("#amount");
                                    if (records.data.Value == "dollar")
                                        amount.currency = "$";
                                    else
                                        amount.currency = "€";
                                    amount.fireEvent('blur', amount);
                                }
                            },
                            store: {
                                type: 'array',
                                fields: ['Value'],
                                data: [
                                    ['euro'],
                                    ['dollar'],
                                ]
                            },
                        },
                       
                    ]
            },
            {
                xtype: 'fieldset',
                itemId: 'materials',
                title: CDI.service.Translate.data["rda-changecost-title2"],
                defaultType: 'numberfield',
                layout: {
                    type: 'table',
                    columns: 2,
                },
                defaults: {
                    anchor: '100%',
                    cls:'tdcontent'
                },

                items:
                    [
                        {
                            fieldLabel: CDI.service.Translate.data["rda-multimaterial-quantityonvehicle-2"],
                            itemId: 'quantityonvehicle',
                        },
                        {
                            fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-totalquantity"],
                            itemId: 'totalquantity',
                        },
                        {
                            fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-pieceprice"],
                            itemId: 'pieceprice',
                            xtype: 'erdacurrencyfield',
                        },
                        {
                            xtype: 'combobox',
                            itemId: 'piecepriceformat',
                            fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-piecepriceformat"],
                            displayField: 'Value',
                            valueField: 'Value',
                            listeners: {
                                select: function (combo, records, eOpts) {
                                    var panel = combo.up();
                                    var pieceprice = panel.down("#pieceprice");
                                    if (records.data.Value == "dollar")
                                        pieceprice.currency = "$";
                                    else
                                        pieceprice.currency = "€";
                                    pieceprice.fireEvent('blur', pieceprice);
                                }
                            },
                            store: {
                                type: 'array',
                                fields: ['Value'],
                                data: [
                                    ['euro'],
                                    ['dollar'],
                                ]
                            },
                        },
                    ]
            }
        ],

        fbar: [
            {
                xtype: 'button',
                iconCls: 'fas fa-ban',
                cls: 'bottombutton x-btn-plain-toolbar-small',
                text: CDI.service.Translate.data["rda-admincenter-cancelbtn-tooltip"],
                handler: 'onButtonCloseWindown',
               

            },
            {
                xtype: 'button',
                iconCls: 'fas fa-save',
                cls: 'bottombutton x-btn-plain-toolbar-small',
                text: CDI.service.Translate.data["rda-admincenter-savebtn-tooltip"],
                width: 150,
                handler: 'onCompleteClick',
                
            }
        ],




    }
});