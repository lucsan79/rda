﻿Ext.define('CDI.view.popup.wfRouteViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wfrouteviewcontroller',

    onBeforeRender: function (view) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId

        var actionType = view.config.actiontype;
        var items = view.items;
        var undeControl = true;
        
        if (Array.isArray(view.config.businessDataId)) {
            for (var i = 0; i < view.config.businessDataLevel.length; i++) {
                if (view.config.businessDataLevel[i].toLowerCase() != "under control")
                    undeControl = false;
            }
            items.items[3].text = "[" + view.config.businessDataId.length + " " + CDI.service.Translate.data["rda-selected"] + "]"
        }
        else {
            if (view.config.businessDataLevel.toLowerCase() != "under control" )
                undeControl = false;
            items.items[3].text = view.config.businessDataName;
        }
        

        if (view.config.extraParam != null) {
            if (view.config.extraParam.comment != "") {
                var textarea = view.down("textareafield");
                textarea.setValue(view.config.extraParam.comment);
            }
        }
        if (actionType == "reject") {
            items.items[0].html = items.items[0].html.replace("approve", "reject");
            items.items[2].html = CDI.service.Translate.data["common-question-wfreject-message"];
            items.items[3].cls = 'modal-body ' + actionType;
            items.items[4].emptyText = CDI.service.Translate.data["common-question-comment-reject"];
            var buttons = view.query("button");
            if (buttons != null && buttons.length == 2) {
                buttons[1].cls='btn btn-danger';
                buttons[1].text = CDI.service.Translate.data['common-wfaction-reject'];
            }
            if (undeControl) {
                view.up().height = "420px";
                view.down("radiogroup").show();
            }
            else
                view.down("radiogroup").destroy();

        }
        
    },
    
    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.close();
            win.destroy();
        }
    },

    onButtonPerformTask: function (btn) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId

        var view = btn.up("wfrouteview");
        var textarea = view.down("textareafield");
        var action = view.config.actiontype;
        

        textarea.removeCls("invalid");
        if (action == "reject") {
            divRef = "progressreject";
            var reason = textarea.value;
            if (reason == "") {
                textarea.addCls('invalid');
                var message = CDI.service.Translate.data["common-question-comment-alert"];
                CDI.service.erdaAction.showToastError(message);
                return;
            }
        }

        btn.disable();
        var progBar = view.down("progressbar");
        progBar.wait({
            //duration: 500000,
            interval:10,
            increment: 100,
            text: CDI.service.Translate.data["common-saving"],
            scope: this,
            fn: function () {
                progBar.updateText(CDI.service.Translate.data["completed-successfully"]);
            }
        });
        //progBar.updateProgress(0.3);
        setTimeout(function () {
            var config = view.config;
            config.reason = reason;
            var backCMS = view.down("radiogroup");
            if (backCMS!=null && backCMS.getChecked().length == 1) {
                if (backCMS.getChecked()[0].inputValue == "cms")
                    config.backCMS = true;
            }

            CDI.service.erdaAction.performTask(view.getController(), config, progBar);
        }, 500);
        

    },
    onActionCompleted: function (actionResult) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        //result
        //message

    },

    donewWithError: function (message) {
        
        var view = this.getView();
        view.result = false;
        view.message = message;
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-donewitherror"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
			try {
                if (parentView && data != null)
                    parentView.getController().doRefresh(data,false);
            }
            catch (err) { };
        }, 500);
    },

    doneSuccess: function (data, closeFlag) {
        var view = this.getView();
        var progBar = view.down("progressbar");
        
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-successfully"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var parentView = Ext.getCmp(view.config.viewId);
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
            try {
                if (parentView && data != null)
                    parentView.getController().doRefresh(data,true);
            }
            catch (err) { };
        }, 800)
        
    }
});