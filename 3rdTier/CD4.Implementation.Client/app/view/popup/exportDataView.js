﻿Ext.define('CDI.view.popup.exportDataView', {});

Ext.deferDefine('CDI.view.popup.exportDataView', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'exportdataview',
        controller: 'exportdataviewcontroller',
        
        title: false,
        buttonAlign: 'center',
        listeners: {
            beforerender: 'onBeforeRender',
        },
        
        cls:'modal-confirm',
        config: {
            //actiontype: actionType,
            //businessDataId: itemId,
            //businessDataName: itemNumber,
            //viewId: viewId
        },
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        bodyPadding: 10,

        defaults: {
            frame: false,
            //bodyPadding: 10
            listeners: {
                click: 'onButtonActionClick',
                element: 'el',
                delegate: 'div.modal-footer'
            },
        },

        defaults: {
            //cls: 'modal-content',
            xtype: 'label',
        },
        items: [
            {
                cls: 'modal-header',
                html: '<div class="icon-box export"><i class="fas fa-file-excel"></i></div>'
            },
            {
                cls: 'modal-title',
                text: CDI.service.Translate.data['common-question-exportdata']
            },
            {
                cls: 'modal-body',
                html: CDI.service.Translate.data['common-question-export-message']
            },
            {
                
                cls: 'modal-body export',
                xtype: 'progressbar',
                text:'',
                visibility:false,
            },
            //{
            //    cls: 'modal-body',
            //    xtype: 'textareafield',
            //    emptyText: CDI.service.Translate.data['common-question-comment-approve']
            //}
            
        ],

        buttons: [
            {
                cls: 'btn btn-info',
                text: CDI.service.Translate.data['common-wfaction-cancel'],
                handler: 'onButtonCloseWindown'
            },
            {
                cls: 'btn btn-export',//btn-danger
                text: CDI.service.Translate.data['common-label-export'],
                handler: 'onButtonPerformTask'
            }
        ],
        
        
        
        
    }
});