﻿Ext.define('CDI.view.popup.updatePriceListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.updatepricelistcontroller',

    onBeforeRender: function (view) {
    },
    
    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.close();
            win.destroy();
        }
    },

    onButtonPerformTask: function (btn) {
        var view = btn.up("updatepricelist");
        var fileInput = view.down("#fileinput");
        var file = document.getElementById(fileInput.button.fileInputEl.id);
        if (file != null && file.files != null && file.files.length > 0) {

            var progBar = view.down("progressbar");
            btn.disable();
            progBar.wait({
                interval: 10,
                increment: 100,
                text: CDI.service.Translate.data["supplier-importing-data"],
                scope: this,
                fn: function () {
                    progBar.updateText(CDI.service.Translate.data["supplier-imported-data"]);
                }
            });
            setTimeout(function () {
                var fileToUpload = file.files[0];
                var reader = new FileReader();
                var records = [];
                reader.onload = function (e) {
                    var fileData = {
                        filename: fileToUpload.name,
                        filecontent: e.target.result,
                    };
                    CDI.service.erdaAction.importData(view.getController(), fileData, progBar);
                };
                reader.readAsDataURL(fileToUpload);

            }, 500);
        }
        else
            CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-export-title'], "Select file to import",null);

        
        

    },
    onActionCompleted: function (actionResult) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        //result
        //message

    },

    donewWithError: function (message) {
        
        var view = this.getView();
        view.result = false;
        view.message = message;
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-donewitherror"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
        }, 500);
    },

    doneSuccess: function (data, closeFlag) {
        var view = this.getView();
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-successfully"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var parentView = Ext.getCmp(view.config.viewId);
            parentView.fireEvent("beforerender", parentView);
            var win = view.up("window");
            setTimeout(function () {
                if (win) {
                    win.close();
                    win.destroy();
                }
            }, 100);
            
            
            
        }, 800)
        
    }
});