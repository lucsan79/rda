﻿Ext.define('CDI.view.popup.changeCostViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.changecostviewcontroller',

    onBeforeRender: function (view) {
        var editPanel = view;
        var data = editPanel.config.data;
        if (data.cbs.toLowerCase() == "materials") {
            editPanel.down("#rdhours").hide();
            editPanel.down("#materials").show();
            editPanel.down("#professionalrole").hide();
            editPanel.up().height = "375px";
        }
        else {
            view.down("#rdhours").show();
            view.down("#materials").hide();
        }
        if (data.requestedsupplier == "REMEDY") {
            editPanel.down("#amount").disable();
            editPanel.down("#amountformat").disable();
        }

        $.each(data, function (key, value) {

            if (value === undefined || value === null)
                value = "";
            var item = editPanel.down("#" + key);


            if (item != null && value != "") {
                //item.reset();
                //item.resetOriginalValue();
                switch (item.getXType()) {
                    case 'datefield':
                        item.setValue(new Date(value))
                        break;
                    case 'erdacurrencyfield':
                        item.currency = "€";
                        if (value === undefined || value == "") {
                            if (key == "amount" || key == "pieceprice")
                                value = "0";
                        }
                        value = value.toString().replace(".", ",");
                        if (key == "amount" || key == "dailycost") {
                            if (data.amountformat == "dollar")
                                item.currency = "$";
                        } else if (key == "pieceprice")
                            if (data.piecepriceformat == "dollar")
                                item.currency = "$";
                        item.setValue(value);
                        item.fireEvent('blur', item);
                        break;
                    default:
                        item.setValue(value);
                        break;
                }
                //item.resetOriginalValue();
            }

        });


       
    },
    
    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.reloadOnClose = false;
            win.close();
            win.destroy();
        }
    },

    onCompleteClick: function (btn) {
        var editPanel = btn.up("panel");
        var data = editPanel.config.data;
        var url = "erda/costs";

        $.each(data, function (key, value) {
            
            if (value === undefined || value === null)
                value = "";
            var item = editPanel.down("#" + key);
            if (item != null) {
                Reflect.set(data, key, item.getValue());
            }
        });
        editPanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: CD.apiUrl + url,
            method: 'POST',
            jsonData: data,
            withCredentials: true,
            failure: function (response) {
                editPanel.setLoading(false);
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-saving-title'], "", response);
            },
            success: function (jsonData) {
                editPanel.setLoading(false);
                var win = editPanel.up("window");
                win.reloadOnClose = true;
                if (win) {
                    win.close();
                    win.destroy();
                }
                var message = '<i class="fa fa-check"></i> ' + CDI.service.Translate.data['completed-cost-successfully'];
                CDI.service.erdaAction.showToastMessage(message);
                

            }
        });
    },
    

  
});