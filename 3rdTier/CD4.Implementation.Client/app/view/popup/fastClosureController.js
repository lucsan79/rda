﻿Ext.define('CDI.view.popup.fastClosureController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.fastclosurecontroller',

    onBeforeRender: function (view) {
    },
    
    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.close();
            win.destroy();
        }
    },

    onButtonPerformTask: function (btn) {
        var view = btn.up("fastclosure");
        var validForm = view.isValid();
        if (validForm) {
            var fileInput = view.down("#fileinput");
            console.log(fileInput);
            if (fileInput) {
                var baan = fileInput.getValue();
                var progBar = view.down("progressbar");
                btn.disable();
                progBar.wait({
                    interval: 10,
                    increment: 100,
                    text: CDI.service.Translate.data["erda-closing-data"],
                    scope: this,
                    fn: function () {
                        progBar.updateText(CDI.service.Translate.data["erda-closed-data"]);
                    }
                });
                setTimeout(function () {
                    view.config.actiontype = "close";
                    view.config.baancode = fileInput.getValue();
                    CDI.service.erdaAction.sendeRdA(view.getController(), view.config, progBar);
                }, 500);
            }
            
        }
        
        
        

    },
    onActionCompleted: function (actionResult) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        //result
        //message

    },

    donewWithError: function (message) {
        
        var view = this.getView();
        view.result = false;
        view.message = message;
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-donewitherror"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
        }, 500);
    },

    doneSuccess: function (data, closeFlag) {
        var view = this.getView();
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-successfully"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var parentView = Ext.getCmp(view.config.viewId);
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
            try {
                if (parentView && data != null)
                    parentView.getController().doRefresh(data);
            }
            catch (err) { };
            
        }, 800)
        
    }
});