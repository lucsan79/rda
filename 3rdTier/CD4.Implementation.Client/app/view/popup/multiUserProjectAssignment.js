﻿Ext.define('CDI.view.popup.multiUserProjectAssignment', {});

Ext.deferDefine('CDI.view.popup.multiUserProjectAssignment', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'multiuserprojectassignment',
        controller: 'multiuserprojectassignmentcontroller',
        listeners: {
            beforerender: 'onBeforeRender',
        },
       // cls: 'rdahomeview',
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        parentView:null,
        defaults: {
            flex:1,
            frame: true,
            bodyPadding: 0
        },
       
        items: [
            {
                itemId: 'projectList',
                xtype:'rolegridsimply'
                //html:'test1'
            },
            {
                itemId: 'pm',
                xtype: 'rolegridsimply'
                //html: 'test2'
            }
        ],
        buttons: [
            {
                cls: 'btn btn-info',
                text: CDI.service.Translate.data['common-wfaction-cancel'],
                handler: 'onButtonCloseWindown'
            },
            {
                cls: 'btn btn-success',//btn-danger
                text: CDI.service.Translate.data['overview-filter-button-apply'],
                handler: 'onButtonPerformTask'
            }
        ],
    }
});