﻿Ext.define('CDI.view.popup.sendToBaanView', {});

Ext.deferDefine('CDI.view.popup.sendToBaanView', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'sendtobaanview',
        controller: 'sendtobaanviewcontroller',
        
        title: false,
        buttonAlign: 'center',
        listeners: {
            beforerender: 'onBeforeRender',
        },
        
        cls:'modal-confirm',
        config: {
            //actiontype: actionType,
            //businessDataId: itemId,
            //businessDataName: itemNumber,
            //viewId: viewId
        },
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        bodyPadding: 10,

        defaults: {
            frame: false,
            //bodyPadding: 10
            listeners: {
                click: 'onButtonActionClick',
                element: 'el',
                delegate: 'div.modal-footer'
            },
        },

        defaults: {
            //cls: 'modal-content',
            xtype: 'label',
        },
        items: [
            {
                cls: 'modal-header',
                html: '<div class="icon-box share"><i class="fas fa-share-alt"></i></div>'
            },
            {
                cls: 'modal-title',
                text: CDI.service.Translate.data['common-question-areyousure']
            },
            {
                cls: 'modal-body',
                html: CDI.service.Translate.data['common-question-sendbaan-message']
            },
            {
                
                cls: 'modal-body share',
                xtype: 'progressbar',
                text:'',
                visibility:false,
            },
            //{
            //    cls: 'modal-body',
            //    xtype: 'textareafield',
            //    emptyText: CDI.service.Translate.data['common-question-comment-approve']
            //}
            
        ],

        buttons: [
            {
                cls: 'btn btn-info',
                text: CDI.service.Translate.data['common-wfaction-cancel'],
                handler: 'onButtonCloseWindown'
            },
            {
                cls: 'btn btn-share',//btn-danger
                text: CDI.service.Translate.data['common-baan-sendcommand'],
                handler: 'onButtonPerformTask'
            }
        ],
        
        
        
        
    }
});