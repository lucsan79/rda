﻿Ext.define('CDI.view.popup.updatePriceList', {});

Ext.deferDefine('CDI.view.popup.updatePriceList', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'updatepricelist',
        controller: 'updatepricelistcontroller',
        
        title: false,
        buttonAlign: 'center',
        listeners: {
            beforerender: 'onBeforeRender',
        },
        
        cls:'modal-confirm',
        config: {
            //actiontype: actionType,
            //businessDataId: itemId,
            //businessDataName: itemNumber,
            //viewId: viewId
        },
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        bodyPadding: 10,

        defaults: {
            frame: false,
            //bodyPadding: 10
            listeners: {
                click: 'onButtonActionClick',
                element: 'el',
                delegate: 'div.modal-footer'
            },
        },

        defaults: {
            //cls: 'modal-content',
            xtype: 'label',
        },
        items: [
            {
                cls: 'modal-header',
                html: '<div class="icon-box export"><i class="fas fa-save"></i></div>'
            },
            {
                cls: 'modal-title',
                text: CDI.service.Translate.data['common-question-updatedata']
            },
            {
                cls: 'modal-body',
                html: CDI.service.Translate.data['common-question-update-message']
            },
            {
                cls: 'modal-label',
                html: CDI.service.Translate.data['supplier-pricelist-labelfile']
            },
            {
                xtype: 'filefield',
                itemId: 'fileinput',
                fieldLabel: null,
                labelWidth: 100,
                msgTarget: 'side',
                allowBlank: false,
                anchor: '100%',
               
                buttonText: "...",
                listeners: {
                    afterrender: function (cmp) {
                        cmp.fileInputEl.set({
                            accept: 'xlsx/*'
                        });
                    },
                    change: function (fld, value) {
                        var newValue = value.replace(/C:\\fakepath\\/g, '');
                        fld.setRawValue(newValue);
                    }
                },
                regex: /(.)+((\.xlsx)(\w)?)$/i,
                regexText: 'Only Excel format is accepted'
            },
            {
                
                cls: 'modal-body export',
                xtype: 'progressbar',
                text:'',
                visibility:false,
            }
        ],

        buttons: [
            {
                cls: 'btn btn-info',
                text: CDI.service.Translate.data['common-wfaction-cancel'],
                handler: 'onButtonCloseWindown'
            },
            {
                cls: 'btn btn-export',//btn-danger
                text: CDI.service.Translate.data['common-label-update'],
                handler: 'onButtonPerformTask'
            }
        ],
        
        
        
        
    }
});