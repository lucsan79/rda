﻿Ext.define('CDI.view.popup.wfRouteView', {});

Ext.deferDefine('CDI.view.popup.wfRouteView', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'wfrouteview',
        controller: 'wfrouteviewcontroller',
        
        title: false,
        buttonAlign: 'center',
        listeners: {
            beforerender: 'onBeforeRender',
        },
        
        cls:'modal-confirm',
        config: {
            //actiontype: actionType,
            //businessDataId: itemId,
            //businessDataName: itemNumber,
            //viewId: viewId
        },
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        bodyPadding: 10,

        defaults: {
            frame: false,
            //bodyPadding: 10
            listeners: {
                click: 'onButtonActionClick',
                element: 'el',
                delegate: 'div.modal-footer'
            },
        },

        defaults: {
            //cls: 'modal-content',
            xtype: 'label',
        },
        items: [
            {
                cls: 'modal-header',
                html: '<div class="icon-box approve"><i class="fas fa-check"></i></div>'
            },
            {
                cls: 'modal-title',
                text: CDI.service.Translate.data['common-question-areyousure']
            },
            {
                cls: 'modal-body',
                html: CDI.service.Translate.data['common-question-wfapprove-message']
            },
            {
                
                cls: 'modal-body approve',
                xtype: 'progressbar',
                text:'',
                visibility:false,
            },
            
            {
                cls: 'modal-body',
                xtype: 'textareafield',
                emptyText: CDI.service.Translate.data['common-question-comment-approve']
            },
            {
                xtype: 'radiogroup',
                visibility: false,
                cls:'wfapprover',
                fieldLabel: CDI.service.Translate.data['common-workflow-to'],
                items: [
                    { boxLabel: 'Requester', inputValue: 'requester',checked:true },
                    { boxLabel: 'CMS', inputValue: 'cms' },
                ]
            },
            
        ],

        buttons: [
            {
                cls: 'btn btn-info',
                text: CDI.service.Translate.data['common-wfaction-cancel'],
                handler: 'onButtonCloseWindown'
            },
            {
                cls: 'btn btn-success',//btn-danger
                text: CDI.service.Translate.data['common-wfaction-approve'],
                handler: 'onButtonPerformTask'
            }
        ],
        
        
        
        
    }
});