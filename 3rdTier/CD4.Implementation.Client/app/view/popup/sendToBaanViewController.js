﻿Ext.define('CDI.view.popup.sendToBaanViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sendtobaanviewcontroller',

    onBeforeRender: function (view) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
     
        var actionType = view.config.actiontype;
        var items = view.items;
        if (Array.isArray(view.config.businessDataId)) {
            items.items[3].text = "[" + view.config.businessDataId.length + " " + CDI.service.Translate.data["rda-selected"] + "]"
        }
        else
            items.items[3].text = view.config.businessDataName;

        if (actionType == "reject") {
            items.items[0].html = items.items[0].html.replace("approve", "reject");
            items.items[2].html = CDI.service.Translate.data["common-question-wfreject-message"];
            items.items[3].cls = 'modal-body ' + actionType;
            items.items[4].emptyText = CDI.service.Translate.data["common-question-comment-reject"];
            var buttons = view.query("button");
            if (buttons != null && buttons.length==2) {
                buttons[1].cls='btn btn-danger';
                buttons[1].text = CDI.service.Translate.data['common-wfaction-reject'];
            }

        }
        
    },
    
    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.close();
            win.destroy();
        }
    },

    onButtonPerformTask: function (btn) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        var view = btn.up("sendtobaanview");
        var action = view.config.actiontype;
        var progBar = view.down("progressbar");
        btn.disable();
        progBar.wait({
            //duration: 500000,
            interval:10,
            increment: 100,
            text: CDI.service.Translate.data["common-sendingbaan"],
            scope: this,
            fn: function () {
                progBar.updateText(CDI.service.Translate.data["completed-successfully"]);
            }
        });
        //progBar.updateProgress(0.3);
        setTimeout(function () {
            CDI.service.erdaAction.sendeRdA(view.getController(), view.config, progBar);
        }, 500);
        

    },
    onActionCompleted: function (actionResult) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        //result
        //message

    },

    donewWithError: function (message) {
        
        var view = this.getView();
        view.result = false;
        view.message = message;
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-donewitherror"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
        }, 500);
    },

    doneSuccess: function (data, closeFlag) {
        var view = this.getView();
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-successfully"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var parentView = Ext.getCmp(view.config.viewId);
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
            try {
                if (parentView && data != null)
                    parentView.getController().doRefresh(data);
            }
            catch (err) { };
        }, 800)
        
    }
});