﻿Ext.define('CDI.view.popup.sendMaterialsView', {});

Ext.deferDefine('CDI.view.popup.sendMaterialsView', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'sendmaterialsview',
        controller: 'sendmaterialsviewcontroller',
        
        title: false,
        buttonAlign: 'center',
        listeners: {
            beforerender: 'onBeforeRender',
        },
        
        cls:'modal-confirm',
        config: {
            //actiontype: actionType,
            //businessDataId: itemId,
            //businessDataName: itemNumber,
            //viewId: viewId
        },
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        bodyPadding: 10,

        defaults: {
            frame: false,
            //bodyPadding: 10
            listeners: {
                click: 'onButtonActionClick',
                element: 'el',
                delegate: 'div.modal-footer'
            },
        },

        defaults: {
            //cls: 'modal-content',
            xtype: 'label',
        },
        items: [
            {
                cls: 'modal-header',
                html: '<div class="icon-box approve"><i class="fas fa-save"></i></div>'
            },
            {
                cls: 'modal-title',
                text: CDI.service.Translate.data['common-question-areyousure']
            },
            {
                cls: 'modal-body',
                html: CDI.service.Translate.data['common-question-material-message']
            },
            {
                
                cls: 'modal-body approve',
                xtype: 'progressbar',
                text:'',
                visibility:false,
            },
            //{
            //    cls: 'modal-body',
            //    xtype: 'textareafield',
            //    emptyText: CDI.service.Translate.data['common-question-comment-approve']
            //}
            
        ],

        buttons: [
            {
                cls: 'btn btn-info',
                text: CDI.service.Translate.data['common-wfaction-cancel'],
                handler: 'onButtonCloseWindown'
            },
            {
                cls: 'btn btn-success',//btn-danger
                text: CDI.service.Translate.data['common-materials-approve'],
                handler: 'onButtonPerformTask'
            }
        ],
        
        
        
        
    }
});