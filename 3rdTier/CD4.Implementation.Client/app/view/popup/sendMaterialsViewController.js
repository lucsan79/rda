﻿Ext.define('CDI.view.popup.sendMaterialsViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sendmaterialsviewcontroller',

    onBeforeRender: function (view) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId

        
        var items = view.items;
        items.items[3].text = "[" + view.config.rows.length + " " + CDI.service.Translate.data["material-selected-tocreate"] + "]"
        
    },
    
    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.close();
            win.destroy();
        }
    },

    onButtonPerformTask: function (btn) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        var view = btn.up("sendmaterialsview");
        var action = view.config.actiontype;
        var progBar = view.down("progressbar");
        btn.disable();
        progBar.wait({
            //duration: 500000,
            interval:10,
            increment: 100,
            text: CDI.service.Translate.data["common-materials-send"],
            scope: this,
            fn: function () {
                progBar.updateText(CDI.service.Translate.data["completed-successfully"]);
            }
        });
        //progBar.updateProgress(0.3);
        setTimeout(function () {

            CDI.service.erdaAction.creatematerials(view.getController(), view.config, progBar);
        }, 500);
        

    },
    onActionCompleted: function (actionResult) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        //result
        //message

    },

    donewWithError: function (message) {
        
        var view = this.getView();
        view.result = false;
        view.message = message;
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-donewitherror"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var win = this.getView().up("window");
            if (win) {
                win.close();
                win.destroy();
            }
        }, 500);
    },

    doneSuccess: function (data, closeFlag) {
        var view = this.getView();
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-successfully"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var parentView = Ext.getCmp(view.config.viewId);
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
            try {
                if (parentView && data != null)
                    parentView.getController().doRefresh(data);
            }
            catch (err) { };
        }, 800)
        
    }
});