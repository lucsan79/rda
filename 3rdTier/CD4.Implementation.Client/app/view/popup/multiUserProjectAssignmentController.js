﻿Ext.define('CDI.view.popup.multiUserProjectAssignmentController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.multiuserprojectassignmentcontroller',

    onBeforeRender: function (view) {
        var gridProject = view.down("#projectList");
        var gridusers = view.down("#pm");
        gridProject.columns[0].setText(CDI.service.Translate.data["dashboard-rda-projectlist-label"]);
        gridusers.columns[0].setText(CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pm"]);

        gridProject.setLoading(true);
        gridusers.setLoading(true);

        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: CD.apiUrl + 'erda/admin/getProjectRoleUserItems',
            jsonData: {
                role: '',
                target: 'projects',
            },
            method: 'POST',
            withCredentials: true,
            failure: function (response) {
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                gridProject.setLoading(false);
            },
            success: function (jsonData) {
                var data = Ext.decode(jsonData.responseText);
                var storeObject = {
                    data: []
                };
                if (data != null && data.length > 0)
                    storeObject.data = data;
                gridProject.setStore(Ext.create('Ext.data.Store', storeObject));
                gridProject.setLoading(false);
            }
        });


        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: CD.apiUrl + 'erda/admin/getProjectRoleUserItems',
            jsonData: {
                role: '',
                target: 'users',
            },
            method: 'POST',
            withCredentials: true,
            failure: function (response) {
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                gridusers.setLoading(false);
            },
            success: function (jsonData) {
                var data = Ext.decode(jsonData.responseText);
                var storeObject = {
                    data: []
                };
                if (data != null && data.length > 0)
                    storeObject.data = data;
                gridusers.setStore(Ext.create('Ext.data.Store', storeObject));
                gridusers.setLoading(false);
            }
        });

    },

    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.reloadOnClose = false;
            win.close();
            win.destroy();
        }
    },

    onButtonPerformTask: function () {
        var self = this;
        var gridProject = this.view.down("#projectList");
        var gridusers = this.view.down("#pm");
        var selectionP = gridProject.getView().getSelectionModel().getSelection();
        var selectionU = gridusers.getView().getSelectionModel().getSelection();
        var dataP = [];
        var dataU = [];
        if (selectionP != null && selectionP.length > 0) {
            for (var i = 0; i < selectionP.length; i++)
                dataP.push(selectionP[i].data.item);
        }
        if (selectionU != null && selectionU.length > 0) {
            for (var i = 0; i < selectionU.length; i++)
                dataU.push(selectionU[i].data.item);
        }
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: CD.apiUrl + 'erda/admin/AssignUserProjectRow',
            jsonData: {
                role:'PM',
                projects: dataP,
                users: dataU,
            },
            method: 'POST',
            withCredentials: true,
            failure: function (response) {
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
            },
            success: function () {
                var win = self.getView().up("window");
                win.reloadOnClose = true;
                if (win) {
                    var parentView = self.getView().parentView;
                    var grid = parentView.query("grid")[0];
                    grid.fireEvent("beforeRender", grid);
                    win.close();
                    win.destroy();
                }
                
                var message = '<i class="fa fa-check"></i> ' + CDI.service.Translate.data['completed-successfully'];
                CDI.service.erdaAction.showToastMessage(message);

            }
        });
       


    }


  
});