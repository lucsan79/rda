﻿Ext.define('CDI.view.popup.exportDataViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.exportdataviewcontroller',

    onBeforeRender: function (view) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        
        var options = view.config.options;
        var exportType = view.config.exporttype;
        var title = CDI.service.Translate.data["ready-to-start"];
        if (options != null && options.title != "")
            title = options.title;
        var items = view.items;
        items.items[3].text = title;
        
    },
    
    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.close();
            win.destroy();
        }
    },

    onButtonPerformTask: function (btn) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        var view = btn.up("exportdataview");
        var progBar = view.down("progressbar");
        btn.disable();
        progBar.wait({
            //duration: 500000,
            interval:10,
            increment: 100,
            text: CDI.service.Translate.data["common-exporting-data"],
            scope: this,
            fn: function () {
                progBar.updateText(CDI.service.Translate.data["common-exported-data"]);
            }
        });
        //progBar.updateProgress(0.3);
        setTimeout(function () {
            CDI.service.erdaAction.exportdata(view.getController(), view.config, progBar);
        }, 500);
        

    },
    onActionCompleted: function (actionResult) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        //result
        //message

    },

    donewWithError: function (message) {
        
        var view = this.getView();
        view.result = false;
        view.message = message;
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-donewitherror"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
        }, 500);
    },

    doneSuccess: function (data, closeFlag) {
        var view = this.getView();
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-successfully"]);
        progBar.stopAnimation();
        setTimeout(function () {
            var parentView = Ext.getCmp(view.config.viewId);
            var win = view.up("window");
            var fileUrl = CD.apiUrl + "erda/export/download/" + data;
            var winDownload = window.open(fileUrl, '_blank');
            winDownload.focus();
            setTimeout(function () {
                if (win) {
                    win.close();
                    win.destroy();
                }
            }, 100);
            
            
            
        }, 800)
        
    }
});