﻿Ext.define('CDI.view.dashboard.archive.data.gridViewController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.archivedatacontroller',

        onBeforeRender: function (grid) {
            var project = grid.config.project;
            var pagingtoolbar = grid.down("pagingtoolbar");
            pagingtoolbar.down('#refresh').hide();

            grid.up().setLoading(true);
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/user/getarchivedlist?project=' + project,
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
                    //console.log('Errore: ', response);
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    grid.up().setLoading(false);
                }
            }).then(function (jsonData) {
                var data = Ext.decode(jsonData.responseText);
                var oldStore = grid.getStore();
                //if (oldStore != null && oldStore.data != null) oldStore.data.destroy();
                //if (oldStore.data != null && oldStore.data.items.length > 0) {
                //    oldStore.data.removeAll()
                //}

                var rdaStore = Ext.create('CDI.store.rdaSimpleStore', {
                    remoteFilter: true,
                    data: data.rdafullList,
                });
                
                grid.setStore(rdaStore);
                //grid.getView().refresh();
                setTimeout(function () { grid.up().setLoading(false); }, 200);
                
                //grid.setLoading(false);

            });
        },

        onForceRender: function (grid) {
            var project = grid.config.project;
            grid.up().setLoading(true);
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/user/getarchivedlist?project=' + project,
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
                    //console.log('Errore: ', response);
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                     grid.up().setLoading(false);
                }
            }).then(function (jsonData) {
                var data = Ext.decode(jsonData.responseText);
                var oldStore = grid.getStore();
                if (oldStore != null && oldStore.data != null) oldStore.data.destroy();
                var rdaStore = {
                    fields: [],
                    data: [],
                    pageSize: 50,
                    proxy: {
                        type: 'memory',
                        enablePaging: true
                    }

                };
                if (data != null && data.rdafullList != null) {
                    rdaStore.data = data.rdafullList;
                }

                grid.setStore(rdaStore);

                grid.up().setLoading(false);
                //grid.setLoading(false);

            });
        },

        
        onDestroyStore: function (grid) {
            var oldStore = grid.getStore();
            if (oldStore != null && oldStore.data != null) oldStore.data.destroy();
        },


        bindStore: function (data, project,currentgrid) {
            var grid = this.view.up().down("#archivedatagrid");
            if (project === undefined || project === null) project = "";
            //grid.up().setLoading(true);
            
            var title = CDI.service.Translate.data["dashboard-rda-worklist-grid-label"];
            if (project != "")
                title = title + " of <span class='headerProjectLabel'>" + project + '</span>';
            grid.setTitle(title);
            grid.up().setLoading(false);
        },
        cellclick: function (gridview, tdEl, cellIndex, record, trEl, rowIndex, e) {
            if (tdEl.innerHTML.indexOf("rdalinkbutton") != -1)
                CDI.service.erdaAction.open(record.get('eId'));
        }

    });
