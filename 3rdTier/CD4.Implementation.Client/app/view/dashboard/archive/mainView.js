﻿Ext.define('CDI.view.dashboard.archive.mainView', {});
Ext.deferDefine('CDI.view.dashboard.archive.mainView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'archiveview',
        controller: 'archivemainviewwcontroller',
        itemId: 'archiveview',
        cls: 'worklistview',
        layout: 'border',
        listeners: {
            beforerender: 'onBeforeRender',
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                split: true,
                collapsible: true,
                title: CDI.service.Translate.data["dashboard-rda-projectlist-label"],
                iconCls: 'x-fa fa-car',
                region: 'west',
                flex: 1,
                minWidth:350,
                defaults: {
                    flex: 1,
                    xtype: 'grid',
                    frame: true,
                    scrollable: true,
                },
                cls: 'notopborder',
                itemId: 'notopborder',
                items: [
                    {
                        split: true,
                        
                        itemId: 'projectList',
                        cls: 'gridCardList',
                        listeners: {
                            select: 'onComponentSelectRow',
                            deselect: 'onComponentSelectRow'
                        },
                        selModel: {
                            
                            mode: 'SINGLE'
                        },
                        allowDeselect: true,
                        columns: {
                            defaults: {
                                tpl: '<div class="projectCard">' +
                                    '<div> ' +
                                    '<div class="projectTitle">{ProjectName}</div>' +
                                    '<div class="rdastatuslist">' +
                                    '<div class="active_true"><i class="far fa-circle {RdAInWorkingStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-working-label"] + '<span class="rdanumber">#{RdAInWorking}</span></div>' +
                                    '<div class="active_true"><i class="far fa-circle {RdAInValidationStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-prvalidation-label"] + '<span class="rdanumber">#{RdAInValidation}</span></div>' +
                                    '</div>' +
                                    '<div class="rdastatuslist">' +
                                    '<div class="active_true"><i class="far fa-circle {RdAInCMSValidationStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-cmsvalidation-label"] + '<span class="rdanumber">#{RdAInCMSValidation}</span></div>' +
                                    '<div class="active_true"><i class="far fa-circle {RdAInCreatedStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-created-label"] + '<span class="rdanumber">#{RdAInCreated}</span></div>' +
                                    '</div>' +
                                    '</div> ' +
                                    '</div>',
                            },
                            items: [
                                {
                                    xtype: 'templatecolumn',
                                    align: 'left',
                                    flex: 1
                                }]
                        }
                    },
                ]
            },
            {
                title: false,
                region: 'center',
                flex: 4,
                itemId: 'rdaGrid',
                defaults: {
                    flex:1,
                    xtype: 'grid',
                    frame: true,
                    scrollable: true,
                },
                items: [
                    {
                        xtype: 'archivedatagrid',
                       
                    }
                ]
            }
        ]

    }
});