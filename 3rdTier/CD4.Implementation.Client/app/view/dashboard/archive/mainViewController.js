﻿Ext.define('CDI.view.dashboard.archive.mainViewwController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.archivemainviewwcontroller',

        onBeforeRender: function (view) {
            var grid = view.down("#rdaGrid");
            view.setLoading(true);
            grid.setLoading(true);
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/user/getarchivedProjectlist',
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
                    //console.log('Errore: ', response);
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    view.setLoading(false);
                }
            }).then(function (jsonData) {
                try {
                    view.down('#projectList').setEmptyText("<h4 style='margin-top: -2px;'>" + CDI.service.Translate.data['rda-norda-to-consult'] + "</h4>");
                    var data = Ext.decode(jsonData.responseText);
                    data.rdafullList = [];
                    var projectStore = { fields: [], data: [] };
                    if (data.projects != null) {
                        projectStore.data = data.projects;
                    }
                    view.down('#projectList').setStore(projectStore);
                    view.setLoading(false);
                }
                catch (err) {
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], err.message, null);
                    view.setLoading(false);
                }
                

            });
        },
        addBusinessDataTab: function (view, rowIndex, colIndex, item, e, record, row) {
            Ext.fireEvent('addTab', rda.eBusinessDataType + ' ' + rda.eName, 'businessdatawrapper',
                {
                    businessDataType: rda.eBusinessDataType,
                    itemName: rda.eName,
                    businessDataId: rda.eId,
                    description: rda.eDescription,
                    revision: rda.eRevision,
                    level: rda.eLevel,
                    project: rda.eProject,
                    workflow: rda.eWorkflow,
                    owner: rda.eOwner,
                    createUser: rda.eCreateUser
                }, true, null, 'fa-donate');

        },
        
        onComponentSelectRow: function (grid, record) {
            setTimeout(function(){
                var hasSelection = grid.getSelected().length == 0 ? false : true;
                var currentView = grid.view.up("archiveview");
                var projectName = "";
                if (hasSelection == false)
                    projectName = record.data.ProjectName;
                /* recupero griglia */
                var currentGrid = currentView.down("#archivedatagrid");//grid.view.up().up().up();// 
                if (currentGrid) {
                    currentGrid.config.project = "";
                    if (hasSelection)
                        currentGrid.config.project = record.data.ProjectName;
                    //currentGrid.fireEvent('destroyStore', currentGrid);
                    currentGrid.fireEvent('beforerender', currentGrid);
                }
            }, 500);
            
            
        },

       
    });
