﻿Ext.define('CDI.view.dashboard.work.mainView', {});
Ext.deferDefine('CDI.view.dashboard.work.mainView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'workarea',
        controller: 'workcontroller',
        itemId: 'workarea',
        cls: 'worklistview',
        layout: 'border',
        title: '<i class="fa fa-signature"></i> <span class="headerL2">' + CDI.service.Translate.data['dashboard-rda-worklist-grid-label'] + '</span>',
        listeners: {
            beforerender: 'onBeforeRender',
            showView:'onShowView'
        },
        header: {
            style: {
                'padding': '5px 20px 5px 15px!important',
            },
            items: [

                {
                    xtype: 'segmentedbutton',
                    cls: 'subheadermenu',
                    listeners: {
                        toggle: 'onToggleButtonClick'
                    },
                    allowToggle: true,
                    defaults: {
                        width: 150,
                    },
                    items: [
                        {
                            cls: 'btn-toolbar-left',
                            iconCls: 'x-fa fa-signature',
                            viewId: 'rdatosign',
                            itemId: 'rdatosign',
                            text: CDI.service.Translate.data['dashboard-rda-worklist-grid-label'],
                            pressed: true,
                            options: {
                                url: "erda/user/getworklist",
                                enableFullView: false,
                                urlfullview: "",
                                allowSelection: true,
                                columnhide: ['Notes', 'CodiceIman','CO_SP', ]
                            }
                        },
                        {
                            cls: 'btn-toolbar-middle',
                            iconCls: 'x-fa fa-bolt',
                            viewId: 'rdainfastmaterial',
                            itemId: 'rdainfastmaterial',
                            text: CDI.service.Translate.data['dashboard-rda-fastmaterialapproval-grid-label'],
                            options: {
                                url: "erda/user/getfastmaterialapproval",
                                enableFullView: false,
                                urlfullview: "",
                                allowSelection: true,
                                columnhide: ['Amount', 'ActivityStart','ActivityEnd']
                            }

                        },
                        {
                            cls: 'btn-toolbar-middle',
                            iconCls: 'x-fa fa-cubes',
                            viewId: 'rdamulticreation',
                            itemId: 'rdamulticreation',
                            text: CDI.service.Translate.data['dashboard-rda-list-button-multicreation'],
                            options: {
                                url: "erda/user/getmultimaterial",
                                enableFullView: false,
                                urlfullview: "",
                                allowSelection: false,
                                columnhide: ['Amount', 'ActivityStart', 'ActivityEnd', 'BaanIn_ATC', 'Supplier', 'CodiceIman','CO_SP',]
                            }
                        },
                        {

                            cls: 'btn-toolbar-right',
                            iconCls: 'fas fa-user-check',
                            viewId: 'rdasigned',
                            itemId: 'rdasigned',
                            text: CDI.service.Translate.data['dashboard-rda-rdasigned-grid-label'],
                            options: {
                                url: "erda/user/getsignedrda",
                                enableFullView: false,
                                urlfullview: "erda/user/getfullsignedrda",
                                allowSelection: false,
                                columnhide: ['Notes']
                            }
                        }

                    ]
                },
            ]
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                split: true,
                collapsible: true,
                title: CDI.service.Translate.data["dashboard-rda-projectlist-label"],
                iconCls: 'x-fa fa-car',
                region: 'west',
                flex: 1,
                minWidth: 350,
                defaults: {
                    flex: 1,
                    frame: true,
                },
                cls: 'notopborder',
                itemId: 'projectPanelView',
                items: [
                    {
                        xtype:'projectgrid'
                    },
                ]
            },
            {
                region: 'center',
                flex: 4,
                itemId: 'objectPanelView',
                defaults: {
                    flex: 1,
                    frame: true,
                    scrollable: true,
                    cls:'basicgrid column-header-major'
                },
                items: [
                    {
                        xtype: 'objectsgrid'

                    }
                ]
            }
        ]

        

    }
});