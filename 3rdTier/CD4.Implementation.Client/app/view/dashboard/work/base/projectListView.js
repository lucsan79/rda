﻿Ext.define('CDI.view.dashboard.work.base.projectListView', {});
Ext.deferDefine('CDI.view.dashboard.work.base.projectListView', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'projectgrid',
        controller: 'workcontroller',
        itemId: 'projectgrid',
        cls: 'gridCardList',

        listeners: {
            select: 'onProjectSelect',
            deselect: 'onProjectDeselect'
        },
        scrollable: true,
        selModel: {
            mode: 'SINGLE'
        },
        allowDeselect: true,
        columns: {
            defaults: {
                tpl: '<div class="projectCard">' +
                    '<div> ' +
                    '<div class="projectTitle">{ProjectName}</div>' +
                    '<div class="rdastatuslist">' +
                    '<div class="active_true"><i class="far fa-circle {RdAInWorkingStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-working-label"] + '<span class="rdanumber">#{RdAInWorking}</span></div>' +
                    '<div class="active_true"><i class="far fa-circle {RdAInValidationStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-prvalidation-label"] + '<span class="rdanumber">#{RdAInValidation}</span></div>' +
                    '</div>' +
                    '<div class="rdastatuslist">' +
                    '<div class="active_true"><i class="far fa-circle {RdAInCMSValidationStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-cmsvalidation-label"] + '<span class="rdanumber">#{RdAInCMSValidation}</span></div>' +
                    '<div class="active_true"><i class="far fa-circle {RdAInCreatedStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-created-label"] + '<span class="rdanumber">#{RdAInCreated}</span></div>' +
                    '</div>' +
                    '</div> ' +
                    '</div>',
            },
            items: [
                {
                    xtype: 'templatecolumn',
                    align: 'left',
                    flex: 1
                }]
        }

    }
});

