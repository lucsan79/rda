﻿Ext.define('CDI.view.dashboard.work.mainViewController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.workcontroller',

        onBeforeRender: function (view) {
           
            var btnPressed = null;
            var buttons = null;
            if (this.view.header.xtype === undefined)
                buttons = this.view.header.items[0].items;
            else
                buttons = this.view.header.down("segmentedbutton").items.items;

            if (buttons != null && buttons.length > 0) {
                for (var i = 0; i < buttons.length; i++) {
                    if (buttons[i].pressed) {
                        btnPressed = buttons[i];
                        break;
                    }

                }
                CDI.service.erdaAction.SetPanelVisibility(buttons);
            }
            if (btnPressed != null)
                this.onShowView(view, btnPressed.viewId, btnPressed.options);

           
            


        },

        onToggleButtonClick: function (container, button, pressed) {
            
            var view = container.up("workarea");
            view.getController().onShowView(view, button.viewId, button.options);
        },

        onShowView: function (view, viewToShow, options) {
            
            var projectPanel = view.down("#projectPanelView");
            var objectPanel = view.down("#objectPanelView");
            var projectGrid = projectPanel.down("grid");
            var objectGrid = objectPanel.down("grid");
            var pagingtoolbar = objectPanel.down("pagingtoolbar");
            pagingtoolbar.down("#btnApprove").disable();
            pagingtoolbar.down("#btnReject").disable();
            pagingtoolbar.down("#btnATC").getController().setreadonly(true);
            
            var url = CD.apiUrl + options.url;

            projectPanel.up().setLoading(true);
            objectPanel.setLoading(true);

            var projectStore = { fields: [], data: [] };

            
            projectGrid.setEmptyText("<h4 style='margin-top: -2px;'>" + CDI.service.Translate.data['rda-norda-to-manage'] + "</h4>");
            projectGrid.setStore(projectStore);
            objectGrid.setStore(projectStore);
            objectGrid.filters.clearFilters();
            

            Ext.Ajax.request({
                url: url,
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
                    view.setLoading(false);
                    var exceptionMessage = Ext.decode(response.responseText).ExceptionMessage;
                    projectPanel.up().setLoading(false);
                    objectPanel.setLoading(false);
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['rda-generic-error'], exceptionMessage);

                },
            }).then(function (jsonData) {
                var data = Ext.decode(jsonData.responseText);

                projectStore = {
                    data: data.projects
                }
                var objectStore = Ext.create('CDI.store.rdaSimpleStore', {
                    data: data.rdafullList,
                    totalCount: data.rdafullList.length
                });

                projectGrid.setStore(projectStore);
                objectGrid.setStore(objectStore);
                objectGrid.mainStore = data.rdafullList;

                var columnSelection = objectGrid.headerCt.down("[isCheckerHd]");
                var pagingtoolbar = objectGrid.view.up().up().down("pagingtoolbar");
                var btn1 = pagingtoolbar.down("#btnApprove");
                var btn2 = pagingtoolbar.down("#btnReject");
                var btn3 = pagingtoolbar.down("#btnATC");
                if (columnSelection && !options.allowSelection) {
                    columnSelection.hide();
                    btn1.hide();
                    btn2.hide();
                    btn3.hide();
                }
                else {
                    columnSelection.show();
                    btn1.show();
                    btn2.show();
                    btn3.show();
                }

                var columns = objectGrid.getColumns();
                for (var i = 0; i < columns.length; i++) {
                    var column = columns[i];
                    var contains = false;
                    if (options.columnhide != null && column.dataIndex && options.columnhide.length > 0) {
                        for (var y = 0; y < options.columnhide.length; y++) {
                            if (options.columnhide[y].toLowerCase() == column.dataIndex.toLowerCase())
                                contains = true;
                        }
                    }
                    if(column.dataIndex)
                        column.setVisible(!contains);
                }
                projectPanel.up().setLoading(false);
                objectPanel.setLoading(false);
                
            });
            //view.up().up().setLoading(false);



        },

        onShowProjetObjtects: function (view, givenProject) {
            var objectPanel = view.down("#objectPanelView");
            var objectGrid = objectPanel.down("grid");
            var pagingtoolbar = objectPanel.down("pagingtoolbar");
            objectPanel.setLoading(true);
            setTimeout(function () {

                pagingtoolbar.down("#btnApprove").disable();
                pagingtoolbar.down("#btnReject").disable();
                pagingtoolbar.down("#btnATC").getController().setreadonly(true);
                var backStore = [];
                if (givenProject != null) {
                    if (objectGrid.mainStore != null) {
                        for (var i = 0; i < objectGrid.mainStore.length; i++) {
                            if (objectGrid.mainStore[i].eProject === givenProject) {
                                backStore.push(objectGrid.mainStore[i]);
                            }
                        }
                    }
                }
                else
                    backStore = objectGrid.mainStore;

                var objectStore = Ext.create('CDI.store.rdaSimpleStore', {
                    data: backStore,
                    totalCount: backStore.length
                });
                objectGrid.filters.clearFilters();
                objectGrid.setStore(objectStore);
                setTimeout(function () {
                    objectPanel.setLoading(false);
                }, 20);
            }, 20);
        },

        onProjectSelect: function (grid, record) {
            var projectName = record.data.ProjectName;
            var view = this.view.up("workarea");
            this.onShowProjetObjtects(view, projectName);
        },

        onProjectDeselect: function (grid, record) {
            var view = this.view.up("workarea");
            this.onShowProjetObjtects(view, null);
        },

        onRowSelect: function (grid, record) {
            var hasSelection = grid.getSelected().length == 0 ? false : true;
            var pagingtoolbar = grid.view.up().up().down("pagingtoolbar");
            var btn1 = pagingtoolbar.down("#btnApprove");
            var btn2 = pagingtoolbar.down("#btnReject");
            var btn3 = pagingtoolbar.down("#btnATC").getController();
            if (hasSelection) { btn1.enable(); btn2.enable(); btn3.setreadonly(false);}
            else if (!hasSelection) { btn1.disable(); btn2.disable(); btn3.setreadonly(true);}
        },

        onRowDeselect: function (grid, record) {
            this.onRowSelect(grid, record);
        },

        onOpenBusinessData: function (gridview, tdEl, cellIndex, record, trEl, rowIndex, e) {
            if (tdEl.innerHTML.indexOf("rdalinkbutton") != -1) {
                CDI.service.erdaAction.open(record.get('eId'));
            }

        },

        onGridBeforeRender: function (view) {
            var pagingtoolbar = view.down("pagingtoolbar");
            if (pagingtoolbar != null) {
                pagingtoolbar.down('#refresh').hide();
            }
        },

        onApproveClick: function (btn) {
            var grid = btn.up("grid");
            var selection = grid.getView().getSelectionModel().getSelection();
            if (selection != null && selection.length > 0) {
                var businessdataId = [];
                var businessdataName = [];
				var businessdataLevel = [];
				
                for (var i = 0; i < selection.length; i++) {
                    businessdataId.push(selection[i].data.eId);
                    businessdataName.push(selection[i].data.eName);
					businessdataLevel.push(selection[i].data.eLevel);
                }
                CDI.service.erdaAction.showWorkflowAction(btn.up("objectsgrid").id, "approve", businessdataId, businessdataName,businessdataLevel);
            }

        },

        onRejectClick: function (btn) {
            var grid = btn.up("grid");
            var selection = grid.getView().getSelectionModel().getSelection();
            if (selection != null && selection.length > 0) {
                var businessdataId = [];
                var businessdataName = [];
				var businessdataLevel = [];
				
                for (var i = 0; i < selection.length; i++) {
                    businessdataId.push(selection[i].data.eId);
                    businessdataName.push(selection[i].data.eName);
					businessdataLevel.push(selection[i].data.eLevel);
                }
                CDI.service.erdaAction.showWorkflowAction(btn.up("objectsgrid").id, "reject", businessdataId, businessdataName,businessdataLevel);
            }

        },

        doRefresh: function (data) {
            var button = this.getView().down("#btnRefresh");
            button.fireEvent("click", button);
        }

    });
