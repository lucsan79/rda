﻿Ext.define('CDI.view.dashboard.common.atcViewController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.atccontroller',

    
    setreadonly: function (flag) {
        if (flag) {
            this.getView().down("textfield").disable();
            this.getView().down("textfield").reset();
            this.getView().down("button").disable();
        }
        else {
            this.getView().down("textfield").enable();
            this.getView().down("button").enable();
        }
    },
    onAssignATC: function () {
        var self = this;
        var view = this.getView();
        var grid = view.up("grid");
        var gridType = grid.getXType();
        var atc = view.down("textfield").getValue();
        if (atc == "")
        {
            Ext.MessageBox.confirm(CDI.service.Translate.data['generic-error-atc-title'], CDI.service.Translate.data['common-question-emptyrda-message'], function (id) {
                if (id === 'yes') {
                    self.onSaveATC();
                }
            }, this);
        }
        else
            self.onSaveATC();
    },

    onSaveATC: function () {
        var view = this.getView();
        var grid = view.up("grid");
        var atc = view.down("textfield").getValue();
        var selection = grid.getView().getSelectionModel().getSelection();
        if (selection != null && selection.length > 0) {
            var businessdataId = [];
            for (var i = 0; i < selection.length; i++) {
                businessdataId.push(selection[i].data.eId);
            }

            var actionATC = {
                ATC: atc,
                businessDataId: businessdataId,
            };

            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/erda/atc',
                method: 'POST',
                jsonData: actionATC,
                withCredentials: true,
                //async: false,
                failure: function (response) {
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-atc-title'], "", response);
                    callbackController.donewWithError();
                },
                success: function (jsonData) {
                    data = Ext.decode(jsonData.responseText);
                    for (var i = 0; i < data.businessDataId.length ; i++) {
                        for (var y = 0; y < selection.length; y++) {
                            if (data.businessDataId[i] == selection[y].data.eId) {
                               selection[y].data.BaanIn_ATC = actionATC.ATC;
                                selection[y].commit();
                                break;
                            }
                        }
                        
                    }
                    var message = '<i class="fa fa-check"></i> ' + CDI.service.Translate.data['common-atc-setted'];
                    CDI.service.erdaAction.showToastMessage(message);

                }
            });

        }
    }
});