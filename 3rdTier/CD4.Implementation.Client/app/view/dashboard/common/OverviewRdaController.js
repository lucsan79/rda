﻿Ext.define('CDI.view.dashboard.common.OverviewRdaController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.OverviewRdaController',

    onBeforeRender: function (view) {
        //alert('luca');
        var panel = view.down("#dashboardGridPanel");
        var items = panel.header.items[0].items;
        
        CDI.service.erdaAction.SetPanelVisibility(items);
    },
    onAfterRender: function (view) {
       
    },
    switchView: function (button) {
        
    },
    onChangeView: function (container, button, pressed) {
        var mainView = container.up("#mainView");
        //recupero sezione main
        var myOpSection = mainView.items.items[0];
        //recupero view corrente
        var actualView = myOpSection.items.items[0];
        //distruggo la view per caricare la prossima
        
        if (pressed) {
            var panel = null;
            console.log(button.itemId);
            switch(button.itemId)
            {
                case 'homeList':
                    panel = Ext.create('CDI.view.dashboard.home.rdaHomeView', {
                        itemId: "rdahomeview",
                    });
                    break;
                case 'worklist':
                    //worklistview
                    panel = Ext.create('CDI.view.dashboard.work.mainView', {
                        //itemId: "workarea",
                    });
                    //panel = Ext.create('CDI.view.dashboard.worklist.mainView', {
                    //    itemId: "worklistview",
                    //});
                    break;
                case 'tobaanpes':
                    panel = Ext.create('CDI.view.dashboard.sync.mainView_pes');
                    break;
                case 'tobaanprp':
                    panel = Ext.create('CDI.view.dashboard.sync.mainView_prp');
                    break;
                case 'folderar':
                    panel = Ext.create('CDI.view.dashboard.sync.mainView_ar');
                    break;
                case 'spendingcurve':
                    panel = Ext.create('CDI.view.dashboard.sync.mainView_sc');
                    break;
                case 'rdaarchive':
                    panel = Ext.create('CDI.view.dashboard.archive.mainView', {
                        itemId: "rdaacrive",
                    });
                    break;
            }
            console.log(panel);
            console.log(actualView);
            if (panel != null) {
                actualView.destroy();
                myOpSection.add(panel);
            }
        }
    }

});