﻿Ext.define('CDI.view.dashboard.common.atcView', {});

Ext.deferDefine('CDI.view.dashboard.common.atcView', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'atcview',
        controller: 'atccontroller',
        cls: 'atcview',
        layout: {
            type: 'hbox',
            align: 'center'
        },
        items: [
            {
                xtype: 'textfield',
                maxLength: 6,
                emptyText: CDI.service.Translate.data["dashboard-rda-emptytext-atc"]
            },
            {
                margin: '0 5',
                xtype:'button',
                text: CDI.service.Translate.data["dashboard-rda-set-atc"],
                tooltip: CDI.service.Translate.data["dashboard-rda-set-atc-tooltip"],
                iconCls: 'fas fa-save',
                cls: 'bottombutton x-btn-plain-toolbar-small',
                handler:'onAssignATC'
            },
        ],

        
    }
});