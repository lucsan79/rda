﻿Ext.define('CDI.view.dashboard.common.OverviewRda', {});
Ext.deferDefine('CDI.view.dashboard.common.OverviewRda', function () {
    return {
        extend: 'Ext.panel.Panel',

        xtype: 'overviewrda',
        controller: 'OverviewRdaController',
        itemId: 'mainView',
        title: false,
        cls: 'gray-window-title rdadashboard',
        layout: 'border',

        
        defaults: {
            collapsible: true,
            split: true,
            //border: true,
        },
        listeners: {
            beforerender: 'onBeforeRender',
            afterrender: 'onAfterRender'
        },

        items: [
            {
                title: '<i class="fa fa-user-shield"></i> <span class="headerL1">' + CDI.service.Translate.data['dashboard-rda-list-header-title'] + '</span>',
                flex: 3,
                region: 'center',
                collapsible: false,
                itemId: 'dashboardGridPanel',
                layout:'fit',
                header: {
                    style: {
                        'padding': '5px 10px 5px 15px',
                    },
                    items: [
                        {
                            xtype: 'segmentedbutton',
                            listeners: {
                                toggle: 'onChangeView'
                            },
                            allowToggle: true,
                            style: {
                                'margin-right': '10px'
                            },
                            defaults: {
                                cls: 'btn-toolbar-middle',
                            },
                            items: [
                                {
                                    cls: 'btn-toolbar-left',
                                    iconCls: 'x-fa fa-home',
                                    itemId: 'homeList',
                                    text: CDI.service.Translate.data['dashboard-rda-list-button-home'],
                                    pressed: true,
                                },
                                {
                                    iconCls: 'x-fa fa-signature',
                                    itemId: 'worklist',
                                    text: CDI.service.Translate.data['dashboard-rda-list-button-myrda'],
                                },
                                //{
                                //    iconCls: 'x-fa fa-check',
                                //    itemId: 'worklist',
                                //    text: CDI.service.Translate.data['dashboard-rda-list-button-signed'],
                                //},
                                {
                                    iconCls: 'x-fa fa-exchange-alt',
                                    itemId: 'tobaanpes',
                                    text: CDI.service.Translate.data['dashboard-rda-list-button-baan-pes'],
                                },
                                {
                                    iconCls: 'x-fa fa-exchange-alt',
                                    itemId: 'tobaanprp',
                                    text: CDI.service.Translate.data['dashboard-rda-list-button-baan-prp'],
                                },
                                {
                                    iconCls: 'x-fa fa-folder-open',
                                    itemId: 'folderar',
                                    text: CDI.service.Translate.data['dashboard-rda-list-button-folder-ar'],
                                },

                                //{
                                //    iconCls: 'x-fa fa-files-o',
                                //    itemId: 'multicreation',
                                //    text: CDI.service.Translate.data['dashboard-rda-list-button-multicreation'],
                                //},
                                {
                                    
                                   
                                    iconCls: 'x-fa fa-eur',
                                    itemId: 'spendingcurve',
                                    text: CDI.service.Translate.data['dashboard-rda-list-button-spendingcurve'],
                                },
                                //{
                                //    iconCls: 'x-fa fa-check',
                                //    itemId: 'worklist',
                                //    text: CDI.service.Translate.data['dashboard-rda-list-button-signed'],
                                //},
                                {
                                    cls: 'btn-toolbar-right',
                                    iconCls: 'x-fa fa-history',
                                    itemId: 'rdaarchive',
                                    text: CDI.service.Translate.data['dashboard-rda-list-button-archive'],
                                },

                                
                            ]
                        },
                    ]
                },
                items:
                    [
                        {
                            
                            xtype: 'rdahomeview',
                            itemId: 'rdahomeview',
                        }
                    ]
            }
        ]
    }
});