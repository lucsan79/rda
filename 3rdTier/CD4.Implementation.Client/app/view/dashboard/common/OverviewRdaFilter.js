Ext.define('CDI.view.dashboard.common.OverviewRdaFilter', {});
Ext.deferDefine('CDI.view.dashboard.common.OverviewRdaFilter', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'overviewrdafilter',
        //controller: 'milestonealertcontroller',
        itemId: 'overviewrdafilter',
        cls: 'overviewrdafilter',
        padding:10,
        //listeners: {
        //    afterrender: 'onDashboardFilterLoad',
        //},

        itemId: 'filterOnAFP',
        items: [
            {
                xtype: 'checkboxgroup',
                itemId: 'checkboxgroupproject',
                cls:'checkboxgroup',
                fieldLabel: CDI.service.Translate.data['etazebao-filter-project'],
                // Put all controls in a single column with width 100%
                columns: 1,
                items: [
                    { boxLabel: 'Item 1', name: 'cb-col-1' },
                    { boxLabel: 'Item 2', name: 'cb-col-2', checked: true },
                    { boxLabel: 'Item 3', name: 'cb-col-3' }
                ]
            },
            {
                xtype: 'checkboxgroup',
                itemId: 'checkboxgrouparea',
                cls: 'checkboxgroup',
                fieldLabel: CDI.service.Translate.data['etazebao-filter-area'],
                // Put all controls in a single column with width 100%
                columns: 1,
                items: [
                    { boxLabel: 'Item 1', name: 'cb-col-1' },
                    { boxLabel: 'Item 2', name: 'cb-col-2', checked: true },
                    { boxLabel: 'Item 3', name: 'cb-col-3' }
                ]
            },
            {
                xtype: 'checkboxgroup',
                itemId: 'checkboxgroupsystem',
                cls: 'checkboxgroup',
                fieldLabel: CDI.service.Translate.data['etazebao-filter-system'],
                // Put all controls in a single column with width 100%
                columns: 1,
                items: [
                    { boxLabel: 'Item 1', name: 'cb-col-1' },
                    { boxLabel: 'Item 2', name: 'cb-col-2', checked: true },
                    { boxLabel: 'Item 3', name: 'cb-col-3' }
                ]
            }
            ,
            {
                xtype: 'checkboxgroup',
                itemId: 'checkboxgrouppr',
                cls: 'checkboxgroup',
                fieldLabel: CDI.service.Translate.data['etazebao-filter-pr'],
                // Put all controls in a single column with width 100%
                columns: 1,
                items: [
                    { boxLabel: 'Item 1', name: 'cb-col-1' },
                    { boxLabel: 'Item 2', name: 'cb-col-2', checked: true },
                    { boxLabel: 'Item 3', name: 'cb-col-3' }
                ]
            },
            {
                xtype: 'checkboxgroup',
                itemId: 'checkboxgrouppc',
                cls: 'checkboxgroup',
                fieldLabel: CDI.service.Translate.data['etazebao-filter-pc'],
                // Put all controls in a single column with width 100%
                columns: 1,
                items: [
                    { boxLabel: 'Item 1', name: 'cb-col-1' },
                    { boxLabel: 'Item 2', name: 'cb-col-2', checked: true },
                    { boxLabel: 'Item 3', name: 'cb-col-3' }
                ]
            }
        ]
    }

});