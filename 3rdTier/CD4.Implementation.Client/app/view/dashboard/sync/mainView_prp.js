﻿Ext.define('CDI.view.dashboard.sync.mainView_prp', {});
Ext.deferDefine('CDI.view.dashboard.sync.mainView_prp', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'syncareaprp',
        controller: 'synccontroller',
        itemId: 'syncareaprp',
        cls: 'worklistview',
        layout: 'border',
        title: '<i class="fa fa-exchange-alt"></i> <span class="headerL2">' + CDI.service.Translate.data['dashboard-rda-list-button-baan-prp'] + '</span>',
        listeners: {
            beforerender: 'onBeforeRender',
            showView:'onShowView'
        },
        header: {
            style: {
                'padding': '5px 20px 5px 15px!important',
            },
            items: [

                {
                    xtype: 'segmentedbutton',
                    cls: 'subheadermenu',
                    listeners: {
                        toggle: 'onToggleButtonClick'
                    },
                    allowToggle: true,
                    defaults: {
                        width: 150,
                    },
                    items: [
                        {
                            cls: 'btn-toolbar-left',
                            iconCls: 'x-fa fa-share-alt',
                            viewId: 'rdabaantosend',
                            text: CDI.service.Translate.data['dashboard-rda-list-button-baan-tosend'],
                            pressed: true,
                            options: {
                                url: "erda/user/getworklist/baan/prp/tosend",
                                allowSelection: true,
                                columnhide: ['BaanIn_ExchangeMessage', 'PRP'],
                                allowEdit: true,
								allowClosure:true
                            }
                        },
                        {
                            cls: 'btn-toolbar-middle',
                            iconCls: 'x-fa fa-bolt',
                            viewId: 'rdabaanprocessing',
                            text: CDI.service.Translate.data['dashboard-rda-list-button-baan-processing'],
                            options: {
                                url: "erda/user/getworklist/baan/prp/processing",
                                allowSelection: false,
                                columnhide: ['Sys_Error', 'BaanIn_ExchangeMessage', 'PRP'],
                                allowEdit: false,
								allowClosure:false
                            }

                        },
                        {

                            cls: 'btn-toolbar-middle',
                            iconCls: 'x-fa fa-exclamation-triangle',
                            viewId: 'rdabaanrejected',
                            text: CDI.service.Translate.data['dashboard-rda-list-button-baan-rejected'],
                            options: {
                                url: "erda/user/getworklist/baan/prp/rejected",
                                allowSelection: true,
                                columnhide: ['Sys_Error','PRP'],
                                allowEdit: true,
								allowClosure:false
                            }
                        }
                        ,
                        {

                            cls: 'btn-toolbar-right',
                            iconCls: 'fas fa-stamp',
                            viewId: 'rdabaanclosed',
                            text: CDI.service.Translate.data['dashboard-rda-list-button-baan-closed'],
                            options: {
                                url: "erda/user/getworklist/baan/prp/closed",
                                allowSelection: false,
                                columnhide: ['Sys_Error', 'BaanIn_ExchangeMessage'],
                                allowEdit: false,
								allowClosure:false

                            }
                        }

                    ]
                },
            ]
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                split: true,
                collapsible: true,
                title: CDI.service.Translate.data["dashboard-rda-projectlist-label"],
                iconCls: 'x-fa fa-car',
                region: 'west',
                flex: 1,
                minWidth: 350,
                defaults: {
                    flex: 1,
                    frame: true,
                },
                cls: 'notopborder',
                itemId: 'projectPanelView',
                items: [
                    {
                        xtype: 'projectsyncgrid',
                        syncView:'syncdatagridprp'
                    },
                ]
            },
            {
                region: 'center',
                flex: 4,
                itemId: 'objectPanelView',
                defaults: {
                    flex: 1,
                    frame: true,
                    scrollable: true,
                    cls:'basicgrid column-header-major'
                },
                items: [
                    {
                        xtype: 'syncdatagridprp'
                    }
                ]
            },
            {
                split: true,
                collapsible: true,
                collapsed: true,
                title: CDI.service.Translate.data["dashboard-rda-erda-edit-tobaan"],
                iconCls: 'x-fa fa-edit',
                region: 'east',
                flex: 1,
                minWidth: 500,
                defaults: {
                    flex: 1,
                    frame: true,
                },
                cls: 'notopborder',
                itemId: 'updatePanelView',
                items: [
                    {
                        xtype: 'rdaupdateprp',
                    },
                ]
            },
        ]

        

    }
});