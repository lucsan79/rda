﻿Ext.define('CDI.view.dashboard.sync.mainView_ar', {});
Ext.deferDefine('CDI.view.dashboard.sync.mainView_ar', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'syncareaar',
        controller: 'synccontroller',
        itemId: 'syncareaar',
        cls: 'worklistview',
        layout: 'border',
        title: '<i class="fas fa-folder-open"></i> <span class="headerL2">' + CDI.service.Translate.data['dashboard-rda-list-button-folder-ar'] + '</span>',
        listeners: {
            beforerender: 'onBeforeRender',
            showView:'onShowView'
        },
        header: {
            style: {
                'padding': '5px 20px 5px 15px!important',
            },
            items: [

                {
                    xtype: 'segmentedbutton',
                    cls: 'subheadermenu',
                    listeners: {
                        toggle: 'onToggleButtonClick'
                    },
                    allowToggle: true,
                    defaults: {
                        width: 150,
                    },
                    items: [
                        {
                            cls: 'btn-toolbar-left',
                            hidden:true,
                            iconCls: 'x-fa fa-share-alt',
                            viewId: 'rdabaantosend',
                            text: CDI.service.Translate.data['dashboard-rda-list-button-baan-tosend'],
                            pressed: true,
                            options: {
                                url: "erda/user/getworklist/folderar",
                                allowSelection: false,
                                columnhide: []
                            }
                        }
                    ]
                },
            ]
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                split: true,
                collapsible: true,
                title: CDI.service.Translate.data["dashboard-rda-projectlist-label"],
                iconCls: 'x-fa fa-car',
                region: 'west',
                flex: 1,
                minWidth: 350,
                defaults: {
                    flex: 1,
                    frame: true,
                },
                cls: 'notopborder',
                itemId: 'projectPanelView',
                items: [
                    {
                        xtype: 'projectsyncgrid',
                        syncView:'syncdatagridar'
                    },
                ]
            },
            {
                region: 'center',
                flex: 4,
                itemId: 'objectPanelView',
                defaults: {
                    flex: 1,
                    frame: true,
                    scrollable: true,
                    cls:'basicgrid column-header-major'
                },
                items: [
                    {
                        xtype: 'syncdatagridar'
                    }
                ]
            }
        ]

        

    }
});