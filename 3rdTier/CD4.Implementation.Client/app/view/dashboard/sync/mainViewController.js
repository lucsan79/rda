﻿Ext.define('CDI.view.dashboard.sync.mainViewController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.synccontroller',

        onBeforeRender: function (view) {
            console.log('sync controoler onBeforeRender');
            var btnPressed = null;
            var buttons = null;
            if (this.view.header.xtype === undefined)
                buttons = this.view.header.items[0].items;
            else
                buttons = this.view.header.down("segmentedbutton").items.items;

            if (buttons != null && buttons.length > 0) {
                for (var i = 0; i < buttons.length; i++) {
                    if (buttons[i].pressed) {
                        btnPressed = buttons[i];
                        break;
                    }

                }
            }
            if (btnPressed != null)
                this.onShowView(view, btnPressed.viewId, btnPressed.options);


            console.log('sync onBeforeRender');


        },

        
        onToggleButtonClick: function (container, button, pressed) {
            var view = container.up().up();
            view.getController().onShowView(view, button.viewId, button.options);
        },

        onShowView: function (view, viewToShow, options) {

            var projectPanel = view.down("#projectPanelView");
            var objectPanel = view.down("#objectPanelView");
            var projectGrid = projectPanel.down("grid");
            var objectGrid = objectPanel.down("grid");
            var pagingtoolbar = objectPanel.down("pagingtoolbar");
            debugger;
            pagingtoolbar.down("#btnApprove").disable();
            pagingtoolbar.down("#btnFastApprove").disable();
            var btn2 = (pagingtoolbar.down("atcview") == null) ? null : pagingtoolbar.down("atcview").getController();
            if (btn2) btn2.setreadonly(true);
            var btn3 = pagingtoolbar.down("#btnReject");
            if (btn3) btn3.disable();


            try {
                var view = objectGrid.view.up("syncareapes");
                var editPanel = null;
                if (view == null)
                    view = objectGrid.view.up("syncareaprp");
                view = view.down("#updatePanelView")
                view.collapse();
                if (options.allowEdit)
                    view.show();
                else
                    view.hide();

                this.onReloadPanel(view, view.items.items[0]);
            } catch (err) { }
            //pagingtoolbar.down("#btnReject").disable();
            
            var url = CD.apiUrl + options.url;

            projectPanel.up().setLoading(true);
            objectPanel.setLoading(true);

            var projectStore = { fields: [], data: [] };


            projectGrid.setEmptyText("<h4 style='margin-top: -2px;'>" + CDI.service.Translate.data['rda-norda-to-manage'] + "</h4>");
            projectGrid.setStore(projectStore);
            objectGrid.setStore(projectStore);
            objectGrid.filters.clearFilters();


            Ext.Ajax.request({
                url: url,
                method: 'GET',
                withCredentials: true,
                failure: function (response, textStatus, error) {
                    view.setLoading(false);
                    projectPanel.up().setLoading(false);
                    objectPanel.setLoading(false);
                    CDI.service.erdaAction.onFailureRequest(response, textStatus, CDI.service.Translate.data['dashboard-rda-list-button-spendingcurves'])

                },
            }).then(function (jsonData) {
                var data = Ext.decode(jsonData.responseText);

                projectStore = {
                    data: data.projects
                }
                var objectStore = Ext.create('CDI.store.rdaSimpleStore', {
                    data: data.rdafullList,
                    totalCount: data.rdafullList.length
                });

                projectGrid.setStore(projectStore);
                objectGrid.setStore(objectStore);
                objectGrid.mainStore = data.rdafullList;

                var columnSelection = objectGrid.headerCt.down("[isCheckerHd]");
                var pagingtoolbar = objectGrid.view.up().up().down("pagingtoolbar");
                var btn1 = pagingtoolbar.down("#btnApprove");
                var btn1Fast = pagingtoolbar.down("#btnFastApprove");
                
                var btn2 = pagingtoolbar.down("atcview");
                var btn3 = pagingtoolbar.down("#btnReject");
                
                if (columnSelection && !options.allowSelection) {
                    columnSelection.hide();
                    btn1.hide();
                    btn1Fast.hide();
                    if (btn2) btn2.hide();
                    if (btn3) btn3.hide();
                }
                else {
                    columnSelection.show();
                    btn1.show();
                    btn1Fast.show();
                    if (btn2) btn2.show();
                    if (btn3) btn3.show();
                }
                if (!options.allowClosure)
                    btn1Fast.hide();

                if (options.url.indexOf("rejected")!=-1)
                    if(btn3) btn3.destroy();

                var columns = objectGrid.getColumns();
                for (var i = 0; i < columns.length; i++) {
                    var column = columns[i];
                    var contains = false;
                    if (options.columnhide != null && column.dataIndex && options.columnhide.length > 0) {
                        for (var y = 0; y < options.columnhide.length; y++) {
                            if (options.columnhide[y].toLowerCase() == column.dataIndex.toLowerCase())
                                contains = true;
                        }
                    }
                    if (column.dataIndex)
                        column.setVisible(!contains);
                }

                projectPanel.up().setLoading(false);
                objectPanel.setLoading(false);
            });

        },

        onShowProjetObjtects: function (view, givenProject) {
            var objectPanel = view.down("#objectPanelView");
            objectPanel.setLoading(true);
            setTimeout(function () {
                var objectGrid = objectPanel.down("grid");
                var pagingtoolbar = objectPanel.down("pagingtoolbar");
                pagingtoolbar.down("#btnApprove").disable();
                pagingtoolbar.down("#btnFastApprove").disable();
                var btn2 = pagingtoolbar.down("atcview");
                var btn3 = pagingtoolbar.down("#btnReject");

                if(btn2!=null)btn2=btn2.getController();
                if (btn2) btn2.setreadonly(true);
                if (btn3) btn3.disable();

                //pagingtoolbar.down("#btnReject").disable();
                var backStore = [];
                if (givenProject != null) {
                    if (objectGrid.mainStore != null) {
                        for (var i = 0; i < objectGrid.mainStore.length; i++) {
                            if (objectGrid.mainStore[i].eProject === givenProject) {
                                backStore.push(objectGrid.mainStore[i]);
                            }
                        }
                    }
                }
                else
                    backStore = objectGrid.mainStore;

                var objectStore = Ext.create('CDI.store.rdaSimpleStore', {
                    data: backStore,
                    totalCount: backStore.length
                });
                objectGrid.filters.clearFilters();
                objectGrid.setStore(objectStore);
                setTimeout(function () {
                    objectPanel.setLoading(false);
                }, 20);

            }, 20);

        },

        onProjectSelect: function (grid, record) {

            var projectName = record.data.ProjectName;
            var view = grid.view.up().up().up();
            this.onShowProjetObjtects(view, projectName);
        },

        onProjectDeselect: function (grid, record) {
            var view = grid.view.up().up().up();
            this.onShowProjetObjtects(view, null);
        },


        onRowSelect: function (grid, record) {

            var hasSelection = grid.getSelected().length == 0 ? false : true;
            var pagingtoolbar = grid.view.up().up().down("pagingtoolbar");
           
            if (hasSelection)
                this.onShowEditOptions(grid, record);
            else
                this.onShowEditOptions(grid, null);
        },
        onReloadPanel: function (view, panelDestroy) {

            if (panelDestroy != null) {
                var newPanel = Ext.create({ xtype: panelDestroy.getXType() });
                panelDestroy.destroy();
                view.add(newPanel);
            }
        },
        onShowEditOptions: function (grid, record) {
            var view = grid.view.up("syncareapes");
            var editPanel = null;
            if (view == null)
                view = grid.view.up("syncareaprp");
            var pagingtoolbar = grid.view.up().up().down("pagingtoolbar");
            var btn1 = pagingtoolbar.down("#btnApprove");
            var btn1Fast = pagingtoolbar.down("#btnFastApprove");
            var btn2 = pagingtoolbar.down("atcview") == null ? null : pagingtoolbar.down("atcview").getController();
            var btn3 = pagingtoolbar.down("#btnReject");

            btn1.disable();
            btn1Fast.disable();
            if (btn2) btn2.setreadonly(true);
            if (btn3) btn3.disable();

            if (view != null) {

                var editPanel = view.down("#updatePanelView");
                var updatepanel = null;
                var hasSelection = grid.getSelected().length == 0 ? false : true;
                var hasMultiSelection = grid.getSelected().length > 1 ? true : false;
                if (grid.getSelected().length >= 2) {
                    editPanel.collapse();
                    editPanel.setDisabled(true);
                }
                else
                    editPanel.setDisabled(false);
                var items = grid.getSelected();
                var hasError = false;
                for (var i = 0; i < items.length;i++) {
                    if (items.items[i].data.Sys_Error != "") {
                        hasError = true;
                        break;
                    }
                    
                }
                if (hasSelection) {
                    if (!hasError) {
                        btn1.enable();
                        if (!hasMultiSelection)btn1Fast.enable();
                    } 
                    if (btn2) btn2.setreadonly(false);
                    if (btn3) btn3.enable();
                }
                if (editPanel != null) {
                    
                    updatepanel = editPanel.items.items[0];
                    updatepanel.query('button').forEach(function (c) { c.setDisabled(true); });
                    if (record == null) {
                        this.onReloadPanel(editPanel, updatepanel);
                    }
                    else {
                        editPanel.setLoading(true);
                       // updatepanel.query('button').forEach(function (c) { c.setDisabled(false); });
                        updatepanel.special = record;
                        $.each(record.data, function (key, value) {
                            
                            if (value === undefined || value === null)
                                value = "";
                            var item = editPanel.down("#" + key);
                            
                            
                            if (item != null && value != "") {
                                item.reset();
                                item.resetOriginalValue();
                                switch (item.getXType()) {
                                    case 'datefield':
                                        item.setValue(new Date(value))
                                        break;
                                    default:
                                        item.setValue(value);
                                        
                                        break;

                                }
                                item.resetOriginalValue();
                               
                                //em.reset();

                            }
                            
                        });
                        if (view.getXType() == "syncareapes")
                            editPanel.down("rdaupdatepes").viewModel.data.changed = false;
                        else if (view.getXType() == "syncareaprp")
                            editPanel.down("rdaupdateprp").viewModel.data.changed = false;
                    }
                                    
                    
                    editPanel.setLoading(false);

                }
            }
        },

        onRowDeselect: function (grid, record) {
            this.onRowSelect(grid, record);
        },

        onclearalldata: function (view) {
        },
        onOpenBusinessData: function (gridview, tdEl, cellIndex, record, trEl, rowIndex, e) {
            if (cellIndex == 1) {
                if (tdEl.innerHTML.indexOf("rdalinkbutton") != -1) {
                    if (tdEl.innerHTML.indexOf("openOnKey1") != -1) {
                        CDI.service.erdaAction.open(record.get('eKey1'));
                        return;
                    }
                    CDI.service.erdaAction.open(record.get('eId'));
                }
            }
            else {
                if (tdEl.innerHTML.indexOf("rdalinkbutton") != -1) {
                    var target = e.target;
                    
                    if (target.tagName == "DIV") {
                        target = e.target.firstElementChild;
                    }
                    else if(target.tagName != "A" && target.tagName != "DIV")
                    {
                        target = this.getParentNode(target, "A");
                    }
                    if (target!=null  && target.tagName == "A") {
                        if (target.attributes != null && target.attributes["refid"] != null) {
                            var id = target.attributes["refid"].value;
                            CDI.service.erdaAction.open(id);
                        }
                    }
                }
            }



        },

        getParentNode: function (givenNode,tagName) {
            var parentNode = givenNode.parentNode;
            while (parentNode != null) {
                if (parentNode.tagName != tagName) {
                    parentNode = parentNode.parentNode;
                }
                else
                    return parentNode;
            }

            return null;
        },
        onGridBeforeRender: function (view) {
            var pagingtoolbar = view.down("pagingtoolbar");
            if (pagingtoolbar != null) {
                pagingtoolbar.down('#refresh').hide();
            }
        },

        onResetClick: function (btn) {
            
            var view = btn.up("panel");
            var grid = null;
            if (view.getXType() == "rdaupdateprp") {
                var mainView = view.up("syncareaprp");
                grid = mainView.down("syncdatagridprp").down("grid");
            }
            else if (view.getXType() == "rdaupdatepes") {
                var mainView = view.up("syncareapes");
                grid = mainView.down("syncdatagridpes").down("grid");
            }
            if (grid != null)
                grid = grid.getSelectionModel();
                
            var record = view.special;
            this.onShowEditOptions(grid, record);


        },

        onCompleteClick: function (btn) {
            
            var url = "";
            var editPanel = btn.up("panel");
            var grid = null;
            if (editPanel.getXType() == "rdaupdateprp") {
                url = "erda/baan/save/prp";
                var mainView = editPanel.up("syncareaprp");
                grid = mainView.down("syncdatagridprp").down("grid");
            }
            else if (editPanel.getXType() == "rdaupdatepes") {
                url = "erda/baan/save/pes";
                var mainView = editPanel.up("syncareapes");
                grid = mainView.down("syncdatagridpes").down("grid");
            }
            if (grid != null)
                grid = grid.getSelectionModel();

            var record = editPanel.special;
            
            $.each(record.data, function (key, value) {
                if (value === undefined || value === null)
                    value = "";
                var item = editPanel.down("#" + key);
                if (item != null) {
                    Reflect.set(record.data, key, item.getValue());
                }
            });
            editPanel.setLoading(true);
            grid.view.setDisabled(true);
            
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json'
                },
                url: CD.apiUrl + url,
                method: 'POST',
                jsonData: record.data,
                withCredentials: true,
                failure: function (response) {
                    editPanel.setLoading(false);
                    grid.view.setDisabled(false);
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-saving-title'], "",response);
                },
                success: function (jsonData) {
                    
                    var data = Ext.decode(jsonData.responseText);
                    var records = grid.getSelection();
                    if (records != null) {
                        for (var i = 0; i < records.length; i++)
                        {
                            if (records[i].data.eId == data.eId) {
                                records[i].data = data;
                                records[i].commit();
                                break;
                            }
                        }
                    }
                    grid.view.setDisabled(false);
                    editPanel.setLoading(false);
                    var message = '<i class="fa fa-check"></i> ' + CDI.service.Translate.data['completed-successfully'];
                    CDI.service.erdaAction.showToastMessage(message);
                }
            });

            


        },

        onPageChange: function (pagingToolBar, changeEvent) {
            
            var grid = pagingToolBar.up("grid");
            var view = grid.view.up("syncareapes");
            var editPanel = null;
            if (view == null)
                view = grid.view.up("syncareaprp");
            view = view.down("#updatePanelView")
            view.collapse();
            this.onReloadPanel(view, view.items.items[0]);
            
        },

        onRejectClick: function (btn) {
            var grid = btn.up("grid");
            var selection = grid.getView().getSelectionModel().getSelection();
            if (selection != null && selection.length > 0) {
                var businessdataId = [];
                var businessdataName = [];
                var businessdataLevel = [];

                for (var i = 0; i < selection.length; i++) {
                    businessdataId.push(selection[i].data.eId);
                    businessdataName.push(selection[i].data.eName);
                    businessdataLevel.push(selection[i].data.eLevel);
                }
                CDI.service.erdaAction.showWorkflowAction(btn.up("grid").id, "reject", businessdataId, businessdataName, businessdataLevel);
            }


        },
        onSendToBaanClick: function (btn) {
            
            var grid = btn.up("grid");
            var selection = grid.getView().getSelectionModel().getSelection();
            if (selection != null && selection.length > 0) {
                var businessdataId = [];
                var businessdataName = [];
                for (var i = 0; i < selection.length; i++) {
                    businessdataId.push(selection[i].data.eId);
                    businessdataName.push(selection[i].data.eName);
                }
                var actionType = "";
                if (grid.getXType() == "syncdatagridpes") {

                    actionType = "pes";
                    for (var i = 0; i < selection.length; i++) {
                        if (selection[i].data.prevTrancheToSend > 0) {
                            var message = CDI.service.Translate.data["dashboard-rda-baan-tranche"];
                            CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['dashboard-rda-baan-warning'], message, null);
                            return;
                        }
                    }

                }
                    
                else if (grid.getXType() == "syncdatagridprp")
                    actionType = "prp";

                if (actionType == "") {
                    var message = CDI.service.Translate.data["generic-error-erdatable-error"];
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-baansync-title'], message, null);
                }
                else {
                    CDI.service.erdaAction.showSendBaaNAction(btn.up("grid").id, actionType, businessdataId, businessdataName);
                }

                
            }

        },


        onFastClosureClick: function (btn) {

            var grid = btn.up("grid");
            var selection = grid.getView().getSelectionModel().getSelection();
            if (selection != null && selection.length > 0) {
                var businessdataId = [];
                var businessdataName = [];
                for (var i = 0; i < selection.length; i++) {
                    businessdataId.push(selection[i].data.eId);
                    businessdataName.push(selection[i].data.eName);
                }
                var actionType = "";
                if (grid.getXType() == "syncdatagridpes") {

                    actionType = "pes";
                    for (var i = 0; i < selection.length; i++) {
                        if (selection[i].data.prevTrancheToSend > 0) {
                            var message = CDI.service.Translate.data["dashboard-rda-baan-tranche"];
                            CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['dashboard-rda-baan-warning'], message, null);
                            return;
                        }
                    }

                }

                else if (grid.getXType() == "syncdatagridprp")
                    actionType = "prp";

                if (actionType == "") {
                    var message = CDI.service.Translate.data["generic-error-erdatable-error"];
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-baansync-title'], message, null);
                }
                else {
                    CDI.service.erdaAction.showFastClosureAction(btn.up("grid").id, actionType, businessdataId, businessdataName);
                }


            }

        },


        doRefresh: function (data) {
            var button = this.getView().down("#btnRefresh");
            button.fireEvent("click", button);
        }
    });
