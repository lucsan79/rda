﻿Ext.define('CDI.view.dashboard.sync.base.objectUpdateView_pes', {});
Ext.deferDefine('CDI.view.dashboard.sync.base.objectUpdateView_pes', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'rdaupdatepes',
        itemId: 'rdaupdatepes',
        //layout: 'fit',
        //height: '100%',
        //width: '100%',
        cls: 'easyeditview',
        requires: [
            'Ext.form.field.Date',
            'Ext.form.field.ComboBox',
        ],
        controller: 'synccontroller',
        bodyPadding: '10px 20px',
        frame: true,
        scrollable: true,
        viewModel: {
            data: {
                changed: false
            }
        },
        
        defaults: {
            xtype: 'textfield',
            width: '100%',
            listeners: {
                change: function (field, value) {
                    field.lookupViewModel().set('changed', field.isDirty());
                }
            }
        },
        items: [
            {
                xtype: 'displayfield',
                itemId: 'eName',
                cls:'code pes',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-code"],
               
            },
            {
                itemId: 'BaanIn_ATC',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-atc"],
                
            },
            {
                xtype: 'datefield',
                itemId: 'BaanIn_DataErosioneBudget',
                format: Ext.decode(localStorage.localizationInfo).ShortDateFormat,
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_dataerosionebudget"],
            },
            {
                xtype: 'combobox',
                
                itemId: 'Supplier',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-supplier"],
                displayField: 'Value',
                valueField: 'Value',
                store: {
                    proxy: {
                        type: 'ajax',
                        withCredentials: true,
                        url: CD.apiUrl + 'erda/combobox/supplier',
                        autoload: false,
                        reader: {
                            type: 'json'
                        }
                    }
                },
                minChars: 1,
                ////queryMode: 'remote',
                filterPickList: false
            },
            {
                xtype: 'combobox',
                itemId: 'BaanIn_Articolo',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_articolo"],
                displayField: 'Value',
                valueField: 'Value',
                store: {
                    type: 'array',
                    fields: ['Value'],
                    data: [
                        ['PRESTAZ01'],
                        ['PRESTAZ02'],
                        ['MATVARIO']
                    ]
                },
            },
            
            //{
            //    xtype: 'numberfield',
            //    itemId: 'Hours',
            //    fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-hours"],
            //},
            {
                itemId:'BaanIn_Buyer',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_buyer"],
            },
            {
                itemId: 'BaanIn_Conto',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_conto"],
            },
            {
                itemId: 'BaanIn_CDC',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-cdc"],
            },
            {
                itemId: 'BaanIn_Commessa',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_commessa"],
            },
            {
                itemId: 'BaanIn_Magazzino',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_magazzino"],
            },
            {
                itemId: 'BaanIn_Requester',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_requester"],
            },
            {
                xtype:'textarea',
                itemId: 'Purchasing',
                height: '150px',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-purchasing"],
            },

        ],
        fbar: [
            {
                xtype: 'button',
                disabled: true,
                iconCls: 'fas fa-ban',
                cls: 'bottombutton x-btn-plain-toolbar-small',
                text: CDI.service.Translate.data["rda-admincenter-cancelbtn-tooltip"],
                handler: 'onResetClick',
                bind: {
                    disabled: '{!changed}'
                }

            },
            {
                xtype: 'button',
                disabled: true,
                iconCls: 'fas fa-save',
                cls: 'bottombutton x-btn-plain-toolbar-small',
                text: CDI.service.Translate.data["rda-admincenter-savebtn-tooltip"],
                width: 150,
                handler: 'onCompleteClick',
                bind: {
                    disabled: '{!changed}'
                }
            }
        ],

       

    }
});

