﻿Ext.define('CDI.view.dashboard.sync.base.objectListView_ar', {});
Ext.deferDefine('CDI.view.dashboard.sync.base.objectListView_ar', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'syncdatagridar',
        itemId: 'syncdatagridar',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid',

        controller: 'synccontroller',
        mainStore: null,

        emptyText: CDI.service.Translate.data["dashboard-rda-worklist-grid-emptyText"],
        scrollable: true,

        listeners: {
            cellclick: 'onOpenBusinessData',
            beforerender: 'onGridBeforeRender'
        },

        syncRowHeight: true,
        viewConfig: {
            loadMask: true,
            listeners: {
                select: 'onRowSelect',
                deselect: 'onRowDeselect',

                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        plugins: 'gridfilters',

        selModel: {
            //type: 'checkboxmodel'
            selType: 'checkboxmodel', //'rowmodel',
            //mode: 'SINGLE',
            checkOnly: 'true',
            allowDeselect: true
        },
        enableColumnHide: false,
        columns:
            [


                {
                    text:  CDI.service.Translate.data["dashboard-rda-worklist-grid-column-code"],
                    dataIndex: 'eName',
                    width: 120,
                    cls: 'locked',
                    autoSizeColumn: false,
                    locked: true,
                    renderer: function (value, meta, record) {
                        var buttonText = "<i class='fas fa-donate'></i> " + value;
                        var extraparam = "";//(record.data.CBS.toLowerCase() == "materials" ? "material" : "");
                        return '<a title="' + CDI.service.Translate.data["dashboard-rda-cardbutton-open"] + '" role="button" class="rdalinkbutton ' + extraparam + ' x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box" hidefocus="on" unselectable="on"><span role="presentation" class="x-btn-wrap" unselectable="on"><span class="x-btn-button" role="presentation"><span class="x-btn-inner x-btn-inner-center" unselectable="on">' + buttonText + '</span><span role="presentation" class="x-btn-icon-el" unselectable="on" style=""></span></span></span></a>';
                    }
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-description"],
                    dataIndex: 'eDescription',
                    minWidth: 100,
                    cls: 'extend',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },


                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-atc"],
                    dataIndex: 'BaanIn_ATC',
                    width: 120,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },

                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_dataerosionebudget"],
                    dataIndex: 'BaanIn_DataErosioneBudget',
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            //Fix d/m/Y format
                            var dateArray = v.toString().split('/');
                            if (dateArray.length == 3) {
                                v = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                            }
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },
                },

                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-project"],
                    dataIndex: 'eProject',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-supplier"],
                    dataIndex: 'Supplier',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_articolo"],
                    dataIndex: 'BaanIn_Articolo',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-cbs"],
                    dataIndex: 'CBS',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-hours"],
                    dataIndex: 'Hours',
                    filter: 'number',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-amount"],
                    dataIndex: 'Amount',
                    filter: 'number',
					align:'right',
                    renderer: function (value, metaData, record, rowIndex) {
                         try {
							var currency = record.data.AmountFormat;
							if (currency == "dollar") 
                                currency = "$";
                            else
                                currency = "€";
							return CDI.service.erdaAction.formatMoney(value, currency, 2, true, " ");
						} catch (err) { };

						return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,

                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_codeprojectform"],
                    dataIndex: 'CodeProjectForm',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_buyer"],
                    dataIndex: 'BaanIn_Buyer',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_conto"],
                    dataIndex: 'BaanIn_Conto',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-cdc"],
                    dataIndex: 'CDC',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_commessa"],
                    dataIndex: 'BaanIn_Commessa',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },

                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_rigatesto"],
                    dataIndex: 'BaanIn_RigaTesto',
                    minWidth: 450,
                    //cls: 'multirowcol',
                    renderer: function (value, metaData) {
                        value = value.split(/\n/i).join('<br/>');
                        return '<div class="multirowcol">' + value + '</div>';
                    },
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_requester"],
                    dataIndex: 'BaanIn_Requester',
                    minWidth: 100,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-purchasing"],
                    dataIndex: 'Purchasing',
                    minWidth: 500,
                    //cls: 'multirowcol',
                    renderer: function (value, metaData) {
                        var cond = value.endsWith(";");
                        if (cond)
                            value = value.substring(0, value.length - 1);//value + ";";
                        var split = value.split(';');
                        if (split.length > 0) {
                            value = "";
                            for (i = 0; i < split.length; i++) {
                                value = value + split[i] + ";";
                                if (split[i + 1] != undefined && split[i + 1]!="")
                                    value = value + split[i+1] + ";";
                                value = value + "<br/>";
                                i++;
                            }
                        }
                        return '<div class="multirowcol">' + value + '</div>';
                    },
                    lockable: false,
                    sortable: false,
                    filter: true
                }
                //,
                //{
                //    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-notes"],
                //    dataIndex: 'Notes',
                //    minWidth: 500,
                //    //cls: 'multirowcol',
                //    renderer: function (value, metaData) {
                //        value = value.split(/\n/i).join('<br/>');
                //        return '<div class="multirowcol">' + value + '</div>';
                //    },
                //    autoSizeColumn: false,
                //    lockable: false,
                //    sortable: false,
                //    filter: true
                //}

            ],


        dockedItems: [{
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["dashboard-rda-worklist-grid-pagingtoolbar-emptyMsg"],

            items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("syncareaar").getController().onBeforeRender(this.up("syncareaar"));
                        }
                    }
                },
                {
                    xtype: 'button',
                    itemId: 'btnApprove',
                    iconCls: 'fas fa-check',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-wfaction-approve"],
                    tooltip: CDI.service.Translate.data["rda-generic-approveobjects-title"],
                  //  tooltipType: 'title',
                    listeners: {
                        click: function () {
                        }
                    }
                },
                //{
                //    xtype: 'button',
                //    itemId: 'btnReject',
                //    iconCls: 'fas fa-times',
                //    cls: 'pagingToolbarButton',
                //    text: CDI.service.Translate.data["common-wfaction-reject"],
                //    tooltip: CDI.service.Translate.data["rda-generic-rejectbjects-title"],
                //    tooltipType: 'title',
                //    listeners: {
                //        click: function () {

                //        }
                //    }
                //},

                // '-',
                //{
                //    xtype: 'atcview',
                //    itemId: 'btnATC',

                //}
            ]
        }]


    }
});

