﻿Ext.define('CDI.view.dashboard.sync.base.objectListView_prp', {});
Ext.deferDefine('CDI.view.dashboard.sync.base.objectListView_prp', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'syncdatagridprp',
        itemId: 'syncdatagridprp',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid',

        controller: 'synccontroller',
        mainStore: null,
        columnLines: true,
        emptyText: CDI.service.Translate.data["dashboard-rda-worklist-grid-emptyText"],
        scrollable: true,

        listeners: {
            cellclick: 'onOpenBusinessData',
            beforerender: 'onGridBeforeRender'
        },

        syncRowHeight: true,
        viewConfig: {
            loadMask: true,
            listeners: {
                select: 'onShowEditOptions',
                deselect: 'onRowDeselect',

                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        plugins: 'gridfilters',

        selModel: {
            selType: 'checkboxmodel', //'rowmodel',
            allowDeselect: true
        },
        enableColumnHide: false,
              
        columns:
            [


                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-code"],
                    dataIndex: 'eName',
                    width: 130,
                    cls: 'locked',
                    autoSizeColumn: false,
                    locked: true,
                    lockable: false,
                    sortable: false,
                    renderer: function (value, meta, record) {
                        var buttonText = "<i class='fas fa-donate'></i> " + value;
                        var extraparam = "material";//(record.data.CBS.toLowerCase() == "materials" ? "material" : "");
                        return '<a title="' + CDI.service.Translate.data["dashboard-rda-cardbutton-open"] + '" role="button" class="rdalinkbutton ' + extraparam + ' x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box" hidefocus="on" unselectable="on"><span role="presentation" class="x-btn-wrap" unselectable="on"><span class="x-btn-button" role="presentation"><span class="x-btn-inner x-btn-inner-center" unselectable="on">' + buttonText + '</span><span role="presentation" class="x-btn-icon-el" unselectable="on" style=""></span></span></span></a>';
                    },
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-sys_error"],
                    dataIndex: 'Sys_Error',
                    align: 'center',
                    minWidth: 50,
                    renderer: function (value, metaData) {
                        value = "" + value.split(/\n/i).join('<br/>');
                        return '<div class="multirowcol inerror">' + value + '</div>';
                    },
                    autoSizeColumn: false,
                    locked: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_error"],
                    dataIndex: 'BaanIn_ExchangeMessage',
                    //maxWidth: 250,
                    minWidth: 150,
                    renderer: function (value, metaData) {
                        try {
                            value = value.split(/\n/i).join('<br/>');
                            return '<div class="multirowcol inerror">' + value + '</div>';
                        } catch (err) { alert(err.message) };

                    },
                    autoSizeColumn: true,
                    locked: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-prp"],
                    dataIndex: 'PRP',
                    width: 120,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,

                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-description"],
                    dataIndex: 'eDescription',
                    minWidth: 100,
                    cls: 'extend',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_commessa"],
                    dataIndex: 'BaanIn_Commessa',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-atc"],
                    dataIndex: 'BaanIn_ATC',
                    width: 120,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_articolo"],
                    dataIndex: 'BaanIn_Articolo',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_descarticolo"],
                    dataIndex: 'BaanIn_DescArticolo',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_magazzino"],
                    dataIndex: 'BaanIn_Magazzino',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-supplier"],
                    dataIndex: 'Supplier',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_acquisitore"],
                    dataIndex: 'BaanIn_Acquisitore',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_pianificatore"],
                    dataIndex: 'BaanIn_Pianificatore',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-totalquantity"],
                    dataIndex: 'TotalQuantity',
                    filter: 'number',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-pieceprice"],
                    dataIndex: 'PiecePrice',
                    filter: 'number',
					align:'right',
                    renderer: function (value, metaData, record, rowIndex) {
                        try {
							var currency = record.data.PiecePriceFormat;
							if (currency == "dollar") 
                                currency = "$";
                            else
                                currency = "€";
                            return CDI.service.erdaAction.formatMoney(value, currency, 2, true, " ");
							//return Ext.util.Format.currency(value, currency, 2, true, " ");
						} catch (err) { };

						return "";
                    },
                    minWidth: 120,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-piecepriceformat"],
                    dataIndex: 'PiecePriceFormat',
                    minWidth: 80,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_datadiconsegna"],
                    dataIndex: 'BaanIn_DataConsegnaPianificata',
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            //Fix d/m/Y format
                            var dateArray = v.toString().split('/');
                            if (dateArray.length == 3) {
                                v = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                            }
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },

                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_riferimento_a"],
                    dataIndex: 'BaanIn_RiferimentoA',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_riferimento_b"],
                    dataIndex: 'BaanIn_RiferimentoB',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_riferimento_c"],
                    dataIndex: 'BaanIn_RiferimentoC',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_rigatesto"],
                    dataIndex: 'BaanIn_RigaTesto',
                    minWidth: 450,
                    //cls: 'multirowcol',
                    renderer: function (value, metaData) {
                        value = value.split(/\n/i).join('<br/>');
                        return '<div class="multirowcol">' + value + '</div>';
                    },
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_gruppoarticoli"],
                    dataIndex: 'BaanIn_GruppoArticoli',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_commassaprp"],
                    dataIndex: 'BaanIn_CommessaOrdinePRP',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_requester"],
                    dataIndex: 'BaanIn_Requester',
                    minWidth: 100,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-purchasing"],
                    dataIndex: 'Purchasing',
                    minWidth: 500,
                    //cls: 'multirowcol',
                    renderer: function (value, metaData) {
                        var cond = value.endsWith(";");
                        if (cond)
                            value = value.substring(0, value.length - 1);//value + ";";
                        var split = value.split(';');
                        if (split.length > 0) {
                            value = "";
                            for (i = 0; i < split.length; i++) {
                                value = value + split[i] + ";";
                                if (split[i + 1] != undefined && split[i + 1]!="")
                                    value = value + split[i+1] + ";";
                                value = value + "<br/>";
                                i++;
                            }
                        }
                        return '<div class="multirowcol">' + value + '</div>';
                    },
                    lockable: false,
                    sortable: false,
                    filter: true
                }
               

            ],


        dockedItems: [{
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["dashboard-rda-worklist-grid-pagingtoolbar-emptyMsg"],
            listeners: {
                change: 'onPageChange',
            },
            items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("syncareaprp").getController().onBeforeRender(this.up("syncareaprp"));
                        }
                    }
                },
                {
                    xtype: 'button',
                    itemId: 'btnReject',
                    iconCls: 'fas fa-times',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-wfaction-reject"],
                    tooltip: CDI.service.Translate.data["rda-generic-rejectbjects-title"],
                    //  tooltipType: 'title',
                    listeners: {
                        click: 'onRejectClick'
                    }
                },
                {
                    xtype: 'button',
                    itemId: 'btnApprove',
                    iconCls: 'fas fa-share-alt',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["rda-generic-send-baan-objects-title"],
                    tooltip: CDI.service.Translate.data["rda-generic-send-baan-objects-title"],
                  //  tooltipType: 'title',
                    handler: 'onSendToBaanClick'
                },
                {

                    xtype: 'button',
                    itemId: 'btnFastApprove',
                    iconCls: 'fas fa-road',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["rda-generic-fastclosure-title"],
                    tooltip: CDI.service.Translate.data["rda-generic-fastclosure-tooltip"],
                    //  tooltipType: 'title',
                    handler: 'onFastClosureClick'
                },
                {
                    xtype: 'atcview',
                    itemId: 'btnATC',

                }
                //{
                //    xtype: 'button',
                //    itemId: 'btnReject',
                //    iconCls: 'fas fa-times',
                //    cls: 'pagingToolbarButton',
                //    text: CDI.service.Translate.data["common-wfaction-reject"],
                //    tooltip: CDI.service.Translate.data["rda-generic-rejectbjects-title"],
                //    tooltipType: 'title',
                //    listeners: {
                //        click: function () {

                //        }
                //    }
                //}
            ]
        }]


    }
});

