﻿Ext.define('CDI.view.dashboard.sync.base.objectListView_pes', {});
Ext.deferDefine('CDI.view.dashboard.sync.base.objectListView_pes', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'syncdatagridpes',
        itemId: 'syncdatagridpes',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid',

        
        controller: 'synccontroller',
        mainStore: null,
        columnLines: true,
        emptyText: CDI.service.Translate.data["dashboard-rda-worklist-grid-emptyText"],
        scrollable: true,

        listeners: {
            cellclick: 'onOpenBusinessData',
            beforerender: 'onGridBeforeRender'
        },

        syncRowHeight: true,
        viewConfig: {
            loadMask: true,
            listeners: {
                select: 'onRowSelect',
                deselect: 'onRowDeselect',

                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        plugins: 'gridfilters',

        selModel: {
            //selType: 'rowmodel', // rowmodel is the default selection model
            //mode: 'MULTI' // Allows selection of multiple rows

            ///type: 'checkboxmodel'
            selType: 'checkboxmodel', //'rowmodel',
            ////mode: 'SINGLE',
            //checkOnly: 'true',
            allowDeselect: true
        },
        enableColumnHide: false,
        columns:
            [


                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-code"],
                    dataIndex: 'eName',
                    width: 130,
                    cls: 'locked',
                    autoSizeColumn: false,
                    locked: true,
                    lockable: false,
                    sortable: false,
                    renderer: function (value, meta, record) {
                        var buttonText = "<i class='fas fa-donate'></i> " + value;
                        var extraparam = "";//(record.data.CBS.toLowerCase() == "materials" ? "material" : "");
                        return '<a  title="' + CDI.service.Translate.data["dashboard-rda-cardbutton-open"] + '" role="button" class="rdalinkbutton ' + extraparam + ' x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box" hidefocus="on" unselectable="on"><span role="presentation" class="x-btn-wrap" unselectable="on"><span class="x-btn-button" role="presentation"><span class="x-btn-inner x-btn-inner-center" unselectable="on">' + buttonText + '</span><span role="presentation" class="x-btn-icon-el" unselectable="on" style=""></span></span></span></a>';
                    },
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-sys_error"],
                    dataIndex: 'Sys_Error',
                    align: 'center',
                    minWidth: 50,
                    renderer: function (value, metaData) {
                        value ="" +  value.split(/\n/i).join('<br/>');
                        return '<div class="multirowcol inerror">' + value + '</div>';
                    },
                    autoSizeColumn: false,
                    locked: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
               {
                   text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_error"],
                   dataIndex: 'BaanIn_ExchangeMessage',
                   minWidth: 150,
                   //maxWidth: 250,
                   renderer: function (value, metaData) {
                       try {
                           value = value.split(/\n/i).join('<br/>');
                           return '<div class="multirowcol inerror">' + value + '</div>';
                       } catch (err) { alert(err.message) };
                        
                    },
                    autoSizeColumn: true,
                    locked: true,
                   sortable: false,
                   lockable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-description"],
                    dataIndex: 'eDescription',
                    minWidth: 100,
                    cls: 'extend',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },


                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-atc"],
                    dataIndex: 'BaanIn_ATC',
                    width: 120,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },

                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_dataerosionebudget"],
                    dataIndex: 'BaanIn_DataErosioneBudget',
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            //Fix d/m/Y format
                            var dateArray = v.toString().split('/');
                            if (dateArray.length == 3) {
                                v = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                            }
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },
                },

                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-project"],
                    dataIndex: 'eProject',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-supplier"],
                    dataIndex: 'Supplier',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_articolo"],
                    dataIndex: 'BaanIn_Articolo',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-cbs"],
                    dataIndex: 'CBS',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-hours"],
                    dataIndex: 'Hours',
                    filter: 'number',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-amount"],
                    dataIndex: 'Amount',
                    filter: 'number',
					align:'right',
                    renderer: function (value, metaData, record, rowIndex) {
                        try {
							var currency = record.data.AmountFormat;
							if (currency == "dollar") 
                                currency = "$";
                            else
                                currency = "€";
                            return CDI.service.erdaAction.formatMoney(value, currency, 2, true, " ");
						} catch (err) { };

						return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,

                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_codeprojectform"],
                    dataIndex: 'CodeProjectForm',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_buyer"],
                    dataIndex: 'BaanIn_Buyer',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_conto"],
                    dataIndex: 'BaanIn_Conto',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-cdc"],
                    dataIndex: 'BaanIn_CDC',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_commessa"],
                    dataIndex: 'BaanIn_Commessa',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_magazzino"],
                    dataIndex: 'BaanIn_Magazzino',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_rigatesto"],
                    dataIndex: 'BaanIn_RigaTesto',
                    minWidth: 450,
                    //cls: 'multirowcol',
                    renderer: function (value, metaData) {
                        value = value.split(/\n/i).join('<br/>');
                        return '<div class="multirowcol">' + value + '</div>';
                    },
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_requester"],
                    dataIndex: 'BaanIn_Requester',
                    minWidth: 100,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-purchasing"],
                    dataIndex: 'Purchasing',
                    minWidth: 500,
                    //cls: 'multirowcol',
                    renderer: function (value, metaData) {
                        var cond = value.endsWith(";");
                        if (cond)
                            value = value.substring(0, value.length - 1);//value + ";";
                        var split = value.split(';');
                        if (split.length > 0) {
                            value = "";
                            for (i = 0; i < split.length; i++) {
                                value = value + split[i] + ";";
                                if (split[i + 1] != undefined && split[i + 1]!="")
                                    value = value + split[i+1] + ";";
                                value = value + "<br/>";
                                i++;
                            }
                        }
                        return '<div class="multirowcol">' + value + '</div>';
                    },
                    lockable: false,
                    sortable: false,
                    filter: true
                }
                //,
                //{
                //    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-notes"],
                //    dataIndex: 'Notes',
                //    minWidth: 500,
                //    //cls: 'multirowcol',
                //    renderer: function (value, metaData) {
                //        value = value.split(/\n/i).join('<br/>');
                //        return '<div class="multirowcol">' + value + '</div>';
                //    },
                //    autoSizeColumn: false,
                //    lockable: false,
                //    sortable: false,
                //    filter: true
                //}

            ],


        dockedItems: [{
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["dashboard-rda-worklist-grid-pagingtoolbar-emptyMsg"],
            listeners: {
                change: 'onPageChange',
            },
            items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("syncareapes").getController().onBeforeRender(this.up("syncareapes"));
                        }
                    }
                },

                {
                    xtype: 'button',
                    itemId: 'btnReject',
                    iconCls: 'fas fa-times',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-wfaction-reject"],
                    tooltip: CDI.service.Translate.data["rda-generic-rejectbjects-title"],
                    //  tooltipType: 'title',
                    listeners: {
                        click: 'onRejectClick'
                    }
                },
                {

                    xtype: 'button',
                    itemId: 'btnApprove',
                    iconCls: 'fas fa-share-alt',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["rda-generic-send-baan-objects-title"],
                    tooltip: CDI.service.Translate.data["rda-generic-send-baan-objects-title"],
                  //  tooltipType: 'title',
                    handler:'onSendToBaanClick'
                },
                {

                    xtype: 'button',
                    itemId: 'btnFastApprove',
                    iconCls: 'fas fa-road',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["rda-generic-fastclosure-title"],
                    tooltip: CDI.service.Translate.data["rda-generic-fastclosure-tooltip"],
                    //  tooltipType: 'title',
                    handler: 'onFastClosureClick'
                },
                {
                    xtype: 'atcview',
                    itemId: 'btnATC',

                }
            ]
        }]


    }
});

