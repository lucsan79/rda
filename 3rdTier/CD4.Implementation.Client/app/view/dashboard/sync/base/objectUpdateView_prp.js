﻿Ext.define('CDI.view.dashboard.sync.base.objectUpdateView_prp', {});
Ext.deferDefine('CDI.view.dashboard.sync.base.objectUpdateView_prp', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'rdaupdateprp',
        itemId: 'rdaupdateprp',
        //layout: 'fit',
        //height: '100%',
        //width: '100%',
        cls: 'easyeditview',

        controller: 'synccontroller',
        bodyPadding: '10px 20px',
        frame: true,
        scrollable: true,
        viewModel: {
            data: {
                changed: false
            }
        },
        defaults: {
            xtype: 'textfield',
            width: '100%',
            listeners: {
                change: function (field, value) {
                    field.lookupViewModel().set('changed', field.isDirty());
                }
            }
        },

        items: [
            {
                xtype: 'displayfield',
                itemId: 'eName',
                cls: 'code prp',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-code"],

            },
            {
                itemId: 'BaanIn_Commessa',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_commessa"],
            },
            {
                itemId: 'BaanIn_ATC',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-atc"],
            },
            {
                itemId: 'BaanIn_Articolo',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_articolo"],
            },
            {
                itemId: 'BaanIn_DescArticolo',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_descarticolo"],
            },
            {
                itemId: 'BaanIn_Magazzino',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_magazzino"],
            },
            {
                xtype: 'combobox',
                itemId: 'Supplier',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-supplier"],
                displayField: 'Value',
                valueField: 'Value',
                store: {
                    proxy: {
                        type: 'ajax',
                        withCredentials: true,
                        url: CD.apiUrl + 'erda/combobox/supplier',
                        autoload: false,
                        reader: {
                            type: 'json'
                        }
                    }
                },
                minChars: 1,
                ////queryMode: 'remote',
                filterPickList: false
            },
            {
                itemId: 'BaanIn_Acquisitore',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_acquisitore"],
            },
            {
                itemId: 'BaanIn_Pianificatore',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_pianificatore"],
            },
            {
                xtype: 'numberfield',
                itemId: 'TotalQuantity',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-totalquantity"],
            },
            {
                xtype: 'numberfield',
                itemId: 'PiecePrice',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-pieceprice"],
            },
            {
                xtype: 'combobox',
                itemId: 'PiecePriceFormat',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-piecepriceformat"],
                displayField: 'Value',
                valueField: 'Value',
                store: {
                    type: 'array',
                    fields: ['Value'],
                    data: [
                        ['euro'],
                        ['dollar'],
                    ]
                },
            },
            {
                xtype: 'datefield',
                itemId: 'BaanIn_DataConsegnaPianificata',
				format: Ext.decode(localStorage.localizationInfo).ShortDateFormat,
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_datadiconsegna"],
            },
            {
                itemId: 'BaanIn_Buyer',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_buyer"],
            },
            {
                itemId: 'BaanIn_RiferimentoA',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_riferimento_a"],
            },
            {
                itemId: 'BaanIn_RiferimentoB',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_riferimento_b"],
            },
            {
                itemId: 'BaanIn_RiferimentoC',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_riferimento_c"],
            },
            {
                xtype: 'textarea',
                itemId: 'BaanIn_RigaTesto',
                height: '150px',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_rigatesto"],
            },
            {
                itemId: 'BaanIn_GruppoArticoli',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_gruppoarticoli"],
            },
            {
                itemId: 'BaanIn_CommessaOrdinePRP',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_commassaprp"],
            },
            {
                itemId: 'BaanIn_Requester',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_requester"],
            },
            {
                xtype: 'textarea',
                itemId: 'Purchasing',
                height: '150px',
                fieldLabel: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-purchasing"],
            },

        ],
        fbar: [
            {
                xtype: 'button',
                disabled: true,
                iconCls: 'fas fa-ban',
                cls: 'bottombutton x-btn-plain-toolbar-small',
                text: CDI.service.Translate.data["rda-admincenter-cancelbtn-tooltip"],
                handler: 'onResetClick',
                bind: {
                    disabled: '{!changed}'
                }

            },
            {
                xtype: 'button',
                disabled: true,
                iconCls: 'fas fa-save',
                cls: 'bottombutton x-btn-plain-toolbar-small',
                text: CDI.service.Translate.data["rda-admincenter-savebtn-tooltip"],
                width: 150,
                handler: 'onCompleteClick',
                bind: {
                    disabled: '{!changed}'
                }
            }
        ],

    }
});

