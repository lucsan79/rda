﻿Ext.define('CDI.view.dashboard.sync.base.objectListView_sc', {});
Ext.deferDefine('CDI.view.dashboard.sync.base.objectListView_sc', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'syncdatagridsc',
        itemId: 'syncdatagridsc',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid',

        controller: 'synccontroller',
        mainStore: null,

        emptyText: CDI.service.Translate.data["dashboard-rda-worklist-grid-emptyText"],
        scrollable: true,

        listeners: {
            cellclick: 'onOpenBusinessData',
            beforerender: 'onGridBeforeRender'
        },

        syncRowHeight: true,
        viewConfig: {
           // stripeRows: false,
            loadMask: true,
            listeners: {
                //select: 'onRowSelect',
                select: 'onShowEditOptions',
                deselect: 'onRowDeselect',

                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        plugins: 'gridfilters',

        selModel: {
            //type: 'checkboxmodel'
            selType: 'checkboxmodel', //'rowmodel',
            //mode: 'SINGLE',
            checkOnly: 'true',
            allowDeselect: true
        },
        enableColumnHide: false,
        columns:
            [

                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-codemaster"],
                    dataIndex: 'eName',
                    width: 130,
                    cls: 'locked',
                    autoSizeColumn: false,
                    locked: true,
                    renderer: function (value, meta, record) {
                        var buttonText = "<i class='fas fa-donate'></i> " + value;
                        var extraparam = "";//(record.data.CBS.toLowerCase() == "materials" ? "material" : "");
                        return '<a title="' + CDI.service.Translate.data["dashboard-rda-cardbutton-open"] + '" role="button" class="openOnKey1 rdalinkbutton ' + extraparam + ' x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box" hidefocus="on" unselectable="on"><span role="presentation" class="x-btn-wrap" unselectable="on"><span class="x-btn-button" role="presentation"><span class="x-btn-inner x-btn-inner-center" unselectable="on">' + buttonText + '</span><span role="presentation" class="x-btn-icon-el" unselectable="on" style=""></span></span></span></a>';
                    }
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-description"],
                    dataIndex: 'eDescription',
                    minWidth: 100,
                    cls: 'extend',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                  //  renderer:'onGridRowSpan'
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-project"],
                    dataIndex: 'eProject',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                    //renderer: 'onGridRowSpan'
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-supplier"],
                    dataIndex: 'Supplier',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                    //renderer: 'onGridRowSpan'
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-totalamount"],
                    dataIndex: 'rdaw1_spendingcurve_totalAmount',
                    filter: 'number',
                    align:'right',
                    renderer: function (val) {
                        try {
                            return Ext.util.Format.currency(val, '€', 2, true, " ");
                        } catch (err) { };
                        
                        return "";
                    },
                    minWidth: 140,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,

                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-tranche"],
                    dataIndex: 'rdaw1_spendingcurve',
                    minWidth: 450,
                    cls: 'multirowcol',
                    //renderer: function (value, metaData) {
                    //    value = value.split(/\n/i).join('<br/>');
                    //    return '<div class="multirowcol">' + value + '</div>';
                    //},
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: false
                },
                //{
                //    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-amount"],
                //    dataIndex: 'Amount',
                //    filter: 'list',
                //    minWidth: 100,
                //    autoSizeColumn: true,
                //    lockable: false,
                //    sortable: false,
                //},
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-cdc"],
                    dataIndex: 'CDC',
                    filter: 'list',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_requester"],
                    dataIndex: 'eCreateUser',
                    minWidth: 100,
                    lockable: false,
                    sortable: false,
                    filter: true
                },
                {
                    text: ' ',
                    dataIndex: 'fake',
                    flex:1,
                    lockable: false,
                    sortable: false,
                    filter: false
                },
                //{
                //    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-notes"],
                //    dataIndex: 'Notes',
                //    minWidth: 500,
                //    //cls: 'multirowcol',
                //    renderer: function (value, metaData) {
                //        value = value.split(/\n/i).join('<br/>');
                //        return '<div class="multirowcol">' + value + '</div>';
                //    },
                //    autoSizeColumn: false,
                //    lockable: false,
                //    sortable: false,
                //    filter: true
                //}

            ],


        dockedItems: [{
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["dashboard-rda-worklist-grid-pagingtoolbar-emptyMsg"],

            items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("syncareasc").getController().onBeforeRender(this.up("syncareasc"));
                        }
                    }
                },
                {
                    xtype: 'button',
                    itemId: 'btnApprove',
                    iconCls: 'fas fa-check',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-wfaction-approve"],
                    tooltip: CDI.service.Translate.data["rda-generic-approveobjects-title"],
                  //  tooltipType: 'title',
                    listeners: {
                        click: function () {
                        }
                    }
                },
                
            ]
        }]


    }
});

