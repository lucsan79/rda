﻿Ext.define('CDI.view.dashboard.home.rdaHomeFilter', {});
Ext.deferDefine('CDI.view.dashboard.home.rdaHomeFilter', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'rdahomefilter',
        controller: 'rdahomeviewcontroller',
        itemId: 'rdahomefilter',
        cls: 'rdahomefilter',
        flex:1,
        bodyPadding: '2px 10px',
        listeners: {
           // beforerender: 'onBeforeRender',
           // afterrender: 'onAfterRender'
        },
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs: {
                style: {
                    width: '100%'
                }
            }
        },

        defaults: {
            cls:'filterfield'
        },
        items: [
            {
                xtype: 'tagfield',
                fieldLabel: 'Project',
                displayField: 'state',
                valueField: 'state',
                filterPickList: true,
                queryMode: 'local',
                publishes: 'value'
            },
            {
                xtype: 'tagfield',
                fieldLabel: 'CDC',
                displayField: 'state',
                valueField: 'state',
                filterPickList: true,
                queryMode: 'local',
                publishes: 'value'
            }, {
                xtype: 'tagfield',
                fieldLabel: 'Area',
                displayField: 'state',
                valueField: 'state',
                filterPickList: true,
                queryMode: 'local',
                publishes: 'value'
            }, {
                xtype: 'tagfield',
                fieldLabel: 'System',
                displayField: 'state',
                valueField: 'state',
                filterPickList: true,
                queryMode: 'local',
                publishes: 'value'
            }, {
                xtype: 'tagfield',
                fieldLabel: 'CBS',
                displayField: 'state',
                valueField: 'state',
                filterPickList: true,
                queryMode: 'local',
                publishes: 'value'
            }, {
                xtype: 'tagfield',
                fieldLabel: 'WBS',
                displayField: 'state',
                valueField: 'state',
                filterPickList: true,
                queryMode: 'local',
                publishes: 'value'
            }, {
                xtype: 'tagfield',
                fieldLabel: 'Owner',
                displayField: 'state',
                valueField: 'state',
                filterPickList: true,
                queryMode: 'local',
                publishes: 'value'
            }, {
                cls:'buttonFilters',
                items: [
                    {
                        buttons: [{
                            text: CDI.service.Translate.data["dashboard-rda-filter-apply"],
                            cls:'btn-create'
                        }, {
                            text: CDI.service.Translate.data["dashboard-rda-filter-clear"],
                            cls: 'btn-danger'
                        }]

                    }
                ]
                
                
            }




        ]




    }
});