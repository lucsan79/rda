﻿Ext.define('CDI.view.dashboard.home.rdaHomeView', {});
Ext.deferDefine('CDI.view.dashboard.home.rdaHomeView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'rdahomeview',
        controller: 'rdahomeviewcontroller',
        itemId: 'rdahomeview',
        cls: 'rdahomeview',
        layout: 'border',
        listeners: {
            beforerender: 'onBeforeRender',
            afterrender: 'onAfterRender'
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                title: '<i class="fa fa-filter"></i> <span class="headerL2">' + CDI.service.Translate.data['dashboard-rda-homefilter-label'] + '</span>',
                region: 'north',
                height: 200,
                minHeight: 200,
                maxHeight: 200,
                hidden:true,
                collapsible: true,
                collapsed: true,
                animCollapse: false,
                frame: true,
                cls: 'homeFilter',
                itemId: 'homeFilter',
                titleCollapse: true,
                items: [
                    {

                        xtype: 'rdahomefilter',

                    }
                ]

            },
            {
                split: true,
                collapsible: true,
                title: CDI.service.Translate.data["dashboard-rda-projectlist-label"],
                iconCls: 'x-fa fa-car',
                region: 'west',
                flex: 1,
                minWidth: 350,
                maxWidth: 350,
                cls: 'notopborder',
                itemId: 'notopborder',
                defaults: {
                    flex: 1,
                    xtype: 'grid',
                    frame: true,
                    scrollable: true,
                },

                items: [
                    {
                        split: true,
                        itemId: 'projectList',
                        cls: 'gridCardList cardwithbbar',
                        listeners: {
                            select: 'onComponentSelectRow',
                            deselect: 'onComponentDeSelectRow'
                        },
                        allowDeselect: true,
                        columns: {
                            defaults: {
                                tpl: '<div class="projectCard">' +
                                    '<div> ' +
                                    '<div class="projectTitle">{ProjectName}</div>' +
                                    '<div class="rdastatuslist">' +
                                    '<div class="active_true"><i class="{RdAInWorkingStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-working-label"] + '<span class="rdanumber">#{RdAInWorking}</span></div>' +
                                    '<div class="active_true"><i class="{RdAInValidationStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-prvalidation-label"] + '<span class="rdanumber">#{RdAInValidation}</span></div>' +
                                    '</div>' +
                                    '<div class="rdastatuslist">' +
                                    '<div class="active_true"><i class="{RdAInCMSValidationStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-cmsvalidation-label"] + '<span class="rdanumber">#{RdAInCMSValidation}</span></div>' +
                                    '<div class="active_true"><i class="{RdAInCreatedStatus}"></i> ' + CDI.service.Translate.data["dashboard-rda-created-label"] + '<span class="rdanumber">#{RdAInCreated}</span></div>' +
                                    '</div>' +
                                    '</div> ' +
                                    '</div>',
                            },
                            items: [
                                {
                                    xtype: 'templatecolumn',
                                    align: 'left',
                                    flex: 1
                                }]
                        },
                        bbar: [
                           
                            {
                                xtype: 'button',
                                itemId: 'btnReload',
                                iconCls: 'fas fa-sync-alt',
                                cls: 'pagingToolbarButton white',
                                text: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                                handler: function (btn) {
                                    
                                    var view = btn.up("rdahomeview");
                                    if (view != null) {
                                        view.fireEvent("beforerender", view);

                                    }
                                }

                            },

                        ]
                    },
                ]
            },
            {
                title: false,
                region: 'center',
                flex: 4,

                defaults: {
                    flex: 1,
                    margin: '0px 5px 0px 0px',
                    xtype: 'grid',
                    frame: true,
                    scrollable: true,
                    cls: 'gridCardList',
                    columns: {
                        defaults: {
                            tpl: '<div>' +
                                '   <div class= "rdaCard {Brand}" > ' +
                                '      <div class="header"> ' +
                                '          <div class="rdaInfo">' +
                                '              <div class="rdaCode"><i class="{rdaactionstatus}" title="{rdaactiontitle}"></i><span>{eName}<span></div>' +
                                '              <div class="rdaTitle">{eDescription}</div>' +
                                '              <div class="rdaCorderTopRight">' +
                                '                <a role="button" class="x-btn view" action="info" title="' + CDI.service.Translate.data['dashboard-rda-cardbutton-open'] + '"><i action="info"  class="fas fa-info-circle"></i></a>' +
                                '                <a role="button" class="x-btn reject {signstatus}" title="' + CDI.service.Translate.data['dashboard-rda-cardbutton-reject'] + '"><i  action="reject" class="fas fa-times-circle"></i></i></a>' +
                                '                <a role="button" class="x-btn approve {signstatus}" action="reject" title="' + CDI.service.Translate.data['dashboard-rda-cardbutton-approve'] + '"><i action="approve" class="fas fa-check-circle"></i></a>' +
                                '              </div>' +
                                '         </div>' +
                                '      </div>' +
                                '      <div class="rdadetails" style="margin-top:5px">' +
                                '         <div><span class="rdalabel">CDC</span><span class="rdavalue">{CDC}</span></div>' +
                                '         <div><span class="rdalabel">Area</span><span class="rdavalue">{Area}</span></div>' +
                                '      </div>' +
                                '      <div class="rdadetails">' +
                                '         <div><span class="rdalabel">System</span><span class="rdavalue">{System}</span></div>' +
                                '         <div><span class="rdalabel">CBS</span><span class="rdavalue">{CBS}</span></div>' +
                                '      </div>' +
                                '      <div class="rdadetails">' +
                                '         <div><span class="rdalabel">WBS</span><span class="rdavalue">{WBS}</span></div>' +
                                '         <div><span class="rdalabel">Owner</span><span class="rdavalue">{eOwner}</span></div>' +
                                '      </div>' +
                                '   </div>' +
                                '</div>'

                        },
                        items: [
                            {
                                xtype: 'templatecolumn',
                                align: 'left',
                                flex: 1,

                            }]
                    }
                },
                items: [
                    {
                        title: CDI.service.Translate.data["dashboard-rda-working-label"],
                        iconCls: 'x-fa fa-user-cog',
                        itemId: 'RdAWorking',

                        listeners: {
                            click: 'onRdaActionClick',
                            element: 'el',
                            delegate: 'div.rdaCorderTopRight'
                        },

                    },
                    {
                        title: CDI.service.Translate.data["dashboard-rda-prvalidation-label"],
                        iconCls: 'x-fa fa-user-check',
                        itemId: 'RdAPRValidation',
                        listeners: {
                            click: 'onRdaActionClick',
                            element: 'el',
                            delegate: 'div.rdaCorderTopRight'
                        },

                    },
                    {
                        title: CDI.service.Translate.data["dashboard-rda-cmsvalidation-label"],

                        iconCls: 'x-fa fa-comments-dollar',
                        itemId: 'RdACMSValidation',
                        listeners: {
                            click: 'onRdaActionClick',
                            element: 'el',
                            delegate: 'div.rdaCorderTopRight'
                        },

                    },
                    {
                        title: CDI.service.Translate.data["dashboard-rda-created-label"],
                        margin: '0px 0px 0px 0px',
                        iconCls: 'fas fa-money-check-alt',
                        itemId: 'RdACreated',
                        listeners: {
                            click: 'onRdaActionClick',
                            element: 'el',
                            delegate: 'div.rdaCorderTopRight'
                        },

                    }
                ]
            }
        ],

        


    }
});