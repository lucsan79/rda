﻿Ext.define('CDI.view.dashboard.home.rdaHomeViewController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.rdahomeviewcontroller',

        onBeforeRender: function (view) {

            var self = view;
            var gridProject = view.down('#projectList');
            //gridProject.setLoading(true);
            view.setLoading(true);
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/dashboard/getitems',
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    view.setLoading(false);
                }
            }).then(function (jsonData) {
                view.down('#projectList').setEmptyText("<h4 style='margin-top: -2px;'>" + CDI.service.Translate.data['rda-norda-to-manage'] + "</h4>");
                var data = Ext.decode(jsonData.responseText);
                var storeObject = { fields: [], data: [] };
                if (data != null) {
                    storeObject.data = data;
                }
                //gridProject.setLoading(false);

                var filterData = data.filterData;
                var projectStore = { fields: [], data: [] };
                if (data.projects != null) {
                    projectStore.data = data.projects;
                }
                view.down('#projectList').setStore(projectStore);
                view.getController().onComponentDeSelectRow(view.down('#projectList'), null);
                view.setLoading(false);
            });
        },

        onAfterRender: function (view) {
            //var test = view.down('#noScheduled').getStore();
            //console.log('after render store: ', test);


        },
        
        onComponentDeSelectRow: function (grid, record) {
            var currentView = grid.view.up("rdahomeview");
            var title = "";
            var myColumnWorkingGrid = currentView.down("#RdAWorking");
            title = CDI.service.Translate.data["dashboard-rda-working-label"];
            myColumnWorkingGrid.setLoading(true);
            var myWorkingStore = { fields: [], data: [] };
            myColumnWorkingGrid.setTitle(title);
            myColumnWorkingGrid.setStore(myWorkingStore);
            setTimeout(function () { myColumnWorkingGrid.setLoading(false); }, 200);

            /* PR VALIDATION */
            var myColumnPRGrid = currentView.down("#RdAPRValidation");
            title = CDI.service.Translate.data["dashboard-rda-prvalidation-label"];
            myColumnPRGrid.setLoading(true);
            var myPRStore = { fields: [], data: [] };
            myColumnPRGrid.setTitle(title);
            myColumnPRGrid.setStore(myPRStore);
            setTimeout(function () { myColumnPRGrid.setLoading(false); }, 200);

            /* CMS VALIDATION */

            var myColumnCMSGrid = currentView.down("#RdACMSValidation");
            title = CDI.service.Translate.data["dashboard-rda-cmsvalidation-label"];
            myColumnCMSGrid.setLoading(true);
            var myCMSStore = { fields: [], data: [] };
            myColumnCMSGrid.setTitle(title);
            myColumnCMSGrid.setStore(myCMSStore);
            setTimeout(function () { myColumnCMSGrid.setLoading(false); }, 200);


            /* Created */
            var myColumnCreatedGrid = currentView.down("#RdACreated");
            title = CDI.service.Translate.data["dashboard-rda-created-label"];
            myColumnCreatedGrid.setLoading(true);
            var myCreatedStore = { fields: [], data: [] };
            myColumnCreatedGrid.setTitle(title);
            myColumnCreatedGrid.setStore(myCreatedStore);
            setTimeout(function () { myColumnCreatedGrid.setLoading(false); }, 200);

        },

        onComponentSelectRow: function (grid, record) {
            
            var hasSelection = grid.getSelected().length == 0 ? false : true;
            if (!hasSelection)
                return;
            var currentView = grid.view.up("rdahomeview");
            var project = record.data.ProjectName;
            //recupero dati per progetto
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/dashboard/getobjectinproject',
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                },
                params: {
                    project: project
                },
                failure: function (response) {
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
					//
                    //var exceptionMessage = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);
                    ////var errorCode = exceptionMessage.ErrorCode;
                    //var errorMessage = "";
                    //try {
                    //    errorMessage = exceptionMessage.ErrorMessage;
                    //} catch (err) { errorMessage = exceptionMessage; }
                    //Ext.MessageBox.show({
                    //    title: CDI.service.Translate.data['generic-error-popup-title'],
                    //    msg: errorMessage,
                    //    icon: 'fa fa-times-circle fa-3x',
                    //    buttons: Ext.Msg.OK
                    //});
                    //gridProject.setLoading(false);
                    currentView.setLoading(false);
                }
            }).then(function (jsonData) {
                var data = Ext.decode(jsonData.responseText);
            /* WORKING */
                data = data.projects[0];
                var myColumnWorkingGrid = currentView.down("#RdAWorking");
                var title = CDI.service.Translate.data["dashboard-rda-working-label"];
                myColumnWorkingGrid.setLoading(true);
                var myWorkingStore = { fields: [], data: [] };
                if (data != null && data.RdAInWorkingList != null && hasSelection) {
                    myWorkingStore.data = data.RdAInWorkingList;
                    title = title + "<div class='divtotal'>#" + data.RdAInWorkingList.length + "</div>";
                }
                
                myColumnWorkingGrid.setTitle(title);
                myColumnWorkingGrid.setStore(myWorkingStore);
                setTimeout(function () { myColumnWorkingGrid.setLoading(false); }, 300);

                /* PR VALIDATION */
                var myColumnPRGrid = currentView.down("#RdAPRValidation");
                var title = CDI.service.Translate.data["dashboard-rda-prvalidation-label"];
                myColumnPRGrid.setLoading(true);
                var myPRStore = { fields: [], data: [] };
                if (data != null && data.RdAInValidationList != null && hasSelection) {
                    myPRStore.data = data.RdAInValidationList;
                    title = title + "<div class='divtotal'>#" + data.RdAInValidationList.length + "</div>";
                }
                myColumnPRGrid.setTitle(title);
                myColumnPRGrid.setStore(myPRStore);
                setTimeout(function () { myColumnPRGrid.setLoading(false); }, 300);

                /* CMS VALIDATION */

                var myColumnCMSGrid = currentView.down("#RdACMSValidation");
                var title = CDI.service.Translate.data["dashboard-rda-cmsvalidation-label"];
                myColumnCMSGrid.setLoading(true);
                var myCMSStore = { fields: [], data: [] };
                if (data != null && data.RdAInCMSValidationList != null && hasSelection) {
                    myCMSStore.data = data.RdAInCMSValidationList;
                    title = title + "<div class='divtotal'>#" + data.RdAInCMSValidationList.length + "</div>";
                }
                myColumnCMSGrid.setTitle(title);
                myColumnCMSGrid.setStore(myCMSStore);
                setTimeout(function () { myColumnCMSGrid.setLoading(false); }, 300);


                /* Created */

                var myColumnCreatedGrid = currentView.down("#RdACreated");
                var title = CDI.service.Translate.data["dashboard-rda-created-label"];
                myColumnCreatedGrid.setLoading(true);
                var myCreatedStore = { fields: [], data: [] };
                if (data != null && data.RdAInCreatedList != null && hasSelection) {
                    myCreatedStore.data = data.RdAInCreatedList;
                    title = title + "<div class='divtotal'>#" + data.RdAInCreatedList.length + "</div>";
                }
                myColumnCreatedGrid.setTitle(title);
                myColumnCreatedGrid.setStore(myCreatedStore);
                setTimeout(function () { myColumnCreatedGrid.setLoading(false); }, 300);


            });



        },


        onRdaActionClick: function (event) {
            var action = event.target.attributes.action.value;
            var rda = event.record.data;
            if (action == "info")
                CDI.service.erdaAction.open(rda.eId);
            else
                CDI.service.erdaAction.showWorkflowAction(this.getView().id, action, rda.eId, rda.eName,rda.eLevel);

        },

        doRefresh: function (data) {
            var grid = this.getView().down("#projectList");
            var records = grid.getSelection();
            var view = this.getView();
            if (records != null && records.length > 0) {

                Ext.Ajax.request({
                    url: CD.apiUrl + 'erda/dashboard/getitemsofproject',
                    method: 'GET',
                    withCredentials: true,
                    async: false,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        project: records[0].data.ProjectName
                    },
                    failure: function (response) {
                        this.onBeforeRender(view);
                    },
                    success: function (jsonData) {
                        var data = Ext.decode(jsonData.responseText);
                        records[0].data = data.projects[0];
                        records[0].commit();
                        view.getController().onComponentSelectRow(grid.getSelectionModel(), grid.getSelection()[0]);  
                        
                        
                              
                    }
                });
            }
            else
                this.onBeforeRender(this.getView());

        }


    });
