﻿Ext.define('CDI.view.dashboard.DashboardHome', {});

Ext.deferDefine('CDI.view.dashboard.DashboardHome', function () {
    return {
        extend: 'CD.view.dashboard.common.DashboardHome',
        xtype: 'dashboard_wrapper',

        listeners: {

            afterrender: function (me) {
                var twoRowsFlag = Ext.getBody().getViewSize().width < 1310 ? true : false;
                me.tab.setText(CDI.service.Translate.data['common-dashboard']);
                me.tab.setIconCls('x-fa fa-home');
                me.contentPane = this._renderContentPane(twoRowsFlag);
                me.add(me.contentPane);
                dashboardWrapper = me;
                if (CD.service.UserData.userData.policyAgree === undefined)
                    CD.service.UserData.userData.policyAgree = false;

                if (CD.service.UserData.userData.policyAgree == false) {
                    var win = new Ext.Window({
                        view:me,
                        cls: 'policy-win',
                        itemId: 'PolicyWin',
                        title: CDI.service.Translate.data['policy-popup-policy-title'],
                        width: "620px",
                        height: '90vh', closeAction: 'hide', buttonAlign: 'center',
                        closable: false,
                        modal: true,
                        animShow: function () {
                            this.el.slideIn('t', {
                                duration: 1, callback: function () {
                                    this.afterShow(true);
                                }, scope: this
                            });
                        },
                        animHide: function () {
                            this.el.disableShadow();
                            this.el.slideOut('t', {
                                duration: 1, callback: function () {
                                    this.el.hide();
                                    this.afterHide();
                                }, scope: this
                            });
                        },
                        layout: {
                            align: 'stretch',
                            type: 'vbox'
                        },
                        listeners: {
                            afterrender: {
                                //element: 'el', //bind to the underlying el property on the panel
                                fn: function () {
                                    var win = this;
                                    setTimeout(function () {
                                        $(Ext.query(".x-mask")).addClass("policyblack");
                                    }, 100);
                                    
                                }
                            }
                        },
                        items: [
                            {
                                cls: 'policy-content',
                                html: '<p>This computer, the information on it and on the networks to which it gives access, are the property of Maserati, and are protected by intellectual/industrial property rights of the company and/or of third parties. Unauthorized access is prohibited. The activities carried out by you through this computer may be monitored in accordance with the current italian legislation and pursuant to the internal procedures. By logging on to this computer you acknowledge what is stated in this notice and expressly undertake to comply with the provisions set out in the it security manual of Maserati. Violation of contract and/or law will be prosecuted. <br/><br/>' +
                                      'Questo computer, le informazioni residenti sullo stesso e sulle reti a cui de accesso sono proprieta di Maserati e sono oggetto di diritti di proprieta intellettuale/ industriale della societa e / o di terzi. L\'accesso non autorizzato risulta proibito. Le attivita da lei svolte tramite questo computer potranno essere oggetto di controllo in conformita\' a quanto previsto dalla normativa italiana vigente e dalle procedure aziendali in materia. L\'accesso a questo computer equivale a presa d\'atto di quanto sopra ed impegno a rispettare le disposizioni del manuale della sicurezza informatica di Maserati. Eventuali violazioni commesse saranno perseguite a norma di legge e contratto.'
                            }
                        ],
                        bbar: {
                            xtype: 'statusbar',
                            reference: 'basic-statusbar',
                            items: [
                                {
                                    html: ' ',
                                    cls: 'itemHidden'
                                },
                                {
                                    //xtype: 'button',
                                    text: CDI.service.Translate.data['policy-popup-button-agree'],
                                    cls: 'btn-danger policy',
                                    handler: function () {

                                        var item = win.down("createrdaview");
                                        if (item)
                                            item.getController().unloadScripts(item);
                                        $(Ext.query(".x-mask")).removeClass("policyblack");
                                        
                                        win.close();
                                        win.destroy();
                                        CD.service.UserData.userData.policyAgree = true;
                                    }
                                },
                            ]
                        }

                    });
                    win.show();
                    win.center();
                }
            }
        },

        _renderContentPane: function (twoRowsFlag) {
            return Ext.create('Ext.container.Container', {
                anchor: '100%, 100%',
                layout: 'fit',
                //padding: 5,
                items: [
                    {
                        xtype: 'overviewrda'
                    }
                ]
            });
        },


        _renderNavigatorPane: function () {
            var me = this;

            //build items from GLOBALS
            var items = [];
            var configPanel = [];
            Ext.Ajax.request({
                url: CD.apiUrl + 'dashboard/getactivepanels',
                method: 'GET',
                withCredentials: true,
                async: false,
                success: function (jsonData) {
                    var data = Ext.decode(jsonData.responseText);
                    if (data != null && data[0] != null && data[0] != "")
                        configPanel = data[0].split(";");
                }
            });

            var pane = Ext.create('Ext.container.Container', {
                height: globalsService.NAV_HEIGHT,
                layout: 'column',
                padding: 10,
                style: {
                    'background': 'transparent'
                },
                defaults: {
                    margin: '10 15 0 0' //top right bottom left (clockwise) margins of each item/column
                },
                'items': items
            });

            return pane;
        },

        isHome: function () {
            return this.currentDashboardId && this.currentDashboardId != 'HOME' ? false : true;
        },
    }
});

