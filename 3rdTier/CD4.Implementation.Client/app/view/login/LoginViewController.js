﻿Ext.define('CDI.view.login.LoginViewController', function () {
    return {

        extend: 'CD.view.login.LoginViewController',
        alias: 'controller.login',
        // ================
        // Lifecycle events
        // ================

        onBeforeRender: function (view) {
            //debugger;
            var query = window.location.href;
            if (query.indexOf("logout") != -1) {
                return;
            }
            if (query.indexOf("fastlogin=debug")!=-1) {
				return;
            }
            var userNameBox = view.down("#username");
            var passwordBox = view.down("#password");

            var btnStartLogin = view.down("#btnWinAuth");
            var btnStandardLogin = view.down("#btnLogin");
            var lblConnection = view.down("#userconneting");

            Ext.getCmp(userNameBox.id).disable();
            Ext.getCmp(passwordBox.id).disable();
            Ext.getCmp(btnStandardLogin.id).disable();

            $(btnStandardLogin.id).attr("disabled", "disabled");

           

            if (Ext.getCmp('viewport').logout)
                return;

            lblConnection.setValue("");
            var form = view;
            //debugger;
            if (view.afterlogin !== undefined && view.afterlogin !== null) {
                if (view.afterlogin == true) {
					
                    return;
                }
            }
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json'
                },
                withCredentials: true,
                url: CD.apiUrl + 'validatewinauth',
                success: function (data) {
                                         
                    var username = Ext.util.JSON.decode(data.responseText);
                    //username = "user windows account";
                    if (username != "") {
                        lblConnection.setValue("Connecting as <b>" + username + "</b>");
                        $("#" + btnStartLogin.id).css('background-color', '#4180be');
                        form.getController().countdown(form.getController(), btnStartLogin, 3);

                    }
                },
                failure: function (data) {

                }
            });
            
        },

        onLoginFormAfterRender: function (combo) {

            var recordSelected = combo.getStore().getAt(0);
            combo.setValue(recordSelected.get('abbr'));
            setTimeout(function () {
                Ext.getCmp(combo.up().down("#btnLogin").id).disable();
            }, 500);

            combo.up().down("#btnLogin").id.handler = null;
        },




    }
});