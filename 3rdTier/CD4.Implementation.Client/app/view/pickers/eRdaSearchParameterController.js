﻿Ext.define('CDI.view.pickers.eRdaSearchParameterController', {});
Ext.deferDefine('CDI.view.pickers.eRdaSearchParameterController', function () {
    return {
        extend: 'Ext.app.ViewController',
        alias: 'controller.erdasearchparameter',

        onBeforeRender: function (view) {
            view.setTitle(view.inputText.fieldLabel)
            view.setLoading(true);
            var selectProjectUserView = view;
            var gridPanel = selectProjectUserView.down('#gridPanel');
            selectProjectUserView.setLoading(true);

            selectProjectUserView.gridStore = null;
            gridPanel.removeAll();
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/triggers/' + view.inputText.itemId,
                method: 'GET',
                withCredentials: true,
                success: function (jsonData) {
                    var data = Ext.decode(jsonData.responseText);
                    var store = Ext.create('Ext.data.Store',
                        {
                            fields: [
                                "Name", "Description",
                            ],
                            data: data
                        });

                    selectProjectUserView.gridStore = store;

                    var grid = Ext.create('Ext.grid.Panel', {
                        store: selectProjectUserView.gridStore,
                        itemId: 'triggergrid',
                        cls: 'basicgrid',
                        layout: 'fit',
                        plugins: [
                            'gridfilters',
                        ],
                        enableColumnHide: false,
                        height: 507,
                        multiSelect: false,
                        allowDeselect: true,

                        columns: [
                            {
                                text: CDI.service.Translate.data["common-list-values-label"],
                                flex: 1,
                                align: 'left',
                                dataIndex: 'Name',
                                filter: true,
                            }
                        ]
                    });

                    gridPanel.add(grid);
                    gridPanel.updateLayout(true);
                    selectProjectUserView.setLoading(false);
                },
                failure: function (response) {
                    var exceptionMessage = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);
                    var errorCode = exceptionMessage.ErrorCode;
                    var errorMessage = exceptionMessage.ErrorMessage;
                    Ext.MessageBox.show({
                        title: 'Error: ' + errorCode,
                        msg: errorMessage,
                        icon: 'fa fa-times-circle fa-3x',
                        buttons: Ext.Msg.OK
                    });
                    view.setLoading(false);
                },
            });
        },

        onAfterRender: function (view) {

        },

        onCancelClick: function () {
            this.getView().close();
        },


        onSaveClick: function () {
            var selectProjectRoleView = this.getView();
            var certificateGrid = selectProjectRoleView.down('grid');
            var selectedValues = '';
            var selections = certificateGrid.getSelection();
            if (selections != null && selections.length > 0)
                selectedValues = selections[0].data.Name;
            var popup = this.getView();
            if (popup && popup.inputText) {
                popup.inputText.setValue(selectedValues);
                popup.close();
            }



        }







    }
});
