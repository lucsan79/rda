﻿Ext.define('CDI.view.pickers.eRdaSearchParameter', {});
Ext.deferDefine('CDI.view.pickers.eRdaSearchParameter', function () {
    return {
        extend: 'Ext.window.Window',
        xtype: 'erdasearchparameter',
        controller: 'erdasearchparameter',
        itemId: 'erdasearchparameter',
        listeners: {
            beforerender: 'onBeforeRender',
            afterrender: 'onAfterRender'
        },

        cls: 'gridPopupList cardwithbbar',
        iconCls: 'fas fa-search',
        modal: true,
        width: 500,
        height: 600,
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        resizable: false,
        items: [
            {
                xtype: 'container',
                defaults: {
                    flex: 1,
                    margin: '0px 5px 0px 0px',
                    frame: true,
                    scrollable: true,
                    cls: 'roleadmin basicgrid',
                    layout: {
                        type: 'hbox',
                        pack: 'start',
                        align: 'stretch'
                    },

                },
                itemId: 'gridPanel',
                items: [
                ]
            }

        ],
        bbar: [
            '->',
            
            {
                xtype: 'button',
                text: CDI.service.Translate.data["common-wfaction-cancel"],
                cls: 'pagingToolbarButton',
                iconCls: 'x-fa fa-ban',
                handler: 'onCancelClick'
            },
            { 
                xtype: 'button',
                text: CDI.service.Translate.data["common-wfaction-select"],
                 cls: 'pagingToolbarButton',
                iconCls: 'x-fa fa-check',
                handler: 'onSaveClick'
            }
        ]
    }
});