﻿Ext.define('CDI.view.main.MainViewController', {});

Ext.deferDefine('CDI.view.main.MainViewController', function () {
    return {
        extend: 'CD.view.main.MainViewController',
        alias: 'controller.rdamain',

        onNewRdaToolbarClick: function (btn) {
           var win = new Ext.Window({
                    
                    itemId:'DialogWin',
                    title: CDI.service.Translate.data['newrda-popup-title'],
                    width: "70vw",
                    height: '95vh', closeAction: 'hide', buttonAlign: 'center',
                    closable: false,
                    modal: true,
                    animShow: function () {
                        this.el.slideIn('t', {
                            duration: 1, callback: function () {
                                this.afterShow(true);
                            }, scope: this
                        });
                    },
                    animHide: function () {
                        this.el.disableShadow();
                        this.el.slideOut('t', {
                            duration: 1, callback: function () {
                                this.el.hide();
                                this.afterHide();
                            }, scope: this
                        });
                    },
                    layout: {
                        align: 'stretch',
                        type: 'vbox'
                    },
                    items: [
                        {
                            xtype: 'createrdaview',
                            flex: 1,
                        }
                    ],
                    bbar: {
                        xtype: 'statusbar',
                        reference: 'basic-statusbar',
                        items: [
                            {
                                html: ' ',
                                cls:'itemHidden'
                            },
                            {
                                //xtype: 'button',
                                text: CDI.service.Translate.data['newrda-popup-button-create'],
                                disabled: true,
                                id:'btnSave',
                                cls: 'btn-create btn-w-text',
                                handler: function () {
                                    var self = this;
                                    self.up("#DialogWin").down("statusbar").showBusy();
                                    var url = CD.apiUrl + "erda/dialog/save";
                                    var response = Ext.Ajax.request({
                                        url: url,
                                        method: 'GET',
                                        withCredentials: true,
                                        async: false,
                                        callback: function (opt, success, response) {
                                            if (!success) {
                                                try {
                                                    self.up("#DialogWin").down("statusbar").clearStatus();
                                                } catch (err) { };
                                                var exceptionMessage = Ext.decode(response.responseText).ExceptionMessage;
                                                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-dialog-title'], exceptionMessage,response);
                                            }
                                            else {
                                                var data = Ext.decode(response.responseText);
                                                var item = win.down("createrdaview");
                                                if (item)
                                                    item.getController().unloadScripts(item);
                                                win.close();
                                                win.destroy();
                                                CDI.service.erdaAction.open(data);
                                            }
                                        }
                                    });


                                    

                                }
                            },
                            {
                                id: 'btnExit',
                                //xtype: 'button',
                                text: CDI.service.Translate.data['newrda-popup-button-cancel'],
                                cls: 'btn-danger',
                                handler: function () {
                                    var item = win.down("createrdaview");
                                    if (item)
                                        item.getController().unloadScripts(item);
                                    win.close();
                                    win.destroy();
                                }
                            },
                        ]
                    }

                });
           win.show(Ext.getBody());
        },

        onAdminRdaToolbarClick: function (btn) {
            
            Ext.fireEvent('addTab', btn.text, 'rdaadmincenterwrapper', {}, true, null, btn.iconCls);
           
        },

        onMainViewAfterRender: function (view) {
            var toolbar = view.down("toolbar");
            var logoutButton = {
                iconCls: 'fas fa-power-off',
                iconAlign: 'right',
                ui: 'header',
                tooltip: CDI.service.Translate.data["navigation-logout"],
                cls: 'username logout',
                id: 'logout',
                height: '50px',
                handler: function () {
                    var self = Ext.getCmp('main').getController();

                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        withCredentials: true,
                        url: CD.apiUrl + 'logout'
                    }).then(function (data) {
                        var errorCode = Ext.util.JSON.decode(data.responseText);
                        if (errorCode == 0) {
                            //localStorage.clear();
							
                            Ext.getCmp('main').destroy();
                            CD.service.WebSockets.stop();
                            var loginView = Ext.getCmp('login');
                            if (loginView!=null)loginView.destroy();
                            loginView = Ext.create('CD.view.login.LoginView', {
                                afterlogin: true
                            });
                            
                            Ext.getCmp('viewport').add(loginView);

                            ////localStorage.clear();
                            ////Ext.getCmp('main').hide();
                            //CD.service.WebSockets.stop();



                            ////Ext.getCmp('viewport').add(
                            ////    Ext.create('CD.view.login.LoginView')
                            ////);
                            ////window.location.reload();
                            ////window.location.href = window.location.href;
                            //window.close();
                            ////window.location.href = window.location.href;
                        }
                    }, function (data) {

                        Ext.Msg.alert('Error', Ext.util.JSON.decode(data.responseText).Message, function () {
                            self.getView().show();
                        });
                    });
                }
            }
            toolbar.add(logoutButton);
            var editProfile = toolbar.down("#username");
            if (editProfile) {
                editProfile.handler = 'onEditClick';
                //editProfile.tooltip = CDI.service.Translate.data["navigation-edit-profile"];
                Ext.create('Ext.tip.ToolTip', {
                    target: editProfile.id,
                    html: CDI.service.Translate.data["navigation-edit-profile"]
                });
            }
            this.callParent(arguments);

        },

        onEditClick: function (view) {
            var myView = Ext.create("CD.view.helpers.editUserDetails.LogoutView");
            var panel = myView;//view.up('logoutview');
            var parentPanelImageHolder = panel.down('#profilePhoto');

            var special = {
                username: CD.service.UserData.userData.Username,
                parentPanelImageHolder: parentPanelImageHolder
            };

            var editProfileWindow = Ext.create('CD.view.helpers.Modal', {

                config: { showMask: true },
                itemId: 'editProfileWindow',
                title: CD.service.Translate.data['admin-organization-manageusers-editprofile'],
                iconCls: 'fa fa-pencil-alt',
                floating: true,
                closable: true,
                special: special,
                parentModal: view.modal,
                resizable: false,
                modal: true,
                header: 'Edit profile',
                
                config: {
                    showMask: true
                },

                items: [
                    {
                        xtype: 'edituserdetailsview',
                        autoScroll: true,
                        listeners: {
                            afterrender: function (view) {
                                view.up().setLoading(true);
                                setTimeout(function () {
                                    
                                    view.down("#btnSave").disable();
                                    view.down("#btnCancel").disable();
                                    view.down("#email").labelEl.update(CD.service.Translate.data['common-servername']);
                                    view.down("#email").setValue(CD.service.UserData.userData.ServerName);
                                    view.down("#phone").labelEl.update(CD.service.Translate.data['common-database']);
                                    view.down("#phone").setValue(CD.service.UserData.userData.DbName);
                                    view.down("#phone").disable();
                                    view.down("#email").disable();
                                    
                                    view.down("#oldpassword").hide();
                                    view.down("#newpassword").hide();
                                    view.down("#confirmpassword").hide();

                                    view.up().setLoading(false);
                                }, 500);

                            }
                        },
                        width: 500,
                        height: 480,
                        overoverflowY: 'auto',
                    }]
            });
            editProfileWindow.show();
            

        },


        onSearchClicked: function (view) {
            
            if (this.areMainStoresLoaded() === false) return;
            var searchText = Ext.getCmp('searchtext').getValue();
            if (!CD.service.UserData.userData.IsAdmin) {
                var match = new RegExp("R\\d\\d[-]\\d{5}");
                if (!match.test(searchText)) {
                    CDI.service.erdaAction.showErrorMessage("Quick Search", "Please fill a valid eRdA number like 'R20-XXXXX' or 'R19-XXXXX'", null);
                    return false;
                }
            }
            var contentPanel = Ext.getCmp('contentPanel');

            var activeTab;
            contentPanel.items.each(function (item) {
                if (item.xtype === 'searchresult' && item.special.data.quicksearch === searchText) {
                    activeTab = item;
                }
            });

            if (activeTab == null) {
                var tab = contentPanel.add({
                    xtype: 'searchresult',
                    title: '<i class="fa fa-search"></i>' + '  ' + '<span class="tab-title">' + (searchText == '' ? CD.service.Translate.data['search-allobjects'] : searchText) + '</span>',
                    cls: 'grid-picker',
                    closable: true,
                    reorderable: true,
                    special: {
                        name: view.special !== undefined && view.special.businessDataType !== undefined ? view.special.businessDataType : "",
                        type: 'searchResult',
                        data: {
                            quicksearch: searchText
                        }
                    }
                });

                contentPanel.setActiveTab(tab);

                var tabStates = Ext.state.Manager.get('tabs') || [];
                var tabState = {
                    xtype: 'searchresult',
                    title: '<i class="fa fa-search"></i>' + '  ' + '<span class="tab-title">' + (searchText == '' ? CD.service.Translate.data['search-allobjects'] : searchText) + '</span>',
                    closable: true,
                    special: {
                        name: view.special !== undefined && view.special.businessDataType !== undefined ? view.special.businessDataType : "",
                        type: 'searchResult',
                        data: {
                            quicksearch: searchText
                        }
                    },
                    node: null,
                    icon: null
                };
                tabStates.push(tabState);
                Ext.state.Manager.clear('tabs');
                Ext.state.Manager.set('tabs', tabStates);
            } else {
                contentPanel.setActiveTab(activeTab);
            }
        },
    }
});