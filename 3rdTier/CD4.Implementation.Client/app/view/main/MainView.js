﻿Ext.define('CDI.view.main.MainView', {});

Ext.deferDefine('CDI.view.main.MainView', function() { return {
    extend: 'CD.view.main.MainView',
    xtype: 'app-main',
    controller: 'rdamain',

    initComponent: function () {
        // Change logo path
        this.items[0].items[0].html = '<div class="main-logo"><img src="implementation-resources/images/logo-header.svg"></div>';
        var toolbarItems = this.items[0].items;
        var sliceNum = 4;
        var erdaadmin = {
            ui: 'header',
            appSettings: true,
            height: 50,
            iconAlign: 'left',
            cls: 'main-toolbar',
            handler: 'onAdminRdaToolbarClick',
            text: CDI.service.Translate.data["rda-admincenter-label"],
            iconCls: 'fas fa-tools',
            tooltip: CDI.service.Translate.data["rda-admincenter-tooltip"],
            itemId: 'headereRdaMenuAdmin',
            enabledGroupList: []
        };

        toolbarItems.splice(sliceNum, 0, erdaadmin);
        sliceNum = sliceNum + 1;

        //var erdaexport = {
        //    ui: 'header',
        //    appSettings: true,
        //    height: 50,
        //    iconAlign: 'left',
        //    cls: 'main-toolbar',
        //    handler: 'onExportRdaToolbarClick',
        //    text: CDI.service.Translate.data["rda-export-label"],
        //    iconCls: 'fas fa-book',
        //    tooltip: CDI.service.Translate.data["rda-export-tooltip"],
        //    itemId: 'headerMenuExport',
        //    enabledGroupList: []
        //};
        //toolbarItems.splice(sliceNum, 0, erdaexport);
        //sliceNum = sliceNum + 1;
        var newRdADialog = {
            ui: 'header',
            appSettings: true,
            height: 50,
            id: 'headerMenuNewRdA',
            itemId: 'headerMenuNewRdA',
            iconAlign: 'left',
            cls: 'main-toolbar new-button',
            handler: 'onNewRdaToolbarClick',
            text: CDI.service.Translate.data["rda-new"],
            iconCls: 'x-fa fa-plus',
            tooltip: CDI.service.Translate.data["rda-new-tooltip"],
            
            enabledGroupList: []
        };
        toolbarItems.splice(sliceNum, 0, newRdADialog);
        this.callSuper();
    }
}});