﻿Ext.define('CDI.view.components.applicationSection.search.SearchViewController',
{
    extend: 'CD.view.components.applicationSection.search.SearchViewController',
    alias: 'controller.search',
    // ----------------
    // Lifecycle events
    // ----------------
  
    // ------------
    // Click Events
    // ------------
    onSearchClick: function (btn) {
        var self = this,
            view = this.getView(),
            businessDataForm = btn.up().up().up(),
            generalAttributes = businessDataForm.down('#generalAttributesForm').getForm().getValues(),
            customAttributes = businessDataForm.down('#customAttributesForm').getForm().getValues(),
            mergedFormParams = {},
            cleanedParams = {},
            tab,
            contentPanel = Ext.getCmp('contentPanel');

        var valuesArray = Object.values(generalAttributes);
        var count = 0;
        for (var value of valuesArray) {
            value = CDI.service.erdaAction.replaceAll(value,"*", "");
            if (value != "" && value != "*" && value != "#" && value.length>=3) {
                count += 1;
            }
        }
		
        if (count == 0 && !CD.service.UserData.userData.IsAdmin) {
            var message = CDI.service.Translate.data['search-alert-no-standardattributes'];
            CDI.service.erdaAction.showErrorMessage("Search", message, null);
            return false;
        }
        valuesArray = Object.values(customAttributes);
        count = 0;
        for (var value of valuesArray) {
            value = CDI.service.erdaAction.replaceAll(value, "*", "");
            if (value != "" && value != "*" && value != "#" && value.length >= 3) {
                count += 1;
            }
        }
         if (count == 0 && !CD.service.UserData.userData.IsAdmin) {
            var message = CDI.service.Translate.data['search-alert-no-customattributes'];
            CDI.service.erdaAction.showErrorMessage("Search", message, null);
            return false;
        }

        this.callParent(arguments);
            
    }
});
