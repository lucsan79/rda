﻿Ext.define('CDI.view.components.applicationSection.search.SearchResultView', {});
Ext.deferDefine('CDI.view.components.applicationSection.search.SearchResultView', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'searchresult1',
        requires: [
            'Ext.panel.Panel',
            'Ext.toolbar.Paging',
            //'CD.view.components.applicationSection.search.SearchResultViewController'
        ],

        controller: 'searchResultView',
        layout: 'vbox',

        defaults: {
            height: '100%',
            width: '100%'
        },

        listeners: {
            beforerender: 'onSearchResultBeforeRender',
            afterrender: 'onSearchresultAfterRender'
        },
        items: [
            {
                hmtl:'lll'
            },
            {
                cls: 'no-cell-focus',
                xtype: 'grid',
                flex: 9.5,
                listeners: {
                    rowcontextmenu: 'onRowContextMenu'
                },
                plugins: ['gridfilters'],
                dockedItems: [{
                    xtype: 'cdpagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    listeners: {
                        change: 'onPagingClick'
                    },
                    //doRefresh : function(){
                    //    // Keep or remove these code
                    //    var grid = this.up();
                    //    grid.setLoading(true);
                    //    var me = this,
                    //        current = me.store.currentPage;

                    //    if (me.fireEvent('beforechange', me, current) !== false) {
                    //        me.store.loadPage(current);
                    //    }
                    //},
                    items: [{
                        xtype: 'button',
                        itemId: 'exportToExcelButton',
                        iconCls: 'fa fa-file-excel-o',
                        text: CD.service.Translate.data['common-excel'],
                        tooltip: CD.service.Translate.data['common-exportexcel'],
                        handler: 'onExportToExcel'
                    },
                        //{
                        //    xtype: 'button',
                        //    iconCls: 'fa fa-file-pdf-o',
                        //    text: CD.service.Translate.data['common-pdf'],
                        //    tooltip: CD.service.Translate.data['common-exportpdf'],
                        //    handler: 'onExportToPdf'
                        //}
                    ]
                }],
                viewConfig: {
                    getRowClass: function (record) {
                        if (record && record.get('CanRead') === false) return 'grid-disabled-row';
                    }
                }
            }
        ]
    }
});