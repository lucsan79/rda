﻿Ext.define('CDI.view.components.applicationSection.search.data.gridViewController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.searchresultgridcontroller',

       
        onBeforeRender: function (grid) {
            var pagingtoolbar = grid.down("pagingtoolbar");
            pagingtoolbar.down('#refresh').hide();
            grid.up().up().setLoading(true);

            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json'
                },
                withCredentials: true,
                method: 'POST',
                disableCaching: false,
                jsonData: {
                    AttributeColumns: grid.special.configuration || null,
                    Data: grid.special.data
                },
                url: CD.apiUrl + 'erda/search',
                failure: function (response) {
                    grid.up().up().setLoading(false);
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-security-alert'], "", response);
                },
                success: function (jsonData) {
                    var data = Ext.decode(jsonData.responseText);
                    
                    var rdaStore = Ext.create('CDI.store.rdaSimpleStore', {
                        remoteFilter: true,
                        data: data.rdafullList,
                        customfilter: data.filter
                    });
                    grid.setStore(rdaStore);
                    setTimeout(function () { grid.up().up().setLoading(false); }, 200);
                    
                }
            });

        },


        
        cellclick: function (gridview, tdEl, cellIndex, record, trEl, rowIndex, e) {
            if (tdEl.innerHTML.indexOf("rdalinkbutton") != -1)
                CDI.service.erdaAction.open(record.get('eId'));
        }

    });
