﻿Ext.define('CDI.view.components.applicationSection.search.data.gridView', {});
Ext.deferDefine('CDI.view.components.applicationSection.search.data.gridView', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'searchresultgrid',
        controller: 'searchresultgridcontroller',
        itemId: 'searchresultgrid',
        cls: 'basicgrid resultgrid',


        emptyText: CDI.service.Translate.data["dashboard-rda-worklist-grid-emptyText"],
        scrollable: true,
        margin: '-1px 0 0 0',
        listeners: {
            beforerender: 'onBeforeRender',
            cellclick: 'cellclick'
           // forceRender:'onForceRender',
           // destroyStore: 'onDestroyStore'
        },


        emptyText: CDI.service.Translate.data["rda-norda-found"],

        syncRowHeight: true,
        viewConfig: {
            loadMask: true,
            listeners: {
                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        plugins: 'gridfilters',
        
        
        columns:
            [
                //{
           
            //defaults: {
            //    minWidth: 100,
            //    autoSizeColumn: true,
            //    filter: true,
            //    lockable: false,
            //    sortable: false,
               
            //},
            
            //items: [
               
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-code"],
                    dataIndex: 'eName',
                    width: 120,
                    cls:'locked',
                    autoSizeColumn: false,
                    locked: true,
                    renderer: function (value, meta, record) {
                        var buttonText = "<i class='fas fa-donate'></i> " + value;
                        return '<a title="' + CDI.service.Translate.data["dashboard-rda-cardbutton-open"] + '" role="button" class="rdalinkbutton x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box" hidefocus="on" unselectable="on"><span role="presentation" class="x-btn-wrap" unselectable="on"><span class="x-btn-button" role="presentation"><span class="x-btn-inner x-btn-inner-center" unselectable="on">' + buttonText + '</span><span role="presentation" class="x-btn-icon-el" unselectable="on" style=""></span></span></span></a>';
                    }
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-description"],
                    dataIndex: 'eDescription',
                    minWidth: 250,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                    renderer: function (value, metaData) {
                        value = value.split(/\n/i).join('<br/>');
                        return '<div class="multirowcol">' + value + '</div>';
                    },
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-prp"],
                    dataIndex: 'PRP',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },

                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-baancode"],
                    dataIndex: 'RDACode',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-oda"],
                    dataIndex: 'baan_oda',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-baanstatus"],
                    dataIndex: 'baan_statorda',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },

                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-createdate"],
                    dataIndex: 'eCreateDate',
                    xtype: 'datecolumn',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            //Fix d/m/Y format
                            var dateArray = v.toString().split('/');
                            if (dateArray.length == 3) {
                                v = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                            }
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },

                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-project"],
                    dataIndex: 'eProject',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-supplier"],
                    dataIndex: 'Supplier',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-amount"],
                    dataIndex: 'Amount',
                    filter: 'number',
					align:'right',
                    renderer: function (value, metaData, record, rowIndex) {
                        try {
							var currency = record.data.AmountFormat;
                            if (currency == "dollar") 
                                currency = "$";
                            else
                                currency = "€";
                            return CDI.service.erdaAction.formatMoney(value, currency, 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-activitystart"],
                    dataIndex: 'ActivityStart',
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            //Fix d/m/Y format
                            var dateArray = v.toString().split('/');
                            if (dateArray.length == 3) {
                                v = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                            }
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-activityend"],
                    dataIndex: 'ActivityEnd',
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            //Fix d/m/Y format
                            var dateArray = v.toString().split('/');
                            if (dateArray.length == 3) {
                                v = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                            }
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-cdc"],
                    dataIndex: 'CDC',
                    filter: true,
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-area"],
                    dataIndex: 'Area',
                    filter: true,
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-cbs"],
                    dataIndex: 'CBS',
                    filter: true,
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-totalquantity"],
                    dataIndex: 'TotalQuantity',
                    filter: 'number',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-pieceprice"],
                    dataIndex: 'PiecePrice',
                    filter: 'number',
					align:'right',
                    renderer: function (value, metaData, record, rowIndex) {
						try {
							var currency = record.data.PiecePriceFormat;
                            if (currency == "dollar") 
                                currency = "$";
                            else
                                currency = "€";
                            return CDI.service.erdaAction.formatMoney(value, currency, 2, true, " ");
                        } catch (err) { };

                        return "";
						
                       
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-status"],
                    dataIndex: 'eLevel',
                    filter: true,
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-requester"],
                    dataIndex: 'eCreateUser',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter:true
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-engaged"],
                    dataIndex: 'BAAN_IMPEGNATO',
                    filter: 'number',
					align:'right',
                    renderer: function (val) {
                        try {

                            
                            return Ext.util.Format.currency(val, '€', 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-recived"],
                    dataIndex: 'BAAN_RICEVUTO',
                    filter: 'number',
					align:'right',
                    renderer: function (val) {
                        try {
                            return Ext.util.Format.currency(val, '€', 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-ordered"],
                    dataIndex: 'BAAN_ORDINATO',
                    filter: 'number',
					align:'right',
                    renderer: function (val) {
                        try {

                            return Ext.util.Format.currency(val, '€', 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-paid"],
                    dataIndex: 'BAAN_LIQUIDATO',
                    filter: 'number',
					align:'right',
                    renderer: function (val) {
                        try {

                            return Ext.util.Format.currency(val, '€', 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-committed"],
                    dataIndex: 'BAAN_COMMITTED',
                    filter: 'number',
					align:'right',
                    renderer: function (val) {
                        try {

                            return Ext.util.Format.currency(val, '€', 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-invoiced"],
                    dataIndex: 'BAAN_INVOICED',
                    filter: 'number',
					align:'right',
                    renderer: function (val) {
                        try {

                            return Ext.util.Format.currency(val, '€', 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
          
            ],
        
        
        dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["dashboard-rda-worklist-grid-pagingtoolbar-emptyMsg"],
            items: [
                //{
                //    xtype: 'button',
                //    itemId: 'btnRefresh',
                //    iconCls: 'fas fa-sync-alt',
                //    cls: 'pagingToolbarButton',
                //    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                //    //tooltipType: 'title',
                //    listeners: {
                //        click: function () {
                //            this.up("archiveview").getController().onBeforeRender(this.up("archiveview"));
                //            this.up("archivedatagrid").getController().onBeforeRender(this.up("archivedatagrid"));
                //        }
                //    }
                //},
                {
                    xtype: 'button',
                    itemId: 'btnExport',
                    iconCls: 'fas fa-file-excel',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-label-export"],
                    tooltip: CDI.service.Translate.data["common-label-export-tooltip"],
                    //    tooltipType: 'title',
                    listeners: {
                        click: function (btn) {
                            var grid = btn.up("grid");
                            var options = {
                                title: CDI.service.Translate.data["dashboard-rda-searchresult"],
                                filter: grid.store.customfilter
                            }
                            CDI.service.erdaAction.showExportPanel("rdasearch", options);
                        }
                    }
                },
            ]
        //    items: [
                
        //        {
        //            xtype: 'button',
        //            iconCls: 'fas fa-check',
        //            cls:'pagingToolbarButton'
        //        },
        //        {
        //            xtype: 'button',
        //            iconCls: 'fas fa-ban',
        //            cls: 'pagingToolbarButton'
                    
        //        }
        //    ]
        }]


    }
});

