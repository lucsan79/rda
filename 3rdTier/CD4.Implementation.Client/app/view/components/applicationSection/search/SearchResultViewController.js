﻿Ext.define('CDI.view.components.applicationSection.search.SearchResultViewController',
    {
        extend: 'CD.view.components.applicationSection.search.SearchResultViewController',
        alias: 'controller.searchResultView',

        // ================
        // Lifecycle events
        // ================
        onSearchResultBeforeRender: function (view) {
            if (view.special.data.quicksearch===undefined  && view.special.businessDataType != "RDA") {
                this.callParent(arguments);
            }
            else {
                Ext.resumeLayouts();
                var panel = Ext.create('CDI.view.components.applicationSection.search.data.gridView', {
                    special:view.special
                });
                var searchResultContainer = view.up("#searchResultContainer");
                if (searchResultContainer === undefined)
                {
                    view.items.items[0].destroy();
                    view.add(panel);
                }
                else {
                    view.hide();
                    searchResultContainer.add(panel);
                }
                
            }
            
           
           

        },


    });
