﻿Ext.define('CDI.view.components.contentSection.workflow.WorkflowItemViewController', {});

Ext.deferDefine('CDI.view.components.contentSection.workflow.WorkflowItemViewController', function () {
    return {
        extend: 'CD.view.components.contentSection.workflow.WorkflowItemViewController',
        alias: 'controller.workflowitem',

        // -----------------
        // Private functions
        // -----------------
        renderWorkflowItem: function (view) {
            
            var self = this;
            var data = self.workflowItemData;
            var parentView = view.up();
            var businessDataWrapper
            if (parentView != null) {
                businessDataWrapper = parentView.up('businessdatawrapper');
            }
            //var businessDataWrapper = parentView.up('businessdatawrapper');

            var itemsArray = [
                //{
                //    html: '<strong>' +
                //        CD.service.Translate.data['businessdata-workflow-requestlevel'] +
                //        ': </strong>' +
                //        data[0].Step
                //},
                //{
                //    html: '<strong>' +
                //        CD.service.Translate.data['businessdata-workflow-quorum'] +
                //        ': </strong> ' +
                //        data[0].Quorum
                //}
            ];
            
            if (view.special.businessDataType == "RDA" && view.special.level == "PR Validation") {
                var response = Ext.Ajax.request({
                    url: CD.apiUrl + 'erda/sign/' + view.special.businessDataId,
                    method: "GET",
                    withCredentials: true,
                    async: false,
                    callback: function (opt, success, response) {
                        if (success) {
                            var rdaSign = Ext.decode(response.responseText);
                            data[0].RoleUserList.push(rdaSign);
                        }
                    }
                });
            }
            
            if (data[0].RoleUserList != null && data[0].RoleUserList.length > 0) {
                var hString ='<table class="tableRoleUsers">';
                Ext.Array.each(data[0].RoleUserList, function (item) {
                    if (item.Role != null) {
                        if (item.Users == null)
                            item.Users = "n.d.";
                        hString = hString + '<tr><td class="rolecell">' + item.Role + '</td><td class="usercell">' + item.Users + '</td></td>';
                    }
                        
                });
                hString = hString + '</table>';
                itemsArray.push({ html: hString });

            }
            
            var disableAction = false;
            if ("UNDER CONTROL;CONTROLLED;CLOSED".indexOf(view.special.level.toUpperCase()) != -1 )
                if (view.special.businessDataType == "RDA" && !CD.service.UserData.userData.IsAdmin)
                    disableAction = true;

            if (!disableAction && !CD.service.UserData.userData.IsAdmin)
                disableAction = !data[0].IsEditable || (businessDataWrapper != null && businessDataWrapper.reserveFlag === true ? true : false);

            var workflowItemContainer = view.down('#workflowItemContainer');
            var workflowItem = Ext.create("Ext.Panel", {
                frame: true,
                //frameWidth: '1px',
                title: '<i class="fa fa-check"></i> ' + CD.service.Translate.data['businessdata-workflow-currentlevel'] + ': ' + data[0].Level,
                animCollapse: false,
                collapsible: true,
                collapsed: true,
                border: false,
                bodyPadding: 10,
                layout: 'hbox',
                special: {
                    parentSpecial: view.special,
                    data: data[0]
                },
                items: [
                    {
                        width: '50%',
                        height: '80px',
                        items: itemsArray
                    },
                    {
                        xtype: 'textareafield',
                        itemId: 'txtComment',
                        height: '114px',
                        fieldLabel: CD.service.Translate.data['common-comment'],
                        disabled: !data[0].IsEditable || (businessDataWrapper != null && businessDataWrapper.reserveFlag === true ? true : false),
                        name: 'pass',
                        cls: 'inner-label',
                        width: '48%',
                        style: {
                            'padding-left': '3px !important',
                            
                        }

                    }
                ],
                buttons: [
                    {
                        text: CD.service.Translate.data['businessdata-workflow-view'],
                        iconCls: 'fa fa-eye',
                        cls: 'btn-info',
                        handler: 'onViewClick',
                        hidden: !CD.service.UserData.userData.IsAdmin
                    },
                    {
                        text: CD.service.Translate.data['businessdata-workflow-changelevel'],
                        iconCls: 'fa fa-exchange-alt',
                        handler: 'onChangeLevelClick',
                        cls: 'btn-info',
                        hidden: !CD.service.UserData.userData.IsAdmin
                    },
                    {
                        text: CD.service.Translate.data['businessdata-workflow-reject'],
                        iconCls: 'fa fa-times',
                        handler: 'onRejectClick',
                        cls: 'btn-danger',
                        disabled:  disableAction
                    },
                    {
                        text: CD.service.Translate.data['businessdata-workflow-approve'],
                        iconCls: 'fa fa-check',
                        handler: 'onApproveClick',
                        cls: 'btn-success',
                        style: {
                            'margin-right':'10px'
                        },
                        disabled: false//!data[0].IsEditable || (businessDataWrapper != null && businessDataWrapper.reserveFlag === true ? true : false) || disableAction
                    }
                   
                ],
                listeners: {
                    expand: function () {
                        parentView.updateLayout();
                    },
                    collapse: function () {
                        parentView.updateLayout();
                    }
                }
            });

            if (view.special.viewMode === 'relation') {
                workflowItem.setCollapsible(false);
            }

            var workflowTerminatedItem = Ext.create("Ext.Panel", {
                title: '<div style="text-align:center;"><i class="fa fa-flag-checkered" ></i> ' + CD.service.Translate.data['businessdata-workflow-currentlevel'] + ': <span style="color:green !important; font-weight:bold; ">' + data[0].Level + '</span></div>',
                layout: 'hbox',
                special: {
                    data: data[0]
                }
            });

            if (data[0].IsWorkflowEnded)
                workflowItemContainer.add(workflowTerminatedItem);
            else {
                workflowItemContainer.add(workflowItem);
                if (view.special.viewMode !== 'relation') {
                    workflowItem.setCollapsed(false);
                }
            }
            view.fireEvent('loaded', view);

            //Ext.suspendLayouts();
            //Ext.resumeLayouts(true);
        },


        onApproveClick: function (button) {

            var view = button.up();
            var special = view.up().special.data;
            var workflowView = view.up('workflow');
            var comment = view.up().getComponent('txtComment').value;

            var businessdatawrapper = {};
            for (var index in view.up('#contentPanel').items.items) {
                var item = view.up('#contentPanel').items.items[index];
                if (item.xtype == 'businessdatawrapper') {
                    if (item.special.businessDataId == special.Id) {
                        businessdatawrapper = item;
                    }
                }

            }
            if (businessdatawrapper) {
                businessdatawrapper = view.up('businessdatawrapper');
            }

            var form = businessdatawrapper.down('businessdataform');

            if (!this.checkMandatoryAttributesFilled(form))
                return;

            var extraOption = {
                url: CD.apiUrl + 'workflow/accept?ID=' + special.Id
                    + '&CurrentCheckType=' + special.CurrentCheckType
                    + '&Comment=' + comment
                    + '&Level=' + special.Level,
                commect:comment
            };
            CDI.service.erdaAction.showWorkflowAction(this.getView().id, "approve", businessdatawrapper.special.businessDataId, businessdatawrapper.special.itemName, businessdatawrapper.special.level, extraOption);

            
        },



        onRejectClick: function (button) {
            var view = button.up();
            var special = view.up().special.data;
            var comment = view.up().getComponent('txtComment').value;
            if (comment == "") {
                view.up().getComponent('txtComment').addCls('invalid')
                var message = CDI.service.Translate.data["common-question-comment-alert"];
                CDI.service.erdaAction.showToastError(message);
                return;
            }
            
            var businessdatawrapper = {};
            for (var index in view.up('#contentPanel').items.items) {
                var item = view.up('#contentPanel').items.items[index];
                if (item.xtype == 'businessdatawrapper') {
                    if (item.special.businessDataId == special.Id) {
                        businessdatawrapper = item;
                    }
                }

            }
            if (businessdatawrapper) {
                businessdatawrapper = view.up('businessdatawrapper');
            }

            var extraOption = {
                url: CD.apiUrl + 'workflow/reject?ID=' + special.Id
                    + '&CurrentCheckType=' + special.CurrentCheckType
                    + '&Comment=' + comment
                    + '&Level=' + special.Level,
                comment: comment
            };
            CDI.service.erdaAction.showWorkflowAction(this.getView().id, "reject", businessdatawrapper.special.businessDataId, businessdatawrapper.special.itemName, businessdatawrapper.special.level,extraOption);

            
        },


        doRefresh: function (data, successflag) {
            
            var view = this.getView();
            //var data = Ext.decode(jsonData.responseText);
            if (successflag) {
                Ext.toast({
                    html: '<i class="fa fa-check"></i> ' + CD.service.Translate.data['businessdata-workflow-acceptrequestsuccess-toast'],
                    bodyStyle: 'background-color: green; color: white',
                    align: 'tr',
                    anchor: 'contentPanel'
                });

            }
            var businessDataWrapper = view.up('businessdatawrapper');
            var contentPanel = businessDataWrapper.up('#contentPanel');
            var businessDataWrapperIndex = contentPanel.items.indexOf(businessDataWrapper);
            var special = Ext.clone(businessDataWrapper.special);
            special.level = data;
            Ext.suspendLayouts();
            businessDataWrapper.close();
            Ext.fireEvent('addTab', special.businessDataType + ' ' + special.itemName + ' ' + special.revision, 'businessdatawrapper',
                special, true, null, 'gear', businessDataWrapperIndex);
            Ext.resumeLayouts(true);

        },

        
    }
});