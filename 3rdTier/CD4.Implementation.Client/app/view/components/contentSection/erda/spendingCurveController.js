﻿Ext.define('CDI.view.components.contentSection.erda.spendingCurveController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.spendingcurvecontroller',

    onBeforeRender: function (view) {

        var self = view;
        var gridSpending = view.down('#spendinglist');
        //gridProject.setLoading(true);
        view.setLoading(true);
        Ext.Ajax.request({
            url: CD.apiUrl + 'erda/spending/' + view.special.businessDataId,
            method: 'GET',
            withCredentials: true,
            failure: function (response) {
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                view.setLoading(false);
            }
        }).then(function (jsonData) {
            var data = Ext.decode(jsonData.responseText);
            var spendingStore = { fields: [], data: [] };
            if (data != null) {
                spendingStore.data = data;
            }
            gridSpending.setStore(spendingStore);
            view.setLoading(false);
        });
    },
    onRdaActionClick: function (event) {
        CDI.service.erdaAction.open(event.record.id);
    },
   

});