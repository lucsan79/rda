﻿Ext.define('CDI.view.components.contentSection.erda.spendingCurve', {});
Ext.deferDefine('CDI.view.components.contentSection.erda.spendingCurve', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'spendingcurve',
        cls: 'spendingcurvepanel',
        controller: 'spendingcurvecontroller',
        itemId: 'spendingcurve',

        autoScroll: true,
        cdTitle: CDI.service.Translate.data['dashboard-rda-list-button-spendingcurve'],
        cdIcon: 'file-invoice-dollar',
        enabledLevelList: [],
        enabledRoleList: [],
        enabledBusinessDataTypesList: [],
        listeners: {
            beforerender: 'onBeforeRender',
        },
        bodyPadding: 20,
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },

        items: [
            {
                title: false,
                region: 'center',

                defaults: {
                    flex: 1,
                    xtype: 'grid',
                    frame: true,
                    scrollable: true,
                    cls: 'gridCardList spendingcard',
                    columns: {
                        defaults: {
                            tpl: '<table class="spending-grid-table">' +
                                '   <tr> ' +
                                //'      <td class="box">' +
                                //'           <a title="View More" refId="{id}" role="button" class="rdalinkbutton  x-btn x-unselectable x-btn -default-small x-noicon x-btn - noicon x-btn -default-small - noicon x-border - box" hidefocus="on" unselectable="on"><span role="presentation" class="x - btn - wrap" unselectable="on"><span class="x - btn - button" role="presentation"><span class="x - btn - inner x-btn - inner - center" unselectable="on"><i class="fas fa-file-invoice-dollar"></i></span><span role="presentation" class="x - btn - icon - el" unselectable="on" style=""></span></span></span></a>' +
                                //'      </td>' +
                                '      <td class= "box x-grid-cell x-grid-td x-grid-cell-gridcolumn-1167 x-grid-cell-first x-grid-cell-last x-unselectable" style="width:120px;" role="gridcell" tabindex="-1" data-columnid="gridcolumn-1167"> <div unselectable="on" class="x-grid-cell-inner " style="text-align:left;"><a title="View More" role="button" class="openOnKey1 rdalinkbutton  x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box" hidefocus="on" unselectable="on"><span role="presentation" class="x-btn-wrap" unselectable="on"><span class="x-btn-button" role="presentation"><span class="x-btn-inner x-btn-inner-center" unselectable="on"><i class="fas fa-donate"></i> {code}</span><span role="presentation" class="x-btn-icon-el" unselectable="on" style=""></span></span></span></a></div></td>'   +
                                '      <td class="tranchedetail">' +
                                '         <span class="rdavalue">{title}</span>' +
                                '      </td>' +
                                '      <td class="tranchestatus">' +
                                '         <span class="rdavalue">{level}</span>' +
                                '      </td>' +
                                '   </tr>' +
                                '</table>'

                        },
                        items: [
                            {
                                xtype: 'templatecolumn',
                                align: 'left',
                                flex: 1,

                            }]
                    }
                },
                items: [
                    {
                        itemId: 'spendinglist',
                        listeners: {
                            click: 'onRdaActionClick',
                            element: 'el',
                            delegate: 'td.box'
                        },
                    },
                ]
            }
        ]
    }
});