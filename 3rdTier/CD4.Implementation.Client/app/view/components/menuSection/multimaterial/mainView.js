﻿Ext.define('CDI.view.components.menuSection.multimaterial.mainView', {});
Ext.deferDefine('CDI.view.components.menuSection.multimaterial.mainView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'importmaterial',
        controller: 'importmaterialcontroller',
        itemId: 'importmaterial',
        cls: 'gray-window-title worklistview',
        layout: 'border',
        padding:'0px 5px 5px 0px',
        title: '<i class="fas fa-cubes"></i> <span class="headerL2">' + CDI.service.Translate.data['dashboard-rda-multirda-grid-label'] + '</span>',
        cdTitle: CDI.service.Translate.data['dashboard-rda-list-button-multicreation'],
        cdTooltip: CDI.service.Translate.data['dashboard-rda-list-button-multicreation'],
        cdIcon: 'fa20 fas fa-cubes',
        listeners: {
            afterrender: 'onAfterRender',
            //showView:'onShowView'
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
           {
                region: 'center',
                flex: 4,
                itemId: 'objectPanelView',
                defaults: {
                    flex: 1,
                    frame: true,
                    scrollable: true,
                    cls:'basicgrid column-header-major'
                },
                items: [
                    {
                        xtype: 'materialgrid',

                    }
                ]
            }
        ]

        

    }
});