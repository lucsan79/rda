﻿Ext.define('CDI.view.components.menuSection.multimaterial.objectListView', {});
Ext.deferDefine('CDI.view.components.menuSection.multimaterial.objectListView', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'materialgrid',
        itemId: 'materialgrid',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid',

        controller: 'importmaterialcontroller',
        columnLines: true,
        emptyText: CDI.service.Translate.data["dashboard-rda-worklist-grid-emptyText"],
        scrollable: true,

        listeners: {
            cellclick: 'onOpenBusinessData',
            beforerender: 'onBeforeRender'
        },

        syncRowHeight: true,
        viewConfig: {
            loadMask: true,
            listeners: {
               
                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        plugins: 'gridfilters',

        //selModel: {
        //    //type: 'checkboxmodel'
        //    selType: 'checkboxmodel', //'rowmodel',
        //    //mode: 'SINGLE',
        //    checkOnly: 'true',
        //    allowDeselect: true
        //},
        enableColumnHide: false,
        columns:
            [
                {
                    text: "#",
                    dataIndex: 'NumRow',
                    width: 50,
                    cls: 'locked',
                    autoSizeColumn: false,
                    locked: true,
                },
                {
                    text: "Error",
                    dataIndex: 'Sys_Error',
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-sys_error"],
                    dataIndex: 'Sys_Error',
                    align: 'center',
                    minWidth: 50,
                    renderer: function (value, metaData) {
                        value = "" + value.split(/\n/i).join('<br/>');
                        return '<div class="multirowcol inerror">' + value + '</div>';
                    },
                    autoSizeColumn: false,
                   
                    lockable: false,
                    sortable: false,
                    filter: true,
                    locked: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-code"],
                    dataIndex: 'eName',
                    width: 120,
                    cls: 'locked',
                    autoSizeColumn: false,
                    locked: true,
                    renderer: function (value, meta, record) {
                        var buttonText = "<i class='fas fa-donate'></i> " + value;
                        var extraparam = "";//(record.data.CBS.toLowerCase() == "materials" ? "material" : "");
                        return '<a title="' + CDI.service.Translate.data["dashboard-rda-cardbutton-open"] + '" role="button" class="rdalinkbutton ' + extraparam + ' x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box" hidefocus="on" unselectable="on"><span role="presentation" class="x-btn-wrap" unselectable="on"><span class="x-btn-button" role="presentation"><span class="x-btn-inner x-btn-inner-center" unselectable="on">' + buttonText + '</span><span role="presentation" class="x-btn-icon-el" unselectable="on" style=""></span></span></span></a>';
                    }
                },
                {
                    text: CDI.service.Translate.data["rda-multimaterial-drawingcode"],
                    dataIndex: 'DRAWINGCODE',
                    minWidth: 110,
                    cls: 'extend',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["rda-multimaterial-materialdescription"],
                    dataIndex: 'MATERIALDESCRIPTION',
                    width: 400,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-co_sp"],
                    dataIndex: 'CO_SP',
                    minWidth: 110,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["rda-multimaterial-quantityonvehicle"],
                    dataIndex: 'QUANTITYONVEHICLE',
                    width: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                    align: 'center',
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-pieceprice"],
                    dataIndex: 'PIECEPRICE',
                    filter: 'number',
                    align: 'right',
                    renderer: function (value, metaData, record, rowIndex) {
                        try {
                            var currency = record.data.PIECEPRICEFORMAT;
                            if (currency == "dollar") 
                                currency = "$";
                            else
                                currency = "€";
                            return CDI.service.erdaAction.formatMoney(value, currency, 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 120,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                //{
                //    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-piecepriceformat"],
                //    dataIndex: 'PIECEPRICEFORMAT',
                //    minWidth: 80,
                //    autoSizeColumn: true,
                //    lockable: false,
                //    sortable: false,
                //    filter: true,
                //},
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-totalquantity"],
                    dataIndex: 'TOTALQUANTITY',
                    filter: 'number',
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                },
                {
                    text: CDI.service.Translate.data["rda-multimaterial-requestedsupplier"],
                    dataIndex: 'REQUESTEDSUPPLIER',
                    minWidth: 110,
                    align:'center',
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-supplier"],
                    dataIndex: 'SUPPLIER',
                    minWidth: 110,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-codiceiman"],
                    dataIndex: 'CODICEIMAN',
                    minWidth: 110,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["rda-multimaterial-supplierrefperson"],
                    dataIndex: 'SUPPLIERREFERENCE',
                    minWidth: 110,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-baangrid-column-baanin_datadiconsegna"],
                    dataIndex: 'REQUESTEDDELIVERYDATE',
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    minWidth: 110,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            //Fix d/m/Y format
                            var dateArray = v.toString().split('/');
                            if (dateArray.length == 3) {
                                v = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                            }
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },
                },
                {
                    text: '',
                    dataIndex: 'fake',
                    flex:1,
                    autoSizeColumn: false,
                    lockable: false,
                    sortable: false,
                    filter: false,
                },
            ],


        bbar: [
            {
                xtype: 'filefield',
                itemId: 'filexlsx',
                fieldLabel: '<b>' + CDI.service.Translate.data["common-upload-material-label"] + '</b>',
                emptyText: CDI.service.Translate.data["dashboard-rda-materials-field-label"],
                cls: 'materialUpload',
                width: '400px',
                buttonText: '',
                buttonConfig: {
                    iconCls: 'far fa-file-excel'
                },
                accept: '.xlsx',
                listeners: {

                    change: function (fld, value) {
                        var btn = this.up().down("#btnImport");
                        this.up().down("#btnClear").disable();
                        if (value != "") {
                            
                            Ext.callback(btn.handler, btn.scope, [btn, null], 0, btn);
                            this.up().down("#btnClear").enable();
                        }
                        var newValue = value.replace(/C:\\fakepath\\/g, '');
                        fld.setRawValue(newValue);
                        var records = [];
                        var projectSettingsGrid = this.up("grid");
                        projectSettingsGrid.setStore(records);
                    }
                },
                regex: /(.)+((\.xlsx)(\w)?)$/i,
                regexText: 'Only XSLX format is accepted'

            },
            '-',
            {
                hidden:true,
                xtype: 'button',
                itemId: 'btnImport',
                disabled:true,
                iconCls: 'fas fa-cloud-upload-alt',
                cls: 'pagingToolbarButton',
                text: CDI.service.Translate.data["common-upload-material"],
                tooltip: CDI.service.Translate.data["common-upload-material"],
                handler: 'onUploadFile'
            },
            {

                xtype: 'button',
                itemId: 'btnClear',
                iconCls: 'fas fa-ban',
                cls: 'pagingToolbarButton',
                text: CDI.service.Translate.data["common-upload-material-clear"],
                tooltip: CDI.service.Translate.data["common-upload-material-clear"],
                handler: 'onRebuildPage'
            },

            '-',
            {
                xtype: 'button',
                itemId: 'btnApprove',
                disabled:true,
                iconCls: 'fas fa-save',
                cls: 'pagingToolbarButton',
                text: CDI.service.Translate.data["common-saveasrda-material"],
                tooltip: CDI.service.Translate.data["common-saveasrda-material-tooltip"],
                handler:'onSaveMaterials'
               
            },

        ]

        ,dockedItems: [{
            //hidden:true,
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["dashboard-rda-worklist-grid-pagingtoolbar-emptyMsg"],

            items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton btnrefresh',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("importmaterial").getController().onBeforeRender(this.up("importmaterial"));
                        }
                    }
                },
                
            ]
        }]

    }
});

