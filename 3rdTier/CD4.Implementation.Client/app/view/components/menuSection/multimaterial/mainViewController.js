﻿Ext.define('CDI.view.components.menuSection.multimaterial.mainViewController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.importmaterialcontroller',

        onBeforeRender: function (view) {
            var grid = view;
            var btnApprove = view.down("#btnApprove");
            var btnClear = view.down("#btnClear");
            var fileInput = view.down("#filexlsx");
            var bbar = view.getDockedItems();
            if (grid.getXType() != "materialgrid")
                grid = view.down("grid");

            if (bbar.length == 2)
                grid.custombar = bbar;
            else
                bbar = grid.custombar;

            view.setLoading(true);
            Ext.Ajax.request({
                method: 'GET',
                withCredentials: true,
                url: CD.apiUrl + 'erda/materials/list/' + view.special.businessDataId,
                success: function (response, operation) {
                    
                    var fileData = (Ext.decode(response.responseText));
                    var store = Ext.create('Ext.data.Store', {
                        data: []
                    });
                    if (fileData.level.toLowerCase() == "completed") {
                        
                        bbar[0].show();
                        if (bbar[0].down('#refresh'))bbar[0].down('#refresh').hide();
                        if (bbar.length>1)
                            bbar[1].hide();
                        var materialStore = { fields: [], data: [] };
                        materialStore.data = fileData.rows;
                        grid.setStore(Ext.create('Ext.data.Store', materialStore));
                    }
                    else {
                        btnClear.disable();
                        bbar[1].show();
                        bbar[0].hide();
                    }
                    var columns = grid.getColumns();
                    for (var i = 0; i < columns.length; i++) {
                        var column = columns[i];
                        if ("numrow;sys_error".indexOf(column.dataIndex.toLowerCase()) != -1)
                            column.setVisible(fileData.level.toLowerCase() == "ready");
                        if ("ename".indexOf(column.dataIndex.toLowerCase()) != -1)
                            column.setVisible(fileData.level.toLowerCase() == "completed");
                    }
                    view.setLoading(false);
                },
                failure: function (response, operation) {
                    view.setLoading(false);
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-materialsimport-title'], "", response);
                }
            });

        },


        onAfterRender: function (view) {
            view.up('businessdatawrapper').isLoading = false;
        },


        onOpenBusinessData: function (gridview, tdEl, cellIndex, record, trEl, rowIndex, e) {
            if (tdEl.innerHTML.indexOf("rdalinkbutton") != -1) {
                CDI.service.erdaAction.open(record.get('eId'));
            }

        },

        onSaveMaterials: function (btn) {
            var grid = btn.up("grid");
            var view = btn.up("importmaterial");
            var selection = grid.getStore().data.count();
            //debugger;
            if (selection > 0) {
                var fileToStore = {
                    businessDataId: view.up("businessdatawrapper").special.businessDataId,
                    rows: []
                }
                for (var i = 0; i < selection; i++) {
                    fileToStore.rows.push(grid.getStore().data.items[i].data);
                }
                CDI.service.erdaAction.showSaveMaterialsAction(view.id, fileToStore);
            }

        },

        doRefresh: function (data) {
            var button = this.getView().down("#btnRefresh");
            if (button) button.fireEvent("click", button);
        },
        onUploadFile: function (btn) {
            var view = btn.up("importmaterial");
            var grid = view.down("materialgrid");
            var fileInput = view.down("#filexlsx");
            var btnApprove = view.down("#btnApprove");
            btnApprove.disable();
            var file = document.getElementById(fileInput.button.fileInputEl.id);
            if (file != null && file.files != null && file.files.length > 0) {
                view.setLoading(true);
                
                var fileToUpload = file.files[0];
                var reader = new FileReader();
                var records = [];
                reader.onload = function (e) {
                    var fileData = {
                        businessDataId: view.up("businessdatawrapper").special.businessDataId,
                        filename: fileToUpload.name,
                        filecontent: e.target.result,
                    };
                    Ext.Ajax.request({
                        method: 'POST',
                        withCredentials: true,
                        url: CD.apiUrl + 'erda/materials/getmaterialfilecontent',
                        jsonData: fileData,
                        success: function (response, operation) {
                            var fileData = (Ext.decode(response.responseText));
                            if (fileData.IsValidFile)
                                btnApprove.enable();
                            var store = Ext.create('Ext.data.Store', {
                                data: []
                            });
                            if (fileData != null && fileData.rows.length > 0) {
                                grid.getStore().loadData(fileData.rows);
                            }
                            fileInput.reset();
                            //grid.setStore(store);
                            view.setLoading(false);
                        },
                        failure: function (response, operation) {
                            view.setLoading(false);
                            CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-materialsimport-title'], "", response);
                        }
                    });


                };
                reader.readAsDataURL(fileToUpload);
                //reader.readAsArrayBuffer(file);
            }


        },

        onRebuildPage: function (btn) {
            var view = btn.up("importmaterial");
            var viewcontainer = view.down("#objectPanelView");
            var grid = viewcontainer.down("materialgrid");
            var fileInput = viewcontainer.down("#filexlsx");
            var btnApprove = view.down("#btnApprove");
            view.setLoading(true);
            grid.getStore().removeAll();
            //grid.destroy();
            //var newGrid= (Ext.create("CDI.view.components.menuSection.multimaterial.objectListView"));
            //viewcontainer.add(newGrid);
            //view.updateLayout();
            btnApprove.disable();
            fileInput.reset();
            fileInput.fireEvent("change", fileInput, "");
            view.setLoading(false);
        }

    });
