﻿Ext.define('CDI.view.components.menuSection.info.businessdata.BusinessDataToolbar', {});
Ext.deferDefine('CDI.view.components.menuSection.info.businessdata.BusinessDataToolbar', function () {
    return {
        extend: 'Ext.toolbar.Toolbar',
        xtype: 'businessdatatoolbar',
        //controller: 'businessdatatoolbarcontroller',
        itemId: 'businessdatatoolbar',
        cls: 'basicgrid rdatoolbar',
        listeners: {
            afterrender: function (view) {
                var budget = view.up("businessdataform").down("#budgetLabel");
                budget.hide();
                Ext.Ajax.request({
                    url: CD.apiUrl + 'erda/checkbudget/' + view.special.businessDataId,
                    method: 'GET',
                    withCredentials: true,
                    async: false,
                    success: function (jsonData) {
                        var value = Ext.decode(jsonData.responseText);
                        if (!value)
                            budget.show();
                    }
                });
                CDI.service.erdaAction.panelbuttonview(view);
            }
        },
        defaults: {
            //width:'130px'
        },
        items: [
            {
                iconCls: 'fas fa-pencil-alt',
                tooltip: CD.service.Translate.data['common-edit'],
                itemId:'btnEdit',
                handler: function (btn) {
                    var businessDataView = btn.up('businessdataview');
                    businessDataView.getController().onEditClick(btn);
                    businessDataView.down("#btnChangeCost").disable();
                }
            },
            {
                iconCls: 'fas fa-ban',
                tooltip: CD.service.Translate.data['common-cancel'],
                itemId: 'btnCancel',
                hidden: true,
                handler: function (btn) {
                    
                    var businessDataView = btn.up('businessdataview');
                    businessDataView.getController().onCancelClick(btn);
                    CDI.service.erdaAction.panelbuttonview(businessDataView);
                    businessDataView.down("#btnChangeCost").enable();
                }
            },
            {
                iconCls: 'fas fa-trash-alt',
                tooltip: CD.service.Translate.data['common-delete'],
                itemId: 'btnDelete',
                handler: function (btn) {
                    var view = btn.up('businessdataview');
                    var container = view.up("businessdatawrapper");
                    var toolBar = btn.up("businessdatatoolbar");

                    Ext.MessageBox.confirm(CD.service.Translate.data['common-delete'], CD.service.Translate.data['businessdata-workbench-delete-alert'], function (id) {
                        if (id === 'yes') {
                            container.setLoading(true);
                            Ext.Ajax.request({
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                method: 'DELETE',
                                withCredentials: true,
                                url: CD.apiUrl + 'erda/' + view.special.businessDataId,
                                failure: function (response) {
                                    var title = CD.service.Translate.data['common-error'];
                                    CDI.service.erdaAction.showErrorMessage(title, "", response);
                                    container.setLoading(false);
                                }
                            }).then(function (response) {
                                
                                var data = Ext.decode(response.responseText);
                                var wrapperSpecial = {
                                    businessDataId: data.Id,
                                    businessDataType: data.BusinessDataType,
                                    createUser: data.CreateUser,
                                    description: data.Description,
                                    itemName: data.Name,
                                    level: data.Level,
                                    owner: data.Owner,
                                    project: data.Project,
                                    revision: data.Revision,
                                    workflow: data.Workflow
                                };

                                CD.service.WebSockets.businessDataDeleted(Ext.getCmp('username').getText(), view.special.businessDataId, view.special.itemName);
                                //container.setLoading(false);
                                Ext.toast({
                                    html: '<i class="fa fa-check"></i> ' + CD.service.Translate.data['businessdata-workbench-successfully-toast'],
                                    bodyStyle: 'background-color: green; color: white',
                                    align: 'tr',
                                    anchor: 'contentPanel'
                                });

                                var businessDataWrapper = view.up('businessdatawrapper');
                                var contentPanel = businessDataWrapper.up('#contentPanel');
                                var businessDataWrapperIndex = contentPanel.items.indexOf(businessDataWrapper);

                                Ext.suspendLayouts();
                                businessDataWrapper.close();
                                CDI.service.erdaAction.open2(wrapperSpecial, businessDataWrapperIndex);

                                Ext.resumeLayouts(true);
                            });
                        }
                    }, this);
                }
                // width: '100px'
            },

            {
                iconCls: 'fas fa-save',
                tooltip: CD.service.Translate.data['common-save'],
                itemId: 'btnSaveChanges',
                disabled: true,
                handler: function (btn) {
                    var businessDataView = btn.up('businessdataview');
                    businessDataView.getController().onSaveClick(btn);
                    CDI.service.erdaAction.panelbuttonview(businessDataView);
                    businessDataView.down("#btnChangeCost").enable();
                }
                //width: '80px'
            },
            '-',
            {
                iconCls: 'far fa-copy',
                itemId:'btnSaveAs',
                tooltip: CDI.service.Translate.data['common-clone'],
                 handler: function (btn) {
                     var view = btn.up('businessdataview');
                     var container = view.up("businessdatawrapper");
                     var toolBar = btn.up("businessdatatoolbar");

                     Ext.MessageBox.confirm(CDI.service.Translate.data['common-clone'], CDI.service.Translate.data['common-question-clonerda-message'], function (id) {
                         if (id === 'yes') {
                             
                             container.setLoading(true);
                             Ext.Ajax.request({
                                 headers: {
                                     'Content-Type': 'application/json'
                                 },
                                 method: 'POST',
                                 withCredentials: true,
                                 url: CD.apiUrl + 'erda/clone/' + view.special.businessDataId,
                                 failure: function (response) {
                                     
                                     var title = CD.service.Translate.data['common-error'];
                                     CDI.service.erdaAction.showErrorMessage(title, "", response);
                                     container.setLoading(false);
                                 }
                             }).then(function (response) {
                                 
                                 var data = Ext.decode(response.responseText);

                                 Ext.toast({
                                     html: '<i class="fa fa-check"></i> ' + CDI.service.Translate.data['completed-successfully'],
                                     bodyStyle: 'background-color: green; color: white',
                                     align: 'tr',
                                     anchor: 'contentPanel'
                                 });
                                 container.setLoading(false);
                                 CDI.service.erdaAction.open(data.Id);
                             });
                         }
                     }, this);
                }
                
            },
            {
                iconCls: 'fas fa-coins',
                tooltip: CDI.service.Translate.data['rda-admincenter-changecostbtn-tooltip'],
                itemId: 'btnChangeCost',
                handler: function (btn) {
                    var businessDataWrapper = btn.up('businessdatawrapper');
                    if (businessDataWrapper.special.itemName.indexOf("_")!=-1) {
                        CDI.service.erdaAction.showToastError(CDI.service.Translate.data['rda-changecost-nofrotranche']);
                        return;
                    }
                    

                    Ext.Ajax.request({
                        url: CD.apiUrl + 'erda/costs/' + businessDataWrapper.special.businessDataId,
                        method: 'GET',
                        withCredentials: true,
                        async: false,
                        failure: function (response) {
                            CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                        },
                        success: function (jsonData) {
                            var data = Ext.decode(jsonData.responseText);
                            if (data.block) {
                                CDI.service.erdaAction.showToastError(data.blockmessage);
                                return;
                            }
                            var win = new Ext.Window({
                                header: false,
                                border: false,
                                closable: false,
                                draggable: false,
                                reloadOnClose: false,
                                originEvent: btn,
                                itemId: 'WfgWin',
                                title: false,
                                width: '700px',
                                height: '420px',
                                closeAction: 'hide',
                                buttonAlign: 'center',
                                closable: false,
                                modal: true,
                                animShow: function () {
                                    this.el.slideIn('t', {
                                        duration: 1, callback: function () {
                                            this.afterShow(true);
                                        }, scope: this
                                    });
                                },
                                animHide: function () {
                                    this.el.disableShadow();
                                    this.el.slideOut('t', {
                                        duration: 1, callback: function () {
                                            this.el.hide();
                                            this.afterHide();
                                        }, scope: this
                                    });
                                },
                                layout: {
                                    align: 'stretch',
                                    type: 'vbox'
                                },
                                listeners: {
                                    close: function (win) {
                                        if (win.reloadOnClose) {
                                            var toolbar = win.originEvent.up();
                                            var btn = toolbar.down("#btnRevise");
                                            var e = null; 
                                            Ext.callback(btn.handler, btn.scope, [btn, e], 0, btn);
                                        }
                                    },
                                    scope: this
                                },
                                items: [
                                    {
                                        xtype: 'changecostview',
                                        config: {
                                            data: data
                                        },

                                        flex: 1,
                                    }
                                ],


                            });
                            win.show(Ext.getBody());

                        }
                    });

                    


                }
            },
            {
                iconCls: 'fas fa-external-link-alt',
                tooltip: 'Go to Master',
                hidden: true

            },
            '-',
            {
                iconCls: 'fas fa-sync-alt',
                itemId: 'btnRevise',
                tooltip: CDI.service.Translate.data['rda-admincenter-reloadbtn-tooltip'],
                handler: function (btn) {
                    
                    //var businessDataView = btn.up('businessdataview');
                    //businessDataView.fireEvent('refresh', businessDataView);

                    var businessDataWrapper = btn.up('businessdatawrapper');
                    var contentPanel = businessDataWrapper.up('#contentPanel');
                    var businessDataWrapperIndex = contentPanel.items.indexOf(businessDataWrapper);
                    var special = Ext.clone(businessDataWrapper.special);
                    Ext.suspendLayouts();
                    businessDataWrapper.close();
                    CDI.service.erdaAction.open2(special, businessDataWrapperIndex);

                    Ext.resumeLayouts(true);

                }
            }
            
        ]
    }
});