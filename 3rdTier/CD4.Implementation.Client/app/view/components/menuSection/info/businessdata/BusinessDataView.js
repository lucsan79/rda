﻿Ext.define('CDI.view.components.menuSection.info.businessdata.BusinessDataView', {});

Ext.deferDefine('CDI.view.components.menuSection.info.businessdata.BusinessDataView', function () {

    return {
        extend: 'CD.view.components.menuSection.info.businessdata.BusinessDataView',

        xtype: 'businessdataview',
        itemId: 'businessdataview',
        //controller: 'businessdataview',

        layout: 'fit',
        viewMode: '',
        setViewMode: function (viewMode) {
            this.viewMode = viewMode;
        },
        cdTitle: CD.service.Translate.data['common-info'],
        cdIcon: 'info-circle',

        disabledFields: [],

        enabledLevelList: [],
        enabledRoleList: [],
        enabledBusinessDataTypesList: [],

        listeners: {
            beforerender: 'onBeforeRender',
            //afterrender: 'onAfterRender',
            editstart: 'onEditStart',
            editfinish: 'onEditFinish',
            createreserveuseranddate: 'createReserveUserAndDate',
            removereserveuseranddate: 'removeReserveUserAndDate',
            refresh: 'refresh'
        },

        cls: 'business-data-view',

        items: [
            {
                xtype: 'businessdataform',
                viewMode: true,
                cls: 'no-input-border',
                overflowY: 'auto',
                listeners: {
                    afterrender: 'onAfterRender'
                },
                header: {
                    xtype: 'panel',
                    layout: 'hbox',
                    height: 50,
                    items: [
                        {
                            xtype: 'businessdatatoolbar',
                            overflowHandler: 'scroller'
                        },
                        {
                            xtype: 'displayfield',
                            itemId:'budgetLabel',
                            value: CDI.service.Translate.data['tooltip-no-budget'],
                            cls:'budgetError'
                        },
                        {
                            itemId: 'headerHamburgerMenu',
                            xtype: 'button',
                            hidden:true,
                            arrowVisible: false,
                            style: {
                                color: 'white',
                                'margin-top': '5px',
                                'margin-left': '5px'
                            },
                            iconCls: 'x-fa fa-bars',
                            menu: [
                                {
                                    text: CD.service.Translate.data['common-copy'],
                                    iconCls: 'x-fa fa-clone',
                                    itemId: 'btnCopy',
                                    uiItemId: 'copy',
                                    handler: 'onCopyClick'
                                }, {
                                    itemId: 'editSeparator',
                                    xtype: 'menuseparator',
                                    uiItemId: '-',
                                }, {
                                    text: CD.service.Translate.data['common-edit'],
                                    iconCls: 'x-fa fa-pencil-alt',
                                    itemId: 'btnEdit',
                                    uiItemId: 'edit',
                                    handler: 'onEditClick'
                                }, {
                                    itemId: 'btnCancel',
                                    uiItemId: 'cancel',
                                    text: CD.service.Translate.data['common-cancel'],
                                    iconCls: 'x-fa fa-ban',
                                    handler: 'onCancelClick',
                                    hidden: true
                                }, {
                                    itemId: 'btnSaveChanges',
                                    uiItemId: 'save',
                                    text: CD.service.Translate.data['common-save'],
                                    iconCls: 'x-fa fa-floppy-o',
                                    handler: 'onSaveClick',
                                    disabled: true
                                }, {
                                    itemId: 'reserveSeparator',
                                    xtype: 'menuseparator',
                                    uiItemId: '-'
                                },
                                {
                                    itemId: 'btnReserve',
                                    uiItemId: 'reserve',
                                    text: CD.service.Translate.data['common-reserve'],
                                    iconCls: 'x-fa fa-lock',
                                    handler: 'onReserveClick'
                                }, {
                                    itemId: 'btnUnReserve',
                                    uiItemId: 'unreserve',
                                    text: CD.service.Translate.data['common-unreserve'],
                                    iconCls: 'x-fa fa-unlock',
                                    disabled: true,
                                    handler: 'onUnReserveClick'
                                }, {
                                    itemId: 'reviseSeparator',
                                    uiItemId: '-',
                                    xtype: 'menuseparator'
                                },
                                {
                                    itemId: 'btnSaveAs',
                                    uiItemId: 'saveas',
                                    text: CD.service.Translate.data['common-saveas'],
                                    iconCls: 'x-fa fa-floppy-o',
                                    handler: 'onSaveAsClick'
                                }, {
                                    itemId: 'btnRevise',
                                    uiItemId: 'revise',
                                    text: CD.service.Translate.data['common-revise'],
                                    iconCls: 'x-fa fa-pencil-square-o',
                                    handler: 'onReviseClick'
                                }, {
                                    itemId: 'btnBaseline',
                                    uiItemId: 'baseline',
                                    text: CD.service.Translate.data['common-baseline'],
                                    iconCls: 'x-fa fa-hdd-o',
                                    handler: 'onBaselineClick'
                                },
                                {
                                    itemId: 'showAllSeparator',
                                    uiItemId: '-',
                                    xtype: 'menuseparator',
                                    hidden: true,
                                }, {
                                    itemId: 'btnShowAll',
                                    uiItemId: 'showall',
                                    text: CD.service.Translate.data['common-showallfields'],
                                    iconCls: 'x-fa fa-arrows',
                                    handler: 'onShowAllClick',
                                    disabled: true, //CURENT REQUEST BY LUCA!!!
                                    hidden: true,
                                },
                                {
                                    itemId: 'workbenchSeparator',
                                    uiItemId: '-',
                                    xtype: 'menuseparator'
                                },
                                {
                                    itemId: 'btnAddWorkbench',
                                    uiItemId: 'addworkbench',
                                    text: CD.service.Translate.data['common-addworkbench'],
                                    iconCls: 'x-fa fa-desktop',
                                    handler: 'onAddWorkbenchClick'
                                },
                                {
                                    itemId: 'deleteSeparator',
                                    uiItemId: '-',
                                    xtype: 'menuseparator'
                                },
                                {
                                    itemId: 'btnDelete',
                                    uiItemId: 'delete',
                                    text: CD.service.Translate.data['common-delete'],
                                    iconCls: 'x-fa fa-trash',
                                    handler: 'onDeleteClick'
                                }
                            ]
                        },
                        {
                            hidden: true,
                            xtype: 'button',
                            iconCls: 'x-fa fa-refresh',
                            itemId: 'btnReload',
                            style: {
                                color: 'white',
                                'margin-top': '5px',
                                'margin-left': '5px',
                                'margin-bottom': '5px',
                            },
                            text: '',
                            handler: 'onReloadClick'
                        },
                        {
                            itemId: 'revisionCombobox',
                            xtype: 'combobox',
                            hidden: true,
                            emptyText: CD.service.Translate.data['businessdata-workbench-changerevision'],
                            valueField: 'Id',
                            displayField: 'Revision',
                            editable: false,
                            style: {
                                'margin-top': '5px',
                                'margin-left': '5px'
                            },
                            listeners: {
                                change: 'onRevisionComboboxChange'
                            }
                        },
                        {
                            xtype: 'panel',
                            flex: 1,
                            //header: false,
                            itemId: 'businessdataformTitle',
                            title: {
                                textAlign: 'right',
                                style: {
                                    color: 'white'
                                }
                            }
                        }
                    ]
                }
            }
        ],

    }

});
