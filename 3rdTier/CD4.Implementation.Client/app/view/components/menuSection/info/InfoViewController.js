﻿Ext.define('CDI.view.components.menuSection.info.InfoViewController', {
    extend: 'CD.view.components.menuSection.info.InfoViewController',
    alias: 'controller.workbenchcontainer',

    
    onTabPanelAdded: function (tabpanel) {
        var spendingcurve = {
            xtype: 'spendingcurve',
        };
        //var leadtime = {
        //    xtype: 'leadtimeview',
        //};
        
        tabpanel.insert(0, spendingcurve);
        //tabpanel.insert(0, milestone);
        tabpanel.setActiveTab(0);
    },
});
