﻿Ext.define('CDI.view.components.dialogSection.dialogConfigurationManager', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dialogconfigurationmanager',


    config: {
        enableAutomatic: false,
        inEdit: false,
        emptyContent: '|<br/>',
        view: null,
        shad: null
    },
    scriptsArray: [],
    goNextStep: function (dialogItem) {
        var self = this;
        var statusBar = self.view.up("#DialogWin").down("statusbar");
        statusBar.showBusy();
        var panel = self.view.up("#DialogWin").down("createrdaview");
        if (self.config.shad != null) self.config.shad.setOpacity(.5);

        setTimeout(function () {
            var url = "erda/dialog/configurationmanager";
            //sself.lookupReference('basic-statusbar').showBusy();
            var content = self.performRequest(url, 'POST', dialogItem);
            if (content)
                self.onContentRender(content);
        }, 500);

    },

    preformatOnClickLink: function (content, viewId) {
        var index = 0;
        while (content.indexOf("SetCAForEditing") > 0) {
            content = content.replace("SetCAForEditing", "Ext.getCmp(\'" + viewId + "\').controller.enableEditing");
        }

        while (content.indexOf("changecurrency") > 0) {
            content = content.replace("changecurrency", "Ext.getCmp(\"" + viewId + "\").controller.updateCurrency");
        }
        while (content.indexOf("setCurrency") > 0) {
            content = content.replace("setCurrency", "Ext.getCmp(\"" + viewId + "\").controller.applyCurrency");
        }

        while (content.indexOf("liSelection") > 0) {
            content = content.replace("liSelection", "Ext.getCmp(\"" + viewId + "\").controller.onAnswerSelection");
        }
        while (content.indexOf("CASendValidationAndUpdate") > 0) {
            content = content.replace("CASendValidationAndUpdate", "Ext.getCmp(\'" + viewId + "\').controller.onInputSelection");
        }

        while (content.indexOf("popUpDate") > 0) {
            content = content.replace("popUpDate", "Ext.getCmp(\'" + viewId + "\').controller.showDatePicker");
        }

        return content;

    },
    onContentRender: function (content) {
        if (content === undefined || content === "") return;

        var self = this;
        var old = $('#showquestion').html();
        content = self.preformatOnClickLink(content, self.config.view.id);

        var old2 = content.split('|')[0] + '<br/>';
        var newQ = "" + content.split('|')[1];


        $('#showquestion').html(old2);
        $('#newquestion').html(newQ);
        this.initExtraControl(self);

        //CONTROLLO PER ABILITARE IL SALVATAGGIO
        var savebutton = Ext.ComponentQuery.query('toolbar button', this.config.view.up())[0];
        savebutton.disabled = true;
        var old = $('#showquestion').html();
        
        self.view.up("#DialogWin").dockedItems.items[1].items.items[3].disable();
        if (old != "") {
            if (old.indexOf("CONFIGURATION ENDED") != -1) {

                old = old.replace("[CONFIGURATION ENDED]", "");
                $('#showquestion').html(old);
                self.view.up("#DialogWin").dockedItems.items[1].items.items[3].enable();
            }
        }
        $("#showquestion input[type='radio']").prop('disabled', true);
        var Height = (self.GetHeight(document) - 50) + "px";
        $('#newquestion').css('max-height', Height);
        if (self.config.inEdit == false) {
            self.ScrollDown();
        }
        setTimeout(function () {
            try {
                var statusBar = self.view.up("#DialogWin").down("statusbar");
                statusBar.clearStatus();
            } catch (err) { };

            if (self.config.shad != null) self.config.shad.setOpacity(1);

        }, 200);




    },
    GetHeight: function (givendocument) {
        var body = givendocument.body,
            html = givendocument.documentElement;

        var height = Math.max(body.scrollHeight, body.offsetHeight,
            html.clientHeight, html.scrollHeight, html.offsetHeight);
        return height;
    },

    ScrollDown: function () {
        $(document.getElementById('showquestion')).animate({
            scrollTop: $(document.getElementById('showquestion'))[0].scrollHeight - $(document.getElementById('showquestion'))[0].clientHeight
        }, 500);
    },

    setLeftHeight: function () {
        var windowHeight;
        try {
            if (typeof window.innerWidth != 'undefined') {
                windowHeight = window.innerHeight;
            }
            else if (typeof document.documentElement != 'undefined'
                && typeof document.documentElement.clientWidth != 'undefined'
                && document.documentElement.clientWidth != 0) {
                windowHeight = document.documentElement.clientHeight;
            }
            else {
                windowHeight = document.getElementsByTagName('body')[0].clientHeight;
            }

            document.getElementById("container").style.height = (windowHeight - 50) + "px";// (windowHeight - 100) + "px";
        } catch (err) { };
    },

    injectScripts: function (view) {
        var self = this;
        var fileVersion = Math.random();
        scriptsArray = null;
        scriptsArray = [];

        scriptsArray.push("implementation-resources/configurator/js/jquery.min.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/jquery-ui12.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_Attachment.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_CheckActivityOnBudget.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_Location.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_ProfessionalFigureRole.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_RemedySummary.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_RemedySupplier.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_SpendingCurve.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_WBSSettings.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_CommonValidaton.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_datepicker.js?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/js/eRdAaddin_autoNumeric.js?filever=" + fileVersion);

        scriptsArray.push("implementation-resources/configurator/css/jquery-ui.css?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/dialog.css?filever=" + fileVersion);
        scriptsArray.push("implementation-resources/configurator/css/datepicker.css?filever=" + fileVersion);

        for (var index = 0; index < scriptsArray.length; index++) {
            self.loadjscssfile(scriptsArray[index]);
        }

    },

    unloadScripts: function (view) {
        try {


            var self = this;
            for (var index = 0; index < scriptsArray.length; index++) {
                self.loadjscssfile(scriptsArray[index], false);
            }
            var prePanel = Ext.getCmp("myPanelTranche");
            if (prePanel != null) prePanel.destroy();
            var fileAttachment = Ext.getCmp("fileAttachment");
            if (fileAttachment != null) fileAttachment.destroy();

            fileAttachment = Ext.getCmp("fileSSAttachment");
            if (fileAttachment != null) fileAttachment.destroy();

        } catch (err) { };
    },
    startDialogConfigurationWithDelay: function (view) {
        
        setTimeout(function () { view.getController().startDialogConfiguration(view) }, 500);
    },
    startDialogConfiguration: function (view) {
        var self = this;
        var url = "erda/dialog/startnewconfiguration";
        var panel = self.view.up("#DialogWin").down("createrdaview");
        if (!self.config.shad) self.config.shad = new Ext.Shadow(panel);
        view.setLoading(true);
        self.config.view = view;
        self.setLeftHeight();

        $('#showquestion').html("");
        $('#questions').hide();
        $('#answers').hide();

        var url = "erda/dialog/startnewconfiguration";
        //debugger;
        Ext.Ajax.request({
            url: CD.apiUrl + url,
            method: 'GET',
            withCredentials: true,
            callback: function (opt, success, response) {
                if (success) {
                    view.setLoading(false);
                    self.onContentRender(self.config.emptyContent);
                    self.goNextStep(self.getEmptyDialogItem());
                    //self.onProgressBar();
                }
                else {
                    //debugger;
                    view.setLoading(false);
                    //CDI.service.erdaAction.showErrorMessage("Error eRdA Configuration", "", response);

                    self.showError(self.getMessageError(response));
                    self.unloadScripts(view);
                    return null;
                }
            }
        });

    },

    initExtraControl: function () {

        RdAWBSSettings.InitElements(this);
        RemedyIdentify.InitElements(this);
        SupplierIdentify.InitElements(this);
        Locationdentify.InitElements(this);
        SpendingCurve.InitElements();
        ProfessionalObjectIdentify.InitElements(this);
        RdARemedySummary.InitElements(this);
        RDAAttachment.InitElements(this);
        RDAAttachment.InitSSElements(this);
        CheckActivityOnBudget.InitElements(this);

    },


    //function on item in card
    onAnswerSelection: function (objli) {
        var self = this;
        //setTimeout(function () {
        //    self.onProgressBar('start');
        //}, 50);
        //setTimeout(function () {
        try {
            if ($(objli).attr('disabled')) return false;

            var target = objli.attributes['id'].value;
            if (objli.attributes['class'] != null) {
                var sel = objli.attributes['class'].value;
                if (sel != null)
                    if (sel == 'selected')
                        return false;
            }

            var parid = objli.attributes['parent'].value;
            var selectionId = objli.attributes['selid'].value;
            var qs = $('#showquestion').html();
            var index1 = qs.indexOf('parent="' + parid + '"');
            var index2 = qs.indexOf('selid="' + selectionId + '"');
            if (index1 != -1 && index2 != -1) self.config.inEdit = true;
            self.goNextStep(self.getEmptyDialogItem(1, parid, target, selectionId));
            if (index1 != -1 && index2 != -1)
                self.config.inEdit = false;
            //return true;
        }
        finally {

        }

        // }, 200);

    },


    onInputSelection: function (givenParentID, objectsID, objectsIDMandatory, idsNumber, idsString, btn) {
        var self = this;
        var selId = "";
        document.getElementById("lbl_" + givenParentID).innerHTML = "";

        if (objectsID != "") {
            var idList = objectsID.split(";");
            var index;
            var mandatory = false;
            var valToSave = "";
            var errorMessageEmpty = "";
            ////////////validazione mandatory effettuta////////////
            for (index = 0; index < idList.length; index++) {
                if (idList[index] != "") {
                    var txt = document.getElementById("txtCA_" + idList[index]);
                    txt.style.bkColor = txt.style.backgroundColor;
                    txt.style.backgroundColor = "white";
                    if (RemedyIdentify.SetControlWhite("txtCA_" + idList[index]) == false)
                        SupplierIdentify.SetControlWhite("txtCA_" + idList[index]);
                    Locationdentify.SetControlWhite("txtCA_" + idList[index]);
                    ProfessionalObjectIdentify.SetControlWhite("txtCA_" + idList[index]);
                    RdAWBSSettings.SetControlWhite("txtCA_" + idList[index]);

                    if (txt != null) {
                        selId = txt.attributes['selid'].value

                        if (txt.type == "checkbox") {
                            if (txt.checked)
                                txt.value = "true";
                            else
                                txt.value = "false";
                        }
                        valToSave = valToSave + "CA_" + idList[index] + "[#]" + encodeURIComponent(txt.value) + "[@]";
                        if (txt.value == "" && objectsIDMandatory.indexOf(idList[index]) != -1) {
                            txt.style.backgroundColor = "red";
                            mandatory = true;
                            if (RemedyIdentify.SetControlRed("txtCA_" + idList[index]) == false)
                                SupplierIdentify.SetControlRed("txtCA_" + idList[index]);
                            Locationdentify.SetControlRed("txtCA_" + idList[index]);
                            ProfessionalObjectIdentify.SetControlRed("txtCA_" + idList[index]);
                            RdAWBSSettings.SetControlRed("txtCA_" + idList[index]);
                        }
                    }

                }
            }

            if (mandatory) {
                var message = "Please fill all mandatory attribute";
                //inserimento messaggi custom
                if (objectsID.indexOf(SpendingCurve.ControlUniqueId) != -1)
                    message = SpendingCurve.MandatoryError(message);
                self.config.inEdit = false;
                document.getElementById("lbl_" + givenParentID).innerHTML = message;
                return false;
            }

            var passType = "";
            ////////////validazione tipologia input ////////////////
            for (index = 0; index < idList.length; index++) {
                if (idList[index] != "") {
                    var txt = document.getElementById("txtCA_" + idList[index]);
                    if (txt != null) {


                        if (txt.value != "") {
                            txt.style.bkColor = txt.style.backgroundColor;
                            txt.style.backgroundColor = "white";
                            if (idsNumber.indexOf(idList[index]) != -1) {
                                //////////////varifica input sia un numero ////////////////
                                if (!self.IsNumeric(txt.value)) {
                                    txt.style.backgroundColor = "yellow";
                                    passType = passType + txt.attributes["descr"].value + " must be in numeric format <br/>";
                                }

                            }
                            else if (idsString.indexOf(idList[index]) != -1) {

                            }
                        }
                    }

                }
            }
            if (passType != "") {
                self.config.inEdit = false;
                document.getElementById("lbl_" + givenParentID).innerHTML = passType;
                if (passType.split("<br/>").length > 1) {
                    var nuovaAltezza = (passType.split("<br/>").length - 1) * 12;
                    document.getElementById("lbl_" + givenParentID).style.height = nuovaAltezza + "px";

                }
                return false;
            }
            ////////////validazione regole////////////
            var listIDNotValid = "";
            for (index = 0; index < idList.length; index++) {
                if (idList[index] != "") {
                    var txt = document.getElementById("txtCA_" + idList[index]);

                    if (txt != null) {

                        txt.style.bkColor = txt.style.backgroundColor;
                        txt.style.backgroundColor = "white";
                        if (txt.value != "") {
                            var retValue = self.ValidateQuestionAnswer(givenParentID, idList[index], txt.value);
                            if (retValue != "OK") {
                                txt.style.backgroundColor = "red";
                                listIDNotValid = listIDNotValid + retValue + "<br/>"
                            }
                        }
                    }

                }
            }
            if (listIDNotValid != "") {
                document.getElementById("lbl_" + givenParentID).innerHTML = listIDNotValid;
                if (listIDNotValid.split("<br/>").length > 1) {
                    var nuovaAltezza = (listIDNotValid.split("<br/>").length - 1) * 12;
                    document.getElementById("lbl_" + givenParentID).style.height = nuovaAltezza + "px";
                }
                self.config.inEdit = false;
                return false;
            }
            else {
                ////////////validazioni custom////////////
                var message = CommonValidation.VerifyRangeDate();
                if (message != "") {
                    document.getElementById("lbl_" + givenParentID).innerHTML = message;
                    return false;
                }
                var message = CheckActivityOnBudget.VerifyRangeDateBudget();
                if (message != "") {
                    document.getElementById("lbl_" + givenParentID).innerHTML = message;
                    return false;
                }
                if (SpendingCurve.ReplacedTextbox() != null && SpendingCurve.ReplacedTextbox().disabled == false) {
                    var message = SpendingCurve.ValidateError("");
                    if (message != "") {
                        document.getElementById("lbl_" + givenParentID).innerHTML = message;
                        return false;
                    }
                }


                if (RDAAttachment.OfferAttachmentTextbox() != null || RDAAttachment.SSAttachmentTextbox() != null) {
                    var myParentCodeO = $(RDAAttachment.OfferAttachmentTextbox()).attr("parent");
                    var myParentCodeS = $(RDAAttachment.SSAttachmentTextbox()).attr("parent");
                    var message = "";
                    if (myParentCodeO == givenParentID)
                        message = RDAAttachment.OfferValidate();
                    else if (myParentCodeS == givenParentID)
                        message = RDAAttachment.SSValidate();

                    if (message != "") {
                        document.getElementById("lbl_" + givenParentID).innerHTML = message;
                        return false;
                    }
                }
                message = CommonValidation.ValidateAlfaNumericValue('DrawingCode');
                if (message != "") {
                    document.getElementById("lbl_" + givenParentID).innerHTML = message;
                    return false;
                }


                setTimeout(function () {
                    self.goNextStep(self.getEmptyDialogItem(1, givenParentID, objectsID, selId, valToSave));
                }, 200);

            }

        }
        self.disable_other_questions(!self.config.inEdit, btn);
        self.config.inEdit = false;

    },

    enableEditing: function (qid, objectsID, btn) {
        var self = this;

        try {
            if (btn.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.outerHTML.indexOf("RdA Remedy Summary") != -1)
                return;
        }
        catch (err) { };
        self.config.inEdit = true;
        self.disable_other_questions(true, btn);
        //$(newquestion).prop('disabled', true);
        if (objectsID != "") {
            inEdit = true;
            var idList = objectsID.split(";");
            var index;
            var mandatory = false;
            var valToSave = "";
            ////////////validazione mandatory effettuta////////////
            for (index = 0; index < idList.length; index++) {
                if (idList[index] != "") {
                    var txt = document.getElementById("txtCA_" + idList[index]);
                    if (txt != null) {
                        txt.removeAttribute('disabled');
                        if (RemedyIdentify.EnableControl("txtCA_" + idList[index]) == false)
                            SupplierIdentify.EnableControl("txtCA_" + idList[index]);
                        Locationdentify.EnableControl("txtCA_" + idList[index]);
                        ProfessionalObjectIdentify.EnableControl("txtCA_" + idList[index]);
                        //RdAProjectSettings.EnableControl("txtCA_" + idList[index]);
                        RdAWBSSettings.EnableControl("txtCA_" + idList[index]);
                        SpendingCurve.EnableControl("txtCA_" + idList[index]);
                        //RdABrand.EnableControl("txtCA_" + idList[index]);
                        RDAAttachment.EnableControl("txtCA_" + idList[index]);
                    }

                }
            }
            var btnSave = "saveCA_" + qid;
            var btn = document.getElementById(btnSave);
            if (btn != null)
                btn.style.display = "inline";

            var btnEdit = "editCA_" + qid;
            btn = document.getElementById(btnEdit);
            if (btn != null)
                btn.style.display = "none";


        }
    },

    disable_other_questions: function (value, currentBtn) {
        $('#newquestion *').prop('disabled', value);
        $('#showquestion a>IMG').prop('disabled', value);
        $('#showquestion li').prop('disabled', value);

        var div = $(currentBtn).parentsUntil("div");
        if (div != null) {

            div = div[div.length - 1];
            div = $(div).parent().get(0);
            $(div).prop('disabled', !value);
            $(div).find('*').prop('disabled', !value);

            var parent = $(div);//.parent();
            if (parent && value)
                $(parent).addClass("inEditing");
            else if (parent && !value)
                $(parent).removeClass("inEditing");
        }





        $('#btnSaveDialog').prop('disabled', value);

        SpendingCurve.EnableDisableControl();

    },

    ValidateQuestionAnswer: function (parid, target, currentDialogValue) {
        var url = "erda/dialog/validateqa?parentId=" + parid + "&target=" + target + "&value=" + currentDialogValue + "&type=CA";
        return this.performRequest(url, "GET", null);
    },

    getFeatureValue: function (givenFeatureName) {
        return this.performRequest("erda/dialog/getfeaturevalue?givenFeatureName=" + givenFeatureName, "GET", null);
    },
    CASendUpdate: function (givenParentID, objectsID, valToSave, selId, btn) {

        var self = this;
        var dialogStep = null;
        self.onContentRender(dialogStep);

        if (self.config.inEdit)
            self.disable_other_questions(false, btn);
        if (self.co.shad)
            self.co.shad.setP
        self.config.inEdit = false;
    },

    showDatePicker: function (img, name, position) {
		
        var self = this;
        var dest = document.getElementById(name);
        if (dest.disabled == false) {
            var cumulativeOffset = null;//getPosition(img, position);
            cumulativeOffset = self.findElementPosition(dest);
            var dateMenu = Ext.create('Ext.menu.DatePicker', {
                handler: function (dp, date) {
                    dest.value = Ext.Date.format(date, Ext.decode(localStorage.localizationInfo).ShortDateFormat);
                    $(dest).trigger("onDateSelected");
                }
            }).showAt([cumulativeOffset.left, cumulativeOffset.top]);


        }
    },
    applyCurrency: function (obj) {
        var self = this;
        try {
            if (obj.value == "") {
                obj.value = "€";
                $(obj).autoNumeric("init", { aSep: '.', aDec: ',', aSign: '€ ' });
            }
            else {

                var currency = "€";
                if (obj.value.indexOf("$") != -1)
                    currency = "$";

                if (currency == "$") {
                    $(obj).autoNumeric("init", { aSep: ',', aDec: '.', aSign: '$ ' });
                    $(obj).autoNumeric('update', { aSep: ',', aDec: '.', aSign: '$ ' });
                }
                else {
                    $(obj).autoNumeric("init", { aSep: '.', aDec: ',', aSign: '€ ' });
                    $(obj).autoNumeric('update', { aSep: '.', aDec: ',', aSign: '€ ' });
                }

            }
        } catch (err) { };
        

    },

    updateCurrency: function (obj, classname) {
        var self = this;
        var objName = document.getElementById(obj);
        $(objName).autoNumeric('init');

        if (classname == "$") {

            //currentValue.replace(",", ".").replace(".", ",");
            $(objName).autoNumeric('update', { aSep: ',', aDec: '.', aSign: '$ ' });
            //$(objName).maskMoney({ thousands: ',', decimal: '.', allowZero: true, suffix: ' $' });
            //$(objName).maskMoney('mask', num);
        }
        else if (classname == "€") {
            //currentValue.replace(",", ".").replace(".", ",");
            $(objName).autoNumeric('update', { aSep: '.', aDec: ',', aSign: '€ ' });

            //document.getElementById(givenId).value = num.toString().replace(".", ",");
            //$(objName).maskMoney({ thousands: '.', decimal: ',', allowZero: true, suffix: ' €' });
            //$(objName).maskMoney('mask', num.toString().replace(".",","));
        }

    },

    //function utility
    IsNumeric: function (sText) {
        var ValidChars = "0123456789.";
        var IsNumber = true;
        var Char;

        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
            }
        }
        return IsNumber;
    },
    showError: function (errorMessage) {
        var win = this.getView().up("#DialogWin");
        Ext.MessageBox.show({
            title: 'Error eRdA Configuration',
            msg: errorMessage,
            cls: "messageboxforerror",
            icon: Ext.MessageBox.ERROR,
            //icon: 'fa fa-times-circle fa-3x',
            buttons: Ext.Msg.OK,
            fn: function (buttonValue, inputText, showConfig) {
                if (buttonValue == "ok") {
                    win.close();
                    win.destroy();
                }
            },
        });
    },
    getEmptyDialogItem: function (op, parentid, id, selectionid, inputvalue) {
        //STARTUP,
        //GETNEXT,
        //SAVE
        if (op === undefined) op = 0;
        if (id === undefined) id = "";
        if (parentid === undefined) parentid = "";
        if (selectionid === undefined) selectionid = "";
        if (inputvalue === undefined) inputvalue = "";

        var dialogItem = {
            id: id,
            parentId: parentid,
            questionType: "",
            inputValue: inputvalue,
            selectedOption: selectionid,
            operationType: op
        }
        return dialogItem;
    },
    loadjscssfile: function (filename, inject) {
        var filetype = "js";
        if (filename.indexOf(".js?filever=") != -1)
            filetype = "js";
        else if (filename.indexOf(".css?filever=") != -1)
            filetype = "css";
        else
            return;

        if (inject === undefined) inject = true;
        // var invalidateCache = "?vr=" + Math.random();
        if (filetype == "js") { //if filename is a external JavaScript file
            if (inject) {
                if ($('head script[src="' + filename + '"]').length > 0)
                    return;
            }
            else {
                var head = document.getElementsByTagName('head')[0];
                if ($('head script[src="' + filename + '"]').length > 0) {
                    var node = $('head script[src="' + filename + '"]');
                    head.removeChild(node[0]);
                    return;
                }
            }
            var fileref = document.createElement('script');
            fileref.setAttribute("type", "text/javascript");
            fileref.setAttribute("src", filename);
            document.getElementsByTagName("head")[0].appendChild(fileref);
        }
        else if (filetype == "css") { //if filename is an external CSS file
            if (inject) {
                if ($('head link[href="' + filename + '"]').length > 0)
                    return;
            }
            else {
                var head = document.getElementsByTagName('head')[0];
                if ($('head link[href="' + filename + '"]').length > 0) {
                    var node = $('head link[href="' + filename + '"]');
                    head.removeChild(node[0]);
                    return;
                }
            }
            var fileref = document.createElement("link");
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", filename);
            document.getElementsByTagName("head")[0].appendChild(fileref);
        }

    },
    findElementPosition: function (_el) {
        var curleft = 0;
        var curtop = 0;
        var curtopscroll = 0;
        var curleftscroll = 0;

        if (_el.offsetParent) {
            curleft = _el.offsetLeft;
            curtop = _el.offsetTop;

            /* get element scroll position */
            var elScroll = _el;
            while (elScroll = elScroll.parentNode) {
                curtopscroll = elScroll.scrollTop ? elScroll.scrollTop : 0;
                curleftscroll = elScroll.scrollLeft ? elScroll.scrollLeft : 0;

                curleft -= curleftscroll;
                curtop -= curtopscroll;
            }

            /* get element offset postion */
            while (_el = _el.offsetParent) {
                curleft += _el.offsetLeft;
                curtop += _el.offsetTop;
            }
        }

        /* get window scroll position */
        var isIE = navigator.appName.indexOf('Microsoft Internet Explorer') != -1;
        var offsetX = isIE ? document.body.scrollLeft : window.pageXOffset;
        var offsetY = isIE ? document.body.scrollTop : window.pageYOffset;
        var myTop = curtop + offsetY;
        var myLeft = curleft + offsetX;
        return {
            top: myTop,
            left: myLeft
        };

    },



    //request management
    performRequest: function (url, method, data) {
        var self = this;
        if (data === undefined) data = null;
        var response = Ext.Ajax.request({
            url: CD.apiUrl + url,
            method: method,
            jsonData: data,
            withCredentials: true,
            async: false,
            callback: function (opt, success, response) {
                if (!success) {
                    //CDI.service.erdaAction.showErrorMessage("Error eRdA Configuration", "", response);
                    self.showError(self.getMessageError(response));
                    return null;
                }
            }
        });
        response = (Ext.decode(response.responseText));
        response = unescape(response);
        return response;

    },

    getMessageError: function (response) {
        var message = Ext.decode(response.responseText);
        if (message.ExceptionMessage === undefined) {
            if (message.MessageDetail === undefined) {
                if (message.Message === undefined)
                    message = Ext.decode((Ext.decode(response.responseText)).Message);
            }
            else
                message = Ext.decode((Ext.decode(response.responseText)).MessageDetail);

        }
        else
            message = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);

        if (message.ErrorMessage !== undefined)
            message = message.ErrorMessage;
        return message;
    },
    //progress bar management
    onProgressBar: function (flag) {
        return;
        var panel = Ext.ComponentQuery.query('toolbar panel', this.config.view.up())[0];
        var pBar = panel.down("#pBar");
        if (!pBar) {
            pBar = new Ext.ProgressBar({
                id: 'pBar',
                cls: 'dialogProgressBar',
                text: "<span class='txtProgress'>Ready</span>"
            });
            panel.add(pBar);
        }
        if (flag === undefined) return;
        if (pBar && flag == 'start') {
            pBar.wait({
                interval: 300, //bar will move fast!
                //duration: 250000,
                //increment: 10,
                text: "<span class='txtProgressAction'>Loading Data...</span>",
                scope: this,
            });
        }
        else if (pBar && flag == 'stops') {
            pBar.reset();
            pBar.updateText("<span class='txtProgress'>Ready</span>");
        }
        //panel.doLayout();
    },
    AddNewTranche: function (button) {
        var myPanel = button.up("#myPanelTranche");
        myPanel.component.addRowTranche(myPanel.down("#rowMaster").component, myPanel.down("#percTranche").component, myPanel.down("#dateTranche").component);

    },
    RemoveTranche: function (button) {
        var myRow = button.up();
        var myPanel = button.up("#myPanelTranche");
        var myArray = myPanel.row;
        var rowMaster = myPanel.down("#rowMaster");
        myPanel.component.removeRowTranche(rowMaster, myArray, myRow);

    }

});