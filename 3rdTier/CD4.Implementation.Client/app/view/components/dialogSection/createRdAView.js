﻿Ext.define('CDI.view.components.dialogSection.createRdAView', {});

Ext.deferDefine('CDI.view.components.dialogSection.createRdAView', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'createrdaview',
        controller: 'dialogconfigurationmanager',
        flex: 1,
        layout: 'fit',
        listeners: {
            beforerender: 'injectScripts',
            afterrender: 'startDialogConfigurationWithDelay',
            
        },
        items: [
            {
            html: '<table style="width: 100%"><tr><td style="width: 50%" valign="top"><div id="container" class="topbar">' + 
                       '<div id="showquestion" class="answers"></div></div></td>' + 
                        '<td style="background-position: right bottom; width: 50%; background-repeat: no-repeat;" align="left" valign="top">' +
                        '<div id="newquestion" class="answers"></div></td></tr ></table >',

            }
        ]
    }
});