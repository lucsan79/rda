﻿Ext.define('CDI.view.components.rdaAdminCenter.rdaAdminCenterWrapperController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.rdaadmincenterwrapper',

        //=== Lifecycle Events=================
        onBeforeRender: function (view) {
            var adminNavigationList = view.down('#adminNavigationList');

            var adminNavigationListEl = adminNavigationList.getEl();
            var root = adminNavigationList.getStore().getRoot();
            var childElements = adminNavigationListEl.query('li');

            adminNavigationList.setSelection(root.firstChild);
            for (var i = 0; i < root.childNodes.length; i++) {
                $(childElements[i]).attr('data-tooltip', root.getChildAt(i).data.text);
            }
        },
        //=====================================

        //=== Click Events ====================
        onTreelistSelectionChange: function (view, treeviewItem) {
           
            var admincenterwrapper = view.up();
            if (admincenterwrapper != null) {
                var adminContainerPanel = admincenterwrapper.down('#adminContainerPanel');
                var adminContainerPanelItem = adminContainerPanel.down();

                if (adminContainerPanelItem != null) {
                    adminContainerPanelItem.destroy();
                    delete adminContainerPanelItem;
                }

                admincenterwrapper.isLoading = true;

                var viewPath = treeviewItem.data.viewPath;
                var viewMode = treeviewItem.data.viewMode;

                //switch (treeviewItem.data.index) {
                //    case 0:
                //        viewPath = 'CD.view.components.applicationSection.adminCenter.domain.DomainWrapper';
                //        viewMode = 'adminDomain';
                //        break;
                //    case 1:
                //        viewPath = 'CD.view.components.applicationSection.adminCenter.organization.OrganizationWrapper';
                //        viewMode = 'adminOrganization';
                //        break;
                //    case 2:
                //        viewPath = 'CD.view.components.applicationSection.adminCenter.modelData.ModelDataWrapper';
                //        viewMode = 'adminModelData';
                //        break;
                //    case 3:
                //        viewPath = 'CD.view.components.applicationSection.adminCenter.tools.ToolsWrapper';
                //        viewMode = 'adminTools';
                //        break;
                //    default:
                //        viewPath = null;
                //        viewMode = null;
                //        break;
                //}

                if (viewPath != null) {
                    //admincenterwrapper.special.viewMode = viewMode;
                    adminContainerPanel.add(Ext.create(viewPath,
                        {
                            special: admincenterwrapper.special
                        }));
                }
            }
        }
        //=====================================
    });