﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogContent.dialogContentWrapper', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogContent.dialogContentWrapper', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'dialogcontentwrapper',
        controller: 'dialogcontentwrappercontroller',
        layout: 'fit',
        cls: 'admin-wrapper-container',
        items: [
            {
                xtype: 'tabpanel',
                width: '100%',
                layout: 'fit',
                ui: 'navigation',
                tabPosition: 'left',
                cls: 'x-subtreelist-navigation domain-navigation subnavigation-border-right',
                tabRotation: 0,
                tabBar: {
                    border: false,
                },
                plain: true,
                minTabWidth: 150,
                bodyBorder: true,
                bodyStyle: {
                    background: '#fff',
                    'padding-left': '5px',
                    'padding-right': '1px',
                    'border-left': 'solid',
                    'border-left-color': '#c5c5c5',
                    'border-left-width': '1px',
                    'border-top-width':'0px'
                },
                defaults: {
                    textAlign: 'left',
                    layout: 'fit',
                    cls: 'admin-content-panel',
                    style: {
                        'padding-bottom': '0px'
                    }
                },

                items: [
                    {
                        title: '<i class="fas fa-user-tie"></i> ' + CDI.service.Translate.data['rda-admincenter-content-professionalrole'],
                        items: {
                            xtype: 'professionalroleview',
                        }
                    },
                    {
                        title: '<i class="fas fa-user-tag"></i> ' + CDI.service.Translate.data['rda-admincenter-content-professionallevels'],
                        items: {
                            xtype: 'professionallevels',
                        }
                    },
                    {
                        title: '<i class="fas fa-map-marker-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-content-location'],
                        items: {
                            xtype: 'professionallocation',
                        }
                    },
                   
                ]
            }
        ]

    }
});