﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalRoleView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalRoleView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'professionalroleview',
        controller: 'adminconfigcontroller',
        title: false,
        cls: 'gray-window-title',
        layout: 'border',

       
        defaults: {
            collapsible: true,
            split: true,
            border: false,
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                region: 'west',
                layout: 'fit',
                margin: '0 5 1 0',
                title: '<i class="fas fa-user-tie"></i> ' + CDI.service.Translate.data['rda-admincenter-content-professionalrole'],
                header: {
                    style: { 'padding': '8px 10px 8px 10px' },
                },
                flex: 2,
                frame: true,
                items: [
                    {
                        xtype: 'adminconfiggrid',
                        itemId: 'professionalrole',
                        listeners: {
                            beforerender: 'onBeforeRender',
                        },
                        config: {
                            masterItem: 'professionalrole',
                            gridItem: '',
                            header: CDI.service.Translate.data['rda-admincenter-content-professionalrole-label'],
                            emptytext: CDI.service.Translate.data['rda-admincenter-content-professionalrole-emptytext'],
                        }
                    }
                ]
            }
            ,
            {
                region: 'center',
                layout: 'fit',
                margin: '0 2 1 0',
                title: '<i class="fas fa-user-tie"></i> ' + CDI.service.Translate.data['rda-admincenter-content-pwtprofessionalrole'],
                header: {
                    style: { 'padding': '8px 10px 8px 10px' },
                },
                flex: 2,
                frame: true,
                items: [
                    {
                        xtype: 'adminconfiggrid',
                        itemId: 'pwtprofessionalrole',
                        listeners: {
                            beforerender: 'onBeforeRender',
                        },
                        config: {
                            masterItem: 'pwtprofessionalrole',
                            gridItem: '',
                            header: CDI.service.Translate.data['rda-admincenter-content-pwtprofessionalrole-label'],
                            emptytext: CDI.service.Translate.data['rda-admincenter-content-pwtprofessionalrole-emptytext'],
                        }
                    }
                ]
            }
        ]
    }
});