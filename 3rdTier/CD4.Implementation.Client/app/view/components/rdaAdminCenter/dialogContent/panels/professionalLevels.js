﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalLevels', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalLevels', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'professionallevels',
        controller: 'adminconfigcontroller',
        title: false,
        cls: 'gray-window-title',
        layout: 'border',

       
        defaults: {
            collapsible: true,
            split: true,
            border: false,
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                region: 'west',
                layout: 'fit',
                margin: '0 5 1 0',
                title: '<i class="fas fa-user-tag"></i> ' + CDI.service.Translate.data['rda-admincenter-content-professionallevels'],
                header: {
                    style: { 'padding': '8px 10px 8px 10px' },
                },
                flex: 1,
                frame: true,
                items: [
                    {
                        xtype: 'adminconfiggrid',
                        itemId: 'professionallevel',
                        listeners: {
                            beforerender: 'onBeforeRender',
                        },
                        config: {
                            masterItem: 'professionallevel',
                            gridItem: '',
                            header: CDI.service.Translate.data['rda-admincenter-content-professionallevels-label'],
                            emptytext: CDI.service.Translate.data['rda-admincenter-content-professionallevels-emptytext'],
                        }
                    }
                ]
            }
          
        ]
    }
});