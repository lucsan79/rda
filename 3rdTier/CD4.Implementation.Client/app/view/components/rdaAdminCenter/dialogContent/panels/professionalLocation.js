﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalLocation', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogContent.panels.professionalLocation', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'professionallocation',
        controller: 'adminconfigcontroller',
        title: false,
        cls: 'gray-window-title',
        layout: 'border',

        listeners: {
            //beforerender: 'onBeforeRender',
        },
        defaults: {
            collapsible: true,
            split: true,
            border: false,
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                region: 'west',
                layout: 'fit',
                margin: '0 5 1 0',
                title: '<i class="fas fa-map-marker-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-content-location'],
                header: {
                    style: { 'padding': '8px 10px 8px 10px' },
                },
                flex: 1,
                frame: true,
                items: [
                    {
                        xtype: 'adminconfiggrid',
                        itemId: 'professionallocation',
                        listeners: {
                            beforerender: 'onBeforeRender',
                        },
                        config: {
                            masterItem: 'locations',
                            gridItem: '',
                            header: CDI.service.Translate.data['rda-admincenter-content-location-label'],
                            emptytext: CDI.service.Translate.data['rda-admincenter-content-location-emptytext'],
                        }
                    }
                ]
            }
          
        ]
    }
});