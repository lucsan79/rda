﻿Ext.define('CDI.view.components.rdaAdminCenter.rdaAdminCenterWrapper', {
    requires: [
        'CDI.view.components.rdaAdminCenter.supplierManagement.SupplierListManagement',
        'CDI.view.components.rdaAdminCenter.supplierManagement.SupplierPriceListManagement'
    ]
});

Ext.deferDefine('CDI.view.components.rdaAdminCenter.rdaAdminCenterWrapper', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'rdaadmincenterwrapper',
        controller: 'rdaadmincenterwrapper',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        cls:'padding-admin',
        items: [
            {
                xtype: 'treelist',
                itemId: 'adminNavigationList',
                ui: 'navigation',
                cls: 'small-navigation admin-navigation',
                getItemConfig: function (node, parent) {
                    return Ext.apply({
                        parentItem: parent.isRootListItem ? null : parent,
                        owner: this,
                        node: node,
                        indent: 10,
                        cls: node.data.cls
                    }, this.getDefaults());
                },
                store: {
                    type: 'tree',
                    fields: ['text'],
                    
                    root: {
                        expanded: true,
                        children: [
                           {
                                text: CDI.service.Translate.data['rda-admincenter-rdaconfgman'],
                                iconCls: 'fas fa-magic',
                                cls: 'adminwrapper-tooltips',
                                viewPath: 'CDI.view.components.rdaAdminCenter.dialogConfig.dialogConfigWrapper',
                                leaf: true
                            },
                            {
                                text: CDI.service.Translate.data['rda-admincenter-rdacontgman'],
                                iconCls: 'fas fa-list-alt',
                                cls: 'adminwrapper-tooltips',
                                viewPath: 'CDI.view.components.rdaAdminCenter.dialogContent.dialogContentWrapper',
                                leaf: true
                            }, 
                            {
                                text: CDI.service.Translate.data['rda-admincenter-userroleman'],
                                iconCls: 'fas fa-users',
                                cls: 'adminwrapper-tooltips',
                                viewPath: 'CDI.view.components.rdaAdminCenter.roleManager.roleManagerWrapper',
                                leaf: true
                            },
                            {
                                text: CDI.service.Translate.data['rda-admincenter-supplierlist'],
                                iconCls: 'fas fa-user-tie',
                                cls: 'adminwrapper-tooltips',
                                viewPath: 'CDI.view.components.rdaAdminCenter.supplierManagement.SupplierListManagement',
                                leaf: true
                            },
                            {
                                text: CDI.service.Translate.data['rda-admincenter-pricelist'],
                                iconCls: 'fas fa-euro-sign',
                                cls: 'adminwrapper-tooltips',
                                viewPath: 'CDI.view.components.rdaAdminCenter.supplierManagement.SupplierPriceListManagement',
                                leaf: true
                            },
                            {
                                text: CDI.service.Translate.data['rda-admincenter-remedymanagement'],
                                iconCls: 'fas fa-coins',
                                cls: 'adminwrapper-tooltips',
                                viewPath: 'CDI.view.components.rdaAdminCenter.supplierManagement.supplierWrapper',
                                leaf: true
                            }
                        ]
                    },
                },
                width: 70,
                style: {
                    'overflow-y': 'auto'
                },
                expanderFirst: false,
                expanderOnly: true,
                listeners: {
                    selectionchange: 'onTreelistSelectionChange'
                }
            },
            {
                itemId: 'adminContainerPanel',
                flex: 1,
                bodyStyle: { 'background-color': 'gray' },
                layout: 'fit',
                style: {
                    'padding-left': '0px',
                    'margin-top': '0px',
                    'margin-bottom': '0px'
                }
            }
        ],


        listeners: {
            beforerender: 'onBeforeRender',
            render: function () {
                Ext.tip.QuickTipManager.init();
            },
        }
    }
});