﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.roleGridWithOutBBar', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.roleGridWithOutBBar', function () {
    return {
        extend: 'Ext.grid.Panel',
        controller: 'rolegridcontroller',
        xtype: 'rolegridadminwithoutbbar',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid colext',
        
        config: {
            role: '',
            header: '',
            starttext: '',
            emptytext: ''

        },
        plugins: [
            'gridfilters'
        ],
        listeners: {
            beforerender: 'onBeforeRender',
        //   'loadData': 'onLoadData',
           select: 'onGridRowSelected',
        },
        enableColumnHide: false,
        allowDeselect: false,
        //selType: 'checkboxmodel',
        columns: [
            {
                text: '.',
                flex: 1,
                dataIndex: 'item',
                editor: { allowBlank: false },
                align: 'left',
                sortable: true,
                filter: true,
                listeners: {
                    render: function () {
                        this.setText(this.up("grid").config.header);
                    },
                },
                renderer: function (value, meta, record) {
                   
                    var project = record.data.item;
                    if (record.data.subitem.toLowerCase() == "vehicle") {
                        return '<span class="grayicon"><i class="fas fa-car"></i><span> ' + project;
                    }
                    else if (record.data.subitem.toLowerCase() == "powertrain") {
                        return '<span class="grayicon"><i class="fas fa-bolt"></i><span> ' + project;
                    }
                    else if (record.data.subitem.toLowerCase() == "transversal") {
                        return '<span class="grayicon"><i class="fas fa-layer-group"></i><span> ' + project;
                    }
                    else {
                        return '<span class="grayicon"><i class="far fa-circle"></i><span> ' + project;
                    }
                }
            },
            //{
            //    text: '',
            //    hidden:true, 
            //    dataIndex: 'subitem',
            //}
        ],
        
    }
});