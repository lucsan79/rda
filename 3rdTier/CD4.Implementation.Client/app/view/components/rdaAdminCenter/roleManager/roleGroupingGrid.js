﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.roleGroupingGrid', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.roleGroupingGrid', function () {
    return {
        extend: 'Ext.panel.Panel',
        controller: 'rolegridcontroller',
        xtype: 'rolegroupinggrid',
        //layout:'accordion',
        layout: {
            type: 'accordion',
            animate: false,
            multi:true,
            fill: false,
           
        },
        
        openable: 2,
        height: '100%',
        width: '100%',
        autoScroll:true,
        cls: 'basicgrid colext subcolext',
        requires: [
            'Ext.layout.container.Accordion',
            'Ext.grid.*'
        ],
        //config: {
        //    role: '',
        //    target: '',
        //    header: '',
        //    starttext: '',
        //    emptytext: ''
        //},

        listeners: {
            beforerender: 'onBeforeRenderComplex',
            //afterrender: 'onAfterRender',
        },
        defaults: {
            plugins: [
                'gridfilters',
                {
                    ptype: 'cellediting',
                    clicksToEdit: 1,
                    listeners: {
                        beforeedit: 'onGridBeforeEdit',
                        validateedit: 'onGridValidateEdit',
                    }
                }
            ],

            controller: 'rolegridcontroller',
            margin: '5px 0px 0px 0px',
            height:'400px',
            layout: 'fit',
            xtype: 'grid',
            selType: 'checkboxmodel',
            viewConfig: {
                emptyText: CDI.service.Translate.data["rda-admincenter-content-role-emptytext2"]
            },
            columns: [
                {
                    flex: 1,
                    cls:'innerColumn',
                    dataIndex: 'item',
                    editor: { allowBlank: false },
                    align: 'left',
                    sortable: true,
                    filter: true,
                    text: CDI.service.Translate.data['rda-admincenter-activitynpi-label'],
                    getEditor: function (record) {
                        var currentGrid = this.up("grid");
                        var combo = Ext.create('Ext.form.field.ComboBox', {
                            displayField: 'Value',
                            valueField: 'Value',
                            store: {
                                proxy: {
                                    type: 'ajax',
                                    withCredentials: true,
                                    url: CD.apiUrl + 'erda/admin/getAvailableItems',
                                    extraParams: {
                                        target: 'activitynpi',
                                        subItem:'project',
                                        value:this.up("grid").up().config.target,
                                    },
                                    autoload: false,
                                    reader: {
                                        type: 'json'
                                    }
                                }
                            },
                            minChars: 1,
                            //queryMode: 'remote',
                            allowBlank: false,
                            forceSelection: true,
                            filterPickList: false
                        });


                        return Ext.create('Ext.grid.CellEditor', {
                            field: combo
                        });


                    },

                },
                {
                    
                    flex: 1,
                    cls: 'innerColumn',
                    dataIndex: 'subitem',
                    editor: { allowBlank: false },
                    align: 'left',
                    sortable: true,
                    filter: true,
                    text: CDI.service.Translate.data['rda-admincenter-userlsit-label'],
                    getEditor: function (record) {
                        var currentGrid = this.up("grid");
                        var combo = Ext.create('Ext.form.field.ComboBox', {
                                displayField: 'Value',
                                valueField: 'Value',
                                store: {
                                    proxy: {
                                        type: 'ajax',
                                        withCredentials: true,
                                        url: CD.apiUrl + 'erda/admin/getAvailableUsers',
                                        extraParams: {
                                            target: this.up("grid").up().config.target,
                                        },
                                        autoload: false,
                                        reader: {
                                            type: 'json'
                                        }
                                    }
                                },
                                minChars: 1,
                                //queryMode: 'remote',
                                allowBlank: false,
                                forceSelection: true,
                                filterPickList: false
                            });
                        
                        
                        return Ext.create('Ext.grid.CellEditor', {
                            field: combo
                        });


                    },
                    
                }
            ],
            //hideHeaders: true
            listeners: {
                afterrender: 'onAfterRender',
            },
            bbar: [
                '->',
                {
                    tooltip: CDI.service.Translate.data['rda-admincenter-editbtn-tooltip'],
                    iconCls: 'fas fa-pencil-alt',
                    cls: 'pencil',
                    itemId: 'edit',
                    handler: 'onButtonClick'
                },
                {
                    tooltip: CDI.service.Translate.data['rda-admincenter-reloadbtn-tooltip'],
                    iconCls: 'fas fa-sync-alt',
                    cls: 'sync',
                    itemId: 'refresh',
                    handler: 'onButtonClick'
                },
                {
                    tooltip: CDI.service.Translate.data['rda-admincenter-cancelbtn-tooltip'],
                    iconCls: 'fas fa-ban',
                    cls: 'ban',
                    itemId: 'cancel',
                    disabled: true,
                    handler: 'onButtonClick'

                },
                {
                    tooltip: CDI.service.Translate.data['rda-admincenter-addbtn-tooltip'],
                    iconCls: 'fas fa-plus',
                    cls: 'plus',
                    itemId: 'add',
                    disabled: true,
                    handler: 'onButtonClick'
                },
                {
                    tooltip: CDI.service.Translate.data['rda-admincenter-deletebtn-tooltip'],
                    iconCls: 'fas fa-trash-alt',
                    cls: 'trash',
                    itemId: 'delete',
                    disabled: true,
                    handler: 'onButtonClick'
                },
                {
                    tooltip: CDI.service.Translate.data['rda-admincenter-savebtn-tooltip'],
                    iconCls: 'fas fa-save',
                    cls: 'save',
                    itemId: 'save',
                    disabled: true,
                    handler: 'onButtonClick'
                }
                
            ],
        },
        items: [],
       
    }
});