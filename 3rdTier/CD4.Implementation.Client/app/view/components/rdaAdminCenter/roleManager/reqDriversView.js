﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.reqDriversView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.reqDriversView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'reqdriversview',
        controller: 'reqdriversviewcontroller',
        itemId: 'reqdriversview',
        cls: 'rdahomeview',
        layout: 'border',
        
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                
                region: 'center',
                flex: 1,
               
                defaults: {
                    flex:1,
                    margin: '0px 5px 0px 0px',
                    frame: true,
                    scrollable: true,
                    cls: 'roleadmin basicgrid',
                    layout: {
                        type: 'hbox',
                        pack: 'start',
                        align: 'stretch'
                    },
                    
                },
                items: [
                    {
                        title: false,
                        //iconCls: 'fas fa-user-friends',
                        itemId:'requester_list',
                        items: [
                            {
                                xtype: 'rolegridadmin',
                                itemId:'req',
                                config: {
                                    role: 'requester',
                                    target: 'RDA',
                                    header: "<i class='fas fa-user-friends' ></i> " + CDI.service.Translate.data["rda-admincenter-rolemanagement-role_requester"] 
                                }
                            },
                        ]
                    },
                    {
                        title: CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pl"],
                        iconCls: 'fas fa-user-friends',
                        //itemId: 'planner_list',
                        flex:1.3,
                        items: [
                            {
                                xtype: 'rolegroupinggrid',
                                itemId: 'planner',
                                itemId: 'planner_list',
                                config: {
                                    role: 'planner',
                                    target: 'RDA',
                                }
                            },
                        ]
                    },
                    {
                        title:false,
                        //iconCls: 'fas fa-user-friends',
                        itemId: 'ppm_list',
                        items: [
                            {
                                xtype: 'rolegridadmin',
                                itemId: 'ppm',
                                config: {
                                    role: 'ppm',
                                    target: 'RDA',
                                    header: "<i class='fas fa-user-friends'></i> " + CDI.service.Translate.data["rda-admincenter-rolemanagement-role_ppm"]
                                }
                            },
                        ]
                    },
                    {
                        title:false,// CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pmp"],
                        //iconCls: 'fas fa-cubes',
                        itemId: 'pmp_list',
                        items: [
                            {
                                xtype: 'rolegridadmin',
                                itemId: 'pmp',
                                config: {
                                    role: 'pmp',
                                    target: 'RDA',
                                    header: "<i class='fas fa-user-friends'></i> " + CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pmp"]
                                }
                            },
                        ]

                    },
                  
                    
                ]
                
            }
        ]




    }
});