﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.roleGridSimply', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.roleGridSimply', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'rolegridsimply',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid colext',
        
        
        plugins: [
            'gridfilters'
        ],
        enableColumnHide: false,
        allowDeselect: false,
        selType: 'checkboxmodel',
        columns: [
            {
                text:'.',
                flex: 1,
                dataIndex: 'item',
                align: 'left',
                sortable: true,
                filter: true,
                
            }
        ],
    }
});