﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.roleGridController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.rolegridcontroller',

    onBeforeRender: function (grid) {
        
        var self = this;
        var currentView = grid.view.up().up().up();
        var data = null;
        
        grid.setLoading(true);
        grid.setEmptyText('<h4 style="margin-top:-2px;">' + CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'] + ' </h4>');
		
        var url = CD.apiUrl + 'erda/admin/getProjectRoleUserItems';
        
        if (grid.getXType() == "rolegridadminwithoutbbar") {
            
            var grids = grid.up().up().query('rolegridadmin');
            if (grids != null && grids.length > 0) {
                grids.forEach(function (gridadmin) {
                    if (gridadmin.config.target != "") {
                        gridadmin.config.target = '';
                        gridadmin.config.competence = '';
                        gridadmin.fireEvent('beforerender', gridadmin);
                    }
                });
            }
        }

        
        if (grid.config.data != null) {
			
            var storeObject = {
                data: grid.config.data
            };
            grid.setStore(Ext.create('Ext.data.Store', storeObject));
            grid.setLoading(false);
        }
        else {
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json'
                },
                url: url,
                method: 'POST',
                jsonData: {
                    role: grid.config.role,
                    target: grid.config.target,
                },
                withCredentials: true,
                failure: function (response) {
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    grid.setLoading(false);
                },
                success: function (jsonData) {
                    var data = Ext.decode(jsonData.responseText);
                    var storeObject = {
                        data: []
                    };
                    if (data != null && data.length > 0)
                        storeObject.data = data;
                    grid.setStore(Ext.create('Ext.data.Store', storeObject));
                    grid.setLoading(false);
                    
                    var cancel = ["edit", "addadv", "refresh"];
                    var buttons = grid.query("button");
                    self.onButtonRender(cancel, buttons);
                    var columnSelection = grid.headerCt.down("[isCheckerHd]");
                    if (columnSelection) columnSelection.hide();
                }
            });
        }
    },

    onBeforeRenderComplex: function (accordion) {
        var layout = accordion.getLayout();
        var currentView = accordion.up().up().up();
        var data = null;
        accordion.setLoading(true);
        var url = CD.apiUrl + 'erda/admin/getProjectRoleUserItems';
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: url,
            method: 'POST',
            jsonData: {
                role: accordion.config.role,
                target: accordion.config.target,
                competence: accordion.config.competence
            },
            withCredentials: true,
            failure: function (response) {
				CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                //var exceptionMessage = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);
                //var errorCode = exceptionMessage.ErrorCode;
                //var errorMessage = exceptionMessage.ErrorMessage;
                //grid.setLoading(false);
                //Ext.MessageBox.show({
                //    title: CDI.service.Translate.data['menuSection-etazebao-variants-title'],
                //    msg: errorMessage,
                //    icon: 'fa fa-times-circle fa-3x',
                //    buttons: Ext.Msg.OK
                //});
                setTimeout(function () { layout.animate = true; }, 1000);
            },
            success: function (jsonData) {
                var opened = [];
                opened.push(0);
				if(accordion.items!=null && accordion.items.getCount() > 0)
				{
					for (var i = accordion.items.getCount() - 1; i >= 0; i--) {
						if (accordion.items.items[i].collapsed == false)
							opened.push(i);
						accordion.items.items[i].destroy();
					}
				}
                accordion.defaults.columns[0].hidden = false;
                accordion.defaults.columns[0].hidden = false;
                if (accordion.config.role == "planner" || accordion.config.role == "ur") {
                    accordion.defaults.columns[0].hidden = true;
                }
                else if (accordion.config.role == "pr") {
                    if (accordion.config.refData.subitem != "TRANSVERSAL")
                        accordion.defaults.columns[0].hidden = true;
                }

                var data = Ext.decode(jsonData.responseText);
                var storeObject = {
                    data: []
                };
                if (data != null && data.length > 0) {
                    var props = Object.keys(data[0].releatedItems);
                    if (props != null) {
                        props.forEach(function (entry) {
                            var myArray = data[0].releatedItems[entry];
                            var myData = [];
                            if (myArray.length > 0) {

                                for (var index = 0; index < myArray.length; index++) {
                                    if (accordion.config.role == "planner" || accordion.config.role == "ur") {
                                        var pushItem = { item: '', subitem: myArray[index] };
                                        myData.push(pushItem);
                                    }
                                    else if (accordion.config.role == "pr") {
                                        var pushItem = { item: myArray[index].split("|")[0], subitem: myArray[index].split("|")[1] };
                                        myData.push(pushItem);
                                    }

                                }
                            }
                            //var storeObject = {
                            //    data: myData
                            //};
                            var collapse = true;
                            if (opened.includes(accordion.items.getCount()))
                                collapse = false;
                            accordion.add({
                                cls: 'accordionpanel',
                                title: entry,
                                collapsed: collapse,
                                store: myData,
                                
                                //config: accordion.config
                            });
                            
                        });
                    }

                }


                accordion.setLoading(false);
            }
        });

    },

    onAfterRender: function (grid) {
        if (grid.config.showAdv == undefined || grid.config.showAdv == false) {
            var toolbar = grid.view.up().down("toolbar");
            var adv = toolbar.down("#addadv");
            if (adv) {
                adv.hide();
            }
        }

        try {
            var columnSelection = grid.headerCt.down("[isCheckerHd]");
            columnSelection.hide();
        } catch (err) {
            
            var m = err.message;
        }
        
    },

    onLoadData: function (grid) {
        var data = null;
        var currentGrid = grid;
        var url = CD.apiUrl + 'erda/admin/getconfigItems';
        var currentPanel = currentGrid.up().getItemId();
        var currentTab = currentGrid.up().up().getActiveTab().itemId;
        
        if (currentPanel != currentTab)
            currentGrid.viewConfig.loadMask = false;
        else
            currentGrid.setLoading(true);

        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: url,
            method: 'POST',
            async: false,
            jsonData: {
                target: currentGrid.config.masterItem,
                subItem: currentGrid.config.gridItem,
                filter: currentGrid.config.data,
            },
            withCredentials: true,
            failure: function (response) {
				CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                //var exceptionMessage = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);
                //var errorCode = exceptionMessage.ErrorCode;
                //var errorMessage = exceptionMessage.ErrorMessage;
                currentGrid.setLoading(false);
                //Ext.MessageBox.show({
                //    title: CDI.service.Translate.data['menuSection-etazebao-variants-title'],
                //    msg: errorMessage,
                //    icon: 'fa fa-times-circle fa-3x',
                //    buttons: Ext.Msg.OK
                //});
            },
            success: function (jsonData) {
                
                currentGrid.setEmptyText('<h4 style="margin-top:-2px;">' + currentGrid.config.emptytext + ' </h4>');
                var data = Ext.decode(jsonData.responseText);
                var storeObject = {
                    data: []
                };
                if (data != null && data.length > 0) {
                    var myArray = data[0].releatedItems[currentGrid.config.gridItem.toUpperCase()];
                    if (myArray.length > 0) {
                        for (var index = 0; index < myArray.length; index++) {
                            var pushItem = { item: myArray[index] };
                            storeObject.data.push(pushItem);

                        }
                        
                    }
                }
                currentGrid.setStore(Ext.create('Ext.data.Store', storeObject));
                
                if (currentPanel != currentTab)
                    currentGrid.viewConfig.loadMask = false;
                else
                    currentGrid.setLoading(false);
               

            }
        });

    },

    onGridRowSelected: function (grid, record) {

        var config = grid.view.up().config;
        
        //se la griglia è di progetto, devo ricaricare tutte le altre griglie della pagina
        if (config.role == "" && config.target == "projects") {
            
            var panelParent = grid.view.up().up().up();
            if (panelParent) {
                var grids = panelParent.query('rolegridadmin');
                if (grids != null && grids.length > 0) {
                    grids.forEach(function (grid) {
                        grid.config.target = record.data.item;
                        grid.config.competence = record.data.subitem;
                        grid.fireEvent('beforerender', grid);
                    });
                }
                var panelPR = panelParent.query("#pr_list");
                if (panelPR != null && panelPR.length > 0) {
                    var myPanel = panelPR[0];
                    var actualAcccordion = myPanel.items.items[0];
                    if (actualAcccordion) actualAcccordion.destroy();

                    var myGroup = Ext.create("CDI.view.components.rdaAdminCenter.roleManager.roleGroupingGrid", {
                        itemId: 'pr_list',
                        autoScroll: true,
                        style: {
                            'top': '-1px!important'
                        },
                        config: {
                            role: 'pr',
                            target: record.data.item,
                            refData: record.data,
                            competence: record.data.subitem
                        }
                    });
                    
                    //if (record.data.subitem.toUpperCase()=="VEHICLE")
                    //    myGroup.defaults.columns[0].hidden = true;
                    //else
                    //    myGroup.defaults.columns[0].hidden = false;
                    //myGroup.defaults.hideHeaders = false;
                    myPanel.add(myGroup);
                }
            }
        }
        else {

        }
        var hasSelection = grid.getSelected().length == 0 ? false : true;
        var currentView = grid.view.up().up().up();
        if (hasSelection && (grid.config.gridItem == null || grid.config.gridItem == "")) {
            var targetItemGrid = currentView.down("#targetItemGrid");
            var index = 0;
            while (true) {
                if (targetItemGrid) {
                    targetItemGrid.config.data = record.data.item;
                    targetItemGrid.fireEvent('loadData', targetItemGrid);
                }
                else if (targetItemGrid == null && index > 0)
                    break;

                index += 1;
                targetItemGrid = currentView.down("#targetItemGrid" + index);
            }
            
        }
        
    },

    fillEmptyStore:function(grid) {
        var myStoreProjectData = new Ext.data.Store({
            fields: [],
            data: []
        });
        grid.setEmptyText('<h4 style="margin-top:-2px;">' + grid.config.starttext + ' </h4>');
        grid.setStore(myStoreProjectData);

    },

    onButtonClick: function (btn) {
        var edit = ["cancel", "add", "delete"];
        var cancel = ["edit", "addadv", "refresh"];
        var add = [];
        var del = [];
        var save = ["edit", "addadv","refresh"];

        var myController = btn.up("grid").getController();
        if (myController == null)
            myController = btn.up("grid").up().getController();
        //disabilito pulsanti in accordo con la fase di edit
        var bar = btn.up();
        var buttons = bar.query("button");
        var itemToEnable = null;
        switch (btn.itemId) {
            case "refresh":
                var grid = btn.up("grid");
                if (grid.getXType() == "gridpanel" || grid.getXType() == "grid")
                    grid.up().fireEvent("beforeRender", grid.up());
                else
                    grid.fireEvent("beforeRender", grid);
                break;
            case "edit":
                
                var grid = btn.up("grid");
                this.onDisableOtherPanel(btn, false);
                itemToEnable = edit;
                this.onButtonRender(itemToEnable, buttons);
                break;
            case "delete":
                var grid = btn.up("grid");
                var selection = grid.getView().getSelectionModel().getSelection();
                grid.store.remove(selection);
                myController.onCheckSaveButton(grid);
                this.onButtonRender(itemToEnable, buttons);
                break;
            case "cancel":
                var me = btn.up("grid");
                itemToEnable = cancel;

                if (this.gridNeedToSync(me)) {
                   
                    var title = CDI.service.Translate.data["generic-error-warning-title"];
                    var message = CDI.service.Translate.data["common-question-pendingchange-abort-message"];

                    Ext.MessageBox.confirm(title, message,
                        function (res) {
                            if (res === 'yes') {
                                me.store.load();
                               
                                myController.onDisableOtherPanel(btn, true);

                                myController.onButtonRender(itemToEnable, buttons);
                                myController.onCheckSaveButton(me);
                            }
                        });
                }
                else {
                    this.onDisableOtherPanel(btn, true);
                    myController.onCheckSaveButton(me);
                    this.onButtonRender(itemToEnable, buttons);
                }
                break;
            case "add":
                var grid = btn.up('grid');
                var record = this.getGridModel(grid);
                grid.store.insert(0, record);
                grid.getView().getEl().scrollTo('Top', 0, true);
                grid.editingPlugin.startEditByPosition({ row: 0, column: 0 });
                myController.onCheckSaveButton(grid);
                this.onButtonRender(itemToEnable, buttons);
                break;
            case "save":
                var gridView = btn.up("grid").up();
                if (gridView != null && gridView.config.role == "pr") {
                    if (gridView.config.competence.toLowerCase() == "vehicle") {
                        if (btn.up("grid").store.queryRecords().length > 1) {
                            var title = CDI.service.Translate.data["generic-error-admin-editing-title"];
                            var message = CDI.service.Translate.data["generic-error-project-oneapprover"];
                            message = message.replace("@competence", gridView.config.competence);
                            CDI.service.erdaAction.showErrorMessage(title, message);
                            break;
                        }
                    }
                    else if (gridView.config.competence.toLowerCase() == "transversal") {
                        var uniqueTable = [];
                        var records = btn.up("grid").store.queryRecords();
                        if (records != null) {
                            var showError = false;
                            var title = CDI.service.Translate.data["generic-error-admin-editing-title"];
                            var message = CDI.service.Translate.data["generic-error-project-oneactivityapprover"];
                            for (var i = 0; i < records.length; i++) {
                                var key = records[i].data.item.toLowerCase();
                                if (uniqueTable.includes(key)) {
                                    message = message.replace("@competence", gridView.config.competence);
                                    message = message.replace("@activitynpi", records[i].data.item);
                                    showError = true;
                                    break;
                                }
                                uniqueTable.push(key);
                            }
                            if (showError) {
                                CDI.service.erdaAction.showErrorMessage(title, message);
                                break;
                            }
                        }

                        //if (btn.up("grid").store.queryRecords().length > 1) {
                        //    var title = CDI.service.Translate.data["generic-error-admin-editing-title"];
                        //    var message = CDI.service.Translate.data["generic-error-project-oneapprover"];
                        //    message = message.replace("@competence", gridView.config.competence);
                        //    CDI.service.erdaAction.showErrorMessage(title, message);
                        //    break;
                        //}
                    }
                }
                itemToEnable = save;
                myController.onSaveData(btn, itemToEnable, buttons);
                break;
            case "addadv":
                var gridView = btn.up("grid").up();
                var win = new Ext.Window({
                    title: CDI.service.Translate.data["multiuser-assignment-title"],
                    border: false,
                    closable: false,
                    draggable: false,
                    itemId: 'WfgWin',
                    width: '60vw',
                    height: '80vh',
                    closeAction: 'hide',
                    buttonAlign: 'center',
                    closable: false,
                    modal: true,
                    animShow: function () {
                        this.el.slideIn('t', {
                            duration: 1, callback: function () {
                                this.afterShow(true);
                            }, scope: this
                        });
                    },
                    animHide: function () {
                        this.el.disableShadow();
                        this.el.slideOut('t', {
                            duration: 1, callback: function () {
                                this.el.hide();
                                this.afterHide();
                            }, scope: this
                        });
                    },
                    layout: {
                        align: 'stretch',
                        type: 'vbox'
                    },
                    items: [
                        {
                            xtype: 'multiuserprojectassignment',
                            parentView: gridView,
                            flex: 1
                        }
                    ],


                });
                win.show(Ext.getBody());
                break;

               

        }
        //this.onButtonRender(itemToEnable, buttons);


    },

    onButtonRender: function (itemToEnable, buttons) {
        if (itemToEnable != null && itemToEnable.length > 0) {
            for (var x = 0; x < buttons.length; x++) {
                buttons[x].disable();
                for (var y = 0; y < itemToEnable.length; y++) {
                    if (buttons[x].itemId == itemToEnable[y]) {
                        buttons[x].enable();
                        break;
                    }
                }

            }
        }
    },

    onCheckSaveButton: function (grid) {
        
        var toolbar = grid.view.up().down("toolbar");
        var button = toolbar.query("#save");
        if (grid.config.allowEdit == false) {
            toolbar.query("#edit")[0].disable();
            toolbar.query("#save")[0].disable();
            toolbar.query("#add")[0].disable();
            toolbar.query("#delete")[0].disable();
            return;
        }

        if (button != null && button.length == 1) {
            button = button[0];
            button.disable();
            button.tooltip = CDI.service.Translate.data['rda-admincenter-savebtn-tooltip'];
            if (grid.store.getNewRecords() != null && grid.store.getNewRecords().length > 0) {
                button.enable();
                button.setTooltip(CDI.service.Translate.data['rda-admincenter-savebtn-pending-tooltip']);
            }

            if (grid.store.getRemovedRecords() != null && grid.store.getRemovedRecords().length > 0) {
                button.enable();
                button.setTooltip(CDI.service.Translate.data['rda-admincenter-savebtn-pending-tooltip']);
            }

        }
    },

    onDisableOtherPanel(btn, activate) {

        var grid = btn.up("grid");
        var columnSelection = grid.headerCt.down("[isCheckerHd]");
        if (grid.showCheckbox === undefined)
            grid.showCheckbox = true;
        grid.inEditMode = !activate;
        if (grid.showCheckbox == true) {
            if (activate)
                columnSelection.hide();
            else
                columnSelection.show();
        }

    },

    gridNeedToSync: function (grid) {

        if (grid.store.getNewRecords() != null && grid.store.getNewRecords().length > 0) {
            return true;
        }

        if (grid.store.getRemovedRecords() != null && grid.store.getRemovedRecords().length > 0) {
            return true;
        }
        return false;
    },

    getGridModel: function (grid) {
        var retValue = null;
        if (grid.getXType() == "wbsgrid") {
            retValue = new Ext.data.Record({
                COMPETENCE: '',
                CDC: '',
                AREA: '',
                SYSTEM: '',
                CBS: '',
                WBS: '',
            });
        }
        else {
            retValue = new Ext.data.Record({
                item: '',
            });
        }
        
        return retValue;
    },

    onGridBeforeEdit: function (editor, event, opts) {
        //var inEditMode = editor.grid.inEditMode;
        return event.record.phantom; //&& inEditMode;
    },

    onGridValidateEdit: function (editor, context) {
        if (context.value == '') {
            context.cancel = true;
            return false;
        }
        if (context.originalValue != context.value) {
            $(context.cell).children().addClass('erda-editing-cell');
        }
    },

    onSaveData: function (btn, itemToEnable, buttons) {
        var grid = btn.up("grid");
        var myController = btn.up("grid").getController();
        var updataData = {
            target: grid.config.target,
            role: grid.config.role,
            filter: grid.config.data,
            updateItems: [],
            deleteItems: []
        }
        if (grid.getXType() == "gridpanel"|| grid.getXType() == "grid") {
            updataData.target = grid.up().config.target;
            updataData.role = grid.up().config.role;
            updataData.subitem = grid.title;
            updataData.competence = grid.up().config.competence;
        }
        var url = "";
        if (grid.store.getNewRecords() != null && grid.store.getNewRecords().length > 0) {
            console.log("entro");
            console.log(grid.getXType());
            for (var i = 0; i < grid.store.getNewRecords().length; i++) {
                var data = grid.store.getNewRecords()[i].data;
                console.log(data);
                if (grid.getXType() == "rolegridadmin") {
                    console.log("entro 1");
                    updataData.updateItems.push(data.item);
                }
                else if (grid.getXType() == "gridpanel" || grid.getXType() == "grid" ) {
                    console.log("entro 2");
                    if (data.item != "")
                        updataData.updateItems.push(data.item + "|" + data.subitem);
                    else
                        updataData.updateItems.push(data.subitem);
                }
                
                
            }
        }
        
        if (grid.store.getRemovedRecords() != null && grid.store.getRemovedRecords().length > 0) {
            for (var i = 0; i < grid.store.getRemovedRecords().length; i++) {
                var data = grid.store.getRemovedRecords()[i].data;
                if (grid.getXType() == "rolegridadmin") {
                    updataData.deleteItems.push(data.item);
                }
                else if (grid.getXType() == "gridpanel" || grid.getXType() == "grid") {
                    if (data.item != "")
                        updataData.deleteItems.push(data.item + "|" + data.subitem);
                    else
                        updataData.deleteItems.push(data.subitem);
                }
            }

            
                
        }
        var title = CDI.service.Translate.data["generic-error-saving-title"];
        var message = CDI.service.Translate.data["common-question-save-pendingchanges-message"];

        Ext.MessageBox.confirm(title, message,
            function (res) {
                if (res === 'yes') {
                    grid.setLoading(true);
                    var url = CD.apiUrl + 'erda/admin/updateprojectuserrole';
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: url,
                        method: 'POST',
                        jsonData: updataData,
                        withCredentials: true,
                        failure: function (response) {
                            grid.setLoading(false);
                            CDI.service.erdaAction.showErrorMessage(title, "", response);
                        },
                        success: function (jsonData) {
                            if (grid.up()!=null && grid.up().getXType() == "rolegroupinggrid")
                                grid.up().fireEvent("beforeRender", grid.up());
                            else
                                grid.fireEvent("beforeRender", grid);
                            myController.onDisableOtherPanel(btn, true);
                            myController.onButtonRender(itemToEnable, buttons);
                            grid.setLoading(false);
                        }
                    });
                }

            });




    },

    onRoleFullViewRender: function (view) {
        var pagingtoolbar = view.down("pagingtoolbar");
        if (pagingtoolbar != null) {
            pagingtoolbar.down('#refresh').hide();
        }
        view.setLoading(true);
        var url = CD.apiUrl + 'erda/admin/getProjectRoleFullView';
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: url,
            method: 'GET',
            withCredentials: true,
            failure: function (response) {
                view.setLoading(false);
                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
            },
            success: function (jsonData) {
                var data = Ext.decode(jsonData.responseText);
                var storeObject = {
                    data: []
                };
                if (data != null && data.length > 0)
                    storeObject.data = data;
                view.setStore(Ext.create('Ext.data.Store', storeObject));
                view.setLoading(false);
            }
        });
    }
    
});