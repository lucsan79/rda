﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.roleManagerWrapperController', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.roleManagerWrapperController', function () {
    return {
        extend: 'Ext.app.ViewController',
        alias: 'controller.rolemanagerwrappercontroller',


        onChangeView: function (container, button, pressed) {
            var mainView = container.up("#rolemanager");
            var myOpSection = mainView.items.items[0];
            var actualView = myOpSection.items.items[0];

            if (pressed) {
                
                var panel = null;
                //var grids = Ext.ComponentQuery.query("rolegridadmin")
                //if (grids != null) {
                //for (var i = grids.length-1; i >= 0; i--) {
                //    grids[i].destroy();
                //    }
                //}
                //grids = Ext.ComponentQuery.query("rolegroupinggrid")
                //if (grids != null) {
                //    for (var i = grids.length - 1; i >= 0; i--) {
                //        grids[i].destroy();
                //    }
                //}
                //grids = Ext.ComponentQuery.query("rolegridadminwithoutbbar")
                //if (grids != null) {
                //    for (var i = grids.length - 1; i >= 0; i--) {
                //        grids[i].destroy();
                //    }
                //} 
                //grids = Ext.ComponentQuery.query("reqcontrollersview")
                //if (grids != null) {
                //    for (var i = grids.length - 1; i >= 0; i--) {
                //        grids[i].destroy();
                //    }
                //} 
                //grids = Ext.ComponentQuery.query("reqdriversview")
                //if (grids != null) {
                //    for (var i = grids.length - 1; i >= 0; i--) {
                //        grids[i].destroy();
                //    }
                //} 
                switch (button.itemId) {
                    case 'drivers':
                        panel = Ext.create('CDI.view.components.rdaAdminCenter.roleManager.reqDriversView', {
                            itemId: "reqdriversview",
                        });
                        break;
                    case 'controllers':
                      
                        panel = Ext.create('CDI.view.components.rdaAdminCenter.roleManager.reqControllersView', {
                            itemId: "reqcontrollersview",
                        });
                        break;

                    case 'readers':

                        panel = Ext.create('CDI.view.components.rdaAdminCenter.roleManager.reqReadersView', {
                            itemId: "reqreadersview",
                        });
                        break;
                    case 'fullview':
                        panel = Ext.create('CDI.view.components.rdaAdminCenter.roleManager.roleFullListView', {
                            itemId: "reqfullview",
                        });
                        break;
                }
                if (panel != null) {
                    
                    //var oldId = mainView.panelId;
                    //var old = Ext.getCmp(oldId);
                    //if (old != null) old.destroy();
                    //var grid = actualView.query("rolegridadmin");
                    //if (grid != null) {
                    //    for (var i = grid.length-1; i >= 0; i--) {
                    //        grid[i].destroy();
                    //    }
                    //}
                    //mainView.panelId = panel.id;
                    actualView.destroy();
                    myOpSection.add(panel);
                }
            }
            //alert("User toggled the '" + button.text + "' button: " + (pressed ? 'on' : 'off'));
        }
    }
});