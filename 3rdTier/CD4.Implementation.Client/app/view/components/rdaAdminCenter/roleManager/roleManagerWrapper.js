﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.roleManagerWrapper', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.roleManagerWrapper', function () {
    return {
        extend: 'Ext.panel.Panel',

        xtype: 'rolemanager',
        controller: 'rolemanagerwrappercontroller',
        itemId: 'rolemanager',
        title: false,
        cls: 'gray-window-title rdadashboard',
        layout: 'border',

        padding: 1,
        //cdIcon: 'list-alt',
       
        defaults: {
            collapsible: true,
            split: true,
            //border: true,
        },
        listeners: {
        },

        items: [
            {
                title: '<i class="fas fa-users-cog"></i> ' + CDI.service.Translate.data['rda-admincenter-rolemanagement'],
                flex: 3,
                region: 'center',
                collapsible: false,
                itemId: 'dashboardGridPanel',
                layout:'fit',
                header: {
                    style: {
                        'padding': '5px 10px 5px 15px',
                    },
                    items: [
                        {
                            xtype: 'segmentedbutton',
                            listeners: {
                                toggle: 'onChangeView'
                            },
                            allowToggle: true,
                            style: {
                                'margin-right': '10px'
                            },
                            defaults: {
                                cls: 'btn-toolbar-middle',
                            },
                            items: [
                                {
                                    iconCls: 'fas fa-code-branch',
                                    cls: 'btn-toolbar-left',
                                    itemId: 'drivers',
                                    text: CDI.service.Translate.data['rda-admincenter-rolemanagement-flow1'],
                                    pressed: true,
                                },
                                {
                                    iconCls:'fas fa-signature',
                                    cls: 'btn-toolbar-middle',
                                    itemId: 'controllers',
                                    text: CDI.service.Translate.data['rda-admincenter-rolemanagement-flow2'],
                                },
                                {
                                    iconCls: 'far fa-eye',
                                    cls: 'btn-toolbar-middle',
                                    itemId: 'readers',
                                    text: CDI.service.Translate.data['rda-admincenter-rolemanagement-flow3'],
                                },
                                {
                                    iconCls: 'far fa-list-alt',
                                    cls: 'btn-toolbar-right',
                                    itemId: 'fullview',
                                    text: CDI.service.Translate.data['rda-admincenter-rolemanagement-full'],
                                },
                                
                            ]
                        },
                    ]
                },
                items:
                    [
                        {
                            xtype: 'reqdriversview',
                            itemId: 'reqdriversview',
                        }
                    ]
            }
        ]
    }
});