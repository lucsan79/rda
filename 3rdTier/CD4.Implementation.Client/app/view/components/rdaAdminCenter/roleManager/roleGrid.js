﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.roleGrid', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.roleGrid', function () {
    return {
        extend: 'Ext.grid.Panel',
        controller: 'rolegridcontroller',
        //title: 'luca',
        xtype: 'rolegridadmin',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid colext',
        
        config: {
            role: '',
            header: '',
            starttext: '',
            showAdv:false,
            emptytext: ''

        },
        plugins: [
            'gridfilters',
            {
                ptype: 'cellediting',
                clicksToEdit: 1,
                listeners: {
                    beforeedit: 'onGridBeforeEdit',
                    validateedit: 'onGridValidateEdit',
                }
            }
        ],
        listeners: {
            beforerender: 'onBeforeRender',
            afterrender: 'onAfterRender',
        ////   'loadData': 'onLoadData',
        //   select: 'onGridRowSelected',
        },
        enableColumnHide: false,
        allowDeselect: false,
        selType: 'checkboxmodel',
        columns: [
            {
                text:'.',
                flex: 1,
                dataIndex: 'item',
                editor: { allowBlank: false },
                align: 'left',
                sortable: true,
                filter: true,
                listeners: {
                    render: function () {
                        this.setText(this.up("grid").config.header);
                    },
                },
                getEditor: function (record) {
                    var currentGrid = this.up("grid");
                    var combo = Ext.create('Ext.form.field.ComboBox', {
                        displayField: 'Value',
                        valueField: 'Value',
                        store: {
                            proxy: {
                                type: 'ajax',
                                withCredentials: true,
                                url: CD.apiUrl + 'erda/admin/getAvailableUsers',
                                extraParams: {
                                    target: this.up("grid").up().config.target,
                                },
                                autoload: false,
                                reader: {
                                    type: 'json'
                                }
                            }
                        },
                        minChars: 1,
                        //queryMode: 'remote',
                        allowBlank: false,
                        forceSelection: true,
                        filterPickList: false
                    });

                    return Ext.create('Ext.grid.CellEditor', {
                        field: combo
                    });


                },
            }
        ],
        bbar: [
            '->',
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-editbtn-tooltip'],
                iconCls: 'fas fa-pencil-alt',
                cls: 'pencil',
                itemId: 'edit',
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-reloadbtn-tooltip'],
                iconCls: 'fas fa-sync-alt',
                cls: 'sync',
                itemId: 'refresh',
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-cancelbtn-tooltip'],
                iconCls: 'fas fa-ban',
                cls: 'ban',
                itemId: 'cancel',
                disabled: true,
                handler: 'onButtonClick'

            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-addbtn-tooltip'],
                iconCls: 'fas fa-plus',
                cls: 'plus',
                itemId: 'add',
                disabled: true,
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-addadvbtn-tooltip'],
                iconCls: 'fas fa-user-plus',
                cls: 'plus',
                itemId: 'addadv',
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-deletebtn-tooltip'],
                iconCls: 'fas fa-trash-alt',
                cls: 'trash',
                itemId: 'delete',
                disabled: true,
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-savebtn-tooltip'],
                iconCls: 'fas fa-save',
                cls: 'save',
                itemId: 'save',
                disabled: true,
                handler: 'onButtonClick'
            }
        ],
    }
});