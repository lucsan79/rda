﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.reqReadersViewController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.reqreaderssviewcontroller',

        onBeforeRender: function (view) {
            
            var self = view;
            var gridProject = view.down('#projectList');
            //gridProject.setLoading(true);
            view.setLoading(true);
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/dashboard/getitems',
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    //var exceptionMessage = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);
                    ////var errorCode = exceptionMessage.ErrorCode;
                    //var errorMessage = "";
                    //try {
                    //    errorMessage = exceptionMessage.ErrorMessage;
                    //} catch (err) { errorMessage = exceptionMessage; }
                    //Ext.MessageBox.show({
                    //    title: CDI.service.Translate.data['generic-error-popup-title'],
                    //    msg: errorMessage,
                    //    icon: 'fa fa-times-circle fa-3x',
                    //    buttons: Ext.Msg.OK
                    //});
                    //gridProject.setLoading(false);
                    view.setLoading(false);
                }
            }).then(function (jsonData) {
                var data = Ext.decode(jsonData.responseText);
                var storeObject = { fields: [], data: [] };
                if (data != null) {
                    storeObject.data = data;
                }
                //gridProject.setLoading(false);
                
                var filterData = data.filterData;
                var projectStore = { fields: [], data: [] };
                if (data.projects != null) {
                    projectStore.data = data.projects;
                }
                view.down('#projectList').setStore(projectStore);
                view.setLoading(false);
            });
        },

        onAfterRender: function (view) {


        },
       
       
      

    });
