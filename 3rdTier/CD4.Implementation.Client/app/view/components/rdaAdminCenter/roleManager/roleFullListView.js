﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.roleFullListView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.roleFullListView', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'rolefullview',
        itemId: 'rolefullview',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls:'basicgrid',

        controller: 'rolegridcontroller',
        columnLines: true,
        emptyText: CDI.service.Translate.data["dashboard-rda-worklist-grid-emptyText"],
        scrollable: true,

        listeners: {
            beforerender:'onRoleFullViewRender'
        },

        viewConfig: {
            loadMask: true,
            listeners: {
                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        plugins: 'gridfilters',
        enableColumnHide: false,
        columns:
            [
                {
                    text: CDI.service.Translate.data["rda-admincenter-competence-label"],
                    dataIndex: 'COMPETENCE',
                    minWidth: 110,
                    cls: 'extend',
                    autoSizeColumn: true,
                    filter: true,
                    sortable: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-project"],
                    dataIndex: 'PROJECT',
                    width: 120,
                    autoSizeColumn: true,
                    filter: true,
                    sortable: true,
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-role"],
                    dataIndex: 'ROLE',
                    width: 120,
                    autoSizeColumn: true,
                    filter: true,
                    sortable: true,
                },
                {
                    text: CDI.service.Translate.data["rda-admincenter-activitynpi-label"],
                    dataIndex: 'ACTIVITYNPI',
                    width: 120,
                    autoSizeColumn: true,
                    filter: true,
                    sortable: true,
                },
                {
                    text: CDI.service.Translate.data["rda-admincenter-userlsit-label"],
                    dataIndex: 'USER',
                    minWidth: 700,
                    cls: 'multirowcol',
                    autoSizeColumn: true,
                    renderer: function (value, metaData) {
                        var cond = value.endsWith(",");
                        if (cond)
                            value = value.substring(0, value.length - 1);//value + ";";
                        var split = value.split(',');
                        if (split.length > 0) {
                            value = "";
                            for (i = 0; i < split.length; i++) {
                                value = value + split[i] + ",";
                                if (split[i + 1] != undefined && split[i + 1] != "")
                                    value = value + split[i + 1] + ",";
                                if ((i > 0 && i % 10 == 0) || i == split.length-1) 
                                    value = value + "<br/>";
                                i++;
                            }
                        }
                        return '<div class="multirowcol">' + value + '</div>';
                    },
                    sortable: true,
                    filter: true
                },
                {
                    text: ' ',
                    dataIndex: 'fake',
                    flex: 1,
                    sortable: false,
                    filter:false
                },

            ],


        bbar:
        [
            //{
            //xtype: 'pagingtoolbar',
            //pageSize: 50,
            //dock: 'bottom',
            //displayInfo: true,
            //emptyMsg: CDI.service.Translate.data["dashboard-rda-worklist-grid-pagingtoolbar-emptyMsg"],
            //items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("rolefullview").getController().onRoleFullViewRender(this.up("rolefullview"));
                        }
                    }
                },
                {
                    xtype: 'button',
                   
                    itemId: 'btnExport',
                    iconCls: 'fas fa-file-excel',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-label-export"],
                    tooltip: CDI.service.Translate.data["common-label-export-tooltip"],
                    //    tooltipType: 'title',
                    listeners: {
                        click: function () {
                            var options = {
                                title: CDI.service.Translate.data["rda-projectroleuserexport"]
                            }
                            CDI.service.erdaAction.showExportPanel("projectroleuser", options);
                        }
                    }
                },

            //]
        //}
        ]


    }
});

