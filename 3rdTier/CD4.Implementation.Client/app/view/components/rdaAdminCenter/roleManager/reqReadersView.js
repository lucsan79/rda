﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.reqReadersView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.reqReadersView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'reqreaderssviewcontroller',
        //controller: 'reqcontrollersviewcontroller',
        itemId: 'reqreaderssview',
        cls: 'rdahomeview',
        layout: 'border',
      
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {

                title: false,
                region: 'center',
                flex: 3,
                defaults: {
                    flex: 1,
                    margin: '0px 5px 0px 0px',
                    frame: true,
                    scrollable: true,
                    cls: 'roleadmin basicgrid',
                    layout: {
                        type: 'hbox',
                        pack: 'start',
                        align: 'stretch'
                    },

                },

                items: [
                    {
                        title: CDI.service.Translate.data["rda-admincenter-rolemanagement-role_ur"],
                        iconCls: 'fas fa-user-friends',
                        //itemId: 'planner_list',
                        flex: 1.1,
                        items: [
                            {
                                xtype: 'rolegroupinggrid',
                                itemId: 'ur',
                                itemId: 'ur_list',
                                config: {
                                    role: 'ur',
                                    target: 'RDA',
                                }
                            },
                        ]
                    },
                    {
                        cls: 'notopborder',
                        itemId: 'notopborder',
                        defaults: {
                            xtype: 'grid',
                            frame: true,
                            scrollable: true,
                        },
                        items: [
                            {
                                itemId: 'projectList',
                                xtype: 'rolegridadminwithoutbbar',
                                config: {
                                    role: '',
                                    target: 'projects',
                                    header: "<i class='fas fa-car'></i> " + CDI.service.Translate.data["dashboard-rda-projectlist-label"]
                                }
                            }
                        ]
                        
                    },
                    {
                        title: false,//CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pm"],
                        //iconCls: 'fas fa-user-shield',
                        itemId: 'pm_list',
                        items: [
                            {
                                xtype: 'rolegridadmin',
                                itemId: 'pm',
                                config: {
                                    role: 'pm',
                                    target: '',
                                    header: "<i class='fas fa-user-shield'></i> " + CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pm"],
                                    starttext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'],
                                    showAdv:true,
                                }
                            },
                        ]

                    },
                ]

            }
        ]




    }
});