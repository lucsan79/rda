﻿Ext.define('CDI.view.components.rdaAdminCenter.roleManager.reqControllersView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.roleManager.reqControllersView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'reqcontrollersview',
        //controller: 'reqcontrollersviewcontroller',
        itemId: 'reqcontrollersview',
        cls: 'rdahomeview',
        layout: 'border',
      
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            
            {
                split: true,
                title:false,// CDI.service.Translate.data["dashboard-rda-projectlist-label"],
                region: 'west',
                flex: 1,
                minWidth: 400,
                cls:'notopborder',
                itemId: 'notopborder',
                defaults: {
                    flex: 1,
                    xtype: 'grid',
                    frame: true,
                    scrollable: true,
                },

                items: [
                    {
                        split: true,
                        itemId: 'projectList',
                        xtype:'rolegridadminwithoutbbar',
                        config: {
                            role: '',
                            target: 'projects',
                            header: "<i class='fas fa-car'></i> " + CDI.service.Translate.data["dashboard-rda-projectlist-label"]
                        }
                    },
                ]
            },
            {

                title: false,
                region: 'center',
                flex: 3,
                defaults: {
                    flex: 1,
                    margin: '0px 5px 0px 0px',
                    frame: true,
                    scrollable: true,
                    cls: 'roleadmin basicgrid',
                    layout: {
                        type: 'hbox',
                        pack: 'start',
                        align: 'stretch'
                    },

                },

                items: [
                    {
                        title: CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pr"],
                        iconCls: 'fas fa-user-edit',
                        itemId: 'pr_list',
                        flex: 1.3,
                        items: [
                            {
                                html: '<div class="x-grid-empty"><h4 style="margin-top:-2px;">' + CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'] + ' </h4></div>',
                            },
                        ]
                    },
                    {
                        title: false,
                        //iconCls: 'fas fa-comments-dollar',
                        itemId: 'cms_list',
                        items: [
                            {
                                xtype: 'rolegridadmin',
                                itemid:'cms',
                                config: {
                                    role: 'cms',
                                    target: '',
                                    header: "<i class='fas fa-comments-dollar'></i> " + CDI.service.Translate.data["rda-admincenter-rolemanagement-role_cms"],
                                    starttext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'],
                                }
                            },
                        ]

                    },
                    //{
                    //    title: false,//CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pm"],
                    //    //iconCls: 'fas fa-user-shield',
                    //    itemId: 'pm_list',
                    //    items: [
                    //        {
                    //            xtype: 'rolegridadmin',
                    //            itemid: 'pm',
                    //            config: {
                    //                role: 'pm',
                    //                target: '',
                    //                header: "<i class='fas fa-user-shield'></i> " + CDI.service.Translate.data["rda-admincenter-rolemanagement-role_pm"],
                    //                starttext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'],
                    //            }
                    //        },
                    //    ]

                    //},
                ]

            }
        ]




    }
});