﻿Ext.define('CD.view.components.applicationSection.adminCenter.tools.ToolsWrapper', {});
Ext.deferDefine('CD.view.components.applicationSection.adminCenter.tools.ToolsWrapper', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'adminotoolswrapper',
        controller: 'adminotoolswrapper',
        layout: 'fit',
        cls: 'admin-wrapper-container',
        items: [
            {
                xtype: 'tabpanel',
                width: '100%',
                layout: 'fit',
                ui: 'navigation',
                tabPosition: 'left',
                tabRotation: 0,
                tabBar: {
                    border: false,
                },
                plain: true,
                minTabWidth: 150,
                bodyBorder: true,
                cls: 'x-subtreelist-navigation  domain-navigation subnavigation-border-right subnavigation-items',
                bodyStyle: {
                    background: '#fff',
                    'padding-left': '5px',
                    'padding-right': '1px',
                    'border-left': 'solid',
                    'border-left-color': '#c5c5c5',
                    'border-left-width': '1px'
                },
                defaults: {
                    textAlign: 'left',
                    layout: 'fit',
                    cls: 'admin-content-panel',
                    style: {
                        'padding-bottom': '0px'
                    }
                },

                listeners: {
                    tabchange: 'onTabChange',
                    afterrender: 'onTabPanelAfterRender'
                },
                items: [
                    {
                        title: '<i class="fa fa-retweet"></i> ' + CD.service.Translate.data['admin-tools-sync-code'],
                        items: {
                            xtype: 'admintoolsynccode'
                        }
                    },
                    {
                        title: '<i class="fa fa-address-card"></i> ' + CD.service.Translate.data['admin-tools-ftr-code'],
                        items: {
                            xtype: 'admintoolsftrview',
                            
                        }
                    }
                ]
            }
        ]
    }
});