﻿Ext.define('CD.view.components.applicationSection.adminCenter.tools.syncCode.SyncView', {});
Ext.deferDefine('CD.view.components.applicationSection.adminCenter.tools.syncCode.SyncView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'admintoolsynccode',
        controller: 'admintoolsynccode',
        itemId: 'syncPanel',
        title: '<i class="fa fa-retweet"></i> ' + CD.service.Translate.data['admin-tools-sync-code'],
        frame: true,
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        flex: 1,
        bodyPadding: 1,
        scrollable: true,
        autoScroll: true,
        listeners: {
            expand: function (view) {
                view.updateLayout();
            },
            beforerender: 'onBeforeRender',
        },
        items: [

            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    pack: 'start',
                    align: 'stretch'
                },
                flex: 1,
                scrollable: false,
                autoScroll: false,
                defaults:{
                    viewConfig: {
                        //preserveScrollOnRefresh: true,
                        deferEmptyText         : true,
                        emptyText: '<div class="grid-data-empty"><div data-icon="/" class="empty-grid-icon"></div><div class="empty-grid-headline"><b>' + 'No SyncEntry(s) found!' + '</b></div></div>'
                    },
                },
                items: [
                    {
                        xtype: 'label',
                        text: 'SyncEntry queue',
                        style: {
                            'padding-top': '5px'
                        }
                    },
                    {
                        xtype: 'grid',
                        itemId: 'syncGrid',
                        maxHeight: 250,
                        //minHeight: 100,
                        multiSelect: true,
                        selModel: {
                            type: 'checkboxmodel',
                            checkOnly: true
                        },
                        plugins: [
                               'gridfilters',
                        ],

                        listeners: {
                            selectionchange: 'onSelectionChange'
                        },
                        columns: [
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-filename'],
                                flex: 1,
                                minWidth: 200,
                                dataIndex: 'FileName',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-synctype'],
                                flex: 1,
                                minWidth: 200,
                                dataIndex: 'SyncType',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-sourcevault'],
                                flex: 1,
                                minWidth: 200,
                                dataIndex: 'SourceVault',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-targetvault'],
                                flex: 1,
                                minWidth: 200,
                                dataIndex: 'TargetVault',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-createdate'],
                                flex: 1,
                                minWidth: 200,
                                dataIndex: 'CreateDate',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-createuser'],
                                flex: 1,
                                minWidth: 200,
                                dataIndex: 'CreateUser',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-returncode'],
                                flex: 1,
                                minWidth: 200,
                                dataIndex: 'ReturnCode',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-returnmessage'],
                                flex: 1,
                                minWidth: 200,
                                dataIndex: 'ReturnMessage',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },

                        ]
                    },
                    {
                        xtype: 'label',
                        text: 'Failed',
                        style: {
                            'padding-top':'10px'
                        }

                    },
                    {
                        xtype: 'grid',
                        itemId: 'failedGrid',
                        maxHeight: 250,
                      
                        plugins: [
                               'gridfilters',
                        ],
                        
                        columns: [
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-filename'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'FileName',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-synctype'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'SyncType',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-sourcevault'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'SourceVault',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-targetvault'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'TargetVault',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-createdate'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'CreateDate',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-createuser'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'CreateUser',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-lastsynccode'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'LastSyncCode',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-returncode'],
                                flex: 1,
                                minWidth: 100,
                                dataIndex: 'ReturnCode',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-returnmessage'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'ReturnMessage',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },

                        ]
                    },
                    {
                        xtype: 'label',
                        text: 'Pending',
                        style: {
                            'padding-top': '20px'
                        }
                    },
                    {
                        xtype: 'grid',
                        itemId: 'pendingGrid',
                        plugins: [
                               'gridfilters',
                        ],
                        maxHeight: 250,
                        //minHeight: 100,
                        columns: [
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-filename'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'FileName',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-synctype'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'SyncType',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-sourcevault'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'SourceVault',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-targetvault'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'TargetVault',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-createdate'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'CreateDate',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-createuser'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'CreateUser',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-lastsynccode'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'LastSyncCode',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-returncode'],
                                flex: 1,
                                minWidth: 100,
                                dataIndex: 'ReturnCode',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },
                            {
                                text: CD.service.Translate.data['admin-tools-sync-code-returnmessage'],
                                flex: 1,
                                minWidth: 190,
                                dataIndex: 'ReturnMessage',
                                sortable: true,
                                align: 'center',
                                filter: {
                                    type: 'string',
                                },

                            },

                        ]
                    }

                ]
            
            }
        ],
        tbar: {
            overflowHandler: 'menu',

            items: [{
                xtype: 'splitbutton',
                text: CD.service.Translate.data['admin-button-actions'],
                listeners: {
                    ArrowClick: 'onArrowClick'
                },
                cls: 'btn-info',
                iconCls: 'x-fa fa-bars',
                menu: {
                    items: [
                        {
                            text: CD.service.Translate.data['admin-tools-sync-code-sync'],
                            handler: 'sync',
                            iconCls: 'x-fa fa-retweet',
                           
                        },
                        {
                            text: CD.service.Translate.data['admin-button-reload'],
                            handler: 'reload',
                            iconCls: 'x-fa fa-refresh',
                        }
                    ]
                }
            }]
        },
        fbar: [{
            minWidth: 80,
            text: CD.service.Translate.data['admin-tools-sync-code-sync'],
            handler: 'sync',
            cls: 'btn-success',
            iconCls: 'fa fa-retweet',
            itemId: 'syncButton',
            disabled: true
        }]
    }
});