﻿Ext.define('CD.view.components.applicationSection.adminCenter.tools.syncCode.SyncViewController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.admintoolsynccode',

    onBeforeRender: function (view) {
       
        view.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'GET',
            withCredentials: true,
            url: CD.apiUrl + 'tools/synccode/getdata',
            success: function (jsonData) {
                var data = Ext.decode(jsonData.responseText);
                var createdGridData = data.CreatedSyncCode;
                var failedGridData = data.FailedSyncCode;
                var pendingGridData = data.PendingSyncCode;
                
                var createdGrid = view.down('#syncGrid');
                var failedGrid = view.down('#failedGrid');
                var pendingGrid = view.down('#pendingGrid');

                var createdGridStore = {
                    data: createdGridData
                };
                var failedGridStore = {
                    data: failedGridData
                };
                var pendingGridStore = {
                    data: pendingGridData
                };

                createdGrid.setStore(createdGridStore);
                failedGrid.setStore(failedGridStore);
                pendingGrid.setStore(pendingGridStore);

                view.setLoading(false);

            },
            failure: function (response) {
				CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                //var exceptionMessage = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);
                //var errorCode = exceptionMessage.ErrorCode;
                //var errorMessage = exceptionMessage.ErrorMessage;
                //Ext.MessageBox.show({
                //    title: 'Error: ' + errorCode,
                //    msg: errorMessage,
                //    icon: 'fa fa-times-circle fa-3x',
                //    buttons: Ext.Msg.OK
                //});
            }
        });

    },

    sync: function(view) {
        var syncView = view.up('admintoolsynccode');
        var grid = syncView.down('#syncGrid');
        var selectedData = [];
        var selection = grid.getSelection();
        for (index in selection) {
            var newObj = {
                Id: selection[index].data.Id,
                BusinessDataFileId: selection[index].data.BusinessDataFileId,
                FileName: selection[index].data.BusinessDataFileId,
                SourceVault: selection[index].data.SourceVault,
                TargetVault: selection[index].data.TargetVault,
                SyncType: selection[index].data.SyncType,
                ReturnCode: selection[index].data.TargetVault,
                CreateDate: selection[index].data.CreateDate,
                ReturnMessage: selection[index].data.ReturnMessage,
                LastSyncDate: selection[index].data.LastSyncDate
            };
            selectedData.push(newObj);
        }

        syncView.setLoading(true);

        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            withCredentials: true,
            url: CD.apiUrl + 'tools/synccode/sync',
            jsonData:  selectedData,
            success: function (response) {
                Ext.toast({
                    html: '<i class="fa fa-check"></i> ' + CD.service.Translate.data['admin-tools-sync-toast'],
                    bodyStyle: 'background-color: green; color: white',
                    align: 'tr',
                    anchor: 'contentPanel'
                });
                syncView.fireEvent('beforerender', syncView);
               
            },
            failure: function (response) {
				CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                //var exceptionMessage = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);
                //var errorCode = exceptionMessage.ErrorCode;
                //var errorMessage = exceptionMessage.ErrorMessage;
                //Ext.MessageBox.show({
                //    title: 'Error: ' + errorCode,
                //    msg: errorMessage,
                //    icon: 'fa fa-times-circle fa-3x',
                //    buttons: Ext.Msg.OK
                //});
            }
        });
    },

    reload: function (view) {
        var syncView = view.up('admintoolsynccode');
        syncView.fireEvent('beforerender', syncView);
    },

    onArrowClick: function (view) {
        var grid = this.getGrid(view);
        var menuItems = view.menu.items.items;
        menuItems[0].setDisabled(!grid.getSelectionModel().hasSelection());
    },

    getGrid: function (view) {
        var panel = view.up('admintoolsynccode');
        var grid = panel.down('#syncGrid');
        return grid;
    },

    onSelectionChange: function (table, selected, eOpts)
    {
        if (table.view != null)
        {
            var syncView = table.view.up('admintoolsynccode');
            var buttonSync = syncView.down('#syncButton');
            var grid = syncView.down('#syncGrid');
            buttonSync.setDisabled(!grid.getSelectionModel().hasSelection());
        }
        
    }

});