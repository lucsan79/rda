﻿Ext.define('CD.view.components.applicationSection.adminCenter.tools.ftr.FTRViewController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.admintoolsftr',
    onBeforeRender: function (view) {

        var self = this;

        var gridContainer = view.down('#ftrGridContainer')
        var grid = Ext.create('Ext.grid.Panel',
            {
                itemId: 'ftrGrid',
                plugins: [
                    'gridfilters'
                ],
                layout: 'fit',
                scrollable: true,
                height: '98%',
                width: '100%',
                renderTo: Ext.getBody(),

                columns: [
                    //hidden fields
                   { text: 'Id', dataIndex: 'Id', sortable: true, align: 'center', hidden: true },
                   { text: 'BusinessDataFileId', dataIndex: 'BusinessDataFileId', sortable: true, align: 'center', hidden: true },
                   { text: 'BusinessDataId', dataIndex: 'BusinessDataId', sortable: true, align: 'center', hidden: true },

                   //shown fields
                   {
                       text: CD.service.Translate.data['admin-tools-ftr-filename'],
                       flex: 1,
                       dataIndex: 'BusinessDataFileName',
                       align: 'left',
                       sortable: true,
                       filter: true
                   },
                   {
                       text: CD.service.Translate.data['admin-tools-ftr-businessdata'],
                       flex: 1,
                       dataIndex: 'BusinessDataName',
                       align: 'left',
                       sortable: true,
                       filter: true
                   },
                   {
                       text: CD.service.Translate.data['admin-tools-ftr-createdate'],
                       flex: 1,
                       dataIndex: 'CreateDate',
                       align: 'left',
                       sortable: true,
                       filter: true
                   },
                   {
                       text: CD.service.Translate.data['admin-tools-ftr-returncode'],
                       flex: 1,
                       dataIndex: 'ReturnCode',
                       align: 'left',
                       sortable: true,
                       filter: true
                   },
                   {
                       text: CD.service.Translate.data['admin-tools-ftr-returnmsg'],
                       flex: 1,
                       dataIndex: 'ReturnMessage',
                       align: 'left',
                       sortable: true,
                       filter: true
                   },
                   {
                       text: CD.service.Translate.data['admin-tools-ftr-lastprocessdate'],
                       flex: 1,
                       dataIndex: 'LastProcessDate',
                       align: 'left',
                       sortable: true,
                       filter: true
                   }
                ],
                store: {
                    groupField: 'Group',
                    proxy: {
                        type: 'ajax',
                        url: CD.apiUrl + 'admincenter/tools/getftr',
                        withCredentials: true,
                        reader: {
                            type: 'json',
                            rootProperty: ''
                        }
                    },
                    autoLoad: true

                },
                tbar: [{
                    minWidth: 80,
                    text: CD.service.Translate.data['common-all-reload'],
                    cls: 'btn-info',
                    iconCls: 'x-fa fa-refresh',
                    handler: 'onReloadData'
                }]
            });

        gridContainer.add(grid);
        gridContainer.updateLayout();


    },
    onReloadData: function (view) {
        var grid = view.up('#ftrGrid');
        grid.store.reload();
    }
});