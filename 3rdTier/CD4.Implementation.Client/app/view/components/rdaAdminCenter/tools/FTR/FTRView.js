﻿Ext.define('CD.view.components.applicationSection.adminCenter.tools.ftr.FTRView', {});
Ext.deferDefine('CD.view.components.applicationSection.adminCenter.tools.ftr.FTRView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'admintoolsftrview',
        controller: 'admintoolsftr',
	    itemId: 'ftrpanel',
	    title: '<i class="fa fa-cog"></i> ' + CD.service.Translate.data['admin-tools-ftr-entryqueue'],
        frame: true,
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        flex: 1,
        bodyPadding: 1,
        listeners: {
            expand: function (view) {
                view.updateLayout();
            },
            beforerender: 'onBeforeRender',
        },
        items: [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'start',
                    align: 'stretch'
                },
                flex: 1,
                scrollable: false,
                autoScroll: false,
                itemId: 'ftrGridContainer',
            }
        ]
    }
});