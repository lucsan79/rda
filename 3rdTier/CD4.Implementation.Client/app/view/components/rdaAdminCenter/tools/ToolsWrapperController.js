﻿Ext.define('CD.view.components.applicationSection.adminCenter.tools.ToolsWrapperController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.adminotoolswrapper',

    onTabChange: function (view) {

        var activeTab = view.getActiveTab();
        var tabContent = activeTab.down();

        var xtype = tabContent.xtype
        tabContent.destroy();

        var newTabContent = Ext.create({
            xtype: xtype
        });

        activeTab.add(newTabContent);
    },

    onTabPanelAfterRender: function (tabpanel) { 
        var inviteUsersTabItem = tabpanel.tabBar.items.items[1];
    }

});