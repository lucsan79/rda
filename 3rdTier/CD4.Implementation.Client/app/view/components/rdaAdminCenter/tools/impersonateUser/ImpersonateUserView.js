﻿Ext.define('CD.view.components.applicationSection.adminCenter.tools.impersonateUser.ImpersonateUserView', {});
Ext.deferDefine('CD.view.components.applicationSection.adminCenter.tools.impersonateUser.ImpersonateUserView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'admintoolsimpersonateuserview',
        controller: 'admintoolsimpersonatecontroller',
	    itemId: 'impersonateuserpanel',
	    title: '<i class="fa fa-user"></i> ' + CD.service.Translate.data['admin-tools-impersonate'],
        frame: true,
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        flex: 1,
        bodyPadding: 1,
        listeners: {
            expand: function (view) {
                view.updateLayout();
            },
            beforerender: 'onBeforeRender',
        },
        items: [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'start',
                    align: 'stretch'
                },
                flex: 1,
                scrollable: false,
                autoScroll: false,
                itemId: 'impersonateUserGridContainer',
            }
        ]
    }
});