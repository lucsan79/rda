﻿Ext.define('CD.view.components.applicationSection.adminCenter.tools.impersonateUser.ImpersonateUserViewController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.admintoolsimpersonatecontroller',
    onBeforeRender: function (view) {

        var self = this;

        var gridContainer = view.down('#impersonateUserGridContainer')
        var grid = Ext.create('Ext.grid.Panel',
            {
                itemId: 'impersonateUserGrid',
                selModel: Ext.create('Ext.selection.CheckboxModel', {
                    checkOnly: true
                }),
                plugins: [
                    'gridfilters'
                ],
                layout: 'fit',
                scrollable: true,
                height: '98%',
                width: '100%',
                multiSelect: false,

                listeners: {
                    itemcontextmenu: function (view, record, item, i, e) {
                        e.preventDefault();		// stop the normal browser contextmenu

                        var menu = new Ext.menu.Menu({
                            style: {
                                padding: '3px'
                            },
                            items: [{
                                text: CD.service.Translate.data['admin-tools-impersonate-button'] + ': ' + record.data.Username,
                                handler: function (record) {
                                    self.impersonateUser(record);
                                }
                            }]
                        });


                        menu.showAt(e.getXY());
                    }
                },

                columns: [
                  {
                      text: CD.service.Translate.data['common-username'],
                      width: 200,
                      minWidth: 150,
                      dataIndex: 'Username',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      text: CD.service.Translate.data['common-admin-column-description'],
                      width: 200,
                      minWidth: 150,
                      dataIndex: 'Description',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      text: CD.service.Translate.data['admin-organization-manageusers-firstname'],
                      flex: 1,
                      minWidth: 200,
                      minWidth: 150,
                      dataIndex: 'FirstName',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      text: CD.service.Translate.data['admin-organization-manageusers-lastname'],
                      flex: 1,
                      minWidth: 200,
                      dataIndex: 'LastName',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      text: CD.service.Translate.data['common-email'],
                      width: 200,
                      minWidth: 150,
                      dataIndex: 'Email',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      text: CD.service.Translate.data['common-telephone'],
                      width: 200,
                      minWidth: 150,
                      dataIndex: 'Telephone',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      text: CD.service.Translate.data['admin-organization-manageusers-defaultvault'],
                      dataIndex: 'DefaultVault',
                      width: 200,
                      minWidth: 150,
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      xtype: 'checkcolumn',
                      text: CD.service.Translate.data['admin-organization-manageusers-status'],
                      width: 100,
                      minWidth: 50,
                      sortable: true,
                      disabled: true,
                      align: 'center',
                      dataIndex: 'Status',
                      renderer: function (value, metadata, record) {
                          if (value == "ENABLED" || value == true) {
                              metadata.style = "color:#367fa9;font-size:20px";
                              return "<span class='x-fa fa-toggle-on'></span>";
                          }
                          else {
                              metadata.style = "color:gray;font-size:20px";
                              return "<span class='x-fa fa-toggle-off'></span>";
                          }

                      },
                  },
                  {
                      text: CD.service.Translate.data['admin-organization-manageusers-authenticationtype'],
                      width: 150,
                      minWidth: 100,
                      dataIndex: 'AuthenticationType',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      text: CD.service.Translate.data['admin-organization-manageusers-authenticationid'],
                      width: 150,
                      minWidth: 100,
                      dataIndex: 'AuthenticationID',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },
                  {
                      text: CD.service.Translate.data['admin-organization-manageusers-isdba'],
                      width: 100,
                      minWidth: 80,
                      dataIndex: 'IsDba',
                      sortable: true,
                      align: 'center',
                      filter: {
                          type: 'string',
                      }
                  },

                  /*{ text: CD.service.Translate.data['admin-organization-manageusers-projects'], flex: 1, minWidth: 200, dataIndex: 'Projects', sortable: true, align: 'center' },
                  { text: CD.service.Translate.data['admin-organization-manageusers-groups'], flex: 1, minWidth: 200, dataIndex: 'Groups', sortable: true, align: 'center' },
                  { text: CD.service.Translate.data['admin-organization-manageusers-roles'], flex: 1, minWidth: 200, dataIndex: 'Roles', sortable: true, align: 'center' }, */

                ],
                store: {
                    groupField: 'Group',
                    proxy: {
                        type: 'ajax',
                        url: CD.apiUrl + 'organization/manageusers/getall',
                        withCredentials: true,
                        reader: {
                            type: 'json',
                            rootProperty: ''
                        }
                    },
                    autoLoad: true

                },
                tbar: {
                    overflowHandler: 'menu',

                    items: [{
                        xtype: 'splitbutton',
                        cls: 'three-dots',
                        iconCls: 'x-fa fa-wrench',
                        itemId: 'UserTools',
                        text: CD.service.Translate.data['common-user-tools'],
                        cls: 'btn-info',
                        scale: 'small',
                        iconAlign: 'left',
                        listeners: {
                            ArrowClick: 'onArrowClick'
                        },
                        menu: [
                            {
                                text: CD.service.Translate.data['admin-tools-impersonate-button'],
                                handler: 'onImpersonateUser',
                                iconCls: 'x-fa fa-user'
                            },
                            {
                                text: CD.service.Translate.data['admin-button-reload'],
                                handler: 'onReloadData',
                                iconCls: 'x-fa fa-refresh'
                            },
                        ]
                    }]
                }
            });

        gridContainer.add(grid);
        gridContainer.updateLayout();


    },

    impersonateUser: function(record){
    },

    onReloadData: function (view) {
        var grid = view.up('#impersonateUserGrid');
        grid.store.reload();
    },

    onImpersonateUser: function (view) {
        var selectedData = this.getCheckedRowData(view);
        this.impersonateUser(selectedData);
    },

    getCheckedRowData: function (view) {
        var panel = view.up('#impersonateuserpanel');
        var grid = panel.down('#impersonateUserGrid');

        var selectedIds = [];

        var selection = grid.getSelection();
        if (selection[0] == null)
            return null;

        return selection[0].data;
    },

    onArrowClick: function (view) {

        var selectedData = this.getCheckedRowData(view);

        var menuItems = view.menu.items.items;

        if (selectedData != null) {
            menuItems[0].setDisabled(false);
        }
        else {
            menuItems[0].setDisabled(true);
        }
    },


});