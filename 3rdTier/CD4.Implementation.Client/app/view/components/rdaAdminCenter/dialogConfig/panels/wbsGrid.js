﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogConfig.panels.wbsGrid', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogConfig.panels.wbsGrid', function () {
    return {
        extend: 'Ext.grid.Panel',
        controller: 'adminconfigcontroller',
        title: false,
        xtype: 'wbsgrid',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid',
        config: {
            competence: '',

        },
        plugins: [
            'gridfilters',
            {
                ptype: 'cellediting',
                clicksToEdit: 1,
                listeners: {
                    beforeedit: 'onGridBeforeEdit',
                    validateedit: 'onGridValidateEdit',
                }
            }
        ],
        listeners: {
            afterrender: 'onAfterRender',
            'loadData': 'onWBSLoadData',
        },
        enableColumnHide: false,

        viewConfig: {
            listeners: {
                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        allowDeselect: false,
        selType: 'checkboxmodel',
        
        columns: [
            {
                text: CDI.service.Translate.data["rda-admincenter-competence-label"],
                dataIndex: 'COMPETENCE',
                minWidth: 120,
                autoSizeColumn: true,
                filter: 'list',
            },
            {
                text: CDI.service.Translate.data["rda-admincenter-cdc-label"],
                dataIndex: 'CDC',
                minWidth: 120,
                autoSizeColumn: true,
                filter: 'list',
                getEditor: function (record) {
                    var filterValue = "";
                    var currentGrid = this.up("grid");
                    var forceSelection = false;
                    if (currentGrid.config.masterItem == "project") {
                        forceSelection = true;
                        filterValue = currentGrid.config.extraparam;
                    }
                        
                    var combo = null;
                    if (currentGrid.config.data != null && currentGrid.config.data == "POWERTRAIN") {
                        combo = Ext.create('Ext.form.field.ComboBox', {
                            disabled: true
                        });
                    }
                    else if (currentGrid.config.extraparam != null && currentGrid.config.extraparam == "POWERTRAIN") {
                        combo = Ext.create('Ext.form.field.ComboBox', {
                            disabled: true
                        });
                    }
                    else {
                        combo = Ext.create('Ext.form.field.ComboBox', {
                            displayField: 'Value',
                            valueField: 'Value',
                            store: {
                                proxy: {
                                    type: 'ajax',
                                    withCredentials: true,
                                    url: CD.apiUrl + 'erda/combobox/wbs/distinct',
                                    extraParams: {
                                        target: 'CDC',
                                        path: filterValue
                                    },
                                    autoload: false,
                                    reader: {
                                        type: 'json'
                                    }
                                }
                            },
                            minChars: 1,
                            //queryMode: 'remote',
                            allowBlank: false,
                            forceSelection: forceSelection,
                            filterPickList: false
                        });
                    }
                    return Ext.create('Ext.grid.CellEditor', {
                        field: combo
                    });
                 }
                    
                   
                //},
                
                
            },
            {
                text: CDI.service.Translate.data["rda-admincenter-area-label"],
                dataIndex: 'AREA',
                minWidth: 120,
                autoSizeColumn: true,
                filter: 'list',
                getEditor: function (record) {
                    var filterValue = "";
                    var currentGrid = this.up("grid");
                    var forceSelection = false;
                    if (currentGrid.config.masterItem == "project") {
                        forceSelection = true;
                        filterValue = currentGrid.config.extraparam + "\\" + record.data.CDC;
                    }
                    var combo = null;
                    
                    combo = Ext.create('Ext.form.field.ComboBox', {
                            displayField: 'Value',
                            valueField: 'Value',
                            store: {
                                proxy: {
                                    type: 'ajax',
                                    withCredentials: true,
                                    url: CD.apiUrl + 'erda/combobox/wbs/distinct',
                                    extraParams: {
                                        target: 'AREA',
                                        path: filterValue
                                    },
                                    autoload: false,
                                    reader: {
                                        type: 'json'
                                    }
                                }
                            },
                            minChars: 1,
                            //queryMode: 'remote',
                            allowBlank: false,
                            forceSelection: forceSelection,
                            filterPickList: false
                        });
                   
                    return Ext.create('Ext.grid.CellEditor', {
                        field: combo
                    });
                }

                
            },
            {
                text: CDI.service.Translate.data["rda-admincenter-system-label"],
                dataIndex: 'SYSTEM',
                minWidth: 120,
                autoSizeColumn: true,
                filter: 'list',
                getEditor: function (record) {
                    var filterValue = "";
                    var currentGrid = this.up("grid");
                    var forceSelection = false;
                    if (currentGrid.config.masterItem == "project") {
                        forceSelection = true;
                        filterValue = currentGrid.config.extraparam + "\\" + record.data.CDC + "\\" + record.data.AREA;
                    }
                    
                    var combo = null;
                    if (currentGrid.config.data != null && currentGrid.config.data == "TRANSVERSAL") {
                        combo = Ext.create('Ext.form.field.ComboBox', {
                            disabled: true
                        });
                    }
                    else if (currentGrid.config.extraparam != null && currentGrid.config.extraparam == "TRANSVERSAL") {
                        combo = Ext.create('Ext.form.field.ComboBox', {
                            disabled: true
                        });
                    }
                    else {
                        combo = Ext.create('Ext.form.field.ComboBox', {
                            displayField: 'Value',
                            valueField: 'Value',
                            store: {
                                proxy: {
                                    type: 'ajax',
                                    withCredentials: true,
                                    url: CD.apiUrl + 'erda/combobox/wbs/distinct',
                                    extraParams: {
                                        target: 'SYSTEM',
                                        path: filterValue
                                    },
                                    autoload: false,
                                    reader: {
                                        type: 'json'
                                    }
                                }
                            },
                            allowBlank: false,
                            forceSelection: forceSelection,
                            minChars: 1,
                            //queryMode: 'remote',
                            filterPickList: false
                        });
                    }
                    return Ext.create('Ext.grid.CellEditor', {
                        field: combo
                    });
                }
            },
            {
                text: CDI.service.Translate.data["rda-admincenter-cbs-label"],
                dataIndex: 'CBS',
                minWidth: 120,
                autoSizeColumn: true,
                filter: 'list',
                getEditor: function (record) {
                    var filterValue = "";
                    var currentGrid = this.up("grid");
                    var forceSelection = false;
                    if (currentGrid.config.masterItem == "project") {
                        forceSelection = true;
                        filterValue = currentGrid.config.extraparam + "\\" + record.data.CDC + "\\" + record.data.AREA + "\\" + record.data.SYSTEM;
                    }
                    
                    var combo = null;

                    combo = Ext.create('Ext.form.field.ComboBox', {
                        displayField: 'Value',
                        valueField: 'Value',
                        store: {
                            proxy: {
                                type: 'ajax',
                                withCredentials: true,
                                url: CD.apiUrl + 'erda/combobox/wbs/distinct',
                                extraParams: {
                                    target: 'CBS',
                                    path: filterValue
                                },
                                autoload: false,
                                reader: {
                                    type: 'json'
                                }
                            }
                        },
                        minChars: 1,
                        //queryMode: 'remote',
                        allowBlank: false,
                        forceSelection: forceSelection,
                        filterPickList: false
                    });

                    return Ext.create('Ext.grid.CellEditor', {
                        field: combo
                    });
                }
            },
            {
                text: CDI.service.Translate.data["rda-admincenter-wbs-label"],
                dataIndex: 'WBS',
                minWidth: 120,
                flex:1,
                autoSizeColumn: true,
                filter: true,
                getEditor: function (record) {
                    var filterValue = "";
                    var currentGrid = this.up("grid");
                    var forceSelection = false;
                    if (currentGrid.config.masterItem == "project") {
                        forceSelection = true;
                        filterValue = currentGrid.config.extraparam + "\\" + record.data.CDC + "\\" + record.data.AREA + "\\" + record.data.SYSTEM + "\\" + record.data.CBS;
                    }
                    var combo = null;

                    combo = Ext.create('Ext.form.field.ComboBox', {
                        displayField: 'Value',
                        valueField: 'Value',
                        store: {
                            proxy: {
                                type: 'ajax',
                                withCredentials: true,
                                url: CD.apiUrl + 'erda/combobox/wbs/distinct',
                                extraParams: {
                                    target: 'WBS',
                                    path: filterValue
                                },
                                autoload: false,
                                reader: {
                                    type: 'json'
                                }
                            }
                        },
                        minChars: 1,
                        //queryMode: 'remote',
                        allowBlank: false,
                        forceSelection: forceSelection,
                        filterPickList: false
                    });

                    return Ext.create('Ext.grid.CellEditor', {
                        field: combo
                    });
                }
                
            },
            {
                text: ' ',
                flex: 1,
                autoSizeColumn: false,
                filter: false,

            },
        ],
        bbar: [
            '->',
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-editbtn-tooltip'],
                iconCls: 'fas fa-pencil-alt',
                cls: 'pencil',
                itemId: 'edit',
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-reloadbtn-tooltip'],
                iconCls: 'fas fa-sync-alt',
                cls: 'sync',
                itemId: 'refresh',
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-cancelbtn-tooltip'],
                iconCls: 'fas fa-ban',
                cls: 'ban',
                itemId: 'cancel',
                disabled: true,
                handler: 'onButtonClick'

            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-addbtn-cancelbtn'],
                iconCls: 'fas fa-plus',
                cls: 'plus',
                itemId: 'add',
                disabled: true,
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-deletebtn-cancelbtn'],
                iconCls: 'fas fa-trash-alt',
                cls: 'trash',
                itemId: 'delete',
                disabled: true,
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-savebtn-cancelbtn'],
                iconCls: 'fas fa-save',
                cls: 'save',
                itemId: 'save',
                disabled: true,
                handler: 'onButtonClick'
            },
           
        ],
    }
});