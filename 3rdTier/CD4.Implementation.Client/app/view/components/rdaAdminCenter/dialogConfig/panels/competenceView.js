﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogConfig.panels.competenceView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogConfig.panels.competenceView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'competenceview',
        controller: 'adminconfigcontroller',
        title: false,
        cls: 'gray-window-title',
        layout: 'border',

        listeners: {
        },
        defaults: {
            collapsible: true,
            split: true,
            border: false,
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                region: 'west',
                layout: 'fit',
                margin: '0 5 1 0',
                title: '<i class="fas fa-globe"></i> ' + CDI.service.Translate.data['rda-admincenter-competence-label'],
                itemId: 'competencepanel',
                header: {
                    style: { 'padding': '8px 10px 8px 10px' },
                },
                flex: 1,
                frame: true,
                items: [
                    {
                        xtype: 'adminconfiggrid',
                        showCheckbox: false,
                        listeners: {
                            beforerender: 'onBeforeRender',
                        },
                        config: {
                            masterItem: 'competence',
                            gridItem: '',
                            header: CDI.service.Translate.data['rda-admincenter-griditem-competence-title'],
                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-competence-emptytext'],
                            allowEdit:false,
                        }
                    }
                ]
            },
            {
                region: 'center',
                layout: 'fit',
                margin: '0',
                frame: true,
                cls: 'configdataform',
                flex: 3,
                collapsible: false,
                defaults: {
                    border: true
                },
                items: [
                    {
                        title: '<i class="fas fa-pencil-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-config-title'],
                        cls: 'configdataform-header',
                        xtype: 'tabpanel',
                        isInEdit:false,
                        flex: 1,
                        icon: null,
                        tabBarHeaderPosition: 2,
                        //reference: 'tabpanel',
                        listeners: {
                            beforetabchange: function (tabpanel, newTab, oldTab) {
                                if (tabpanel.isInEdit)
                                    return false;
                            },
                        },
                        plain: true,
                        defaults: {
                            scrollable: true,
                            layout: {
                                type: 'hbox',
                                pack: 'start',
                                align: 'stretch'
                            },
                        },
                        items: [
                            {
                                title: '<i class="fas fa-car-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-project-label'],
                                itemId: 'competence-project',
                                items: [
                                    {
                                        xtype: 'adminconfiggrid',
                                        itemId: 'targetItemGrid1',
                                        config: {
                                            masterItem: 'competence',
                                            gridItem: 'project',
                                            header: CDI.service.Translate.data['rda-admincenter-griditem-project-title'],
                                            starttext: CDI.service.Translate.data['rda-admincenter-griditem-competence-child-starttext'],
                                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-competence-child-emptytext'],
                                        }
                                    }
                                ]

                            },
                            {
                                title: '<i class="fas fa-project-diagram"></i> ' + CDI.service.Translate.data['rda-admincenter-wbs-label'],
                                itemId: 'competence-wbs',
                                items: [
                                    {
                                        xtype: 'wbsgrid',
                                       
                                        options: {
                                            columnhide: ['COMPETENCE'],
                                        },
                                        itemId: 'targetItemGrid2',
                                        config: {
                                            masterItem: 'competence',
                                            gridItem: 'wbs',
                                            header: CDI.service.Translate.data['rda-admincenter-griditem-wbs-title'],
                                            starttext: CDI.service.Translate.data['rda-admincenter-griditem-competence-child-starttext'],
                                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-competence-child-emptytext'],
                                        }
                                    }
                                ]

                            },
                        ]
                    }
                ],
            }
        ]
    }
});