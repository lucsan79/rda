﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogConfig.panels.brandView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogConfig.panels.brandView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'brandview',
        controller: 'adminconfigcontroller',
        title: false,
        cls: 'gray-window-title',
        layout: 'border',

        listeners: {
        },
        defaults: {
            collapsible: true,
            split: true,
            border: false,
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                region: 'west',
                layout: 'fit',
                margin: '0 5 1 0',
                title: '<i class="fas fa-shield-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-brand-label'],
                itemId: 'brandpanel',
                header: {
                    style: { 'padding': '8px 10px 8px 10px' },
                },
                flex: 1,
                frame: true,
                items: [
                    {
                        xtype: 'adminconfiggrid',
                        showCheckbox:false,
                        listeners: {
                            beforerender: 'onBeforeRender',
                        },
                        config: {
                            masterItem: 'brand',
                            gridItem: '',
                            header: CDI.service.Translate.data['rda-admincenter-griditem-brand-title'],
                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-brand-emptytext'],
                        }
                    }
                ]
            },
            {
                region: 'center',
                layout: 'fit',
                margin: '0',
                frame: true,
                cls: 'configdataform',
                flex: 3,
                collapsible: false,
                defaults: {
                    border: true
                },
                items: [
                    {
                        title: '<i class="fas fa-pencil-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-config-title'],
                        cls: 'configdataform-header',
                        xtype: 'tabpanel',
                        flex: 1,
                        icon: null,
                        tabBarHeaderPosition: 2,
                        //reference: 'tabpanel',
                        plain: true,
                        defaults: {
                            scrollable: true,
                            layout: {
                                type: 'hbox',
                                pack: 'start',
                                align: 'stretch'
                            },
                        },
                        items: [
                            {
                                title: '<i class="fas fa-car-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-project-label'],
                                items: [
                                    {
                                        xtype: 'adminconfiggrid',
                                        itemId: 'targetItemGrid',
                                        config: {
                                            masterItem: 'brand',
                                            gridItem: 'project',
                                            header: CDI.service.Translate.data['rda-admincenter-griditem-project-title'],
                                            starttext: CDI.service.Translate.data['rda-admincenter-griditem-brand-child-starttext'],
                                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-brand-child-emptytext'],
                                        }
                                    }
                                ]

                            },
                        ]
                    }
                ],
            }
        ]
    }
});