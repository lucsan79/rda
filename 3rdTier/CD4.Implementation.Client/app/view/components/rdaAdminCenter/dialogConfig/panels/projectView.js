﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogConfig.panels.projectView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogConfig.panels.projectView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'projectview',
        controller: 'adminconfigcontroller',
        title: false,
        cls: 'gray-window-title',
        layout: 'border',

        listeners: {
            //beforerender: 'onBeforeRender',
        },
        defaults: {
            collapsible: true,
            split: true,
            border: false,
        },
        defaults: {
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
        },
        items: [
            {
                region: 'west',
                layout: 'fit',
                margin: '0 5 1 0',
                title: '<i class="fas fa-car-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-project-label'],
                itemId: 'projectpanel',
                header: {
                    style: { 'padding': '8px 10px 8px 10px' },
                },
                flex: 1,
                frame: true,
                items: [
                    {
                        xtype: 'adminconfiggrid',
                        showCheckbox: false,
                        listeners: {
                            beforerender: 'onBeforeRender',
                        },
                        config: {
                            masterItem: 'project',
                            gridItem: '',
                            header: CDI.service.Translate.data['rda-admincenter-griditem-project-title'],
                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-project-emptytext'],
                        }
                    }
                ]
            },
            {
                region: 'center',
                layout: 'fit',
                margin: '0',
                frame: true,
                cls: 'configdataform',
                flex: 3,
                collapsible: false,
                defaults: {
                    border: true
                },
                items: [
                    {
                        title: '<i class="fas fa-pencil-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-config-title'],
                        cls: 'configdataform-header',
                        xtype: 'tabpanel',
                        flex: 1,
                        icon: null,
                        tabBarHeaderPosition: 2,
                        //reference: 'tabpanel',
                        listeners: {
                            beforetabchange: function (tabpanel, newTab, oldTab) {
                                if (tabpanel.isInEdit)
                                    return false;
                            },
                        },
                        plain: true,
                        defaults: {
                            scrollable: true,
                            layout: {
                                type: 'hbox',
                                pack: 'start',
                                align: 'stretch'
                            },
                        },
                        items: [
                            
                            {
                                title: '<i class="fas fa-car-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-content-label'],
                                itemId: 'project-content',
                                items: [
                                    {
                                        xtype: 'adminconfiggrid',
                                        itemId: 'targetItemGrid1',
                                        config: {
                                            masterItem: 'project',
                                            gridItem: 'contents',
                                            header: CDI.service.Translate.data['rda-admincenter-griditem-content-title'],
                                            starttext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'],
                                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-emptytext'],
                                        }
                                    }
                                ]

                            },
                            {
                                title: '<i class="fas fa-project-diagram"></i> ' + CDI.service.Translate.data['rda-admincenter-wbs-label'],
                                itemId: 'project-wbs',
                                items: [
                                    {
                                        xtype: 'wbsgrid',
                                        itemId: 'targetItemGrid2',
                                        options: {
                                            columnhide: [],
                                        },
                                        config: {
                                            masterItem: 'project',
                                            gridItem: 'wbs',
                                            header: CDI.service.Translate.data['rda-admincenter-griditem-wbs-title'],
                                            starttext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'],
                                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-emptytext'],
                                        }
                                    }
                                ]

                            },
                            {
                                title: '<i class="fas fa-tasks"></i> ' + CDI.service.Translate.data['rda-admincenter-activitynpi-label'],
                                itemId: 'project-activitynpi',
                                items: [
                                    {
                                        xtype: 'adminconfiggrid',
                                        itemId: 'targetItemGrid3',
                                        config: {
                                            masterItem: 'project',
                                            gridItem: 'activitynpi',
                                            header: CDI.service.Translate.data['rda-admincenter-griditem-activitynpi-title'],
                                            starttext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'],
                                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-emptytext'],
                                        }
                                    }
                                ]

                            },
                            {
                                title: '<i class="fas fa-bolt"></i> ' + CDI.service.Translate.data['rda-admincenter-engine-label'],
                                itemId: 'project-engine',
                                items: [
                                    {
                                        xtype: 'adminconfiggrid',
                                        itemId: 'targetItemGrid4',
                                        config: {
                                            masterItem: 'project',
                                            gridItem: 'engines',
                                            header: CDI.service.Translate.data['rda-admincenter-griditem-engine-title'],
                                            starttext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-starttext'],
                                            emptytext: CDI.service.Translate.data['rda-admincenter-griditem-project-child-emptytext'],
                                        }
                                    }
                                ]

                            },
                            {
                                title: '<i class="fas fa-link"></i> ' +'BaaN Link',
                                itemId: 'baan-mapping',
                                //visible: CD.service.UserData.userData.IsAdmin,
                                items: [
                                    {
                                        xtype: 'baanconfigview',
                                        itemId: 'baanconfigview',
                                        
                                    }
                                ]

                            }
                        ]
                    }
                ],
            }
        ]
    }
});