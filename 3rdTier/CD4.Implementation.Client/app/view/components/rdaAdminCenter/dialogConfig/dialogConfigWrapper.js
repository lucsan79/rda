﻿Ext.define('CDI.view.components.rdaAdminCenter.dialogConfig.dialogConfigWrapper', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.dialogConfig.dialogConfigWrapper', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'dialogconfigwrapper',
        controller: 'dialogconfigwrappercontroller',
        layout: 'fit',
        cls: 'admin-wrapper-container',
        items: [
            {
                xtype: 'tabpanel',
                width: '100%',
                layout: 'fit',
                ui: 'navigation',
                tabPosition: 'left',
                cls: 'x-subtreelist-navigation domain-navigation subnavigation-border-right',
                tabRotation: 0,
                tabBar: {
                    border: false,
                },
                
                plain: true,
                minTabWidth: 150,
                bodyBorder: true,
                bodyStyle: {
                    background: '#fff',
                    'padding-left': '5px',
                    'padding-right': '1px',
                    'border-left': 'solid',
                    'border-left-color': '#c5c5c5',
                    'border-left-width': '1px',
                    'border-top-width':'0px'
                },
                defaults: {
                    textAlign: 'left',
                    layout: 'fit',
                    cls: 'admin-content-panel',
                    style: {
                        'padding-bottom': '0px'
                    },
                    
                    listeners: {
                        
                        activate: function (tab, eOpts) {
                            
                            if (tab.items.items[0].activated) {
                                var panel = {
                                    xtype: tab.items.items[0].getXType(),
                                    activated: false,
                                };
                                tab.items.items[0].destroy();
                                tab.add(panel);
                            }
                            else
                                tab.items.items[0].activated = true;
                            //var grid = tab.down("adminconfiggrid");
                            //if (grid!=null)
                            //    grid.fireEvent('gridrenderprefix', grid);
                        }
                    },
                },

                items: [
                    {
                        title: '<i class="fas fa-shield-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-brand-label'],
                        items: {
                            xtype: 'brandview',
                            activated:false,
                        }
                    },
                    {
                        title: '<i class="fas fa-globe"></i> ' + CDI.service.Translate.data['rda-admincenter-competence-label'],
                        items: [
                            {
                                xtype: 'competenceview',
                                activated: false,
                            }
                        ]
                    },
                    {
                        title: '<i class="fas fa-car-alt"></i> ' + CDI.service.Translate.data['rda-admincenter-project-label'],
                        items: {
                            xtype: 'projectview',
                            activated: false,
                        }
                    },
                   
                ]
            }
        ]

    }
});