﻿Ext.define('CDI.view.components.rdaAdminCenter.supplierManagement.supplierWrapperController', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.supplierManagement.supplierWrapperController', function () {
    return {
        extend: 'Ext.app.ViewController',
        alias: 'controller.suppliermanagerwrappercontroller',

        onBeforeRender: function (view) {
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/admin/getallbudgetyear',
                method: 'GET',
                withCredentials: true,
                async: false,
                failure: function (response) {
                    this.onBeforeRender(view);
                },
                success: function (jsonData) {
                    var data = Ext.decode(jsonData.responseText);
                    var btnPressed = null;
                    var year = new Date().getFullYear();
                    var segButton = view.down().header.items[0];
                    segButton.items[0].text = data.actual;
                    segButton.items[1].text = data.actual_1;
                    segButton.items[2].text = data.actual_2;
                    if (segButton.items[3].menu) segButton.items[3].menu.destroy();
                    if (data.older != null && data.older.length > 0) {

                        var menu = Ext.create('Ext.menu.Menu');
                        menu.on('click', function (menu, item) {
                            var view = menu.up("suppliermanager");
                            var label = CDI.service.Translate.data['rda-admincenter-fullviewyear'];
                            label = label.replace("----", item.text);
                            menu.up().setText(label);
                            view.getController().onShowView(view, item);
                        });
                        for (var i = 0; i < data.older.length; i++) {
                            menu.add({
                                text: data.older[i]
                            })
                        }
                        segButton.items[3].menu = menu;
                    }
                }
            });






        },

        onAfterRender: function (view) {
            var segButton = view.down("segmentedbutton").items;
            if (segButton.items[3].menu == null || segButton.items[3].menu.items.length == 0)
                segButton.items[3].disable();

            var button = segButton.items[0];
            button.pressed = true;
            this.onShowView(view, button);
        },

        onShowView: function (container, button) {

            if (button.itemId == "year-full")
                return;

            if (button.up().xtype == "segmentedbutton") {
                var buttonLast = button.up().items.items[3];
                var label = CDI.service.Translate.data['rda-admincenter-fullviewyear'];
                buttonLast.setText(label);
            }


            var mainView = this.getView();
            if (mainView.itemId != "suppliermanager")
                mainView = mainView.up("#suppliermanager");
            mainView.btn = button;
            var myOpSection = mainView.items.items[0];
            var actualView = myOpSection.items.items[0];
            var panel = Ext.create('CDI.view.components.rdaAdminCenter.supplierManagement.supplierGrid', {
                year: button.text,
            });
            if (panel != null) {
                if (actualView) actualView.destroy();
                myOpSection.add(panel);
            }

        },

        onCreateGridSchema: function (view) {

            var self = view;
            var pagingtoolbar = view.down("pagingtoolbar");
            if (pagingtoolbar != null) {
                pagingtoolbar.down('#refresh').hide();
            }


            var loading = view.up("suppliermanager");
            var gridProject = view.down('#suppliergrid');
            loading.setLoading(true);

            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/admin/remedysuppliermanagement/' + view.year,
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    //var exceptionMessage = Ext.decode((Ext.decode(response.responseText)).ExceptionMessage);
                    ////var errorCode = exceptionMessage.ErrorCode;
                    //var errorMessage = "";
                    //try {
                    //    errorMessage = exceptionMessage.ErrorMessage;
                    //} catch (err) { errorMessage = exceptionMessage; }
                    //Ext.MessageBox.show({
                    //    title: CDI.service.Translate.data['generic-error-popup-title'],
                    //    msg: errorMessage,
                    //    icon: 'fa fa-times-circle fa-3x',
                    //    buttons: Ext.Msg.OK
                    //});
                    loading.setLoading(false);
                }
            }).then(function (jsonData) {

                var data = Ext.decode(jsonData.responseText);

                if (data != null && data.supplierData != null && data.CDC != null) {
                    var columns = [];
                    var columnCode = {
                        xtype: 'gridcolumn',
                        text: CDI.service.Translate.data["rda-supplier-grid-suppliercode"],
                        dataIndex: 'SupplierCode',
                        width: 110,
                        align: 'center',
                        //cls: 'locked',
                        autoSizeColumn: false,
                        locked: true,
                        sortable: false,
                    };
                    columns.push(columnCode);
                    var columnName = {
                        text: CDI.service.Translate.data["rda-supplier-grid-businessname"],
                        dataIndex: 'BusinessName',
                        minWidth: 250,
                        cls: 'extend',
                        autoSizeColumn: true,
                        //filter: true,
                        locked: true,
                        sortable: false,
                    };
                    columns.push(columnName);
                    for (var index = 0; index < data.CDC.length; index++) {
                        var columnBudget = {
                            text: data.CDC[index],
                            autoSizeColumn: true,
                            cls: 'colHeaderText',
                            sortable: false,
                            columns: [
                                {
                                    cls: 'colSubHeaderText',
                                    text: "<i class='far fa-edit'></i> " + CDI.service.Translate.data["rda-supplier-grid-allocated"],
                                    sortable: false,
                                    minWidth: 150,
                                    align: 'right',
                                    renderer: function (val) {
                                        return Ext.util.Format.currency(val, "€", 2, true, " ");
                                    },
                                    dataIndex: data.CDC_KEY[index] + "_allocated",
                                    autoSizeColumn: true,
                                    //filter: true,
                                    lockable: false,
                                    sortable: false,
                                    editor: {
                                        xtype: 'numberfield',
                                        allowBlank: false,
                                        minValue: 0
                                    }
                                },
                                {
                                    cls: 'colSubHeaderStandard',
                                    text: CDI.service.Translate.data["rda-supplier-grid-residual"],
                                    sortable: false,
                                    minWidth: 150,
                                    align: 'right',
                                    renderer: function (val, meta, record, row, col) {
                                        //var cls = record.data[meta.column.dataIndex + "_cls"];
                                        //meta.tdCls = cls;
                                        return Ext.util.Format.currency(val, "€", 2, true, " ");
                                    },

                                    dataIndex: data.CDC_KEY[index] + "_residual",
                                    autoSizeColumn: true,
                                    //filter: true,
                                    lockable: false,
                                    sortable: false,
                                }
                            ]
                        }
                        columns.push(columnBudget);
                    }
                    var storeObject = {
                        fields: [],
                        data: [],
                        listeners: {
                            datachanged: function (store, options) {
                                var grid = Ext.ComponentQuery.query("#suppliergrid")[0];
                                var pagingtoolbar = grid.view.up().up().down("pagingtoolbar");
                                var btn1 = pagingtoolbar.down("#btnSave");
                                var btn2 = pagingtoolbar.down("#btnReject");
                                if (store.getUpdatedRecords().length > 0) { btn1.enable(); btn2.enable(); }
                                else { btn1.disable(); btn2.disable(); }
                            }
                        },
                    };
                    if (data != null) {
                        storeObject.data = data.supplierData;
                    }
                    view.reconfigure(storeObject, columns);
                    loading.setLoading(false);
                }


            });




        },

        onReload: function (view) {
            var mainView = view.up("suppliermanager");
            this.onShowView(mainView, mainView.btn)
        },

        onSaveDiscard: function (view, actionType) {
            var grid = view.up().down("#suppliergrid");
            var store = grid.getStore();
            var arrayToSave = [];
            if (store.getUpdatedRecords().length > 0) {
                if (actionType == "save") {

                    for (var index = 0; index < store.getUpdatedRecords().length; index++) {
                        var currentRow = store.getUpdatedRecords()[index];
                        var props = Object.keys(currentRow.modified);
                        if (props != null && props.length > 0) {
                            var supplierData = [];
                            for (var index2 = 0; index2 < props.length; index2++) {
                                var itemToSend = {
                                    cdc: props[index2].replace("_allocated", ""),
                                    value: currentRow.data[props[index2]]
                                };
                                supplierData.push(itemToSend);
                            }
                            var supplierUpdate = {
                                id: currentRow.data.ID,
                                year: currentRow.data.YEAR,
                                name: currentRow.data.BusinessName,
                                data: supplierData
                            }
                            arrayToSave.push(supplierUpdate);

                        }
                    }
                }
                var win = new Ext.Window({
                    header: false,
                    border: false,
                    closable: false,
                    draggable: false,
                    itemId: 'WfgWin',
                    title: false,
                    width: '500px',
                    height: '280px', closeAction: 'hide', buttonAlign: 'center',
                    closable: false,
                    modal: true,
                    animShow: function () {
                        this.el.slideIn('t', {
                            duration: 1, callback: function () {
                                this.afterShow(true);
                            }, scope: this
                        });
                    },
                    animHide: function () {
                        this.el.disableShadow();
                        this.el.slideOut('t', {
                            duration: 1, callback: function () {
                                this.el.hide();
                                this.afterHide();
                            }, scope: this
                        });
                    },
                    layout: {
                        align: 'stretch',
                        type: 'vbox'
                    },
                    items: [
                        {
                            xtype: 'suppliersavediscardview',
                            config: {
                                actiontype: actionType,
                                data: arrayToSave
                            },
                            flex: 1,
                        }
                    ],


                });
                win.show(Ext.getBody());
            }
        },

        onSupplierListLoad: function (view) {
            
            var grid = view;
            var pagingtoolbar = view.down("pagingtoolbar");
            if (pagingtoolbar != null) {
                pagingtoolbar.down('#refresh').hide();
            }
            var loading = grid.up();
            loading.setLoading(true);
            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/admin/supplierlist',
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    loading.setLoading(false);
                }
            }).then(function (jsonData) {
                var inputdata = Ext.decode(jsonData.responseText);
                var objectStore = Ext.create('CDI.store.rdaSimpleStore', {
                    data: inputdata,
                    pageSize: 100,
                    totalCount: (inputdata != null && inputdata.length > 0 ? inputdata.length:0)
                });
                grid.setStore(objectStore);
                loading.setLoading(false);
            });
        },

        onSupplierPriceListLoad: function (view) {
            
            var grid = view;
            var pagingtoolbar = view.down("pagingtoolbar");
            if (pagingtoolbar != null) {
                pagingtoolbar.down('#refresh').hide();
            }
            var loading = grid.up().up();
            loading.setLoading(true);

            Ext.Ajax.request({
                url: CD.apiUrl + 'erda/admin/supplierpricelist',
                method: 'GET',
                withCredentials: true,
                failure: function (response) {
                    CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    loading.setLoading(false);
                }
            }).then(function (jsonData) {
                var inputdata = Ext.decode(jsonData.responseText);
                var objectStore = Ext.create('CDI.store.rdaSimpleStore', {
                    data: inputdata,
                    pageSize: 100,
                    totalCount: (inputdata != null && inputdata.length > 0 ? inputdata.length : 0)
                });
                grid.setStore(objectStore);
                setTimeout(function () { loading.setLoading(false); }, 200);
            });
        },

        onButtonClick: function (btn) {
            
            var view = btn.up("supplierpricelistmanagement");
            var id = btn.itemId;
            if (id == "downloadPriceList") {
                var options = {
                    title:'Remedy Price List'
                }
                CDI.service.erdaAction.showExportPanel("supplierpricelist", options);
            }
            else if (id == "uploadPriceList") {
                var options = {
                    title: 'Update Remedy Price List'
                }
                CDI.service.erdaAction.showImportPanel("supplierpricelist", view.id, options);
            }

            

        }
    }
});
