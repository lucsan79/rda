﻿Ext.define('CDI.view.components.rdaAdminCenter.supplierManagement.supplierGrid', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.supplierManagement.supplierGrid', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'suppliergrid',
        itemId: 'suppliergrid',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls:'basicgrid',
        columnLines: true,
        controller: 'suppliermanagerwrappercontroller',
        mainStore: null,


        requires: [
            'Ext.selection.CellModel'
        ],


        emptyText: CDI.service.Translate.data["rda-admincenter-suppliermanagement"],
        scrollable: true,

        listeners: {
            beforerender:'onCreateGridSchema'
        },
        plugins: {
            cellediting: {
                clicksToEdit: 1
            }
        },
        syncRowHeight: true,
       
        enableColumnHide: false,
        columns:
            [


                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-code"],
                    dataIndex: 'supplierCode',
                    width: 120,
                    cls: 'locked',
                    autoSizeColumn: false,
                    locked: true,
                    renderer: function (value, meta, record) {
                        var buttonText = "<i class='fas fa-donate'></i> " + value;
                        var extraparam = (record.data.CBS.toLowerCase() == "materials" ? "material" : "");
                        return '<a title="' + CDI.service.Translate.data["dashboard-rda-cardbutton-open"] + '" role="button" class="rdalinkbutton ' + extraparam + ' x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box" hidefocus="on" unselectable="on"><span role="presentation" class="x-btn-wrap" unselectable="on"><span class="x-btn-button" role="presentation"><span class="x-btn-inner x-btn-inner-center" unselectable="on">' + buttonText + '</span><span role="presentation" class="x-btn-icon-el" unselectable="on" style=""></span></span></span></a>';
                    }
                },
                {
                    text: CDI.service.Translate.data["dashboard-rda-worklist-grid-column-description"],
                    dataIndex: 'supplierBusinessName',
                    minWidth: 100,
                    cls: 'extend',
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    sortable: false,
                }
                

            ],


        dockedItems: [{
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["rda-admincenter-suppliermanagement-grid-pagingtoolbar-emptyMsg"],
            
            items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("#suppliergrid").getController().onReload(this.up("#suppliergrid"));
                        }
                    }
                },
                {
                    xtype: 'button',
                    itemId: 'btnReject',
                    disabled: true,
                    iconCls: 'fas fa-ban',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-label-discard"],
                    tooltip: CDI.service.Translate.data["rda-admincenter-cancelbtn-tooltip"],
                  //  tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("#suppliergrid").getController().onSaveDiscard(this.up("#suppliergrid"), "discard");
                        }
                    }
                },
                {
                    xtype: 'button',
                    itemId:'btnSave',
                    iconCls: 'fas fa-save',
                    disabled: true,
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-label-savechange"],
                    tooltip: CDI.service.Translate.data["rda-admincenter-savebtn-tooltip"],
                //    tooltipType: 'title',
                    listeners: {
                        click: function () {
                            this.up("#suppliergrid").getController().onSaveDiscard(this.up("#suppliergrid"),"save");
                        }
                    }
                },
                '-',
                {
                    xtype: 'button',
                    itemId: 'btnExport',
                    iconCls: 'fas fa-file-excel',
                    cls: 'pagingToolbarButton',
                    text: CDI.service.Translate.data["common-label-export"],
                    tooltip: CDI.service.Translate.data["common-label-export-tooltip"],
                    //    tooltipType: 'title',
                    listeners: {
                        click: function (btn) {
                            
                            var grid = btn.up("grid");
                            var options = {
                                title: CDI.service.Translate.data["rda-admincenter-remedy"],
                                paramName: ["year"],
                                paramValue: [grid.year],
                            }
                            CDI.service.erdaAction.showExportPanel("remedy", options);
                        }
                    }
                },

                
            ]
        }]


    }
});


