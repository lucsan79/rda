﻿Ext.define('CDI.view.components.rdaAdminCenter.supplierManagement.SupplierListManagement', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.supplierManagement.SupplierListManagement', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'supplierlistmanagement',
        itemId: 'supplierlistmanagement',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls:'basicgrid',
        columnLines: true,
        controller: 'suppliermanagerwrappercontroller',
        mainStore: null,
               

        emptyText: CDI.service.Translate.data["rda-admincenter-suppliermanagement"],
        scrollable: true,
        plugins: [
            'gridfilters',
        ],
        viewConfig: {
            listeners: {
                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        listeners: {
            beforerender:'onSupplierListLoad'
        },
        
        syncRowHeight: true,
       
        enableColumnHide: false,
        columns:
            [


                {
                    text: CDI.service.Translate.data["supplier-grid-code"],
                    dataIndex: 'code',
                    width: 120,
                    autoSizeColumn: false,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-label"],
                    dataIndex: 'name',
                    minWidth: 300,
                    flex:1,
                    //autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-address"],
                    dataIndex: 'address',
                    minWidth: 200,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-city"],
                    dataIndex: 'city',
                    minWidth: 200,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-zipcode"],
                    dataIndex: 'zipcode',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-buyer"],
                    dataIndex: 'buyer',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-isremedy"],
                    dataIndex: 'isremedy',
                    minWidth: 80,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["grid-lastmodifydate"],
                    dataIndex: 'modifydate',
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    renderer: function (value, meta, record) {
                        value = Ext.util.Format.date(value, 'd/m/Y');
                        return value;
                    },
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            //Fix d/m/Y format
                            //debugger;
                            //var dateArray = v.toString().split('/');
                            //if (dateArray.length == 3) {
                            //    v = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                            //}
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-isvalid"],
                    dataIndex: 'valid',
                    minWidth: 80,
                    autoSizeColumn: true,
                    filter: 'list',
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-validfrom"],
                    dataIndex: 'validfrom',
                    xtype: 'datecolumn',
                    dateFormat: 'd/m/Y',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    renderer: function (value, meta, record) {
                        value = Ext.util.Format.date(value, 'd/m/Y');
                        return value;
                    },
                    
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-validto"],
                    dataIndex: 'validto',
                    xtype: 'datecolumn',
                    dateFormat: 'd/m/Y',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    renderer: function (value, meta, record) {
                        value = Ext.util.Format.date(value, 'd/m/Y');
                        return value;
                    },
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            var result = null;
                            if (v) {
                                result = Ext.Date.clearTime(new Date(v), true).getTime();
                            }
                            return result;
                        },
                    },
                }
            ],


        dockedItems: [{
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["rda-admincenter-suppliermanagement-grid-pagingtoolbar-emptyMsg"],
            
            items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            var grid = this.up("#supplierlistmanagement");
                            grid.fireEvent("beforerender", grid);
                        }
                    }
                },
                
                
            ]
        }]


    }
});


