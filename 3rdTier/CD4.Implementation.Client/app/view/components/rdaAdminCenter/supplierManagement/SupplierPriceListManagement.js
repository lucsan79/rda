﻿Ext.define('CDI.view.components.rdaAdminCenter.supplierManagement.SupplierPriceListManagement', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.supplierManagement.SupplierPriceListManagement', function () {
    return {
        extend: 'Ext.grid.Panel',
        xtype: 'supplierpricelistmanagement',
        itemId: 'supplierpricelistmanagement',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid',
        columnLines: true,
        controller: 'suppliermanagerwrappercontroller',
        mainStore: null,


        emptyText: CDI.service.Translate.data["rda-admincenter-suppliermanagement"],
        scrollable: true,
        plugins: [
            'gridfilters',
        ],
        viewConfig: {
            listeners: {
                refresh: function (dataview) {
                    Ext.each(dataview.panel.columns, function (column) {
                        if (column.autoSizeColumn === true)
                            column.autoSize();
                    })
                }
            }
        },
        listeners: {
            beforerender: 'onSupplierPriceListLoad'
        },

        syncRowHeight: true,

        enableColumnHide: false,
        columns:
            [


                {
                    text: CDI.service.Translate.data["supplier-grid-idcontratto"],
                    dataIndex: 'idcontratto',
                    width: 100,
                    autoSizeColumn: false,
                    filter: true,
                    locked: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-supplier"],
                    dataIndex: 'fornitore',
                    minWidth: 100,
                    autoSizeColumn: true,
                    locked: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-suppliername"],
                    dataIndex: 'fullname',
                    minWidth: 200,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-contractstate"],
                    dataIndex: 'stato',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: 'list',
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-attivazione"],
                    dataIndex: 'attivazione',
                    xtype: 'datecolumn',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    renderer: function (value, meta, record) {
                        value = Ext.util.Format.date(value, 'd/m/Y');
                        return value;
                    },
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            var result = null;
                            if (v) result = Ext.Date.clearTime(new Date(v), true).getTime();
                            return result;
                        },
                    },
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-scadenza"],
                    dataIndex: 'scadenza',
                    xtype: 'datecolumn',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    renderer: function (value, meta, record) {
                        value = Ext.util.Format.date(value, 'd/m/Y');
                        return value;
                    },
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            var result = null;
                            if (v) result = Ext.Date.clearTime(new Date(v), true).getTime();
                            return result;
                        },
                    },
                },

                {
                    text: CDI.service.Translate.data["supplier-grid-pos"],
                    dataIndex: 'pos',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-articolo"],
                    dataIndex: 'articolo',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-description"],
                    dataIndex: 'descrizione',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-fulldescription"],
                    dataIndex: 'descrizionecompleta',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-Price"],
                    dataIndex: 'prezzo',
                    filter: 'number',
                    align: 'right',
                    renderer: function (value, metaData, record, rowIndex) {
                        try {
                            var currency = record.data.divisa;
                            if (currency.toLowerCase() == "euro")
                                currency = "€";
                            else
                                currency = "$";
                            return CDI.service.erdaAction.formatMoney(value, currency, 2, true, " ");
                        } catch (err) { };

                        return "";
                    },
                    minWidth: 100,
                    autoSizeColumn: true,
                    lockable: false,
                    sortable: false,

                },
                {
                    text: CDI.service.Translate.data["supplier-grid-rowstatus"],
                    dataIndex: 'statoriga',
                    minWidth: 80,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                {
                    text: CDI.service.Translate.data["supplier-grid-seq"],
                    dataIndex: 'seq',
                    minWidth: 80,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                },
                //{
                //    text: CDI.service.Translate.data["supplier-grid-pwt"],
                //    dataIndex: 'pwt',
                //    minWidth: 80,
                //    autoSizeColumn: true,
                //    filter: 'list',
                //    lockable: false,
                //},
                {
                    text: CDI.service.Translate.data["supplier-grid-officialstartdate"],
                    dataIndex: 'startvalidita',
                    xtype: 'datecolumn',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    renderer: function (value, meta, record) {
                        value = Ext.util.Format.date(value, 'd/m/Y');
                        return value;
                    },
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            var result = null;
                            if (v) result = Ext.Date.clearTime(new Date(v), true).getTime();
                            return result;
                        },
                    },
                },
                {
                     text: CDI.service.Translate.data["supplier-grid-officialenddate"],
                     dataIndex: 'endvalidita',
                    xtype: 'datecolumn',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    renderer: function (value, meta, record) {
                        value = Ext.util.Format.date(value, 'd/m/Y');
                        return value;
                    },
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            var result = null;
                            if (v) result = Ext.Date.clearTime(new Date(v), true).getTime();
                            return result;
                        },
                    },
                },
                {
                    text: CDI.service.Translate.data["grid-lastmodifydate"],
                    dataIndex: 'modifydate',
                    xtype: 'datecolumn',
                    minWidth: 100,
                    autoSizeColumn: true,
                    filter: true,
                    lockable: false,
                    renderer: function (value, meta, record) {
                        value = Ext.util.Format.date(value, 'd/m/Y');
                        return value;
                    },
                    filter: {
                        type: 'date',
                        pickerDefaults: {
                        },
                        convertDateOnly: function (v) {
                            var result = null;
                            if (v) result = Ext.Date.clearTime(new Date(v), true).getTime();
                            return result;
                        },
                    },
                }
            ],


        dockedItems: [{
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            displayInfo: true,
            emptyMsg: CDI.service.Translate.data["rda-admincenter-suppliermanagement-grid-pagingtoolbar-emptyMsg"],

            items: [
                {
                    xtype: 'button',
                    itemId: 'btnRefresh',
                    iconCls: 'fas fa-sync-alt',
                    cls: 'pagingToolbarButton',
                    tooltip: CDI.service.Translate.data["rda-admincenter-reloadbtn-tooltip"],
                    //tooltipType: 'title',
                    listeners: {
                        click: function () {
                            var grid = this.up("#supplierpricelistmanagement");
                            grid.fireEvent("beforerender", grid);
                        }
                    }
                },
                {
                    text: CDI.service.Translate.data['supplier-label-download'],
                    iconCls: 'fas fa-cloud-download-alt',
                    cls: 'pagingToolbarButton',
                    itemId: 'downloadPriceList',
                    handler: 'onButtonClick'
                },
                {
                    text: CDI.service.Translate.data['supplier-label-upload'],
                    iconCls: 'fas fa-cloud-upload-alt',
                    cls: 'pagingToolbarButton green',
                    itemId: 'uploadPriceList',
                    handler: 'onButtonClick' 
                },


            ]
        }]


    }
});


