﻿Ext.define('CDI.view.components.rdaAdminCenter.supplierManagement.popupSaveDiscardView', {});

Ext.deferDefine('CDI.view.components.rdaAdminCenter.supplierManagement.popupSaveDiscardView', function () {
    return {
        extend: 'Ext.form.Panel',
        xtype: 'suppliersavediscardview',
        controller: 'suppliersavediscardviewcontroller',
        
        title: false,
        buttonAlign: 'center',
        listeners: {
            beforerender: 'onBeforeRender',
        },
        
        cls:'modal-confirm',
        config: {
            //actiontype: actionType,
            //businessDataId: itemId,
            //businessDataName: itemNumber,
            //viewId: viewId
        },
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        bodyPadding: 10,

        defaults: {
            frame: false,
            //bodyPadding: 10
            listeners: {
                click: 'onButtonActionClick',
                element: 'el',
                delegate: 'div.modal-footer'
            },
        },

        defaults: {
            //cls: 'modal-content',
            xtype: 'label',
        },
        items: [
            {
                cls: 'modal-header',
                html: '<div class="icon-box approve"><i class="fas fa-save"></i></div>'
            },
            {
                cls: 'modal-title',
                text: CDI.service.Translate.data['common-question-areyousure']
            },
            {
                cls: 'modal-body',
                html: CDI.service.Translate.data['common-question-save-pendingchanges-message']
            },
            {
                
                cls: 'modal-body approve',
                xtype: 'progressbar',
                text:'',
                visibility:false,
            }
            
        ],

        buttons: [
            {
                cls: 'btn btn-info',
                text: CDI.service.Translate.data['common-wfaction-cancel'],
                handler: 'onButtonCloseWindown'
            },
            {
                cls: 'btn btn-success',//btn-danger
                text: CDI.service.Translate.data['common-action-confirm'],
                handler: 'onButtonSaveBudget'
            }
        ],
        
        
        
        
    }
});