﻿Ext.define('CDI.view.components.rdaAdminCenter.supplierManagement.supplierWrapper', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.supplierManagement.supplierWrapper', function () {
    return {
        extend: 'Ext.panel.Panel',

        xtype: 'suppliermanager',
        controller: 'suppliermanagerwrappercontroller',
        itemId: 'suppliermanager',
        title: false,
        cls: 'gray-window-title rdadashboard',
        layout: 'border',

        padding: 1,
        //cdIcon: 'list-alt',
       
        defaults: {
            collapsible: true,
            split: true,
            //border: true,
        },
        listeners: {
            beforerender: 'onBeforeRender',
            afterrender: 'onAfterRender'
        },

        items: [
            {
                title: '<i class="fas fa-user-tie"></i> ' + CDI.service.Translate.data['rda-admincenter-suppliermanagement'],
                flex: 3,
                region: 'center',
                collapsible: false,
                itemId: 'supplierGridPanel',
                layout: 'fit',
                
                header: {
                    style: {
                        'padding': '5px 10px 5px 15px',
                    },
                    items: [
                        {
                            xtype: 'segmentedbutton',
                            listeners: {
                                toggle: 'onShowView'
                            },
                            allowToggle: true,
                            style: {
                                'margin-right': '10px'
                            },
                            defaults: {
                                cls: 'btn-toolbar-middle',
                            },
                            items: [
                                {
                                    iconCls: 'fas fa-calendar-alt',
                                    cls: 'btn-toolbar-left',
                                    itemId: 'year0',
                                    text: '',
                                    pressed: true,
                                },
                                {
                                    iconCls:'far fa-calendar-alt',
                                    cls: 'btn-toolbar-middle',
                                    itemId: 'year1',
                                    text: '',
                                },
                                {
                                    iconCls: 'far fa-calendar-alt',
                                    cls: 'btn-toolbar-middle',
                                    itemId: 'year2',
                                    text: '',
                                },
                                {
                                    iconCls: 'far fa-calendar-check',
                                    cls: 'btn-toolbar-right',
                                    itemId: 'year-full',
                                    text: CDI.service.Translate.data['rda-admincenter-fullviewyear'],
                                    //menu: [{
                                    //    text: 'Menu Item 1'
                                    //}, {
                                    //    text: 'Menu Item 2'
                                    //}, {
                                    //    text: 'Menu Item 3'
                                    //}]
                                },
                                
                            ]
                        },
                    ]
                },
                defaults: {
                    style: {
                        'margin-top': '-2px!important'
                    },
                },
                items:
                    [
                        
                    ]
            }
        ]
    }
});