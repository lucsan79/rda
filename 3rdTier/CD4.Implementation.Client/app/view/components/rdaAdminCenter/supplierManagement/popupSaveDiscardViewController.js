﻿Ext.define('CDI.view.components.rdaAdminCenter.supplierManagement.popupSaveDiscardViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.suppliersavediscardviewcontroller',

    onBeforeRender: function (view) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        var actionType = view.config.actiontype;
        var items = view.items;
        items.items[3].text = '&nbsp;';
        if (actionType == "discard") {
            items.items[0].html = '<div class="icon-box reject"><i class="fas fa-undo-alt"></i></div>', // items.items[0].html.replace("approve", "reject");
                items.items[2].html = CDI.service.Translate.data["common-question-discard-pendingchanges-message"];
            items.items[3].cls = 'modal-body reject';

            var buttons = view.query("button");
            if (buttons != null && buttons.length == 2) {
                buttons[1].cls = 'btn btn-danger';
                buttons[1].text = CDI.service.Translate.data['common-action-discard'];
            }

        }

    },
    onButtonCloseWindown: function () {
        var win = this.getView().up("window");
        if (win) {
            win.close();
            win.destroy();
        }
    },
    onButtonSaveBudget: function (btn) {
        var view = btn.up("suppliersavediscardview");
        var action = view.config.actiontype;
        var data = view.config.data;
        var parentView = Ext.ComponentQuery.query("#suppliermanager")[0];
        var progBar = view.down("progressbar");
        btn.disable();
        if (action == "discard") {
            progBar.updateProgress(1, CDI.service.Translate.data["common-label-discarded"]);
            
            setTimeout(function () {
                var win = view.up("window");
                if (win) {
                    win.close();
                    win.destroy();
                }
                parentView.getController().onShowView(parentView, parentView.btn);
            }, 800);
        }
        else {
            progBar.wait({
                interval: 5,
                increment: 80,
                text: CDI.service.Translate.data["common-saving"],
                scope: this,
                fn: function () {
                    progBar.updateText(CDI.service.Translate.data["completed-successfully"]);
                }
            });
            setTimeout(function () {
                var errorOccurred = false;
                if (data != null && data.length > 0) {
                    for (var index = 0; index < data.length; index++) {
                        
                        Ext.Ajax.request({
                            url: CD.apiUrl + 'erda/admin/remedysuppliermanagement/update',
                            method: 'POST',
                            withCredentials: true,
                            async: false,
                            jsonData: data[index],
                            failure: function (response) {
                                errorOccurred = true;
                            },
                            success: function (jsonData) {
                            }
                        });
                    }
                }
                if (errorOccurred)
                    progBar.updateProgress(1, CDI.service.Translate.data["completed-successfully"]);
                else
                    progBar.updateProgress(1, CDI.service.Translate.data["completed-donewitherror"]);
                parentView.getController().onShowView(parentView, parentView.btn);
                var win = view.up("window");
                if (win) {
                    win.close();
                    win.destroy();
                }
            }, 600);
        }

    },
    onActionCompleted: function (actionResult) {
        //actiontype: actionType,
        //businessDataId: itemId,
        //businessDataName: itemNumber,
        //viewId: viewId
        //result
        //message

    },
    donewWithError: function (message) {

        var view = this.getView();
        view.result = false;
        view.message = message;
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-donewitherror"]);
        setTimeout(function () {
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
        }, 500);
    },
    doneSuccess: function (data, closeFlag) {
        var view = this.getView();
        var progBar = view.down("progressbar");
        progBar.reset();
        progBar.updateProgress(1, CDI.service.Translate.data["completed-successfully"]);
        setTimeout(function () {
            var parentView = Ext.getCmp(view.config.viewId);
            var win = view.up("window");
            if (win) {
                win.close();
                win.destroy();
            }
            try {
                if (parentView && data != null)
                    parentView.getController().doRefresh(data);
            }
            catch (err) { };
        }, 800)

    }
});