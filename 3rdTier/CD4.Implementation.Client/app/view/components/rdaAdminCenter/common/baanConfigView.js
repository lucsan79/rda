﻿Ext.define('CDI.view.components.rdaAdminCenter.common.baanConfigView', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.common.baanConfigView', function () {
    return {
        extend: 'Ext.panel.Panel',
        xtype: 'baanconfigview',
        controller: 'baanconfigview',
        listeners: {
            beforerender: 'onBeforeRender',
            afterrender: 'onAfterRender'
        },
        project:null,
        cls: 'basicgrid',
        header: {
            cls:'x-column-header',
            style: {
                'background-color': '#f5f5f5!important;',
                'height': '35px',
            },
            html:'<span class="spaninheader">CD - BaaN Project Link</span>'
            
        },
        layout: 'fit',
        height: '100%',
        width: '100%',
        
        itemId: 'baanconfigviewpanel',
        items: [
            {
                xtype: 'form',
                itemId: 'newBaaNForm',
                flex: 1,
                width: '100%',
                items: [
                    {
                        layout: {
                            type: 'vbox'
                        },
                        style: {
                            'padding-top': '10px',
                            'padding-left': '20px',
                            'padding-right': '20px'
                        },
                        height: '100%',
                        defaults: {
                            width: '100%',
                            cls: 'inner-label'
                        },
                        items: [
                            
                            {
                                xtype: 'textfield',
                                required: true,
                                name: 'Value',
                                itemId: 'projectbaan',
                                fieldLabel: "Project BaaN",
                            },
                            {
                                xtype: 'textfield',
                                name: 'financecode',
                                itemId: 'financecode',
                                fieldLabel: 'Finance Code',
                            },
                            {
                                xtype: 'textfield',
                                name: 'prpcode',
                                itemId: 'prpcode',
                                fieldLabel: 'PRP Code',
                            },
                            {
                                xtype: 'textfield',
                                name: 'codeprojectform',
                                itemId: 'codeprojectform',
                                fieldLabel: 'Code Project Form',
                            }
                        ]
                    }
                ]

            }
        ],
        bbar: [
            '->',
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-savebtn-tooltip'],
                iconCls: 'fas fa-save',
                cls: 'save',
                itemId: 'save',
                handler: 'onButtonClick'
            }
        ],
    }
})