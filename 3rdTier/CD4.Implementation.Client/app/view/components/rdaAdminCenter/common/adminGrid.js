﻿Ext.define('CDI.view.components.rdaAdminCenter.common.adminGrid', {});
Ext.deferDefine('CDI.view.components.rdaAdminCenter.common.adminGrid', function () {
    return {
        extend: 'Ext.grid.Panel',
        controller: 'adminconfigcontroller',
        title: false,
        xtype: 'adminconfiggrid',
        layout: 'fit',
        height: '100%',
        width: '100%',
        cls: 'basicgrid',

        config: {
            masterItem: '',
            gridItem: '',
            header: '',
            starttext: '',
            emptytext: '',
            allowEdit:true

        },
        plugins: [
            'gridfilters',
            {
                ptype: 'cellediting',
                clicksToEdit: 1,
                listeners: {
                    beforeedit: 'onGridBeforeEdit',
                    validateedit: 'onGridValidateEdit',
                }
            }
        ],

       
        listeners: {
          // beforerender: 'onBeforeRender',
            afterrender:'onAfterRender',
           'loadData': 'onLoadData',
           select: 'onGridRowSelected',

        },
        enableColumnHide: false,
        allowDeselect: false,
        selType: 'checkboxmodel',
        columns: [
            {
                text:'.',
                flex: 1,
                dataIndex: 'item',
                getEditor: function (record) {
                    var currentGrid = this.up("grid");
                   // var editor = record.editor;
                    
                    //if (editor === undefined) {
                        if (currentGrid.config.gridItem != null && currentGrid.config.gridItem != "" && currentGrid.config.gridItem != "wbs") {
                            if (currentGrid.config.data === undefined || currentGrid.config.data == "") {
                                CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-admin-editing-title'], CDI.service.Translate.data['generic-error-admin-editing-selectonleft'], null);
                                return;
                            }
                            var data = null;
                            Ext.Ajax.request({
                                url: CD.apiUrl + 'erda/admin/getAvailableItems?query=&target=' + currentGrid.config.masterItem + '&subItem=' + currentGrid.config.gridItem + '&value=' + currentGrid.config.data,
                                method: 'GET',
                                withCredentials: true,
                                async: false,
                                success: function (jsonData) {
                                    data = Ext.decode(jsonData.responseText);

                                }
                            });
                            var store = Ext.create('Ext.data.Store', {
                                data: data
                            });
                            var combo = Ext.create('Ext.form.field.Tag', {
                                displayField: 'Value',
                                valueField: 'Value',
                                createNewOnEnter: true,
                                createNewOnBlur: true,
                                store: store,
                                queryMode: 'local',
                                minChars: 1,
                                filterPickList: true,
                            });
                            editor = Ext.create('Ext.grid.CellEditor', {
                                field: combo
                            });
                        }
                        else {
                            editor = Ext.create('Ext.grid.CellEditor', {
                                xtype: 'textfield',
                                allowBlank: false
                            });
                        }
                        record.editor = editor;
                    //}
                    return editor;
                },
               
                align: 'left',
                sortable: true,
                filter: true,
                listeners: {
                    render: function () {
                        this.setText(this.up("grid").config.header);
                    },
                }
            }
        ],
        bbar: [
            '->',
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-editbtn-tooltip'],
                iconCls: 'fas fa-pencil-alt',
                disabled:true,
                cls: 'pencil',
                itemId: 'edit',
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-reloadbtn-tooltip'],
                iconCls: 'fas fa-sync-alt',
                cls: 'sync',
                itemId: 'refresh',
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-cancelbtn-tooltip'],
                iconCls: 'fas fa-ban',
                cls: 'ban',
                itemId: 'cancel',
                disabled: true,
                handler: 'onButtonClick'

            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-addbtn-tooltip'],
                iconCls: 'fas fa-plus',
                cls: 'plus',
                itemId: 'add',
                disabled: true,
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-deletebtn-tooltip'],
                iconCls: 'fas fa-trash-alt',
                cls: 'trash',
                itemId: 'delete',
                disabled: true,
                handler: 'onButtonClick'
            },
            {
                tooltip: CDI.service.Translate.data['rda-admincenter-savebtn-tooltip'],
                iconCls: 'fas fa-save',
                cls: 'save',
                itemId: 'save',
                disabled: true,
                handler: 'onButtonClick'
            }
        ],
    }
});