﻿Ext.define('CDI.view.components.rdaAdminCenter.common.adminGridController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.adminconfigcontroller',

        onBeforeRender: function (grid) {
            //
            
            var toolbar = grid.view.up().down("toolbar");
            var button = toolbar.query("#edit");
            if (button!=null)button[0].enable();
            var currentView = grid.view.up().up().up();
            var targetItemGrid = currentView.down("#targetItemGrid");
            var index = 0;
            while (true) {
                if (targetItemGrid) {
                    var myStoreProjectData = new Ext.data.Store({
                        fields: [],
                        data: []
                    });
                    targetItemGrid.setEmptyText('<h4 style="margin-top:-2px;">' + targetItemGrid.config.starttext + ' </h4>');
                    targetItemGrid.setStore(myStoreProjectData);
                }
                else if (targetItemGrid == null && index > 0)
                    break;

                index += 1;
                targetItemGrid = currentView.down("#targetItemGrid" + index);
            }
            var data = null;

            this.onCheckSaveButton(grid);
            grid.setLoading(true);
            grid.setEmptyText('<h4 style="margin-top:-2px;">' + grid.config.emptytext + ' </h4>');
            var url = CD.apiUrl + 'erda/admin/getconfigItems';
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json'
                },
                url: url,
                method: 'POST',
                jsonData: {
                    target: grid.config.masterItem,
                    subItem: '',
                    filter: '',
                },
                withCredentials: true,
                failure: function (response) {
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    grid.setLoading(false);
                },
                success: function (jsonData) {
                    var data = Ext.decode(jsonData.responseText);
                    var storeObject = {
                        data: []
                    };
                    if (data != null && data.length > 0)
                        storeObject.data = data;

                    grid.setStore(Ext.create('Ext.data.Store', storeObject));
                    grid.setLoading(false);
                }
            });
        },

        onAfterRender: function (view) {
            var columnSelection = view.headerCt.down("[isCheckerHd]");
            columnSelection.hide();
            

        },

        onLoadData: function (grid) {
            
            var toolbar = grid.view.up().down("toolbar");
            var button = toolbar.query("#edit");
            if (button != null) button[0].enable();
            if (grid.config.data == "") {
                if (button != null) button[0].disable();
            }
            

            var data = null;
            var currentGrid = grid;
            var url = CD.apiUrl + 'erda/admin/getconfigItems';
            var currentPanel = currentGrid.up().getItemId();
            var currentTab = currentGrid.up().up().getActiveTab().itemId;

            if (currentPanel != currentTab)
                currentGrid.viewConfig.loadMask = false;
            else
                currentGrid.setLoading(true);

            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json'
                },
                url: url,
                method: 'POST',
                async: false,
                jsonData: {
                    target: currentGrid.config.masterItem,
                    subItem: currentGrid.config.gridItem,
                    filter: currentGrid.config.data,
                },
                withCredentials: true,
                failure: function (response) {
					CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                    currentGrid.setLoading(false);
                },
                success: function (jsonData) {

                    currentGrid.setEmptyText('<h4 style="margin-top:-2px;">' + currentGrid.config.emptytext + ' </h4>');
                    var data = Ext.decode(jsonData.responseText);
                    var storeObject = {
                        data: []
                    };
                    if (data != null && data.length > 0) {
                        var myArray = data[0].releatedItems[currentGrid.config.gridItem.toUpperCase()];
                        if (myArray.length > 0) {
                            for (var index = 0; index < myArray.length; index++) {
                                var pushItem = { item: myArray[index] };
                                storeObject.data.push(pushItem);
                            }
                        }
                    }
                    
                    currentGrid.getStore().removeAll();
                    currentGrid.setStore(Ext.create('Ext.data.Store', storeObject));
                    if (currentPanel != currentTab)
                        currentGrid.viewConfig.loadMask = false;
                    else
                        currentGrid.setLoading(false);


                }
            });

        },

        onWBSBeforeRender: function (grid) {
            var storeObject = {
                data: []
            };
            grid.setStore(Ext.create('Ext.data.Store', storeObject));
        },
        onWBSLoadData: function (grid) {
            var toolbar = grid.view.up().down("toolbar");
            var button = toolbar.query("#edit");
            if (button != null) button[0].enable();
            if (grid.config.data == "") {
                if (button != null) button[0].disable();

            }
            var data = null;
            var currentGrid = grid;

            
            var columns = grid.getColumns();
            var options = grid.options;
            if (options != null) {
                for (var i = 0; i < columns.length; i++) {
                    var column = columns[i];
                    var contains = false;
                    if (options.columnhide != null && column.dataIndex && options.columnhide.length > 0) {
                        for (var y = 0; y < options.columnhide.length; y++) {
                            if (options.columnhide[y].toLowerCase() == column.dataIndex.toLowerCase())
                                contains = true;
                        }
                    }
                    if (column.dataIndex)
                        column.setVisible(!contains);
                }
            }
            var url = CD.apiUrl + 'erda/admin/getconfigItems';
            var currentPanel = currentGrid.up().getItemId();
            var currentTab = currentGrid.up().up().getActiveTab().itemId;
            ////
            if (currentPanel == currentTab)
                currentGrid.setLoading(true);

            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json'
                },
                url: url,
                method: 'POST',
                async: false,
                jsonData: {
                    target: currentGrid.config.masterItem,
                    subItem: currentGrid.config.gridItem,
                    filter: currentGrid.config.data,
                },
                withCredentials: true,
                failure: function (response) {
                    if (currentPanel == currentTab) {
						CDI.service.erdaAction.showErrorMessage(CDI.service.Translate.data['generic-error-popup-title'], "", response);
                        currentGrid.setLoading(false);

                        currentGrid.setLoading(false);
                    }
                    
                },
                success: function (jsonData) {
                    ////
                    currentGrid.setEmptyText('<h4 style="margin-top:-2px;">' + currentGrid.config.emptytext + ' </h4>');
                    var data = Ext.decode(jsonData.responseText);
                    var storeObject = {
                        data: []
                    };

                    if (data != null && data.length > 0)
                        if (data[0].wbs != null && data[0].wbs.length > 0)
                            storeObject.data = data[0].wbs;
                    
                    currentGrid.getStore().removeAll();
                    currentGrid.setStore(Ext.create('Ext.data.Store', storeObject));

                    if (currentPanel == currentTab)
                        currentGrid.setLoading(false);
                    //currentGrid.setLoading(false);

                }
            });

        },

        onGridRowSelected: function (grid, record) {
            
            if (grid.view.up().config.gridItem != null && grid.view.up().config.gridItem != "")
                return;

            
            //if (record.phantom)
            //    return;
            var hasSelection = grid.getSelected().length == 0 ? false : true;
            var currentView = grid.view.up().up().up();
            if (hasSelection && (grid.config.gridItem == null || grid.config.gridItem == "")) {
                var targetItemGrid = currentView.down("#targetItemGrid");
                var index = 0;
                while (true) {
                    if (targetItemGrid) {
                        targetItemGrid.config.data = record.data.item;
                        if (record.data.item.indexOf("span class='projectdeleted'") != -1)
                            targetItemGrid.config.data = "";

                        if (targetItemGrid.getXType() == "wbsgrid")
                            targetItemGrid.config.extraparam = null;

                        if (record.data.subitem != null && targetItemGrid.getXType() == "wbsgrid") {
                            targetItemGrid.config.extraparam = record.data.subitem;
                            if (targetItemGrid.config.extraparam == null || targetItemGrid.config.extraparam == "") {
                                if (targetItemGrid.config.masterItem == "project")
                                    targetItemGrid.config.extraparam = "lock";
                            }
                        }

                        targetItemGrid.enable();
                        if (targetItemGrid.config.masterItem == "project" && targetItemGrid.config.gridItem == "activitynpi") {
                            if (record.data.subitem != "TRANSVERSAL") {
                                targetItemGrid.disable();
                                targetItemGrid.config.data = "";
                            }
                        }
                        if (targetItemGrid.config.masterItem == "project" && targetItemGrid.config.gridItem == "engines") {
                            if (record.data.subitem != "POWERTRAIN") {
                                targetItemGrid.disable();
                                targetItemGrid.config.data = "";
                            }
                        }
                            
                        targetItemGrid.fireEvent('loadData', targetItemGrid);
                    }
                    else if (targetItemGrid == null && index > 0)
                        break;

                    index += 1;
                    targetItemGrid = currentView.down("#targetItemGrid" + index);
                }
                var baan = currentView.down("#baanconfigview");
                if (baan !== undefined) {
                    baan.project = record.data.item;
                    baan.getController().onLoadData(baan);
                }
                else
                    CDI.service.erdaAction.showErrorMessage("Please select a project");

            }

        },

        fillEmptyStore: function (grid) {
            var myStoreProjectData = new Ext.data.Store({
                fields: [],
                data: []
            });
            grid.setEmptyText('<h4 style="margin-top:-2px;">' + grid.config.starttext + ' </h4>');
            grid.setStore(myStoreProjectData);

        },

        onGridBeforeEdit: function (editor, event, opts) {
            //var inEditMode = editor.grid.inEditMode;
            return event.record.phantom; //&& inEditMode;
        },

        onGridValidateEdit: function (editor, context) {
            if (context.value == '') {
                context.cancel = true;
                return false;
            }
            if (context.originalValue != context.value) {
                $(context.cell).children().addClass('erda-editing-cell');
            }
        },

        onDisableOtherPanel(btn, activate) {

            var grid = btn.up("grid");
            var columnSelection = grid.headerCt.down("[isCheckerHd]");
            if (grid.showCheckbox === undefined)
                grid.showCheckbox = true;
            grid.inEditMode = !activate;
            if (grid.showCheckbox == true) {
                if (activate)
                    columnSelection.hide();
                else
                    columnSelection.show();
            }
            var btnContainer = btn.up().up().up().up();
            var panel = null;
            if (btnContainer.getXType() != "tabpanel") {
                panel = btnContainer.down("tabpanel");
                //mi trovo sul pannello di sinistra
            }
            else {
                btnContainer.isInEdit = !activate,
                    btnContainer = btnContainer.up().up();
                panel = btnContainer.items.items[0]
            }
            if (panel != null && !activate)
                panel.disable();
            else if (panel != null && activate)
                panel.enable();

        },

        onButtonRender: function (itemToEnable, buttons) {
            if (itemToEnable != null && itemToEnable.length > 0) {
                for (var x = 0; x < buttons.length; x++) {
                    buttons[x].disable();
                    for (var y = 0; y < itemToEnable.length; y++) {
                        if (buttons[x].itemId == itemToEnable[y]) {
                            buttons[x].enable();
                            break;
                        }
                    }

                }
            }
        },

        onButtonClick: function (btn) {

            var edit = ["cancel", "add", "delete"];
            var cancel = ["edit", "refresh"];
            var add = [];
            var del = [];
            var save = ["edit", "refresh"];
            var myController = btn.up("grid").getController();
            //disabilito pulsanti in accordo con la fase di edit
            var bar = btn.up();
            var buttons = bar.query("button");
            var itemToEnable = null;
            switch (btn.itemId) {
                case "refresh":
                    var grid = btn.up("grid");

                    if (grid.config.gridItem == null || grid.config.gridItem == "")
                        grid.fireEvent("beforeRender", grid);
                    else
                        grid.fireEvent("loadData", grid);
                    break;
                case "edit":
                    var grid = btn.up("grid");
                    if (grid.config.extraparam == "lock") {
                        var title = CDI.service.Translate.data["generic-error-admin-editing-title"];
                        var message = CDI.service.Translate.data["generic-error-project-outof-competence"];
                        CDI.service.erdaAction.showErrorMessage(title, message);
                        break;
                    }

                    this.onDisableOtherPanel(btn, false);
                    itemToEnable = edit;
                    this.onButtonRender(itemToEnable, buttons);
                    break;
                case "delete":
                    var grid = btn.up("grid");
                    var selection = grid.getView().getSelectionModel().getSelection();
                    grid.store.remove(selection);
                    myController.onCheckSaveButton(grid);
                    this.onButtonRender(itemToEnable, buttons);
                    break;
                case "cancel":
                    var me = btn.up("grid");
                    itemToEnable = cancel;

                    if (this.gridNeedToSync(me)) {
                        var title = CDI.service.Translate.data["generic-error-warning-title"];
                        var message = CDI.service.Translate.data["common-question-pendingchange-abort-message"];

                        Ext.MessageBox.confirm(title, message,
                            function (res) {
                                if (res === 'yes') {
                                    if (me.config.gridItem == null || me.config.gridItem == "")
                                        me.fireEvent("beforeRender", me);
                                    else
                                        me.fireEvent("loadData", me);

                                    //var storeObject = {
                                    //    data: []
                                    //};
                                    //storeObject.data = me.originalData;
                                    //me.setStore(Ext.create('Ext.data.Store', storeObject));
                                    myController.onDisableOtherPanel(btn, true);

                                    myController.onButtonRender(itemToEnable, buttons);
                                    myController.onCheckSaveButton(me);
                                }
                            });
                    }
                    else {
                        this.onDisableOtherPanel(btn, true);
                        myController.onCheckSaveButton(me);
                        this.onButtonRender(itemToEnable, buttons);
                    }
                    break;
                case "add":
                    //
                    var grid = btn.up('grid');
                    var record = this.getGridModel(grid);
                    grid.store.insert(0, record);
                    grid.getView().getEl().scrollTo('Top', 0, true);

                    grid.editingPlugin.startEditByPosition({ row: 0, column: 0 });
                    myController.onCheckSaveButton(grid);
                    this.onButtonRender(itemToEnable, buttons);
                    break;
                case "save":
                    itemToEnable = save;
                    myController.onSaveData(btn, itemToEnable, buttons);
                    break;

            }
            //this.onButtonRender(itemToEnable, buttons);


        },

        gridNeedToSync: function (grid) {

            if (grid.store.getNewRecords() != null && grid.store.getNewRecords().length > 0) {
                return true;
            }

            if (grid.store.getRemovedRecords() != null && grid.store.getRemovedRecords().length > 0) {
                return true;
            }
            return false;
        },

        onCheckSaveButton: function (grid) {

            var toolbar = grid.view.up().down("toolbar");
            var button = toolbar.query("#save");
            if (grid.config.allowEdit == false) {
                toolbar.query("#edit")[0].disable();
                toolbar.query("#save")[0].disable();
                toolbar.query("#add")[0].disable();
                toolbar.query("#delete")[0].disable();
                return;
            }

            if (button != null && button.length == 1) {
                button = button[0];
                button.disable();
                button.tooltip = CDI.service.Translate.data['rda-admincenter-savebtn-tooltip'];
                if (grid.store.getNewRecords() != null && grid.store.getNewRecords().length > 0) {
                    button.enable();
                    button.setTooltip(CDI.service.Translate.data['rda-admincenter-savebtn-pending-tooltip']);
                }

                if (grid.store.getRemovedRecords() != null && grid.store.getRemovedRecords().length > 0) {
                    button.enable();
                    button.setTooltip(CDI.service.Translate.data['rda-admincenter-savebtn-pending-tooltip']);
                }

            }
        },

        getGridModel: function (grid) {
            var retValue = null;

            if (grid.getXType() == "adminconfiggrid") {
                retValue = new Ext.data.Record({
                    item: '',
                });
            }
            else if (grid.getXType() == "wbsgrid") {
                retValue = new Ext.data.Record({
                    COMPETENCE: '',
                    CDC: '',
                    AREA: '',
                    SYSTEM: '',
                    CBS: '',
                    WBS: '',
                });
            }

            return retValue;
        },

        onSaveData: function (btn, itemToEnable, buttons) {
            var grid = btn.up("grid");
           

            var myController = btn.up("grid").getController();
            var updataData = {
                target: grid.config.masterItem,
                subItem: grid.config.gridItem,
                filter: grid.config.data,
                updateItems: [],
                deleteItems: []
            }

            var url = "";
            if (grid.store.getNewRecords() != null && grid.store.getNewRecords().length > 0) {
                if (grid.getXType() == "adminconfiggrid") {
                    for (var i = 0; i < grid.store.getNewRecords().length; i++) {
                        var data = grid.store.getNewRecords()[i].data;
                        if (Array.isArray(data.item)) {
                            for (var y = 0; y < data.item.length; y++)
                                updataData.updateItems.push(data.item[y]);
                        }
                        else
                            updataData.updateItems.push(data.item);
                    }

                }
                else if (grid.getXType() == "wbsgrid") {
                    for (var i = 0; i < grid.store.getNewRecords().length; i++) {
                        var data = grid.store.getNewRecords()[i].data;
                        var value = "";
                        if (grid.config.masterItem=="competence")
                            value = "COMPETENCE=" + grid.config.data + "|CDC=" + data.CDC + "|AREA=" + data.AREA + "|SYSTEM=" + data.SYSTEM + "|CBS=" + data.CBS + "|WBS=" + data.WBS;
                        else if (grid.config.masterItem == "project")
                            value = "COMPETENCE=" + grid.config.extraparam + "|CDC=" + data.CDC + "|AREA=" + data.AREA + "|SYSTEM=" + data.SYSTEM + "|CBS=" + data.CBS + "|WBS=" + data.WBS;
                        updataData.updateItems.push(value);
                    }
                }

            }
            if (grid.store.getRemovedRecords() != null && grid.store.getRemovedRecords().length > 0) {
                if (grid.getXType() == "adminconfiggrid")
                    grid.store.getRemovedRecords().forEach(element => updataData.deleteItems.push(element.data.item));
                else if (grid.getXType() == "wbsgrid"){
                    for (var i = 0; i < grid.store.getRemovedRecords().length; i++) {
                        if (grid.store.getRemovedRecords()[i].phantom == false) {
                            updataData.deleteItems.push(grid.store.getRemovedRecords()[i].data.ID);
                        }
                       
                    }
                }
            }
            var title = CDI.service.Translate.data["generic-error-saving-title"];
            var message = CDI.service.Translate.data["common-question-save-pendingchanges-message"];

            Ext.MessageBox.confirm(title, message,
                function (res) {
                    if (res === 'yes') {
                        grid.setLoading(true);
                        var url = CD.apiUrl + 'erda/admin/updateconfigItems';
                        Ext.Ajax.request({
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            url: url,
                            method: 'POST',
                            jsonData: updataData,
                            withCredentials: true,
                            failure: function (response) {
                                grid.setLoading(false);
                                CDI.service.erdaAction.showErrorMessage(title, "", response);
                            },
                            success: function (jsonData) {
                                if (grid.config.gridItem == null || grid.config.gridItem == "")
                                    grid.fireEvent("beforeRender", grid);
                                else
                                    grid.fireEvent("loadData", grid);

                                myController.onDisableOtherPanel(btn, true);
                                myController.onButtonRender(itemToEnable, buttons);
                                grid.setLoading(false);
                            }
                        });
                    }
                   
                });




        },



    });


