﻿Ext.define('CDI.view.components.rdaAdminCenter.common.baanConfigViewController',
{
    extend: 'Ext.app.ViewController',
    alias: 'controller.baanconfigview',

    onBeforeRender: function (view)
    {

    },

    onAfterRender: function (view)
    {
       
    },

    onButtonClick: function (btn) {
        var view = btn.up("baanconfigview");
        if (view.project != null) {
            var response = Ext.Ajax.request({
                url: CD.apiUrl + 'erda/setbaandata',
                method: "POST",
                jsonData: {
                    baanProjectName: view.down("#projectbaan").value,
                    baanFinance: view.down("#financecode").value,
                    baanPRP: view.down("#prpcode").value,
                    baanCodeProjectForm: view.down("#codeprojectform").value,
                    rdaProjectName: view.project,
                },
                withCredentials: true,
                async: false,
                callback: function (opt, success, response) {
                    if (!success) {
                        self.showError(self.getMessageError(response));
                        return null;
                    }
                }
            });
            

        }

    },
    onLoadData: function (view) {
        var response = Ext.Ajax.request({
            url: CD.apiUrl + 'erda/baandata',
            method: "POST",
            jsonData: {
                rdaProjectName: view.project,
            },
            withCredentials: true,
            async: false,
            callback: function (opt, success, response) {
                
                if (success) {
                    var baan = Ext.decode(response.responseText);
                    view.down("#projectbaan").setValue(baan.baanProjectName);
                    view.down("#financecode").setValue(baan.baanFinance);
                    view.down("#prpcode").setValue(baan.baanPRP);
                    view.down("#codeprojectform").setValue(baan.baanCodeProjectForm);
                }
            }
        });
    },

    
})