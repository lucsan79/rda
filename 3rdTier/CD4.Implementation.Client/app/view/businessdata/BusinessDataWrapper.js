Ext.define('CDI.view.businessdata.BusinessDataWrapper', {});
Ext.deferDefine('CDI.view.businessdata.BusinessDataWrapper', function () {
    return {
        extend: 'CD.view.businessdata.BusinessDataWrapper',
        xtype: 'businessdatawrapper',

        initComponent: function () {
            // Change logo path
            this.items = Ext.clone(this.items);
            var menuSection = this.items[0].store.data.children;
            
            var importmaterial = {
                text: CDI.service.Translate.data['dashboard-rda-list-button-multicreation'],
                cls: 'bdwrapper-tooltips',
                itemId: 'importmaterial',
                enabledGroupList: [],
                enabledBusinessDataTypes: [],
                enabledLevelList: [],
                leaf: true
            };

            menuSection.push(importmaterial);
          
            this.callSuper();
        }
    }
});
