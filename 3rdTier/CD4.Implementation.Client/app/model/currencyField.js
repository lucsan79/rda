﻿Ext.define('CDI.model.CurrencyField', {});

Ext.deferDefine('CDI.model.CurrencyField', function () {
    return {

        extend: 'Ext.form.field.Text',
        // extend: 'Ext.form.field.Number',
        xtype: 'erdacurrencyfield',


        initComponent: function (config) {
            this.callParent(arguments);
        },

        hasFocus: false,

        enableKeyEvents: true,
        listeners: {
            keydown:
                function (obj, e) {
                    var code = e.browserEvent.keyCode;
                    if (!(code >= 48 && code <= 57) && !(code >= 97 && code <= 105) && code !== 46 && code !== 8 && code != 190 && code != 110) {
                        e.stopEvent();
                    }
                    if (code == 190 || code == 110) {
                        var value = obj.getRawValue();
                        if (value.indexOf(",")!=-1)
                            e.stopEvent();
                    }
                },
            keyup: function (me, e) {
                
                if (e.getCharCode() == 190 || e.getCharCode() == 110) {
                    var value = me.getRawValue();
                    
                    var dom = me.inputEl.dom;
                    var nStart = dom.selectionStart;
                    me.setRawValue(value.replace(".", ","));
                    //me.setRawValue(value.substring(0, nStart-1) + ',' + value.substring(nStart));
                    dom.selectionStart = dom.selectionEnd = nStart + 1;
                }
            },
            render: function () {
                var form = this.findParentByType('form');
                form.on('afterLoadRecord', function () {
                    this.toRaw();
                    if (this.getRawValue() == 0) {
                        this.setRawValue('');
                    } else {
                        this.toFormatted();
                    }
                }, this);

                form.on('beforeUpdateRecord', function () {
                    this.toRaw();
                }, this);

                form.on('afterUpdateRecord', function () {
                    this.toRaw();
                    if (this.getRawValue() == 0) {
                        this.setRawValue('');
                    } else {
                        this.toFormatted();
                    }
                }, this);
            },
            focus: function (field, e, eOpts) {
                this.toRaw();
                this.hasFocus = true;
            },
            blur: function (field, e, eOpts) {
                //Clear out commas and $
                this.toRaw();

                //If there's a value, format it
                if (field.getValue() != '') {
                    this.toFormatted();
                    this.hasFocus = false;
                }
            }
        },

        stripAlpha: function (value) {
            
            if (value.indexOf("€") != -1 || value.indexOf("$") != -1) {
                var v1 = value.toString().replace("€ ", "");
                v1 = v1.replace("$ ", "");
                while (v1.indexOf(".") != -1)
                    v1 = v1.replace(".", "");
                v1 = v1.replace(",", ".");
                value = v1;
            }
            return value;
           
        },

        toRaw: function () {
            if (this.readOnly !== true) {
                this.setRawValue(this.stripAlpha(this.getRawValue()));
            }
        },

        toFormatted: function () {
            
            var format = Ext.util.Format;
            format.thousandSeparator = '.';
            format.decimalSeparator = ',';
            var value = this.getRawValue();
            value = value.replace(",", ".");
            value = format.currency(value, this.currency + ' ', 2);
            this.setRawValue(value);
        },

        getValue: function () {
            var value = this.stripAlpha(this.getRawValue());
            value = value.replace(",", ".");
            return parseFloat(value,2);
        }

        
    }
});


