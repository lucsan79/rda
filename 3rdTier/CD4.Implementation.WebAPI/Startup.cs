﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CD4.ViewModels.BridgeClient;
using CD4.WebAPI.Controllers.BridgeClient;
using CD4.WebAPI.Services;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Newtonsoft.Json;
using Owin;

[assembly: OwinStartup("Implementation", typeof(CD4.Implementation.WebAPI.Startup))]

namespace CD4.Implementation.WebAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                map.RunSignalR(new HubConfiguration {});
            });

            // Interceptor
            // Handles route data from Request Header and maps the according route
            app.Use(async (context, next) =>
            {
                if (context.Request.Path.StartsWithSegments(new PathString("/workspacesync/upload")))
                {
                    await Task.Run(() =>
                    {
                        var fileDescriptors = StreamingService.DownloadToMemoryStream(context);
                        var workspaceSyncController = new WorkspaceSyncController();
                        var response = workspaceSyncController.Upload(fileDescriptors);

                        context.Response.Write(JsonConvert.SerializeObject(response));
                    });
                }
                else if (context.Request.Path.StartsWithSegments(new PathString("/workspacesync/download")))
                {
                    await Task.Run(() =>
                    {
                        var receiveFilesHeader = StreamingService.UploadToClientReadHeader(context);

                        var workspaceSyncController = new WorkspaceSyncController();
                        var tuple = workspaceSyncController.Download(receiveFilesHeader);

                        StreamingService.UploadToClient(context, tuple);
                    });
                }
                // Download files from the server and WPF does the rest
                else if (context.Request.Path.StartsWithSegments(new PathString("/workspacesync/import")))
                {
                    // Start new thread, so that the thread which handles requests remains free
                    await Task.Run(() =>
                    {
                        // Initializations
                        var workspaceSyncController = new WorkspaceSyncController(context);
                        ReceiveFilesHeaderViewModel receiveFilesHeader;
                        Tuple<ReceiveFilesOutputViewModel, IEnumerable<FileDescriptorViewModel>> tuple = null;

                        // If at any step the connection is broken, thread goes into catch
                        try
                        {
                            // Read header which WPF sends to the server, first thing that is done
                            receiveFilesHeader = StreamingService.UploadToClientReadHeader(context);

                            // Read files from the file service, and initialize response all descriptor
                            // In file descriptors (tuple item2), the filestream is already initialized, and has to be closed later on
                            // FileShare.Read is used so multiple processes can access it at the same time
                            tuple = workspaceSyncController.Import(receiveFilesHeader);

                            StreamingService.UploadToClient(context, tuple);
                        }
                        catch (Exception e)
                        {
                            workspaceSyncController.DeleteGeneratedFiles(tuple.Item2);
                            throw e;
                        }
                        
                        workspaceSyncController.DeleteGeneratedFiles(tuple.Item2);

                        workspaceSyncController.AddToWorkspace(receiveFilesHeader);
                    });
                }
                else
                {
                    await next();
                }
            });
        }
    }
}
