﻿
using Parallaksis.ePLMSM40;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;
using static Parallaksis.ePLMSM40.Defs.eDataListElementModule;
using Parallaksis.ePLMSM40.Defs;
using System.Data;
using CD4.Implementation.WebAPI.Services.Interfaces;
using CD4.Implementation.WebAPI.Services.Implementations;
using CD4.ViewModels;
using CD4.Implementation.WebAPI.ViewModels;
using static CD4.WebAPI.Controllers.Objects.BusinessDataController;
using System.Net.Http;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System;
using CD4.WebAPI.Helpers;
using System.Threading;

namespace CD4.Implementation.WebAPI.Controllers
{
    [SessionState(SessionStateBehavior.Required)]
    public class eRdAController : ApiController
    {
        private readonly IeRdAServices _eRdAService;
       
        public eRdAController(IeRdAServices eRdAService)
        {
            _eRdAService = eRdAService;
        }

        public eRdAController() : this(new eRdAServices())
        { }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/triggers/{name}")]
        public IHttpActionResult GetTriggerParams(string name)
        {
            return Ok(_eRdAService.GetTriggerParams(name));

        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/baandata")]
        public IHttpActionResult GetBaaNProjectMantch([FromBody] BaaNProjectMatch eRdABaaNProjectMatch)
        {
            return Ok(_eRdAService.GetBaaNProjectMantch(eRdABaaNProjectMatch.rdaProjectName));

        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/setbaandata")]
        public IHttpActionResult SetBaaNProjectMantch([FromBody] BaaNProjectMatch eRdABaaNProjectMatch)
        {
            return Ok(_eRdAService.SetBaaNProjectMantch(eRdABaaNProjectMatch));

        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/userprofile")]
        public IHttpActionResult GetUserProfile()
        {
            return Ok(_eRdAService.GetUserProfile());
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/dialog/startnewconfiguration")]
        public IHttpActionResult StartNewRdAConfiguration()
        {
            return Ok(_eRdAService.StartNewRdAConfiguration());
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/dialog/configurationmanager")]
        public IHttpActionResult ConfigurationManager(eDialogItemModel givenDialogItem)
        {
            return Ok(_eRdAService.GetDialogNextStep(givenDialogItem));

        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/dialog/getdialogitems")]
        public IHttpActionResult GetDialogListItems(eDialogListItem givenItem)
        {
            return Ok(_eRdAService.GetDialogListItems(givenItem));

        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/dialog/check")]
        public IHttpActionResult GetValidation(eValidationData givenItem)
        {
            return Ok(_eRdAService.GetValidation(givenItem));

        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/dialog/getfeaturevalue")]
        public IHttpActionResult GetFeatureValue([FromUri] string givenFeatureName)
        {
            return Ok(_eRdAService.GetDialogFeatureValue(givenFeatureName));

        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/dialog/validateqa")]
        public IHttpActionResult IsValidQA([FromUri] string parentId, [FromUri] string target, [FromUri] string value, [FromUri] string type)
        {
            return Ok(_eRdAService.IsValidQA(parentId,target, value,type));

        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/dialog/save")]
        public IHttpActionResult SaveDialog()
        {
            return Ok(_eRdAService.SaveDialog());

        }

        //ADMIN RDA

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/cachefile")]
        public IHttpActionResult PushCacheFile([FromBody] CacheFile data)
        {
            if (data != null && !string.IsNullOrEmpty(data.Name) && data.data != null)
            {
                var content = data.data;
                var index = content.ToString().IndexOf(";base64,");
                content = content.ToString().Substring(index + 8);
                data.data = System.Convert.FromBase64String(content.ToString());
                HttpContext.Current.Session[data.cacheName] = data;
            }
            
            return Ok();
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/admin/getconfigItems")]
        public IHttpActionResult GetAdminConfigItems([FromBody] AdminDataConfig data)
        {
            if(string.IsNullOrEmpty(data.subItem))
                return Ok(_eRdAService.GetAdminConfigItems(data.target));
            else
                return Ok(_eRdAService.GetAdminConfigItems(data.target, data.subItem,data.filter));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/admin/getAvailableItems")]
        public IHttpActionResult GetAvailableItems([FromUri] string target, [FromUri] string subitem, [FromUri] string value, [FromUri] string query)
        {
           
           return Ok(_eRdAService.GetAvailableItems(target, subitem, value, query));
           
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/admin/getAvailableUsers")]
        public IHttpActionResult getAvailableUsers([FromUri] string target, [FromUri] string query)
        {
            return Ok(_eRdAService.GetAvailableUsers("users", target, query));
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/admin/updateconfigItems")]
        public IHttpActionResult UpdateConfigItems([FromBody] AdminDataConfig data)
        {
            if (string.IsNullOrEmpty(data.subItem))
                return Ok(_eRdAService.UpdateAdminItems(data));
            else
            {
               
                AdminDataConfig dummy = new AdminDataConfig();
                dummy.target = data.subItem;
                dummy.updateItems = data.updateItems;
                if (data.subItem != "project")
                    dummy.deleteItems = data.deleteItems;
                var retValue = _eRdAService.UpdateAdminItemWithItems(data);
                
                if (data.subItem !="wbs")
                    _eRdAService.UpdateAdminItems(dummy);

                return Ok(retValue);
            }
                
        }



        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/admin/getProjectRoleUserItems")]
        public IHttpActionResult GetAdminProjectRoleUserItems([FromBody] AdminDataConfig data)
        {
            return Ok(_eRdAService.GetAdminProjectRoleUser(data.role,data.target,data.competence ));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/admin/updateprojectuserrole")]
        public IHttpActionResult UpdateProjectUserRole([FromBody] AdminDataConfig data)
        {
            
            return Ok(_eRdAService.UpdateProjectUserRole(data));

        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/admin/getProjectRoleFullView")]
        public IHttpActionResult GetProjectRoleFullView()
        {
            return Ok(_eRdAService.GetProjectRoleFullView());
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/admin/remedysuppliermanagement/{year}")]
        public IHttpActionResult GetRemedtSupplierManagement(string year)
        {
            return Ok(_eRdAService.GetRemedtSupplierManagement(year));

        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/admin/supplierlist")]
        public IHttpActionResult GetSupplierList()
        {
            return Ok(_eRdAService.GetSupplierList());

        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/admin/supplierpricelist")]
        public IHttpActionResult GetSupplierPriceList()
        {
            return Ok(_eRdAService.GetSupplierPriceList());

        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/admin/remedysuppliermanagement/update")]
        public IHttpActionResult UpdateRemedtSupplierManagement(SupplierManagementUppdate newBudgetValue)
        {
            return Ok(_eRdAService.UpdateRemedtSupplierManagement(newBudgetValue));

        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/admin/getallbudgetyear")]
        public IHttpActionResult GetAllBudgetYear()
        {
            return Ok(_eRdAService.GetAllBudgetYear());

        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/admin/AssignUserProjectRow")]
        public IHttpActionResult AssignUserProjectRow([FromBody] MultiAssignment data)
        {
            return Ok(_eRdAService.AssignUserProjectRow(data));

        }

        

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/checkbudget/{Id}")]
        public IHttpActionResult CheckRdABudget(int Id)
        {
            return Ok(_eRdAService.CheckRdABudget(Id));

        }



        //DASHBOARD
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/dashboard/getitems")]
        public IHttpActionResult GetDashboardItems()
        {
            return Ok(_eRdAService.GetDashboardItems());
        }

        //DASHBOARD
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/dashboard/getitemsofproject")]
        public IHttpActionResult GetDashboardItems(string project)
        {
            return Ok(_eRdAService.GetDashboardItems(project, true));
        }


        //DASHBOARD
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/dashboard/getobjectinproject")]
        public IHttpActionResult GetDashboardObjects(string project)
        {
            return Ok(_eRdAService.GetDashboardItems(project));
        }


        //WORKLSIT
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getworklist")]
        public IHttpActionResult GetUserWorkList()
        {
            return Ok(_eRdAService.GetRdaList(eRdAViewType.TOSIGN));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getfastmaterialapproval")]
        public IHttpActionResult GetFastMaterialApproval()
        {
            return Ok(_eRdAService.GetRdaList(eRdAViewType.FASTMATERIAL));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getmultimaterial")]
        public IHttpActionResult GetMultiMaterialCreation()
        {
            return Ok(_eRdAService.GetRdaList(eRdAViewType.MULTIMATERIAL));
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getsignedrda")]
        public IHttpActionResult GetUserSignedRdA()
        {
            return Ok(_eRdAService.GetRdaList(eRdAViewType.SIGNED));
            //return Ok(_eRdAService.GetUserRdASigned());
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getfullsignedrda")]
        public IHttpActionResult GetfullUserSignedRdA()
        {
            return Ok(_eRdAService.GetRdaList(eRdAViewType.SIGNED,"full"));
            //return Ok(_eRdAService.GetUserRdASigned("full"));
        }
        //WORKLSIT
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getarchivedlist")]
        public IHttpActionResult GetArchiveList([FromUri] string project, [FromUri] int page = 1, [FromUri] int pageSize = 50)
        {
            return Ok(_eRdAService.GetArchiveList(project, page, pageSize));
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getarchivedProjectlist")]
        public IHttpActionResult GetArchiveProjectList()
        {
            return Ok(_eRdAService.GetArchiveProjectList());
        }


        //END WORKLSIT

        //RDA ACTION
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/{Id}")]
        public IHttpActionResult GetRDA(int Id)
        {
            return Ok(_eRdAService.GetRDA(Id));
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("erda/{Id}")]
        public IHttpActionResult DeleteRDA(int Id)
        {
            
            return Ok(_eRdAService.DeleteRDA(Id));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/clone/{Id}")]
        public IHttpActionResult CloneRDA(int Id)
        {

            return Ok(_eRdAService.CloneRDA(Id));
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/sign/{Id}")]
        public IHttpActionResult GetRDASigner(int Id)
        {
            return Ok(_eRdAService.GetRDASigner(Id));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/workflow/execute")]
        public IHttpActionResult PerfomWorkflowAction(eRdAWorkflowAction action)
        {
            return Ok(_eRdAService.BusinessDataPerformTask(action));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getworklist/baan/{viewType}/{syncType}")]
        public IHttpActionResult GetBaanData(string viewType, string syncType)
        {
            return Ok(_eRdAService.GetBaaNRdaList(viewType, syncType));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/baan/save/{viewType}")]
        public IHttpActionResult SetBaaNData(string viewType, [FromBody] eRdABasicView data)
        {
            return Ok(_eRdAService.UpdateRdABaaNData(viewType, data));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/baan/sync")]
        public IHttpActionResult SyncBaan(eRdABaanAction action)
        {
            return Ok(_eRdAService.SyncRdAToBaan(action));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/close")]
        public IHttpActionResult FastClose(eRdACloseAction action)
        {
            return Ok(_eRdAService.FastClose(action));
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/erda/atc")]
        public IHttpActionResult SeteRdAATC(eRdAATCAction action)
        {
            return Ok(_eRdAService.SaveeRdAATC(action));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getworklist/folderar")]
        public IHttpActionResult GetFolderAR()
        {
            return Ok(_eRdAService.GetFolderAR());
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/user/getworklist/spendingcurves")]
        public IHttpActionResult GetSpendingCurves()
        {
            return Ok(_eRdAService.GetSpendingCurves());
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/spending/{id}")]
        public IHttpActionResult GetRdATranches(int id)
        {
            return Ok(_eRdAService.GetRdATranches(id));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/costs/{id}")]
        public IHttpActionResult GetRdACosts(int id)
        {
            return Ok(_eRdAService.GetRdACosts(id));

        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/costs")]
        public IHttpActionResult UpdateRdACosts(eRdACosts data)
        {
            return Ok(_eRdAService.UpdateRdACosts(data));

        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/combobox/{listitem}")]
        public IHttpActionResult GetComboBoxList(string listitem,[FromUri] string query, [FromUri] int page, [FromUri] int  start, [FromUri] int limit )
        {
            return Ok(_eRdAService.GetCombobox(listitem, query));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/combobox/wbs/distinct")]
        public IHttpActionResult GetComboboxWBS([FromUri] string target, [FromUri] string path, [FromUri] string query, [FromUri] int page, [FromUri] int start, [FromUri] int limit)
        {
            return Ok(_eRdAService.GetComboboxWBS(target, path, query));
        }

        
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/materials/list/{id}")]
        public IHttpActionResult GetMaterialsList(int id)
        {
            return Ok(_eRdAService.GetMaterialsList(id));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/materials/getmaterialfilecontent")]
        public IHttpActionResult GetMaterialImportContent(eRdAMaterialsFile data)
        {
            return Ok(_eRdAService.GetMaterialImportContent(data));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/materials/save")]
        public IHttpActionResult SetBaaNData([FromBody]eRdAMaterialsFile data)
        {
            return Ok(_eRdAService.SaveMaterials( data));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/search")]
        // GET: api/businessdata
        public IHttpActionResult Get([FromBody] GetClass data)
        {
            int total = 0;
            var queryParameters = ControllerContext.Request.GetQueryNameValuePairs();

            List<KeyValuePair<string, string>> bre;
            if (data.Data != null)
            {
                bre = data.Data.ToList();
                bre.AddRange(queryParameters);
            }
            else
            {
                bre = queryParameters.ToList();
            }

            return Ok(_eRdAService.GeteRdaSearch(bre));


            
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/export/{exporttype}")]
        public IHttpActionResult eRdAExport(string exporttype, [FromBody] eRdAExportOptions data)
        {
            HttpResponseMessage result = null;
            try
            {
                string fileName = string.Format("{0}-{1}.xlsx", data.title, DateTime.Now.ToString("yyyy-MM-dd-HHmmss"));
                for (int i = 0; i < HttpContext.Current.Session.Count; i++)
                {
                    var crntSession = HttpContext.Current.Session.Keys[i];
                    if (crntSession.StartsWith("exp_"))
                        HttpContext.Current.Session[crntSession] = null;
                }

                using (var stream = _eRdAService.eRdAExport(exporttype, data))
                {
                    result = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(stream.ToArray())
                    };

                    result.Content.Headers.ContentDisposition =
                        new ContentDispositionHeaderValue("attachment")
                        {
                            FileName = fileName
                        };
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
            }
            catch (Exception e)
            {
                _eRdAService.writeLog("erda export",ref e);
                throw e;
            }
            string id = "exp_" + Guid.NewGuid().ToString();
            HttpContext.Current.Session[id] = result;
            return Ok(id);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("erda/update/remedypricelist")]
        public IHttpActionResult UpdateRemedyPriceList(RemedyPriceListData data)
        {
            _eRdAService.UpdateRemedyPriceList(data);
            return Ok();
        }



        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("erda/export/download/{id}")]
        public HttpResponseMessage eRdADownload(string id)
        {
            HttpResponseMessage result = (HttpResponseMessage)HttpContext.Current.Session[id];
            HttpContext.Current.Session[id] = null;
            return result;
        }

    }
}