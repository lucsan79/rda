﻿using CD4.ApiExtensions.Helpers.Tree;
using CD4.Implementation.WebAPI.ViewModels;
using CD4.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace CD4.Implementation.WebAPI.Services.Interfaces
{
    public interface IeRdAServices
    {

        DataTable GetTriggerParams(string name);
        BaaNProjectMatch GetBaaNProjectMantch(string name);
        bool SetBaaNProjectMantch(BaaNProjectMatch givenRdABaaNMatch);
        UserProfile GetUserProfile();

       

        int StartNewRdAConfiguration();
        string GetDialogNextStep(eDialogItemModel previousDialogItem);
        string GetDialogListItems(eDialogListItem givenDialogListItem);
        string GetValidation(eValidationData givenDialogListItem);
        string GetDialogFeatureValue(string givenFeatureName);

        string IsValidQA(string currentParentId, string currentSelectionId, string currentValue, string operation);
        long SaveDialog();
        IEnumerable<erdaComboBoxItem> GetAvailableItems(string target, string subitem, string value, string filter);
        IEnumerable<erdaComboBoxItem> GetAvailableUsers(string target, string subitem,string filter);
        List<AdminItemConfiguration> GetAdminConfigItems(string target);
        List<AdminItemConfiguration> GetAdminConfigItems(string target, string subitem, string filter);
        List<AdminItemConfiguration> GetAdminProjectRoleUser(string role,string project,string competence);
        SupplierManagement GetRemedtSupplierManagement(string year);
        List<Supplier> GetSupplierList();
        List<SupplierPriceList> GetSupplierPriceList();

        List<RoleFullView> GetProjectRoleFullView();


        bool UpdateRemedtSupplierManagement(SupplierManagementUppdate data);
        bool UpdateAdminItems(AdminDataConfig data);
        bool UpdateAdminItemWithItems(AdminDataConfig data);
        bool UpdateProjectUserRole(AdminDataConfig data);

        BudgetYears GetAllBudgetYear();
        bool AssignUserProjectRow(MultiAssignment data);
        bool CheckRdABudget(int givenBusinessDataId);

        eUserDashboardData GetDashboardItems(string project = "", bool onlyCount=false);
        // eUserWorklistData GetWorkListItems();

        eUserArchivedData GetArchiveList(string project, int page, int pageSize);

        eUserArchivedData GeteRdaSearch(List<KeyValuePair<string, string>> parameters);


        eUserWorklistData GetArchiveProjectList();
        //eUserWorklistData GeFastMaterialItems();

       // eUserWorklistData GetUserRdASigned(string optionalstr = "");

        eUserWorklistData GetRdaList(eRdAViewType givenviewType, string optionalstr = "");

        eRdA GetRDA(int id);
        eRdA DeleteRDA(int id);
        eRdA CloneRDA(int id);
        
        eRdASigner GetRDASigner(int id);
        eRdAWorkflowResult BusinessDataPerformTask(eRdAWorkflowAction action);

        eUserWorklistData GetBaaNRdaList(string viewType, string syncType);
        eRdABasicView UpdateRdABaaNData(string viewType, eRdABasicView data);
        
        eRdAATCAction SaveeRdAATC(eRdAATCAction action);
        eRdABaanAction SyncRdAToBaan(eRdABaanAction action);

        eRdACloseAction FastClose(eRdACloseAction action);

        eUserWorklistData GetFolderAR();

        eUserWorklistData GetSpendingCurves();
        IEnumerable<eRdATranche> GetRdATranches(int id);

        eRdACosts GetRdACosts(int id);
        bool UpdateRdACosts(eRdACosts rdacosts);

        IEnumerable<erdaComboBoxItem> GetCombobox(string listItem, string filterparam);
        IEnumerable<erdaComboBoxItem> GetComboboxWBS(string target, string path, string filterparam);
        eRdAMaterialsFile GetMaterialsList(int id);
        eRdAMaterialsFile GetMaterialImportContent(eRdAMaterialsFile data);

        eRdAMaterialsFile SaveMaterials(eRdAMaterialsFile data);


        MemoryStream eRdAExport(string exporttype, eRdAExportOptions data);
        
        void UpdateRemedyPriceList(RemedyPriceListData data);
        void writeLog(string origin, ref Exception e);

    }
}