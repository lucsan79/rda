﻿using CD4.ApiExtensions.Helpers;
using CD4.ApiExtensions.Helpers.Tree;
using CD4.WebAPI.Helpers;
using CD4.WebAPI.Services.Implementations.AdminCenter;
using CD4.WebAPI.Services.Interfaces;
using CD4.Implementation.WebAPI.Services.Interfaces;
using CD4.Implementation.WebAPI.ViewModels;
using Parallaksis.ePLMSM40;
using Parallaksis.ePLMSM40.Defs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using static Parallaksis.ePLMSM40.Defs.eDataListElementModule;
using static Parallaksis.ePLMSM40.Defs.eErrorDefs;
using static System.Net.Mime.MediaTypeNames;
using CD4.ViewModels;
using CD4.ViewModels.AdminCenter;

using System.Text;
using CD4.ApiExtensions;
using Parallaksis.M40.Configurator;
using Parallaksis.M40.DataModel;
using static Parallaksis.ePLMSM40.Defs.eStructDefs;
using System.Text.RegularExpressions;
using Aspose.Cells;
using CD4.WebAPI.Services.Implementations;

namespace CD4.Implementation.WebAPI.Services.Implementations
{
    public class eRdAServices : CDApiWrapper, IeRdAServices
    {
        #region "trigger"
        public DataTable GetTriggerParams(string name)
        {
            DataTable retValue = new DataTable();
            string query = string.Format("select distinct  {0} as Name from eplms.rdadb where {0}<>'' order by {0}", name);
            retValue = ePortalHelper.ExecuteQuery(ref _connection, query);
            return retValue;
        }
        #endregion
        #region "User Profile"
        public UserProfile GetUserProfile()
        {
            UserProfile retValue = new UserProfile(_connection.isCurrentUserDba);
            string query = "with usergroup as																	" +
                            "(SELECT  																			" +
                            "		DISTINCT UPPER(eplms.eGroup.eName) AS PROFILE								" +
                            "FROM   																			" +
                            "		eplms.eGroupUser INNER JOIN													" +
                            "        eplms.eGroup ON eplms.eGroupUser.eGroupId = eplms.eGroup.eId				" +
                            "WHERE																				" +
                            "		eplms.eGroupUser.eUserId= @Param1										" +
                            "UNION ALL																			" +
                            "																					" +
                            "SELECT   																			" +
                            "		DISTINCT UPPER(eplms.eRole.eName) AS PROFILE								" +
                            "FROM    																			" +
                            "		eplms.eProjectRoleUser INNER JOIN											" +
                            "       eplms.eProjectRole ON 														" +
                            "		eplms.eProjectRoleUser.eProjectRoleId = eplms.eProjectRole.eId INNER JOIN	" +
                            "       eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId					" +
                            "where 																				" +
                            "		eplms.eProjectRoleUser.eUserId=@Param1									" +
                            "																					" +
                            "union all																			" +
                            "		select TOP(1) 'PR' as PROFILE from eplms.eListItem							" +
                            "where eListItem.eValue='@Param2'										" +
                            ")																					" +
                            "																					" +
                            "select distinct usergroup.PROFILE from usergroup									";
            if (!_connection.isCurrentUserDba)
            {
                DataTable myTableRole = ePortalHelper.ExecuteQuery(ref _connection, query, new string[] { _connection.currentUserId.ToString(), _connection.currentUserName }, null);
                if (myTableRole != null && myTableRole.Rows.Count > 0)
                {
                    foreach (DataRow r in myTableRole.Rows)
                    {

                        //Planner = defaultValue;

                        //PESController = defaultValue;
                        //PRPController = defaultValue;
                        //ConfigAdmin = defaultValue;
                        //SuperVisor = defaultValue;
                        //UR = defaultValue;

                        switch (r["PROFILE"].ToString())
                        {
                            case "REQUESTER":
                                retValue.REQUESTER = true;
                                break;
                            case "PR":
                                retValue.PR = true;
                                break;
                            case "CMS":
                                retValue.CMS = true;
                                break;
                            case "PM":
                                retValue.PM = true;
                                break;
                            case "PMP":
                                retValue.PRPController = true;
                                break;
                            case "PPM":
                                retValue.PESController = true;
                                break;
                            case "SUPERVISORE":
                                retValue.SuperVisor = true;
                                break;
                            case "ERDA CONFIG ADMIN":
                                retValue.ConfigAdmin = true;
                                break;
                            default:
                                if (r["PROFILE"].ToString().StartsWith("PL_"))
                                    retValue.Planner = true;
                                else if (r["PROFILE"].ToString().StartsWith("UR_"))
                                    retValue.UR = true;
                                else if (r["PROFILE"].ToString().StartsWith("PRCDC_"))
                                    retValue.UR = true;
                                break;
                        }
                    }
                }
                if (retValue.PESController || retValue.PRPController)
                {
                    if (retValue.REQUESTER == false &&
                    retValue.PR == false &&
                    retValue.Planner == false &&
                    retValue.CMS == false &&
                    retValue.ConfigAdmin == false &&
                    retValue.SuperVisor == false &&
                    retValue.UR == false &&
                    retValue.PM == false)
                        retValue.OnlyBaaN = true;
                }
            }
            return retValue;
        }
        #endregion

        #region "Dialog Configuration"
        public int StartNewRdAConfiguration()
        {
            int retValue = Parallaksis.ePLMSM40.Defs.eErrorDefs.ePLMS_OK;
            int currentUserId = _connection.currentUserId;
            string configFullFileNAme = "";
            var dbaConnection = ePortalHelper.GetDbaConnection(ref _connection);
            HttpContext.Current.Session["erda_std_attach"] = null;
            HttpContext.Current.Session["erda_ss_attach"] = null;
            HttpContext.Current.Session["UserRdAConfigurtionDialgo"] = "";
            eBusinessDataType myBusinessDataRdA = new eBusinessDataType(ref dbaConnection);
            try
            {
                if (myBusinessDataRdA.Load("RDA") == ePLMS_OK)
                {
                    eBusinessData myConfiguration = new eBusinessData(ref dbaConnection);
                    if (myConfiguration.Load("Configuration", myBusinessDataRdA.Description, "", "", "", "", -1) == ePLMS_OK)
                        configFullFileNAme = eRdAHelper.DeployDialogConfiguration(ref dbaConnection, _connection.currentUserId, ref myConfiguration);
                }

            }
            catch (Exception ex)
            {
                _connection.errorMessage = ex.Message;
                retValue = ePLMS_ERROR;

            }
            finally
            {
                HttpContext.Current.Session["UserRdAConfigurtionDialgo"] = null;
                HttpContext.Current.Session["UserConfigurationManager"] = null;
                if (System.IO.File.Exists(configFullFileNAme))
                {
                    Dictionary<string, string> inputVariables = new Dictionary<string, string>();
                    Dictionary<string, double> kFeaturesValues = new Dictionary<string, double>();
                    inputVariables.Add("USER_GROUP", eRdAHelper.GetUserWhereReferencedByGroupList(ref _connection));
                    ConfiguratorManager userConfigurationManager = new ConfiguratorManager(configFullFileNAme, inputVariables, kFeaturesValues, false, true, "");
                    HttpContext.Current.Session["UserRdAConfigurtionDialgo"] = configFullFileNAme;
                    HttpContext.Current.Session["UserConfigurationManager"] = userConfigurationManager;
                }

                DestroyDbAdminConnection(ref _connection, ref dbaConnection);
            }

            if (retValue != ePLMS_OK)
                ThrowError = retValue;
            return retValue;
        }

        public static void ParseDialogItem(ref eDialogItemModel givenDialogItem)
        {
            foreach (var property in givenDialogItem.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(string))
                    property.SetValue(givenDialogItem, HttpUtility.UrlDecode(property.GetValue(givenDialogItem, null).ToString()), null);

            }
        }
        public string GetDialogNextStep(eDialogItemModel previousDialogItem)
        {
            string retValue = "";
            Dialogs currentDialog = null;
            ParseDialogItem(ref previousDialogItem);
            ConfiguratorManager userConfigurationManager = (ConfiguratorManager)HttpContext.Current.Session["UserConfigurationManager"];
            if (userConfigurationManager != null)
            {
                if (previousDialogItem.operationType == eDialogOperationType.STARTUP)
                    currentDialog = userConfigurationManager.GetNextConfiguratorStep("", "", "");
                else if (previousDialogItem.operationType == eDialogOperationType.GONEXT)
                {
                    userConfigurationManager.UpdateCurrentQASelection(previousDialogItem.selectedOption, previousDialogItem.parentId, previousDialogItem.id, previousDialogItem.inputValue);
                    currentDialog = userConfigurationManager.GetNextConfiguratorStep(previousDialogItem.selectedOption, previousDialogItem.parentId, previousDialogItem.id);
                }
                else if (previousDialogItem.operationType == eDialogOperationType.SAVE)
                {
                }

                if (currentDialog != null)
                {
                    if (currentDialog.Questions != null && currentDialog.Questions.Count > 0)
                        retValue = userConfigurationManager.GetQADialogHTMLFilter1(currentDialog.Questions[0].sequenceID);

                    retValue += "|" + userConfigurationManager.GetDialogHTML(currentDialog);
                }
                else
                {
                    retValue = userConfigurationManager.GetFullQADialogHTML();
                    retValue += ConfiguratorSequenceHelper.DIALOG_TAG_END;
                }
            }
            // if (!string.IsNullOrEmpty(retValue))
            //    retValue += ConfiguratorSequenceHelper.DIALOG_TAG_NEWLINE;




            return retValue;
        }
        public string GetDialogListItems(eDialogListItem givenDialogListItem)
        {
            string retValue = "";
            var dbaConnection = ePortalHelper.GetDbaConnection(ref _connection);
            switch (givenDialogListItem.reqtype)
            {
                case "GetLocationList":
                    {
                        retValue = eRdAHelper.GetLocationList(ref _connection);
                        break;
                    }

                case "GetList":
                    {
                        retValue = eRdAHelper.GetList(ref _connection, givenDialogListItem.basicList, givenDialogListItem.searchItem);
                        break;
                    }

                case "GetNestedList":
                    {
                        retValue = eRdAHelper.GetNestedList(ref _connection, givenDialogListItem.basicList, givenDialogListItem.lvl1, givenDialogListItem.lvl2, givenDialogListItem.searchItem, givenDialogListItem.competence);
                        break;
                    }

                case "GetAttributeValue":
                    {
                        string givenid = givenDialogListItem.eId;
                        if (givenid.Contains("#"))
                            givenid = givenid.Split("#".ToCharArray())[0];
                        eBusinessData.getAttribute(ref dbaConnection, int.Parse(givenid), givenDialogListItem.eName, ref retValue);
                        break;
                    }
                case "GetSupplierList":
                    {
                        retValue = eRdAHelper.GetSupplierList(ref _connection, givenDialogListItem.searchItem);
                        break;
                    }
                case "GetRemedySupplierList":
                    {
                        bool isTest = false;
                        if(!string.IsNullOrEmpty(givenDialogListItem.cbs) && !string.IsNullOrEmpty(givenDialogListItem.wbs))
                            isTest=givenDialogListItem.cbs.ToLower().Equals("test/tool proto/outsource&facilities") && givenDialogListItem.wbs.ToLower().Contains("test");
                        retValue = eRdAHelper.GetRemedySupplierList(ref _connection, givenDialogListItem.searchItem, givenDialogListItem.brand, isTest);
                        break;
                    }
                case "GetMaxTrancheNumber":
                case "GetMaxSplitTrancheNumber":
                case "GetOfferAllowedFileType":
                case "SingleSourceAllowedFileType":
                    {

                        string key = givenDialogListItem.reqtype.ToUpper().Substring(3);
                        var myList = new eList(ref dbaConnection);
                        if (myList.Load("RDA WAVE1 CONFIG") == ePLMS_OK)
                        {
                            var listItem = new List<listItemElement>();
                            myList.getItemList(ref listItem);
                            if (listItem != null)
                            {
                                foreach (listItemElement item in listItem)
                                {
                                    if (item.Value.ToString().ToUpper() == key)
                                    {
                                        retValue = item.Description;
                                        break;
                                    }
                                }
                            }
                        }

                        break;
                    }

                case "GetWBSWizard":
                    {
                        string competence = givenDialogListItem.competence;
                        string project = givenDialogListItem.project;
                        string cbc = givenDialogListItem.cdc;
                        string area = givenDialogListItem.area;
                        string system = givenDialogListItem.system;
                        string cbs = givenDialogListItem.cbs;
                        string wbs = givenDialogListItem.wbs;

                        retValue = eRdAHelper.GetWBSWizardItems(ref dbaConnection, givenDialogListItem);
                        break;
                    }

                case "GetDefaultWBSWizard":
                    {
                        //string competence = Request.Params("competence");
                        //string param = Request.Params("param");
                        //string project = Request.Params("project");
                        //retValue = GetDefaultWBSWizardItems[competence, param, project];
                        break;
                    }

                case "GetProfessionalRoles":
                    {
                        string competence = givenDialogListItem.competence;// Request.Params("competence");
                        string context = givenDialogListItem.context; //Request.Params("context");
                        string supplier = givenDialogListItem.suppliercode;
                        string searchitem = givenDialogListItem.searchItem;
                        bool isTest = false;
                        if (!string.IsNullOrEmpty(givenDialogListItem.cbs) && !string.IsNullOrEmpty(givenDialogListItem.wbs))
                            isTest = givenDialogListItem.cbs.ToLower().Equals("test/tool proto/outsource&facilities") && givenDialogListItem.wbs.ToLower().Contains("test");

                        retValue = eRdAHelper.GetProfessionalRolesOnContext(ref _connection, competence, context, supplier, searchitem, isTest);
                        break;
                    }

                case "GetSite":
                    {
                        string competence = givenDialogListItem.competence;// Request.Params("competence");
                        string context = givenDialogListItem.context; //Request.Params("context");
                        string supplier = givenDialogListItem.suppliercode;
                        string professionalRole = HttpUtility.UrlDecode(givenDialogListItem.professionalrole);
                        string searchitem = givenDialogListItem.searchItem;

                        //string competence = Request.Params("competence");
                        //string context = Request.Params("context");
                        //string supplier = Request.Params("suppliercode");
                        //string professionalRole = Server.UrlDecode(Request.Params("profrole"));
                        //string searchitem = Request.Params("searchItem");

                        retValue = eRdAHelper.GetSites(ref _connection, competence, context, supplier, professionalRole, searchitem);
                        break;
                    }

                case "GetPrice":
                    {
                        string competence = givenDialogListItem.competence;// Request.Params("competence");
                        string context = givenDialogListItem.context; //Request.Params("context");
                        string supplier = givenDialogListItem.suppliercode;
                        string professionalRole = HttpUtility.UrlDecode(givenDialogListItem.professionalrole);
                        string site = givenDialogListItem.site;
                        string startdate = givenDialogListItem.startdate;
                        string enddate = givenDialogListItem.enddate;
                        string days = givenDialogListItem.days;
                        bool isTest = false;
                        if (!string.IsNullOrEmpty(givenDialogListItem.cbs) && !string.IsNullOrEmpty(givenDialogListItem.wbs))
                            isTest = givenDialogListItem.cbs.ToLower().Equals("test/tool proto/outsource&facilities") && givenDialogListItem.wbs.ToLower().Contains("test");

                        retValue = eRdAHelper.GetPrice(ref _connection, competence, context, supplier, professionalRole, site, days, startdate, enddate, isTest);
                        break;
                    }

                default:
                    {
                        break;
                    }
            }




            return retValue;
        }

        public string GetValidation(eValidationData givenData)
        {
            string returnValue = "";
            if (givenData.CheckType == "verifyactivityrange")
            {
                string startdate = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("startdate"))];
                string enddate = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("enddate"))];
                string valSeparator = ".";
                if (enddate.Contains("/"))
                    valSeparator = "/";
                string myNewEnd = (enddate.Split(valSeparator.ToCharArray())[2] + (enddate.Split(valSeparator.ToCharArray())[1] + enddate.Split(valSeparator.ToCharArray())[0]));
                string competence = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("competence"))];
                string supplier = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("supplier"))];
                string professionalRole = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("professionalrole"))];
                string site = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("site"))];

                competence = System.Net.WebUtility.UrlDecode(competence);
                supplier = System.Net.WebUtility.UrlDecode(supplier);
                professionalRole = System.Net.WebUtility.UrlDecode(professionalRole);
                site = System.Net.WebUtility.UrlDecode(site);

                string myMaxEnd = eRdAHelper.GetMaxDataRange(ref _connection, startdate, competence, supplier, professionalRole, site, enddate);
                returnValue = "No valid price list for the selected range!<br>Please consider Max Actvitity End";
                if (!string.IsNullOrEmpty(myMaxEnd))
                    if ((myNewEnd.CompareTo(myMaxEnd) <= 0))
                        returnValue = "";

            }
            else if (givenData.CheckType == "getactivityrange")
            {
                string startdate = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("startdate"))];
                string competence = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("competence"))];
                string supplier = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("supplier"))];
                string professionalRole = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("professionalrole"))];
                string site = givenData.PropertyValue[Array.IndexOf(givenData.PropertyName, ("site"))];

                competence = System.Net.WebUtility.UrlDecode(competence);
                supplier = System.Net.WebUtility.UrlDecode(supplier);
                professionalRole = System.Net.WebUtility.UrlDecode(professionalRole);
                site = System.Net.WebUtility.UrlDecode(site);

                returnValue = eRdAHelper.GetMaxDataRange(ref _connection, startdate, competence, supplier, professionalRole, site);
            }
            return returnValue;
        }
        public string GetDialogFeatureValue(string givenFeatureName)
        {
            string retValue = "";
            ConfiguratorManager userConfigurationManager = (ConfiguratorManager)HttpContext.Current.Session["UserConfigurationManager"];
            retValue = userConfigurationManager.GetFeatureValue(givenFeatureName);

            return retValue;
        }

        public string IsValidQA(string currentParentId, string currentSelectionId, string currentValue, string operation)
        {
            string retValue = "";
            if (operation.ToUpper() == "CA")
            {
                ConfiguratorManager userConfigurationManager = (ConfiguratorManager)HttpContext.Current.Session["UserConfigurationManager"];
                retValue = userConfigurationManager.ValidateCA(currentSelectionId.Split(".".ToCharArray())[1], currentValue);
            }
            return retValue;
        }

        public long SaveDialog()
        {

            ConfiguratorManager userConfigurationManager = (ConfiguratorManager)HttpContext.Current.Session["UserConfigurationManager"];
            if (userConfigurationManager != null)
            {
                string tmpPath = "";
                DataSet dummyDataSet = null;

                tmpPath = System.IO.Path.Combine(_connection.ePLMS_HOME, "tmp");
                tmpPath = System.IO.Path.Combine(tmpPath, _connection.currentDBStatus);
                System.IO.Directory.CreateDirectory(tmpPath);
                tmpPath = System.IO.Path.Combine(tmpPath, "dialog.xml");
                try
                {
                    System.IO.File.Delete(tmpPath);
                }
                catch (Exception e) { };


                dummyDataSet = userConfigurationManager.SaveDialog();
                if (dummyDataSet != null)
                {
                    dummyDataSet.WriteXml(tmpPath);

                    if (System.IO.File.Exists(tmpPath))
                    {
                        Dictionary<string, string> myListAttribute = new Dictionary<string, string>();
                        myListAttribute = eRdAHelper.StoreRDAAttribute(tmpPath, dummyDataSet, ref userConfigurationManager);
                        if (myListAttribute != null && myListAttribute.Count > 0)
                        {
                            eBusinessData myNewRDA = new eBusinessData(ref _connection);
                            myNewRDA.Name = "#";
                            myNewRDA.businessDataType = "RDA";
                            myNewRDA.Project = "RDA";
                            myNewRDA.Owner = _connection.currentUserName;
                            myNewRDA.Revision = "";
                            if (myListAttribute.ContainsKey("eName"))
                            {
                                myNewRDA.Name = myListAttribute["eName"];
                                myListAttribute.Remove("eName");
                            }
                            if (myListAttribute.ContainsKey("eDescription"))
                            {
                                myNewRDA.Description = myListAttribute["eDescription"];
                                myListAttribute.Remove("eDescription");
                            }
                            ArrayList myAttirubtes = new ArrayList();
                            ArrayList myUserAttirubtes = new ArrayList();
                            eBusinessDataType myRDA = new eBusinessDataType(ref _connection);
                            myRDA.Load("RDA");
                            myRDA.getAttributeList(ref myAttirubtes);
                            if (myAttirubtes != null)
                            {
                                //foreach (eDataListElement edata in myAttirubtes)
                                for (int index = 0; index < myAttirubtes.Count; index++)
                                {
                                    eDataListElement edata = (eDataListElement)myAttirubtes[index];
                                    if (edata.Name == "Supplier")
                                    {
                                        if (myListAttribute.ContainsKey("SupplierRemedy"))
                                        {
                                            edata.Name = "SupplierRemedy";
                                            if (myListAttribute.ContainsKey("ActivityEnd"))
                                            {
                                                eDataListElement myNewDataErosioneBudget = new eDataListElement();
                                                myNewDataErosioneBudget.Name = "BaanIn_DataErosioneBudget";
                                                myNewDataErosioneBudget.Value = myListAttribute["ActivityEnd"];
                                                myUserAttirubtes.Add(myNewDataErosioneBudget);
                                            }
                                        }
                                    }
                                    if (edata.Name == "rdaw1_professionalrole")
                                    {
                                        if (myListAttribute.ContainsKey("remedy_rdaw1_professionalrole"))
                                        {
                                            edata.Value = myListAttribute["remedy_rdaw1_professionalrole"];
                                            myUserAttirubtes.Add(edata);
                                        }
                                    }

                                    if (myListAttribute.ContainsKey(edata.Name))
                                    {
                                        if (edata.Name.ToUpper() == "ProjectName".ToUpper())
                                            myNewRDA.Project = myListAttribute[edata.Name];
                                        edata.Value = myListAttribute[edata.Name];
                                        if (edata.Type == "LOGICAL")
                                        {
                                            if (edata.Value == "1" | edata.Value == "true")
                                                edata.Value = "TRUE";
                                            else
                                                edata.Value = "FALSE";
                                        }
                                        myUserAttirubtes.Add(edata);
                                    }
                                }
                                if (myListAttribute.ContainsKey("SummaryAmount"))
                                {
                                    var currentValue = myListAttribute["SummaryAmount"];
                                    string valore = "";
                                    string divisa = "";

                                    string val1 = currentValue.Split(" ".ToCharArray())[0].Trim();
                                    string val2 = currentValue.Split(" ".ToCharArray())[1].Trim();

                                    if (eRdAHelper.IsNumeric(val1.Replace(",", "").Replace(".", "").Trim()))
                                    {
                                        valore = val1;
                                        divisa = val2;
                                    }
                                    else
                                    {
                                        valore = val2;
                                        divisa = val1;
                                    }

                                    valore = valore.Replace(".", "").Replace(",", ".");
                                    eDataListElement myNewDataAmount = new eDataListElement();
                                    myNewDataAmount.Name = "Amount";
                                    myNewDataAmount.Value = valore;
                                    myNewDataAmount.Value = eRdAHelper.GetDoubleValue(myNewDataAmount.Value).ToString().Replace(",", ".");
                                    myUserAttirubtes.Add(myNewDataAmount);

                                    eDataListElement myNewDataAmountFormat = new eDataListElement();
                                    myNewDataAmountFormat.Name = "AmountFormat";
                                    if (divisa == "€" | divisa.ToLower() == "euro")
                                        divisa = "euro";
                                    else if (divisa == "$" | divisa.StartsWith("dollar"))
                                        divisa = "dollar";
                                    myNewDataAmountFormat.Value = divisa;
                                    myUserAttirubtes.Add(myNewDataAmountFormat);

                                    string currentDailyCost = "0 €";
                                    if(myListAttribute.ContainsKey("SummaryDailyCost"))
                                        currentDailyCost = myListAttribute["SummaryDailyCost"];

                                    val1 = currentDailyCost.Split(" ".ToCharArray())[0].Trim();
                                    val2 = currentDailyCost.Split(" ".ToCharArray())[1].Trim();

                                    if (eRdAHelper.IsNumeric(val1.Replace(",", "").Replace(".", "").Trim()))
                                    {
                                        valore = val1;
                                        divisa = val2;
                                    }
                                    else
                                    {
                                        valore = val2;
                                        divisa = val1;
                                    }
                                    valore = valore.Replace(".", "").Replace(",", ".");
                                    eDataListElement myNewDataDailyCost = new eDataListElement();
                                    myNewDataDailyCost.Name = "DailyCost";
                                    myNewDataDailyCost.Value = valore;
                                    myNewDataDailyCost.Value = eRdAHelper.GetDoubleValue(valore).ToString().Replace(",", ".");
                                    myUserAttirubtes.Add(myNewDataDailyCost);

                                    string currentArticolo = myListAttribute["SummaryItemCode"];
                                    eDataListElement myNewDatacurrentArticolo = new eDataListElement();
                                    myNewDatacurrentArticolo.Name = "BaanIn_Articolo";
                                    myNewDatacurrentArticolo.Value = currentArticolo;
                                    myUserAttirubtes.Add(myNewDatacurrentArticolo);
                                }
                            }
                            ThrowError = myNewRDA.Create(ref myUserAttirubtes);

                            CacheFile offerAttach = (CacheFile)HttpContext.Current.Session["erda_std_attach"];
                            CacheFile ssAttach = (CacheFile)HttpContext.Current.Session["erda_ss_attach"];

                            CacheFile dialog = new CacheFile()
                            {
                                Name = myNewRDA.Name + ".xml",
                                Description = "Configurazione RDA",
                                data = System.IO.File.ReadAllBytes(tmpPath),
                                FileType = "XML"
                            };
                            eRdAHelper.AddFileToRDA(dialog, ref _connection, ref myNewRDA);
                            try
                            {
                                System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(tmpPath), true);
                            }
                            catch (Exception ex)
                            {
                            }
                            string messageError = "";
                            if (offerAttach != null)
                            {
                                offerAttach.FileType = "Offer";
                                eRdAHelper.AddFileToRDA(offerAttach, ref _connection, ref myNewRDA);
                                HttpContext.Current.Session["erda_std_attach"] = null;
                            }
                            if (ssAttach != null)
                            {
                                ssAttach.FileType = "SingleSource";
                                eRdAHelper.AddFileToRDA(ssAttach, ref _connection, ref myNewRDA);
                                HttpContext.Current.Session["erda_ss_attach"] = null;
                            }

                            return myNewRDA.Id;

                        }
                    }
                    ;
                }
                throw new Exception("Error saving user dialog configuration!Please contact system administrator");

            }
            throw new Exception("User Session in error!Please contact system administrator");

        }

        #endregion

        #region "ADMIN"
        public List<AdminItemConfiguration> GetAdminConfigItems(string target)
        {
            List<AdminItemConfiguration> retValue = new List<AdminItemConfiguration>();
            string retContent = "";
            string scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\RdAConfigurationTarget-" + target + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\RdAConfigurationTarget.eplms");

            if (System.IO.File.Exists(scriptFullName))
            {
                retContent = System.IO.File.ReadAllText(scriptFullName);
                DataTable configTable = ePortalHelper.ExecuteQuery(ref _connection, retContent, new string[1] { target }, null);
                if (configTable != null && configTable.Rows.Count > 0)
                {



                    if (!configTable.Columns.Contains("SUBGROUP") && !configTable.Columns.Contains("GROUP"))
                    {
                        if (!configTable.Columns.Contains("subitem"))
                            configTable.Columns.Add("subitem");
                        foreach (DataRow row in configTable.Rows)
                        {
                            AdminItemConfiguration tmp = new AdminItemConfiguration();
                            tmp.item = row[target].ToString();
                            tmp.subitem = ("" + row["subitem"]).ToString();
                            retValue.Add(tmp);
                        }
                        if (target == "project")
                        {
                            string scriptFullNamedisabled = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\RdAConfigurationTarget-" + target + "disabled.eplms");
                            if (System.IO.File.Exists(scriptFullNamedisabled))
                            {
                                retContent = System.IO.File.ReadAllText(scriptFullNamedisabled);
                                configTable = ePortalHelper.ExecuteQuery(ref _connection, retContent, new string[1] { target }, null);
                                if (!configTable.Columns.Contains("subitem"))
                                    configTable.Columns.Add("subitem");
                                foreach (DataRow row in configTable.Rows)
                                {
                                    AdminItemConfiguration tmp = new AdminItemConfiguration();
                                    tmp.item = "<span class='projectdeleted' title='Project deleted'>" + row[target].ToString() + "</span>";
                                    tmp.subitem = ("" + row["subitem"]).ToString();
                                    retValue.Add(tmp);
                                }

                            }
                        }
                    }
                    else
                    {
                        var distinctIds = configTable.AsEnumerable()
                                        .Select(s => new
                                        {
                                            targetValue = s.Field<string>(target),
                                        })
                                        .Distinct().OrderBy(x => x.targetValue);

                        var distinctSubItems = configTable.AsEnumerable()
                                         .Select(s => new
                                         {
                                             subGroupValue = s.Field<string>("SUBGROUP"),
                                         })
                                         .Distinct()
                                         .ToList().OrderBy(x => x.subGroupValue);

                        if (distinctIds != null && distinctIds.Count() > 0)
                        {
                            foreach (var item in distinctIds.ToList())
                            {
                                AdminItemConfiguration tmp = new AdminItemConfiguration();
                                tmp.item = item.targetValue;
                                List<string> distinctSubValues = new List<string>();
                                if (distinctSubItems != null && distinctSubItems.Count() > 0)
                                {
                                    foreach (var subItem in distinctSubItems.ToList())
                                    {

                                        var subValues = from subValue in configTable.AsEnumerable()
                                                        where
                                                            !string.IsNullOrEmpty(subItem.subGroupValue) &&
                                                            subValue.Field<string>(target) == item.targetValue &&
                                                            subValue.Field<string>("SUBGROUP") == subItem.subGroupValue
                                                        select subValue.Field<string>(subItem.subGroupValue);


                                        if (subValues != null)
                                            distinctSubValues = subValues.Distinct().ToList().Cast<string>().ToList();
                                        tmp.releatedItems.Add(subItem.subGroupValue.ToString(), distinctSubValues);
                                    }

                                }

                                retValue.Add(tmp);
                            }
                        }
                    }
                }
            }

            return retValue;
        }

        public IEnumerable<erdaComboBoxItem> GetAvailableItems(string target, string subitem, string value, string filter)
        {

            IEnumerable<erdaComboBoxItem> retValue = new List<erdaComboBoxItem>();
            string retContent = "";
            string scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\AvailableItems-" + target + "-" + subitem + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\AvailableItems-" + target + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\AvailableItems.eplms");

            if (System.IO.File.Exists(scriptFullName))
            {
                retContent = System.IO.File.ReadAllText(scriptFullName);
                DataTable configTable = ePortalHelper.ExecuteQuery(ref _connection, retContent, new string[4] { target, subitem, value, filter }, null);
                if (configTable != null && configTable.Rows.Count > 0)
                {

                    retValue = new ExtensionsMapper<erdaComboBoxItem>()
                        .SetDataTable(configTable)
                        .SetPrefix("")
                        .Map();
                }
            }
            return retValue;
        }
        public IEnumerable<erdaComboBoxItem> GetAvailableUsers(string target, string subitem, string filter)
        {

            IEnumerable<erdaComboBoxItem> retValue = new List<erdaComboBoxItem>();
            string retContent = "";
            string scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\AvailableItems-" + target + "-" + subitem + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\AvailableItems-" + target + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\AvailableItems.eplms");

            if (System.IO.File.Exists(scriptFullName))
            {
                retContent = System.IO.File.ReadAllText(scriptFullName);
                DataTable configTable = ePortalHelper.ExecuteQuery(ref _connection, retContent, new string[4] { target, subitem, "", filter }, null);
                if (configTable != null && configTable.Rows.Count > 0)
                {

                    retValue = new ExtensionsMapper<erdaComboBoxItem>()
                        .SetDataTable(configTable)
                        .SetPrefix("")
                        .Map();
                }
            }
            return retValue;
        }


        public bool UpdateAdminItems(AdminDataConfig data)
        {
            if (data.target == "project")
                eRdAHelper.UpdateItemOnProject(data, ref _connection);
            else
                eRdAHelper.UpdateItemOnList(data, ref _connection);

            return true;
        }
        public bool UpdateAdminItemWithItems(AdminDataConfig data)
        {
            bool retValue = true;
            if (data != null)
            {
                if (data.subItem != "wbs")
                    eRdAHelper.UpdateItemOnMultiList(data, ref _connection);
                else
                    eRdAHelper.UpdateWBS(data, ref _connection);



            }
            return true;
        }


        public List<AdminItemConfiguration> GetAdminConfigItems(string target, string subitem, string filter)
        {
            List<AdminItemConfiguration> retValue = new List<AdminItemConfiguration>();
            string retContent = "";
            string scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\RdAConfigurationItems-" + target + "-" + subitem + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\RdAConfigurationItems-" + target + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\RdAConfigurationItems.eplms");

            if (System.IO.File.Exists(scriptFullName))
            {
                retContent = System.IO.File.ReadAllText(scriptFullName);
                DataTable configTable = ePortalHelper.ExecuteQuery(ref _connection, retContent, new string[3] { target, subitem, filter }, null);
                if (configTable != null && configTable.Rows.Count > 0)
                {
                    if (subitem.ToLower().Equals("wbs"))
                    {
                        IEnumerable<WBSRow> data = new ExtensionsMapper<WBSRow>()
                         .SetDataTable(configTable)
                         .SetPrefix("")
                         .Map();
                        AdminItemConfiguration tmp = new AdminItemConfiguration();
                        tmp.wbs = data.ToList();
                        retValue.Add(tmp);
                    }
                    else
                    {
                        var distinctIds = new string[1] { filter };

                        var distinctSubItems = configTable.AsEnumerable()
                                             .Select(s => new
                                             {
                                                 subGroupValue = s.Field<string>("SUBGROUP"),
                                             })
                                             .Distinct()
                                             .ToList().OrderBy(x => x.subGroupValue);


                        AdminItemConfiguration tmp = new AdminItemConfiguration();
                        tmp.item = filter;
                        List<string> distinctSubValues = new List<string>();
                        if (distinctSubItems != null && distinctSubItems.Count() > 0)
                        {
                            foreach (var subItem in distinctSubItems.ToList())
                            {

                                var subValues = from subValue in configTable.AsEnumerable()
                                                where
                                                    !string.IsNullOrEmpty(subItem.subGroupValue) &&
                                                    subValue.Field<string>(target) == filter &&
                                                    subValue.Field<string>("SUBGROUP") == subItem.subGroupValue
                                                select subValue.Field<string>(subItem.subGroupValue);


                                if (subValues != null)
                                    distinctSubValues = subValues.Distinct().ToList().Cast<string>().ToList();
                                tmp.releatedItems.Add(subItem.subGroupValue.ToString().ToUpper(), distinctSubValues);
                            }

                        }

                        retValue.Add(tmp);
                    }

                }

            }


            return retValue;
        }

        public List<AdminItemConfiguration> GetAdminProjectRoleUser(string role, string project, string competence)
        {
            List<AdminItemConfiguration> retValue = new List<AdminItemConfiguration>();
            string retContent = "";
            string scriptFullName = "";

            if (!string.IsNullOrEmpty(competence))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\roles\\RdARoleUser-" + role + "_" + competence + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\roles\\RdARoleUser-" + role + "_" + project + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\roles\\RdARoleUser-" + role + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\roles\\RdARoleUser-" + project + ".eplms");
            if (!System.IO.File.Exists(scriptFullName))
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\roles\\RdARoleUser.eplms");

            if (project.ToLower() == "users")
                scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\roles\\getallusers.eplms");

            if (System.IO.File.Exists(scriptFullName))
            {
                retContent = System.IO.File.ReadAllText(scriptFullName);
                DataTable configTable = ePortalHelper.ExecuteQuery(ref _connection, retContent, new string[2] { role, project }, null);
                // if (configTable != null && configTable.Rows.Count > 0)
                // {
                if (role.ToLower() == "planner" || role.ToLower() == "ur")
                {
                    AdminItemConfiguration tmp = new AdminItemConfiguration();
                    DataTable myCDC = ePortalHelper.ExecuteQuery(ref _connection, "select distinct CDC from MASAD_WBS where CDC IS NOT NULL and CDC <>'' order by CDC");

                    var distinctCDC = myCDC.AsEnumerable()
                                    .Select(s => new
                                    {
                                        targetValue = s.Field<string>("CDC"),
                                    })
                                    .Distinct().OrderBy(x => x.targetValue);

                    if (distinctCDC != null && distinctCDC.Count() > 0)
                    {
                        foreach (var cdc in distinctCDC.ToList())
                        {
                            if (string.IsNullOrEmpty(cdc.targetValue))
                                continue;
                            string value = "";
                            if (role.ToLower() == "planner")
                                value = eRdAHelper.GetPlannerRoleDBValue(cdc.targetValue.ToString());
                            else if (role.ToLower() == "ur")
                                value = eRdAHelper.GetURRoleDBValue(cdc.targetValue.ToString());

                            DataRow[] userrole = configTable.Select("role='" + value + "'");
                            List<string> username = new List<string>();
                            if (userrole != null && userrole.Count() > 0)
                            {

                                foreach (DataRow row in userrole)
                                    if (!username.Contains(row["user"].ToString()))
                                        username.Add(row["user"].ToString());


                            }
                            tmp.releatedItems.Add(cdc.targetValue.ToString(), username);
                        }
                    }
                    retValue.Add(tmp);


                }
                else if (role.ToLower() == "pr")
                {
                    AdminItemConfiguration tmp = new AdminItemConfiguration();
                    DataTable myCDC = null;
                    if (competence.ToUpper() == "POWERTRAIN")
                        myCDC = ePortalHelper.ExecuteQuery(ref _connection, "select 'POWERTRAIN' as CDC");
                    else
                        myCDC = ePortalHelper.ExecuteQuery(ref _connection, "select distinct CDC from MASAD_WBS where CDC IS NOT NULL and CDC <>'' order by CDC");
                    var distinctCDC = myCDC.AsEnumerable()
                                    .Select(s => new
                                    {
                                        targetValue = s.Field<string>("CDC"),
                                    })
                                    .Distinct().OrderBy(x => x.targetValue);

                    if (distinctCDC != null && distinctCDC.Count() > 0)
                    {
                        foreach (var cdc in distinctCDC.ToList())
                        {
                            if (string.IsNullOrEmpty(cdc.targetValue))
                                continue;
                            //string value = "PL_" + cdc.targetValue.ToString().Replace(" & ", "-").Replace(" ", "_");
                            DataRow[] pr = configTable.Select("CDC='" + cdc.targetValue + "'");
                            List<string> username = new List<string>();
                            if (pr != null && pr.Count() > 0)
                            {
                                // string competence = pr[0]["COMPENTECE"].ToString();
                                string formatValue = "{0}|{1}";
                                foreach (DataRow row in pr)
                                {
                                    string item = string.Format(formatValue, row["ACTIVITYNPI"].ToString(), row["APPROVER"].ToString());
                                    if (!username.Contains(item))
                                        username.Add(item);

                                }
                            }
                            tmp.releatedItems.Add(cdc.targetValue.ToString(), username);
                        }
                    }
                    retValue.Add(tmp);

                }
                else
                {
                    foreach (DataRow row in configTable.Rows)
                    {
                        AdminItemConfiguration tmp = new AdminItemConfiguration();
                        if (string.IsNullOrEmpty(role) && project.ToLower() == "projects")
                        {
                            tmp.item = row["project"].ToString();
                            tmp.subitem = row["competence"].ToString();
                        }
                        else
                            tmp.item = row["user"].ToString();
                        retValue.Add(tmp);
                    }
                }

                //}
            }
            return retValue;
        }

        public bool UpdateProjectUserRole(AdminDataConfig data)
        {
            bool retValue = true;
            eUser givenUser = null;
            if (data != null)
            {
                if ((data.updateItems != null && data.updateItems.Count > 0) || (data.deleteItems != null && data.deleteItems.Count > 0))
                {

                    eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref _connection);
                    try
                    {
                        //if (data.role.ToLower() == "pr" && data.competence!="POWERTRAIN")
                        //    eRdAHelper.ResetUserRoleList(ref dbaConnection, data.target, data.subItem, "");

                        givenUser = new eUser(ref dbaConnection);
                        if (data.updateItems != null && data.updateItems.Count > 0)
                        {
                            foreach (string value in data.updateItems)
                            {
                                if (string.IsNullOrEmpty(value))
                                    continue;
                                string userName = value;
                                data.activitynpi = "";
                                if (value.Contains("|"))
                                {
                                    data.activitynpi = userName.Split("|".ToCharArray())[0];
                                    userName = userName.Split("|".ToCharArray())[1];
                                    if (string.IsNullOrEmpty(data.activitynpi) || string.IsNullOrEmpty(userName))
                                        continue;
                                }
                                if (givenUser.Load(userName) == ePLMS_OK)
                                    eRdAHelper.AddUserRoleToProject(ref dbaConnection, givenUser, data.role, data.target, data.subItem, data.competence, data.activitynpi);
                            }

                        }
                        if (data.deleteItems != null && data.deleteItems.Count > 0)
                        {
                            foreach (string value in data.deleteItems)
                            {
                                if (string.IsNullOrEmpty(value))
                                    continue;
                                string userName = value;
                                data.activitynpi = "";
                                if (value.Contains("|"))
                                {
                                    data.activitynpi = userName.Split("|".ToCharArray())[0];
                                    userName = userName.Split("|".ToCharArray())[1];
                                    if (string.IsNullOrEmpty(data.activitynpi) || string.IsNullOrEmpty(userName))
                                        continue;
                                }
                                if (givenUser.Load(userName) == ePLMS_OK)
                                    eRdAHelper.RemoveUserRoleToProject(ref dbaConnection, givenUser, data.role, data.target, data.subItem, data.competence, data.activitynpi);
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        ePortalHelper.DestroyDbaConnection(ref _connection, ref dbaConnection);
                        throw e;
                    }

                    ePortalHelper.DestroyDbaConnection(ref _connection, ref dbaConnection);


                }

            }
            return true;
        }

        public List<RoleFullView> GetProjectRoleFullView()
        {

            DataTable dummyTable = eRdAExportHelper.GetProjectRoleFullView(ref _connection);
            IEnumerable<RoleFullView> data = new ExtensionsMapper<RoleFullView>()
                    .SetDataTable(dummyTable)
                    .SetPrefix("")
                    .Map();

            return data.ToList();

        }


        public SupplierManagement GetRemedtSupplierManagement(string year)
        {
            SupplierManagement retValue = new SupplierManagement();

            string retContent = "";
            string scriptFullName = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\SupplierManagement.eplms");
            string myCDC = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\cdc.eplms");
            string mySupplierList = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\remedysuppliers.eplms");
            if (string.IsNullOrEmpty(year)) year = DateTime.Now.Year.ToString();

            DataTable retData = new DataTable("remedy");

            if (System.IO.File.Exists(scriptFullName))
            {
                retContent = System.IO.File.ReadAllText(scriptFullName);
                DataTable totaleRdA = ePortalHelper.ExecuteQuery(ref _connection, retContent, new string[] { year }, null);
                retContent = System.IO.File.ReadAllText(myCDC);
                DataTable myTableCDC = ePortalHelper.ExecuteQuery(ref _connection, retContent, null, null);
                retContent = System.IO.File.ReadAllText(mySupplierList);
                DataTable mySupplierTable = ePortalHelper.ExecuteQuery(ref _connection, retContent, new string[] { year }, null);

                if (myTableCDC != null && myTableCDC.Rows.Count > 0)
                {
                    DataTable myRetTable = new DataTable();

                    DataColumn myNewGenericColoum = new DataColumn();
                    myNewGenericColoum.DataType = typeof(string);
                    myNewGenericColoum.ColumnName = "ID";
                    myRetTable.Columns.Add(myNewGenericColoum);

                    myNewGenericColoum = new DataColumn();
                    myNewGenericColoum.DataType = typeof(string);
                    myNewGenericColoum.ColumnName = "YEAR";
                    myRetTable.Columns.Add(myNewGenericColoum);


                    myNewGenericColoum = new DataColumn();
                    myNewGenericColoum.DataType = typeof(string);
                    myNewGenericColoum.ColumnName = "SupplierCode";
                    myRetTable.Columns.Add(myNewGenericColoum);

                    myNewGenericColoum = new DataColumn();
                    myNewGenericColoum.DataType = typeof(string);
                    myNewGenericColoum.ColumnName = "BusinessName";
                    myRetTable.Columns.Add(myNewGenericColoum);


                    foreach (DataRow cdc in myTableCDC.Rows)
                    {
                        string cdclabel = cdc[0].ToString().ToUpper();
                        retValue.CDC.Add(cdclabel);
                        cdclabel = eRdAHelper.GetPlannerRoleDBValue(cdclabel);
                        retValue.CDC_KEY.Add(cdclabel);

                        DataColumn myNewColoum = new DataColumn();
                        myNewColoum.DataType = typeof(double);
                        myNewColoum.ColumnName = cdclabel + "_allocated";
                        myNewColoum.DefaultValue = 0;
                        myRetTable.Columns.Add(myNewColoum);

                        myNewColoum = new DataColumn();
                        myNewColoum.DataType = typeof(double);
                        myNewColoum.ColumnName = cdclabel + "_residual";
                        myNewColoum.DefaultValue = 0;
                        myRetTable.Columns.Add(myNewColoum);

                        myNewColoum = new DataColumn();
                        myNewColoum.DataType = typeof(string);
                        myNewColoum.ColumnName = cdclabel + "_residual_cls";
                        myNewColoum.DefaultValue = "";
                        myRetTable.Columns.Add(myNewColoum);


                    }


                    if (mySupplierTable != null)
                    {
                        eRdAHelper.FIXYEAR(mySupplierTable, year, ref _connection);
                        for (int IDX = mySupplierTable.Rows.Count - 1; IDX >= 0; IDX += -1)
                        {
                            DataRow item = mySupplierTable.Rows[IDX];
                            DataRow myNewRow = myRetTable.NewRow();
                            myNewRow["ID"] = item["ID"];
                            myNewRow["YEAR"] = year;
                            myNewRow["SupplierCode"] = item["CODE"].ToString().Trim();
                            myNewRow["BusinessName"] = item["NAME"];
                            myNewRow["BusinessName"] = myNewRow["BusinessName"].ToString().Replace("    cod.forn.", "|");
                            myNewRow["BusinessName"] = myNewRow["BusinessName"].ToString().Split("|".ToCharArray())[0].Trim();
                            //myNewRow["Error"] = "";


                            if (item["Budget"] != null)
                            {
                                foreach (string budget in item["Budget"].ToString().Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                                {
                                    string area = budget.Split("#".ToCharArray())[0];
                                    string value = budget.Split("#".ToCharArray())[1];
                                    double currentValue = 0;
                                    currentValue = eRdAHelper.GetDoubleValue(value);
                                    DataRow[] AreaCheck = myTableCDC.Select("CDC='" + area + "'");
                                    if (AreaCheck.Length > 0)
                                    {
                                        string label = eRdAHelper.GetPlannerRoleDBValue(area);
                                        string myLabel = label + "_allocated";
                                        string myLabel2 = label + "_residual";
                                        string myLabel3 = label + "_residual_cls";
                                        if (myRetTable.Columns.Contains(myLabel))
                                            myNewRow[myLabel] = currentValue.ToString("N2");
                                        string myQuery = string.Format("CDC='{0}' and Supplier='{1}'", area, item["NAME"].ToString().ToLower());
                                        double totale = 0;
                                        try
                                        {
                                            DataRow[] myRows = totaleRdA.Select(myQuery);
                                            foreach (DataRow row in myRows)
                                            {
                                                double committed = eRdAHelper.GetDoubleValue(row["Committed"].ToString());
                                                double amount = eRdAHelper.GetDoubleValue(row["Amount"].ToString());
                                                if (committed > 0)
                                                    totale = totale + committed;
                                                else
                                                    totale = totale + amount;
                                            }
                                        }
                                        // totale = totaleRdA.Compute("Sum(Amount)", myQuery) ' "srno=1 or srno in(1,2)")
                                        catch (Exception ex)
                                        {
                                        }

                                        double residuo = currentValue - totale;
                                        myNewRow[myLabel2] = residuo.ToString("N2");
                                        if (residuo < 0 || (residuo == 0 && currentValue > 0))
                                            myNewRow[myLabel3] = "redLabel";

                                    }
                                }
                            }
                            myRetTable.Rows.InsertAt(myNewRow, 0);
                        }
                    }

                    retValue.supplierData = myRetTable;

                }
            }
            return retValue;
        }

        public List<Supplier> GetSupplierList()
        {
            string mySelect = "SELECT ID, NAME, ADDRESS, CITY, ZIPCODE, CODE, BUYER, IsRemedy, Budget, Brand, FCAGroup, ModifyDate, Consolidamento as Valid, ConsolidamentoFrom as ValidFrom, ConsolidamentoTo as ValidTo from[dbo].[FORNITORI] order by Name";

            DataTable result = ePortalHelper.ExecuteQuery(ref _connection, mySelect);

            IEnumerable<Supplier> data = new ExtensionsMapper<Supplier>()
                         .SetDataTable(result)
                         .SetPrefix("")
                         .Map();

            return data.ToList();

        }
        public List<SupplierPriceList> GetSupplierPriceList()
        {
            string mySelect = "SELECT [ID] " +
                            "      ,[ID Contratto] as idcontratto " +
                            "      ,[Fornitore] as fornitore " +
                            "      ,[Ragione Sociale] as fullname " +
                            "      ,[Stato] as stato " +
                            "      ,convert(datetime, [Data Attiviazione], 103) as attivazione " +
                            "      ,convert(datetime, [Data Scadenza], 103) as scadenza " +
                            "      ,[Pos]  as pos " +
                            "      ,[Articolo]  as articolo " +
                            "      ,[Descrizione] as descrizione " +
                            "      ,[Descrizione Completa] as descrizionecompleta " +
                            "      ,[Prezzo] as prezzo " +
                            "      ,[Descrizione Divisa] as divisa " +
                            "      ,[Stato Riga] as statoriga " +
                            "      ,[PWT COMPETENCE] as pwt " +
                            "      ,[Seq] as seq " +
                            "      ,convert(datetime, [Data Inizio Validità], 103) as startvalidita " +
                            "      ,convert(datetime, [Data Fine Validità], 103) as endvalidita " +
                            "      ,[ModifyDate] as modifydate " +
                            "  FROM[dbo].[LISTINO_FORNITORI] order by fornitore";

            DataTable result = ePortalHelper.ExecuteQuery(ref _connection, mySelect);

            IEnumerable<SupplierPriceList> data = new ExtensionsMapper<SupplierPriceList>()
                         .SetDataTable(result)
                         .SetPrefix("")
                         .Map();

            return data.ToList();

        }
        
        public bool UpdateRemedtSupplierManagement(SupplierManagementUppdate newBudgetValue)
        {

            string myCDC = System.IO.Path.Combine(_connection.ePLMS_HOME, "env\\scripts\\admin\\cdc.eplms");
            string retContent = System.IO.File.ReadAllText(myCDC);
            DataTable myTableCDC = ePortalHelper.ExecuteQuery(ref _connection, retContent, null, null);

            string mySelect = "SELECT ID, SUPPLIERID, YEAR, BUDGET FROM SUPPLIERBUDGETYEAR where SUPPLIERID=@Param1 and YEAR=@Param2";
            string myUpdate = "UPDATE SUPPLIERBUDGETYEAR SET BUDGET='@Param3' where SUPPLIERID=@Param1 and YEAR=@Param2";

            List<string> cdcList = new List<string>();
            if (myTableCDC != null && myTableCDC.Rows.Count > 0 && newBudgetValue != null)
            {
                Dictionary<string, string> keys = new Dictionary<string, string>();
                foreach (DataRow r in myTableCDC.Rows)
                {
                    string lbl = r["CDC"].ToString().ToUpper();
                    cdcList.Add(lbl + "#@" + lbl);
                    keys.Add(eRdAHelper.GetPlannerRoleDBValue(lbl.ToString()), lbl);

                }

                DataTable myBudget = ePortalHelper.ExecuteQuery(ref _connection, mySelect, new string[] { newBudgetValue.id, newBudgetValue.year }, null);
                string myBudgetValue = String.Join("|", cdcList.ToArray()).ToUpper();
                string dbValue = "";

                if (newBudgetValue.data != null && newBudgetValue.data.Length > 0)
                {
                    foreach (sm_dataToUppdate value in newBudgetValue.data)
                    {
                        if (keys.ContainsKey(value.cdc.ToUpper()))
                        {
                            string lbl = "@" + keys[value.cdc.ToUpper()];
                            string val = value.value.Replace(",", ".");
                            if (string.IsNullOrEmpty(val))
                                val = "0";
                            myBudgetValue = myBudgetValue.Replace(lbl, val);
                        }

                    }
                }

                if (myBudget != null && myBudget.Rows.Count > 0)
                {
                    dbValue = myBudget.Rows[0]["Budget"].ToString();
                    string[] singleValue = dbValue.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (singleValue.Length > 0)
                    {
                        foreach (string bg in singleValue)
                        {
                            if (bg.Contains("#"))
                            {

                                string lbl = "@" + bg.Split("#".ToCharArray())[0];

                                string val = bg.Split("#".ToCharArray())[1];
                                if (string.IsNullOrEmpty(val))
                                    val = "0";

                                myBudgetValue = myBudgetValue.Replace(lbl, val);
                            }
                        }
                    }
                }


                foreach (string key in keys.Keys)
                {
                    string lbl = "@" + keys[key];
                    string val = "0";
                    myBudgetValue = myBudgetValue.Replace(lbl, val);
                }
                ePortalHelper.ExecuteQuery(ref _connection, myUpdate, new string[] { newBudgetValue.id, newBudgetValue.year, myBudgetValue }, null);
            }

            return true;

        }

        public BudgetYears GetAllBudgetYear()
        {

            BudgetYears retValue = new BudgetYears();
            DataTable myTable = ePortalHelper.ExecuteQuery(ref _connection, "SELECT DISTINCT YEAR  FROM  SUPPLIERBUDGETYEAR", null, null);
            int current = DateTime.Now.Year;

            if (myTable != null && myTable.Rows.Count > 0)
            {
                foreach (DataRow r in myTable.Rows)
                    if (int.Parse(r[0].ToString()) < current)
                        retValue.older.Add(int.Parse(r[0].ToString()));
            }

            return retValue;
        }

        public bool AssignUserProjectRow(MultiAssignment data)
        {
            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref _connection);
            eUser givenUser;
            try
            {
                givenUser = new eUser(ref dbaConnection);
                if (data.projects != null && data.projects.Count > 0 && data.users != null && data.users.Count > 0)
                {
                    foreach (string project in data.projects)
                    {
                        foreach (string user in data.users)
                        {
                            if (givenUser.Load(user) == ePLMS_OK)
                                eRdAHelper.AddUserRoleToProject(ref dbaConnection, givenUser, data.role, project, "", "", "");
                        }


                    }

                }

            }
            catch (Exception e)
            {
                ePortalHelper.DestroyDbaConnection(ref _connection, ref dbaConnection);
                throw e;
            }

            ePortalHelper.DestroyDbaConnection(ref _connection, ref dbaConnection);
            return true;
        }
        public bool CheckRdABudget(int businessDataId)
        {

            bool retValue = true;
            string amount = "";
            string CDC = "";
            string supplier = "";
            string remedy = "";
            string activityEnd = "";
            string Level = "";

            eBusinessData.getAttribute(ref _connection, businessDataId, "Level", ref Level);
            if (Level.ToUpper() != "CLOSED")
            {
                eBusinessData.getAttribute(ref _connection, businessDataId, "RequestedSupplier", ref remedy);

                if (remedy.ToUpper() == "REMEDY")
                {
                    eBusinessData.getAttribute(ref _connection, businessDataId, "Supplier", ref supplier);
                    eBusinessData.getAttribute(ref _connection, businessDataId, "amount", ref amount);
                    eBusinessData.getAttribute(ref _connection, businessDataId, "CDC", ref CDC);
                    eBusinessData.getAttribute(ref _connection, businessDataId, "ActivityEnd", ref activityEnd);

                    if (activityEnd.Length > 4)
                    {
                        _connection.Core.dateToString(activityEnd, ref activityEnd);
                        activityEnd = activityEnd.Substring(0, 4);
                    }
                    DataTable myTableRDA = eRdAHelper.GetRdAOnRemedy(ref _connection, CDC, supplier, activityEnd);
                    double speso = 0;
                    if (myTableRDA != null && myTableRDA.Rows.Count > 0)
                    {
                        foreach (DataRow row in myTableRDA.Rows)
                        {
                            double commited = eRdAHelper.GetDoubleValue(row["Committed"].ToString());
                            double value = eRdAHelper.GetDoubleValue(row["Amount"].ToString());
                            if (commited > 0)
                                speso = speso + commited;
                            else
                                speso = speso + value;
                        }
                    }
                    string myQuery = "SELECT        ID, NAME, ADDRESS, CITY, ZIPCODE, CODE, BUYER, IsRemedy, Brand, " + " (SELECT BUDGET " +
                                    " FROM  SUPPLIERBUDGETYEAR	" +
                                    " WHERE        (SUPPLIERID IN (F.ID)) AND (YEAR = '" + activityEnd + "')) AS budget " +
                                    " FROM FORNITORI AS F	" +
                                    " WHERE        (IsRemedy = 1)	and Name='" + supplier + "'  ORDER BY NAME";


                    DataTable myTableSupplier = ePortalHelper.ExecuteQuery(ref _connection, myQuery); // "Select Budget from dbo.SUPPLIERBUDGETYEAR where Name='" & givenSupplier & "'")
                    if (myTableRDA != null & myTableSupplier != null)
                    {
                        if (myTableSupplier.Rows.Count > 0)
                        {
                            string myBudget = "" + myTableSupplier.Rows[0]["budget"].ToString();
                            foreach (string item in myBudget.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (item.ToLower().StartsWith(CDC.ToLower()))
                                {
                                    string allocated = item.Split("#".ToCharArray())[1];
                                    double value = eRdAHelper.GetDoubleValue(allocated);
                                    double residuo = value - speso - eRdAHelper.GetDoubleValue(amount);
                                    if (residuo < 0)
                                        retValue = false;
                                }
                            }
                        }
                    }
                }

            }
            return retValue;
        }

        #endregion

        #region "DASHBOARD / USER VIEW"



        public eUserDashboardData GetDashboardItems(string project = "", bool onlyCount = false)
        {
            eUserDashboardData retValue;

            DataTable dummyTable = null;
            eUserRdAProfile myUserProfile = eRdAHelper.GetUserProfile(ref _connection);
            List<string> myStringBuilder = new List<string>();
            string fileName = "projectlist";
            string userFilters = "";
            string basicQuery = "";
            if (!string.IsNullOrEmpty(project) && onlyCount == false)
                fileName = "objectsInProject";


            basicQuery = eRdAHelper.GetQueryContent(ref _connection, "Overview\\" + fileName + ".eplms", new string[] { _connection.currentUserName, project });


            if (!myUserProfile.HasFullView)
            {
                if (myUserProfile.IsRequester)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Overview\\Requester.filter", new string[] { _connection.currentUserName, project }));
                if (myUserProfile.IsPMP)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Overview\\PMP.filter", new string[] { _connection.currentUserName, project }));
                if (myUserProfile.IsPPM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Overview\\PPM.filter", new string[] { _connection.currentUserName, project }));
                if (myUserProfile.IsCSM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Overview\\CMS.filter", new string[] { _connection.currentUserName, project }));
                if (myUserProfile.IsPM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Overview\\PM.filter", new string[] { _connection.currentUserName, project }));
                if (myUserProfile.IsPR)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Overview\\PR.filter", new string[] { _connection.currentUserName, project }));
                if (myUserProfile.IsUR)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Overview\\UR.filter", new string[] { _connection.currentUserName, project }));
                if (myUserProfile.IsPlanner)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Overview\\PLANNER.filter", new string[] { _connection.currentUserName, project }));

                if (myStringBuilder.Count() > 0)
                    userFilters = String.Join(" or ", myStringBuilder.ToArray());
                else
                    userFilters = "(1=2)";
            }



            if (!string.IsNullOrEmpty(project) && onlyCount)
                basicQuery = basicQuery.Replace("@Cond1", "AND eProject = '" + project + "'  @Cond1");

            if (!string.IsNullOrEmpty(userFilters))
                userFilters = string.Format(" AND ({0})", userFilters);

            string queryToExecute = basicQuery.Replace("@Cond1", userFilters);
            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (string.IsNullOrEmpty(project) || (!string.IsNullOrEmpty(project) && onlyCount))
                retValue = eRdAHelper.GetProjectListCard(ref dummyTable);
            else
                retValue = eRdAHelper.GetObjectCardInProject(ref dummyTable, _connection.isCurrentUserDba);


            return retValue;
        }



        //public eUserDashboardData GetDashboardItems ORIGINAL()
        //{
        //    eUserDashboardData retValue = new eUserDashboardData();

        //    DataTable dummyTable = null;
        //    eUserRdAProfile myUserProfile = eRdAHelper.GetUserProfile(ref _connection);
        //    List<string> myStringBuilder = new List<string>();

        //    if (myUserProfile.HasFullView)
        //        myStringBuilder.Add(GetQueryContent("Dashboard\\FullView.eplms", new string[] { "" }));
        //    else
        //    {
        //        if (myUserProfile.IsRequester)
        //            myStringBuilder.Add(GetQueryContent("Dashboard\\Requester.eplms", new string[] { _connection.currentUserName }));
        //        if (myUserProfile.IsPMP)
        //            myStringBuilder.Add(GetQueryContent("Dashboard\\PMP.eplms", new string[] { _connection.currentUserName }));
        //        if (myUserProfile.IsPPM)
        //            myStringBuilder.Add(GetQueryContent("Dashboard\\PPM.eplms", new string[] { _connection.currentUserName }));
        //        if (myUserProfile.IsCSM)
        //            myStringBuilder.Add(GetQueryContent("Dashboard\\CMS.eplms", new string[] { _connection.currentUserName }));
        //        if (myUserProfile.IsPM)
        //            myStringBuilder.Add(GetQueryContent("Dashboard\\PM.eplms", new string[] { _connection.currentUserName }));
        //        if (myUserProfile.IsPR)
        //            myStringBuilder.Add(GetQueryContent("Dashboard\\PR.eplms", new string[] { _connection.currentUserName }));
        //        if (myUserProfile.IsUR)
        //            myStringBuilder.Add(GetQueryContent("Dashboard\\UR.eplms", new string[] { _connection.currentUserName }));
        //        if (myUserProfile.IsPlanner)
        //            myStringBuilder.Add(GetQueryContent("Dashboard\\PLANNER.eplms", new string[] { _connection.currentUserName }));
        //    }
        //    string queryToExecute = String.Join(" union all ", myStringBuilder.ToArray());
        //    if (queryToExecute.ToLower().Contains("eproject") && queryToExecute.ToLower().Contains("ecreatedate"))
        //        queryToExecute = queryToExecute + " order by eProject ASC,eCreateDate DESC";
        //    else if (queryToExecute.ToLower().Contains("eproject"))
        //        queryToExecute = queryToExecute + " order by eProject";

        //    dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

        //    if (dummyTable != null && dummyTable.Rows.Count > 0)
        //    {
        //        List<long> businessdatatosign = eRdAHelper.GetBusinessDataTaskEntry(ref _connection);
        //        DataTable projects = dummyTable.DefaultView.ToTable(true, "eProject");
        //        DataRow[] dummyRow = null;

        //        string status = "";
        //        List<eRdABasicView> erdaObjectsList = null;
        //        foreach (DataRow r in projects.Rows)
        //        {
        //            erdaObjectsList = null;
        //            eProjectRdaListView pj = new eProjectRdaListView();
        //            pj.ProjectName = r["eProject"].ToString();
        //            pj.RdAInWorking = "0";
        //            pj.RdAInValidation = "0";
        //            pj.RdAInCMSValidation = "0";
        //            pj.RdAInCreated = "0";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "Working"));
        //            pj.RdAInWorking = dummyRow.Length.ToString();
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            pj.RdAInWorkingList = erdaObjectsList;
        //            pj.RdAInWorkingStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "PR Validation"));
        //            pj.RdAInValidation = dummyRow.Length.ToString();
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            pj.RdAInValidationList = erdaObjectsList;
        //            pj.RdAInValidationStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "CMS Validation"));
        //            pj.RdAInCMSValidation = dummyRow.Length.ToString();
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            pj.RdAInCMSValidationList = erdaObjectsList;
        //            pj.RdAInCMSValidationStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and (eLevel='{1}' or eLevel='{2}')", pj.ProjectName, "Under Control", "Controlled"));
        //            pj.RdAInCreated = dummyRow.Length.ToString();
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            pj.RdAInCreatedList = erdaObjectsList;
        //            pj.RdAInCreatedStatus = status;

        //            if (pj.RdAInWorking != "0" || pj.RdAInValidation != "0" || pj.RdAInCMSValidation != "0" || pj.RdAInCreated != "0")
        //                retValue.projects.Add(pj);
        //        }
        //    }
        //    return retValue;
        //}

        //public eUserWorklistData GetWorkListItems()
        //{
        //    eUserWorklistData retValue = new eUserWorklistData();

        //    DataTable dummyTable = null;

        //    string queryToExecute = GetQueryContent("Worklist\\basicitem.eplms", new string[] { _connection.currentUserName });
        //    dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

        //    if (dummyTable != null)
        //    {
        //        List<long> businessdatatosign = eRdAHelper.GetBusinessDataTaskEntry(ref _connection);
        //        DataTable projects = dummyTable.DefaultView.ToTable(true, "eProject");
        //        DataRow[] dummyRow = null;

        //        string status = "";
        //        List<eRdABasicView> erdaObjectsList = null;
        //        foreach (DataRow r in projects.Rows)
        //        {
        //            erdaObjectsList = null;
        //            eProjectRdaListView pj = new eProjectRdaListView();
        //            pj.ProjectName = r["eProject"].ToString();
        //            pj.RdAInWorking = "0";
        //            pj.RdAInValidation = "0";
        //            pj.RdAInCMSValidation = "0";
        //            pj.RdAInCreated = "0";

        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "Working"));
        //            pj.RdAInWorking = dummyRow.Length.ToString();
        //            pj.totalRdA += dummyRow.Length;
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);

        //            pj.RdAInWorkingStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "PR Validation"));
        //            pj.RdAInValidation = dummyRow.Length.ToString();
        //            pj.totalRdA += dummyRow.Length;
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);
        //            pj.RdAInValidationStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "CMS Validation"));
        //            pj.RdAInCMSValidation = dummyRow.Length.ToString();
        //            pj.totalRdA += dummyRow.Length;
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);
        //            pj.RdAInCMSValidationStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and (eLevel='{1}' or eLevel='{2}')", pj.ProjectName, "Under Control", "Controlled"));
        //            pj.RdAInCreated = dummyRow.Length.ToString();
        //            pj.totalRdA += dummyRow.Length;
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);

        //            pj.RdAInCreatedStatus = status;

        //            if (pj.RdAInWorking != "0" || pj.RdAInValidation != "0" || pj.RdAInCMSValidation != "0" || pj.RdAInCreated != "0")
        //                retValue.projects.Add(pj);
        //        }
        //    }
        //    return retValue;
        //}

        public eUserArchivedData GetArchiveList(string project, int page, int pageSize)
        {

            eUserArchivedData retValue = new eUserArchivedData();
            eUserRdAProfile myUserProfile = eRdAHelper.GetUserProfile(ref _connection);
            string userFilters = "";
            DataTable dummyTable = null;
            List<string> myStringBuilder = new List<string>();

            if (!string.IsNullOrEmpty(project))
            {
                project = "and eProject='" + project + "'";
                string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, "Archive\\archivedonproject.eplms", new string[] { _connection.currentUserName, project });

                if (!myUserProfile.HasFullView)
                {
                    if (myUserProfile.IsRequester)
                        myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\Requester.filter", new string[] { _connection.currentUserName }));
                    if (myUserProfile.IsPMP)
                        myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PMP.filter", new string[] { _connection.currentUserName }));
                    if (myUserProfile.IsPPM)
                        myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PPM.filter", new string[] { _connection.currentUserName }));
                    if (myUserProfile.IsCSM)
                        myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\CMS.filter", new string[] { _connection.currentUserName }));
                    if (myUserProfile.IsPM)
                        myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PM.filter", new string[] { _connection.currentUserName }));
                    if (myUserProfile.IsPR)
                        myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PR.filter", new string[] { _connection.currentUserName }));
                    if (myUserProfile.IsUR)
                        myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\UR.filter", new string[] { _connection.currentUserName }));
                    if (myUserProfile.IsPlanner)
                        myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PLANNER.filter", new string[] { _connection.currentUserName }));
                    if (myStringBuilder.Count() > 0)
                        userFilters = String.Join(" or ", myStringBuilder.ToArray());
                    else
                        userFilters = "(1=2)";

                }


                if (!string.IsNullOrEmpty(userFilters))
                    userFilters = string.Format(" AND ({0})", userFilters);

                queryToExecute = queryToExecute.Replace("@Cond1", userFilters);

                dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

                if (dummyTable != null)
                {
                    //var rows = dummyTable.AsEnumerable();
                    //if (rows.Count() != 0)
                    //{
                    //    var dtResult = rows.Skip(pageSize * (page - 1)).Take(pageSize);
                    //    dummyTable = dtResult.CopyToDataTable();
                    //}

                    IEnumerable<eRdAArchiveView> data = new ExtensionsMapper<eRdAArchiveView>()
                         .SetDataTable(dummyTable)
                         .SetPrefix("")
                         .Map();

                    retValue.total = data.Count();
                    retValue.rdafullList = data.ToList();
                }
            }
            return retValue;
        }

        public eUserWorklistData GetArchiveProjectList()
        {
            eUserWorklistData retValue = new eUserWorklistData();
            eUserRdAProfile myUserProfile = eRdAHelper.GetUserProfile(ref _connection);
            string userFilters = "";
            DataTable dummyTable = null;
            List<string> myStringBuilder = new List<string>();

            string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, "Archive\\projectcounting.eplms", new string[] { _connection.currentUserName });

            if (!myUserProfile.HasFullView)
            {
                if (myUserProfile.IsRequester)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\Requester.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPMP)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PMP.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPPM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PPM.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsCSM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\CMS.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PM.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPR)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PR.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsUR)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\UR.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPlanner)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PLANNER.filter", new string[] { _connection.currentUserName }));

                if (myStringBuilder.Count() > 0)
                    userFilters = String.Join(" or ", myStringBuilder.ToArray());
                else
                    userFilters = "(1=2)";

            }

            if (myStringBuilder.Count() > 0)
                userFilters = String.Join(" or ", myStringBuilder.ToArray());


            if (!string.IsNullOrEmpty(userFilters))
                userFilters = string.Format(" AND ({0})", userFilters);

            queryToExecute = queryToExecute.Replace("@Cond1", userFilters);
            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (dummyTable != null && dummyTable.Rows.Count > 0)
            {
                DataTable projects = dummyTable.DefaultView.ToTable(true, "eProject");
                DataRow[] dummyRow = null;

                string status = "";
                List<eRdABasicView> erdaObjectsList = null;
                foreach (DataRow r in projects.Rows)
                {
                    erdaObjectsList = null;
                    eProjectRdaListView pj = new eProjectRdaListView();
                    pj.ProjectName = r["eProject"].ToString();
                    pj.RdAInWorking = "0";
                    pj.RdAInValidation = "0";
                    pj.RdAInCMSValidation = "0";
                    pj.RdAInCreated = "0";

                    dummyRow = dummyTable.Select(string.Format("eProject='{0}'", pj.ProjectName, "Working"));
                    // pj.totalRdA += dummyRow.Length;
                    foreach (DataRow subRow in dummyRow)
                    {
                        switch (subRow["eLevel"].ToString().ToUpper())
                        {
                            case "WORKING":
                                pj.RdAInWorking = subRow[2].ToString();
                                break;
                            case "PR VALIDATION":
                                pj.RdAInValidation = subRow[2].ToString();
                                break;
                            case "CMS VALIDATION":
                                pj.RdAInCMSValidation = subRow[2].ToString();
                                break;
                            case "UNDERCONTROL":
                            case "CONTROLLED":
                            case "CLOSED":
                                pj.RdAInCreated = subRow[2].ToString();
                                break;
                        }


                    }
                    retValue.projects.Add(pj);

                }
            }
            return retValue;
        }

        //public eUserWorklistData GeFastMaterialItems()
        //{
        //    eUserWorklistData retValue = new eUserWorklistData();

        //    DataTable dummyTable = null;
        //    string myQueryParam = _connection.isCurrentUserDba ? "(1=1)" : "eUser='" + _connection .currentUserName + "'";
        //    string queryToExecute = GetQueryContent("Worklist\\fastmaterials.eplms", new string[] { myQueryParam });
        //    dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

        //    if (dummyTable != null)
        //    {
        //        List<long> businessdatatosign = eRdAHelper.GetBusinessDataTaskEntry(ref _connection);
        //        DataTable projects = dummyTable.DefaultView.ToTable(true, "eProject");
        //        DataRow[] dummyRow = null;

        //        string status = "";
        //        List<eRdABasicView> erdaObjectsList = null;
        //        foreach (DataRow r in projects.Rows)
        //        {
        //            erdaObjectsList = null;
        //            eProjectRdaListView pj = new eProjectRdaListView();
        //            pj.ProjectName = r["eProject"].ToString();
        //            pj.RdAInWorking = "0";
        //            pj.RdAInValidation = "0";
        //            pj.RdAInCMSValidation = "0";
        //            pj.RdAInCreated = "0";

        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "Working"));
        //            pj.RdAInWorking = dummyRow.Length.ToString();
        //            pj.totalRdA += dummyRow.Length;
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);

        //            pj.RdAInWorkingStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "PR Validation"));
        //            pj.RdAInValidation = dummyRow.Length.ToString();
        //            pj.totalRdA += dummyRow.Length;
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);
        //            pj.RdAInValidationStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "CMS Validation"));
        //            pj.RdAInCMSValidation = dummyRow.Length.ToString();
        //            pj.totalRdA += dummyRow.Length;
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);
        //            pj.RdAInCMSValidationStatus = status;

        //            erdaObjectsList = null;
        //            status = "";
        //            dummyRow = dummyTable.Select(string.Format("eProject='{0}' and (eLevel='{1}' or eLevel='{2}')", pj.ProjectName, "Under Control", "Controlled"));
        //            pj.RdAInCreated = dummyRow.Length.ToString();
        //            pj.totalRdA += dummyRow.Length;
        //            eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //            if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);

        //            pj.RdAInCreatedStatus = status;

        //            if (pj.RdAInWorking != "0" || pj.RdAInValidation != "0" || pj.RdAInCMSValidation != "0" || pj.RdAInCreated != "0")
        //                retValue.projects.Add(pj);
        //        }
        //    }
        //    return retValue;
        //}


        //public eUserWorklistData GetUserRdASigned(string optionalstr = "")
        //{
        //    eUserWorklistData retValue = new eUserWorklistData();

        //    DataTable dummyTable = null;

        //    string queryToExecute = GetQueryContent(string.Format("Worklist\\signed{0}.eplms",optionalstr), new string[] { _connection.currentUserName });
        //    dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

        //    if (dummyTable != null)
        //    {
        //        IEnumerable<eRdABasicView> data = new ExtensionsMapper<eRdABasicView>()
        //             .SetDataTable(dummyTable)
        //             .SetPrefix("")
        //             .Map();
        //        retValue.rdafullList = data.ToList();
        //        var result = dummyTable.AsEnumerable()
        //        .GroupBy(x => new {
        //            eproject = x.Field<string>("eProject"),
        //            elevel = x.Field<string>("eLevel")
        //        }).Select(grp => new
        //        {
        //            project = grp.Key.eproject,
        //            level = grp.Key.elevel,
        //            //NoOfRotten = grp.Count(x => x.Field<string>("Is Rotten") == "yes"),
        //            TotalNo = grp.Count()
        //        }).OrderBy(item => item.project).ToList();

        //        eProjectRdaListView pj = new eProjectRdaListView();

        //        for (var index = 0; index < result.Count(); index++)
        //        {
        //            var r = result[index];
        //            if (string.IsNullOrEmpty(pj.ProjectName))
        //                pj.ProjectName = r.project;

        //            switch (r.level.ToUpper())
        //            {
        //                case "WORKING":
        //                    pj.totalRdA += r.TotalNo;
        //                    pj.RdAInWorking = r.TotalNo.ToString();
        //                    break;
        //                case "PR VALIDATION":
        //                    pj.totalRdA += r.TotalNo;
        //                    pj.RdAInValidation = r.TotalNo.ToString();
        //                    break;
        //                case "CMS VALIDATION":
        //                    pj.totalRdA += r.TotalNo;
        //                    pj.RdAInCMSValidation = r.TotalNo.ToString();
        //                    break;
        //                case "UNDER CONTROL":
        //                case "CONTROLLED":
        //                case "CLOSED":
        //                    pj.totalRdA += r.TotalNo;
        //                    pj.RdAInCreated = r.TotalNo.ToString();
        //                    break;
        //            }
        //            if (index == result.Count() - 1)
        //                retValue.projects.Add(pj);
        //            else if (index < result.Count() - 2)
        //            {
        //                if (r.project.ToUpper() != result[index + 1].project.ToUpper())
        //                {
        //                    retValue.projects.Add(pj);
        //                    pj = new eProjectRdaListView();
        //                }

        //            }

        //        }


        //        //List<DataTable> tables = dummyTable.AsEnumerable()
        //        //           .GroupBy(row => new {
        //        //               eProject = row.Field<string>("eProject"),
        //        //               eLevel = row.Field<string>("eLevel")
        //        //           }).Select(g => g.eProject .CopyToDataTable()).ToList();

        //        //foreach (DataTable dummy in tables)
        //        //{
        //        //    eProjectRdaListView pj = new eProjectRdaListView();
        //        //    pj.ProjectName = dummy
        //        //}


        //        //List<long> businessdatatosign = eRdAHelper.GetBusinessDataTaskEntry(ref _connection);
        //        //DataTable projects = dummyTable.DefaultView.ToTable(true, "eProject");
        //        //DataRow[] dummyRow = null;

        //        //string status = "";
        //        //List<eRdABasicView> erdaObjectsList = null;
        //        //foreach (DataRow r in projects.Rows)
        //        //{
        //        //    erdaObjectsList = null;
        //        //    eProjectRdaListView pj = new eProjectRdaListView();
        //        //    pj.ProjectName = r["eProject"].ToString();
        //        //    pj.RdAInWorking = "0";
        //        //    pj.RdAInValidation = "0";
        //        //    pj.RdAInCMSValidation = "0";
        //        //    pj.RdAInCreated = "0";

        //        //    dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "Working"));
        //        //    pj.RdAInWorking = dummyRow.Length.ToString();
        //        //    pj.totalRdA += dummyRow.Length;
        //        //    //eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //        //    eRdAHelper.GetIds(ref _connection, ref dummyRow, ref erdaObjectsList, ref status);
        //        //    if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);

        //        //    pj.RdAInWorkingStatus = status;

        //        //    erdaObjectsList = null;
        //        //    status = "";
        //        //    dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "PR Validation"));
        //        //    pj.RdAInValidation = dummyRow.Length.ToString();
        //        //    pj.totalRdA += dummyRow.Length;
        //        //    //eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //        //    eRdAHelper.GetIds(ref _connection, ref dummyRow, ref erdaObjectsList, ref status);
        //        //    if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);
        //        //    pj.RdAInValidationStatus = status;

        //        //    erdaObjectsList = null;
        //        //    status = "";
        //        //    dummyRow = dummyTable.Select(string.Format("eProject='{0}' and eLevel='{1}'", pj.ProjectName, "CMS Validation"));
        //        //    pj.RdAInCMSValidation = dummyRow.Length.ToString();
        //        //    pj.totalRdA += dummyRow.Length;
        //        //    //eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //        //    eRdAHelper.GetIds(ref _connection, ref dummyRow, ref erdaObjectsList, ref status);
        //        //    if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);
        //        //    pj.RdAInCMSValidationStatus = status;

        //        //    erdaObjectsList = null;
        //        //    status = "";
        //        //    dummyRow = dummyTable.Select(string.Format("eProject='{0}' and (eLevel='{1}' or eLevel='{2}' or eLevel='{3}')", pj.ProjectName, "Under Control", "Controlled","Closed"));
        //        //    pj.RdAInCreated = dummyRow.Length.ToString();
        //        //    pj.totalRdA += dummyRow.Length;
        //        //    //eRdAHelper.GetIdsAndWorstCaseColorOnActionDate(ref _connection, ref dummyRow, ref businessdatatosign, ref erdaObjectsList, ref status);
        //        //    eRdAHelper.GetIds(ref _connection, ref dummyRow, ref erdaObjectsList, ref status);
        //        //    if (erdaObjectsList != null) pj.RdAList.AddRange(erdaObjectsList);

        //        //    pj.RdAInCreatedStatus = status;

        //        //    if (pj.RdAInWorking != "0" || pj.RdAInValidation != "0" || pj.RdAInCMSValidation != "0" || pj.RdAInCreated != "0")
        //        //        retValue.projects.Add(pj);
        //        //}
        //    }
        //    return retValue;
        //}

        public eUserWorklistData GetRdaList(eRdAViewType givenviewType, string optionalstr = "")
        {
            eUserWorklistData retValue = new eUserWorklistData();
            DataTable dummyTable = null;
            string queryScript = "";
            switch (givenviewType)
            {
                case eRdAViewType.TOSIGN:
                    queryScript = string.Format("Worklist\\basicitem{0}.eplms", optionalstr);
                    break;
                case eRdAViewType.FASTMATERIAL:
                    if (_connection.isCurrentUserDba)
                        queryScript = string.Format("Worklist\\fastmaterialsdba{0}.eplms", optionalstr);
                    else
                        queryScript = string.Format("Worklist\\fastmaterials{0}.eplms", optionalstr);

                    break;
                case eRdAViewType.MULTIMATERIAL:
                    if (_connection.isCurrentUserDba)
                        queryScript = string.Format("Worklist\\multimaterialsdba{0}.eplms", optionalstr);
                    else
                        queryScript = string.Format("Worklist\\multimaterials{0}.eplms", optionalstr);
                    break;
                case eRdAViewType.SIGNED:
                    queryScript = string.Format("Worklist\\signed{0}.eplms", optionalstr);
                    break;

            }

            string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, queryScript, new string[] { _connection.currentUserName });
            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (dummyTable != null)
            {
                IEnumerable<eRdABasicView> data = new ExtensionsMapper<eRdABasicView>()
                     .SetDataTable(dummyTable)
                     .SetPrefix("")
                     .Map();
                retValue.rdafullList = data.ToList();

                var result = dummyTable.AsEnumerable()
                .GroupBy(x => new
                {
                    eproject = x.Field<string>("eProject"),
                    elevel = x.Field<string>("eLevel"),
                    edelay = x.Field<int>("delay")
                }).Select(grp => new
                {
                    project = grp.Key.eproject,
                    level = grp.Key.elevel,
                    delay = grp.Key.edelay,
                    TotalNo = grp.Count()
                }).OrderBy(item => item.project).ToList();

                for (var index = 0; index < result.Count(); index++)
                {
                    var r = result[index];
                    eProjectRdaListView pj = new eProjectRdaListView();
                    pj.ProjectName = r.project;
                    for (var index2 = index; index2 < result.Count(); index2++)
                    {
                        var r2 = result[index2];
                        if (r2.project != r.project)
                        {
                            index = index - 1;
                            if (index < result.Count() - 1)
                                retValue.projects.Add(pj);
                            break;
                        }
                        index += 1;
                        switch (r2.level.ToUpper())
                        {
                            case "WORKING":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayReq)
                                    pj.delayReq = r2.delay;
                                pj.RdAInWorkingStatus = eRdAHelper.GetDelayClass(pj.delayReq);
                                pj.RdAInWorking = (int.Parse(pj.RdAInWorking) + r2.TotalNo).ToString();
                                break;
                            case "PR VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayPR)
                                    pj.delayPR = r2.delay;
                                pj.RdAInValidationStatus = eRdAHelper.GetDelayClass(pj.delayPR);
                                pj.RdAInValidation = (int.Parse(pj.RdAInValidation) + r2.TotalNo).ToString();
                                break;
                            case "CMS VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayCMS)
                                    pj.delayCMS = r2.delay;
                                pj.RdAInCMSValidationStatus = eRdAHelper.GetDelayClass(pj.delayCMS);
                                pj.RdAInCMSValidation = (int.Parse(pj.RdAInCMSValidation) + r2.TotalNo).ToString();
                                break;
                            case "UNDER CONTROL":
                            case "CONTROLLED":
                            case "CLOSED":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayContr)
                                    pj.delayContr = r2.delay;
                                pj.RdAInCreatedStatus = eRdAHelper.GetDelayClass(pj.delayContr);
                                pj.RdAInCreated = (int.Parse(pj.RdAInCreated) + r2.TotalNo).ToString();
                                break;
                        }
                    }
                    if (index == result.Count())
                        retValue.projects.Add(pj);
                }
            }
            return retValue;
        }


        public eUserArchivedData GeteRdaSearch(List<KeyValuePair<string, string>> parameters)
        {
            eUserArchivedData retValue = new eUserArchivedData();
            eUserRdAProfile myUserProfile = eRdAHelper.GetUserProfile(ref _connection);
            string userFilters = "";
            DataTable dummyTable = null;
            List<string> myStringBuilder = new List<string>();

            string queryOnParams = "";
            string[] additionalParams = eRdAHelper.PrepareFind(ref _connection, parameters, ref queryOnParams);

            if (additionalParams != null && additionalParams.Length > 0)
            {
                for (int i = 0; i < additionalParams.Length; i++)
                {
                    string value = additionalParams[i];
                    string toreplace = "= {" + (i + 1).ToString() + "}";
                    if (value.Contains("*") || value.Contains("%"))
                        queryOnParams = queryOnParams.Replace(toreplace, "like '" + value.Replace("*", "%") + "'");
                    else
                        queryOnParams = queryOnParams.Replace(toreplace, "= '" + value + "'");
                }
            }
            if (!string.IsNullOrEmpty(queryOnParams))
                queryOnParams = " and " + queryOnParams;

            retValue.filter = queryOnParams;
            string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, "Archive\\archivedonproject.eplms", new string[] { _connection.currentUserName, queryOnParams });

            if (!myUserProfile.HasFullView)
            {
                if (myUserProfile.IsRequester)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\Requester.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPMP)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PMP.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPPM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PPM.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsCSM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\CMS.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PM.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPR)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PR.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsUR)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\UR.filter", new string[] { _connection.currentUserName }));
                if (myUserProfile.IsPlanner)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref _connection, "Archive\\filters\\PLANNER.filter", new string[] { _connection.currentUserName }));
                if (myStringBuilder.Count() > 0)
                    userFilters = String.Join(" or ", myStringBuilder.ToArray());
                else
                    userFilters = "(1=2)";
            }


            if (!string.IsNullOrEmpty(userFilters))
                userFilters = string.Format(" AND ({0})", userFilters);

            queryToExecute = queryToExecute.Replace("@Cond1", userFilters);

            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (dummyTable != null)
            {

                IEnumerable<eRdAArchiveView> data = new ExtensionsMapper<eRdAArchiveView>()
                     .SetDataTable(dummyTable)
                     .SetPrefix("")
                     .Map();

                retValue.total = data.Count();
                retValue.rdafullList = data.ToList();
            }
            return retValue;
        }




        #endregion

        #region "RDA ACTION"
        public eRdA GetRDA(int id)
        {
            eRdA retValue = new eRdA();
            eBusinessData myRdA = new eBusinessData(ref _connection);
            ThrowError = myRdA.Load(id);
            Mapper.Map<eRdA>(myRdA, retValue);
            return retValue;

        }
        public eRdA DeleteRDA(int id)
        {
            eRdA retValue = new eRdA();
            var dbaConnection = ePortalHelper.GetDbaConnection(ref _connection);
            eBusinessData dummyRdA = new eBusinessData(ref dbaConnection);
            try
            {

                ArrayList givenAttributes = new ArrayList();
                ThrowError = dummyRdA.Load(id);
                string myActualWF = dummyRdA.Workflow;


                if (myActualWF == eRdADefs.WorkflowMasterSpending)
                {
                    eWorkflowEngine myWF = new eWorkflowEngine(ref dbaConnection, dummyRdA.Id);
                    myWF.reassignLevel("Cancelled", "Cancelled by " + _connection.currentUserName, true);

                    eRdAWorkflowAction givenWorkflowAction = new eRdAWorkflowAction();
                    givenWorkflowAction.businessDataId = dummyRdA.Id;
                    givenWorkflowAction.actionType = eRdAWFActionType.APPROVE;
                    eRdAHelper.BusinessDataPerformTask(ref dbaConnection, givenWorkflowAction);
                    //workflowTask currentTask = myWF.getCurrentWorkflowTask();
                    //if (currentTask != null) currentTask.performTask();
                    eRdAHelper.CancelSubTranche(ref dbaConnection, ref _connection, dummyRdA);
                }
                else
                {
                    dummyRdA.Reserve();
                    dummyRdA.Workflow = "WF_CANCEL";
                    dummyRdA.Modify(ref givenAttributes);
                    dummyRdA.Unreserve();

                    eRdAWorkflowAction givenWorkflowAction = new eRdAWorkflowAction();
                    givenWorkflowAction.businessDataId = dummyRdA.Id;
                    givenWorkflowAction.actionType = eRdAWFActionType.APPROVE;
                    eRdAHelper.BusinessDataPerformTask(ref dbaConnection, givenWorkflowAction);

                    //eWorkflowEngine myWF = new eWorkflowEngine(ref dbaConnection, dummyRdA.Id);
                    //workflowTask currentTask = myWF.getCurrentWorkflowTask();
                    //if (currentTask != null) currentTask.performTask();
                }
                string refDate = DateTime.Now.ToString("yyyyMMdd000000");
                dbaConnection.Core.stringToDate(refDate, ref refDate);
                refDate = refDate.Split(" ".ToCharArray())[0];
                dummyRdA.changeAttribute("ActionDate", refDate);
                dummyRdA.changeAttribute("ActionComment", "Cancelled by " + _connection.currentUserName);

            }
            catch (Exception ex)
            {
                DestroyDbAdminConnection(ref _connection, ref dbaConnection);
                throw new Exception(ex.Message);
            }
            DestroyDbAdminConnection(ref _connection, ref dbaConnection);

            eBusinessData myRdA = new eBusinessData(ref _connection);
            ThrowError = myRdA.Load(id);
            Mapper.Map<eRdA>(myRdA, retValue);
            return retValue;
        }

        public eRdA CloneRDA(int id)
        {
            eRdA retValue = new eRdA();
            eBusinessData myeBusinessDataNew = new eBusinessData(ref _connection);
            eBusinessData myeBusinessData = new eBusinessData(ref _connection);
            ThrowError = myeBusinessData.Load(id);
            eSaveasCriteria myeSaveAsCriteria = new eSaveasCriteria();


            myeSaveAsCriteria.businessDataAttributeFlag = true;
            myeSaveAsCriteria.businessDataFileFlag = true;
            myeSaveAsCriteria.businessDataRelationFlag = true;
            myeSaveAsCriteria.businessDataLineFlag = true;
            myeBusinessDataNew.businessDataType = myeBusinessData.businessDataType;
            myeBusinessDataNew.Name = "#";
            myeBusinessDataNew.Revision = myeBusinessData.Revision;
            myeBusinessDataNew.Description = myeBusinessData.Description;
            myeBusinessDataNew.Workflow = myeBusinessData.Workflow;

            myeBusinessDataNew.Project = myeBusinessData.Project;

            ArrayList arrayList = new ArrayList();
            ThrowError = myeBusinessDataNew.Create(ref arrayList);
            myeBusinessDataNew.Reserve();
            ArrayList myAttributeList = new ArrayList();
            myeBusinessData.getAttributeList(ref myAttributeList);
            if (myAttributeList != null && myAttributeList.Count > 0)
            {
                //foreach (eDataListElement edata in myAttributeList)
                for (int i = 0; i < myAttributeList.Count; i++)
                {
                    eDataListElement edata = (eDataListElement)myAttributeList[i];
                    if ("ProjectName;ActionOperation;ActionDate;ActionComment;BaanCodeWriter".Contains(edata.Name))
                        continue;
                    if (edata.Type == "DATE")
                    {
                        if (edata.Value.Contains(" "))
                            edata.Value = edata.Value.Split(" ".ToCharArray())[0];
                        else
                        {
                            string date = "";
                            _connection.Core.dateToString(edata.Value, ref date);
                            edata.Value = date;
                        }
                    }
                    myeBusinessDataNew.changeAttribute(edata.Name, edata.Value);
                }
                myeBusinessDataNew.changeAttribute("ProjectName", myeBusinessDataNew.Project);
                myeBusinessDataNew.changeAttribute("ActionOperation", "");
                string actionDate = DateTime.Now.ToString("yyyyMMdd000000");
                _connection.Core.stringToDate(actionDate, ref actionDate);
                actionDate = actionDate.Split(" ".ToCharArray())[0];
                myeBusinessDataNew.changeAttribute("ActionDate", actionDate);
                myeBusinessDataNew.changeAttribute("ActionComment", "");
                myeBusinessDataNew.changeAttribute("BaanCodeWriter", "");
                myeBusinessDataNew.changeAttribute("BaanCodeWriteDate", "");
            }
            myeBusinessDataNew.Unreserve();
            Mapper.Map<eRdA>(myeBusinessDataNew, retValue);
            return retValue;
        }

        public eRdASigner GetRDASigner(int id)
        {


            eRdASigner retValue = new eRdASigner();
            string level = "";
            string cbs = "";
            string competence = "";
            string queryString = "SELECT        eplms.eBusinessDataTaskEntry.eUser " +
                                " FROM eplms.eBusinessData INNER JOIN " +
                                " eplms.eBusinessDataTaskEntry ON eplms.eBusinessData.eId = eplms.eBusinessDataTaskEntry.eBusinessDataId " +
                                " AND eplms.eBusinessData.eCurrentTaskId = eplms.eBusinessDataTaskEntry.eTaskId " +
                                " WHERE(eplms.eBusinessData.eLevel = N'PR Validation') AND(eplms.eBusinessData.eId = " + id + ")" +
                                " ORDER BY eplms.eBusinessDataTaskEntry.eUser";
            eBusinessData.getAttribute(ref _connection, id, "CBS", ref cbs);
            eBusinessData.getAttribute(ref _connection, id, "RDACompetence", ref competence);
            eBusinessData.getAttribute(ref _connection, id, "level", ref level);
            if (level.ToUpper() == "PR VALIDATION")
            {
                retValue.Role = "PR";
                retValue.Users = "";
                if ((competence.ToUpper() == "VEHICLE" || competence.ToUpper() == "POWERTRAIN") && (cbs.ToUpper() == "R&D HOURS" || cbs.ToUpper() == "WORK PACKAGE"))
                {
                    retValue.Role = "PLANNER";
                }
                DataTable myDataTable = ePortalHelper.ExecuteQuery(ref _connection, queryString);
                if (myDataTable != null && myDataTable.Rows.Count > 0)
                {

                    foreach (DataRow row in myDataTable.Rows)
                    {
                        if (retValue.Users != "")
                            retValue.Users = retValue.Users + " - ";
                        retValue.Users = retValue.Users + row[0];
                    }
                }
                else
                    retValue.Users = "?";
            }

            return retValue;

        }
        public eRdAWorkflowResult BusinessDataPerformTask(eRdAWorkflowAction action)
        {
            eRdAWorkflowResult retvalue = null;
            if (action.actionType.ToString().ToLower() == "reject" && action.backCMS)
                retvalue = eRdAHelper.BusinessDataPerformRejectToCMS(ref _connection, action);
            else
                retvalue = eRdAHelper.BusinessDataPerformTask(ref _connection, action);
            if (retvalue.result == false)
            {
                if (string.IsNullOrEmpty(retvalue.message))
                    retvalue.message = "Error happened during eRdA promotion! Please contact admin.";

                throw new Exception(retvalue.message);
            }
            return retvalue;
        }


        public eUserWorklistData GetBaaNRdaList(string givenviewType, string syncType)
        {
            eUserWorklistData retValue = new eUserWorklistData();
            DataTable dummyTable = null;
            string queryScript = "";

            queryScript = string.Format("BaaN\\{0}_{1}.eplms", givenviewType, syncType);


            string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, queryScript, new string[] { _connection.currentUserName }, new string[] { _connection.currentUserName });
            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (dummyTable != null)
            {
                if (givenviewType == "pes")
                    eRdAHelper.SetUpPESdefaultValue(ref _connection, ref dummyTable);
                else if (givenviewType == "prp")
                    eRdAHelper.SetUpPRPdefaultValue(ref _connection, ref dummyTable);

                IEnumerable<eRdABasicView> data = new ExtensionsMapper<eRdABasicView>()
                     .SetDataTable(dummyTable)
                     .SetPrefix("")
                     .Map();
                retValue.rdafullList = data.ToList();

                var result = dummyTable.AsEnumerable()
                .GroupBy(x => new
                {
                    eproject = x.Field<string>("eProject"),
                    elevel = x.Field<string>("eLevel"),
                    edelay = x.Field<int>("delay")
                }).Select(grp => new
                {
                    project = grp.Key.eproject,
                    level = grp.Key.elevel,
                    delay = grp.Key.edelay,
                    TotalNo = grp.Count()
                }).OrderBy(item => item.project).ToList();

                for (var index = 0; index < result.Count(); index++)
                {
                    var r = result[index];
                    eProjectRdaListView pj = new eProjectRdaListView();
                    pj.ProjectName = r.project;
                    for (var index2 = index; index2 < result.Count(); index2++)
                    {
                        var r2 = result[index2];
                        if (r2.project != r.project)
                        {
                            index = index - 1;
                            if (index < result.Count() - 1)
                                retValue.projects.Add(pj);
                            break;
                        }
                        index += 1;
                        switch (r2.level.ToUpper())
                        {
                            case "WORKING":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayReq)
                                    pj.delayReq = r2.delay;
                                pj.RdAInWorkingStatus = eRdAHelper.GetDelayClass(pj.delayReq);
                                pj.RdAInWorking = (int.Parse(pj.RdAInWorking) + r2.TotalNo).ToString();
                                break;
                            case "PR VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayPR)
                                    pj.delayPR = r2.delay;
                                pj.RdAInValidationStatus = eRdAHelper.GetDelayClass(pj.delayPR);
                                pj.RdAInValidation = (int.Parse(pj.RdAInValidation) + r2.TotalNo).ToString();
                                break;
                            case "CMS VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayCMS)
                                    pj.delayCMS = r2.delay;
                                pj.RdAInCMSValidationStatus = eRdAHelper.GetDelayClass(pj.delayCMS);
                                pj.RdAInCMSValidation = (int.Parse(pj.RdAInCMSValidation) + r2.TotalNo).ToString();
                                break;
                            case "UNDER CONTROL":
                            case "CONTROLLED":
                            case "CLOSED":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayContr)
                                    pj.delayContr = r2.delay;
                                pj.RdAInCreatedStatus = eRdAHelper.GetDelayClass(pj.delayContr);
                                pj.RdAInCreated = (int.Parse(pj.RdAInCreated) + r2.TotalNo).ToString();
                                break;
                        }
                    }
                    if (index == result.Count())
                        retValue.projects.Add(pj);
                }
            }
            return retValue;
        }

        public eRdABasicView UpdateRdABaaNData(string givenviewType, eRdABasicView data)
        {

            string[] attributeToSync = new string[] { };
            if (givenviewType.ToUpper() == "PES")
                attributeToSync = new string[] { "BaanIn_ATC", "BaanIn_DataErosioneBudget", "Supplier", "BaanIn_Articolo", "Hours", "BaanIn_Buyer", "BaanIn_Conto", "BaanIn_CDC", "BaanIn_Commessa", "BaanIn_Magazzino", "BaanIn_Requester", "Purchasing" };
            else if (givenviewType.ToUpper() == "PRP")
                attributeToSync = new string[] { "BaanIn_Commessa", "BaanIn_Dimensione3", "BaanIn_Articolo", "BaanIn_DescArticolo", "BaanIn_Magazzino", "Supplier", "BaanIn_Acquisitore", "BaanIn_Pianificatore", "TotalQuantity", "PiecePrice", "PiecePriceFormat", "BaanIn_DataConsegnaPianificata", "BaanIn_Buyer", "BaanIn_RiferimentoA", "BaanIn_RiferimentoB", "BaanIn_RiferimentoC", "BaanIn_RigaTesto", "BaanIn_GruppoArticoli", "BaanIn_CommessaOrdinePRP", "BaanIn_Requester", "Purchasing" };

            eBusinessData rdaToBaan = new eBusinessData(ref _connection);
            ThrowError = rdaToBaan.Load(int.Parse(data.eId));
            ArrayList attributeList = new ArrayList();
            foreach (string attributeName in attributeToSync)
            {
                string propName = attributeName;
                if (givenviewType.ToUpper() == "PRP" && propName.ToLower().Equals("baanin_dimensione3"))
                    propName = "BaanIn_ATC";

                object value = eRdAHelper.GetPropValue(data, propName);
                eDataListElement edata = new eDataListElement();
                edata.Name = attributeName;
                if (value == null)
                    value = "";
                if (value.GetType().Equals(typeof(DateTime)))
                {
                    edata.Value = ((DateTime)value).ToString("yyyyMMdd000000");
                    _connection.Core.stringToDate(edata.Value, ref edata.Value);
                    edata.Value = edata.Value.Split(" ".ToCharArray())[0];

                }
                else if (value.GetType().Equals(typeof(double)))
                {
                    edata.Value = value.ToString().Replace(",", ".");
                }
                else
                    edata.Value = value.ToString();

                attributeList.Add(edata);
            }
            rdaToBaan.Reserve();
            try
            {
                ThrowError = rdaToBaan.Modify(ref attributeList);
            }
            catch (Exception ex)
            {
                string error = _connection.errorMessage;
                rdaToBaan.Unreserve();
                throw new Exception(error);
            }
            rdaToBaan.Unreserve();

            DataTable dummyTable = null;
            string queryScript = "";

            queryScript = string.Format("BaaN\\{0}.eplms", givenviewType);

            string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, queryScript, new string[] { rdaToBaan.Id.ToString() });
            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (dummyTable != null)
            {
                if (givenviewType == "pes")
                    eRdAHelper.SetUpPESdefaultValue(ref _connection, ref dummyTable);
                else if (givenviewType == "prp")
                    eRdAHelper.SetUpPRPdefaultValue(ref _connection, ref dummyTable);

                IEnumerable<eRdABasicView> dataRda = new ExtensionsMapper<eRdABasicView>()
                     .SetDataTable(dummyTable)
                     .SetPrefix("")
                     .Map();

                return (eRdABasicView)dataRda.ToArray()[0];

            }
            return data;
        }

        public eRdABaanAction SyncRdAToBaan(eRdABaanAction action)
        {
            eRdABaanAction retvalue = new eRdABaanAction();
            try
            {
                eRdAHelper.SyncDataToBaan(ref _connection, action);
            }
            catch (Exception e)
            {
                retvalue.result = false;
                retvalue.message = e.Message;
            }


            if (retvalue.result == false)
            {
                if (string.IsNullOrEmpty(retvalue.message))
                    retvalue.message = "Rrror occurred sending eRdA to BaaN! Please contact admin.";

                throw new Exception(retvalue.message);
            }

            return retvalue;
        }

        public eRdACloseAction FastClose(eRdACloseAction action)
        {
            eRdACloseAction retvalue = new eRdACloseAction();
            try
            {
                eRdAHelper.FastClose(ref _connection, action);
            }
            catch (Exception e)
            {
                retvalue.result = false;
                retvalue.message = e.Message;
            }


            if (retvalue.result == false)
            {
                if (string.IsNullOrEmpty(retvalue.message))
                    retvalue.message = "Error occurred closing the eRdA! Please contact admin.";

                throw new Exception(retvalue.message);
            }

            return retvalue;
        }


        public eRdAATCAction SaveeRdAATC(eRdAATCAction action)
        {
            eRdAATCAction retvalue = new eRdAATCAction();
            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref _connection);
            List<int> updated = new List<int>();
            try
            {

                eBusinessData myRdA = new eBusinessData(ref dbaConnection);
                foreach (int id in action.businessDataId)
                    if (myRdA.Load(id) == ePLMS_OK)
                    {
                        string rdaCBS = "";
                        string attribute = "BaanIn_ATC";
                        myRdA.getAttribute("CBS", ref rdaCBS);
                        myRdA.changeAttribute(attribute, action.atc);

                        if (rdaCBS.ToLower() == "materials")
                            attribute = "BaanIn_Dimensione3";
                        if (myRdA.changeAttribute(attribute, action.atc) == ePLMS_OK)
                            updated.Add(id);
                    }



            }
            catch (Exception e)
            {
                retvalue.result = false;
                retvalue.message = e.Message;
            }
            retvalue.businessDataId = updated.ToArray();
            ePortalHelper.DestroyDbaConnection(ref _connection, ref dbaConnection);

            if (retvalue.result == false)
            {
                if (string.IsNullOrEmpty(retvalue.message))
                    retvalue.message = "Error happer during Sending eRdA to BaaN! Please contact admin.";

                throw new Exception(retvalue.message);
            }





            return retvalue;
        }


        public eUserWorklistData GetFolderAR()
        {
            eUserWorklistData retValue = new eUserWorklistData();
            DataTable dummyTable = null;
            string queryScript = "";

            queryScript = string.Format("Worklist\\folderar.eplms", "");


            string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, queryScript, new string[] { _connection.currentUserName });
            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (dummyTable != null)
            {
                IEnumerable<eRdABasicView> data = new ExtensionsMapper<eRdABasicView>()
                     .SetDataTable(dummyTable)
                     .SetPrefix("")
                     .Map();
                retValue.rdafullList = data.ToList();

                var result = dummyTable.AsEnumerable()
                .GroupBy(x => new
                {
                    eproject = x.Field<string>("eProject"),
                    elevel = x.Field<string>("eLevel"),
                    edelay = x.Field<int>("delay")
                }).Select(grp => new
                {
                    project = grp.Key.eproject,
                    level = grp.Key.elevel,
                    delay = grp.Key.edelay,
                    TotalNo = grp.Count()
                }).OrderBy(item => item.project).ToList();

                for (var index = 0; index < result.Count(); index++)
                {
                    var r = result[index];
                    eProjectRdaListView pj = new eProjectRdaListView();
                    pj.ProjectName = r.project;
                    for (var index2 = index; index2 < result.Count(); index2++)
                    {
                        var r2 = result[index2];
                        if (r2.project != r.project)
                        {
                            index = index - 1;
                            if (index < result.Count() - 1)
                                retValue.projects.Add(pj);
                            break;
                        }
                        index += 1;
                        switch (r2.level.ToUpper())
                        {
                            case "WORKING":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayReq)
                                    pj.delayReq = r2.delay;
                                pj.RdAInWorkingStatus = eRdAHelper.GetDelayClass(pj.delayReq);
                                pj.RdAInWorking = (int.Parse(pj.RdAInWorking) + r2.TotalNo).ToString();
                                break;
                            case "PR VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayPR)
                                    pj.delayPR = r2.delay;
                                pj.RdAInValidationStatus = eRdAHelper.GetDelayClass(pj.delayPR);
                                pj.RdAInValidation = (int.Parse(pj.RdAInValidation) + r2.TotalNo).ToString();
                                break;
                            case "CMS VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayCMS)
                                    pj.delayCMS = r2.delay;
                                pj.RdAInCMSValidationStatus = eRdAHelper.GetDelayClass(pj.delayCMS);
                                pj.RdAInCMSValidation = (int.Parse(pj.RdAInCMSValidation) + r2.TotalNo).ToString();
                                break;
                            case "UNDER CONTROL":
                            case "CONTROLLED":
                            case "CLOSED":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayContr)
                                    pj.delayContr = r2.delay;
                                pj.RdAInCreatedStatus = eRdAHelper.GetDelayClass(pj.delayContr);
                                pj.RdAInCreated = (int.Parse(pj.RdAInCreated) + r2.TotalNo).ToString();
                                break;
                        }
                    }
                    if (index == result.Count())
                        retValue.projects.Add(pj);
                }
            }
            return retValue;
        }

        public eUserWorklistData GetSpendingCurves()
        {
            eUserWorklistData retValue = new eUserWorklistData();
            eUserRdAProfile myUserProfile = eRdAHelper.GetUserProfile(ref _connection);
            DataTable dummyTable = null;
            string queryScript = "";
            List<string> myStringBuilder = new List<string>();
            string fileName = "projectlist";
            string userFilters = "";

            //queryScript = string.Format("Worklist\\spendingcurves.eplms", "");
            if (_connection.isCurrentUserDba)
                queryScript = string.Format("Worklist\\spendingcurvesdba{0}.eplms", "");
            else
                queryScript = string.Format("Worklist\\spendingcurves{0}.eplms", "");

            string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, queryScript, new string[] { _connection.currentUserName });

            //if (!myUserProfile.HasFullView)
            //{
            //    if (myUserProfile.IsRequester)
            //        myStringBuilder.Add(GetQueryContent("Dashboard\\" + fileName + "\\Requester.filter", new string[] { _connection.currentUserName }));
            //    if (myUserProfile.IsPMP)
            //        myStringBuilder.Add(GetQueryContent("Dashboard\\" + fileName + "\\PMP.filter", new string[] { _connection.currentUserName }));
            //    if (myUserProfile.IsPPM)
            //        myStringBuilder.Add(GetQueryContent("Dashboard\\" + fileName + "\\PPM.filter", new string[] { _connection.currentUserName }));
            //    if (myUserProfile.IsCSM)
            //        myStringBuilder.Add(GetQueryContent("Dashboard\\" + fileName + "\\CMS.filter", new string[] { _connection.currentUserName }));
            //    if (myUserProfile.IsPM)
            //        myStringBuilder.Add(GetQueryContent("Dashboard\\" + fileName + "\\PM.filter", new string[] { _connection.currentUserName }));
            //    if (myUserProfile.IsPR)
            //        myStringBuilder.Add(GetQueryContent("Dashboard\\" + fileName + "\\PR.filter", new string[] { _connection.currentUserName }));
            //    if (myUserProfile.IsUR)
            //        myStringBuilder.Add(GetQueryContent("Dashboard\\" + fileName + "\\UR.filter", new string[] { _connection.currentUserName }));
            //    if (myUserProfile.IsPlanner)
            //        myStringBuilder.Add(GetQueryContent("Dashboard\\" + fileName + "\\PLANNER.filter", new string[] { _connection.currentUserName }));
            //}

            //if (myStringBuilder.Count() > 0)
            //    userFilters = String.Join(" or ", myStringBuilder.ToArray());

            //if (!string.IsNullOrEmpty(userFilters))
            //    userFilters = string.Format(" AND ({0})", userFilters);

            //queryToExecute = queryToExecute.Replace("@Cond1", userFilters);

            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (dummyTable != null)
            {
                //dummyTable.Columns.Add("eNameMaster");
                DataTable change = dummyTable.Clone();
                for (int x = 0; x < dummyTable.Rows.Count; x++)
                {
                    DataRow currentRow = change.NewRow();
                    currentRow.ItemArray = dummyTable.Rows[x].ItemArray.Copy();
                    string name = currentRow["eName"].ToString();
                    string colorref = "";
                    currentRow["rdaw1_spendingcurve"] = "<div class='divtable'>" + eRdAHelper.GetSpendingText(currentRow["rdaw1_spendingcurve_admin"].ToString(), currentRow["eId"].ToString(), currentRow["rdaw1_spendingcurve_totalAmount"].ToString(), currentRow["AmountFormat"].ToString(), currentRow["eLevel"].ToString(), (double)currentRow["BaanIn_DataExchange"], currentRow["BaanIn_ExchangeMessage"].ToString(), ref colorref) + "</div>";
                    currentRow["eName"] = name.Split("_".ToCharArray())[0];
                    bool skip = currentRow["eLevel"].ToString().ToUpper() == "CLOSED";

                    for (int y = x + 1; y < dummyTable.Rows.Count; y++)
                    {
                        if (currentRow["eKey1"].ToString() == dummyTable.Rows[y]["eKey1"].ToString())
                        {
                            x = y;
                            if (dummyTable.Rows[y]["eLevel"].ToString().ToUpper() != "CLOSED")
                                skip = false;
                            currentRow["rdaw1_spendingcurve"] += "<div class='divtable border'>" + eRdAHelper.GetSpendingText(dummyTable.Rows[y]["rdaw1_spendingcurve_admin"].ToString(), dummyTable.Rows[y]["eId"].ToString(), dummyTable.Rows[y]["rdaw1_spendingcurve_totalAmount"].ToString(), dummyTable.Rows[y]["AmountFormat"].ToString(), dummyTable.Rows[y]["eLevel"].ToString(), (double)dummyTable.Rows[y]["BaanIn_DataExchange"], dummyTable.Rows[y]["BaanIn_ExchangeMessage"].ToString(), ref colorref) + "</div>";
                        }
                        else
                            break;
                    }
                    if (!skip)
                        change.Rows.Add(currentRow);
                }
                dummyTable = change.Copy();
                change = null;
                IEnumerable<eRdABasicView> data = new ExtensionsMapper<eRdABasicView>()
                     .SetDataTable(dummyTable)
                     .SetPrefix("")
                     .Map();
                retValue.rdafullList = data.ToList();

                var result = dummyTable.AsEnumerable()
                .GroupBy(x => new
                {
                    eproject = x.Field<string>("eProject"),
                    elevel = x.Field<string>("eLevel"),
                    edelay = x.Field<int>("delay")
                }).Select(grp => new
                {
                    project = grp.Key.eproject,
                    level = grp.Key.elevel,
                    delay = grp.Key.edelay,
                    TotalNo = grp.Count()
                }).OrderBy(item => item.project).ToList();

                for (var index = 0; index < result.Count(); index++)
                {
                    var r = result[index];
                    eProjectRdaListView pj = new eProjectRdaListView();
                    pj.ProjectName = r.project;
                    for (var index2 = index; index2 < result.Count(); index2++)
                    {
                        var r2 = result[index2];
                        if (r2.project != r.project)
                        {
                            index = index - 1;
                            if (index < result.Count() - 1)
                                retValue.projects.Add(pj);
                            break;
                        }
                        index += 1;
                        switch (r2.level.ToUpper())
                        {
                            case "WORKING":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayReq)
                                    pj.delayReq = r2.delay;
                                pj.RdAInWorkingStatus = eRdAHelper.GetDelayClass(pj.delayReq);
                                pj.RdAInWorking = (int.Parse(pj.RdAInWorking) + r2.TotalNo).ToString();
                                break;
                            case "PR VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayPR)
                                    pj.delayPR = r2.delay;
                                pj.RdAInValidationStatus = eRdAHelper.GetDelayClass(pj.delayPR);
                                pj.RdAInValidation = (int.Parse(pj.RdAInValidation) + r2.TotalNo).ToString();
                                break;
                            case "CMS VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayCMS)
                                    pj.delayCMS = r2.delay;
                                pj.RdAInCMSValidationStatus = eRdAHelper.GetDelayClass(pj.delayCMS);
                                pj.RdAInCMSValidation = (int.Parse(pj.RdAInCMSValidation) + r2.TotalNo).ToString();
                                break;
                            case "UNDER CONTROL":
                            case "CONTROLLED":
                            case "CLOSED":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayContr)
                                    pj.delayContr = r2.delay;
                                pj.RdAInCreatedStatus = eRdAHelper.GetDelayClass(pj.delayContr);
                                pj.RdAInCreated = (int.Parse(pj.RdAInCreated) + r2.TotalNo).ToString();
                                break;
                        }
                    }
                    if (index == result.Count())
                        retValue.projects.Add(pj);
                }
            }
            return retValue;
        }

        public IEnumerable<eRdATranche> GetRdATranches(int id)
        {
            List<eRdATranche> retValue = new List<eRdATranche>();
            DataTable dummyTable = null;
            string queryScript = "";

            queryScript = string.Format("rda\\spendingcurves.eplms", "");
            string queryToExecute = eRdAHelper.GetQueryContent(ref _connection, queryScript, new string[] { id.ToString() });
            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, queryToExecute);

            if (dummyTable != null)
            {
                foreach (DataRow row in dummyTable.Rows)
                {
                    string[] data = row["rdaw1_spendingcurve_admin"].ToString().Split("|".ToCharArray());
                    double total = (double)row["rdaw1_spendingcurve_totalAmount"];
                    string format = row["AmountFormat"].ToString();

                    //1 | 100 | 04 / 11 / 2019    245000  euro
                    string colorRef = "";
                    eRdATranche myTranche = new eRdATranche();
                    myTranche.code = row["eName"].ToString();
                    myTranche.id = int.Parse(row["eId"].ToString());
                    myTranche.value = eRdAHelper.GetDoubleValue(data[1].ToString());
                    myTranche.invoiceDate = data[2].ToString();
                    myTranche.level = row["eLevel"].ToString();
                    myTranche.amountFormat = row["AmountFormat"].ToString();
                    myTranche.title = eRdAHelper.GetSpendingText(row["rdaw1_spendingcurve_admin"].ToString(), row["eId"].ToString(),
                                                                        row["rdaw1_spendingcurve_totalAmount"].ToString(),
                                                                        row["AmountFormat"].ToString(), row["eLevel"].ToString(),
                                                                        (double)row["BaanIn_DataExchange"], "", ref colorRef, false);

                    myTranche.colorStatus = colorRef;
                    retValue.Add(myTranche);

                }
            }
            return retValue.ToArray();
        }

        public eRdACosts GetRdACosts(int id)
        {

            eRdACosts retValue = new eRdACosts();

            string myQuery = " select																									" +
                            " 	eId,																									" +
                            " 	eKey1,																									" +
                            " 	elevel,																									" +
                            " 	ecreateuser as createuser,																				" +
                            " 	cbs,																									" +
                            "	requestedsupplier,																						" +
                            " 	supplier,																								" +
                            " 	rdaw1_professionalrole as professionalrole,																" +
                            " 	quantityonvehicle,																						" +
                            " 	totalquantity,																							" +
                            " 	pieceprice,																								" +
                            " 	piecepriceformat,																						" +
                            " 	[hours],																								" +
                            " 	dailycost,																								" +
                            " 	amount,																									" +
                            " 	amountformat,																							" +
                            "	(SELECT        COUNT(eplms.eProjectRole.eId) 															" +
                            "		FROM eplms.eProjectRole INNER JOIN																	" +
                            "				eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId INNER JOIN						" +
                            "               eplms.eProject ON eplms.eProjectRole.eProjectId = eplms.eProject.eId INNER JOIN				" +
                            "                eplms.eProjectRoleUser ON eplms.eProjectRole.eId = eplms.eProjectRoleUser.eProjectRoleId	" +
                            "		WHERE    																							" +
                            "				(eplms.eRole.eName = N'CMS') AND (eplms.eProject.eName = eProject) AND 						" +
                            "				(eplms.eProjectRoleUser.eUserId = {1})) as CMS										        " +
                            " from eplms.RDADB where eId={0}																		    ";

            myQuery = string.Format(myQuery, id, _connection.currentUserId);
            DataTable dummy = ePortalHelper.ExecuteQuery(ref _connection, myQuery);
            if (dummy != null)
            {
                IEnumerable<eRdACosts> listRda = new ExtensionsMapper<eRdACosts>()
                     .SetDataTable(dummy)
                     .SetPrefix("")
                     .Map();
                if (listRda.Count() > 0)
                {
                    retValue = listRda.ToList()[0];
                    if (string.IsNullOrEmpty(retValue.amountformat))
                        retValue.amountformat = "euro";
                    if (string.IsNullOrEmpty(retValue.piecepriceformat))
                        retValue.piecepriceformat = "euro";
                    if (!_connection.isCurrentUserDba)
                    {
                        if (!string.IsNullOrEmpty(retValue.eKey1) && retValue.cbs.ToLower() == "codesign")
                        {
                            retValue.block = true;
                            retValue.blockmessage = "Functionality cannot be activated on eRdA managed with tranches";
                        }
                        if (retValue.elevel.ToLower() == "working" && retValue.createuser.ToLower() != _connection.currentUserName.ToLower())
                        {
                            retValue.block = true;
                            retValue.blockmessage = "Functionality cannot be activated! User is not the Requester of current eRdA";
                        }
                        if (retValue.elevel.ToLower() == "cms validation" && retValue.CMS == 0)
                        {
                            retValue.block = true;
                            retValue.blockmessage = "Functionality cannot be activated! User is not the CMS of current eRdA";
                        }
                        if (retValue.elevel.ToLower() != "working" && retValue.elevel.ToLower() != "cms validation")
                        {
                            retValue.block = true;
                            retValue.blockmessage = "Functionality cannot be activated in current state of the eRdA";
                        }
                    }


                }

                if (retValue.requestedsupplier.ToLower() == "" || retValue.requestedsupplier.ToLower() == "to be sourced")
                    retValue.supplier = "To Be Sourced";
            }
            return retValue;
        }
        public bool UpdateRdACosts(eRdACosts rdacosts)
        {
            bool retValue = true;
            eBusinessData currenterda = new eBusinessData(ref _connection);
            ThrowError = currenterda.Load(rdacosts.eId);
            currenterda.Reserve();
            try
            {
                ArrayList myAttribute = new ArrayList();
                if (rdacosts.cbs.ToLower() == "materials")
                {
                    eDataListElement attribute = new eDataListElement();
                    attribute.Name = "QuantityOnVehicle";
                    attribute.Value = rdacosts.quantityonvehicle.ToString().Replace(",", ".");
                    myAttribute.Add(attribute);

                    attribute = new eDataListElement();
                    attribute.Name = "TotalQuantity";
                    attribute.Value = rdacosts.totalquantity.ToString().Replace(",", ".");
                    myAttribute.Add(attribute);

                    attribute = new eDataListElement();
                    attribute.Name = "PiecePrice";
                    attribute.Value = rdacosts.pieceprice.ToString().Replace(",", ".");
                    myAttribute.Add(attribute);

                    attribute = new eDataListElement();
                    attribute.Name = "PiecePriceFormat";
                    attribute.Value = rdacosts.piecepriceformat;
                    myAttribute.Add(attribute);
                }
                else
                {
                    currenterda.changeAttribute("AmountFormat", rdacosts.amountformat);
                    eDataListElement attribute = new eDataListElement();
                    attribute.Index = myAttribute.Count;
                    attribute.Name = "Hours";
                    attribute.Value = rdacosts.hours.ToString().Replace(",", ".");
                    myAttribute.Add(attribute);

                    //attribute = new eDataListElement();
                    //attribute.Index = myAttribute.Count;
                    //attribute.Name = "AmountFormat";
                    //attribute.Value = rdacosts.amountformat;
                    //myAttribute.Add(attribute);

                    attribute = new eDataListElement();
                    attribute.Index = myAttribute.Count;
                    attribute.Name = "Amount";
                    attribute.Value = rdacosts.amount.ToString().Replace(",", ".");
                    myAttribute.Add(attribute);

                }
                int rc = currenterda.Modify(ref myAttribute);
                if (rc != ePLMS_OK)
                    throw new Exception(currenterda.Connection.errorMessage);

            }
            catch (Exception e)
            {
                currenterda.Unreserve();
                throw new Exception(e.Message);
            }

            return retValue;

        }

        #endregion

        #region "combobox"
        public IEnumerable<erdaComboBoxItem> GetCombobox(string listitem, string filterparam)
        {
            IEnumerable<erdaComboBoxItem> data = null;
            string myQuery = "";
            DataTable dummyTable = null;

            switch (listitem.ToLower())
            {
                case "atc":
                    myQuery = "SELECT CODE as Value From ATC Where (CODE like '%{0}%')";
                    myQuery = string.Format(myQuery, filterparam);
                    dummyTable = ePortalHelper.ExecuteQuery(ref _connection, myQuery);
                    data = new ExtensionsMapper<erdaComboBoxItem>()
                     .SetDataTable(dummyTable)
                     .SetPrefix("")
                     .Map();
                    break;
                case "supplier":
                    myQuery = "SELECT NAME as Value From FORNITORI Where (Name like '%{0}%') and (Consolidamento = 1 and ConsolidamentoFrom<=GETDATE() and ConsolidamentoTo>=GETDATE()) order by Name";
                    myQuery = string.Format(myQuery, filterparam);
                    dummyTable = ePortalHelper.ExecuteQuery(ref _connection, myQuery);
                    data = new ExtensionsMapper<erdaComboBoxItem>()
                     .SetDataTable(dummyTable)
                     .SetPrefix("")
                     .Map();
                    break;
            }

            return data;
        }

        public IEnumerable<erdaComboBoxItem> GetComboboxWBS(string target, string path, string filterparam)
        {
            IEnumerable<erdaComboBoxItem> data = null;
            string myQuery = "Select distinct {0} as Value from MASAD_WBS where {0} like '%{1}%'";
            DataTable dummyTable = null;
            if (path != null)
            {
                string[] keys = new string[] { "COMPETENCE", "CDC", "AREA", "SYSTEM", "CBS", "WBS" };
                string[] pathValue = path.Split("\\".ToArray());
                List<string> where = new List<string>();
                for (int index = 0; index < pathValue.Length; index++)
                {
                    if (string.IsNullOrEmpty(pathValue[index]))
                        where.Add(string.Format("({0} IS NULL or {0}='')", keys[index]));
                    else
                        where.Add(string.Format("{0}='{1}'", keys[index], pathValue[index]));
                }

                if (where.Count > 0)
                    myQuery = myQuery + " AND " + string.Join(" AND ", where.ToArray());
            }
            myQuery = string.Format(myQuery, target, filterparam);

            dummyTable = ePortalHelper.ExecuteQuery(ref _connection, myQuery);
            data = new ExtensionsMapper<erdaComboBoxItem>()
                .SetDataTable(dummyTable)
                .SetPrefix("")
                .Map();

            return data;
        }

        public eRdAMaterialsFile GetMaterialsList(int id)
        {
            eRdAMaterialsFile retValue = new eRdAMaterialsFile();
            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref _connection);
            string query = "select eId,eName,DRAWINGCODE, MaterialDescription as MATERIALDESCRIPTION,CO_SP,QUANTITYONVEHICLE,PIECEPRICE,PIECEPRICEFORMAT " +
                           ",TOTALQUANTITY,REQUESTEDSUPPLIER,SUPPLIER,CODICEIMAN,SUPPLIERREFERENCE,REQUESTEDDELIVERYDATE " +
                           "from eplms.rdadb where eKey1 = @Param1";
            eBusinessData myMaster = new eBusinessData(ref dbaConnection);
            ThrowError = myMaster.Load(id);
            retValue.businessDataId = myMaster.Id;
            retValue.level = myMaster.Level;
            if (myMaster.Level.ToLower() == "completed")
            {
                DataTable myTable = ePortalHelper.ExecuteQuery(ref dbaConnection, query, new string[] { myMaster.Id.ToString() }, new string[] { "" });
                IEnumerable<eRdAMaterialsFileRow> data = new ExtensionsMapper<eRdAMaterialsFileRow>()
                        .SetDataTable(myTable)
                        .SetPrefix("")
                        .Map();


                retValue.rows = data.ToList();
                for (int i = 0; i < retValue.rows.Count(); i++)
                {
                    retValue.rows[i].NumRow = i;
                    retValue.rows[i].Sys_Error = "";
                }

            }
            return retValue;
        }


        public eRdAMaterialsFile GetMaterialImportContent(eRdAMaterialsFile data)
        {
            eRdAMaterialsFile retValue = new eRdAMaterialsFile();
            var str = data.filecontent.Replace("data:application/vnd.ms-excel;base64,", "");
            str = str.Replace("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,", "");
            byte[] bytes = System.Convert.FromBase64String(str);
            DataTable fornitori = ePortalHelper.ExecuteQuery(ref _connection, "select NAME from dbo.FORNITORI");
            DataTable attributi = ePortalHelper.ExecuteQuery(ref _connection, "select * from eplms.eAttribute");
            if (bytes.Length > 0)
            {
                MemoryStream stream = new MemoryStream(bytes);
                Workbook workbook = new Workbook(stream);
                Worksheet worksheet = workbook.Worksheets[0];

                worksheet.Cells.DeleteBlankRows();
                int maxRowCount = worksheet.Cells.Rows.Count;
                DataTable dTable = new DataTable();
                dTable = worksheet.Cells.ExportDataTableAsString(3, 0, maxRowCount - 3, 16, false);

                if (dTable != null && dTable.Rows.Count > 0)
                {
                    dTable.TableName = "multi";
                    //foreach (DataRow dummyRow in dTable.Rows)

                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        StringBuilder errors = new StringBuilder();
                        DataRow dummyRow = dTable.Rows[i];
                        eRdAMaterialsFileRow row = new eRdAMaterialsFileRow();
                        row.NumRow = i + 4;
                        //row.Sys_Error = "";
                        row.DRAWINGCODE = dummyRow[0].ToString();
                        if (string.IsNullOrEmpty(row.DRAWINGCODE))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Value cannot be null", "Drawing Code"));
                        if (!eRdAHelper.HasValidLength(ref attributi, "DRAWINGCODE", row.DRAWINGCODE))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Drawing Code"));

                        row.MATERIALDESCRIPTION = dummyRow[1].ToString();
                        if (string.IsNullOrEmpty(row.MATERIALDESCRIPTION))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Value cannot be null", "Comp.Description"));
                        if (!eRdAHelper.HasValidLength(ref attributi, "ComponentDescription", row.MATERIALDESCRIPTION))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Comp.Description"));


                        row.CO_SP = dummyRow[2].ToString();
                        if (row.CO_SP != "CO" && row.CO_SP != "SP")
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Wrong value", "CO / SP"));
                        if (!eRdAHelper.HasValidLength(ref attributi, "CO_SP", row.CO_SP))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "CO / SP"));

                        row.QUANTITYONVEHICLE = eRdAHelper.GetDoubleValue(dummyRow[3].ToString());
                        if (!eRdAHelper.HasValidLength(ref attributi, "QuantityOnVehicle", row.QUANTITYONVEHICLE.ToString()))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Quantity On Vehicle"));


                        string price = dummyRow[4].ToString();
                        row.PIECEPRICEFORMAT = "euro";
                        if (price.Contains("$"))
                            row.PIECEPRICEFORMAT = "dollar";
                        price = price.Replace("€", "").Trim();
                        price = price.Replace("$", "").Trim();

                        if (CultureInfo.CurrentCulture.Name.ToLower() == "it-it")
                        {
                            price = price.Replace(".", "").Trim();
                            price = price.Replace(",", ".").Trim();
                        }
                        else
                        {
                            price = price.Replace(",", "").Trim();
                            price = price.Replace(".", ".").Trim();
                        }


                        if (string.IsNullOrEmpty(price))
                            price = "0";
                        row.PIECEPRICE = eRdAHelper.GetDoubleValue(price);
                        if (!eRdAHelper.HasValidLength(ref attributi, "PiecePrice", row.PIECEPRICE.ToString()))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Piece Price"));


                        row.TOTALQUANTITY = eRdAHelper.GetDoubleValue(dummyRow[5].ToString());
                        if (!eRdAHelper.HasValidLength(ref attributi, "TotalQuantity", row.TOTALQUANTITY.ToString()))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Total Quantity On Vehicle"));



                        string tobesourced = dummyRow[6].ToString().ToUpper();
                        string suggested = dummyRow[7].ToString().ToUpper();
                        string supplier = dummyRow[8].ToString();
                        row.SUPPLIER = supplier;
                        if (!string.IsNullOrEmpty(supplier))
                        {
                            row.REQUESTEDSUPPLIER = "Suggested / Defined";
                            if (fornitori != null)
                            {
                                DataRow[] sups = fornitori.Select("NAME = '" + supplier + "'");
                                if (sups.Length != 1)
                                {
                                    errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Supplier not identified", "Supplier"));
                                }
                            }
                        }
                        else
                        {

                            row.REQUESTEDSUPPLIER = "To be Sourced";
                            if ((suggested.StartsWith("Y") || suggested.StartsWith("S")) && tobesourced.StartsWith("N"))
                            {
                                row.REQUESTEDSUPPLIER = "Suggested / Defined";
                                errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Supplier Name mandatory if value Suggested = Y", "Reqquested Supplier"));
                            }

                        }
                        if (!eRdAHelper.HasValidLength(ref attributi, "RequestedSupplier", row.REQUESTEDSUPPLIER))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Requested Supplier"));

                        if (!eRdAHelper.HasValidLength(ref attributi, "Supplier", row.SUPPLIER))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Supplier"));



                        row.CODICEIMAN = dummyRow[9].ToString();
                        if (string.IsNullOrEmpty(row.CODICEIMAN))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Iman Code cannot be null", "Iman Code"));

                        if (!eRdAHelper.HasValidLength(ref attributi, "CODICEIMAN", row.CODICEIMAN))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Iman Code"));


                        row.SUPPLIERREFERENCE = dummyRow[10].ToString();
                        if (!eRdAHelper.HasValidLength(ref attributi, "SUPPLIERREFERENCE", row.SUPPLIERREFERENCE))
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Bad value Length", "Supplier Reference"));
                        if (!string.IsNullOrEmpty(dummyRow[11].ToString()))
                        {
                            try
                            {
                                string[] dateValue = dummyRow[11].ToString().Split("/".ToCharArray());
                                if(dateValue[0].Length==4)
                                    row.REQUESTEDDELIVERYDATE = new DateTime(int.Parse(dateValue[0]), int.Parse(dateValue[1]), int.Parse(dateValue[2]));
                                else
                                    row.REQUESTEDDELIVERYDATE = new DateTime(int.Parse(dateValue[2]), int.Parse(dateValue[1]), int.Parse(dateValue[0]));
                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    DateTime myTymi = DateTime.Parse(dummyRow[11].ToString());
                                    row.REQUESTEDDELIVERYDATE = myTymi;
                                }
                                catch (Exception innerEx)
                                {
                                    errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Delivery date " + dummyRow[11].ToString() + " not valid format (dd/MM/yyyy)", "Delivery date"));
                                }


                            }

                        }
                        else
                            errors.AppendLine(eRdAHelper.GetMessageErrorBaanTable("Delivery date cannot be null", "Delivery date"));


                        if (errors.Length > 0)
                        {
                            row.Sys_Error = errors.ToString();
                            retValue.IsValidFile = false;
                        }

                        retValue.rows.Add(row);

                    }


                }


            }
            return retValue;
        }

        public eRdAMaterialsFile SaveMaterials(eRdAMaterialsFile data)
        {
            eRdAMaterialsFile retValue = new eRdAMaterialsFile();
            if (data.rows != null && data.rows.Count() > 0)
            {
                //throw new Exception("ole");
                eBusinessData myMaster = new eBusinessData(ref _connection);
                ArrayList myBaseAttribute = new ArrayList();
                ThrowError = myMaster.Load(data.businessDataId);
                myMaster.getAttributeList(ref myBaseAttribute);

                foreach (eRdAMaterialsFileRow row in data.rows)
                {
                    eBusinessData givenMaterial = new eBusinessData(ref _connection);
                    givenMaterial.Name = "#";
                    givenMaterial.Project = myMaster.Project;
                    givenMaterial.Workflow = "WF_RDA_01";
                    givenMaterial.businessDataType = myMaster.businessDataType;
                    givenMaterial.Description = myMaster.Description;
                    givenMaterial.Key1 = myMaster.Id.ToString();
                    ArrayList currentValues = new ArrayList();
                    if (myBaseAttribute != null && myBaseAttribute.Count > 0)
                    {
                        foreach (var item in myBaseAttribute)
                        {
                            eDataListElement elem = (eDataListElement)item;
                            if (elem.Name == "RDASCOPE")
                                continue;
                            currentValues.Add(elem);
                        }
                    }

                    eDataListElement myData = new eDataListElement();
                    myData.Name = "DrawingCode";
                    myData.Value = row.DRAWINGCODE;
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "MaterialDescription";
                    myData.Value = row.MATERIALDESCRIPTION;
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "CO_SP";
                    myData.Value = row.CO_SP;
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "QuantityOnVehicle";
                    myData.Value = row.QUANTITYONVEHICLE.ToString().Replace(",", ".");
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "PiecePrice";
                    myData.Value = row.PIECEPRICE.ToString().Replace(",", ".");
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "PiecePriceFormat";
                    myData.Value = row.PIECEPRICEFORMAT;
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "TotalQuantity";
                    myData.Value = row.TOTALQUANTITY.ToString().Replace(",", ".");
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "RequestedSupplier";
                    myData.Value = row.REQUESTEDSUPPLIER;
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "Supplier";
                    myData.Value = row.SUPPLIER;
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "SupplierReference";
                    myData.Value = row.SUPPLIERREFERENCE;
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "CodiceIman";
                    myData.Value = row.CODICEIMAN;
                    currentValues.Add(myData);

                    myData = new eDataListElement();
                    myData.Name = "RequestedDeliveryDate";
                    string date = row.REQUESTEDDELIVERYDATE.ToString("yyyyMMdd000000");
                    _connection.Core.stringToDate(date, ref date);
                    myData.Value = date.Split(" ".ToCharArray())[0];
                    currentValues.Add(myData);

                    try
                    {
                        ThrowError = givenMaterial.Create(ref currentValues);
                    }
                    catch (Exception e)
                    {
                        row.Sys_Error = e.Message;
                        retValue.result = false;
                    }
                    retValue.rows.Add(row);

                }

                eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref _connection);
                try
                {
                    eWorkflowEngine myWF = new eWorkflowEngine(ref dbaConnection, myMaster.Id);
                    eRdAWorkflowAction givenWorkflowAction = new eRdAWorkflowAction();
                    givenWorkflowAction.businessDataId = myMaster.Id;
                    givenWorkflowAction.actionType = eRdAWFActionType.APPROVE;
                    eRdAHelper.BusinessDataPerformTask(ref dbaConnection, givenWorkflowAction);
                }
                catch (Exception e)
                {

                }
                ePortalHelper.DestroyDbaConnection(ref _connection, ref dbaConnection);



            }
            return retValue;
        }
        #endregion

        #region "EXPORT"
        public MemoryStream eRdAExport(string exporttype, eRdAExportOptions data)
        {

            MemoryStream retValue = null;
            switch (exporttype)
            {
                case "remedy":
                    string year = DateTime.Now.ToString("yyyy");
                    try
                    {
                        if (data.paramName[0] == "year")
                            if (eRdAHelper.IsNumeric(data.paramValue[0]))
                                year = data.paramValue[0];
                    }
                    catch (Exception e) { }
                    retValue = eRdAExportHelper.GetRemedySupplier(ref _connection, year);
                    break;
                case "rdaarchive":
                    retValue = eRdAExportHelper.ExporteRdAList(ref _connection);
                    break;
                case "rdasearch":
                    retValue = eRdAExportHelper.ExporteRdAList(ref _connection, data.filter);
                    break;
                case "projectroleuser":
                    retValue = eRdAExportHelper.ExportProjectRoleUser(ref _connection, data.filter);
                    break;
                case "supplierpricelist":
                    retValue = eRdAExportHelper.ExportSupplierPriceList(ref _connection);
                    break;

            }
            return retValue;
        }

        public void UpdateRemedyPriceList(RemedyPriceListData data)
        {
            var str = data.filecontent.Replace("data:application/vnd.ms-excel;base64,", "");
            str = str.Replace("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,", "");
            byte[] bytes = System.Convert.FromBase64String(str);
            Stream stream = new MemoryStream(bytes);
            Workbook myWork = new Workbook(stream);
            if (myWork != null && myWork.Worksheets.Count > 0)
            {
                Worksheet myData = myWork.Worksheets[0];
                //posizione 7 è la descrizione completa
                //posizione 8 è PWT
                string query = "update LISTINO_FORNITORI set [Descrizione Completa]='{0}',[PWT COMPETENCE]={1},ModifyDate=GETDATE()  where ID={2}" + Environment.NewLine;
                StringBuilder updateQuery = new StringBuilder();

                for (int i = 1; i <= myData.Cells.MaxDataRow; i++)
                {
                    if (myData.Cells[i, 7].Value is null || myData.Cells[i, 8].Value is null)
                        continue;
                    updateQuery.AppendLine(string.Format(query, myData.Cells[i, 7].Value.ToString().Replace("'"," ") , 0, myData.Cells[i, 0].Value));
                }
                if (updateQuery.ToString().Length > 0)
                {
                    eRdAHelper.ExecuteLongQuery(ref _connection, updateQuery.ToString());
                }
            }
            
        }

        public BaaNProjectMatch GetBaaNProjectMantch(string name)
        {
            BaaNProjectMatch retValue = new BaaNProjectMatch();
            if (name.ToLower().Contains("projectdeleted"))
                return retValue;

            string query = "SELECT  PRJ_TRANSCODE.ID, PRJ_TRANSCODE.PRJ_ERDA, PRJ_TRANSCODE.PRJ_BAAN, PRJ_COMMESSE.ID AS ID2, PRJ_COMMESSE.COMMESSA_FINANCE, PRJ_COMMESSE.COMMESSA_PRP, PRJ_COMMESSE.COD_PRJ_FORM FROM PRJ_TRANSCODE INNER JOIN PRJ_COMMESSE ON PRJ_TRANSCODE.PRJ_BAAN = PRJ_COMMESSE.PRJ_BAAN where PRJ_ERDA='{0}'";
            query = string.Format(query, name);
            DataTable table = ePortalHelper.ExecuteQuery(ref _connection, query);
            if (table != null && table.Rows.Count > 0)
            {
                retValue.rdaProjectName = table.Rows[0]["PRJ_ERDA"].ToString();
                retValue.baanProjectName = table.Rows[0]["PRJ_BAAN"].ToString();
                retValue.baanFinance = table.Rows[0]["COMMESSA_FINANCE"].ToString();
                retValue.baanPRP = table.Rows[0]["COMMESSA_PRP"].ToString();
                retValue.baanCodeProjectForm = table.Rows[0]["COD_PRJ_FORM"].ToString();
            }
            return retValue;

        }

        public bool SetBaaNProjectMantch(BaaNProjectMatch givenRdABaaNMatch)
        {
            string myDel1 = "DELETE [dbo].[PRJ_TRANSCODE]  where PRJ_ERDA='" + givenRdABaaNMatch.rdaProjectName + "'";
            string myDel2 = "DELETE [dbo].[PRJ_COMMESSE]  where PRJ_BAAN='" + givenRdABaaNMatch.baanProjectName + "'";

            ePortalHelper.ExecuteQuery(ref _connection, myDel1);
            ePortalHelper.ExecuteQuery(ref _connection, myDel2);

            string myStringInsert1 = "INSERT INTO [dbo].[PRJ_COMMESSE] ([PRJ_BAAN],[COMMESSA_FINANCE],[COMMESSA_PRP],[COD_PRJ_FORM]) VALUES ('{0}','{1}','{2}','{3}')";

            myStringInsert1 = string.Format(myStringInsert1, givenRdABaaNMatch.baanProjectName, givenRdABaaNMatch.baanFinance, givenRdABaaNMatch.baanPRP, givenRdABaaNMatch.baanCodeProjectForm);

            ePortalHelper.ExecuteQuery(ref _connection, myStringInsert1);

            string myStringInsert2 = "INSERT INTO [dbo].[PRJ_TRANSCODE] ([PRJ_ERDA],[PRJ_BAAN]) VALUES ('{0}','{1}')";

            myStringInsert2 = string.Format(myStringInsert2, givenRdABaaNMatch.rdaProjectName, givenRdABaaNMatch.baanProjectName);

            ePortalHelper.ExecuteQuery(ref _connection, myStringInsert2);

            

            return true;
        }


        #endregion

        public void writeLog(string origin, ref Exception e)
        {

            _connection.Logger.writeToLog(origin + " - " + _connection.currentUserName,"API ERROR");
            _connection.Logger.writeToLog(origin + " - " + e.Message,"API ERROR");
            _connection.Logger.writeToLog(origin + " - " + e.ToString(),"API ERROR");
        }
    }


}