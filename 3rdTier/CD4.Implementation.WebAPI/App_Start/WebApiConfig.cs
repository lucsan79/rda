﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Cors;
using System.Web.Http.Dispatcher;
using CD4.Implementation.WebAPI.Helpers;

namespace CD4.Implementation.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var corsAttribute = new EnableCorsAttribute(
                    origins: System.Configuration.ConfigurationManager.AppSettings["ClientsUrl"],
                    headers: "*",
                    methods: "*")
                { SupportsCredentials = true };
            // Web API configuration and services
            config.EnableCors(corsAttribute);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Services.Replace(typeof(IAssembliesResolver), new CDAssembliesResolver());
            config.Services.Replace(typeof(IHttpControllerSelector), new CD4ControllerSelector(config));
            config.Services.Replace(typeof(IHttpActionSelector), new CD4ActionSelector());

        }
    }
}
