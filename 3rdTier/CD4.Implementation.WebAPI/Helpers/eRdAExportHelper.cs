﻿using CD4.ApiExtensions.Helpers;
using CD4.Implementation.WebAPI.ViewModels;
using Parallaksis.ePLMSM40;
using Parallaksis.ePLMSM40.Defs;
using Parallaksis.M40.Configurator;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using static Parallaksis.ePLMSM40.Defs.eDataListElementModule;
using static Parallaksis.ePLMSM40.Defs.eErrorDefs;
using static Parallaksis.ePLMSM40.Defs.eAdminDataDefs;
using static Parallaksis.ePLMSM40.Defs.eStructDefs;
using System.Reflection;
using System.Text;
using System.Diagnostics;
using Aspose.Cells;
using System.IO;

namespace CD4.WebAPI.Helpers
{

    public class eRdAExportHelper
    {
        public static DataTable GetProjectRoleFullView(ref eConnection givenConnection)
        {
            eUserRdAProfile currentUser = new eUserRdAProfile();
            DataTable myTableCDC = GetCDCTable(ref givenConnection);
            DataTable myRetTable = GetDataSourceEmpty(ref givenConnection);
            DataTable listOfProject = null/* TODO Change to default(_) if this is not a reference type */;
            bool fullView = false;
            DataTable myFullTable = GetProjectRoleUsers(ref givenConnection);
            fullView = eRdAExportHelper.IsUserConfigAdmin(ref givenConnection);//  givenConnection.isCurrentUserDba;

            if (fullView)
                listOfProject = GetProject(ref givenConnection);
            else
                listOfProject = GetCMSProject(ref givenConnection,givenConnection.currentUserName);

            DataTable roleUsers = GetProjectRoleUsersExtraCDC(ref givenConnection);

            if (listOfProject != null && listOfProject.Rows.Count > 0 && myFullTable != null && myFullTable.Rows.Count > 0)
            {
                foreach (DataRow row in listOfProject.Rows)
                {
                    DataRow[] myRows = myFullTable.Select("ePROJECT='" + row["Project"].ToString() + "'");
                    if (myRows.Length > 0)
                    {
                        string comp = myRows[0]["ECOMPETENCE"].ToString();
                        foreach (DataRow myNewRow in myRows)
                        {
                            DataRow importRow = myRetTable.NewRow();
                            importRow["Num"] = myRetTable.Rows.Count + 1;
                            importRow["Competence"] = myNewRow["ECOMPETENCE"];
                            importRow["Project"] = myNewRow["EPROJECT"];
                            importRow["Role"] = "PR CDC " + myNewRow["EROLE"];
                            importRow["ACTIVITYNPI"] = myNewRow["EACTIVITYNPI"];
                            importRow["User"] = myNewRow["EUSER"];
                            myRetTable.Rows.Add(importRow);
                        }
                        myRows = roleUsers.Select("PROJECT='" + row["Project"].ToString() + "' and ROLE='CMS'");
                        if (myRows.Length > 0)
                        {
                            foreach (DataRow myNewRow in myRows)
                            {
                                DataRow importRow = myRetTable.NewRow();
                                importRow["Num"] = myRetTable.Rows.Count + 1;
                                importRow["Competence"] = comp;
                                importRow["Project"] = row["Project"].ToString().ToUpper();
                                importRow["Role"] = myNewRow["ROLE"].ToString().ToUpper();
                                importRow["User"] = myNewRow["USER"].ToString().ToUpper();
                                myRetTable.Rows.Add(importRow);
                            }
                        }
                        myRows = roleUsers.Select("PROJECT='" + row["Project"] + "' and ROLE='PM'");
                        if (myRows.Length > 0)
                        {
                            foreach (DataRow myNewRow in myRows)
                            {
                                DataRow importRow = myRetTable.NewRow();
                                importRow["Num"] = myRetTable.Rows.Count + 1;
                                importRow["Competence"] = comp;
                                importRow["Project"] = row["Project"].ToString().ToUpper();
                                importRow["Role"] = myNewRow["ROLE"].ToString().ToUpper();
                                importRow["User"] = myNewRow["USER"].ToString().ToUpper();
                                myRetTable.Rows.Add(importRow);
                            }
                        }
                    }
                }
            }

            DataRow[] Requester = roleUsers.Select("PROJECT='RDA' and ROLE='Requester'");
            DataRow[] PPM = roleUsers.Select("PROJECT='RDA' and ROLE='PPM'");
            DataRow[] PMP = roleUsers.Select("PROJECT='RDA' and ROLE='PMP'");
            DataRow[] UR = roleUsers.Select("PROJECT='RDA' and ROLE like 'UR_%'");
            DataRow[] PL = roleUsers.Select("PROJECT='RDA' and ROLE like 'PL_%'");

            DataRow importRequester = myRetTable.NewRow();
            importRequester["Num"] = myRetTable.Rows.Count + 1;
            importRequester["Competence"] = "";
            importRequester["Project"] = "All Project";
            importRequester["Role"] = "Requester";
            importRequester["User"] = "";
            if (Requester.Length > 0)
            {
                DataTable dummyTable = Requester.CopyToDataTable();
                dummyTable.DefaultView.Sort = "USER ASC";
                foreach (DataRow row in dummyTable.DefaultView.ToTable().Rows)
                {
                    if (!string.IsNullOrEmpty(importRequester["User"].ToString()))
                        importRequester["User"] += ", ";
                    importRequester["User"] += row["USER"].ToString().ToUpper();
                }
            }

            myRetTable.Rows.Add(importRequester);


            DataRow importPPM = myRetTable.NewRow();
            importPPM["Num"] = myRetTable.Rows.Count + 1;
            importPPM["Competence"] = "";
            importPPM["Project"] = "All Project";
            importPPM["Role"] = "PPM";
            importPPM["User"] = "";
            if (PPM.Length > 0)
            {
                DataTable dummyTable = PPM.CopyToDataTable();
                dummyTable.DefaultView.Sort = "USER ASC";
                foreach (DataRow row in dummyTable.DefaultView.ToTable().Rows)
                {
                    if (!string.IsNullOrEmpty(importPPM["User"].ToString()))
                        importPPM["User"] += ", ";
                    importPPM["User"] += row["USER"].ToString().ToUpper();
                }
            }

            myRetTable.Rows.Add(importPPM);



            DataRow importPMP = myRetTable.NewRow();
            importPMP["Num"] = myRetTable.Rows.Count + 1;
            importPMP["Competence"] = "";
            importPMP["Project"] = "All Project";
            importPMP["Role"] = "PMP";
            importPMP["User"] = "";
            if (PMP.Length > 0)
            {
                DataTable dummyTable = PMP.CopyToDataTable();
                dummyTable.DefaultView.Sort = "USER ASC";
                foreach (DataRow row in dummyTable.DefaultView.ToTable().Rows)
                {
                    if (!string.IsNullOrEmpty(importPMP["User"].ToString()))
                        importPMP["User"] += ", ";
                    importPMP["User"] += row["USER"].ToString().ToUpper();
                }
            }

            myRetTable.Rows.Add(importPMP);


            if (UR.Length > 0)
            {
                DataTable myAREACDC = UR.CopyToDataTable().DefaultView.ToTable(true, "ROLE");

                foreach (DataRow row in myAREACDC.Rows)
                {
                    List<string> myListItem = new List<string>();
                    foreach (DataRow urRow in UR)
                    {
                        if (urRow["ROLE"].ToString() != row["ROLE"].ToString())
                            continue;
                        if (!myListItem.Contains(urRow["USER"].ToString().ToUpper()))
                            myListItem.Add(urRow["USER"].ToString().ToUpper());
                    }
                    DataRow importUR = myRetTable.NewRow();
                    importUR["Num"] = myRetTable.Rows.Count + 1;
                    importUR["Competence"] = "";
                    importUR["Project"] = "All Project";
                    importUR["Role"] = row["ROLE"].ToString().Replace("_", " ");
                    importUR["User"] = string.Join(", ", myListItem.ToArray());
                    myRetTable.Rows.Add(importUR);
                }
            }

            if (PL.Length > 0)
            {
                DataTable myAREACDC = PL.CopyToDataTable().DefaultView.ToTable(true, "ROLE");

                foreach (DataRow row in myAREACDC.Rows)
                {
                    List<string> myListItem = new List<string>();
                    foreach (DataRow urRow in PL)
                    {
                        if (urRow["ROLE"].ToString() != row["ROLE"].ToString())
                            continue;
                        if (!myListItem.Contains(urRow["USER"].ToString().ToUpper()))
                            myListItem.Add(urRow["USER"].ToString().ToUpper());
                    }
                    DataRow importUR = myRetTable.NewRow();
                    importUR["Num"] = myRetTable.Rows.Count + 1;
                    importUR["Competence"] = "";
                    importUR["Project"] = "All Project";
                    importUR["Role"] = row["ROLE"].ToString().Replace("_", " ");
                    importUR["User"] = string.Join(", ", myListItem.ToArray());
                    myRetTable.Rows.Add(importUR);
                }
            }




            myRetTable.DefaultView.Sort = "Competence ASC, Project ASC, Role ASC, ACTIVITYNPI ASC, User ASC";

            return myRetTable.DefaultView.ToTable();
        }
        private static DataTable GetCDCTable(ref eConnection givenConnection)
        {
            DataTable myTableArea = ePortalHelper.ExecuteQuery(ref givenConnection,"SELECT distinct CDC from dbo.MASAD_WBS where CDC IS NOT NULL order by CDC");
            if (myTableArea != null)
            {
                myTableArea.Columns.Add("eId");
                for (int idx = 0; idx <= myTableArea.Rows.Count - 1; idx++)
                    myTableArea.Rows[idx]["eId"] = idx;
            }
            return myTableArea;
        }
        private static DataTable GetDataSourceEmpty(ref eConnection givenConnection)
        {
            DataTable myTableCDC = GetCDCTable(ref givenConnection);
            DataTable myRetTable = new DataTable();



            DataColumn myNewGenericColoum = new DataColumn();
            myNewGenericColoum.DataType = typeof(int);
            myNewGenericColoum.ColumnName = "Num";
            myNewGenericColoum.Caption = "#";
            myRetTable.Columns.Add(myNewGenericColoum);

            myNewGenericColoum = new DataColumn();
            myNewGenericColoum.DataType = typeof(string);
            myNewGenericColoum.ColumnName = "Competence";
            myNewGenericColoum.Caption = "Competence";
            myRetTable.Columns.Add(myNewGenericColoum);

            myNewGenericColoum = new DataColumn();
            myNewGenericColoum.DataType = typeof(string);
            myNewGenericColoum.ColumnName = "Project";
            myNewGenericColoum.Caption = "Project";
            myRetTable.Columns.Add(myNewGenericColoum);



            myNewGenericColoum = new DataColumn();
            myNewGenericColoum.DataType = typeof(string);
            myNewGenericColoum.ColumnName = "Role";
            myNewGenericColoum.Caption = "Role";
            myRetTable.Columns.Add(myNewGenericColoum);


            myNewGenericColoum = new DataColumn();
            myNewGenericColoum.DataType = typeof(string);
            myNewGenericColoum.ColumnName = "ACTIVITYNPI";
            myRetTable.Columns.Add(myNewGenericColoum);

            myNewGenericColoum = new DataColumn();
            myNewGenericColoum.DataType = typeof(string);
            myNewGenericColoum.ColumnName = "User";
            myRetTable.Columns.Add(myNewGenericColoum);


            return myRetTable;
        }
        private static DataTable GetProject(ref eConnection givenConnection)
        {
            string query = "SELECT    distinct    eplms.eProject.eName as [Project]	 FROM  eplms.eUser INNER JOIN  eplms.eProjectRoleUser ON eplms.eUser.eId = eplms.eProjectRoleUser.eUserId INNER JOIN			  " + 
                           " eplms.eProjectRole ON eplms.eProjectRoleUser.eProjectRoleId = eplms.eProjectRole.eId INNER JOIN " +
                           " eplms.eProject ON eplms.eProjectRole.eProjectId = eplms.eProject.eId INNER JOIN				  " + 
                           "                         eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId									  " + 
                           "WHERE   eplms.eProject.eStatus='ENABLED' order by [Project]												          ";
            query = string.Format(query);
            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection,query);
            return myTable;
        }
        private static DataTable GetPMProject(ref eConnection givenConnection,string givenUserName)
        {
            string query = "SELECT    distinct    eplms.eProject.eName as [Project]											  " + 
                            "FROM            eplms.eUser INNER JOIN																					  " + 
                            "                         eplms.eProjectRoleUser ON eplms.eUser.eId = eplms.eProjectRoleUser.eUserId INNER JOIN			  " + 
                            "                         eplms.eProjectRole ON eplms.eProjectRoleUser.eProjectRoleId = eplms.eProjectRole.eId INNER JOIN " + 
                            "                         eplms.eProject ON eplms.eProjectRole.eProjectId = eplms.eProject.eId INNER JOIN				  " + 
                            "                         eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId									  " + 
                            "WHERE        (eplms.eUser.eName = N'{0}')																	              " + 
                            "and eplms.eRole.eName ='PM' and eplms.eProject.eStatus='ENABLED' order by [Project]						          ";
            query = string.Format(query, givenUserName);
            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection,query);
            return myTable;
        }
        private static DataTable GetCMSProject(ref eConnection givenConnection,string givenUserName)
        {
            string query = "SELECT    distinct    eplms.eProject.eName as [Project]											  " + 
                "FROM            eplms.eUser INNER JOIN																					  " + 
                "                         eplms.eProjectRoleUser ON eplms.eUser.eId = eplms.eProjectRoleUser.eUserId INNER JOIN			  " + 
                "                         eplms.eProjectRole ON eplms.eProjectRoleUser.eProjectRoleId = eplms.eProjectRole.eId INNER JOIN " + 
                "                         eplms.eProject ON eplms.eProjectRole.eProjectId = eplms.eProject.eId INNER JOIN				  " + 
                "                         eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId									  " + 
                "WHERE        (eplms.eUser.eName = N'{0}')																	              " + 
                "and eplms.eRole.eName ='CMS' and eplms.eProject.eStatus='ENABLED' order by [Project]						          ";
            query = string.Format(query, givenUserName);
            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection,query);
            return myTable;
        }
        private static DataTable GetProjectRoleUsers(ref eConnection givenConnection)
        {
            string query = "SELECT        eplms.eListItem.eValue AS ECOMPETENCE, eListItem_2.eValue AS EPROJECT, eListItem_3.eValue AS EROLE, '' AS EACTIVITYNPI, eListItem_4.eValue AS EUSER                  " +
              "FROM            eplms.eListItem INNER JOIN																																			" +
              "                         eplms.eList ON eplms.eListItem.eListId = eplms.eList.eId INNER JOIN																						" +
              "                         eplms.eListItem AS eListItem_1 ON eplms.eListItem.eValueId = eListItem_1.eListId INNER JOIN																" +
              "                         eplms.eListItem AS eListItem_2 ON eListItem_1.eValue = eListItem_2.eValue INNER JOIN																		" +
              "                         eplms.eList AS eList_1 ON eListItem_2.eListId = eList_1.eId INNER JOIN																					" +
              "                         eplms.eListItem AS eListItem_3 ON eListItem_2.eValueId = eListItem_3.eListId INNER JOIN																	" +
              "                         eplms.eListItem AS eListItem_4 ON eListItem_3.eValueId = eListItem_4.eListId 																				" +
              "WHERE        (eplms.eList.eName = N'COMPETENCE_PROJECT') AND (eList_1.eName = N'PROJECT_CDCAPPROVERS') AND eListItem.eValue<>'TRANSVERSAL'											" +
              "																																													" +
              "UNION ALL																																											" +
              "																																													" +
              "SELECT        eplms.eListItem.eValue AS ECOMPETENCE, eListItem_2.eValue AS EPROJECT, eListItem_3.eValue AS EROLE, eListItem_4.eValue AS EACTIVITYNPI, eListItem_5.eValue AS EUSER	" +
              "FROM            eplms.eListItem INNER JOIN																																			" +
              "                         eplms.eList ON eplms.eListItem.eListId = eplms.eList.eId INNER JOIN																						" +
              "                         eplms.eListItem AS eListItem_1 ON eplms.eListItem.eValueId = eListItem_1.eListId INNER JOIN																" +
              "                         eplms.eListItem AS eListItem_2 ON eListItem_1.eValue = eListItem_2.eValue INNER JOIN																		" +
              "                         eplms.eList AS eList_1 ON eListItem_2.eListId = eList_1.eId INNER JOIN																					" +
              "                         eplms.eListItem AS eListItem_3 ON eListItem_2.eValueId = eListItem_3.eListId INNER JOIN																	" +
              "                         eplms.eListItem AS eListItem_4 ON eListItem_3.eValueId = eListItem_4.eListId INNER JOIN																	" +
              "                         eplms.eListItem AS eListItem_5 ON eListItem_4.eValueId = eListItem_5.eListId																				" +
"WHERE        (eplms.eList.eName = N'COMPETENCE_PROJECT') AND (eList_1.eName = N'PROJECT_CDCAPPROVERS') AND eListItem.eValue='TRANSVERSAL'											" +
"ORDER BY ECOMPETENCE, EPROJECT, EROLE, EACTIVITYNPI,EUSER																															";



            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection,query);
            return myTable;
        }
        private static DataTable GetProjectRoleUsersExtraCDC(ref eConnection givenConnection)
        {
            string query = "SELECT        eplms.eProject.eName AS PROJECT, eplms.eRole.eName AS ROLE, eplms.eUser.eName AS [USER] " +
"FROM            eplms.eProjectRole INNER JOIN																					" +
"                         eplms.eProject ON eplms.eProjectRole.eProjectId = eplms.eProject.eId INNER JOIN						" +
"                         eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId INNER JOIN								" +
"                         eplms.eProjectRoleUser ON eplms.eProjectRole.eId = eplms.eProjectRoleUser.eProjectRoleId INNER JOIN	" +
"                         eplms.eUser ON eplms.eProjectRoleUser.eUserId = eplms.eUser.eId	ORDER BY 	USER";

            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection,query);
            return myTable;
        }
        private static string GetUsersInRole(ref eConnection givenConnection,ref DataTable givenTable, string givenProject, string givenRole)
        {
            System.Text.StringBuilder myStringBuilder = new System.Text.StringBuilder();
            DataRow[] myRows = givenTable.Select("Role='" + givenRole + "' and Project='" + givenProject + "'");
            foreach (DataRow row in myRows)
                myStringBuilder.AppendLine(row["User"].ToString());
            // Dim myTable As DataTable = ePortalHelper.ExecuteQuery(ref givenConnection,Query)
            return myStringBuilder.ToString();
        }

        #region "REMEDY"
        public static MemoryStream GetRemedySupplier(ref eConnection givenConnection, string year)
        {
            string retContent = "";
            MemoryStream output = new MemoryStream();
            string scriptFullName = System.IO.Path.Combine(givenConnection.ePLMS_HOME, "env\\scripts\\admin\\SupplierManagement.eplms");
            string myCDC = System.IO.Path.Combine(givenConnection.ePLMS_HOME, "env\\scripts\\admin\\cdc.eplms");
            string mySupplierList = System.IO.Path.Combine(givenConnection.ePLMS_HOME, "env\\scripts\\admin\\remedysuppliers.eplms");
            if (string.IsNullOrEmpty(year)) year = DateTime.Now.Year.ToString();

            DataTable myRetTable = new DataTable("remedy");

            if (System.IO.File.Exists(scriptFullName))
            {
                retContent = System.IO.File.ReadAllText(scriptFullName);
                DataTable totaleRdA = ePortalHelper.ExecuteQuery(ref givenConnection, retContent, new string[] { year }, null);
                retContent = System.IO.File.ReadAllText(myCDC);
                DataTable myTableCDC = ePortalHelper.ExecuteQuery(ref givenConnection, retContent, null, null);
                retContent = System.IO.File.ReadAllText(mySupplierList);
                DataTable mySupplierTable = ePortalHelper.ExecuteQuery(ref givenConnection, retContent, new string[] { year }, null);

                if (myTableCDC != null && myTableCDC.Rows.Count > 0)
                {

                    DataColumn myNewGenericColoum = new DataColumn();
                    myNewGenericColoum.DataType = typeof(string);
                    myNewGenericColoum.ColumnName = "ID";
                    myRetTable.Columns.Add(myNewGenericColoum);

                    myNewGenericColoum = new DataColumn();
                    myNewGenericColoum.DataType = typeof(string);
                    myNewGenericColoum.ColumnName = "YEAR";
                    myRetTable.Columns.Add(myNewGenericColoum);


                    myNewGenericColoum = new DataColumn();
                    myNewGenericColoum.DataType = typeof(string);
                    myNewGenericColoum.ColumnName = "SupplierCode";
                    myRetTable.Columns.Add(myNewGenericColoum);

                    myNewGenericColoum = new DataColumn();
                    myNewGenericColoum.DataType = typeof(string);
                    myNewGenericColoum.ColumnName = "BusinessName";
                    myRetTable.Columns.Add(myNewGenericColoum);


                    foreach (DataRow cdc in myTableCDC.Rows)
                    {
                        string cdclabel = cdc[0].ToString().ToUpper();
                        cdclabel = eRdAHelper.GetPlannerRoleDBValue(cdclabel);

                        DataColumn myNewColoum = new DataColumn();
                        myNewColoum.DataType = typeof(double);
                        myNewColoum.ColumnName = cdclabel + "_allocated";
                        myNewColoum.DefaultValue = 0;
                        myRetTable.Columns.Add(myNewColoum);

                        myNewColoum = new DataColumn();
                        myNewColoum.DataType = typeof(double);
                        myNewColoum.ColumnName = cdclabel + "_residual";
                        myNewColoum.DefaultValue = 0;
                        myRetTable.Columns.Add(myNewColoum);

                        myNewColoum = new DataColumn();
                        myNewColoum.DataType = typeof(string);
                        myNewColoum.ColumnName = cdclabel + "_residual_cls";
                        myNewColoum.DefaultValue = "";
                        myRetTable.Columns.Add(myNewColoum);


                    }


                    if (mySupplierTable != null)
                    {
                        eRdAHelper.FIXYEAR(mySupplierTable, year, ref givenConnection);
                        for (int IDX = mySupplierTable.Rows.Count - 1; IDX >= 0; IDX += -1)
                        {
                            DataRow item = mySupplierTable.Rows[IDX];
                            DataRow myNewRow = myRetTable.NewRow();
                            myNewRow["ID"] = item["ID"];
                            myNewRow["YEAR"] = year;
                            myNewRow["SupplierCode"] = item["CODE"].ToString().Trim();
                            myNewRow["BusinessName"] = item["NAME"];
                            myNewRow["BusinessName"] = myNewRow["BusinessName"].ToString().Replace("    cod.forn.", "|");
                            myNewRow["BusinessName"] = myNewRow["BusinessName"].ToString().Split("|".ToCharArray())[0].Trim();
                            //myNewRow["Error"] = "";


                            if (item["Budget"] != null)
                            {
                                foreach (string budget in item["Budget"].ToString().Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                                {
                                    string area = budget.Split("#".ToCharArray())[0];
                                    string value = budget.Split("#".ToCharArray())[1];
                                    double currentValue = 0;
                                    currentValue = eRdAHelper.GetDoubleValue(value);
                                    DataRow[] AreaCheck = myTableCDC.Select("CDC='" + area + "'");
                                    if (AreaCheck.Length > 0)
                                    {
                                        string label = eRdAHelper.GetPlannerRoleDBValue(area);
                                        string myLabel = label + "_allocated";
                                        string myLabel2 = label + "_residual";
                                        string myLabel3 = label + "_residual_cls";
                                        if (myRetTable.Columns.Contains(myLabel))
                                            myNewRow[myLabel] = currentValue.ToString("N2");
                                        string myQuery = string.Format("CDC='{0}' and Supplier='{1}'", area, item["NAME"].ToString().ToLower());
                                        double totale = 0;
                                        try
                                        {
                                            DataRow[] myRows = totaleRdA.Select(myQuery);
                                            foreach (DataRow row in myRows)
                                            {
                                                double committed = eRdAHelper.GetDoubleValue(row["Committed"].ToString());
                                                double amount = eRdAHelper.GetDoubleValue(row["Amount"].ToString());
                                                if (committed > 0)
                                                    totale = totale + committed;
                                                else
                                                    totale = totale + amount;
                                            }
                                        }
                                        // totale = totaleRdA.Compute("Sum(Amount)", myQuery) ' "srno=1 or srno in(1,2)")
                                        catch (Exception ex)
                                        {
                                        }

                                        double residuo = currentValue - totale;
                                        myNewRow[myLabel2] = residuo.ToString("N2");
                                        if (residuo < 0 || (residuo == 0 && currentValue > 0))
                                            myNewRow[myLabel3] = "redLabel";

                                    }
                                }
                            }
                            myRetTable.Rows.InsertAt(myNewRow, 0);
                        }
                    }
                }
            }
            if (myRetTable == null || myRetTable.Rows.Count == 0)
                throw new Exception("Error during Remedy Supplier Export! Please contact the system administrator");


            Workbook remedyData = GetRemedyExcel(ref givenConnection, ref myRetTable);
            remedyData.Save(output, new XlsSaveOptions(SaveFormat.Xlsx));// new OoxmlSaveOptions());
            return output;
        }

        public static MemoryStream ExporteRdAList(ref eConnection givenConnection, string extraCondition="")
        {
            eUserRdAProfile myUserProfile = eRdAHelper.GetUserProfile(ref givenConnection);
            string userFilters = "";
            DataTable dummyTable = null;
            List<string> myStringBuilder = new List<string>();
            MemoryStream output = new MemoryStream();
            

            string queryToExecute = eRdAHelper.GetQueryContent(ref givenConnection, "export\\basequery.eplms", new string[] { givenConnection.currentUserName,""});

            if (!myUserProfile.HasFullView)
            {
                if (myUserProfile.IsRequester)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref givenConnection, "Archive\\filters\\Requester.filter", new string[] { givenConnection.currentUserName}));
                if (myUserProfile.IsPMP)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref givenConnection, "Archive\\filters\\PMP.filter", new string[] { givenConnection.currentUserName}));
                if (myUserProfile.IsPPM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref givenConnection, "Archive\\filters\\PPM.filter", new string[] { givenConnection.currentUserName }));
                if (myUserProfile.IsCSM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref givenConnection, "Archive\\filters\\CMS.filter", new string[] { givenConnection.currentUserName }));
                if (myUserProfile.IsPM)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref givenConnection, "Archive\\filters\\PM.filter", new string[] { givenConnection.currentUserName }));
                if (myUserProfile.IsPR)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref givenConnection, "Archive\\filters\\PR.filter", new string[] { givenConnection.currentUserName }));
                if (myUserProfile.IsUR)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref givenConnection, "Archive\\filters\\UR.filter", new string[] { givenConnection.currentUserName }));
                if (myUserProfile.IsPlanner)
                    myStringBuilder.Add(eRdAHelper.GetQueryContent(ref givenConnection, "Archive\\filters\\PLANNER.filter", new string[] { givenConnection.currentUserName }));
                if (myStringBuilder.Count() > 0)
                    userFilters = String.Join(" or ", myStringBuilder.ToArray());
                else
                    userFilters = "(1=2)";

            }


            if (!string.IsNullOrEmpty(userFilters))
                userFilters = string.Format(" AND ({0})", userFilters);

            if (!string.IsNullOrEmpty(extraCondition))
                userFilters = userFilters + " " + extraCondition;

            queryToExecute = queryToExecute.Replace("@Cond1", userFilters);

            dummyTable = ePortalHelper.ExecuteQuery(ref givenConnection, queryToExecute);

            if (dummyTable != null)
            {

                Workbook remedyData = GetSearchRdAExcel(ref givenConnection, ref dummyTable);
                remedyData.Save(output, new XlsSaveOptions(SaveFormat.Xlsx));
            }
            return output;
        }

        public static bool IsUserConfigAdmin(ref eConnection givenConnection)
        {
            bool active = givenConnection.isCurrentUserDba;

            string query = "with usergroup as																	" +
                            "(SELECT  																			" +
                            "		DISTINCT UPPER(eplms.eGroup.eName) AS PROFILE								" +
                            "FROM   																			" +
                            "		eplms.eGroupUser INNER JOIN													" +
                            "        eplms.eGroup ON eplms.eGroupUser.eGroupId = eplms.eGroup.eId				" +
                            "WHERE																				" +
                            "		eplms.eGroupUser.eUserId= @Param1										" +
                            "UNION ALL																			" +
                            "																					" +
                            "SELECT   																			" +
                            "		DISTINCT UPPER(eplms.eRole.eName) AS PROFILE								" +
                            "FROM    																			" +
                            "		eplms.eProjectRoleUser INNER JOIN											" +
                            "       eplms.eProjectRole ON 														" +
                            "		eplms.eProjectRoleUser.eProjectRoleId = eplms.eProjectRole.eId INNER JOIN	" +
                            "       eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId					" +
                            "where 																				" +
                            "		eplms.eProjectRoleUser.eUserId=@Param1									" +
                            "																					" +
                            "union all																			" +
                            "		select TOP(1) 'PR' as PROFILE from eplms.eListItem							" +
                            "where eListItem.eValue='@Param2'										" +
                            ")																					" +
                            "																					" +
                            "select distinct usergroup.PROFILE from usergroup									";
            if (!givenConnection.isCurrentUserDba)
            {
                DataTable myTableRole = ePortalHelper.ExecuteQuery(ref givenConnection, query, new string[] { givenConnection.currentUserId.ToString(), givenConnection.currentUserName }, null);
                if (myTableRole != null && myTableRole.Rows.Count > 0)
                {
                    foreach (DataRow r in myTableRole.Rows)
                    {
                        switch (r["PROFILE"].ToString())
                        {
                            case "ERDA CONFIG ADMIN":
                                active = true;
                                break;
                        }
                    }
                }
              
            }
            return active;
        }

        public static MemoryStream ExportProjectRoleUser(ref eConnection givenConnection, string year)
        {
            string retContent = "";
            MemoryStream output = new MemoryStream();

            DataTable dummyTable = GetProjectRoleFullView(ref givenConnection);

            Workbook remedyData = GetProjectRoleExcel(ref givenConnection, ref dummyTable);
            remedyData.Save(output, new XlsSaveOptions(SaveFormat.Xlsx));// new OoxmlSaveOptions());
            return output;
        }

        public static MemoryStream ExportSupplierPriceList(ref eConnection givenConnection)
        {
            string retContent = "";
            MemoryStream output = new MemoryStream();

            DataTable dummyTable = ePortalHelper.ExecuteQuery(ref givenConnection, "SELECT  ID, [ID Contratto], Fornitore, [Ragione Sociale], Pos, Articolo, Descrizione, [Descrizione Completa], Seq FROM  LISTINO_FORNITORI order by Fornitore");

            Workbook remedyData = GetPriceList(ref givenConnection, ref dummyTable);
            remedyData.Save(output, new XlsSaveOptions(SaveFormat.Xlsx));// new OoxmlSaveOptions());
            return output;
        }

        #endregion


        #region "EXCEL"

        private static  Workbook GetRemedyExcel(ref eConnection givenConnection, ref DataTable supplierTable)
        {
            string TemplateReportPath = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\export\Remedy.xlsx");
            Workbook workbookTemplate;
            if (System.IO.File.Exists(TemplateReportPath))
                workbookTemplate = new Aspose.Cells.Workbook(TemplateReportPath,new LoadOptions(LoadFormat.Xlsx));
            else
                throw new Exception("Template Path to export Remedy Setting not found:" + TemplateReportPath);
            Workbook workbook = new Workbook(FileFormatType.Xlsx);
            workbook.Copy(workbookTemplate);
            
            Worksheet datiSheet = workbook.Worksheets[0];
            Range myFirstRange = getCellRangeByName(ref datiSheet, "CDC_START");
            int myLastndex = myFirstRange.FirstColumn;

            if (supplierTable.Columns != null)
            {
                //foreach (DataColumn col in supplierTable.Columns)
                for(var index=0;index< supplierTable.Columns.Count;index++)
                {
                    //int index = 1;
                    DataColumn col = supplierTable.Columns[index];
                    if (col.ColumnName.EndsWith("_allocated"))
                    {
                        myLastndex += 2;
                        datiSheet.Cells.InsertColumns(myLastndex, 2);
                        Range myRange = workbook.Worksheets[0].Cells.CreateRange(myLastndex, 2, true);
                        col.Caption= "REMCOL" + index.ToString();
                        supplierTable.Columns[index+1].Caption= "REMCOL" + index.ToString();
                        myRange.Name = GetRangeName(col.Caption);
                        string title = col.ColumnName.Replace("PL_", "").Replace("_allocated", "").Replace("_residual", "");
                        title = title.Replace("_", " ");
                        myFirstRange.CopyStyle(myRange);
                        datiSheet.Cells.Merge(myFirstRange.FirstRow, myRange.FirstColumn, 1, 2);
                        datiSheet.Cells[myFirstRange.FirstRow,myRange.FirstColumn].PutValue(title);
                        datiSheet.Cells[myFirstRange.FirstRow + 1, myRange.FirstColumn].PutValue("Allocated");
                        datiSheet.Cells[myFirstRange.FirstRow + 1, myRange.FirstColumn + 1].PutValue("Residual");
                    }
                }
            }
            if (supplierTable.Rows != null)
            {
                int startRow = myFirstRange.FirstRow + 3;
                int startCol = getCellRangeByName(ref datiSheet, "SupplierCode").FirstColumn;

                datiSheet.Cells.InsertRows(startRow, supplierTable.Rows.Count - 1);

                startRow = myFirstRange.FirstRow + 2;
                for (int idx = startRow; idx <= startRow + supplierTable.Rows.Count - 1; idx++)
                {
                    datiSheet.Cells.Merge(idx, 2, 1, 3);
                    datiSheet.Cells.Merge(idx, 0, 1, 2);
                }
                foreach (DataRow row in supplierTable.Rows)
                {
                    foreach (DataColumn col in supplierTable.Columns)
                    {
                        if (col.ColumnName.ToLower() == "SupplierCode".ToLower())
                            setValueByCellName(ref datiSheet, "SupplierCode", row[col].ToString(), startRow, 0);
                        else if (col.ColumnName.ToLower() == "BusinessName".ToLower())
                            setValueByCellName(ref datiSheet, "BusinessName", row[col].ToString(), startRow, 0);
                        else if (col.ColumnName.ToLower().EndsWith("_allocated".ToLower()))
                            setValueByCellName(ref datiSheet, col.Caption, row[col].ToString(), startRow + 1, 0);
                        else if (col.ColumnName.ToLower().EndsWith("_residual".ToLower()))
                            setValueByCellName(ref datiSheet, col.Caption, row[col].ToString(), startRow + 1, 1);
                    }
                    startRow += 1;
                }
            }

            datiSheet.Cells.DeleteColumn(myFirstRange.FirstColumn + 1);
            datiSheet.Cells.DeleteColumn(myFirstRange.FirstColumn);
            
            return workbook;
        }

        private static Workbook GetSearchRdAExcel(ref eConnection givenConnection, ref DataTable findTable)
        {
            string TemplateReportPath = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\export\Find.xlsx");
            Workbook workbookTemplate;
            if (System.IO.File.Exists(TemplateReportPath))
                workbookTemplate = new Aspose.Cells.Workbook(TemplateReportPath, new LoadOptions(LoadFormat.Xlsx));
            else
                throw new Exception("Template Path to export eRdA Find not found:" + TemplateReportPath);
            Workbook workbook = new Workbook(FileFormatType.Xlsx);
            workbook.Copy(workbookTemplate);

            Worksheet datiSheet = workbook.Worksheets[0];

            Range myFirstRange = getCellRangeByName(ref datiSheet, "ENAME");
            int startIndex = myFirstRange.FirstRow;
            int indexRow = startIndex + 1;
            
            datiSheet.Cells.ImportDataTable(findTable, false, "A3");
            //if (findTable != null && findTable.Rows.Count > 0)
            //{
            //    datiSheet.Cells.InsertRows(indexRow + 1, findTable.Rows.Count);
            //    //foreach (DataRow myRow in findTable.Rows)
            //    for(int i=0;i<findTable.Rows.Count;i++)
            //    {
            //        DataRow myRow = findTable.Rows[i];
            //        //foreach (DataColumn myCol in findTable.Columns)
            //        for (int y = 0; y < findTable.Columns.Count; y++)
            //        {
            //            string value = "";
            //            if (myRow[y].GetType().Equals(typeof(DateTime)))
            //                try
            //                {
            //                    value = ((DateTime)myRow[y]).ToString("dd/MM/yyyy");
            //                }
            //                catch (Exception e) { }
            //            else 
            //                value = "" + myRow[y].ToString();

            //            if (eRdAHelper.IsNumeric(value))
            //                datiSheet.Cells[i +3, y].PutValue(eRdAHelper.GetDoubleValue(value));
            //            else
            //                datiSheet.Cells[i + 3, y].PutValue(value);

            //            //setValueByCellName(ref datiSheet, myCol.ColumnName, value, 0, 0);
            //        }
            //    }
            //}


            return workbook;
        }

        private static Workbook GetProjectRoleExcel(ref eConnection givenConnection, ref DataTable supplierTable)
        {
            string TemplateReportPath = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\export\projectroleuser.xlsx");
            Workbook workbookTemplate;
            if (System.IO.File.Exists(TemplateReportPath))
                workbookTemplate = new Aspose.Cells.Workbook(TemplateReportPath, new LoadOptions(LoadFormat.Xlsx));
            else
                throw new Exception("Template Path to export Project Role User not found:" + TemplateReportPath);
            Workbook workbook = new Workbook(FileFormatType.Xlsx);
            workbook.Copy(workbookTemplate);

            
            if (workbook.Worksheets != null)
            {
                foreach (Worksheet sheet in workbook.Worksheets)
                {
                    string query = "COMPETENCE='" + sheet.Name + "'";
                    if (sheet.Name.ToUpper() == "REQUESTER")
                        query = "role='requester'";
                    else if (sheet.Name.ToUpper() == "Program Control".ToUpper())
                        query = "role='PMP' or role='PPM'";
                    else if (sheet.Name.ToUpper() == "UR".ToUpper())
                        query = "role like 'UR %'";
                    else if (sheet.Name.ToUpper() == "Planner".ToUpper())
                        query = "role like 'PL %'";
                    DataRow[] myProjectRows = supplierTable.Select(query);
                    int indexRow = 1;
                    int indexRowUR = 1;
                    string[] users=null;
                    foreach (DataRow row in myProjectRows)
                    {
                        switch (sheet.Name.ToUpper())
                        {
                            case "TRANSVERSAL":
                                
                                    sheet.Cells[indexRow, 0].PutValue(row["Project"].ToString());
                                    sheet.Cells[indexRow, 1].PutValue(row["Role"].ToString());
                                    sheet.Cells[indexRow, 2].PutValue(row["ACTIVITYNPI"].ToString());
                                    sheet.Cells[indexRow, 3].PutValue(row["User"].ToString());
                                    break;
                                

                            case "VEHICLE":
                                
                                    sheet.Cells[indexRow, 0].PutValue(row["Project"].ToString());
                                    sheet.Cells[indexRow, 1].PutValue(row["Role"].ToString());
                                    sheet.Cells[indexRow, 2].PutValue(row["User"].ToString());
                                    break;
                                

                            case "POWERTRAIN":
                                
                                    sheet.Cells[indexRow, 0].PutValue(row["Project"].ToString());
                                    sheet.Cells[indexRow, 1].PutValue(row["Role"].ToString());
                                    sheet.Cells[indexRow, 2].PutValue(row["User"].ToString());
                                    break;
                                

                            case "REQUESTER":

                                users = row["User"].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    for (int idx = 0; idx <= users.Length - 1; idx++)
                                        sheet.Cells[idx + 1, 0].PutValue(users[idx].ToString().Trim());
                                    break;
                                

                            case "PROGRAM CONTROL":
                                
                                    users = row["User"].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    

                                    for (int idx = 0; idx <= users.Length - 1; idx++)
                                    {
                                        int idx3 = sheet.Cells.LastCell.Row + 1;
                                        if (row["Role"].ToString() == "PMP")
                                            sheet.Cells[idx3, 0].PutValue("MATERIALI");
                                        else if (row["Role"].ToString() == "PPM")
                                            sheet.Cells[idx3, 0].PutValue("PRESTAZIONI");

                                        sheet.Cells[idx3, 1].PutValue(users[idx].ToString().Trim());
                                    }

                                    break;
                                

                            case "UR":
                                
                                     users = row["User"].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    for (int idx = 0; idx <= users.Length - 1; idx++)
                                    {
                                        sheet.Cells[indexRowUR, 0].PutValue(row["Role"].ToString().Substring(3));
                                        sheet.Cells[indexRowUR, 1].PutValue(users[idx].ToString().Trim());
                                        indexRowUR += 1;
                                    }

                                    break;
                                

                            case  "PLANNER":
                                   users = row["User"].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    for (int idx = 0; idx <= users.Length - 1; idx++)
                                    {
                                        sheet.Cells[indexRowUR, 0].PutValue(row["Role"].ToString().Substring(3));
                                        sheet.Cells[indexRowUR, 1].PutValue(users[idx].ToString().Trim());
                                        indexRowUR += 1;
                                    }

                                    break;
                                
                        }
                        indexRow += 1;
                    }
                }
            }
            


            return workbook;
        }

        private static Workbook GetPriceList(ref eConnection givenConnection, ref DataTable supplierTable)
        {
            string TemplateReportPath = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\export\SupplierPriceList.xlsx");
            Workbook workbookTemplate;
            if (System.IO.File.Exists(TemplateReportPath))
                workbookTemplate = new Aspose.Cells.Workbook(TemplateReportPath, new LoadOptions(LoadFormat.Xlsx));
            else
                throw new Exception("Template Path to export Supplier Price List not found:" + TemplateReportPath);
            Workbook workbook = new Workbook(FileFormatType.Xlsx);
            workbook.Copy(workbookTemplate);


            if (workbook.Worksheets != null)
            {
                Worksheet sheet = workbook.Worksheets[0];
                for (int i = 0; i < supplierTable.Rows.Count; i++)
                {
                    DataRow current = supplierTable.Rows[i];
                    for (int j = 0; j < supplierTable.Columns.Count; j++)
                        sheet.Cells[i+2, j].PutValue(current[j].ToString());
                }
            }
            return workbook;
        }


        private static void setValueByCellName(ref Aspose.Cells.Worksheet givenWS, string cellName, string cellValue, int row, int col)
        {
            var myRanges = getCellRangeByName(ref givenWS, GetRangeName(cellName));
            if (myRanges != null)
            {
                Cell myCell = givenWS.Cells[myRanges.FirstRow, myRanges.FirstColumn];
                Style myStyle = myCell.GetStyle();
                if (eRdAHelper.IsNumeric(cellValue))
                    givenWS.Cells[myRanges.FirstRow + row, myRanges.FirstColumn + col].PutValue(eRdAHelper.GetDoubleValue(cellValue));
                else
                    givenWS.Cells[myRanges.FirstRow + row, myRanges.FirstColumn + col].PutValue(cellValue);
            }
        }


        private static Range getCellRangeByName(ref Aspose.Cells.Worksheet givenWS, string cellName)
        {
            return givenWS.Workbook.Worksheets.GetRangeByName(cellName);
        }
        private static string GetRangeName(string givenName)
        {
            string myNewName = givenName;

            myNewName = myNewName.Replace("&", "");
            myNewName = myNewName.Replace(" ", "_");
            myNewName = myNewName.Replace("/", "_");
            myNewName = myNewName.Replace(@"\", "_");

            return myNewName;
        }

        #endregion

    }


}


