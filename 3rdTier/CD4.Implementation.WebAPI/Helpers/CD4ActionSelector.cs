﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;

namespace CD4.Implementation.WebAPI.Helpers
{
    public class CD4ActionSelector: ApiControllerActionSelector
    {
        private static readonly string HttpMethodQueryParameter = "httpMethod";

        public override HttpActionDescriptor SelectAction(HttpControllerContext controllerContext)
        {
            if (controllerContext.RouteData.Values.ContainsKey("MS_SubRoutes"))
            {
                var routeData = controllerContext.RouteData;
                foreach (var route in (IHttpRouteData[])routeData.Values["MS_SubRoutes"])
                {
                    var httpActionDescriptor = (HttpActionDescriptor[]) route.Route.DataTokens["actions"];
                    var controllerDescriptor = httpActionDescriptor[0].ControllerDescriptor;

                    if (controllerDescriptor.ControllerName.EndsWith("Override"))
                    {
                        if (!httpActionDescriptor[0].SupportedHttpMethods.Contains(controllerContext.Request.Method))
                        {
                            // Todo if same route with different methods
                            throw new Exception("Http Method not the same");
                        }
                        return httpActionDescriptor[0];
                    }
                }
            }

            return base.SelectAction(controllerContext);
        }
    }
}