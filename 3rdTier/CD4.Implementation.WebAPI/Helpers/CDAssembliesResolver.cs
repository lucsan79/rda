﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http.Dispatcher;

namespace CD4.Implementation.WebAPI.Helpers
{
    public class CDAssembliesResolver: DefaultAssembliesResolver
    {
        public virtual ICollection<Assembly> GetAssemblies()
        {
            ICollection<Assembly> baseAssemblies = base.GetAssemblies();
            List<Assembly> assemblies = new List<Assembly>(baseAssemblies);
            var controllersAssembly = Assembly.Load("CD4.WebAPI");
            baseAssemblies.Add(controllersAssembly);
            return baseAssemblies;
        }
    }
}