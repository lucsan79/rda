﻿using CD4.ApiExtensions.Helpers;
using CD4.Implementation.WebAPI.ViewModels;
using Parallaksis.ePLMSM40;
using Parallaksis.ePLMSM40.Defs;
using Parallaksis.M40.Configurator;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using static Parallaksis.ePLMSM40.Defs.eDataListElementModule;
using static Parallaksis.ePLMSM40.Defs.eErrorDefs;
using static Parallaksis.ePLMSM40.Defs.eAdminDataDefs;
using static Parallaksis.ePLMSM40.Defs.eStructDefs;
using System.Reflection;
using System.Text;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.IO;

namespace CD4.WebAPI.Helpers
{

    public class eRdAHelper
    {

        public const int ERR_NO_TMP_DIR = 100100;
        public const int ERR_DOWNLOAD_CONFIG = 100101;
        public const int ERR_DOWNLOADING_CONFIG = 100102;
        public const int ERR_DOWNLOAD_CONFIGFILE = 100103;
        public const String KEY_DIALOG_XML_FILENAME = "dialog configuration";
        public const String KEY_DIALOG_VB_FILENAME = "function configuration";

        #region "DIALOG HELPER"
        public static string DeployDialogConfiguration(ref eConnection givenDBAConnection, int givenUserId, ref eBusinessData givenDialogConfiguration)
        {
            string retPath = "";
            string myConfigurationPath = System.IO.Path.Combine(givenDBAConnection.ePLMS_HOME, "tmp");
            string downloadFilePath = "";
            if (!System.IO.Directory.Exists(myConfigurationPath))
                System.IO.Directory.CreateDirectory(myConfigurationPath);

            if (!System.IO.Directory.Exists(myConfigurationPath))
                throw new Exception(givenDBAConnection.Core.getError(ERR_NO_TMP_DIR));

            myConfigurationPath = System.IO.Path.Combine(myConfigurationPath, givenUserId.ToString());
            if (System.IO.Directory.Exists(myConfigurationPath))
                System.IO.Directory.Delete(myConfigurationPath, true);

            if (!System.IO.Directory.Exists(myConfigurationPath))
                System.IO.Directory.CreateDirectory(myConfigurationPath);

            if (!System.IO.Directory.Exists(myConfigurationPath))
                throw new Exception(givenDBAConnection.Core.getError(ERR_NO_TMP_DIR));

            eBusinessDataFile[] myeFileList = null;
            givenDialogConfiguration.getBusinessDataFileList(true, ref myeFileList);


            if (myeFileList != null)
            {
                try
                {
                    System.IO.Directory.Delete(myConfigurationPath, true);
                    System.IO.Directory.CreateDirectory(myConfigurationPath);
                }
                catch (Exception e) { }

                System.Net.WebClient myWebClient = new System.Net.WebClient();
                foreach (eBusinessDataFile file in myeFileList)
                {
                    if (file.Description.ToLower() == "dialog configuration" | file.Description.ToLower() == "function configuration")
                    {

                        //PEZZOTTO
                        //string basepath = @"P:\etc\Parallaksis\Collaboration Desktop\Vaults\eRDA";
                        //downloadFilePath = System.IO.Path.Combine(myConfigurationPath, file.Name);

                        //string fileToCopy = System.IO.Path.Combine(basepath, file.cryptedName);
                        //System.IO.Directory.Delete(myConfigurationPath, true);
                        //System.IO.Directory.CreateDirectory(myConfigurationPath);
                        //System.IO.File.Copy(fileToCopy, downloadFilePath);
                        //if (downloadFilePath.EndsWith(".xml"))
                        //    retPath = downloadFilePath;




                        downloadFilePath = System.IO.Path.Combine(myConfigurationPath, file.Name);
                        string returnFileURI = "";
                        
                        if (file.copyOut(ref returnFileURI) == Parallaksis.ePLMSM40.Defs.eErrorDefs.ePLMS_OK)
                        {
                            if (string.IsNullOrEmpty(returnFileURI ))
                                throw new Exception("Error downloading dialog configuration setting");
                            myWebClient.DownloadFile(returnFileURI, downloadFilePath);
                            if (downloadFilePath.EndsWith(".xml"))
                                retPath = downloadFilePath;
                        }
                        else
                            throw new Exception(givenDialogConfiguration.Connection.errorMessage);
                    }
                }
            }
            else
                throw new Exception(givenDBAConnection.Core.getError(ERR_DOWNLOAD_CONFIG));

            if (string.IsNullOrEmpty(retPath))
                throw new Exception(givenDBAConnection.Core.getError(ERR_DOWNLOAD_CONFIG));
            return retPath;
        }

        public static Dictionary<string, string> StoreRDAAttribute(string tmpPath, DataSet dummyDataSet, ref ConfiguratorManager myConfigurationManager)
        {
            Dictionary<string, string> myListPropery = new Dictionary<string, string>();

            if (dummyDataSet.Tables != null)
            {
                DataTable myTableOption = dummyDataSet.Tables["QAOption"];
                if (myTableOption != null)
                {
                    DataRow[] mySelectedRow;
                    // prendiamo le CP
                    // mySelectedRow = myTableOption.Select("selected='true' and hasCP='true' and hasCA='false' and hasTE='false'")
                    mySelectedRow = myTableOption.Select("selected='true' and hasCP='true' and hasTE='false'");

                    if (mySelectedRow.Length > 0)
                    {
                        DataTable myTableCP = dummyDataSet.Tables["QAFeatures"];
                        foreach (DataRow cadRow in mySelectedRow)
                        {
                            string number = cadRow["number"].ToString();

                            DataRow[] subFeature = myTableCP.Select("dtRefNumber='" + number + "'");
                            foreach (DataRow rowF in subFeature)
                            {
                                string code = rowF["code"].ToString();
                                string codeVal = rowF["value"].ToString();

                                if (myConfigurationManager.IsAFunction(codeVal))
                                    codeVal = myConfigurationManager.CalculateCPFunction(codeVal);
                                if (!myListPropery.ContainsKey(code))
                                    myListPropery.Add(code, codeVal);
                                else
                                    myListPropery[code] = codeVal;
                            }
                        }
                    }

                    // prendiamo le CA
                    // mySelectedRow = myTableOption.Select("selected='true' and hasCP='false' and hasCA='true' and hasTE='false'")
                    mySelectedRow = myTableOption.Select("selected='true' and hasCA='true' and hasTE='false'");

                    if (mySelectedRow.Length > 0)
                    {
                        DataTable myTableCP = dummyDataSet.Tables["QAInput"];
                        foreach (DataRow cadRow in mySelectedRow)
                        {
                            string number = cadRow["number"].ToString();

                            DataRow[] subFeature = myTableCP.Select("dtRefNumber='" + number + "'");
                            foreach (DataRow rowF in subFeature)
                            {
                                string code = rowF["code"].ToString();
                                string codeVal = rowF["value"].ToString();

                                if (!myListPropery.ContainsKey(code))
                                    myListPropery.Add(code, codeVal);
                                else
                                    myListPropery[code] = codeVal;
                            }
                        }
                    }


                    // prendiamo le TE
                    // mySelectedRow = myTableOption.Select("selected='true' and hasCP='false' and hasCA='false' and hasTE='true'")
                    mySelectedRow = myTableOption.Select("selected='true'  and hasTE='true'");

                    if (mySelectedRow.Length > 0)
                    {
                        DataTable myTableCP = dummyDataSet.Tables["QAExternalReference"];
                        foreach (DataRow cadRow in mySelectedRow)
                        {
                            string number = cadRow["number"].ToString();

                            DataRow[] subFeature = myTableCP.Select("dtRefNumber='" + number + "'");
                            foreach (DataRow rowF in subFeature)
                            {
                                string code = rowF["code"].ToString();
                                string codeVal = rowF["selectedValue"].ToString();
                                if (!myListPropery.ContainsKey(code))
                                    myListPropery.Add(code, codeVal);
                                else
                                    myListPropery[code] = codeVal;
                            }
                        }
                    }
                }
                return myListPropery;
            }


            return null/* TODO Change to default(_) if this is not a reference type */;
        }

        public static int AddFileToRDA(CacheFile givenFile, ref eConnection givenConnection, ref eBusinessData myBusinessData)
        {

            int retValue = 0;
            eFileService.eFileService myeFileService;
            eVault myeVault;
            eBusinessDataFile myeBDFile;

            string tempFileName = "";


            if (myBusinessData != null)
            {
                myeFileService = new eFileService.eFileService();
                myeVault = new eVault(ref givenConnection);
                myBusinessData.getCandidateVault(ref myeVault);
                myeFileService.Url = myeVault.fileServiceURL + "/eFileService.asmx";
                byte[] fileContent = (byte[])givenFile.data;
                int rc= myeFileService.UploadSingleFile(fileContent, ref tempFileName);
                myeBDFile = new eBusinessDataFile(ref givenConnection, myBusinessData);
                myeBDFile.Name = givenFile.Name;
                myeBDFile.Description = givenFile.Description;

                myeBDFile.Type = ePLMSFileTypeSource;
                myeBDFile.Source = tempFileName;
                myeBDFile.businessDataFileType = givenFile.FileType;

                givenConnection.pushMessageFlag = true;

                eBusinessDataFile[] oldFiles = new eBusinessDataFile[] { };
                myBusinessData.getBusinessDataFileList(true, ref oldFiles);
                if (oldFiles != null)
                {
                    for (var index = 0; index < oldFiles.Length; index++)
                    {
                        eBusinessDataFile file = oldFiles[index];
                        if (file.Name == myeBDFile.Name)
                            myBusinessData.removeBusinessDataFile(ref file);
                    }
                }
                ArrayList attributes = new ArrayList();
                retValue = myBusinessData.addBusinessDataFile(ref myeBDFile, ref attributes);
                givenConnection.pushMessageFlag = false;
            }

            return retValue;
        }

        #endregion

        #region "DIALOG SUPPORT FUNCTION / RDA ADMIN LIST MANAGER"
        public static string GetLocationList(ref eConnection givenConnection)
        {
            string returnList = "";
            string myQuery = ("SELECT  eplms.eListItem.eValue as eName " + ("from eplms.eListItem INNER Join " + ("eplms.eList ON eplms.eListItem.eListId = eplms.eList.eId " + "Where (eplms.eList.eName = N'Locations')")));
            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                foreach (DataRow row in mytable.Rows)
                {
                    returnList += string.Format("|{0}", row["eName"].ToString().Trim());
                }

                if ((returnList.Length > 1))
                {
                    returnList = returnList.Substring(1);
                }

            }

            return returnList;
        }
        public static string GetList(ref eConnection givenConnection, string givenListName, string givenStartName)
        {
            string returnList = "";
            if ((givenListName == "rdaw1_professionalrole"))
            {
                givenListName = "Professional Role";
            }
            else if ((givenListName == "rdaw1_professionalrole_pwt"))
            {
                givenListName = "PWT Professional Role";
            }
            else if ((givenListName == "rdaw1_professionallevel"))
            {
                givenListName = "Professional Level";
            }

            string myQuery = ("SELECT  eplms.eListItem.eValue as eName " + ("from eplms.eListItem INNER Join " + ("eplms.eList ON eplms.eListItem.eListId = eplms.eList.eId " + "Where (eplms.eList.eName = N'{0}' and eplms.eListItem.eValue like '{1}%')")));
            myQuery = string.Format(myQuery, givenListName, givenStartName);
            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                bool na_item_entry = false;
                foreach (DataRow row in mytable.Rows)
                {
                    if (row["eName"].ToString().ToLower().Equals("n/a"))
                        na_item_entry = true;

                    returnList += string.Format("|{0}", row["eName"].ToString().Trim());
                }

                if (na_item_entry)
                    returnList += "|N/A";

                if ((returnList.Length > 1))
                    returnList = returnList.Substring(1);

            }

            return returnList;
        }
        public static string GetNestedList(ref eConnection givenConnection, string basicList, string firstLevelValue, string secondLevelValue, string searchParam, string competence)
        {
            string returnList = "";
            string myQuery = "";
            bool enableND = false;
            competence = (competence + "");
            if (string.IsNullOrEmpty(firstLevelValue))
            {
                myQuery = ("SELECT eplms.eListItem.* " + ("FROM   eplms.eListItem INNER JOIN " + ("\t\teplms.eList ON eplms.eListItem.eListId = eplms.eList.eId " + "WHERE  (eplms.eList.eName = N'{0}')")));
                if (!string.IsNullOrEmpty(searchParam))
                    myQuery += " AND (eplms.eListItem.eValue LIKE '{1}%') ";

                myQuery = string.Format(myQuery, basicList, searchParam);
            }
            else if ((!string.IsNullOrEmpty(firstLevelValue)
                        && string.IsNullOrEmpty(secondLevelValue)))
            {
                myQuery = ("SELECT eplms.eListItem.* " + ("FROM\teplms.eListItem AS eListItem_1 INNER JOIN " + ("\t\teplms.eList ON eListItem_1.eListId = eplms.eList.eId INNER JOIN " + ("\t\teplms.eListItem ON eListItem_1.eValueId = eplms.eListItem.eListId " + "WHERE        (eplms.eList.eName = N'{0}') AND (eListItem_1.eValue = N'{1}')"))));
                if (!string.IsNullOrEmpty(searchParam))
                    myQuery += " AND (eplms.eListItem.eValue LIKE '%{2}%') ";

                myQuery = string.Format(myQuery, basicList, firstLevelValue, searchParam);
                if ((basicList == "PROJECT_CONTENTS"))
                    enableND = true;

            }

            List<string> parseList = null;
            if ((basicList.EndsWith("PROJECT") && (competence != "")))
                parseList = GetProjectsInCompetence(ref givenConnection, competence);

            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                bool na_item_entry = false;
                foreach (DataRow row in mytable.Rows)
                {
                    if (row["eValue"].ToString().ToLower().Equals("n/a"))
                        na_item_entry = true;

                    if (parseList != null)
                        if (!parseList.Contains(row["eValue"]))
                            continue;


                    returnList += string.Format("|{0}", row["eValue"].ToString().Trim());
                }

                if (na_item_entry)
                    returnList += "|N/A";

                if ((returnList.Length > 1))
                    returnList = returnList.Substring(1);

            }

            if (((returnList == "") && enableND))
                returnList = "N.D.";

            return returnList;
        }
        public static string GetRemedySupplierList(ref eConnection givenConnection, string givenFilter, string givenBrand, bool isTest)
        {
            string returnList = "";
            string myQuery = ("Select TOP(100) CODE As eName, NAME As eDescription from dbo.FORNITORI where BRANDCONDITION TESTCONDITION IsRemedy=1" +
            " and NAME Like '%" + (givenFilter + "%'  and (Consolidamento = 1 and ConsolidamentoFrom<=GETDATE() and ConsolidamentoTo>=GETDATE()) order by NAME"));
            if (!string.IsNullOrEmpty(givenBrand))
            {
                myQuery = myQuery.Replace("BRANDCONDITION", ("(BRAND IS NULL OR BRAND like '%" + (givenBrand + "%') AND ")));
            }
            else
            {
                myQuery = myQuery.Replace("BRANDCONDITION", "BRAND IS NULL AND ");
            }
            if(isTest)
                myQuery = myQuery.Replace("TESTCONDITION", "REPLACE(CODE,' ','') in (select fornitore from dbo.LISTINO_FORNITORI where articolo like '%TEST%') and ");
            else
                myQuery = myQuery.Replace("TESTCONDITION", " ");
            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                foreach (DataRow row in mytable.Rows)
                {
                    if ((!string.IsNullOrEmpty(givenFilter) && (givenFilter.Trim() != "")))
                        if (!row["eDescription"].ToString().ToLower().Contains(givenFilter.ToLower()))
                            continue;

                    returnList += string.Format(";{0}|{1}", row["eName"].ToString().Trim(), row["eDescription"].ToString().Trim());
                }

                if ((returnList.Length > 1))
                    returnList = returnList.Substring(1);
            }
            return returnList;
        }
        public static string GetSupplierList(ref eConnection givenConnection, string givenFilter)
        {
            // var filterValue = "PROJECT:{0}|AREA:{1}|CBS:{2}|SYSTEM:{3}";
            string retValue = "";
            string myQuery = "Select TOP(100) CODE As eName, NAME As eDescription from dbo.FORNITORI where NAME Like'{0}%' and (Consolidamento = 1 and ConsolidamentoFrom<=GETDATE() and ConsolidamentoTo>=GETDATE())  order by NAME";
            string ValueToFilter = ("" + givenFilter).Trim();
            if (!string.IsNullOrEmpty(ValueToFilter))
                ValueToFilter = eRdAHelper.FormatSupplierNameForQuery(givenFilter);
            myQuery = string.Format(myQuery, ValueToFilter);

            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            var myList = new List<string>();
            if (myTable != null)
                foreach (DataRow row in myTable.Rows)
                    retValue += String.Format(";{0}|{1}", row["eName"].ToString().Trim(), row["eDescription"].ToString().Trim());

            if (retValue.Length > 1)
                retValue = retValue.Substring(1);

            return retValue;
        }
        public static string GetATCList(ref eConnection givenConnection, string givenFilter)
        {
            // var filterValue = "PROJECT:{0}|AREA:{1}|CBS:{2}|SYSTEM:{3}";

            string myQuery = "SELECT CODE From ATC Where (CODE like '%{0}%')";

            myQuery = string.Format(myQuery, givenFilter);

            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            var myList = new List<string>();
            if (myTable != null)
            {
                foreach (DataRow row in myTable.Rows)
                {
                    if (!myList.Contains(row["CODE"]))
                        myList.Add(row["CODE"].ToString());
                }
            }


            return string.Join("|", myList.ToArray());
        }
        public static string GetUserWhereReferencedByGroupList(ref eConnection givenConnection)
        {

            eUser myUser;
            string retValue = "null";
            var referencedGroups = new ArrayList();
            var myeGroup = new eGroup(ref givenConnection);


            myUser = new eUser(ref givenConnection);
            if (myUser.Load(givenConnection.currentUserName) == ePLMS_OK)
            {
                myUser.getWhereReferencedByGroupList(ref referencedGroups);
                if (referencedGroups != null)
                {
                    foreach (eDataListElement myGroup in referencedGroups)
                    {
                        if (myeGroup.Load(myGroup.Name) == eErrorDefs.ePLMS_OK)
                        {
                            if (string.IsNullOrEmpty(retValue))
                                retValue += "|";
                            retValue += myeGroup.Name;
                            if (myeGroup.Description != "")
                                retValue += " - " + myeGroup.Description;
                        }
                    }
                }
            }
            return retValue;
        }
        public static string GetWBSWizardItems(ref eConnection givenConnection, eDialogListItem givenDialogListItem)
        {
            string myQuery = "Select distinct @BASIC from MASAD_WBS";
            string myVehicle = "CDC='@Param'\\AREA='@Param'\\SYSTEM='@Param'\\CBS='@Param'\\WBS='@Param'";
            string mypwt = "(CDC IS NULL or CDC ='')\\AREA='@Param'\\SYSTEM='@Param'\\CBS='@Param'\\WBS='@Param'";
            string myTRANSVERSAL = "CDC='@Param'\\AREA='@Param'\\SYSTEM IS NULL\\CBS='@Param'\\WBS='@Param'";
            string[] myArray = null;
            switch (givenDialogListItem.competence.ToLower())
            {
                case "vehicle":
                    myArray = myVehicle.Split("\\".ToCharArray());
                    
                    break;
                case "powertrain":
                    myArray = mypwt.Split("\\".ToCharArray());
                    break;
                case "transversal":
                    myArray = myTRANSVERSAL.Split("\\".ToCharArray());
                    break;
            }
            myQuery = myQuery.Replace("@BASIC", givenDialogListItem.param);
            string where = (" COMPETENCE=\'"
                        + (givenDialogListItem.competence + "\'"));
            for (int idx = 0; (idx
                        <= (myArray.Length - 1)); idx++)
            {
                string givenParamName = "";
                if (myArray[idx].Contains("="))
                {
                    givenParamName = myArray[idx].Split("=".ToCharArray())[0];
                }
                else
                {
                    givenParamName = myArray[idx].Split(" ".ToCharArray())[0];
                }

                if ((givenParamName == givenDialogListItem.param))
                {
                    break;
                }

                if (!string.IsNullOrEmpty(where))
                {
                    where += " and ";
                }

                if (myArray[idx].Contains("="))
                {
                    foreach (string item in myArray)
                    {
                        string key = myArray[idx].Split("=".ToCharArray())[0];
                        string value = "";
                        switch (key.ToUpper())
                        {
                            case "AREA":
                                value = givenDialogListItem.area;
                                break;
                            case "CDC":
                                value = givenDialogListItem.cdc;
                                break;
                            case "CBS":
                                value = givenDialogListItem.cbs;
                                break;
                            case "WBS":
                                value = givenDialogListItem.wbs;
                                break;
                            case "SYSTEM":
                                value = givenDialogListItem.system;
                                break;
                        }
                        if (string.IsNullOrEmpty(value))
                            where = myArray[idx].Replace("=\'@Param\'", " IS NULL ");
                        else
                            where = myArray[idx].Replace("@Param", value);
                    }
                    //string value = ("" + Request.Params(myArray[idx].Split("=")[0].ToLower));
                    //if ((value == ""))
                    //{
                    //    myArray[idx].Replace("=\'@Param\'", " IS NULL ");
                    //}
                    //else
                    //{
                    //    myArray[idx].Replace("@Param", value);
                    //}

                }
                else
                {
                    where += myArray[idx];
                }

                if (myArray[idx].ToLower().StartsWith(givenDialogListItem.param.ToLower()))
                    break;
            }

            string returnList = "";
            DataTable mytable = GetProjectWBS(ref givenConnection, givenDialogListItem.project, givenDialogListItem.competence, givenDialogListItem.param, where);
            // ePortalManager.ExecuteSql(myQuery)
            if (mytable != null)
            {
                mytable.DefaultView.Sort=(givenDialogListItem.param + " ASC");
                mytable = mytable.DefaultView.ToTable();
                foreach (DataRow row in mytable.Rows)
                {
                    if (!row[givenDialogListItem.param].ToString().ToLower().Contains(givenDialogListItem.searchItem.ToLower()))
                        continue;
                    returnList += string.Format("|{0}", row[givenDialogListItem.param].ToString().Trim());
                }

                // givenSearchParam
                // For Each row As DataRow In mytable.Rows
                //     returnList &= String.Format("|{0}", row(givenParam).ToString.Trim)
                // Next
                if ((returnList.Length > 1))
                {
                    returnList = returnList.Substring(1);
                }

            }

            return returnList;
        }
        public string GetDefaultWBSWizardItems(ref eConnection givenConnection, string competence, string givenParam, string givenProject)
        {
            string myQuery = "Select distinct @BASIC from MASAD_WBS";
            string myVehicle = "CDC=\'@Param\'\\AREA=\'@Param\'\\SYSTEM=\'@Param\'\\CBS=\'@Param\'\\WBS=\'@Param\'";
            string mypwt = "CDC IS NULL\\AREA=\'@Param\'\\SYSTEM=\'@Param\'\\CBS=\'@Param\'\\WBS=\'@Param\'";
            string myTRANSVERSAL = "CDC=\'@Param\'\\AREA=\'@Param\'\\SYSTEM IS NULL\\CBS=\'@Param\'\\WBS=\'@Param\'";
            string[] myArray = null;
            switch (competence.ToLower())
            {
                case "vehicle":
                    myArray = myVehicle.Split("\\".ToCharArray());
                    break;
                case "powertrain":
                    myArray = mypwt.Split("\\".ToCharArray());
                    break;
                case "transversal":
                    myArray = myTRANSVERSAL.Split("\\".ToCharArray());
                    break;
            }
            myQuery = myQuery.Replace("@BASIC", givenParam);
            string where = (" COMPETENCE=\'"
                        + (competence + "\'"));
            for (int idx = 0; (idx
                        <= (myArray.Length - 1)); idx++)
            {
                string givenParamName = "";
                if (myArray[idx].Contains("="))
                {
                    givenParamName = myArray[idx].Split("=".ToCharArray())[0];
                }
                else
                {
                    givenParamName = myArray[idx].Split(" ".ToCharArray())[0];
                }

                if ((givenParamName == givenParam))
                {
                    break;
                }

                if (!string.IsNullOrEmpty(where))
                {
                    where += " and ";
                }

                if (myArray[idx].Contains("="))
                {
                    string value = "";// ("" + Request.Params(myArray[idx].Split("="where)[0].ToLower));
                    if ((value == ""))
                    {
                        where += myArray[idx].Replace("=\'@Param\'", " IS NULL ");
                    }
                    else
                    {
                        where += myArray[idx].Replace("@Param", value);
                    }

                }
                else
                    where += myArray[idx];

                if (myArray[idx].ToLower().StartsWith(givenParam.ToLower()))
                    break;

            }

            string returnList = "";
            DataTable mytable = GetProjectWBS(ref givenConnection, givenProject, competence, givenParam, where);
            // ePortalManager.ExecuteSql(myQuery)
            if (mytable != null && mytable.Rows.Count == 1)
            {
                foreach (DataRow row in mytable.Rows)
                    string.Format("|{0}", row[givenParam].ToString().Trim());

                if ((returnList.Length > 1))
                    returnList = returnList.Substring(1);
            }

            return returnList;
        }
        public static string GetProfessionalRolesOnContext(ref eConnection givenConnection, string competence, string context, string supplier, string searchitem, bool isTest)
        {
            string myQuery = "Select distinct [Descrizione Completa],[Descrizione] from dbo.[LISTINO_FORNITORI] where Fornitore='{0}' and [Descrizione Completa] like '%{1}%'";
            if(isTest && string.IsNullOrEmpty(supplier))
                myQuery = "Select distinct [Descrizione Completa],[Descrizione] from dbo.[LISTINO_FORNITORI] where [Descrizione Completa] like '%{1}%'";

            if ((context != "REMEDY"))
                myQuery = "Select distinct [Descrizione Completa],[Descrizione] from dbo.[LISTINO_FORNITORI] where [Descrizione Completa] like '%{1}%'";

            if (((competence.ToUpper() == "PWT") || (competence.ToUpper() == "POWERTRAIN")))
                myQuery = (myQuery + " and [PWT COMPETENCE]=1");
            
            if (isTest)
                myQuery = myQuery + " and articolo like '%TEST%'";
            else
                myQuery = myQuery + " and articolo not like '%TEST%'";

            myQuery = string.Format(myQuery, supplier, searchitem);
            string returnList = "";
            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                foreach (DataRow row in mytable.Rows)
                {
                    if (!string.IsNullOrEmpty(row[0].ToString().Trim()))
                        returnList += string.Format("|{0}", row[0].ToString().Trim());
                    else
                        returnList += string.Format("|{0}", row[1].ToString().Trim());
                }
                if ((returnList.Length > 1))
                    returnList = returnList.Substring(1);
            }

            return returnList;
        }
        public string GetSitesNOTFCA(ref eConnection givenConnection, string competence, string context, string supplier, string professionalRole, string searchitem)
        {
            string myQuery = "Select distinct [Articolo] from dbo.[LISTINO_FORNITORI] where Fornitore = '{0}' and [ARTICOLO] like '" +
            "%{2}%' and [Descrizione Completa]='{1}'";
            if ((context != "REMEDY"))
            {
                myQuery = "Select distinct [Articolo] from dbo.[LISTINO_FORNITORI] where [ARTICOLO] like '%{2}%'  and [Descrizione Completa]='{1}'";
            }

            if (((competence.ToUpper() == "PWT") || (competence.ToUpper() == "POWERTRAIN")))
            {
                myQuery = (myQuery + " and [PWT COMPETENCE]=1");
            }

            myQuery = string.Format(myQuery, supplier, professionalRole, searchitem);
            string returnList = "";
            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                foreach (DataRow row in mytable.Rows)
                {
                    object suffix = row[0].ToString().Trim().Substring(Math.Max(0, (row[0].ToString().Trim().Length - 2)));
                    object suffixAlternative = row[0].ToString().Trim().Substring(Math.Max(0, (row[0].ToString().Trim().Length - 3)));
                    string value = "";
                    switch (suffix.ToString().ToUpper())
                    {
                        case "ON":
                            value = "Onsite";
                            break;
                        case "OF":
                            value = "Offsite";
                            break;
                        case "BL":
                            value = "Balocco";
                            break;
                    }
                    if (!returnList.Contains(value))
                    {
                        returnList += String.Format("|{0}", value);
                    }

                    returnList += string.Format("|{0}", value);
                }

                if ((returnList.Length > 1))
                {
                    returnList = returnList.Substring(1);
                }

            }

            return returnList;
        }
        public string GetSitesFCA(ref eConnection givenConnection, string competence, string context, string supplier, string professionalRole, string searchitem)
        {
            string myQuery = "Select distinct [Articolo] from dbo.[LISTINO_FORNITORI] where Fornitore = \'{0}\' and [ARTICOLO] like \'" +
            "%{2}%\' and [Descrizione Completa]=\'{1}\'";
            if ((context != "REMEDY"))
            {
                myQuery = "Select distinct [Articolo] from dbo.[LISTINO_FORNITORI] where [ARTICOLO] like \'%{2}%\'  and [Descrizio" +
                "ne Completa]=\'{1}\'";
            }

            if (((competence.ToUpper() == "PWT") || (competence.ToUpper() == "POWERTRAIN")))
            {
                myQuery = (myQuery + " and [PWT COMPETENCE]=1");
            }

            myQuery = string.Format(myQuery, supplier, professionalRole, searchitem);
            string returnList = "";
            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                foreach (DataRow row in mytable.Rows)
                {
                    object suffix = row[0].ToString().Trim().Substring(Math.Max(0, (row[0].ToString().Trim().Length - 2)));
                    object suffixAlternative = row[0].ToString().Trim().Substring(Math.Max(0, (row[0].ToString().Trim().Length - 3)));
                    string value = "";
                    switch (suffix.ToString().ToUpper())
                    {
                        case "CA":
                            if ((suffixAlternative.ToString().ToUpper() == "FCA"))
                            {
                                if (!returnList.Contains("Onsite"))
                                    returnList += String.Format("|{0}", "Onsite");

                                if (!returnList.Contains("Offsite"))
                                    returnList += String.Format("|{0}", "Offsite");
                            }

                            break;
                    }
                    if (!returnList.Contains(value))
                        returnList += String.Format("|{0}", value);

                }

                if ((returnList.Length > 1))
                    returnList = returnList.Substring(1);

            }

            return returnList;
        }
        public static string GetSites(ref eConnection givenConnection, string competence, string context, string supplier, string professionalRole, string searchitem)
        {
            string myQuery = "Select distinct [Articolo] from dbo.[LISTINO_FORNITORI] where Fornitore = '{0}' and [ARTICOLO] like '%{2}%' and ([Descrizione Completa] like '{3}' or [Descrizione]='{1}')";
            if (String.IsNullOrEmpty(supplier))
                myQuery = myQuery.Replace("Fornitore = '{0}' and", "");


            if ((context != "REMEDY"))
                myQuery = "Select distinct [Articolo] from dbo.[LISTINO_FORNITORI] where [ARTICOLO] like \'%{2}%\'  and ([Descrizione Completa] like '{3}' or [Descrizione]='{1}')";

            if (((competence.ToUpper() == "PWT") || (competence.ToUpper() == "POWERTRAIN")))
                myQuery = (myQuery + " and [PWT COMPETENCE]=1");

            string fullDescr = professionalRole.Replace(" ", "%").Replace(Environment.NewLine, "%").Replace(".","%");
            myQuery = string.Format(myQuery, supplier, professionalRole, searchitem, fullDescr);
            string returnList = "";
            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                foreach (DataRow row in mytable.Rows)
                {
                    object suffix = row[0].ToString().Trim().Substring(Math.Max(0, (row[0].ToString().Trim().Length - 2)));
                    object suffixAlternative = row[0].ToString().Trim().Substring(Math.Max(0, (row[0].ToString().Trim().Length - 3)));
                    string value = "";
                    switch (suffix.ToString().ToUpper())
                    {
                        case "ON":
                            value = "Onsite";
                            break;
                        case "OF":
                            value = "Offsite";
                            break;
                        case "BL":
                            value = "Balocco";
                            break;
                        case "CA":
                            if (suffixAlternative.ToString().ToUpper() == "FCA")
                            {
                                if (!returnList.Contains("Onsite"))
                                    returnList += string.Format("|{0}", "Onsite");

                                if (!returnList.Contains("Offsite"))
                                    returnList += string.Format("|{0}", "Offsite");
                            }
                            break;
                    }
                    if (!returnList.Contains(value))
                        returnList += string.Format("|{0}", value);
                }

                if ((returnList.Length > 1))
                    returnList = returnList.Substring(1);
            }

            return returnList;
        }
        public static string GetPrice(ref eConnection givenConnection, string competence, string context, string supplier, string professionalRole, string site, string days, string startdate, string enddate, bool isTest)
        {
            double numDays = eRdAHelper.GetDoubleValue(days);
            //double price = 0;
            string myQuery = "Select * from dbo.[LISTINO_FORNITORI] where Fornitore = '{0}' and ([Descrizione]='{1}' or [Descrizione Completa]='{1}') and (Articolo like'%{2}' or Articolo like'%FCA')";
            if ((competence.ToUpper() ?? "") == "PWT" | (competence.ToUpper() ?? "") == "POWERTRAIN")
                myQuery = myQuery + " and [PWT COMPETENCE]=1";
            if (isTest)
                myQuery = myQuery + " and articolo like '%TEST%'";

            myQuery = myQuery + " order by Seq DESC";
            if (string.IsNullOrEmpty(site))
                site = "";
            switch (site.ToUpper())
            {
                case "OFFSITE":
                    {
                        site = "OF";
                        break;
                    }

                case "ONSITE":
                    {
                        site = "ON";
                        break;
                    }

                case "BALOCCO":
                    {
                        site = "BL";
                        break;
                    }
            }
            myQuery = string.Format(myQuery, supplier, professionalRole, site);

            string returnValue = "";
            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                if (mytable.Rows.Count > 0)
                {
                    DataRow currentRow = null;
                    string myDateToVerify = enddate;
                    myDateToVerify = myDateToVerify.Replace("/", ".");
                    myDateToVerify = myDateToVerify.Split(".".ToCharArray())[2] + myDateToVerify.Split(".".ToCharArray())[1] + myDateToVerify.Split(".".ToCharArray())[0];

                    foreach (DataRow row in mytable.Rows)
                    {
                        string myStart = row["Data Inizio Validità"].ToString();
                        myStart = myStart.Split("/".ToCharArray())[2] + myStart.Split("/".ToCharArray())[1] + myStart.Split("/".ToCharArray())[0];

                        string myEnd = row["Data Fine Validità"].ToString();
                        myEnd = myEnd.Split("/".ToCharArray())[2] + myEnd.Split("/".ToCharArray())[1] + myEnd.Split("/".ToCharArray())[0];

                        if (myEnd.CompareTo(myDateToVerify) >= 0 & myDateToVerify.CompareTo(myStart) >= 0)
                        {
                            currentRow = row;
                            break;
                        }
                    }

                    double priceDays = eRdAHelper.GetDoubleValue(currentRow["Prezzo"].ToString());
                    double fullAmount = priceDays;
                    string divisa = currentRow["Descrizione Divisa"].ToString().Trim();
                    string CodiceArticolo = currentRow["Articolo"].ToString().Trim();
                    //if(!isTest)
                    fullAmount = priceDays * numDays;
                    string dailyCost = "";
                    if ((divisa.ToLower() ?? "") == "euro")
                    {
                        dailyCost = string.Format("{0}", priceDays.ToString("C2", new CultureInfo("it-IT")));
                        returnValue = string.Format("{0}", fullAmount.ToString("C2", new CultureInfo("it-IT")));
                        returnValue += "|" + dailyCost;
                    }
                    else if ((divisa.ToLower() ?? "") == "dollaro")
                    {
                        dailyCost = string.Format("{0}", priceDays.ToString("C2", new CultureInfo("en-US")));
                        returnValue = string.Format("{0}", fullAmount.ToString("C2", new CultureInfo("en-US")));
                        returnValue += "|" + dailyCost;
                    }
                    else
                    {
                        returnValue = fullAmount.ToString();
                        returnValue = returnValue + " " + divisa.ToString();
                        returnValue += "|";
                        returnValue += "|" + priceDays.ToString() + " " + divisa.ToString();
                    }
                    returnValue += "|" + CodiceArticolo;
                }
            }


            return returnValue;
        }


        public struct ProjectWBS
        {

            public string Id;

            public string COMPETENCE;

            public string CDC;

            public string AREA;

            public string SYSTEM;

            public string CBS;

            public string WBS;
        }
        public static string FormatSupplierNameForQuery(string givenText)
        {
            string supplier = givenText;
            supplier = supplier.Replace("\"", "%");
            supplier = supplier.Replace("'", "%");
            supplier = supplier.Replace("[", "%");
            supplier = supplier.Replace("]", "%");
            supplier = supplier.Replace(" ", "%");
            supplier = supplier.Replace("%%", "%");
            return supplier;
        }
        public static DataTable GetProjectWBS(ref eConnection givenConnection, string givenProject, string defaultCompetence, string givenParam, string whereCondition)
        {
            string myQuery = ("" + ("SELECT        eplms.eListItem.eValue, MASAD_WBS.ID, MASAD_WBS.COMPETENCE, MASAD_WBS.CDC, MASAD_WBS.AR" +
            "EA, MASAD_WBS.SYSTEM, MASAD_WBS.CBS, MASAD_WBS.WBS " + ("FROM            eplms.eList INNER JOIN                                                               " +
            "                                     " + ("                         eplms.eListItem AS eListItem_1 ON eplms.eList.eId = eListItem_1.eListId INNE" +
            "R JOIN                               " + ("                         eplms.eListItem ON eListItem_1.eValueId = eplms.eListItem.eListId INNER JOIN" +
            "                                     " + ("                         MASAD_WBS ON eplms.eListItem.eValue = MASAD_WBS.ID                          " +
            "                                     " + "WHERE        (eplms.eList.eName = N\'PROJECT_WBS\') AND (eListItem_1.eValue = N\'{0}\')  order by CDC,ARE" +
            "A,SYSTEM,CBS,WBS                     "))))));
            myQuery = string.Format(myQuery, givenProject);
            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            Dictionary<string, ProjectWBS> myResult = null;
            if (myTable == null || myTable.Rows.Count == 0)
            {
                myTable = GetWBSofCompentence(ref givenConnection, defaultCompetence, whereCondition);
                if (myTable != null)
                {
                    myTable = myTable.DefaultView.ToTable(true, new string[] {
                            givenParam});
                }

            }
            else
            {
                DataRow[] myRows = myTable.Select(whereCondition);
                if ((myRows.Length > 0))
                {
                    myTable = myRows.CopyToDataTable();
                    myTable = myTable.DefaultView.ToTable(true, new string[] {
                            givenParam});
                }
                else
                    myTable = null;
            }

            return myTable;
        }
        public static List<string> GetProjectsInCompetence(ref eConnection givenConnection, string givenCompetence)
        {
            List<string> projectList = new List<string>();
            string myQuery = ("" + ("Select eplms.eListItem.eValue As Competence, eListItem_1.eValue As Projects " + ("From eplms.eListItem INNER Join" + ("              eplms.eList ON eplms.eListItem.eListId = eplms.eList.eId INNER Join" + ("              eplms.eListItem AS eListItem_1 ON eplms.eListItem.eValueId = eListItem_1.eListId " + "Where (eplms.eList.eName = N\'COMPETENCE_PROJECT\') and eplms.eListItem.eValue=\'{0}\'")))));
            myQuery = string.Format(myQuery, givenCompetence);
            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (myTable != null && (myTable.Rows.Count > 0))
                foreach (DataRow item in myTable.Rows)
                {
                    if (!projectList.Contains(item["Projects"].ToString()))
                        projectList.Add(item["Projects"].ToString());
                }

            return projectList;
        }
        public static DataTable GetFullWBS(ref eConnection givenConnection)
        {
            string myQuery = "Select * from dbo.MASAD_WBS";
            myQuery = string.Format(myQuery, "");
            myQuery += " order by CDC,AREA,SYSTEM,CBS,WBS";
            return ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
        }
        public static DataTable GetWBSofCompentence(ref eConnection givenConnection, string givenCompetence, string whereCondition = "")
        {
            string myQuery = "Select * from dbo.MASAD_WBS where COMPETENCE=\'{0}\'";
            // Warning!!! Optional parameters not supported
            myQuery = string.Format(myQuery, givenCompetence);
            if (!string.IsNullOrEmpty(whereCondition))
            {
                if (!whereCondition.Trim().StartsWith("AND"))
                    myQuery += " AND ";

                myQuery += (" " + whereCondition);
            }

            myQuery += " order by CDC,AREA,SYSTEM,CBS,WBS";
            return ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
        }

        public static string GetMaxDataRange(ref eConnection givenConnection, string startdate, string competence, string supplier, string professionalRole, string site, string enddate = "")
        {
            string retValue = "";
            string myQuery = "Select * from dbo.[LISTINO_FORNITORI] where Fornitore = '{0}' and ([Descrizione]='{1}' or [Descrizione Completa]='{1}') and (Articolo like'%{2}' or Articolo like'%FCA') order by Seq DESC";
            if ((competence.ToUpper() ?? "") == "PWT" | (competence.ToUpper() ?? "") == "POWERTRAIN")
                myQuery = myQuery + " and [PWT COMPETENCE]=1";
            string valSeparator = ".";
            if (startdate.Contains("/"))
                valSeparator = "/";
            string myNewStart = startdate.Split(valSeparator.ToCharArray())[2] + startdate.Split(valSeparator.ToCharArray())[1] + startdate.Split(valSeparator.ToCharArray())[0];
            string myNewEnd = "";
            if (enddate.Length == 8)
                myNewEnd = enddate.Split(valSeparator.ToCharArray())[2] + enddate.Split(valSeparator.ToCharArray())[1] + enddate.Split(valSeparator.ToCharArray())[0];


            switch (site.ToUpper())
            {
                case "OFFSITE":
                    {
                        site = "OF";
                        break;
                    }

                case "ONSITE":
                    {
                        site = "ON";
                        break;
                    }

                case "BALOCCO":
                    {
                        site = "BL";
                        break;
                    }
            }
            myQuery = string.Format(myQuery, supplier, professionalRole, site);


            DataTable mytable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (mytable != null)
            {
                if (mytable.Rows.Count > 0)
                {
                    bool found = false;
                    foreach (DataRow row in mytable.Rows)
                    {
                        string dummyStart = "" + row["Data Inizio Validità"].ToString();
                        string dummyEnd = "" + row["Data Fine Validità"].ToString();

                        dummyStart = dummyStart.Split("/".ToCharArray())[2] + dummyStart.Split("/".ToCharArray())[1] + dummyStart.Split("/".ToCharArray())[0];
                        dummyEnd = dummyEnd.Split("/".ToCharArray())[2] + dummyEnd.Split("/".ToCharArray())[1] + dummyEnd.Split("/".ToCharArray())[0];
                        if (dummyStart.CompareTo(myNewStart) <= 0 && myNewStart.CompareTo(dummyEnd) <= 0)
                        {
                            retValue = dummyEnd;
                            if (myNewEnd.Length == 0)
                                break;
                            else if (myNewEnd.Length == 8)
                            {
                                if (dummyEnd.CompareTo(myNewEnd) >= 0 && myNewEnd.CompareTo(dummyStart) >= 0)
                                    break;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(retValue))
                    {
                        foreach (DataRow row in mytable.Rows)
                        {
                            string dummyEnd = "" + row["Data Fine Validità"].ToString();
                            dummyEnd = dummyEnd.Split("/".ToCharArray())[2] + dummyEnd.Split("/".ToCharArray())[1] + dummyEnd.Split("/".ToCharArray())[0];
                            if (dummyEnd.CompareTo(myNewStart) <= 0)
                            {
                                retValue = dummyEnd;
                                break;
                            }
                        }
                    }
                }
            }


            return retValue;
        }

        #endregion

        #region "USER PROFILE / VIEW RIGHT"
        public static bool IsSupervisore(ref eConnection givenConnection)
        {
            bool returnFlag = false;
            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConnection);
            eGroup myGroupSuperVisore = new eGroup(ref dbaConnection);
            if (myGroupSuperVisore.Load("SuperVisore") == ePLMS_OK)
            {
                ArrayList users = new ArrayList();
                myGroupSuperVisore.getUserList(ref users);
                if (users != null)
                {
                    foreach (eDataListElement edata in users)
                    {
                        if (edata.Name.ToUpper() == givenConnection.currentUserName.ToUpper())
                        {
                            returnFlag = true;
                            break;
                        }
                    }
                }
            }
            ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
            return returnFlag;
        }

        public static eUserRdAProfile GetUserProfile(ref eConnection givenConnection)
        {
            string myQuery = "SELECT        eplms.eProject.eName as projectName, eplms.eRole.eName AS roleName  " +
                      " FROM eplms.eUser INNER JOIN " +
                      " eplms.eProjectRoleUser ON eplms.eUser.eId = eplms.eProjectRoleUser.eUserId INNER JOIN " +
                      " eplms.eProjectRole ON eplms.eProjectRoleUser.eProjectRoleId = eplms.eProjectRole.eId INNER JOIN " +
                      " eplms.eProject ON eplms.eProjectRole.eProjectId = eplms.eProject.eId INNER JOIN " +
                      " eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId " +
                      " WHERE(eplms.eUser.eName = N'{0}')";
            eUserRdAProfile userProfile = new eUserRdAProfile();
            if (givenConnection.isCurrentUserDba)
                myQuery = myQuery.Replace("WHERE(eplms.eUser.eName = N'{0}')", "");
            myQuery = string.Format(myQuery, givenConnection.currentUserName);
            DataTable dummyTable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            userProfile.IsDBA = givenConnection.isCurrentUserDba;
            userProfile.IsSuperVisore = eRdAHelper.IsSupervisore(ref givenConnection);
            if (dummyTable != null && dummyTable.Rows.Count > 0)
            {
                foreach (DataRow r in dummyTable.Rows)
                {

                    string roleName = r["roleName"].ToString().ToUpper();
                    if (roleName.ToUpper() == Enum.GetName(typeof(UserProfileValue), UserProfileValue.Requester).ToUpper())
                        userProfile.IsRequester = true;
                    else if (roleName.ToUpper().StartsWith(Enum.GetName(typeof(UserProfileValue), UserProfileValue.PRCDC_).ToUpper()))
                        userProfile.IsPR = true;
                    else if (roleName.ToUpper().StartsWith(Enum.GetName(typeof(UserProfileValue), UserProfileValue.PL_).ToUpper()))
                        userProfile.IsPlanner = true;
                    else if (roleName.ToUpper().Equals(Enum.GetName(typeof(UserProfileValue), UserProfileValue.CMS).ToUpper()))
                        userProfile.IsCSM = true;
                    else if (roleName.ToUpper().Equals(Enum.GetName(typeof(UserProfileValue), UserProfileValue.PM).ToUpper()))
                        userProfile.IsPM = true;
                    else if (roleName.ToUpper().StartsWith(Enum.GetName(typeof(UserProfileValue), UserProfileValue.UR_).ToUpper()))
                        userProfile.IsUR = true;
                    else if (roleName.ToUpper() == Enum.GetName(typeof(UserProfileValue), UserProfileValue.PMP).ToUpper())
                        userProfile.IsPMP = true;
                    else if (roleName.ToUpper() == Enum.GetName(typeof(UserProfileValue), UserProfileValue.PPM).ToUpper())
                        userProfile.IsPPM = true;

                }
            }

            return userProfile;
        }
        #endregion

        #region "Function on USER"

        public static eRdAWorkflowResult BusinessDataPerformTask(ref eConnection givenConnection, eRdAWorkflowAction givenWorkflowAction)
        {
            eRdAWorkflowResult retvalue = new eRdAWorkflowResult();
            int rc = 0;
            retvalue.result = true;

            retvalue.businessDataId = givenWorkflowAction.businessDataId;
            var businessData = new eBusinessData(ref givenConnection);

            try
            {
                if (givenWorkflowAction.actionType == eRdAWFActionType.REJECT)
                    if (string.IsNullOrEmpty(givenWorkflowAction.reason))
                        throw new Exception("Please fill a valid reason in comment section to Reject the eRdA");

                rc = businessData.Load(givenWorkflowAction.businessDataId);
                if (rc != ePLMS_OK)
                    throw new Exception(givenConnection.errorMessage);
                businessData.Unreserve();
                retvalue.fromLevel = businessData.Level;
                var workflowEngine = new eWorkflowEngine(ref givenConnection, businessData.Id);
                if (!workflowEngine.isWorkflowEnded)
                {
                    workflowTask currentWorkflowTask = workflowEngine.getCurrentWorkflowTask();
                    if (currentWorkflowTask != null)
                    {

                        if (currentWorkflowTask.getTaskType().Equals(eAdminDataDefs.ePLMS_STARTSTEP))
                            rc = currentWorkflowTask.SetCurrentTaskParameter("REASON", givenWorkflowAction.reason);
                        else if (currentWorkflowTask.getTaskType().Equals(eAdminDataDefs.ePLMS_ASSIGNLEVELTASK))
                            rc = currentWorkflowTask.SetCurrentTaskParameter("REASON", givenWorkflowAction.reason);
                        else
                        {
                            workflowCheckTask myeCheckTask = (workflowCheckTask)currentWorkflowTask;
                            if (myeCheckTask.getUnmarkedCheckTableAssignableByCurrentUser.Count > 0)
                            {
                                foreach (eCheck myeCheck in myeCheckTask.getUnmarkedCheckTableAssignableByCurrentUser.Values)
                                {
                                    rc = currentWorkflowTask.SetCurrentTaskParameter(myeCheck.Name + "_comment", givenWorkflowAction.reason);
                                    if (givenWorkflowAction.actionType == eRdAWFActionType.APPROVE)
                                        rc = currentWorkflowTask.SetCurrentTaskParameter(myeCheck.Name + "_action", eWorkflowDefs.ePLMSApprovedCheck);
                                    else
                                        rc = currentWorkflowTask.SetCurrentTaskParameter(myeCheck.Name + "_action", eWorkflowDefs.ePLMSRejectedCheck);

                                }
                            }
                            else
                                throw new Exception("Attention! User hasn't permission to manage current eRdA. If you think to have the correct right please contact Administrator.");
                        }
                        eBusinessData dummy = businessData;
                        rc = ePortalHelper.WorkflowPromoteRaiseEvent(ref givenConnection, ref businessData, dummy, ref currentWorkflowTask, "executeWorkflowTaskPrefix");
                        givenConnection.pushMessageFlag = true;
                        if (rc != ePLMS_OK)
                            throw new Exception(givenConnection.errorMessage);

                        rc = currentWorkflowTask.performTask();
                        givenConnection.pushMessageFlag = false;
                        if (rc != ePLMS_OK)
                            throw new Exception(givenConnection.errorMessage);

                        rc = ePortalHelper.WorkflowPromoteRaiseEvent(ref givenConnection, ref businessData, dummy, ref currentWorkflowTask, "executeWorkflowTaskSuffix");
                        if (rc != ePLMS_OK)
                            throw new Exception(givenConnection.errorMessage);
                    }
                }
                else
                    throw new Exception("Workflow already ended");
            }
            catch (Exception ex)
            {
                retvalue.result = false;
                retvalue.message = ex.Message;
                return retvalue;
            }

            return retvalue;
        }

        public static eRdAWorkflowResult BusinessDataPerformRejectToCMS(ref eConnection givenConnection, eRdAWorkflowAction givenWorkflowAction)
        {
            eRdAWorkflowResult retvalue = new eRdAWorkflowResult();
            int rc = 0;
            retvalue.result = true;

            retvalue.businessDataId = givenWorkflowAction.businessDataId;
            
            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConnection);
            try
            {

                if (givenWorkflowAction.actionType == eRdAWFActionType.REJECT)
                    if (string.IsNullOrEmpty(givenWorkflowAction.reason))
                        throw new Exception("Please fill a valid reason in comment section to Reject the eRdA");

                var businessData = new eBusinessData(ref dbaConnection);
                rc = businessData.Load(givenWorkflowAction.businessDataId);
                if (rc != ePLMS_OK)
                    throw new Exception(givenConnection.errorMessage);
                businessData.Unreserve();

                var workflowEngine = new eWorkflowEngine(ref dbaConnection, businessData.Id);
                int RetCode = workflowEngine.reassignLevel("CMS Validation", "Rejected by " + givenConnection.currentUserName + "  with this reason:" + givenWorkflowAction.reason, true);

                workflowEngine = new eWorkflowEngine(ref dbaConnection, businessData.Id);

                workflowTask currentWorkflowTask = workflowEngine.getCurrentWorkflowTask();
                eBusinessData dummy = businessData;
                rc = ePortalHelper.WorkflowPromoteRaiseEvent(ref dbaConnection, ref businessData, dummy, ref currentWorkflowTask, "executeWorkflowTaskPrefix");
                givenConnection.pushMessageFlag = true;
                if (rc != ePLMS_OK)
                    throw new Exception(givenConnection.errorMessage);

                rc = currentWorkflowTask.performTask();
                givenConnection.pushMessageFlag = false;
                if (rc != ePLMS_OK)
                    throw new Exception(givenConnection.errorMessage);

                rc = ePortalHelper.WorkflowPromoteRaiseEvent(ref dbaConnection, ref businessData, dummy, ref currentWorkflowTask, "executeWorkflowTaskSuffix");
                if (rc != ePLMS_OK)
                    throw new Exception(givenConnection.errorMessage);

                if (RetCode == ePLMS_OK)
                {
                    string myFile = System.IO.Path.Combine(dbaConnection.ePLMS_HOME, @"env\scripts\Mail\RDA Control Rejected.htm");
                    string myContent = System.IO.File.ReadAllText(myFile);
                    rc = businessData.Load(givenWorkflowAction.businessDataId);
                    myContent = myContent.Replace("{ePLMS_INSTANCE_URL}", dbaConnection.ePLMS_INSTANCE_URL);
                    myContent = myContent.Replace("{eBusinessDataId}", businessData.Id.ToString());
                    myContent = myContent.Replace("{eBusinessDataName}", businessData.Name);
                    myContent = myContent.Replace("{eBusinessDataDescription}", businessData.Description);
                    myContent = myContent.Replace("{eBusinessDataProject}", businessData.Project);
                    myContent = myContent.Replace("{eBusinessDataOwner}", businessData.Owner);
                    myContent = myContent.Replace("{eBusinessDataCreator}", businessData.createUser);
                    myContent = myContent.Replace("{eBusinessDataLevel}", businessData.Level);

                    try
                    {

                        SmtpClient SmtpServer = new SmtpClient();
                        MailMessage mail = new MailMessage();
                        Parallaksis.ePLMS.Security.pSecurity mySecurity = new Parallaksis.ePLMS.Security.pSecurity();

                        SmtpServer.Host = dbaConnection.ePLMS_SMTP_SERVER;
                        SmtpServer.Port = dbaConnection.ePLMS_SMTP_PORT;
                        if (dbaConnection.ePLMS_SMTP_USER_PASSWORD != "")
                        {
                            string myCredentialP = "";
                            mySecurity.DecryptBase64CNV(dbaConnection.ePLMS_SMTP_USER_PASSWORD, ref myCredentialP);
                            SmtpServer.Credentials = new System.Net.NetworkCredential(dbaConnection.ePLMS_SMTP_USER, myCredentialP);
                        }

                        mail = new MailMessage();
                        mail.From = new MailAddress(dbaConnection.ePLMS_SMTP_MAIL_FROM);

                        workflowEngine = new eWorkflowEngine(ref dbaConnection, businessData.Id);

                        currentWorkflowTask = workflowEngine.getCurrentWorkflowTask();

                        List<string> myMail = new List<string>();
                        if (currentWorkflowTask.getTaskType().Equals(ePLMS_CHECKTASK))
                        {
                            eTask myTask = null;
                            currentWorkflowTask.getCurrentWorkflow.getTask(currentWorkflowTask.getTaskId(), ref myTask);

                            if (myTask != null)
                            {
                                eRoleCheckTaskEntry[] myeRoleCheckTaskEntryList = null;


                                eProject myProject = new eProject(ref dbaConnection);
                                eProjectRole[] myProjectRole = null;

                                myProject.Load(currentWorkflowTask.getCurrentBusinessData.Project);
                                myProject.getAllProjectRoleList(ref myProjectRole);
                                myTask.getRoleCheckTaskEntryList(ref myeRoleCheckTaskEntryList);
                                if (myeRoleCheckTaskEntryList != null)
                                {
                                    ePerson myPerson = new ePerson(ref dbaConnection);
                                    foreach (eProjectRole role in myProjectRole)
                                    {
                                        eProjectRoleUser[] myUSers = null;
                                        role.getProjectRoleUserList(ref myUSers);
                                        if (myUSers != null)
                                        {
                                            foreach (eProjectRoleUser user in myUSers)
                                            {
                                                if (myPerson.Load(user.userName) == ePLMS_OK)
                                                {
                                                    if (!myMail.Contains(myPerson.eMail))
                                                        myMail.Add(myPerson.eMail);
                                                }
                                            }
                                        }
                                    }
                                }
                                myeRoleCheckTaskEntryList = null;
                            }
                        }

                        foreach (string User in myMail)
                        {
                            if (User + "" != "")
                                mail.To.Add(User);
                        }
                        mail.Subject = businessData.Name + " - RDA Control Rejected";
                        mail.IsBodyHtml = true;
                        mail.Body = myContent;
                        SmtpServer.Send(mail);
                        rc = ePLMS_OK;
                    }
                    catch (Exception ex)
                    {
                    }
                    

                }

            }
            catch (Exception ex)
            {
                ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
                retvalue.result = false;
                retvalue.message = ex.Message;
                return retvalue;
            }
            ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
            return retvalue;
        }


        public static List<long> GetBusinessDataTaskEntry(ref eConnection eConnection)
        {
            List<long> retValue = new List<long>();
            DataTable myObjectToSign = ePortalHelper.ExecuteQuery(ref eConnection, "select eBusinessDataId FROM eplms.eBusinessDataTaskEntry where eUser='" + eConnection.currentUserName + "'");
            if (myObjectToSign != null && myObjectToSign.Rows.Count > 0)
                retValue = myObjectToSign.Rows.OfType<DataRow>().Select(dr => dr.Field<long>("eBusinessDataId")).ToList();

            return retValue;
        }
        #endregion

        #region "ADMIN"
        public static void UpdateItemOnList(AdminDataConfig data, ref eConnection givenConenction)
        {

            if ((data.updateItems != null && data.updateItems.Count > 0) || (data.deleteItems != null && data.deleteItems.Count > 0))
            {
                eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConenction);
                try
                {
                    eList myList = new eList(ref dbaConnection);
                    switch (data.target.ToLower())
                    {
                        case "professionallevel":
                            data.target = "Professional Level";
                            break;
                        case "professionalrole":
                            data.target = "Professional Role";
                            break;
                        case "pwtprofessionalrole":
                            data.target = "PWT Professional Role";
                            break;

                    }
                    int rc = myList.Load(data.target);
                    if (rc != 0) throw new Exception(dbaConnection.errorMessage);
                    if (data.updateItems != null && data.updateItems.Count > 0)
                    {
                        foreach (string value in data.updateItems)
                            myList.addItem(value, "");
                    }

                    if (data.deleteItems != null && data.deleteItems.Count > 0)
                    {
                        List<listItemElement> itemList = new List<listItemElement>();
                        myList.getItemList(ref itemList);
                        foreach (string value in data.deleteItems)
                        {
                            string query = "SELECT  TOP(1) eplms.eList.eName  FROM eplms.eListItem AS eListItem_1 INNER JOIN  eplms.eListItem ON eListItem_1.eValueId = eplms.eListItem.eListId INNER JOIN  eplms.eList ON eListItem_1.eListId = eplms.eList.eId WHERE (eplms.eListItem.eValue = N'" + value + "') and eName like '%_" + myList.Name + "'";
                            DataTable dataTable = ePortalHelper.ExecuteQuery(ref givenConenction, query);
                            if (dataTable == null || dataTable.Rows.Count == 0)
                            {

                                for (var index = 0; index < itemList.Count(); index++)
                                {
                                    listItemElement element = itemList[index];
                                    if (value.ToLower() == element.Value.ToString().ToLower())
                                    {
                                        myList.removeItem(ref element);
                                        break;
                                    }
                                }
                            }
                        }
                        
                    }
                    eRdAHelper.ValidateRDAConfiguration(ref givenConenction, myList);
                }
                catch (Exception e)
                {
                    ePortalHelper.DestroyDbaConnection(ref givenConenction, ref dbaConnection);
                    throw e;
                }

                ePortalHelper.DestroyDbaConnection(ref givenConenction, ref dbaConnection);

            }


        }
        public static void UpdateItemOnProject(AdminDataConfig data, ref eConnection givenConenction)
        {

            if ((data.updateItems != null && data.updateItems.Count > 0) || (data.deleteItems != null && data.deleteItems.Count > 0))
            {
                eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConenction);
                try
                {

                    eProject myeProject = new eProject(ref dbaConnection);
                    eProject myParenteProject = new eProject(ref dbaConnection);
                    int rc = myParenteProject.Load("RDA");
                    if (rc != 0) throw new Exception(dbaConnection.errorMessage);
                    if (data.updateItems != null && data.updateItems.Count > 0)
                    {
                        foreach (string value in data.updateItems)
                        {
                            myeProject = new eProject(ref dbaConnection, ref myParenteProject);
                            rc = myeProject.Load(value);
                            if (rc != ePLMS_OK)
                            {
                                myeProject.Name = value;
                                myeProject.Description = "";
                                myeProject.Status = "ENABLED";

                                myeProject.defaultVault = myParenteProject.defaultVault;
                                rc = myeProject.Create();
                            }

                            if (rc == 0)
                            {
                                eWorkflow myWF = new eWorkflow(ref dbaConnection);
                                if (myWF.Load("WF_RDA_01") == ePLMS_OK)
                                {
                                    eProjectWorkflow newProjectWorkflow = new eProjectWorkflow(ref dbaConnection, ref myeProject);
                                    newProjectWorkflow.workflowId = myWF.Id;
                                    newProjectWorkflow.workflowName = myWF.Name;
                                    if (myeProject.addProjectWorkflow(newProjectWorkflow) == ePLMS_OK)
                                    {
                                        eBusinessDataType bdt = new eBusinessDataType(ref dbaConnection);
                                        if (bdt.Load("RDA") == ePLMS_OK)
                                        {
                                            eProjectWorkflowBusinessDataType myeProjectWorkflowBusinessDataType = new eProjectWorkflowBusinessDataType(ref dbaConnection, ref newProjectWorkflow);
                                            myeProjectWorkflowBusinessDataType.businessDataTypeName = bdt.Name;
                                            myeProjectWorkflowBusinessDataType.businessDataTypeId = bdt.Id;
                                            myeProjectWorkflowBusinessDataType.workflowType = ePLMSPRIMARYLIFECYCLE;
                                            newProjectWorkflow.addBusinessDataTypeToProjectWorkflow(myeProjectWorkflowBusinessDataType);
                                        }
                                    }
                                }
                            }
                            else
                                throw new Exception(dbaConnection.errorMessage);
                        }
                    }
                    if (data.deleteItems != null && data.deleteItems.Count > 0)
                    {

                        foreach (string value in data.deleteItems)
                        {


                            myeProject = new eProject(ref dbaConnection);
                            if (myeProject.Load(value) == ePLMS_OK)
                            {
                                string myQuery = "select top(1) eid from eplms.eBusinessData where eProject='" + myeProject.Name + "'";
                                DataTable myTable = ePortalHelper.ExecuteQuery(ref dbaConnection, myQuery);
                                if (myTable != null && myTable.Rows.Count > 0)
                                {
                                    myeProject.Description = "DISABLED";
                                    myeProject.Modify();
                                }
                                else
                                    myeProject.Delete();


                            }


                        }

                        ValidateRDAConfiguration(ref dbaConnection, null, "PROJECTS", GetProjectListItem(ref dbaConnection));
                    }



                }
                catch (Exception e)
                {
                    ePortalHelper.DestroyDbaConnection(ref givenConenction, ref dbaConnection);
                    throw e;
                }

                ePortalHelper.DestroyDbaConnection(ref givenConenction, ref dbaConnection);

            }

        }

        public static void UpdateItemOnMultiList(AdminDataConfig data, ref eConnection givenConenction)
        {
            if ((data.updateItems != null && data.updateItems.Count > 0) || (data.deleteItems != null && data.deleteItems.Count > 0))
            {
                eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConenction);
                try
                {
                    string currentList = "";

                    currentList = string.Format("{0}_{1}", data.target.ToUpper(), data.subItem.ToUpper());

                    eList myList = new eList(ref dbaConnection);
                    if (myList.Load(currentList) == ePLMS_OK)
                    {
                        List<listItemElement> itemList = new List<listItemElement>();
                        myList.getItemList(ref itemList);
                        myList = null;
                        if (itemList != null && itemList.Count > 0)
                        {
                            foreach (listItemElement item in itemList)
                            {
                                if (((eList)item.Value).Name.ToLower() == data.filter.ToLower())
                                {
                                    myList = (eList)item.Value;
                                    break;
                                }
                            }
                        }
                        if (myList == null)
                        {
                            myList = new eList(ref dbaConnection);
                            myList.Load(currentList);

                            eList dummy = new eList(ref dbaConnection, myList, ePLMSListItemTypeList);
                            dummy.Name = data.filter;
                            dummy.Description = "";
                            if (dummy.Create() == ePLMS_OK)
                                myList = dummy;
                            else
                                throw new Exception("Administrative configuration is corruted!Please contact the system administrator");
                        }


                        if (data.updateItems != null && data.updateItems.Count > 0)
                        {
                            foreach (string item in data.updateItems)
                            {


                                eList basicList = new eList(ref dbaConnection, myList, ePLMSListItemTypeList);
                                basicList.Name = item;
                                basicList.Description = "";
                                basicList.Create();
                            }
                        }
                        if (data.deleteItems != null && data.deleteItems.Count > 0)
                        {
                            itemList = new List<listItemElement>();
                            myList.getItemList(ref itemList);
                            if (itemList != null && itemList.Count > 0)
                            {
                                foreach (string item in data.deleteItems)
                                {
                                    foreach (listItemElement removeItem in itemList)
                                    {
                                        listItemElement dummy = removeItem;
                                        if (((eList)dummy.Value).Name.ToLower() == item.ToLower())
                                        {
                                            myList.removeItem(ref dummy);
                                            if (currentList == "COMPETENCE_PROJECT")
                                            {
                                                eList myCP = new eList(ref dbaConnection);
                                                if (myCP.Load("PROJECT_WBS") == ePLMS_OK)
                                                {
                                                    List<listItemElement> pjList = new List<listItemElement>();
                                                    myCP.getItemList(ref pjList);
                                                    if (pjList != null)
                                                    {
                                                        //foreach (listItemElement pjItem in pjList)
                                                        for(var indexpj =0; indexpj<pjList.Count; indexpj++)// 
                                                        {
                                                            listItemElement pjItem = pjList[indexpj];
                                                            if (((eList)pjItem.Value).Name.ToLower() == item.ToLower())
                                                            {
                                                                myCP.removeItem(ref pjItem);
                                                                break;
                                                            }
                                                        }
                                                    }

                                                }

                                            }
                                            if (data.subItem.ToLower() == "activitynpi" && data.target.ToLower()=="project")
                                            {
                                                RemoveUserRoleList2(ref dbaConnection, data.filter, item);
                                            }

                                            break;
                                        }

                                    }

                                }
                            }

                        }
                    }
                    else
                        throw new Exception(dbaConnection.errorMessage);
                }
                catch (Exception e)
                {
                    ePortalHelper.DestroyDbaConnection(ref givenConenction, ref dbaConnection);
                    throw e;
                }

                ePortalHelper.DestroyDbaConnection(ref givenConenction, ref dbaConnection);

            }


        }
        public static void UpdateWBS(AdminDataConfig data, ref eConnection givenConenction)
        {
            if ((data.updateItems != null && data.updateItems.Count > 0) || (data.deleteItems != null && data.deleteItems.Count > 0))
            {
                eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConenction);
                try
                {
                    if (data.updateItems != null && data.updateItems.Count > 0)
                    {
                        if (data.target == "project" || data.target == "competence")
                        {
                            string message = "";
                            foreach (string item in data.updateItems)
                            {
                                string[] myItems = item.Split("|".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                                string compotence = "";
                                int valueDefined = 0;
                                bool sys = false;
                                bool cdc = false;
                                foreach (string line in myItems)
                                {
                                    string key = line.Split("=".ToArray())[0].ToUpper();
                                    string value = line.Split("=".ToArray())[1].ToUpper();
                                    if (key == "COMPETENCE")
                                    {
                                        compotence = value;
                                    }
                                    else if (key == "CDC")
                                    {
                                        if (string.IsNullOrEmpty(value)) cdc = true;
                                    }
                                    else if (key == "SYSTEM")
                                    {
                                        if (string.IsNullOrEmpty(value)) sys = true;
                                    }
                                    if (!string.IsNullOrEmpty(value)) valueDefined += 1;
                                }
                                if (compotence == "VEHICLE" && valueDefined != 6)
                                    throw new Exception("Error!Vehicle require all value of the wbs");
                                else if (compotence != "VEHICLE" && valueDefined != 5)
                                    throw new Exception("Error!Verify all value of selected competence");

                            }

                            foreach (string item in data.updateItems)
                            {
                                if (data.target == "competence")
                                    createwbsline(ref givenConenction, item);
                                else if (data.target == "project")
                                {
                                    createprojectwbsline(ref dbaConnection, data.filter,item);
                                }

                            }
                        }
                    }
                    if (data.deleteItems != null && data.deleteItems.Count > 0)
                    {
                        foreach (string item in data.deleteItems)
                        {
                            if (data.target == "competence")
                                deleteWbsline(ref givenConenction, item);
                            else if(data.target == "project")
                            {
                                deleteprojectwbsline(ref dbaConnection,data.filter, item);
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    ePortalHelper.DestroyDbaConnection(ref givenConenction, ref dbaConnection);
                    throw e;
                }

                ePortalHelper.DestroyDbaConnection(ref givenConenction, ref dbaConnection);

            }


        }

        private static void createwbsline(ref eConnection givenConnection, string valueToUpdate)
        {
            string myStringInser = "INSERT INTO [dbo].[MASAD_WBS] ([COMPETENCE],[CDC],[AREA],[SYSTEM],[CBS],[WBS])VALUES('@COMPETENCE','@CDC','@AREA','@SYSTEM','@CBS','@WBS')";

            foreach (string entry in valueToUpdate.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                string key = "@" + entry.Split("=".ToCharArray())[0];
                string value = entry.Split("=".ToCharArray())[1];
                myStringInser = myStringInser.Replace(key, value);
            }
            ePortalHelper.ExecuteQuery(ref givenConnection, myStringInser);
        }

        private static void deleteWbsline(ref eConnection givenConnection, string wbsId)
        {
            string myStringDelete = "DELETE FROM DBO.MASAD_WBS WHERE ID='" + wbsId + "'";

            ePortalHelper.ExecuteQuery(ref givenConnection, myStringDelete);
        }


        private static void createprojectwbsline(ref eConnection givenConnection,string givenProject,string valueToUpdate)
        {
            eList myList = new eList(ref givenConnection);
            if (myList.Load("PROJECT_WBS") == ePLMS_OK)
            {
                string[] keys = new string[] { "COMPETENCE", "CDC", "AREA", "SYSTEM", "CBS", "WBS" };
                string[] pathValue = valueToUpdate.Split("|".ToArray());
                List<string> where = new List<string>();
                for (int index = 0; index < pathValue.Length; index++)
                {
                    pathValue[index] = pathValue[index].Split("=".ToCharArray())[1];
                    if (string.IsNullOrEmpty(pathValue[index]))
                        where.Add(string.Format("({0} IS NULL or {0}='')", keys[index]));
                    else
                        where.Add(string.Format("{0}='{1}'", keys[index], pathValue[index]));
                }


                string wbs = "select * from MASAD_WBS where " +  string.Join(" and ",where.ToArray());
                wbs = wbs.Replace("|", " and ");
                DataTable dummyTable = ePortalHelper.ExecuteQuery(ref givenConnection, wbs);
                if (dummyTable == null || dummyTable.Rows.Count == 0)
                    throw new Exception("Administrative configuration is corruted!WBS is not valid");
                else
                    wbs = dummyTable.Rows[0]["ID"].ToString();

                List<listItemElement> itemList = new List<listItemElement>();
                myList.getItemList(ref itemList);
                myList = null;
                if (itemList != null && itemList.Count > 0)
                {
                    foreach (listItemElement item in itemList)
                    {
                        if (((eList)item.Value).Name.ToLower() == givenProject.ToLower())
                        {
                            myList = (eList)item.Value;
                            break;
                        }
                    }
                }
                if (myList == null)
                {
                    myList = new eList(ref givenConnection);
                    myList.Load("PROJECT_WBS");

                    eList dummy = new eList(ref givenConnection, myList, ePLMSListItemTypeList);
                    dummy.Name = givenProject;
                    dummy.Description = "";
                    if (dummy.Create() == ePLMS_OK)
                        myList = dummy;
                    else
                        throw new Exception("Administrative configuration is corruted!Please contact the system administrator");
                }
                if (myList != null)
                {
                    eList dummy = new eList(ref givenConnection, myList, ePLMSListItemTypeList);
                    dummy.Name = wbs ;
                    dummy.Description = "";
                    if (dummy.Create() != ePLMS_OK)
                        throw new Exception("Administrative configuration is corruted!Please contact the system administrator");
                }
            }
        }

        private static void deleteprojectwbsline(ref eConnection givenConnection, string projectName, string wbsId)
        {
            string myStringDelete = "SELECT 														   " +
            "     	 eListItem_1.eId, eListItem_1.eListId, eListItem_1.eValueId                                " +
            "FROM                                                                                      " +
            "       eplms.eList INNER JOIN                                                             " +
            "       eplms.eListItem ON eplms.eList.eId = eplms.eListItem.eListId INNER JOIN            " +
            "       eplms.eListItem AS eListItem_1 ON eplms.eListItem.eValueId = eListItem_1.eListId   " +
            "WHERE                                                                                     " +
            "		(eplms.eList.eName = N'Project_WBS') AND                                           " +
            "		(eplms.eListItem.eValue = N'{0}') AND                                 " +
            "(eListItem_1.eValue = N'{1}')                                                ";

            myStringDelete = string.Format(myStringDelete, projectName, wbsId);
            DataTable dummy = ePortalHelper.ExecuteQuery(ref givenConnection, myStringDelete);
            if (dummy != null && dummy.Rows.Count > 0)
            {
                string delete1 = "delete from eplms.eListItem where eId=" + dummy.Rows[0]["eId"];
                string delete2 = "delete from eplms.eList where eId=" + dummy.Rows[0]["eValueId"];
                ePortalHelper.ExecuteQuery(ref givenConnection, delete1);
                ePortalHelper.ExecuteQuery(ref givenConnection, delete2);

            }
        }

        public static List<listItemElement> GetProjectListItem(ref eConnection givenConnection)
        {
            string myQuery = "  select * from (SELECT   top(1000000)     eId, eName AS eValue,                            " +
                " (CASE WHEN eDescription like '%DISABLED%' THEN 'DISABLED' ELSE 'ENABLED' END) as STATUS    " +
                " FROM            eplms.eProject                                                             " +
                " WHERE        (eParentProjectId IN                                                          " +
                " (SELECT        eId                                                                         " +
                "                                FROM            eplms.eProject AS eProject_1                " +
                " WHERE        (eName = 'RDA'))) AND (eDescription not like  '%disabled%')                   " +
                " order by  eplms.eProject.eName ) PJENABLED                                                 " +
                " UNION ALL                                                                                  " +
                " select * from (SELECT   top(1000000)     eId, eName AS eValue,                             " +
                " (CASE WHEN eDescription like '%DISABLED%' THEN 'DISABLED' ELSE 'ENABLED' END) as STATUS    " +
                " FROM            eplms.eProject                                                             " +
                " WHERE        (eParentProjectId IN                                                          " +
                " (SELECT        eId                                                                         " +
                "                                FROM            eplms.eProject AS eProject_1                " +
                "  WHERE        (eName = 'RDA'))) AND (eDescription  like '%disabled%')                      " +
                " order by  eplms.eProject.eName ) PJDISABLED                                                ";

            DataTable dummyTable;
            dummyTable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            List<listItemElement> retValue = new List<listItemElement>();
            if (dummyTable != null)
            {
                foreach (DataRow row in dummyTable.Rows)
                {
                    listItemElement newItem = new listItemElement();
                    newItem.Description = row["STATUS"].ToString();
                    newItem.Value = row["eValue"].ToString();
                    retValue.Add(newItem);
                }
            }
            return retValue;
        }

        public static void ValidateRDAConfiguration(ref eConnection givenConnection, eList givenListModified, string givenlistname = "", List<listItemElement> additionalValue = null)
        {
            List<listItemElement> myActualValues = new List<listItemElement>();
            eList myDestinationList;
            string myListToUpdate = "";
            string myListLevelToUpdate = "";
            if (givenListModified != null)
            {
                givenListModified.Load(givenListModified.Id);
                givenListModified.getItemList(ref myActualValues);
                if (givenListModified != null)
                    givenlistname = givenListModified.Name;
            }
            else
                myActualValues = additionalValue;
            if (string.IsNullOrEmpty(givenlistname))
                return;
            switch (givenlistname.ToUpper())
            {
                case "BRAND":
                    {
                        myListToUpdate = "BRAND_PROJECT";
                        myListLevelToUpdate = "1";
                        break;
                    }

                case "PROJECTS":
                    {
                        myListToUpdate = "PROJECT_CONTENTS;COMPETENCE_PROJECT;BRAND_PROJECT;PROJECT_ACTIVITYNPI;PROJECT_ENGINES;PROJECT_CONTENTS";
                        myListLevelToUpdate = "1;2;2;1;1;1";
                        break;
                    }

                case "CONTENTS":
                    {
                        myListToUpdate = "PROJECT_CONTENTS";
                        myListLevelToUpdate = "2";
                        break;
                    }

                case "ENGINES":
                    {
                        myListToUpdate = "PROJECT_ENGINES";
                        myListLevelToUpdate = "2";
                        break;
                    }

                case "ACTIVITYNPI":
                    {
                        myListToUpdate = "PROJECT_ACTIVITYNPI";
                        myListLevelToUpdate = "2";
                        break;
                    }
            }
            for (int idx = 0; idx <= myListToUpdate.Split(";".ToArray()).Length - 1; idx++)
            {
                string listName = myListToUpdate.Split(";".ToArray())[idx];
                string lvl = myListLevelToUpdate.Split(";".ToArray())[idx];
                eList myList = new eList(ref givenConnection);
                if (myList.Load(listName) == ePLMS_OK)
                {
                    List<listItemElement> myItems1 = new List<listItemElement>();
                    myList.getItemList(ref myItems1);
                    if (myItems1 != null)
                    {
                        if (int.Parse(lvl) == 1)
                        {
                            foreach (listItemElement subItem1 in myItems1)
                            {
                                listItemElement dummy = subItem1;
                                bool found = false;
                                foreach (listItemElement val in myActualValues)
                                {
                                    if (((eList)subItem1.Value).Name.ToLower() == val.Value.ToString().ToLower())
                                    {
                                        if (val.Description != "DISABLED")
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                                if (!found)
                                    myList.removeItem(ref dummy);
                            }
                            myList.getItemList(ref myItems1);
                            foreach (listItemElement val in myActualValues)
                            {
                                bool found = false;
                                if (val.Description == "DISABLED")
                                    continue;
                                foreach (listItemElement subItem1 in myItems1)
                                {
                                    if (((eList)subItem1.Value).Name.ToLower() == val.Value.ToString().ToLower())
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    var dummyListMaster = new eList(ref givenConnection, myList, ePLMSListItemTypeList);
                                    dummyListMaster.Name = val.Value.ToString();
                                    dummyListMaster.Description = "";
                                    dummyListMaster.Create();
                                }
                            }
                        }
                        else if (int.Parse(lvl) == 2)
                        {
                            foreach (listItemElement subItem1 in myItems1)
                            {
                                List<listItemElement> myItems2 = new List<listItemElement>();
                                ((eList)subItem1.Value).getItemList(ref myItems2);
                                if (myItems2 != null)
                                {
                                    foreach (var subItem2 in myItems2)
                                    {
                                        bool found = false;
                                        listItemElement dummy = subItem2;
                                        foreach (listItemElement val in myActualValues)
                                        {
                                            string itemValue = "";
                                            if (subItem2.Type == ePLMSListItemTypeList)
                                                itemValue = ((eList)subItem2.Value).Name;
                                            else
                                                itemValue = subItem2.Value.ToString();
                                            if (itemValue.ToLower() == val.Value.ToString().ToLower())
                                            {
                                                if (val.Description != "DISABLED")
                                                {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!found)
                                            ((eList)subItem1.Value).removeItem(ref dummy);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static bool AddUserRoleToProject(ref eConnection givenConnection, eUser givenUser, string givenRoleName, string myeProjectName, string cdc,string competence,string activitynpi)
        {
            eGroup myGroup;
            eProject myeProject = new eProject(ref givenConnection);
            eProjectRole projectRole = null;
            eProjectRole[] projectRoleList = new eProjectRole[]{};

            myeProject.Load(myeProjectName);
            if (givenRoleName.ToUpper() == "REQUESTER" || givenRoleName.ToUpper() == "PMP" )
                myeProject.Load("RDA");
            else if (givenRoleName.ToUpper() == "PLANNER" || givenRoleName.ToUpper() == "PR" || givenRoleName.ToUpper()== "UR")
            {
                if (string.IsNullOrEmpty(cdc))
                    throw new Exception("CDC not defined");
                if (givenRoleName.ToUpper() == "PLANNER")
                    givenRoleName = eRdAHelper.GetPlannerRoleDBValue(cdc);
                else if (givenRoleName.ToUpper() == "UR")
                    givenRoleName = eRdAHelper.GetURRoleDBValue(cdc);
                else
                {
                    
                    eRdAHelper.SaveUserRoleList(ref givenConnection, myeProjectName, cdc.ToUpper(), activitynpi, givenUser.Name);
                    givenRoleName = eRdAHelper.GetPRRoleDBValue(givenRoleName, competence);
                }
                    

            }

            eRole myRole = new eRole(ref givenConnection);
            if(myRole.Load(givenRoleName)!= ePLMS_OK)
            {
                myRole = new eRole(ref givenConnection);
                myRole.Name = givenRoleName;
                myRole.Create();
            }
            
            projectRole = new eProjectRole(ref givenConnection, ref myeProject);
            projectRole.roleName = givenRoleName;
            myeProject.addProjectRole(projectRole);

            myeProject.getProjectRoleList(ref projectRoleList);
            if (projectRoleList != null)
            {
                for(int i=0;i<projectRoleList.Count();i++)
                {
                    projectRole = projectRoleList[i];
                    if (projectRole.roleName.ToUpper() == givenRoleName.ToUpper())
                    {
                        
                        eProjectRoleUser myeProjectRoleUser = new eProjectRoleUser(ref givenConnection,ref projectRole);
                        myeProjectRoleUser.isProjectAdmin = false;
                        myeProjectRoleUser.userId = givenUser.Id;
                        projectRole.addUserToProjectRole(myeProjectRoleUser);
                        break;
                    }
                }
            }
            if (givenRoleName.ToUpper() == "REQUESTER")
            {
                myGroup = new eGroup(ref givenConnection);
                if (myGroup.Load("Requester") == ePLMS_OK)
                    myGroup.addUser(givenUser.Id);
            }
            else if (givenRoleName.ToUpper() == "PMP" || givenRoleName.ToUpper() == "PPM")
            {
                myGroup = new eGroup(ref givenConnection);
                if (myGroup.Load("Controller") == ePLMS_OK)
                    myGroup.addUser(givenUser.Id);
            }
            else if (givenRoleName.ToUpper().StartsWith("PRCDC_"))
            {
                myGroup = new eGroup(ref givenConnection);
                if (myGroup.Load("PR") == ePLMS_OK)
                    myGroup.addUser(givenUser.Id);
                
            }
            else if (givenRoleName.ToUpper().StartsWith("PL_"))
            {
                myGroup = new eGroup(ref givenConnection);
                if (myGroup.Load("PR") == ePLMS_OK)
                    myGroup.addUser(givenUser.Id);
            }
        
            else if (givenRoleName.ToUpper().StartsWith("UR_"))
            {
                myGroup = new eGroup(ref givenConnection);
                if (myGroup.Load("UR") == ePLMS_OK)
                    myGroup.addUser(givenUser.Id);
            }
            return true;
        }
        public static bool RemoveUserRoleToProject(ref eConnection givenConnection, eUser givenUser, string givenRoleName, string myeProjectName, string cdc, string competence,string activitynpi)
        {
            eGroup myGroup;
            eProject myeProject = new eProject(ref givenConnection);
            eProjectRole projectRole = null;
            eProjectRole[] projectRoleList = new eProjectRole[] { };

            myeProject.Load(myeProjectName);
            if (givenRoleName.ToUpper() == "REQUESTER" | givenRoleName.ToUpper() == "PMP" | givenRoleName.ToUpper().StartsWith("UR_"))
                myeProject.Load("RDA");
            else if (givenRoleName.ToUpper() == "PLANNER" || givenRoleName.ToUpper() == "PR")
            {
                if (string.IsNullOrEmpty(cdc))
                    throw new Exception("CDC not defined");
                if (givenRoleName.ToUpper() == "PLANNER")
                    givenRoleName = eRdAHelper.GetPlannerRoleDBValue(cdc);
                else
                {
                    eRdAHelper.RemoveUserRoleList(ref givenConnection, myeProjectName, cdc.ToUpper(), activitynpi, givenUser);
                    givenRoleName = eRdAHelper.GetPRRoleDBValue(givenRoleName, competence);

                }


            }
            string roleToRemove = givenRoleName.ToUpper();
            if (roleToRemove == "UR")
                roleToRemove = "UR_" + cdc.ToUpper();
            myeProject.getProjectRoleList(ref projectRoleList);
            if (projectRoleList != null)
            {
                for (int i = 0; i < projectRoleList.Count(); i++)
                {
                    projectRole = projectRoleList[i];
                    if (projectRole.roleName.ToUpper() == roleToRemove)
                    {
                        eProjectRoleUser[] actualRoleUserList = new eProjectRoleUser[] { };
                        projectRole.getProjectRoleUserList(ref actualRoleUserList);
                        if (actualRoleUserList != null)
                        {
                            foreach (eProjectRoleUser roleUser in actualRoleUserList)
                            {
                                if (roleUser.userId == givenUser.Id)
                                {
                                    projectRole.removeUserFromProjectRole(roleUser);
                                    break;
                                }
                                    
                            }
                        }
                    }
                }
            }
            if (givenRoleName.ToUpper() == "REQUESTER")
            {
                myGroup = new eGroup(ref givenConnection);
                if (myGroup.Load("Requester") == ePLMS_OK)
                    myGroup.removeUser(givenUser.Id);
            }
            else if (givenRoleName.ToUpper() == "PMP")
            {
                myGroup = new eGroup(ref givenConnection);
                if (myGroup.Load("Controller") == ePLMS_OK)
                    myGroup.removeUser(givenUser.Id);
            }
            else if (givenRoleName.ToUpper().StartsWith("PRCDC"))
            {
                if (HasUserPRroles(ref givenConnection, givenUser) == false)
                {
                    myGroup = new eGroup(ref givenConnection);
                    if (myGroup.Load("PR") == ePLMS_OK)
                        myGroup.removeUser(givenUser.Id);
                }
                
            }

            else if (givenRoleName.ToUpper().StartsWith("PL"))
            {
                if (HasUserPLroles(ref givenConnection, givenUser) == false)
                {
                    myGroup = new eGroup(ref givenConnection);
                    if (myGroup.Load("PR") == ePLMS_OK)
                        myGroup.removeUser(givenUser.Id);
                }
            }
            else if (givenRoleName.ToUpper().StartsWith("UR"))
            {
                if (HasUserURroles(ref givenConnection, givenUser) == false)
                {
                    myGroup = new eGroup(ref givenConnection);
                    if (myGroup.Load("UR") == ePLMS_OK)
                        myGroup.removeUser(givenUser.Id);
                }
            }
            
            return true;

        }


        public static void RemoveUserRoleList(ref eConnection givenConnection, string givenProject, string givenRole, string givenActivity, eUser givenUser)
        {
            eList myListCDCApprover = new eList(ref givenConnection);
            eList myListProject=null;
            eList myRoleProject = null;
            eList myActivityProject = null;
            eList myUserProject = null;

            List<listItemElement> dummyList = new List<listItemElement>();
            if (myListCDCApprover.Load("PROJECT_CDCAPPROVERS") == ePLMS_OK)
            {
                myListCDCApprover.getItemList(ref dummyList);
                if (dummyList != null && dummyList.Count > 0)
                {
                    foreach (listItemElement dummyItem in dummyList)
                    {
                        if (((eList)dummyItem.Value).Name.ToLower() == givenProject.ToLower())
                            myListProject = (eList)dummyItem.Value;
                    }
                }
                
                if (myListProject != null)
                {
                    dummyList = null;
                    myListProject.getItemList(ref dummyList);
                    if (dummyList != null && dummyList.Count > 0)
                    {
                        foreach (listItemElement dummyItem in dummyList)
                        {
                            if (((eList)dummyItem.Value).Name.ToLower() == givenRole.ToLower())
                            {
                                myRoleProject = (eList)dummyItem.Value;
                                break;
                            }
                                
                        }
                    }
                    if (myRoleProject != null)
                    {
                        if (string.IsNullOrEmpty(givenActivity))
                        {
                            dummyList = null;
                            myRoleProject.getItemList(ref dummyList);
                            foreach (listItemElement dummyItem in dummyList) {
                                if (((eList)dummyItem.Value).Name.ToLower() == givenUser.Name.ToLower())
                                {
                                    listItemElement remove = dummyItem;
                                    myRoleProject.removeItem(ref remove);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            dummyList = null;
                            myRoleProject.getItemList(ref dummyList);
                            listItemElement activity = new listItemElement();
                            if (dummyList != null && dummyList.Count > 0)
                            {
                                foreach (listItemElement dummyItem in dummyList)
                                {
                                    if (((eList)dummyItem.Value).Name.ToLower() == givenActivity.ToLower())
                                    {
                                        activity = dummyItem;
                                        myActivityProject = (eList)dummyItem.Value;
                                        break;
                                    }
                                }
                            }

                            if (myActivityProject != null)
                            {
                                dummyList = null;
                                myActivityProject.getItemList(ref dummyList);
                                foreach (listItemElement dummyItem in dummyList) {
                                    if (((eList)dummyItem.Value).Name.ToLower() == givenUser.Name.ToLower())
                                    {
                                        listItemElement remove = dummyItem;
                                        if (myActivityProject.removeItem(ref remove) == ePLMS_OK)
                                            myRoleProject.removeItem(ref activity);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public static void RemoveUserRoleList2(ref eConnection givenConnection, string givenProject, string givenActivity)
        {
            eList myListCDCApprover = new eList(ref givenConnection);
            eList myListProject = null;
            eList myRoleProject = null;
            eList myActivityProject = null;
            eList myUserProject = null;

            List<listItemElement> dummyList = new List<listItemElement>();
            List<listItemElement> dummyList1 = new List<listItemElement>();
            List<listItemElement> dummyList2 = new List<listItemElement>();
            if (myListCDCApprover.Load("PROJECT_CDCAPPROVERS") == ePLMS_OK)
            {
                myListCDCApprover.getItemList(ref dummyList);
                if (dummyList != null && dummyList.Count > 0)
                {
                    foreach (listItemElement dummyItem in dummyList)
                    {
                        if (((eList)dummyItem.Value).Name.ToLower() == givenProject.ToLower())
                            myListProject = (eList)dummyItem.Value;
                    }
                }

                if (myListProject != null)
                {
                    dummyList = null;
                    myListProject.getItemList(ref dummyList);
                    if (dummyList != null && dummyList.Count > 0)
                    {
                        foreach (listItemElement dummyItem in dummyList)
                        {
                            myRoleProject = (eList)dummyItem.Value;
                            if (myRoleProject != null)
                            {
                                
                                dummyList1 = null;
                                myRoleProject.getItemList(ref dummyList1);
                                listItemElement activity = new listItemElement();
                                if (dummyList1 != null && dummyList1.Count > 0)
                                {
                                    foreach (listItemElement dummyItem1 in dummyList1)
                                    {
                                        if (((eList)dummyItem1.Value).Name.ToLower() == givenActivity.ToLower())
                                        {
                                            activity = dummyItem1;
                                            myActivityProject = (eList)dummyItem1.Value;
                                            if (myActivityProject != null)
                                            {
                                                dummyList2 = null;
                                                myActivityProject.getItemList(ref dummyList2);
                                                foreach (listItemElement dummyItem2 in dummyList2)
                                                {
                                                    listItemElement remove = dummyItem2;
                                                    if (myActivityProject.removeItem(ref remove) == ePLMS_OK)
                                                        myRoleProject.removeItem(ref activity);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                
                                
                            }
                        }
                    }
                    
                }
            }
        }

        public static void SaveUserRoleList(ref eConnection givenConnection, string givenProject, string givenRole, string givenActivity, string givenUser)
        {
            eList myListCDCApprover = new eList(ref givenConnection);
            eList myListProject = null;
            eList myRoleProject = null;
            eList myActivityProject = null;
            eList myUserProject = null;

            List<listItemElement> dummyList = new List<listItemElement>();
            if (myListCDCApprover.Load("PROJECT_CDCAPPROVERS") == ePLMS_OK)
            {
                myListCDCApprover.getItemList(ref dummyList);
                if (dummyList != null && dummyList.Count > 0)
                {
                    foreach (listItemElement dummyItem in dummyList)
                    {
                        if (((eList)dummyItem.Value).Name.ToLower() == givenProject.ToLower())
                        {
                            myListProject = (eList)dummyItem.Value;
                            break;
                        }
                    }
                }
                if (myListProject == null)
                {
                    myListProject = new eList(ref givenConnection, myListCDCApprover, ePLMSListItemTypeList);
                    myListProject.Name = givenProject.ToUpper();
                    if (myListProject.Create() != ePLMS_OK)
                        throw new Exception(givenConnection.errorMessage);
                }

                if (myListProject != null)
                {
                    dummyList = null;
                    myListProject.getItemList(ref dummyList);
                    if (dummyList != null && dummyList.Count > 0)
                    {
                        foreach (listItemElement dummyItem in dummyList)
                        {
                            if (((eList)dummyItem.Value).Name.ToLower() == givenRole.ToLower())
                                myRoleProject = (eList)dummyItem.Value;
                        }
                    }

                    if (myRoleProject == null)
                    {
                        myRoleProject = new eList(ref givenConnection, myListProject, ePLMSListItemTypeList);
                        myRoleProject.Name = givenRole.ToUpper();
                        if (myRoleProject.Create() != ePLMS_OK)
                            throw new Exception(givenConnection.errorMessage);
                    }

                    if (myRoleProject != null)
                    {
                        if (string.IsNullOrEmpty(givenActivity))
                        {
                            myUserProject = new eList(ref givenConnection, myRoleProject, ePLMSListItemTypeList);
                            myUserProject.Name = givenUser.ToUpper();
                            if (myUserProject.Create() != ePLMS_OK)
                                throw new Exception(givenConnection.errorMessage);
                        }
                        else
                        {
                            dummyList = null;
                            myRoleProject.getItemList(ref dummyList);
                            if (dummyList != null && dummyList.Count > 0)
                            {
                                foreach (listItemElement dummyItem in dummyList)
                                {
                                    if (((eList)dummyItem.Value).Name.ToLower() == givenActivity.ToLower())
                                        myActivityProject = (eList)dummyItem.Value;
                                }
                            }

                            if (myActivityProject == null)
                            {
                                myActivityProject = new eList(ref givenConnection, myRoleProject, ePLMSListItemTypeList);
                                myActivityProject.Name = givenActivity.ToUpper();
                                if (myActivityProject.Create() != ePLMS_OK)
                                    throw new Exception(givenConnection.errorMessage);
                            }

                            if (myActivityProject != null)
                            {
                                myUserProject = new eList(ref givenConnection, myActivityProject, ePLMSListItemTypeList);
                                myUserProject.Name = givenUser.ToUpper();
                                if (myUserProject.Create() != ePLMS_OK)
                                    throw new Exception(givenConnection.errorMessage);
                            }
                        }
                    }
                }
            }
        }

        public static bool HasUserPRroles(ref eConnection givenConnection,eUser givenUser)
        {
            string query = "SELECT        top(1) eplms.eRole.eName as CDC												" +
"FROM            eplms.eRole INNER JOIN																					" +
"                         eplms.eProjectRole ON eplms.eRole.eId = eplms.eProjectRole.eRoleId INNER JOIN					" +
"                         eplms.eProjectRoleUser ON eplms.eProjectRole.eId = eplms.eProjectRoleUser.eProjectRoleId		" +
"WHERE        (eplms.eRole.eName LIKE N'PRCDC_%') AND (eplms.eProjectRoleUser.eUserId = {0})							" +
"																														" +
"union all																												" +
"																														" +
"SELECT        top(1) eListItem_1.eValue AS CDC 																		" +
"FROM            eplms.eList INNER JOIN																					" +
"                         eplms.eListItem ON eplms.eList.eId = eplms.eListItem.eListId INNER JOIN						" +
"                         eplms.eListItem AS eListItem_1 ON eplms.eListItem.eValueId = eListItem_1.eListId INNER JOIN	" +
"                         eplms.eListItem AS eListItem_2 ON eListItem_1.eValueId = eListItem_2.eListId					" +
"WHERE        (eplms.eList.eName = N'PROJECT_CDCAPPROVERS') and  eListItem_2.eValue ='{1}'					" +
"																														" +
"union all																												" +
"																														" +
"SELECT       top(1) eListItem_1.eValue AS CDC 																			" +
"FROM            eplms.eList INNER JOIN																					" +
"                         eplms.eListItem ON eplms.eList.eId = eplms.eListItem.eListId INNER JOIN						" +
"                         eplms.eListItem AS eListItem_1 ON eplms.eListItem.eValueId = eListItem_1.eListId INNER JOIN	" +
"                         eplms.eListItem AS eListItem_2 ON eListItem_1.eValueId = eListItem_2.eListId INNER JOIN		" +
"                         eplms.eListItem AS eListItem_3 ON eListItem_2.eValueId = eListItem_3.eListId					" +
"WHERE        (eplms.eList.eName = N'PROJECT_CDCAPPROVERS') and  eListItem_3.eValue ='{1}'					";

            query = string.Format(query, givenUser.Id, givenUser.Name);
            DataTable givenTable = ePortalHelper.ExecuteQuery(ref givenConnection, query);
            return givenTable.Rows.Count > 0;
        }
        public static bool HasUserPLroles(ref eConnection givenConnection, eUser givenUser)
        {
            string query = "SELECT        top(1) eplms.eRole.eName as CDC												" +
"FROM            eplms.eRole INNER JOIN																					" +
"                         eplms.eProjectRole ON eplms.eRole.eId = eplms.eProjectRole.eRoleId INNER JOIN					" +
"                         eplms.eProjectRoleUser ON eplms.eProjectRole.eId = eplms.eProjectRoleUser.eProjectRoleId		" +
"WHERE        (eplms.eRole.eName LIKE N'PL_%') AND (eplms.eProjectRoleUser.eUserId = {0})							";

            query = string.Format(query, givenUser.Id, givenUser.Name);
            DataTable givenTable = ePortalHelper.ExecuteQuery(ref givenConnection, query);
            return givenTable.Rows.Count > 0;
        }
        public static bool HasUserURroles(ref eConnection givenConnection, eUser givenUser)
        {
            string query = "SELECT        top(1) eplms.eRole.eName as CDC												" +
"FROM            eplms.eRole INNER JOIN																					" +
"                         eplms.eProjectRole ON eplms.eRole.eId = eplms.eProjectRole.eRoleId INNER JOIN					" +
"                         eplms.eProjectRoleUser ON eplms.eProjectRole.eId = eplms.eProjectRoleUser.eProjectRoleId		" +
"WHERE        (eplms.eRole.eName LIKE N'UR_%') AND (eplms.eProjectRoleUser.eUserId = {0})							";

            query = string.Format(query, givenUser.Id, givenUser.Name);
            DataTable givenTable = ePortalHelper.ExecuteQuery(ref givenConnection, query);
            return givenTable.Rows.Count > 0;
        }

        #endregion

        #region "UTILITY"
        public static object GetPropValue(object source, string propertyName)
        {
            var property = source.GetType().GetRuntimeProperties().FirstOrDefault(p => string.Equals(p.Name, propertyName, StringComparison.OrdinalIgnoreCase));
            return property?.GetValue(source);
        }

        public static void FIXYEAR(DataTable givenTable, string givenYear, ref eConnection givenConnection)
        {
            if (string.IsNullOrEmpty(givenYear))
                givenYear = DateTime.Now.ToString("yyyy");
            string insertQuery = "INSERT [dbo].[SUPPLIERBUDGETYEAR] ([SUPPLIERID], [YEAR], [BUDGET]) VALUES ({0}, N'{1}', N'{2}')";
            string dummyInser = "";
            System.Text.StringBuilder myStringBuilder = new System.Text.StringBuilder();

            if (givenTable != null)
            {
                foreach (DataRow row in givenTable.Rows)
                {
                    if (row["Budget"] == DBNull.Value)
                    {
                        dummyInser = string.Format(insertQuery, row["ID"], givenYear, "");
                        myStringBuilder.AppendLine(dummyInser);
                    }
                }
            }
            dummyInser = myStringBuilder.ToString();
            if (!string.IsNullOrEmpty(dummyInser))
                ePortalHelper.ExecuteQuery(ref givenConnection, myStringBuilder.ToString());
        }

        public static string GetDelayClass(int days)
        {
            string returnColorStatus = "far fa-circle";
            string prefixIcon = "fas";
            if (days <= 2)
                returnColorStatus = prefixIcon + " lessdelay";
            else if (days > 2 && days <= 5)
                returnColorStatus = prefixIcon + " middledelay";
            else if (days > 5)
                returnColorStatus = prefixIcon + " highdelay";

            return returnColorStatus;
        }

        public static void GetIdsAndWorstCaseColorOnActionDate(ref eConnection givenConnection, ref DataRow[] givenRows, ref List<long> givenObjectInSign, ref List<eRdABasicView> returnIds, ref string returnColorStatus)
        {
            returnColorStatus = "far fa-circle";
            string prefixIcon = "fas";
            double days = 0;
            try
            {
                if (givenRows != null && givenRows.Length > 0)
                {
                    IEnumerable<eRdABasicView> data = new ExtensionsMapper<eRdABasicView>()
                      .SetDataTable(givenRows.CopyToDataTable())
                      .SetPrefix("")
                      .Map();
                    returnIds = data.ToList();
                    DateTime myMinDate = DateTime.MaxValue;
                    if (returnIds != null && returnIds.Count > 0)
                    {
                        foreach (eRdABasicView card in returnIds)
                        {
                            if (givenObjectInSign.Contains(int.Parse(card.eId)) || givenConnection.isCurrentUserDba)
                                card.signstatus = "";
                            if (card.eLevel == "Under Control" || card.eLevel == "Controlled")
                                card.signstatus = "noaction";
                            if (myMinDate > card.ActionDate)
                                myMinDate = card.ActionDate;
                            days = DateTime.Now.Subtract(card.ActionDate).TotalDays;
                            if (days <= 2)
                                card.rdaactionstatus = prefixIcon + " lessdelay";
                            else if (days > 2 && days <= 5)
                                card.rdaactionstatus = prefixIcon + " middledelay";
                            else if (days > 5)
                                card.rdaactionstatus = prefixIcon + " highdelay";
                            card.rdaactiontitle = string.Format("{0} day(s) of delay", Math.Round(Math.Abs(days), 0).ToString());
                        }
                    }
                    days = DateTime.Now.Subtract(myMinDate).TotalDays;
                    if (days <= 2)
                        returnColorStatus = prefixIcon + " lessdelay";
                    else if (days > 2 && days <= 5)
                        returnColorStatus = prefixIcon + " middledelay";
                    else if (days > 5)
                        returnColorStatus = prefixIcon + " highdelay";
                }
            }
            catch (Exception e) { };



        }

        public static void GetIds(ref eConnection givenConnection, ref DataRow[] givenRows, ref List<eRdABasicView> returnIds, ref string returnColorStatus)
        {
            returnColorStatus = "far fa-circle";
            try
            {
                if (givenRows != null && givenRows.Length > 0)
                {
                    IEnumerable<eRdABasicView> data = new ExtensionsMapper<eRdABasicView>()
                      .SetDataTable(givenRows.CopyToDataTable())
                      .SetPrefix("")
                      .Map();
                    returnIds = data.ToList();
                }
            }
            catch (Exception e) { };
        }

        public static double GetDoubleValue(string givenValue)
        {
            if (IsNumeric(givenValue))
            {
                givenValue = givenValue.ToString().Replace(",", ".");
                return Convert.ToDouble(givenValue, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            }
            else
                return 0;
        }

        public static bool IsNumeric(System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { } // just dismiss errors but return false
            return false;
        }
        public static DataTable GetRdAOnRemedy(ref eConnection givenConnection, string givenCDC, string givenSupplier, string givenYear)
        {
            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConnection);

            eList myList = new eList(ref dbaConnection);
            string startDate = "";
            string endDate = "";
            if (myList.Load("REMEDY_RDA_RANGE") != ePLMS_OK)
            {
                myList = new eList(ref dbaConnection);
                myList.Name = "REMEDY_RDA_RANGE";
                if (myList.Create() == ePLMS_OK)
                {
                    myList.addItem("start-date", "03.12");
                    myList.addItem("end-date", "03.12");
                }
            }
           
            string myQuery = "SELECT  eId, Amount, baan_committed as Committed, LOWER(Supplier) AS [Supplier], CDC AS [CDC] from eplms.rdadb";

            //myQuery = myQuery + " where eLevel='Closed' and eWorkflow='WF_RDA_01' and CBS = 'R&D Hours' and RequestedSupplier = 'REMEDY'";
            myQuery = myQuery + " where eLevel='Closed' and eWorkflow='WF_RDA_01' and RequestedSupplier = 'REMEDY'";

            if (!string.IsNullOrEmpty(givenCDC))
                myQuery = myQuery + " and CDC = '" + givenCDC + "' ";
            if (!string.IsNullOrEmpty(givenSupplier))
                myQuery = myQuery + " and Supplier = '" + givenSupplier + "' ";
            myQuery = myQuery + " and YEAR(ActivityEnd) = " + givenYear ;

            DataTable myTable = ePortalHelper.ExecuteQuery(ref dbaConnection, myQuery);
            ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
            return myTable;
        }
        public static eUserDashboardData GetProjectListCard(ref DataTable givenTable)
        {
            eUserDashboardData retValue = new eUserDashboardData();
            if (givenTable != null && givenTable.Rows.Count > 0)
            {
                IEnumerable<eRdABasicView> result = new ExtensionsMapper<eRdABasicView>()
                     .SetDataTable(givenTable)
                     .SetPrefix("")
                     .Map();

                for (var index = 0; index < result.Count(); index++)
                {
                    var r = result.ElementAt(index);
                    eProjectRdaListView pj = new eProjectRdaListView();
                    pj.ProjectName = r.eProject;
                    for (var index2 = index; index2 < result.Count(); index2++)
                    {
                        var r2 = result.ElementAt(index2);
                        if (r2.eProject != r.eProject)
                        {
                            retValue.projects.Add(pj);
                            index = index2 - 1;
                            break;
                        }
                        else
                            index = index2;
                        switch (r2.eLevel.ToUpper())
                        {
                            case "WORKING":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayReq)
                                    pj.delayReq = r2.delay;
                                pj.RdAInWorkingStatus = eRdAHelper.GetDelayClass(pj.delayReq);
                                pj.RdAInWorking = (int.Parse(pj.RdAInWorking) + r2.TotalNo).ToString();
                                break;
                            case "PR VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayPR)
                                    pj.delayPR = r2.delay;
                                pj.RdAInValidationStatus = eRdAHelper.GetDelayClass(pj.delayPR);
                                pj.RdAInValidation = (int.Parse(pj.RdAInValidation) + r2.TotalNo).ToString();
                                break;
                            case "CMS VALIDATION":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayCMS)
                                    pj.delayCMS = r2.delay;
                                pj.RdAInCMSValidationStatus = eRdAHelper.GetDelayClass(pj.delayCMS);
                                pj.RdAInCMSValidation = (int.Parse(pj.RdAInCMSValidation) + r2.TotalNo).ToString();
                                break;
                            case "UNDER CONTROL":
                            case "CONTROLLED":
                            case "CLOSED":
                                pj.totalRdA += r2.TotalNo;
                                if (r2.delay > pj.delayContr)
                                    pj.delayContr = r2.delay;
                                pj.RdAInCreatedStatus = eRdAHelper.GetDelayClass(pj.delayContr);
                                pj.RdAInCreated = (int.Parse(pj.RdAInCreated) + r2.TotalNo).ToString();
                                break;
                        }
                    }
                    if (index == result.Count() - 1)
                        retValue.projects.Add(pj);
                }
            }

            return retValue;
        }

        public static eUserDashboardData GetObjectCardInProject(ref DataTable givenTable, bool isDba)
        {
            eUserDashboardData retValue = new eUserDashboardData();
            if (givenTable != null && givenTable.Rows.Count > 0)
            {
                IEnumerable<eRdABasicView> result = new ExtensionsMapper<eRdABasicView>()
                     .SetDataTable(givenTable)
                     .SetPrefix("")
                     .Map();

                eProjectRdaListView pj = new eProjectRdaListView();
                foreach (eRdABasicView rda in result)
                {
                    rda.rdaactionstatus = GetDelayClass(rda.delay);
                    rda.rdaactiontitle = string.Format("{0} day(s) of delay", rda.delay.ToString());
                    if (rda.InSign > 0 || isDba)
                        rda.signstatus = "";

                    switch (rda.eLevel.ToUpper())
                    {
                        case "WORKING":
                            pj.RdAInWorkingList.Add(rda);

                            break;
                        case "PR VALIDATION":
                            pj.RdAInValidationList.Add(rda);
                            break;
                        case "CMS VALIDATION":
                            pj.RdAInCMSValidationList.Add(rda);
                            break;
                        case "UNDER CONTROL":
                        case "CONTROLLED":
                        case "CLOSED":
                            rda.signstatus = "noaction";
                            pj.RdAInCreatedList.Add(rda);
                            break;


                    }
                }
                retValue.projects.Add(pj);


            }
            return retValue;
        }

        public static string GetSpendingText(string givenSpendingTextAdmin, string givenId, string givenAmount, string givenFormat, string givenLevel, double BaanIn_DataExchange, string BaanIn_ExchangeMessage, ref string colorRef, bool includeTemplate = true)
        {
            string template = "<a title='View More' refId='{1}' role='button' class='rdalinkbutton  x-btn x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon x-border-box' hidefocus='on' unselectable='on'><span role='presentation' class='x-btn-wrap' unselectable='on'><span class='x-btn-button' role='presentation'><span class='x-btn-inner x-btn-inner-center' unselectable='on'>{0}</span><span role='presentation' class='x-btn-icon-el' unselectable='on' style=''></span></span></span></a>";
            string[] splitValue = givenSpendingTextAdmin.Split("|".ToCharArray());
            int tranche = int.Parse(splitValue[0]);
            double rate = eRdAHelper.GetDoubleValue(splitValue[1]);
            string invoicedate = splitValue[2];
            string myFormatAmount = "";
            string prefixView = "";

            if (IsNumeric(givenAmount))
            {
                if (givenFormat == "" || givenFormat == "euro")
                    myFormatAmount = eRdAHelper.GetDoubleValue(givenAmount).ToString("C", CultureInfo.CreateSpecificCulture("it-IT"));
                else
                    myFormatAmount = eRdAHelper.GetDoubleValue(givenAmount).ToString("C", CultureInfo.CreateSpecificCulture("en-US"));
            }
            prefixView = "<i class='grayflag fas fa-eye'></i>";
            colorRef = "gray";
            if (givenLevel.ToLower() == "closed")
            {
                prefixView = "<i class='greenflag fas fa-eye'></i>";
                colorRef = "green";
            }
            else
            {
                if (BaanIn_DataExchange > 0)
                {
                    prefixView = "<i class='yellowflag fas fa-eye'></i>";
                    colorRef = "yellow";
                    
                    if (!string.IsNullOrEmpty(BaanIn_ExchangeMessage))
                    {
                        prefixView = "<i class='redflag fas fa-eye'></i>";
                        colorRef = "red";
                    }
                        
                }

            }
            template = string.Format(template, prefixView, long.Parse(givenId));
            if (!includeTemplate)
                template = "";
            else
                template = template + " ";
            string spend = string.Format("{0}tranche {1}% of the total amount {2} in date {3}", template, rate, myFormatAmount, invoicedate.Replace(" ", ""));



            return spend;
        }

        public static void CancelSubTranche(ref eConnection dbaConnection, ref eConnection userConnection, eBusinessData givenBusinessData)
        {
            string myQuery = "select *," + "eplms.GetAttributeValue('BaanIn_DataExchange',eId) as BaanIn_DataExchange, " + "eplms.GetAttributeValue('RDACode',eId) as RDACode, " + "eplms.GetAttributeValue('rdaw1_spendingcurve_admin',eId) as rdaw1_spendingcurve_admin  " + "from eplms.eBusinessData where eWorkflow = N'WF_RDA_01' and eKey1=" + givenBusinessData.Id;

            System.Data.DataTable myTableResult = ePortalHelper.ExecuteQuery(ref dbaConnection, myQuery);
            if (myTableResult != null)
            {
                eBusinessData myChild = new eBusinessData(ref dbaConnection);

                for (int idx = myTableResult.Rows.Count - 1; idx >= 0; idx += -1)
                {
                    var currentRow = myTableResult.Rows[idx];
                    ArrayList attributes = new ArrayList();
                    if (myChild.Load(int.Parse(currentRow["eId"].ToString())) == ePLMS_OK)
                    {
                        myChild.Reserve();
                        myChild.Workflow = "WF_CANCEL";
                        myChild.Modify(ref attributes);
                        myChild.Unreserve();

                        eRdAWorkflowAction givenWorkflowAction = new eRdAWorkflowAction();
                        givenWorkflowAction.businessDataId = myChild.Id;
                        givenWorkflowAction.actionType = eRdAWFActionType.APPROVE;
                        eRdAHelper.BusinessDataPerformTask(ref dbaConnection, givenWorkflowAction);

                        string refDate = DateTime.Now.ToString("yyyyMMdd000000");
                        dbaConnection.Core.stringToDate(refDate,ref refDate);
						refDate = refDate.Split(" ".ToCharArray())[0];
                        myChild.changeAttribute("ActionDate", refDate);
                        myChild.changeAttribute("ActionComment", "Cancelled by " + userConnection.currentUserName);
                    }
                }
            }
        }


        public static DataTable ExecuteLongQuery(ref eConnection givenConnection, string sql)
        {
            int myDBErrorNumber = 0;
            string myDBErrorMsg = null;
            int myDBResultRecords = 0;
            IDbConnection myDBConnetion = null/* TODO Change to default(_) if this is not a reference type */;
            var Rc = givenConnection.SQLAdapter.openPersistentConnection(ref myDBConnetion, ref myDBErrorNumber, ref myDBErrorMsg);
            DataTable findTable = null/* TODO Change to default(_) if this is not a reference type */;
            if (Rc == Parallaksis.ePLMSM40.Defs.eErrorDefs.ePLMS_OK)
            {
                Parallaksis.ePLMS.Security.pSecurity mySecurity = new Parallaksis.ePLMS.Security.pSecurity();

                string password = "";
                mySecurity.DecryptBase64CNV(givenConnection.ePLMS_EPLMS_PASSWORD, ref password);

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(myDBConnetion.ConnectionString + "password=" + password + ";");
                builder.ConnectTimeout = 300;
                givenConnection.SQLAdapter.closePersistentConnection(ref myDBConnetion);

                // Dim con As SqlConnection = New SqlConnection(builder.ToString)
                // Dim myQuery As String = myFind.SQLQueryString
                findTable = new DataTable();
                using (SqlConnection cnn = new SqlConnection(builder.ToString()))
                {
                    try
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand(sql, cnn);
                        cmd.CommandTimeout = 0;
                        try
                        {
                            using (SqlDataAdapter dad = new SqlDataAdapter(cmd))
                            {
                                dad.Fill(findTable);
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        cnn.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }

            try
            {
            }
            catch (Exception ex)
            {
            }

            return findTable;
        }

        public static string GetPlannerRoleDBValue(string cdclabel)
        {
            cdclabel = cdclabel.ToUpper();
            return "PL_" + cdclabel.Replace(" & ", "-").Replace(" ", "_").Replace("/", "_");
        }
        public static string GetURRoleDBValue(string cdclabel)
        {
            cdclabel = cdclabel.ToUpper();
            return "UR_" + cdclabel.Replace(" & ", "-").Replace(" ", "_").Replace("/", "_");
        }
        public static string GetPRRoleDBValue(string cdclabel, string competence)
        {
            return "PRCDC_" + competence;
        }

        public static string[] PrepareFind(ref eConnection givenConnection, List<KeyValuePair<string, string>> queryParameters, ref string queryString)
        {
            var counter = 1;
            var parameterList = new List<string>();
            string[] parameters;
            DataTable attributesDate = ePortalHelper.ExecuteQuery(ref givenConnection, "select eName from eplms.eAttribute where eType='DATE'");
            var paramsToRemove = queryParameters.Where(p => p.Key == "pageSize" || p.Key == "page").ToList();
            foreach (var param in paramsToRemove)
                queryParameters.Remove(param);

            if (!queryParameters.Any())
            {
                var param = new KeyValuePair<string, string>("BusinessDataType", "*");
                queryParameters.Add(param);
            }

            var preparedParams = PrepareParams(queryParameters);

            foreach (var item in preparedParams)
            {
                if (queryString != "")
                {
                    queryString += " and ";
                }
                if (item.Value is string)
                {
                    if (item.Key == "quicksearch")
                    {
                        queryString += "(ename = {" + counter + "} or edescription = {" + counter + "})";
                        parameterList.Add("*" + item.Value + "*");
                        counter++;
                    }
                    // Here
                    else
                    {
                        bool isdate = false;
                        // From comparison
                        if (item.Key.Equals("createDateFrom") || item.Key.Equals("modifyDateFrom"))
                        {
                            isdate = true;
                            queryString += "CONVERT(VARCHAR(8), e" + new Regex("from", RegexOptions.IgnoreCase).Replace(item.Key, string.Empty) + ", 112) >= {" + counter + "}";
                            //queryString += new Regex("from", RegexOptions.IgnoreCase).Replace(item.Key, string.Empty) + " >= {" + counter + "}";
                        }
                        // To comparison
                        else if (item.Key.Equals("createDateTo") || item.Key.Equals("modifyDateTo"))
                        {
                            isdate = true;
                            queryString += "CONVERT(VARCHAR(8), e" + new Regex("to", RegexOptions.IgnoreCase).Replace(item.Key, string.Empty) + ", 112) <= {" + counter + "}";
                            //queryString += new Regex("to", RegexOptions.IgnoreCase).Replace(item.Key, string.Empty) + " <= {" + counter + "}";
                        }
                        // Default comparison
                        else
                        {

                            if ((item.Key.ToLower()=="ecreatedate" || item.Key.ToLower()=="emodifydate" || item.Key.ToLower()=="ereservedate") || 
                                attributesDate != null && attributesDate.Rows.Count > 0 && attributesDate.Select("eName='" + item.Key + "'").Length > 0)
                            {
                                isdate = true;
                                queryString += "CONVERT(VARCHAR(8), " + item.Key + ", 112) = {" + counter + "}";
                            }
                            else
                                queryString += item.Key + " = {" + counter + "}";
                        }
                        if (isdate)
                        {
                            string newdate = item.Value.ToString();
                            if (newdate.Length > 8)
                                newdate = newdate.Substring(0, 8);
                            parameterList.Add(newdate);
                        }
                        else
                            parameterList.Add((string)item.Value);
                        counter++;
                    }
                }
                // If passing an IEnumerable compare with or
                else if (item.Value is List<string>)
                {
                    if (item.Key == "quicksearch")
                    {
                        throw new NotImplementedException();
                    }
                    else
                    {
                        queryString += "(";
                        for (var index = 0; index < ((List<string>)item.Value).Count; index++)
                        {
                            var listItem = ((List<string>)item.Value)[index];

                            if (index != 0) queryString += " or ";
                            queryString += item.Key + " = {" + counter + "}";

                            parameterList.Add(listItem);
                            counter++;
                        }
                        queryString += ")";
                    }
                }
            }

            parameters = parameterList.ToArray();
            return parameters;
        }
        public static Dictionary<string, object> PrepareParams(List<KeyValuePair<string, string>> queryParameters)
        {
            var returnList = new Dictionary<string, object>();
            string basicKey = "Id;BaselineId;BusinessDataType;Workflow;AssignLevelTaskId;CurrentTaskId;Project;LevelPosition;Name;Description;Revision;Owner;ModifyUser;CreateUser;ReserveUser;CreateDate;ModifyDate;ReserveDate;Level;Key1;Key2;Key3";

            foreach (KeyValuePair<string, string> item in queryParameters)
            {

                

                if (item.Key == "revision")
                    continue;
                
                string key =  item.Key + ";";

                if (basicKey.ToLower().Contains(key.ToLower()))
                    key = "e" + item.Key;
                else
                    key = item.Key; 
                if (returnList.ContainsKey(key))
                {
                    if (returnList[key] is string)
                    {
                        returnList[key] = new List<string> { (string)returnList[key], item.Value };
                    }
                    else if (returnList[key] is List<string>)
                    {
                        ((List<string>)returnList[key]).Add(item.Value);
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
                else
                {
                    returnList.Add(key, item.Value);
                }
            }

            
           
            return returnList;
        }
        #endregion


        #region "SYNC BAAN"
        public static string GetTextTrucate(string givenText, int truncateIndex)
        {
            string retvalue = givenText.Replace(System.Environment.NewLine, " ");
            if (givenText.Length > truncateIndex)
                retvalue = givenText.Substring(0, truncateIndex);
            return retvalue;
        }

        public static string GetSupplierCode(ref eConnection givenConnection, string givenSupplier)
        {
            string myFindQuery = "";
            string givenText = givenSupplier;
            givenText = givenText.Replace("\"", "%");
            givenText = givenText.Replace("'", "%");
            givenText = givenText.Replace("[", "%");
            givenText = givenText.Replace("]", "%");
            givenText = givenText.Replace(" ", "%");
            givenText = givenText.Replace("%%", "%");
            myFindQuery = "Select Code,Name from FORNITORI where Name like '" + givenText + "'";


            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection, myFindQuery);
            if (myTable != null || myTable.Rows.Count > 0)
            {
                foreach (DataRow row in myTable.Rows)
                {
                    if (row["Name"].ToString().ToLower() == givenSupplier.ToLower())
                        return row["Code"].ToString();
                }
            }
            return "";
        }

        public static string GetBuyerCode(ref eConnection givenConnection, string givenSupplier)
        {
            string myFindQuery = "";
            string givenText = givenSupplier;
            givenText = givenText.Replace("\"", "%");
            givenText = givenText.Replace("'", "%");
            givenText = givenText.Replace("[", "%");
            givenText = givenText.Replace("]", "%");
            givenText = givenText.Replace(" ", "%");
            givenText = givenText.Replace("%%", "%");
                
            myFindQuery = "Select Buyer,Name from FORNITORI where Name like '" + givenText + "'";


            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection, myFindQuery);
            if (myTable != null || myTable.Rows.Count > 0)
            {
                foreach (DataRow row in myTable.Rows)
                {
                    if (row["Name"].ToString().ToLower() == givenSupplier.ToLower())
                        return row["Buyer"].ToString();
                }
            }
            return "";
        }

        public static string GetContentBaan(ref eConnection givenConnection, string erdaProject, string erdaContent)
        {
            string retContent = erdaContent;
            string myQuery = "SELECT eplms.GetContentBaan('" + erdaProject + "', '" + erdaContent + "')";
            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (myTable != null && myTable.Rows.Count > 0 && !string.IsNullOrEmpty(myTable.Rows[0][0].ToString().Trim()))
                retContent = myTable.Rows[0][0].ToString();

            return  retContent.Trim();
        }

        public static string GetProjectBaan(ref eConnection givenConnection, string erdaProject)
        {
            string retProject = "";
            string myQuery = "SELECT eplms.GetProjectBaan('" + erdaProject + "')";
            DataTable myTable = ePortalHelper.ExecuteQuery(ref givenConnection, myQuery);
            if (myTable != null)
                retProject = myTable.Rows[0][0].ToString();

            return retProject.Trim();
        }
        
        public static void SetUpPESdefaultValue(ref eConnection givenConnection, ref DataTable givenTable, bool saveDataFlag = false)
        {
            ArrayList userAttribute;

            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConnection);
            try
            {
                if (givenTable != null && givenTable.Rows.Count > 0)
                {
                    if (saveDataFlag)
                        userAttribute = new ArrayList();
                    if (!givenTable.Columns.Contains("Sys_Error"))
                        givenTable.Columns.Add("Sys_Error", typeof(string));

                    foreach (DataRow r in givenTable.Rows)
                    {
                        System.Text.StringBuilder errors = new System.Text.StringBuilder();
                        string supplier = "";
                        try
                        {
                            supplier=r["Supplier"].ToString();
                        }
                        catch (Exception e) { }
                            
                            
                        foreach (DataColumn c in givenTable.Columns)
                        {
                            var currentCell = r[c].ToString();
                            //if (string.IsNullOrEmpty(currentCell))
                            //{
                            //if (r[c] == DBNull.Value && !c.DataType.Equals(typeof(DateTime)))
                            //    r[c] = "";
                            switch (c.ColumnName.ToLower())
                            {
                                case "edescription":
                                    if (saveDataFlag)
                                        r[c] = GetTextTrucate(r[c].ToString(), 30);
                                    break;
                                case "supplier":

                                    string requested = r["RequestedSupplier"].ToString();
                                    if (requested.ToUpper() == "TO BE SOURCED" && saveDataFlag)
                                        r[c] = "PPPPPP";
                                    else if (requested.ToUpper() == "TO BE SOURCED" && !saveDataFlag)
                                        r[c] = "Fornitore da Definire";
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(currentCell))
                                        {
                                            string code = GetSupplierCode(ref dbaConnection, currentCell);
                                            code = GetTextTrucate(code.ToString(), 6);
                                            //if (saveDataFlag)
                                            r[c] = code;
                                        }

                                    }
                                    if (string.IsNullOrEmpty(r[c].ToString()))
                                    {
                                        errors.AppendLine(GetMessageErrorBaanTable("Not a valid supplier", "Supplier"));
                                        r[c] = currentCell;
                                    }
                                    else if (!saveDataFlag)
                                        r[c] = currentCell;
                                    break;
                                case "baanin_dataerosionebudget":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = DateTime.Now;
                                    break;
                                case "baanin_articolo":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "PRESTAZ02";
                                    break;
                                case "baanin_tiporichiesta":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "R&S";
                                    break;
                                case "baanin_um":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "NR";
                                    break;
                                case "hours":
                                    if (string.IsNullOrEmpty(currentCell) || GetDoubleValue(r[c].ToString()) == 0)
                                        r[c] = 1;
                                    if (saveDataFlag)
                                        r[c] = GetDoubleValue(currentCell) * 100;

                                    break;
                                case "amount":
                                    if (saveDataFlag)
                                        r[c] = GetDoubleValue(currentCell) * 1000000;
                                    break;
                                case "contents":
                                    if (!string.IsNullOrEmpty(currentCell))
                                    {
                                        if (string.IsNullOrEmpty(GetContentBaan(ref dbaConnection, r["eProject"].ToString(), currentCell)))
                                            errors.AppendLine(GetMessageErrorBaanTable("Not a valid content", "Content"));
                                    }
                                    break;
                                case "baanin_conto":
                                    if (string.IsNullOrEmpty(currentCell))
                                    {
                                        switch (r["CBS"].ToString().ToUpper())
                                        {
                                            case "R&D HOURS":
                                                r[c] = "53800015";
                                                break;
                                            case "WORK PACKAGE":
                                                r[c] = "53800021";
                                                break;
                                            case "CODESIGN":
                                                r[c] = "53800048";
                                                break;
                                            case "TEST/TOOL PROTO/OUTSOURCE&FACILITIES":
                                                if (r["RDACompetence"].ToString() == "TRANSVERSAL")
                                                    r[c] = "53800011";
                                                else if (r["RDACompetence"].ToString() == "VEHICLE")
                                                {
                                                    if (r["WBS"].ToString().ToLower().Contains("Tooling Proto".ToLower()))
                                                        r[c] = "53800038";
                                                    else if (r["WBS"].ToString().ToLower().Contains("Outsource".ToLower()))
                                                        r[c] = "53800011";
                                                    else if (r["WBS"].ToString().ToLower().Contains("Test".ToLower()))
                                                        r[c] = "53800113";

                                                }
                                                break;
                                        }
                                    }
                                    break;
                                case "baanin_commessa":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = r["CodeCommessa"];
                                    break;
                                case "baanin_magazzino":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "TIN";
                                    break;
                                case "baanin_cdc":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "M0200";
                                    break;
                                case "purchasing":
                                    string mailingTo = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\scripts\mailinglist\pes.eplms");
                                    if (System.IO.File.Exists(mailingTo))
                                        mailingTo = System.IO.File.ReadAllText(mailingTo);
                                    else
                                        mailingTo = "";
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = SetMailingList(ref dbaConnection, ref givenConnection, mailingTo, r["eId"].ToString());
                                    break;
                                case "baanin_buyer":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = GetBuyerCode(ref dbaConnection, supplier);
                                    break;
                                case "codeprojectform":
                                    if (string.IsNullOrEmpty(currentCell))
                                        errors.AppendLine(GetMessageErrorBaanTable("Invalid code", "Code Project Form"));

                                    break;
                                case "baanin_rigatesto":
                                    break;
                                case "baanin_requester":
                                    if (saveDataFlag || string.IsNullOrEmpty(currentCell))
                                    {
                                        r[c] = "";
                                        string query = "SELECT TOP(1) eplms.ePerson.eText1 " +
                                                        " FROM            eplms.eUser INNER JOIN " +
                                                        " eplms.eUserPerson ON eplms.eUser.eId = eplms.eUserPerson.eUserId INNER JOIN " +
                                                        " eplms.ePerson ON eplms.eUserPerson.ePersonId = eplms.ePerson.eId where eplms.ePerson.eText1<>'' and  eUser.eName='{0}' ";
                                        query = string.Format(query, givenConnection.currentUserName);
                                        DataTable dummyTable = ePortalHelper.ExecuteQuery(ref givenConnection, query);
                                        if (dummyTable != null && dummyTable.Rows.Count > 0)
                                            r[c] = dummyTable.Rows[0][0].ToString();
                                    }
                                    break;

                            }
                            if (string.IsNullOrEmpty(r[c].ToString()))
                            {
                                foreach (string mandatory in BaanMandatoryFields.PES())
                                {
                                    if (mandatory.ToLower() == c.ColumnName.ToLower())
                                    {
                                        string attributeName = c.ColumnName.Replace("BaanIn_", "");
                                        
                                        errors.AppendLine(GetMessageErrorBaanTable("Mandatory field requested", attributeName));
                                        break;
                                    }
                                }
                            }
                            //}
                        }
                        r["Sys_Error"] = errors.ToString();
                        r.AcceptChanges();
                    }
                }
            }
            catch (Exception e)
            {

            }
            ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
        }

        public static void SetUpPRPdefaultValue(ref eConnection givenConnection, ref DataTable givenTable, bool saveDataFlag = false)
        {
            ArrayList userAttribute;
            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConnection);
            try
            {

                if (givenTable != null && givenTable.Rows.Count > 0)
                {
                    if (saveDataFlag)
                        userAttribute = new ArrayList();
                    if (!givenTable.Columns.Contains("Sys_Error"))
                        givenTable.Columns.Add("Sys_Error", typeof(string));

                    foreach (DataRow r in givenTable.Rows)
                    {
                        System.Text.StringBuilder errors = new System.Text.StringBuilder();
                        foreach (DataColumn c in givenTable.Columns)
                        {
                            var currentCell = r[c].ToString();

                            switch (c.ColumnName.ToLower())
                            {
                                case "edescription":
                                    if (saveDataFlag)
                                        r[c] = GetTextTrucate(r[c].ToString(), 30);
                                    break;
                                case "baanin_tipoarticolo":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "acquistato";
                                    break;
                                case "baanin_metodopianificazione":
                                    r[c] = "1";
                                    break;
                                case "baanin_contolavoro":
                                    r[c] = "2";
                                    break;
                                case "baanin_gruppoarticoli":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "PRT000";
                                    break;

                                case "supplier":
                                    string requested = r["RequestedSupplier"].ToString();
                                    if (requested.ToUpper() == "TO BE SOURCED" && saveDataFlag)
                                        r[c] = "PPPPPP";
                                    else if (requested.ToUpper() == "TO BE SOURCED" && !saveDataFlag)
                                        r[c] = "Fornitore da Definire";
                                    else
                                    {
                                        string supplierCode = GetSupplierCode(ref dbaConnection, currentCell);
                                        supplierCode = GetTextTrucate(supplierCode.ToString(), 6);
                                        if (saveDataFlag)
                                            r[c] = supplierCode;
                                        if (string.IsNullOrEmpty(supplierCode))
                                            errors.AppendLine(GetMessageErrorBaanTable("Not a valid supplier", "Supplier"));

                                    }
                                    break;

                                case "baanin_descarticolo":
                                    if (string.IsNullOrEmpty(currentCell))
                                    {
                                        r[c] = r["MaterialDescription"];
                                        r[c] = GetTextTrucate(r[c].ToString(), 30);
                                    }
                                    break;
                                case "baanin_magazzino":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "ES2";
                                    break;

                                case "baanin_umimmagazzinaggio":
                                    if (string.IsNullOrEmpty(r[c].ToString()))
                                        r[c] = "NR";

                                    break;
                                case "baanin_umstoccaggio":
                                    if (string.IsNullOrEmpty(r[c].ToString()))
                                        r[c] = "NR";
                                    break;
                                case "baanin_dataordine":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = DateTime.Now;
                                    break;
                                case "baanin_articolo":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = r["DrawingCode"];
                                    break;
                                case "baanin_umacquistoprp":
                                    r[c] = "NR";
                                    break;
                                case "totalquantity":
                                case "baanin_sconto":
                                    if (saveDataFlag)
                                        r[c] = GetDoubleValue(r[c].ToString()) * 100;
                                    break;

                                case "pieceprice":
                                case "baanin_importoscontato":
                                    if (saveDataFlag)
                                        r[c] = GetDoubleValue(r[c].ToString()) * 1000000;
                                    break;

                                case "piecepriceformat":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "euro";
                                    break;
                                case "contents":
                                    if (!string.IsNullOrEmpty(currentCell))
                                    {
                                        if (string.IsNullOrEmpty(GetContentBaan(ref dbaConnection, r["eProject"].ToString(), currentCell)))
                                            errors.AppendLine(GetMessageErrorBaanTable("Not a valid content", "Content"));
                                    }
                                    break;
                                case "baanin_requester":
                                    if (saveDataFlag || string.IsNullOrEmpty(currentCell))
                                    {
                                        r[c] = "";
                                        string query = "SELECT TOP(1) eplms.ePerson.eText1 " +
                                                        " FROM            eplms.eUser INNER JOIN " +
                                                        " eplms.eUserPerson ON eplms.eUser.eId = eplms.eUserPerson.eUserId INNER JOIN " +
                                                        " eplms.ePerson ON eplms.eUserPerson.ePersonId = eplms.ePerson.eId where eplms.ePerson.eText1<>'' and  eUser.eName='{0}' ";
                                        query = string.Format(query, givenConnection.currentUserName);
                                        DataTable dummyTable = ePortalHelper.ExecuteQuery(ref givenConnection, query);
                                        if (dummyTable != null && dummyTable.Rows.Count > 0)
                                            r[c] = dummyTable.Rows[0][0].ToString();
                                    }
                                    break;

                                case "baanin_riferimentoa":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = r["eProject"];

                                    string projectBaan = GetProjectBaan(ref dbaConnection, r[c].ToString());
                                    if (!string.IsNullOrEmpty(projectBaan))
                                        r[c] = projectBaan;
                                    else
                                        errors.AppendLine(GetMessageErrorBaanTable("Project not found on transcoding table", "Referement [A]"));

                                    break;
                                case "baanin_riferimentob":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = GetTextTrucate(r["eDescription"].ToString(), 20);
                                    break;
                                case "baanin_riferimentoc":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = r["eCreateUser"];
                                    break;

                                case "baanin_acquisitore":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "9040";
                                    break;
                                case "baanin_pianificatore":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = "8200";
                                    break;
                                case "baanin_commessa":
                                case "baanin_commessaordineprp":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = r["CodeCommessa"];
                                    break;

                                case "baanin_dataconsegnapianificata":
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = r["RequestedDeliveryDate"];
                                    break;
                                case "purchasing":
                                    string mailingTo = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\scripts\mailinglist\prp.eplms");
                                    if (System.IO.File.Exists(mailingTo))
                                        mailingTo = System.IO.File.ReadAllText(mailingTo);
                                    else
                                        mailingTo = "";
                                    if (string.IsNullOrEmpty(currentCell))
                                        r[c] = SetMailingList(ref dbaConnection, ref givenConnection, mailingTo, r["eId"].ToString());
                                    break;

                            }
                            if (string.IsNullOrEmpty(r[c].ToString()))
                            {
                                foreach (string mandatory in BaanMandatoryFields.PRP())
                                {
                                    if (mandatory.ToLower() == c.ColumnName.ToLower() || (mandatory.ToLower()== "baanin_dimensione3" && c.ColumnName.ToLower()=="baanin_atc"))
                                    {
                                        string attributeName = c.ColumnName.Replace("BaanIn_", "");
                                        errors.AppendLine(GetMessageErrorBaanTable("Mandatory field requested", attributeName));
                                        break;
                                    }
                                }
                            }
                            //}
                        }
                        //errors.AppendLine(GetMessageErrorBaanTable("Mandatory field", "test"));
                        //errors.AppendLine(GetMessageErrorBaanTable("requested", "test2"));
                        r["Sys_Error"] = errors.ToString();
                        r.AcceptChanges();
                    }
                }
            }
            catch (Exception e)
            {
            }
            ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
            
        }

        public static string GetMessageErrorBaanTable(string givenMessage, string givenColumn)
        {
            string template = "<span class='baanError' title='{1}'>{0}</span>";
            return string.Format(template, givenColumn, givenMessage);

        }
        

        public static void SyncDataToBaan(ref eConnection givenConnection, eRdABaanAction givenAction)
        {
            
            string myTemplate = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\scripts\baan\sync\{0}_in.eplms");
            string myTemplateRemedy = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\scripts\baan\sync\pes_in_remedy.eplms");

            string myQuerySQL = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\scripts\baan\sync\{0}.eplms");

            myTemplate = string.Format(myTemplate, givenAction.actionType);
            myQuerySQL = string.Format(myQuerySQL, givenAction.actionType);
            if (!System.IO.File.Exists(myTemplate))
                throw new Exception("Error! Baan referement not found on server. Please contact administrator");
            if (!System.IO.File.Exists(myTemplateRemedy))
                throw new Exception("Error! Baan referement for Remedy not found on server. Please contact administrator");
            if (!System.IO.File.Exists(myQuerySQL))
                throw new Exception("Error! Baan query not found on server. Please contact administrator");
            StringBuilder myOut = new StringBuilder();
            myQuerySQL = System.IO.File.ReadAllText(myQuerySQL);

            int[] rdaToProcess = givenAction.businessDataId;
            
            
            eConnection dbaConnection = ePortalHelper.GetDbaConnection(ref givenConnection);

            if (rdaToProcess.Length > 0)
            {

                eBusinessData myDE = new eBusinessData(ref dbaConnection);
                myDE.businessDataType = "DataExchange";
                myDE.Name = "#";
                myDE.Description = string.Format("{2} eRdA2Baan {0} {1}", DateTime.Now.ToString(), givenConnection.currentUserName, givenAction.actionType.ToUpper());
                myDE.Project = "RDA";
                myDE.addObjectToWorkbenchFolder = false;
                ArrayList attributes = new ArrayList();
                if (myDE.Create(ref attributes)!=ePLMS_OK)
                {
                    throw new Exception(myDE.Connection.errorMessage);
                }
                    
                string myFileDEtxt = string.Format("eRDA-{0}_" + myDE.Name + ".txt", givenAction.actionType.ToUpper());
                eBusinessData dummyRdA = new eBusinessData(ref dbaConnection);
                myOut.AppendLine(string.Format(" F|{0}", myFileDEtxt));
                foreach (int eId in rdaToProcess)
                {
                    System.Threading.Thread.Sleep(80);
                    if (dummyRdA.Load(eId) == ePLMS_OK)
                    {
                        string myScript = "update eplms.eBusinessData set eOwner='" + givenConnection.currentUserName + "' where eid=" + dummyRdA.Id;
                        ePortalHelper.ExecuteQuery(ref dbaConnection, myScript);
                        myDE.changeAttribute("BaanIn_ExchangeMessage", "");
                        myDE.changeAttribute("RDACode", "");

                        string description = dummyRdA.Description;
                        if (description.Length > 30)
                        {
                            description = description.Substring(0, 29);
                            description = description + "...";
                        }
                     
                        DataTable myRDA = ePortalHelper.ExecuteQuery(ref dbaConnection, myQuerySQL, new string[] { dummyRdA.Id.ToString() },null);
                        if (myRDA != null && myRDA.Rows.Count == 1)
                        {
                            DataRow originValue = myRDA.Clone().NewRow();
                            originValue.ItemArray = (Object[])myRDA.Rows[0].ItemArray.Clone();
                            string[] myContent = null;
                            if (givenAction.actionType.ToUpper() == "PES")
                            {
                                 SetUpPESdefaultValue(ref givenConnection, ref myRDA, true);
                                myContent = System.IO.File.ReadAllLines(myTemplate);
                                string supplierType = "";
                                eBusinessData.getAttribute(ref dbaConnection, int.Parse(myRDA.Rows[0]["eId"].ToString()), "RequestedSupplier", ref supplierType);
                                if (("" + supplierType).ToUpper() == "REMEDY")
                                    myContent = System.IO.File.ReadAllLines(myTemplateRemedy);
                            }
                            else
                            {
                                SetUpPRPdefaultValue(ref givenConnection, ref myRDA, true);
                                
                                myContent = System.IO.File.ReadAllLines(myTemplate);
                            }
                            CodificaDati(ref dbaConnection, ref myRDA, givenAction.actionType);

                            foreach (DataColumn column in myRDA.Columns)
                            {
                                string label = column.ColumnName.ToUpper();
                                string value = myRDA.Rows[0][column].ToString();
                                try
                                {
                                    if (originValue.Table.Columns.Contains(column.ColumnName))
                                    {
                                        string orginalVal = "" + originValue[column.ColumnName];
                                        string newVal = "" + myRDA.Rows[0][column].ToString();
                                        if (orginalVal.ToUpper()!=newVal.ToUpper() && (orginalVal== "" || column.ToString().ToUpper()== "BaanIn_Requester".ToUpper()))
                                        {
                                            string localValue = value;
                                            if (column.GetType().Equals(typeof(double)))
                                                localValue = eRdAHelper.GetDoubleValue(myRDA.Rows[0][column].ToString()).ToString().Replace(",",".");
                                            else if (column.GetType().Equals(typeof(DateTime)))
                                            {
                                                localValue = ((DateTime)myRDA.Rows[0][column]).ToString("yyyyMMdd000000");
                                                dbaConnection.Core.stringToDate(localValue, ref localValue);
                                                localValue = localValue.Split(" ".ToCharArray())[0];
                                            }
                                           
                                            dummyRdA.changeAttribute(column.ColumnName, localValue);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                }

                                try
                                {


                                    if (column.DataType == typeof(DateTime))
                                        value = ((DateTime)myRDA.Rows[0][column]).ToString("yyyyMMdd");
                                    for (int idx = 0; idx <= myContent.Length - 1; idx++)
                                    {
                                        string[] myItems = myContent[idx].Split("|".ToCharArray());
                                        StringBuilder myStringBulder = new StringBuilder();
                                        myStringBulder.Append(myItems[0]);
                                        if (givenAction.actionType.ToUpper() == "PES")
                                        {
                                            for (int idy = 1; idy <= myItems.Length - 1; idy++)
                                            {
                                                if (myItems[idy].Equals(label))
                                                {
                                                    if (myContent[idx].StartsWith("TH|") & label == "BaanIn_RigaTesto".ToUpper())
                                                    {
                                                        value = myRDA.Rows[0][column].ToString();
                                                        // If value.Length > 72 Then
                                                        if (value.Length > 0)
                                                        {
                                                            string dummyValue = "";
                                                            List<string> mySubString = SplitStringInSubstring(value, 72);
                                                            for (int counter = 0; counter <= mySubString.Count - 1; counter++)
                                                            {
                                                                if (counter == 0)
                                                                    dummyValue += mySubString[counter];
                                                                else
                                                                    dummyValue += string.Format(System.Environment.NewLine + "TH|{0}|{1}", myRDA.Rows[0]["eId"], mySubString[counter]);
                                                            }
                                                            value = dummyValue;
                                                        }
                                                        else
                                                            value = ".";
                                                    }
                                                    else if (myContent[idx].StartsWith("TL|") & label == "BaanIn_RigaTesto".ToUpper())
                                                    {
                                                        value = myRDA.Rows[0][column].ToString();
                                                        // If value.Length > 72 Then
                                                        if (value.Length > 0)
                                                        {
                                                            string dummyValue = "";
                                                            List<string> mySubString = SplitStringInSubstring(value, 72);
                                                            for (int counter = 0; counter <= mySubString.Count - 1; counter++)
                                                            {
                                                                if (counter == 0)
                                                                    dummyValue += mySubString[counter];
                                                                else
                                                                    dummyValue += string.Format(System.Environment.NewLine + "TL|{0}|1|{1}", myRDA.Rows[0]["eId"].ToString(), mySubString[counter]);
                                                            }
                                                            value = dummyValue;
                                                        }
                                                        else
                                                            value = ".";
                                                    }
                                                    else if (label == "Contents".ToUpper())
                                                        value = GetContentBaan(ref dbaConnection, myRDA.Rows[0]["eProject"].ToString(), myRDA.Rows[0][column].ToString());
                                                    else if (label == "BaanIn_Buyer".ToUpper() | label == "Supplier".ToUpper())
                                                        value = value.Trim();
                                                    myStringBulder.Append("|" + value);
                                                }
                                                else
                                                    myStringBulder.Append("|" + myItems[idy]);
                                            }

                                        }
                                        else
                                        {
                                            for (int idy = 1; idy <= myItems.Length - 1; idy++)
                                            {

                                                if (myItems[idy].Equals(label))
                                                {
                                                    if (myContent[idx].StartsWith("TH|") & label == "BaanIn_RigaTesto".ToUpper())
                                                    {
                                                        value = myRDA.Rows[0][column].ToString();
                                                        // If value.Length > 72 Then
                                                        if (value.Length > 0)
                                                        {
                                                            string dummyValue = "";
                                                            List<string> mySubString = SplitStringInSubstring(value, 72);
                                                            for (int counter = 0; counter <= mySubString.Count - 1; counter++)
                                                            {
                                                                if (counter == 0)
                                                                    dummyValue += mySubString[counter];
                                                                else
                                                                    dummyValue += string.Format(System.Environment.NewLine + "TH|{0}|{1}", myRDA.Rows[0]["eId"], mySubString[counter]);
                                                            }
                                                            value = dummyValue;
                                                        }
                                                        else
                                                            value = ".";
                                                    }
                                                    else if (myContent[idx].StartsWith("TL|") & label == "BaanIn_RigaTesto".ToUpper())
                                                    {
                                                        value = myRDA.Rows[0][column].ToString();
                                                        // If value.Length > 72 Then
                                                        if (value.Length > 0)
                                                        {
                                                            string dummyValue = "";
                                                            List<string> mySubString = SplitStringInSubstring(value, 72);
                                                            for (int counter = 0; counter <= mySubString.Count - 1; counter++)
                                                            {
                                                                if (counter == 0)
                                                                    dummyValue += mySubString[counter];
                                                                else
                                                                    dummyValue += string.Format(System.Environment.NewLine + "TL|{0}|1|{1}", myRDA.Rows[0]["eId"].ToString(), mySubString[counter]);
                                                            }
                                                            value = dummyValue;
                                                        }
                                                        else
                                                            value = ".";
                                                    }
                                                    else if (label == "Contents".ToUpper())
                                                        value = GetContentBaan(ref givenConnection, myRDA.Rows[0]["eProject"].ToString(), myRDA.Rows[0][column].ToString());
                                                    else if (label == "BaanIn_RiferimentoA".ToUpper())
                                                    {
                                                        if (myRDA.Rows[0][column].ToString() == "")
                                                            value = GetProjectBaan(ref givenConnection, myRDA.Rows[0]["eProject"].ToString());
                                                        else
                                                            value = GetProjectBaan(ref givenConnection, myRDA.Rows[0][column].ToString());
                                                    }
                                                    else if (label == "BaanIn_Articolo".ToUpper())
                                                        value = myRDA.Rows[0][column].ToString().Replace("/", "");
                                                    else if ((label == "BaanIn_GruppoStatisticoAcquisto".ToUpper() ||
                                                            label == "BaanIn_GruppoStatisticoVendite".ToUpper() ||
                                                            label == "BaanIn_StatoOrdine".ToUpper()) &&
                                                            myRDA.Rows[0][column].ToString().Equals("0"))
                                                        value = "";
                                                    myStringBulder.Append("|" + value);
                                                }
                                                else
                                                    myStringBulder.Append("|" + myItems[idy]);
                                            }
                                        }

                                        myContent[idx] = myStringBulder.ToString();
                                    }
                                }
                                catch (Exception e1)
                                {
                                    if(DBNull.Value.Equals(myRDA.Rows[0][column]))
                                        throw new Exception("Error on attribute '" + column.Caption + "'. Attribute cannot be null");
                                    else
                                        throw new Exception("Error parsing value of attribute '" + column.Caption + "'");
                                    
                                }
                        }
                            for (int idx = 0; idx <= myContent.Length - 1; idx++)
                            {
                                myContent[idx] = myContent[idx].Replace("DATETIME.NOW", DateTime.Now.ToString("yyyyMMdd"));
                                myContent[idx] = myContent[idx].Replace("EPERSON.BAANREF", givenConnection.currentUserName);
                            }

                            foreach (string line in myContent)
                                myOut.AppendLine(line);
                        }
                        eBusinessDataRelation myeBusinessDataRelation2 = new eBusinessDataRelation(ref dbaConnection, ref myDE);
                        ArrayList myUserDefinedAttributeList2 = new ArrayList();
                        myeBusinessDataRelation2.relationType = "Contains";
                        myeBusinessDataRelation2.Quantity = Convert.ToDouble(1);
                        myeBusinessDataRelation2.childBusinessDataBusinessDataType = dummyRdA.businessDataType;
                        myeBusinessDataRelation2.childBusinessDataName = dummyRdA.Name;
                        myeBusinessDataRelation2.childBusinessDataRevision = dummyRdA.Revision;
                        myeBusinessDataRelation2.childBusinessDataKey1 = dummyRdA.Key1;
                        myeBusinessDataRelation2.childBusinessDataKey2 = dummyRdA.Key2;
                        myeBusinessDataRelation2.childBusinessDataKey3 = dummyRdA.Key3;
                        myeBusinessDataRelation2.resolutionType = eBusinessDataDefs.ePLMSResolutionTypePrecise;
                        myeBusinessDataRelation2.promotionRestriction = eBusinessDataDefs.ePLMSNoRestriction;
                        if (myDE.addBusinessDataRelation(ref myeBusinessDataRelation2,ref  myUserDefinedAttributeList2)==ePLMS_OK)
                            dummyRdA.changeAttribute("BaanIn_DataExchange", myDE.Id.ToString());
                        else
                            throw new Exception(dbaConnection.errorMessage);
                    }
                }
                myOut.AppendLine(string.Format(" F|{0}", myFileDEtxt));


                string myFileOutput = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"tmp\synctobaan");
                System.IO.Directory.CreateDirectory(myFileOutput);
                myFileOutput = System.IO.Path.Combine(myFileOutput, myFileDEtxt);
                System.IO.File.WriteAllText(myFileOutput, myOut.ToString());

                if (System.IO.File.Exists(myFileOutput))
                {
                    try
                    {
                        Run_Process(ref givenConnection, myFileOutput, givenAction.actionType.ToUpper());
                    }
                    catch (Exception ex)
                    {
                        myDE.changeAttribute("BaanIn_ExchangeMessage", ex.Message.Replace("<br/>", " ").Replace("<b>", "").Replace("</b>", ""));
                        throw ex;
                    }
                    // SE IL COMANDO E' OK
                    // 1. collego il file al DATAEXCHANGE
                    // 2. cambio l'owner delle rda con l'utente corrente
                    // 3. avanzo il WF allo stato Controlled
                   
                    foreach (int eId in rdaToProcess)
                    {
                        dummyRdA.Load(eId);
                        dummyRdA.changeAttribute("BaanIn_ExchangeMessage", "");
                        if (dummyRdA.Level == "Under Control")
                        {
                            eRdAWorkflowAction givenWorkflowAction = new eRdAWorkflowAction();
                            givenWorkflowAction.businessDataId = dummyRdA.Id;
                            givenWorkflowAction.actionType = eRdAWFActionType.APPROVE;
                            BusinessDataPerformTask(ref givenConnection, givenWorkflowAction);
                        }
                    }
                    try
                    {
                        StoreFile(ref givenConnection, ref myDE, System.IO.File.ReadAllBytes(myFileOutput), "eRdAToBaan-IN.txt", "Attachment", "");
                    }
                    catch (Exception ex)
                    {
                    }
                }

            }

            ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
        }


        public static void FastClose(ref eConnection givenConnection, eRdACloseAction givenAction)
        {
            int rc = 0;
            string message = "";
            int[] rdaToProcess = givenAction.businessDataId;
            eConnection dbaConnection = null;
            givenAction.baancode = givenAction.baancode.Trim();
            if (string.IsNullOrEmpty(givenAction.baancode))
                throw new Exception("Baan Code cannot be null");
            try
            {
                dbaConnection = ePortalHelper.GetDbaConnection(ref givenConnection);
                if (rdaToProcess.Length > 0)
                {
                    eBusinessData dummyRdA = new eBusinessData(ref givenConnection);
                    foreach (int eId in rdaToProcess)
                    {
                        System.Threading.Thread.Sleep(80);
                        if (dummyRdA.Load(eId) == ePLMS_OK)
                        {
                            //aggiorno l'ower
                            string myScript1 = "update eplms.eBusinessData set eOwner='" + givenConnection.currentUserName + "' where eid=" + dummyRdA.Id;
                            string myScript2 = "update eplms.RDADB set eOwner='" + givenConnection.currentUserName + "' where eid=" + dummyRdA.Id;
                            ePortalHelper.ExecuteQuery(ref dbaConnection, myScript1);
                            ePortalHelper.ExecuteQuery(ref dbaConnection, myScript2);

                            //setto il codice Baan
                            dummyRdA.Reserve();
                            rc = dummyRdA.changeAttribute("RDACode", givenAction.baancode);
                            if (rc != ePLMS_OK)
                            {
                                message = dummyRdA.Connection.errorMessage;
                                dummyRdA.Unreserve();
                                throw new Exception(message);
                            }
                            string dateWrite = DateTime.Now.ToString("yyyyMMddHHmmss");
                            dbaConnection.Core.stringToDate(dateWrite, ref dateWrite);
                            dateWrite = dateWrite.Split(" ".ToCharArray())[0];
                            rc = dummyRdA.changeAttribute("BaanCodeWriteDate", dateWrite);
                            if (rc != ePLMS_OK)
                            {
                                message = dummyRdA.Connection.errorMessage;
                                dummyRdA.Unreserve();
                                throw new Exception(message);
                            }
                            dummyRdA.Unreserve();
                            //se codice Baan è settato procedo con la chiusura a WF della erda
                            eWorkflowEngine workflowEngine = new eWorkflowEngine(ref dbaConnection, dummyRdA.Id);
                            if (!workflowEngine.isWorkflowEnded)
                            {
                                int RetCode = workflowEngine.reassignLevel("Closed", "Fast Closure by " + givenConnection.currentUserName, true);

                                workflowEngine = new eWorkflowEngine(ref dbaConnection, dummyRdA.Id);

                                workflowTask currentWorkflowTask = workflowEngine.getCurrentWorkflowTask();
                                eBusinessData dummy = dummyRdA;
                                rc = ePortalHelper.WorkflowPromoteRaiseEvent(ref dbaConnection, ref dummyRdA, dummy, ref currentWorkflowTask, "executeWorkflowTaskPrefix");
                                givenConnection.pushMessageFlag = true;
                                if (rc != ePLMS_OK)
                                    throw new Exception(givenConnection.errorMessage);

                                rc = currentWorkflowTask.performTask();
                                givenConnection.pushMessageFlag = false;
                                if (rc != ePLMS_OK)
                                    throw new Exception(givenConnection.errorMessage);

                                rc = ePortalHelper.WorkflowPromoteRaiseEvent(ref dbaConnection, ref dummyRdA, dummy, ref currentWorkflowTask, "executeWorkflowTaskSuffix");
                                if (rc != ePLMS_OK)
                                    throw new Exception(givenConnection.errorMessage);

                                //se arrivo qui è perchè la erda si è chiusa quindi avvio la mail al purchasing
                                NotifyPurchasing(ref dbaConnection, dummyRdA.Id);

                            }
                        }
                    }
                }
                ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
            }
            catch (Exception e)
            {
                givenConnection.Logger.writeToLog("Error sending puchasing mail:" + e.ToString(), "FAST CLOSURE");
                ePortalHelper.DestroyDbaConnection(ref givenConnection, ref dbaConnection);
                throw e;
            }
            
        }

        private static void NotifyPurchasing(ref eConnection dbaConnection, int myRdAId)
        {
            string myBAAN = "";
            int rc;
            string errorMessage = "";
            string mailingList = "";
            string myPath = System.Configuration.ConfigurationManager.AppSettings["ePLMSTmpPath"];

            if (string.IsNullOrEmpty(myPath))
                myPath = System.IO.Path.GetTempPath();
            eBusinessData myRdA = new eBusinessData(ref dbaConnection);
            myRdA.Load(myRdAId);
            string deploy = myPath;
            myRdA.getAttribute("RDACode", ref myBAAN);
            myRdA.getAttribute("Purchasing", ref mailingList);
            if (myBAAN == "")
                return;
            if (mailingList.Replace(";", "").Trim() == "")
                return;
            
            string myBody = "";
            string mysubject = "";
            eBusinessDataFile[] myFiles= null;

            FillData(ref dbaConnection,ref myRdA,ref myBody,ref mysubject, ref myFiles);
            // Dim myMail As New eMail(givenRDA.Connection)
            string[] toList = mailingList.Trim().Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string Subject = mysubject + " [" + myRdA.Name + "]";
            string Body = myBody;
            List<string> myListaFiles = new List<string>();

            if (myFiles != null && myFiles.Length > 0)
            {
                try
                {
                    deploy = System.IO.Path.Combine(myPath, myRdA.Connection.currentUserId.ToString());
                    System.IO.Directory.CreateDirectory(deploy);

                    foreach (eBusinessDataFile file in myFiles)
                    {
                        string retFile="";
                        file.copyOut(ref retFile);
                        if (retFile != "")
                        {
                            System.Net.WebClient myWeb = new System.Net.WebClient();
                            myWeb.DownloadFile(retFile, System.IO.Path.Combine(deploy, file.Name));
                            if (System.IO.File.Exists(System.IO.Path.Combine(deploy, file.Name)))
                                myListaFiles.Add(System.IO.Path.Combine(deploy, file.Name));
                        }
                    }
                }

                catch (Exception ex)
                {
                    dbaConnection.Logger.writeToLog("Error preparing mail to purchasing on RDA:" + myRdA.Name + "->" + ex.Message, "FAST CLOSURE");
                    throw new Exception("Error preparing mail to purchasing on RDA:" + myRdA.Name + "->" + ex.Message);
                }
            }

            myFiles = null;

            //SmtpClient SmtpServer = new SmtpClient();
            //SmtpServer.Host = dbaConnection.ePLMS_SMTP_SERVER;
            //SmtpServer.Port = dbaConnection.ePLMS_SMTP_PORT;


            eMail mail = new eMail(ref dbaConnection);
            mail.Subject = "";
            

            mail.From = myRdA.Connection.ePLMS_SMTP_MAIL_FROM;
            mail.ToList = toList;
            mail.Subject = Subject;
            mail.Body = Body;
            if (myListaFiles != null && myListaFiles.Count > 0)
                mail.attachmentList = myListaFiles.ToArray();

            try
            {
                rc =mail.Send();
            }
            catch (Exception ex)
            {
                dbaConnection.Logger.writeToLog("Error sending mail Purchasing on RDA:" + myRdA.Name + "->" + ex.Message, "FAST CLOSURE");
                throw new Exception("Error sending mail Purchasing on RDA " + myRdA.Name + ":" + ex.Message);
            }
            if (rc != ePLMS_OK)
            {
                throw new Exception("eRdA closed succesfully, but an error occurred sending mail to the purchasing:" + dbaConnection.errorMessage);
             }

            try
            {
                if (deploy != myPath)
                {
                    if (deploy.ToLower().EndsWith(myRdA.Connection.currentUserId.ToString()))
                        System.IO.Directory.Delete(deploy, true);
                }
            }
            catch (Exception ex)
            {
            }


            if (rc == ePLMS_OK)
                myRdA.changeAttribute("PurchasingeMailSent", "TRUE");
            else
                myRdA.changeAttribute("PurchasingeMailSent", "FALSE");
        }
        private static void FillData(ref eConnection givenConnection, ref eBusinessData givenRdA, ref string myMailBody, ref string returnSubject, ref eBusinessDataFile[] returnFiles)
        {
            string BAANCode = "";
            string supplier = "";
            string BaanCodeWriter = "";
            string Purchasing = "";
            string PurchasingeMailSent = "";
            string myMail = "";

            StringBuilder myAttributeList = new StringBuilder();
            ArrayList rdaAttributeList = new ArrayList();
            eBusinessDataType bdt = new eBusinessDataType(ref givenConnection);
            eAttribute myeAttribute = new eAttribute(ref givenConnection);
            string createUser = "";
            string myTemplate = "<tr><td style='width:150px;bgcolor:#eeeeee'><b>{0}</b></td><td>{1}<td></tr>";
            string remedy = " ";
            bdt.Load(givenRdA.businessDataType);
            bdt.getAttributeList(ref rdaAttributeList);
            
            myMail = System.IO.Path.Combine(givenRdA.Connection.ePLMS_HOME, "env\\scripts\\BaaN\\Purchasing.html");
            myMailBody = System.IO.File.ReadAllText(myMail);


            myAttributeList.Append("<table width='95%'><tr><th align='left' color='black' colspan='2' bgcolor='#000055'><font size='2pt'><b>Dettaglio RDA</b></font></th></tr>");

            if (rdaAttributeList != null)
            {
                createUser = givenRdA.createUser;

                createUser = createUser.Replace(".", " ");
                createUser = (System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(createUser));
                if (myMailBody.Contains("[eCreateUser]"))
                    myMailBody = myMailBody.Replace("[eCreateUser]", "<b>" + createUser + "</b>");
                if (myMailBody.Contains("[eCreateDate]"))
                    myMailBody = myMailBody.Replace("[eCreateDate]", "<b>" + givenRdA.createDate.Split(" ".ToCharArray())[0] + "</b>");
                if (myMailBody.Contains("[eDescription]"))
                    myMailBody = myMailBody.Replace("[eDescription]", "<b>" + givenRdA.Description + "</b>");
                foreach (eDataListElement edata in rdaAttributeList)
                {
                    string value = "";
                    givenRdA.getAttribute(edata.Name, ref value);
                    if (edata.Name == "Purchasing")
                        Purchasing = value;
                    else if (edata.Name == "PurchasingeMailSent")
                        PurchasingeMailSent = value;
                    else if (edata.Name == "RDACode")
                        BAANCode = value;
                    else if (edata.Name == "BaanCodeWriter")
                    {
                        BaanCodeWriter = value;
                        value = edata.Value.Replace(".", " ");
                        value = (System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(edata.Value));
                        BaanCodeWriter = value;
                    }
                    else if (edata.Name == "Supplier")
                        supplier = value;
                    else if (edata.Name == "RequestedSupplier")
                    {
                        remedy = value;
                        if (remedy.ToUpper() != "REMEDY")
                            remedy = " ";
                        else
                            remedy = " REMEDY ";
                    }
                    if (edata.Type == "DATE" && !string.IsNullOrEmpty(value))
                        value = value.Split(" ".ToCharArray())[0];
                    if (myMailBody.Contains("[" + edata.Name + "]"))
                    {
                        if (edata.Name != "BaanCodeWriter")
                            myMailBody = myMailBody.Replace("[" + edata.Name + "]", edata.Value);
                    }
                    if (edata.Value != "")
                    {
                        myeAttribute.Load(edata.Key);
                        string mMessageLabel="";
                        if (givenRdA.Connection.labelCatalog.getText(myeAttribute.labelNumber,ref mMessageLabel) != ePLMS_OK)
                            mMessageLabel = edata.Name;
                        if (edata.Name.ToUpper().StartsWith("Purchasing".ToUpper()))
                            continue;
                        myAttributeList.Append(string.Format(myTemplate, mMessageLabel, edata.Value));
                    }
                }
            }

            myAttributeList.Append("</table>");

            List<eBusinessDataFile> myListFiles = new List<eBusinessDataFile>();
            givenRdA.getBusinessDataFileList(true, ref returnFiles);
            if (returnFiles != null)
            {
                foreach (eBusinessDataFile file in returnFiles)
                {
                    if (file.Description.ToUpper() != "Configurazione RDA".ToUpper())
                        myListFiles.Add(file);
                }
            }
            if (myListFiles.Count > 0)
                returnFiles = myListFiles.ToArray();
            else
                returnFiles = null;

            if (myMailBody.Contains("[BaanCodeWriter]") & BaanCodeWriter == "" & BAANCode != "")
            {
                myMailBody = myMailBody.Replace("[BaanCodeWriter]", createUser);
                BaanCodeWriter = createUser;
            }
            else if (myMailBody.Contains("[BaanCodeWriter]") & BaanCodeWriter != "" & BAANCode != "")
                myMailBody = myMailBody.Replace("[BaanCodeWriter]", BaanCodeWriter);
            else
                myMailBody = myMailBody.Replace("[BaanCodeWriter]", "-------------");

            myMailBody = myMailBody.Replace("[EBUSINESSDATAATTRIBUTELIST]", myAttributeList.ToString());
            returnSubject = "Emissione RDA" + remedy + "N. " + (BAANCode != "" ? BAANCode : "****");

            if (BaanCodeWriter.ToUpper() == createUser.ToUpper())
                myMailBody = myMailBody.Replace("per conto di <b><b>" + createUser + "</b></b>", "");

            
        }

        public static void CodificaDati(ref eConnection givenConnection , ref DataTable givenTableToValidate, string givensynctype)
        {
            string myFileMapping = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\scripts\BaaN\sync\Baan" + givensynctype.ToUpper() + "Mapping.eplms");
            Dictionary<string, string> myDict = new Dictionary<string, string>();
            if (System.IO.File.Exists(myFileMapping))
            {
                foreach (string line in System.IO.File.ReadAllLines(myFileMapping))
                {
                    string key = line.Split("=".ToCharArray())[0].Trim().ToUpper();
                    if (!myDict.ContainsKey(key))
                        myDict.Add(key, line.Split("=".ToCharArray())[1]);
                }
                if (givenTableToValidate != null && givenTableToValidate.Rows.Count > 0)
                {
                    foreach (DataColumn column in givenTableToValidate.Columns)
                    {
                        if (myDict.ContainsKey(column.ColumnName.ToUpper()))
                        {
                            string[] mappingValue = myDict[column.ColumnName.ToUpper()].Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                            foreach (DataRow row in givenTableToValidate.Rows)
                            {
                                string currentValue = row[column].ToString();
                                foreach (string mpValue in mappingValue)
                                {
                                    if (mpValue.Split(":".ToCharArray())[0].ToLower() == currentValue.ToLower())
                                    {
                                        row[column] = mpValue.Split(":".ToCharArray())[1];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

     
        public static List<string> SplitStringInSubstring(string givenText, int givenLimit)
        {
            List<string> list = new List<string>();
            string s = givenText;
            int num = 0;
            int crIdx = 0;
            while (s.Length > 0)
            {
                num = Math.Min(givenLimit, s.Length);
                //crIdx = Math.Min(num, String.IndexOf(s.Substring(0, num), System.Environment.NewLine)) - 1;
                crIdx = Math.Min(num, s.Substring(0, num).IndexOf(System.Environment.NewLine));
                if (crIdx > -1)
                    num = crIdx;
                list.Add(s.Substring(0, num));
                if (crIdx > -1)
                    num = crIdx + 2;
                s = s.Remove(0, num);
            }

            return list;
        }


        public static void Run_Process(ref eConnection givenConnection, string givenFile, string cmdType)
        {

            string baseRXC = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\scripts\BaaN\RXC");
            string Process_Name = System.IO.Path.Combine(baseRXC, "rxc.exe");// ConfigurationManager.AppSettings(cmdType + "_BAAN_EXE");
            string Process_Arguments = System.IO.File.ReadAllText(System.IO.Path.Combine(baseRXC, cmdType + "_BAAN_ARGS.eplms"));// ConfigurationManager.AppSettings(cmdType + "_BAAN_ARGS");
            bool Read_Output = false;
            bool Process_Hide = true;
            int Process_TimeOut = 999999999;
            if (!System.IO.File.Exists(Process_Name))
            {
                string message = string.Format("ExitCode: {0},<br/>Message: {1}", 999, "Command not found");
                throw new Exception(message);
            }
            Process_Arguments = string.Format("{0} < {1}", Process_Arguments, System.IO.Path.GetFileName(givenFile), "\"");
            string myBatContent = string.Format("{0} {1}", Process_Name, Process_Arguments);
            if (Process_Name.Contains(" "))
                myBatContent= string.Format("\"{0}\" {1}", Process_Name, Process_Arguments);
                
            string myFileBat = givenFile.Replace(System.IO.Path.GetExtension(givenFile), ".bat");
            System.IO.File.WriteAllText(myFileBat, myBatContent);

            int ERRORLEVEL;
            try
            {
                Process My_Process = new Process();
                ProcessStartInfo My_Process_Info = new ProcessStartInfo();

                My_Process_Info.FileName = myFileBat; // Process_Name ' Process filename
                My_Process_Info.Arguments = null; // Process_Arguments ' Process arguments
                My_Process_Info.CreateNoWindow = Process_Hide; // Show or hide the process Window
                My_Process_Info.UseShellExecute = false; // Don't use system shell to execute the process
                My_Process_Info.RedirectStandardOutput = Read_Output; // Redirect (1) Output
                My_Process_Info.RedirectStandardError = Read_Output; // Redirect non (1) Output
                My_Process.EnableRaisingEvents = true; // Raise events
                My_Process_Info.WorkingDirectory = System.IO.Path.GetDirectoryName(givenFile);
                My_Process.StartInfo = My_Process_Info;
                My_Process.Start(); // Run the process NOW

                My_Process.WaitForExit(Process_TimeOut); // Wait X ms to kill the process (Default value is 999999999 ms which is 277 Hours)

                ERRORLEVEL = My_Process.ExitCode; // Stores the ExitCode of the process
                if (ERRORLEVEL != 0)
                {
                    string message = string.Format("ExitCode: {0},<br/>Message: {1}", ERRORLEVEL, GetBaanMessageError(ref givenConnection,ERRORLEVEL));
                    throw new Exception(message);
                } // Return ERRORLEVEL ' Returns the Exitcode if is not 0

                if (Read_Output == true)
                {
                    string Process_ErrorOutput = My_Process.StandardOutput.ReadToEnd(); // Stores the Error Output (If any)
                    string Process_StandardOutput = My_Process.StandardOutput.ReadToEnd(); // Stores the Standard Output (If any)
                                                                                           // Return output by priority
                    if (Process_ErrorOutput != null)
                        throw new Exception(Process_ErrorOutput);// Return Process_ErrorOutput ' Returns the ErrorOutput (if any)
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw new Exception("<b>Error sending to Baan.</b><br/>" + message);
            }
        }

        public static int StoreFile(ref eConnection givenConnection, ref eBusinessData BusinessData, byte[] givenStream, string givenFileName, string givenFileType, string givenContentType, string givenDescription = null)
        {
            if (BusinessData == null)
                return ePLMS_ERROR;
            int rc = ePLMS_OK;

            eVault returnVault = new eVault(ref givenConnection);
            BusinessData.getCandidateVault(ref returnVault);
            eFileService.eFileService myFileService = new eFileService.eFileService();
            string returnTempFileName = null;
            if (returnVault != null)
            {
                myFileService.Url = returnVault.fileServiceURL + "/efileservice.asmx";
                myFileService.UploadSingleFile(givenStream, ref returnTempFileName);

                eBusinessDataFile myFile = new eBusinessDataFile(ref givenConnection, BusinessData);
                myFile.Name = givenFileName;
                myFile.businessDataFileType = givenFileType;
                myFile.Source = returnTempFileName;
                myFile.Type = ePLMSFileTypeSource;


                //string myContentType = ePortalManager.getUserConnection.contentTypeFile.SelectSingleNode("/root/content-type[@Extension='" + Path.GetExtension(givenFileName).Replace(".", "") + "']").Attributes("MIME").Value;
                //myFile.contentType = myContentType;


                myFile.Description = "" + givenDescription;
                ArrayList arraylist = new ArrayList();
                BusinessData.addBusinessDataFile(ref myFile,ref arraylist); 
            }

            return ePLMS_OK;
        }

        public static string GetBaanMessageError(ref eConnection givenConnection, int givenCode)
        {
            string myFiles = System.IO.Path.Combine(givenConnection.ePLMS_HOME, @"env\scripts\BaaN\BaanExitCode.eplms");
            string[] myLines = System.IO.File.ReadAllLines(myFiles);
            foreach (string line in myLines)
            {
                int code = int.Parse(line.Split(":".ToArray())[0]);
                string message = line.Split(":".ToArray())[1];
                if (code == givenCode)
                    return message;
            }
            return "";
        }


        public static string SetMailingList(ref eConnection dbaConnection, ref eConnection  givenUserConnection, string mailingTo, string rdaId)
        {
            string[] emailAddr = mailingTo.Split(";".ToArray());

            foreach (string item in emailAddr)
            {
                if (item.Contains("@"))
                {
                    if (!mailingTo.Contains(item))
                        mailingTo += item + ";";
                }
                else
                    switch (item)
                    {
                        case "op_corrente":
                            {
                                string currentUser = GetRdaCurrentUserMail(ref dbaConnection, givenUserConnection.currentUserId);
                                if (currentUser != null && !mailingTo.Contains(currentUser))
                                    mailingTo = mailingTo.Replace(item, currentUser);
                                break;
                            }

                        case "requester":
                            {
                                string requester = GetRdaRequesterMail(ref dbaConnection, rdaId);
                                if (requester != null && !mailingTo.Contains(requester))
                                    mailingTo = mailingTo.Replace(item, requester);
                                break;
                            }

                        case "cms_corrente":
                            {
                                string cms = GetRdaCMSMail(ref dbaConnection, rdaId);
                                if (cms != null && !mailingTo.Contains(cms))
                                    mailingTo = mailingTo.Replace(item, cms);
                                break;
                            }
                    }
            }

            string myreturnMail = "";
            if (mailingTo != "")
            {
                foreach (string item in mailingTo.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (item.Contains("@"))
                        myreturnMail += item + ";";
                }
            }
            try
            {
                eBusinessData myRDA = new eBusinessData(ref dbaConnection);
                myRDA.Load(int.Parse(rdaId));
                myRDA.changeAttribute("Purchasing", myreturnMail);
            }
            catch (Exception ex)
            {
            }
            return myreturnMail;
        }

        public static string GetRdaCurrentUserMail(ref eConnection dbaConnection, int currentUserId)
        {
            string retEmail = "";
            string myQuery = "SELECT eplms.ePerson.eId, eplms.ePerson.eEMail " +
"FROM   eplms.ePerson " +
"INNER JOIN eplms.eUserPerson ON eplms.ePerson.eId = eplms.eUserPerson.ePersonId " +
"WHERE eplms.eUserPerson.eUserId = '" + currentUserId.ToString() + "' ";
            DataTable myTable = ePortalHelper.ExecuteQuery(ref dbaConnection,myQuery);
            if (myTable != null && myTable.Rows.Count > 0)
            {
                ePerson userPerson = new ePerson(ref dbaConnection);
                if (userPerson.Load(int.Parse(myTable.Rows[0][0].ToString())) == ePLMS_OK)
                    retEmail = userPerson.eMail;// myTable.Rows[0][0].ToString();
            }
                

            return retEmail;
        }

        public static string GetRdaCMSMail(ref eConnection dbaConnection, string rdaId)
        {
            string retEmail = "";
            string myQuery = "SELECT eplms.ePerson.eId,eplms.ePerson.eEMail FROM   eplms.ePerson INNER JOIN eplms.eUserPerson ON eplms.ePerson.eId = eplms.eUserPerson.ePersonId INNER JOIN  eplms.eBusinessDataCheckHistory INNER JOIN  eplms.eUser ON eplms.eBusinessDataCheckHistory.eUser = eplms.eUser.eName ON eplms.eUserPerson.eUserId = eplms.eUser.eId  WHERE  (eplms.eBusinessDataCheckHistory.eCheck = N'Cost Mngmt Spec. Check')  AND    (eplms.eBusinessDataCheckHistory.eBusinessDataId = '" + rdaId + "')";
            DataTable myTable = ePortalHelper.ExecuteQuery(ref dbaConnection, myQuery);
            if (myTable != null && myTable.Rows.Count > 0)
            {
                ePerson userPerson = new ePerson(ref dbaConnection);
                if (userPerson.Load(int.Parse(myTable.Rows[0][0].ToString())) == ePLMS_OK)
                    retEmail = userPerson.eMail;// myTable.Rows[0][0].ToString();
            }

            return retEmail;
        }

        public static string GetRdaRequesterMail(ref eConnection dbaConnection, string rdaId)
        {
            string retEmail = "";
            string myQuery = "SELECT eplms.ePerson.eId,eplms.ePerson.eEMail FROM   eplms.ePerson INNER JOIN        eplms.eUserPerson ON eplms.ePerson.eId = eplms.eUserPerson.ePersonId INNER JOIN        eplms.eUser ON eplms.eUserPerson.eUserId = eplms.eUser.eId INNER JOIN        eplms.eBusinessData ON eplms.eUser.eName = eplms.eBusinessData.eCreateUser WHERE  (eplms.eBusinessData.eId = '" + rdaId + "')";
            DataTable myTable = ePortalHelper.ExecuteQuery(ref dbaConnection, myQuery);
            if (myTable != null && myTable.Rows.Count > 0)
            {
                ePerson userPerson = new ePerson(ref dbaConnection);
                if (userPerson.Load(int.Parse(myTable.Rows[0][0].ToString())) == ePLMS_OK)
                    retEmail = userPerson.eMail;// myTable.Rows[0][0].ToString();
            }

            return retEmail;
        }
        public static bool HasValidLength(ref DataTable attributeTable, string attributeName,string attributeValue)
        {
            bool retvalue = true;
            if (attributeTable != null && attributeTable.Rows.Count > 0)
            {
                DataRow[] rows = attributeTable.Select("eName='" + attributeName + "'");
                if (rows.Length > 0)
                {
                    var len = int.Parse(rows[0]["eLength"].ToString());
                    if (attributeValue.Length > len)
                        retvalue = false;
                }
            }
            return retvalue;
        }

        public static string GetQueryContent(ref eConnection givenConnection ,string queryName, string[] extraParams, string[] conditions = null)
        {
            string retContent = "";
            string myScript = System.IO.Path.Combine(givenConnection.ePLMS_HOME, "env\\scripts\\");
            string scriptFullName = System.IO.Path.Combine(myScript, queryName);
            if (System.IO.File.Exists(scriptFullName))
            {
                retContent = System.IO.File.ReadAllText(scriptFullName);
                if (extraParams != null)
                {
                    for (int idx = 0; idx <= extraParams.Length - 1; idx++)
                        retContent = retContent.Replace("@Param" + (idx + 1), extraParams[idx]);
                }

                if (conditions != null)
                {
                    for (int idx = 0; idx <= conditions.Length - 1; idx++)
                        retContent = retContent.Replace("@Cond" + (idx + 1), conditions[idx]);
                }
            }
            return retContent;
        }

      
        #endregion
    }
}




