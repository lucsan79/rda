﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Http.Routing;

namespace CD4.Implementation.WebAPI.Helpers
{
    public class CD4ControllerSelector: DefaultHttpControllerSelector
    {
        private HttpConfiguration _configuration;
        public CD4ControllerSelector(HttpConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;
        }
        
        public override HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            if (request.GetRouteData().Values.ContainsKey("MS_SubRoutes"))
            {
                var routeData = request.GetRouteData();
                foreach (var route in (IHttpRouteData[])routeData.Values["MS_SubRoutes"])
                {
                    var httpActionDescriptor = (HttpActionDescriptor[]) route.Route.DataTokens["actions"];
                    var controllerDescriptor = httpActionDescriptor[0].ControllerDescriptor;

                    if (controllerDescriptor.ControllerName.EndsWith("Override"))
                    {
                        return controllerDescriptor;
                    }
                }
            }

            return base.SelectController(request);
        }
    }
}