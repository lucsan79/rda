﻿using CD4.ViewModels.AdminCenter;
using Parallaksis.ePLMSM40;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace CD4.Implementation.WebAPI.ViewModels
{

    public class BaanMandatoryFields
    {
        public static string[] PES()
        {
            return "BaanIn_Articolo;BaanIn_Conto;BaanIn_CDC;BaanIn_Commessa;BaanIn_ATC;Purchasing;BaanIn_Requester".Split(";".ToCharArray());
        }
        public static string[] PRP()
        {
            return "BaanIn_Articolo;BaanIn_DescArticolo;BaanIn_Dimensione3;Purchasing;BaanIn_Requester".Split(";".ToCharArray());
        }

    }
    public class eRdADefs
    {
        public const string FilterType_Delay = "delay";
        public const string FilterType_Delay_today = "today";
        public const string FilterType_Delay_current_week = "currentweek";
        public const string FilterType_Delay_next_week = "nextweek";
        public const string FilterType_Delay_all = "all";
        public const string FilterType_Review_all = "all";
        public const string FilterType_Review_evaluation = "evaluation";
        public const string FilterType_Review_rejected = "rejected";

        public const string FilterType_Replan = "replan";
        public const string FilterType_Review = "review";
        public const string UserFilter_BusinessDataType = "UserFilter";
        public const string UserFilter_DASHNOARD = "DASHBOARDFILTER";
        public const string UserFilter_REPORT = "REPORTFILTER";
        public const string UserFilter_FULLVIEW = "FULLVIEWFILTER";

        public const string Color_Green = "green";
        public const string Color_Red = "Red";
        public const string Color_Orange = "orange";
        public const string Color_Yellow = "#b3b300";
        public const string Color_Purple = "purple";
        public const string WorkflowMasterSpending = "WF_RDA_03";
        public const string WorkflowMultiCreateMaterial = "WF_RDA_02";

    }

    public enum eRdAViewType
    {
        TOSIGN,
        FASTMATERIAL,
        SIGNED,
        MULTIMATERIAL
    }

    public enum eRdAWFActionType
    {
        APPROVE,
        REJECT
    }
    public class extraParam
    {
        public string name = "";
        public string value = "";
    }
    public class eRdAWorkflowAction
    {
        public eRdAWorkflowAction() {
            backCMS = false;
        }
        public int businessDataId { get; set; }
        public bool backCMS { get; set; }
        public eRdAWFActionType actionType { get; set; }
        public string reason { get; set; }
        public List<extraParam> additionValue { get; set; }

    }
    public class eRdAATCAction
    {
        public eRdAATCAction()
        {
            result = true;
            message = "";
        }
        public int[] businessDataId { get; set; }
        public string atc { get; set; }
        public bool result { get; set; }
        public string message { get; set; }
    }

    public class eRdACloseAction
    {
        public eRdACloseAction()
        {
            result = true;
            message = "";
        }
        public int[] businessDataId { get; set; }
        public string baancode { get; set; }
        public string actionType { get; set; }
        public bool result { get; set; }
        public string message { get; set; }
        //public string reason { get; set; }
        //public List<extraParam> additionValue { get; set; }

    }

    public class eRdABaanAction
    {
        public eRdABaanAction()
        {
            result = true;
            message = "";
        }
        public int[] businessDataId { get; set; }
        public string actionType { get; set; }
        public bool result { get; set; }
        public string message { get; set; }
        //public string reason { get; set; }
        //public List<extraParam> additionValue { get; set; }

    }
    public class eRdAWorkflowResult
    {
        public int businessDataId { get; set; }
        public bool result { get; set; }
        public string message { get; set; }
        public string fromLevel { get; set; }
        public string toLevel { get; set; }
    }
    public class RemedyPriceListData
    {
        public string filename { get; set; }
        public string filecontent { get; set; }
    }

    public class eRdAMaterialsFile
    {
        public eRdAMaterialsFile()
        {
            IsValidFile = true;
            rows = new List<eRdAMaterialsFileRow>();
            result = true;
            message = "";
        }

        public int businessDataId { get; set; }
        public string level { get; set; }
        public string filename { get; set; }
        public string filecontent { get; set; }
        public bool IsValidFile { get; set; }
        public bool result { get; set; }
        public string message { get; set; }
        public List<eRdAMaterialsFileRow> rows { get; set; }
    }
    public class eRdAMaterialsFileRow
    {
        public eRdAMaterialsFileRow()
        {
            Sys_Error = "<span class='importmatok'  title=''><i class='fas fa-check'></i> valid row</span>";
        }
        public int eId { get; set; }
        public string eName { get; set; }
        public int NumRow { get; set; }
        public string Sys_Error { get; set; }
        public string DRAWINGCODE { get; set; }
        public string MATERIALDESCRIPTION { get; set; }
        public string CO_SP { get; set; }
        public double QUANTITYONVEHICLE { get; set; }
        public double PIECEPRICE { get; set; }
        public string PIECEPRICEFORMAT { get; set; }
        public double TOTALQUANTITY { get; set; }
        public string REQUESTEDSUPPLIER { get; set; }
        public string SUPPLIER { get; set; }
        public string CODICEIMAN { get; set; }
        public string SUPPLIERREFERENCE { get; set; }
        public DateTime REQUESTEDDELIVERYDATE { get; set; }


    }

    public enum eDialogOperationType
    {
        STARTUP,
        GONEXT,
        SAVE
    }
    public enum UserProfileValue
    {
        Requester,
        Checker,
        Controller,
        PES,
        PRP,
        Generic,
        UR,
        UR_,
        CMS,
        PM,
        PMP,
        PPM,
        PR,
        PRCDC_,
        PL,
        PL_,
    }
    public class eDialogItemModel
    {
        public string id { get; set; }
        public string parentId { get; set; }
        public string inputValue { get; set; }
        public string selectedOption { get; set; }
        public string questionType { get; set; }

        public eDialogOperationType operationType { get; set; }

    }
    public class eDialogListItem
    {
        public string eId { get; set; }
        public string eName { get; set; }
        public string brand { get; set; }
        public string context { get; set; }
        public string suppliercode { get; set; }
        public string project { get; set; }
        public string area { get; set; }
        public string system { get; set; }
        public string cbs { get; set; }
        public string cdc { get; set; }
        public string wbs { get; set; }
        public string param { get; set; }
        public string reqtype { get; set; }
        public string basicList { get; set; }
        public string lvl1 { get; set; }
        public string lvl2 { get; set; }
        public string searchItem { get; set; }
        public string competence { get; set; }
        public string professionalrole { get; set; }
        public string site { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string days { get; set; }


    }

    public class eValidationData
    {
        public string CheckType { get; set; }
        public string[] PropertyName { get; set; }
        public string[] PropertyValue { get; set; }

    }

    public class eProjectRdaListView
    {
        public eProjectRdaListView() {

            RdAList = new List<eRdABasicView>();
            RdAInWorkingList = new List<eRdABasicView>();
            RdAInValidationList = new List<eRdABasicView>();
            RdAInCMSValidationList = new List<eRdABasicView>();
            RdAInCreatedList = new List<eRdABasicView>();
            totalRdA = 0;
            delayPR = 0;
            delayCMS = 0;
            delayContr = 0;
            delayReq = 0;
            RdAInWorking = "0";
            RdAInValidation = "0";
            RdAInCMSValidation = "0";
            RdAInCreated = "0";

            RdAInWorkingStatus = "far fa-circle";
            RdAInValidationStatus = "far fa-circle";
            RdAInCMSValidationStatus = "far fa-circle";
            RdAInCreatedStatus = "far fa-circle";


            
        }
        public List<eRdABasicView> RdAList { get; set; }
        public string ProjectName { get; set; }
        public string RdAInWorking { get; set; }
        public List<eRdABasicView> RdAInWorkingList { get; set; }
        public string RdAInWorkingStatus { get; set; }

        public string RdAInValidation { get; set; }
        public List<eRdABasicView> RdAInValidationList { get; set; }
        public string RdAInValidationStatus { get; set; }

        public string RdAInCMSValidation { get; set; }
        public List<eRdABasicView> RdAInCMSValidationList { get; set; }
        public string RdAInCMSValidationStatus { get; set; }

        public string RdAInCreated { get; set; }
        public List<eRdABasicView> RdAInCreatedList { get; set; }
        public string RdAInCreatedStatus { get; set; }

        public int totalRdA { get; set; }
        public int delayReq { get; set; }
        public int delayPR { get; set; }
        public int delayCMS { get; set; }
        public int delayContr { get; set; }

    }
    public class eRdASigner
    {
        public string Role { get; set; }
        public string Users { get; set; }
    }
    public class eRdA
    {
        public Int32 Id { get; set; }
        public string BusinessDataType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Owner { get; set; }
        public string Project { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyUser { get; set; }

        public DateTime? ModifyDate { get; set; }
        public string Level { get; set; }
        public string Workflow { get; set; }
        public string Revision { get; set; }
    }
    
    public class eRdABasicView
    {
        public eRdABasicView()
        {
            signstatus = "discard";
            rdaactionstatus = "";
            rdaactiontitle = "";
        }

        public string eId { get; set; }
        public int InSign { get; set; }
        public string eBusinessDataType { get; set; }
        public string eName { get; set; }
        public string eNameMaster { get; set; }
        public string eDescription { get; set; }
        public string eOwner { get; set; }
        public string eProject { get; set; }
        public string eCreateUser { get; set; }
        public DateTime eCreateDate { get; set; }
        public string eLevel { get; set; }
        public string eKey1 { get; set; }
        public string eWorkflow { get; set; }
        public string eRevision { get; set; }
        public string RDACompetence { get; set; }
        public string RequestType { get; set; }
        public string Brand { get; set; }
        public string CDC { get; set; }
        public string Area { get; set; }
        public string System { get; set; }
        public string CBS { get; set; }
        public string WBS { get; set; }
        public double Amount { get; set; }
        public string RDACode { get; set; }
        public string Supplier { get; set; }

        public DateTime? ActivityStart { get; set; }
        public DateTime? ActivityEnd { get; set; }
        public double PiecePrice { get; set; }
        public double TotalQuantity { get; set; }
        public DateTime ActionDate { get; set; }
        public string rdaactionstatus { get; set; }
        public string rdaactiontitle { get; set; }
        public string signstatus { get; set; }

        public string BaanIn_Dimensione3 { get; set; }
        public string Supply { get; set; }
        public string CodiceIman { get; set; }
        public string CO_SP { get; set; }
        public double SUMPiecePrice { get; set; }
        public string Notes { get; set; }
        public int TotalNo { get; set; }
        public int delay { get; set; }

        public string BaanIn_TipoRichiesta { get; set; }
        public string BaanIn_TestoDestinazione { get; set; }
        public string BaanIn_Articolo { get; set; }
        public string BaanIn_DescArticolo { get; set; }
        
        public DateTime? BaanIn_DataErosioneBudget { get; set; }
        public DateTime? BaanIn_DataConsegnaPianificata { get; set; }
        public string Hours { get; set; }
        public string CodeProjectForm { get; set; }
        public string BaanIn_Buyer { get; set; }
        public string Contents { get; set; }
        public string BaanIn_Conto { get; set; }
        public string BaanIn_CDC { get; set; }
        public string BaanIn_Commessa { get; set; }
        public string BaanIn_ATC { get; set; }
        public string BaanIn_Magazzino { get; set; }
        public string BaanIn_RigaTesto { get; set; }
        public string BaanIn_Requester { get; set; }
        public string Purchasing { get; set; }
        public string PiecePriceFormat { get; set; }
        public string BaanIn_Acquisitore { get; set; }
        public string BaanIn_Pianificatore { get; set; }
        public string BaanIn_RiferimentoA { get; set; }
        public string BaanIn_RiferimentoB { get; set; }
        public string BaanIn_RiferimentoC { get; set; }

        public string BaanIn_GruppoArticoli { get; set; }
        public string BaanIn_CommessaOrdinePRP { get; set; }

        public string BaanIn_ExchangeMessage { get; set; }
        public string BaanIn_DataExchange { get; set; }

        public string rdaw1_spendingcurve { get; set; }

        public string rdaw1_spendingcurve_admin { get; set; }

        public double rdaw1_spendingcurve_totalAmount { get; set; }

        public string AmountFormat { get; set; }
        public string Sys_Error { get; set; }

        public string BaanIn_TipoArticolo { get; set; }
        public string BaanIn_MetodoPianificazione { get; set; }
        public string BaanIn_ContoLavoro { get; set; }
        public string DrawingCode { get; set; }
        public string MaterialDescription { get; set; }
        public string BaanIn_UMImmagazzinaggio { get; set; }
        public string BaanIn_UMStoccaggio { get; set; }
        public DateTime? BaanIn_DataOrdine { get; set; }
        public string RequestedSupplier { get; set; }
        public string BaanIn_UMAcquistoPRP { get; set; }
        public string BaanIn_Sconto { get; set; }
        public string BaanIn_ImportoScontato { get; set; }
        public string RequestedDeliveryDate { get; set; }
        public string prevTrancheToSend { get; set; }




    }

    public class eBasicFilter
    {

        public List<string> Project { get; set; }
        public List<string> FullProjectDescription { get; set; }
        public List<string> Area { get; set; }
        public List<string> AreaDescription { get; set; }
        public List<string> System { get; set; }
        public List<string> PR { get; set; }
        public List<string> PC { get; set; }

        public eBasicFilter()
        {
            Project = new List<string>();
            FullProjectDescription = new List<string>();
            Area = new List<string>();
            AreaDescription = new List<string>();
            System = new List<string>();
            PR = new List<string>();
            PC = new List<string>();
        }
    }

    public class eUserDashboardData {
        public eBasicFilter filterData;
        public List<eProjectRdaListView> projects;

        public eUserDashboardData() {
            projects = new List<eProjectRdaListView>();
        }
    }
    public class eRdATranche
    {
        public int id { get; set; }
        public double value { get; set; }
        public double totalAmount { get; set; }
        public string code { get; set; }
        public string title { get; set; }
        public string level { get; set; }
        public string colorStatus { get; set; }
        public string amountFormat { get; set; }
        public string invoiceDate { get; set; }
    }


    public class eRdACosts
    {
        public eRdACosts() {
            block = false;
            blockmessage="";
            CMS = 0;
        }
        public bool block { get; set; }
        public string blockmessage { get; set; }
        public int eId { get; set; }
        public string eKey1 { get; set; }
        public string elevel { get; set; }
        public string cbs { get; set; }
        public string requestedsupplier { get; set; }
        public string supplier { get; set; }
        public string professionalrole { get; set; }
        public string createuser { get; set; }
        public double quantityonvehicle { get; set; }
        public double totalquantity { get; set; }
        public double pieceprice { get; set; }
        public string piecepriceformat { get; set; }
        public double hours { get; set; }
        public double dailycost { get; set; }
        public double amount { get; set; }
        public string amountformat { get; set; }
        public int CMS { get; set; }
    }


    public class eUserWorklistData
    {
        public eBasicFilter filterData;
        public List<eProjectRdaListView> projects;
        public List<eRdABasicView> rdafullList;
        public int total;
        public eUserWorklistData()
        {
            projects = new List<eProjectRdaListView>();
            rdafullList = new List<eRdABasicView>();
            total = 0;
        }
    }


    public class eUserArchivedData
    {
        public List<eRdAArchiveView> rdafullList;
        public int total { get; set; }
        public string filter { get; set; }
        public eUserArchivedData()
        {
            rdafullList = new List<eRdAArchiveView>();
            total = 0;
        }
    }

    public class eRdAArchiveView
    {
		

        public string eId { get; set; }
        public int InSign { get; set; }
        public string eBusinessDataType { get; set; }
        public string eName { get; set; }
        public string eDescription { get; set; }
        public string RDACode { get; set; }
        public string baan_oda { get; set; }
        public string baan_statorda { get; set; }
        public DateTime eCreateDate { get; set; }
        public string eProject { get; set; }
        public string Supplier { get; set; }
        public double Amount { get; set; }
        public string AmountFormat { get; set; }
        public DateTime? ActivityStart { get; set; }
        public DateTime? ActivityEnd { get; set; }
        public string CDC { get; set; }
        public string Area { get; set; }
        public string CBS { get; set; }
        public string PRP { get; set; }
        public double TotalQuantity { get; set; }
        public double PiecePrice { get; set; }
        public string PiecePriceFormat { get; set; }
        public string eLevel { get; set; }
        public string eCreateUser { get; set; }
        public double BAAN_IMPEGNATO { get; set; }
        public double BAAN_RICEVUTO { get; set; }
        public double BAAN_ORDINATO { get; set; }
        public double BAAN_LIQUIDATO { get; set; }
        public double BAAN_COMMITTED { get; set; }
        public double BAAN_INVOICED { get; set; }

    }

    public class erdaComboBoxItem
    {
        public string Value;
        public string Id;
    }


    public class eUserWorkListObjects
    {
        public eBasicFilter filterData;
        public List<eProjectRdaListView> projects;

        public eUserWorkListObjects()
        {
            projects = new List<eProjectRdaListView>();
        }

    }

    public class eUserRdAProfile {
        private bool _isRequester = false;
        private bool _isPMP = false;
        private bool _isPPM = false;
        private bool _isCSM = false;
        private bool _isPM = false;
        private bool _isPR = false;
        private bool _isUR = false;
        private bool _isPlanner = false;
        private bool _isDBA = false;
        private bool _isSuperVisore = false;

        public Boolean IsRequester
        {
            get => _isRequester;
            set => _isRequester = value;
        }
        public Boolean IsPMP
        {
            get => _isPMP;
            set => _isPMP = value;
        }
        public Boolean IsPPM
        {
            get => _isPPM;
            set => _isPPM = value;
        }
        public Boolean IsCSM
        {
            get => _isCSM;
            set => _isCSM = value;
        }
        public Boolean IsPM
        {
            get => _isPM;
            set => _isPM = value;
        }
        public Boolean IsPR
        {
            get => _isPR;
            set => _isPR = value;
        }
        public Boolean IsUR
        {
            get => _isUR;
            set => _isUR = value;
        }
        public Boolean IsPlanner
        {
            get => _isPlanner;
            set => _isPlanner = value;
        }

        public Boolean IsDBA
        {
            get => _isDBA;
            set => _isDBA = value;
        }
        public Boolean IsSuperVisore
        {
            get => _isSuperVisore;
            set => _isSuperVisore = value;
        }

        public Boolean HasFullView
        {
            get => _isSuperVisore || _isDBA;
        }
    }

    public class WBSRow {
        public string ID;
        public string COMPETENCE;
        public string CDC;
        public string AREA;
        public string SYSTEM;
        public string CBS;
        public string WBS;
    }
    public class RoleFullView
    {
        public string COMPETENCE;
        public string PROJECT;
        public string ROLE;
        public string ACTIVITYNPI;
        public string USER;
    }

    public class AdminItemConfiguration
    {
        public string item;
        public string subitem;
        public Dictionary<string, List<string>> releatedItems;
        public List<WBSRow> wbs;
        public AdminItemConfiguration()
        {
            releatedItems = new Dictionary<string, List<string>>();
            wbs = new List<WBSRow>();
        }

    }
    public class CacheFile
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string FileType { get; set; }
        public string cacheName { get; set; }
        public object data { get; set; }

    }
    public class BaaNProjectMatch
    {
        public BaaNProjectMatch() {
            rdaProjectName = "";
            baanProjectName = "";
            baanFinance = "";
            baanPRP = "";
            baanCodeProjectForm = "";
        }
        public string rdaProjectName { get; set; }
        public string baanProjectName { get; set; }
        public string baanFinance { get; set; }
        public string baanPRP { get; set; }
        public string baanCodeProjectForm { get; set; }
    }
    public class UserProfile
    {
        public UserProfile(bool defaultValue = false)
        {
            REQUESTER = defaultValue;
            PR = defaultValue;
            Planner  = defaultValue;
            CMS  = defaultValue;
            PESController  = defaultValue;
            PRPController  = defaultValue;
            ConfigAdmin = defaultValue;
            SuperVisor = defaultValue;
            UR = defaultValue;
            PM = defaultValue;
            OnlyBaaN = false;
        }

        public bool REQUESTER { get; set; }
        public bool PR { get; set; }
        public bool Planner { get; set; }
        public bool CMS { get; set; }
        public bool PM { get; set; }
        public bool PESController { get; set; }
        public bool PRPController { get; set; }
        public bool ConfigAdmin { get; set; }
        public bool SuperVisor { get; set; }
        public bool UR { get; set; }
        public bool OnlyBaaN { get; set; }
    }


    public class AdminDataConfig
    {
        public string target = "";
        public string subItem = "";
        public string filter = "";
        public string role = "";
        public string competence = "";
        public string activitynpi = "";

        public Dictionary<string, List<string>> structure;
        public List<string> updateItems;
        public List<string> deleteItems;
        public AdminDataConfig()
        {
            structure = new Dictionary<string, List<string>>();
            updateItems = new List<string>();
            deleteItems = new List<string>();
        }

    }

    public class MultiAssignment
    {
        public List<string> projects;
        public List<string> users;
        public string role;
        public MultiAssignment()
        {
            projects = new List<string>();
            users = new List<string>();
            role = "";
        }

    }

    public class BudgetYears {
        public int actual { get; set; }
        public int actual_1 { get; set; }
        public int actual_2 { get; set; }

        public List<int> older { get; set; }

        public BudgetYears()
        {
            actual = DateTime.Now.Year;
            actual_1 = DateTime.Now.Year + 1;
            actual_2 = DateTime.Now.Year + 2;
            older = new List<int>();
        }


    }
    public class sm_dataToUppdate
    {
        public string cdc { get; set; }
        public string value { get; set; }
    }

    public class SupplierManagementUppdate
    {
        public string id { get; set; }
        public string year { get; set; }
        public string code { get; set; }
        public string name { get; set; }

        public sm_dataToUppdate[] data { get; set; }

        public SupplierManagementUppdate()
        {
            data = new sm_dataToUppdate[]{ };
        }
    }

    public class Supplier
    { 
        public string code { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
        public string buyer { get; set; }
        public bool isremedy { get; set; }
        public bool valid { get; set; }
        public DateTime? modifydate { get; set; }
        public DateTime? validfrom { get; set; }
        public DateTime? validto { get; set; }

    }
    public class SupplierPriceList
    {
        public string idcontratto { get; set; }
        public string fornitore { get; set; }
        public string fullname { get; set; }
        public string stato { get; set; }

        public DateTime? attivazione { get; set; }

        public DateTime? scadenza { get; set; }
        public int pos { get; set; }
        public string articolo { get; set; }

        public string descrizione { get; set; }
        public string descrizionecompleta { get; set; }
        public double prezzo { get; set; }
        public string divisa { get; set; }
        public string statoriga { get; set; }
        public bool pwt { get; set; }
        public int seq { get; set; }
        public DateTime? startvalidita { get; set; }
        public DateTime? endvalidita { get; set; }
        public DateTime? modifydate { get; set; }

    }

                       

    public class SupplierManagement
    {
        public List<string> CDC { get; set; }
        public List<string> CDC_KEY { get; set; }
        public DataTable supplierData { get; set; }

        public SupplierManagement()
        {
            CDC = new List<string>();
            CDC_KEY = new List<string>();
        }
    }

    public class RemedySupplierManagement1
    {
        public string supplierId { get; set; }
        public string supplierCode { get; set; }
        public string supplierBusinessName { get; set; }

        public List<RemedyBudget> releatedItems;

        public RemedySupplierManagement1()
        {
            releatedItems = new List<RemedyBudget>();
        }

    }
    public class RemedyBudget 
    {
        public string cdc = "";
        public string cdckey = "";
        public double allocated = 0;
        public double residual = 0;
    }


    public class eRdAExportOptions {
        public string title { get; set; }
        public string[] paramName;
        public string[] paramValue;
        public string filter { get; set; }
    }
}
