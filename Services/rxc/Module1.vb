﻿Module Module1

    Sub Main(args() As String)


        Dim myFile As String = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "read.txt")

        If args IsNot Nothing AndAlso args.Length > 0 Then
            Dim myArgs As String = ""
            For Each item As String In args
                myArgs &= item & " "
            Next
            If myArgs.ToLower.Contains("prp") Then
                myFile = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "read_prp.txt")
            ElseIf myArgs.ToLower.Contains("supplier") Then
                myFile = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "supplier.txt")
            ElseIf myArgs.ToLower.Contains("price") Then
                myFile = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "pricelist.txt")
            End If
        End If
        Dim myLines() As String = IO.File.ReadAllLines(myFile)
        For Each line As String In myLines
            Console.WriteLine(line)
        Next
        myFile = myFile.Replace(".txt", "_out.txt")
        IO.File.WriteAllLines(myFile, myLines)


    End Sub

End Module
