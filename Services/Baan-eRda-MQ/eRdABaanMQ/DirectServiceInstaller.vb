' Source: http://www.c-sharpcorner.com/Code/2003/Sept/InstallingWinServiceProgrammatically.asp

Imports System.Runtime.InteropServices



''' <summary>
''' Summary description for ServiceInstaller.
''' </summary>
Class DirectServiceInstaller
#Region "DLLImport"

    <DllImport("advapi32.dll")> _
    Public Shared Function OpenSCManager(ByVal lpMachineName As String, ByVal lpSCDB As String, ByVal scParameter As Integer) As IntPtr
    End Function
    <DllImport("Advapi32.dll")> _
    Public Shared Function CreateService(ByVal SC_HANDLE As IntPtr, ByVal lpSvcName As String, ByVal lpDisplayName As String, ByVal dwDesiredAccess As Integer, ByVal dwServiceType As Integer, ByVal dwStartType As Integer, _
    ByVal dwErrorControl As Integer, ByVal lpPathName As String, ByVal lpLoadOrderGroup As String, ByVal lpdwTagId As Integer, ByVal lpDependencies As String, ByVal lpServiceStartName As String, _
    ByVal lpPassword As String) As IntPtr
    End Function
    <DllImport("advapi32.dll")> _
    Public Shared Sub CloseServiceHandle(ByVal SCHANDLE As IntPtr)
    End Sub
    <DllImport("advapi32.dll")> _
    Public Shared Function StartService(ByVal SVHANDLE As IntPtr, ByVal dwNumServiceArgs As Integer, ByVal lpServiceArgVectors As String) As Integer
    End Function

    <DllImport("advapi32.dll", SetLastError:=True)> _
    Public Shared Function OpenService(ByVal SCHANDLE As IntPtr, ByVal lpSvcName As String, ByVal dwNumServiceArgs As Integer) As IntPtr
    End Function
    <DllImport("advapi32.dll")> _
    Public Shared Function DeleteService(ByVal SVHANDLE As IntPtr) As Integer
    End Function

    <DllImport("kernel32.dll")> _
    Public Shared Function GetLastError() As Integer
    End Function

#End Region

    ''' <summary>
    ''' This method installs and runs the service in the service conrol manager.
    ''' </summary>
    ''' <param name="svcPath">The complete path of the service.</param>
    ''' <param name="svcName">Name of the service.</param>
    ''' <param name="svcDispName">Display name of the service.</param>
    ''' <returns>True if the process went thro successfully. False if there was any error.</returns>
    Public Function InstallService(ByVal svcPath As String, ByVal svcName As String, ByVal svcDispName As String) As Boolean
        '#Region "Constants declaration."
        Dim SC_MANAGER_CREATE_SERVICE As Integer = &H2
        Dim SERVICE_WIN32_OWN_PROCESS As Integer = &H10
        'int SERVICE_DEMAND_START = 0x00000003;
        Dim SERVICE_ERROR_NORMAL As Integer = &H1

        Dim STANDARD_RIGHTS_REQUIRED As Integer = &HF0000
        Dim SERVICE_QUERY_CONFIG As Integer = &H1
        Dim SERVICE_CHANGE_CONFIG As Integer = &H2
        Dim SERVICE_QUERY_STATUS As Integer = &H4
        Dim SERVICE_ENUMERATE_DEPENDENTS As Integer = &H8
        Dim SERVICE_START As Integer = &H10
        Dim SERVICE_STOP As Integer = &H20
        Dim SERVICE_PAUSE_CONTINUE As Integer = &H40
        Dim SERVICE_INTERROGATE As Integer = &H80
        Dim SERVICE_USER_DEFINED_CONTROL As Integer = &H100

        Dim SERVICE_ALL_ACCESS As Integer = (STANDARD_RIGHTS_REQUIRED Or SERVICE_QUERY_CONFIG Or SERVICE_CHANGE_CONFIG Or SERVICE_QUERY_STATUS Or SERVICE_ENUMERATE_DEPENDENTS Or SERVICE_START Or SERVICE_STOP Or SERVICE_PAUSE_CONTINUE Or SERVICE_INTERROGATE Or SERVICE_USER_DEFINED_CONTROL)
        Dim SERVICE_AUTO_START As Integer = &H2
        '#End Region

        Try
            Dim sc_handle As IntPtr = OpenSCManager(Nothing, Nothing, SC_MANAGER_CREATE_SERVICE)

            If sc_handle.ToInt32() <> 0 Then
                Dim sv_handle As IntPtr = CreateService(sc_handle, svcName, svcDispName, SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, _
                 SERVICE_ERROR_NORMAL, svcPath, Nothing, 0, Nothing, Nothing, _
                 Nothing)

                If sv_handle.ToInt32() = 0 Then

                    CloseServiceHandle(sc_handle)
                    Return False
                Else
                    'now trying to start the service
                    Dim i As Integer = StartService(sv_handle, 0, Nothing)
                    ' If the value i is zero, then there was an error starting the service.
                    ' note: error may arise if the service is already running or some other problem.
                    If i = 0 Then
                        'Console.WriteLine("Couldnt start service");
                        Return False
                    End If
                    'Console.WriteLine("Success");
                    CloseServiceHandle(sc_handle)
                    Return True
                End If
            Else
                'Console.WriteLine("SCM not opened successfully");
                Return False

            End If
        Catch e As Exception
            Throw e
        End Try
    End Function


    ''' <summary>
    ''' This method uninstalls the service from the service conrol manager.
    ''' </summary>
    ''' <param name="svcName">Name of the service to uninstall.</param>
    Public Function UnInstallService(ByVal svcName As String) As Boolean
        Dim GENERIC_WRITE As Integer = &H40000000
        Dim sc_hndl As IntPtr = OpenSCManager(Nothing, Nothing, GENERIC_WRITE)

        If sc_hndl.ToInt32() <> 0 Then
            Dim DELETE As Integer = &H10000
            Dim svc_hndl As IntPtr = OpenService(sc_hndl, svcName, DELETE)
            'Console.WriteLine(svc_hndl.ToInt32());
            If svc_hndl.ToInt32() <> 0 Then
                Dim i As Integer = DeleteService(svc_hndl)
                If i <> 0 Then
                    CloseServiceHandle(sc_hndl)
                    Return True
                Else
                    CloseServiceHandle(sc_hndl)
                    Return False
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
End Class

