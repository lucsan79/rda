Imports System.Reflection
Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Text
Imports System.Timers
'Imports Parallaksis.ServiceScheduler
Public Class eSender

    Dim myType As String = "Each"
    Dim myTime As String = "1"
    Public WithEvents aTimer As System.Timers.Timer
    Dim myLastName As String = ""

#Region "Service Actions"

    Public Sub StartServer()

        Threading.Thread.Sleep(10000)
        Dim myStartPath As String = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
        Dim myScheduler As String = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.SchedulerType")
        Parallaksis.eJobScheduler.eJobSchedulerServer.Start()

        myType = myScheduler.Split("(")(0)
        myTime = myScheduler.Split("(")(1).Replace(")", "")

        Try

        Catch ex As Exception
            Parallaksis.eJobScheduler.eJobSchedulerServer.Log.Error(String.Format("BAAN.RDA.MQ:Baan eRdA MQ First excecution error:{0}", ex.ToString))
            StopServer()
        End Try
        Dim numeroSecondi As Integer = 300000
        If Not IsNumeric(myTime) Then
            myTime = "5"
        End If
        If IsNumeric(myTime) Then
            numeroSecondi = Integer.Parse(myTime) * 60 * 1000
        End If

        aTimer = New System.Timers.Timer(numeroSecondi)
        ' Hook up the Elapsed event for the timer. 
        aTimer.AutoReset = True
        aTimer.Enabled = True
        aTimer.Start()




    End Sub

    Public Sub StopServer()
        Try
            Parallaksis.eJobScheduler.eJobSchedulerServer.Stop()
        Catch ex As Exception

        End Try



    End Sub
#End Region


#Region "EVENTS"


    Public Sub MyCustomScheduler(sender As Object, e As ElapsedEventArgs) Handles aTimer.Elapsed
        aTimer.Enabled = False
        aTimer.Stop()

        Parallaksis.eJobScheduler.eJobSchedulerServer.Log.Info(String.Format("BAAN.RDA.MQ:Baan eRdA MQ service started"))
        Try
            Dim myPath As String = AppDomain.CurrentDomain.BaseDirectory

            Dim prog_name As String = IO.Path.Combine(myPath, "eRdABaaNMQBatch.exe")
            Parallaksis.eJobScheduler.eJobSchedulerServer.Log.Info("start request on BAAN MQ")


            If Not String.IsNullOrEmpty(myLastName) Then
                Dim P() As Process = Process.GetProcessesByName(myLastName)
                If P.Length > 0 Then
                    Parallaksis.eJobScheduler.eJobSchedulerServer.Log.Info("force start request on BAAN MQ on " & myLastName)
                    For Each pp As Process In P
                        pp.Kill()
                    Next
                End If
                GC.SuppressFinalize(P)
                P = Nothing
            End If


            Try

                Dim p As New Process()
                p.StartInfo.FileName = prog_name

                p.Start()
                myLastName = p.ProcessName
                ' Shell(s, AppWinStyle.NormalFocus)
            Catch ex As Exception
                Parallaksis.eJobScheduler.eJobSchedulerServer.Log.Error(ex.ToString)
            End Try




            'INSERIRE IL BATCH


            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()

        Catch ex As Exception
            Parallaksis.eJobScheduler.eJobSchedulerServer.Log.Error(String.Format("BAAN.RDA.MQ:" & ex.ToString))
        End Try

        aTimer.Enabled = True
        aTimer.Start()

    End Sub


#End Region




End Class
