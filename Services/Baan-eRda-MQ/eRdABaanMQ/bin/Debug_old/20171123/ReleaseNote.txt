Scopo: 
------------------------------------------------------------------------
Rilascio package:
- Servizio di invio RdA a baan

Note per il deploy del package corrente: 
------------------------------------------------------------------------
* Si assume che il contenuto della cartella 
  "Servizio Baan"
  mappa il contenuto della cartella sul server dedicato a suddetto servizio

Sequenza di rilascio:
------------------------------------------------------------------------
	1.	Effettuare stop Servizio
	2. 	Effettuare deploy dei files: "Servizio Baan\*.*"
	3.	Riavviare Servizio
