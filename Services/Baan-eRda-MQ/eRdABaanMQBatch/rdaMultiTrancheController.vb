﻿Imports System.Configuration
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs

Public Class RdaMultiTrancheController

    Public Shared Function ExecuteSql(givenSql As String, givenConnection As eConnection) As DataTable
        Dim _eFind As eFind
        Dim dataTable As New DataTable()

        If givenConnection IsNot Nothing AndAlso givenConnection.isLoggedIn Then
            _eFind = New eFind(givenConnection)
            _eFind.queryString = ("$(" & givenSql & ")")
            _eFind.Prepare(Nothing)
            _eFind.Execute(dataTable)
        End If
        Return dataTable
    End Function

    Public Shared Function GetWorkflowMaster() As String
        Dim item As String = "" & ConfigurationSettings.AppSettings("WF_RDA_MULTITRANCHES_CODESIGN")
        If String.IsNullOrEmpty(item) Then item = "WF_RDA_03"
        Return item
    End Function
    Public Shared Sub CancelSubTranche(givenBusinessData As eBusinessData)
        Dim str As String = String.Format("select eid  from eplms.eBusinessData where eWorkflow = N'WF_RDA_01' and eKey1='{0}'", givenBusinessData.Id.ToString)
        Dim dataTable As DataTable = RdaMultiTrancheController.ExecuteSql(str, givenBusinessData.Connection())
        If dataTable IsNot Nothing AndAlso dataTable.Rows.Count > 0 Then
            Dim connection As eConnection = givenBusinessData.Connection()
            Dim eBusinessDatum1 As New eBusinessData(connection)
            For Each row As DataRow In dataTable.Rows
                For Each dataRow1 As DataRow In dataTable.Rows
                    If (eBusinessDatum1.Load(Integer.Parse(dataRow1("eId").ToString)) = 0) Then
                        Dim workflow As String = eBusinessDatum1.Workflow
                        eBusinessDatum1.Reserve()
                        eBusinessDatum1.Workflow = "WF_CANCEL"
                        Dim arrayLists As ArrayList = New ArrayList
                        eBusinessDatum1.Modify(arrayLists)
                        eBusinessDatum1.Unreserve()
                        Dim _eWorkflowEngine As eWorkflowEngine = New eWorkflowEngine(givenBusinessData.Connection, eBusinessDatum1.Id)
                        _eWorkflowEngine.getCurrentWorkflowTask.performTask()

                        eBusinessDatum1.changeAttribute("ActionDate", Now.ToString("dd.MM.yyyy"))
                        eBusinessDatum1.changeAttribute("ActionComment", "Cancelled by BaaN Feedback")
                    End If
                Next
            Next

        End If



     End Sub
End Class


