﻿Imports System.Net.Mail
Imports log4net

Public Class eUtility

    Public Shared Function GetDoubleValue(ByVal givenValue As String) As Double
        givenValue = givenValue.Trim
        If (givenValue = "") Then
            Return 0
        Else
            givenValue = givenValue.Replace(".", "")
            givenValue = givenValue.Replace(",", ".")
            Try
                Return Convert.ToDouble(givenValue, System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            Catch e As Exception
                Return Double.Parse(givenValue)
            End Try

        End If

        Return 0
    End Function

    Public Shared Sub sendICTMail(ByVal message As String, ByRef Log As ILog)
        Dim client As SmtpClient = Nothing
        Try
            Dim objeto_mail As MailMessage = New MailMessage
            client = New SmtpClient
            client.Port = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Port")
            client.Host = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Host")
            client.Timeout = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Timeout")
            client.DeliveryMethod = SmtpDeliveryMethod.Network
            client.UseDefaultCredentials = True
            Dim mailFrom As String = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.ICT.MailAlert.From")
            objeto_mail.From = New MailAddress(mailFrom)
            Dim myTo As String = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.ICT.MailAlert.To")
            For Each emailTo As String In myTo.Split(";".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
                objeto_mail.To.Add(New MailAddress(emailTo))
            Next
            objeto_mail.Subject = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.ICT.MailAlert.Subject")
            objeto_mail.Body = message
            client.Send(objeto_mail)
        Catch e As Exception
            Log.Error(String.Format("BAAN.RDA.MQ:Error sending mail:{0}", e.Message))
        End Try

        Try
            client.Dispose()
        Catch ex As Exception

        End Try
        Try
            client = Nothing
        Catch ex As Exception

        End Try
    End Sub



End Class
