﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Collections
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.Runtime.InteropServices
Imports Parallaksis.ePLMS.Security
Imports System.Data

Namespace Parallaksis.eJobScheduler.Jobs

    Public Class ePLMSRDAConnectionJob
        Implements IDisposable
        Dim myeConnection As eConnection = Nothing

        Public myLogEntryName As String = "BAAN.RDA.MQ"

        Dim mydir As DirectoryInfo
        Dim PES_CMD_BAAN_EXE As String = ""
        Dim PES_CMD_BAAN_ARGS As String = ""
        Dim PRP_CMD_BAAN_EXE As String = ""
        Dim PRP_CMD_BAAN_ARGS As String = ""
        Dim CMD_BAAN_TMPPATH As String = ""
        Dim CMD_BAAN_ERROR_MAP As String = ""
        Dim RDA_PURCHASINGMAIL As String = ""
        Dim CMD_NOTIFY_ON_EXECUTION As Boolean = False

        Dim My_Process As Process
        Dim My_Process_Info As ProcessStartInfo


        ' Dim BaanList As System.Data.DataTable = Nothing
        '  Dim lines() As String = Nothing
        Dim OKRDA As Dictionary(Of Integer, String)
        Dim OKRDAOwner As Dictionary(Of Integer, String)
        Dim KORDA As Dictionary(Of Integer, String)
        Dim KORDAOwner As Dictionary(Of Integer, String)


        Dim myListOfRDACodesForSignature As New Dictionary(Of String, String)
        Dim myListOfRDACodesForSetCancelled As List(Of String)
        Dim myLinesFirme As List(Of String)
        Dim myLinesRemedy As List(Of String)

        Dim listUserToNotify As List(Of String)
        'Dim builder As StringBuilder
        Dim myRdA As eBusinessData
        Dim myeAttribute As eAttribute

        Dim rdaAttributeList As New ArrayList
        Dim SmtpServer As SmtpClient
        Dim subject As String
        Dim mydummyBD As eBusinessData
        Dim myBasicMailinglist As String = ""
        Dim myMailAlert As String = ""

        'Public Enum JOBMODULE
        '    BAAN
        '    RDADELAY
        'End Enum

        Public Sub OpenConnection()
            PES_CMD_BAAN_EXE = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ-PES.CMD.EXE")
            PES_CMD_BAAN_ARGS = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ-PES.CMD.ARGS")
            PRP_CMD_BAAN_EXE = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ-PRP.CMD.EXE")
            PRP_CMD_BAAN_ARGS = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ-PRP.CMD.ARGS")
            CMD_BAAN_TMPPATH = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.CMD.TMP_PATH")
            CMD_BAAN_ERROR_MAP = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.CMD.BAAN_ERROR_MAP")
            RDA_PURCHASINGMAIL = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.RDA.PurchasingMailAlert")
            CMD_NOTIFY_ON_EXECUTION = False
            subject = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.RDA.KO.MailAlertt.Subject")
            myBasicMailinglist = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.ICT.MailAlert.To")
            myMailAlert = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.RDA.KO.MailAlertt.body")
            Try
                CMD_NOTIFY_ON_EXECUTION = Boolean.Parse(System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.CMD.NOTIFY_ON_EXECUTION"))
            Catch ex As Exception
                CMD_NOTIFY_ON_EXECUTION = False
            End Try

            If CMD_NOTIFY_ON_EXECUTION Then
                eUtility.sendICTMail("eRdA BaaN MQ GET started " & Now.ToString, eJobSchedulerServer.Log)
            End If
            If Not IO.File.Exists(PES_CMD_BAAN_EXE) Then
                eJobSchedulerServer.Log.Error(String.Format("cmd '{0}' not found ", PES_CMD_BAAN_EXE))
                Dim message As String = String.Format("cmd '{0}' not found ", PES_CMD_BAAN_EXE)
                Throw New Exception(message)
            End If
            If Not IO.File.Exists(PRP_CMD_BAAN_EXE) Then
                eJobSchedulerServer.Log.Error(String.Format("cmd '{0}' not found ", PRP_CMD_BAAN_EXE))
                Dim message As String = String.Format("cmd '{0}' not found ", PRP_CMD_BAAN_EXE)
                Throw New Exception(message)
            End If
            If Not IO.File.Exists(CMD_BAAN_ERROR_MAP) Then
                eJobSchedulerServer.Log.Error(String.Format("file baan error mapping '{0}' not found ", CMD_BAAN_ERROR_MAP))
                Dim message As String = String.Format("file baan error mapping '{0}' not found ", CMD_BAAN_ERROR_MAP)
                Throw New Exception(message)
            End If

            If Not IO.File.Exists(RDA_PURCHASINGMAIL) Then
                eJobSchedulerServer.Log.Error(String.Format("file for mail to purchasing '{0}' not found ", RDA_PURCHASINGMAIL))
                Dim message As String = String.Format("file baan error mapping '{0}' not found ", RDA_PURCHASINGMAIL)
                Throw New Exception(message)
            End If

            Dim eplms_home As String = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.ePLMS_HOME")
            myeConnection = New eConnection(eplms_home)
            Dim rc As Integer = myeConnection.internalDbLogIn("eplms")
            If rc <> 0 Then
                Throw New Exception([String].Format("Error in internal login"))
            End If

            mydir = New DirectoryInfo(CMD_BAAN_TMPPATH)
            '    BaaNMQExecute()
            'builder = New StringBuilder

            If mydir.Exists = False Then
                eJobSchedulerServer.Log.Error(String.Format("path '{0}' not found ", mydir.FullName))
                Dim message As String = String.Format("path '{0}' not found ", mydir.FullName)
                Throw New Exception(message)
            End If

            SmtpServer = New SmtpClient()
            'Dim mail As MailMessage
            'mail = New MailMessage()
            Dim mySecurity As New pSecurity

            SmtpServer.Host = myeConnection.ePLMS_SMTP_SERVER
            SmtpServer.Port = myeConnection.ePLMS_SMTP_PORT

            Try
                Dim timeOut As String = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Timeout")
                If IsNumeric(timeOut) Then
                    SmtpServer.Timeout = Integer.Parse(timeOut)
                End If

            Catch ex As Exception

            End Try

            Dim myBDT As New eBusinessDataType(myeConnection)
            If myBDT.Load("RDA") = ePLMS_OK Then
                myBDT.getAttributeList(rdaAttributeList)
            End If
            Try
                myBDT.Dispose()
                GC.SuppressFinalize(myBDT)

            Catch ex As Exception

            End Try

        End Sub

        Friend Sub CloseConnection()

            Try
                myeConnection.logOut()

            Catch ex As Exception

            End Try
            myeConnection = Nothing
        End Sub



#Region "BAAN"
        Friend Sub BaaNMQExecute()

            'Dim message1 As String = ""
            'Dim message2 As String = ""
            'Dim message3 As String = ""
            'Dim message4 As String = ""
            'Dim message5 As String = ""
            'Dim message6 As String = ""





            Try

                OKRDA = Nothing
                OKRDAOwner = Nothing
                KORDA = Nothing
                KORDAOwner = Nothing
                listUserToNotify = Nothing
                myListOfRDACodesForSignature = Nothing

                If OKRDA Is Nothing Then OKRDA = New Dictionary(Of Integer, String)
                If OKRDAOwner Is Nothing Then OKRDAOwner = New Dictionary(Of Integer, String)
                If KORDA Is Nothing Then KORDA = New Dictionary(Of Integer, String)
                If KORDAOwner Is Nothing Then KORDAOwner = New Dictionary(Of Integer, String)
                If listUserToNotify Is Nothing Then listUserToNotify = New List(Of String)
                If myLinesFirme Is Nothing Then myLinesFirme = New List(Of String)
                If myLinesRemedy Is Nothing Then myLinesRemedy = New List(Of String)
                If myListOfRDACodesForSignature Is Nothing Then myListOfRDACodesForSignature = New Dictionary(Of String, String)
                If myListOfRDACodesForSetCancelled Is Nothing Then myListOfRDACodesForSetCancelled = New List(Of String)
                ' If builder Is Nothing Then builder = New StringBuilder

                OKRDA.Clear()
                OKRDAOwner.Clear()
                KORDA.Clear()
                KORDAOwner.Clear()
                listUserToNotify.Clear()
                myLinesFirme.Clear()
                myLinesRemedy.Clear()
                myListOfRDACodesForSignature.Clear()
                myListOfRDACodesForSetCancelled.Clear()
                'builder.Clear()


                'myLinesFirme = Nothing
                'myLinesFirme = New List(Of String)
                eJobSchedulerServer.Log.Info(String.Format("# CHECK PRESTAZIONI"))
                GetMemory()
                Try
                    Dim givenFile As String = IO.Path.Combine(mydir.FullName, Now.ToString("yyyyMMddHHmmss") & "-PES.txt")
                    Run_Process(PES_CMD_BAAN_EXE, PES_CMD_BAAN_ARGS, givenFile, CMD_BAAN_ERROR_MAP)
                    'myLinesFirme = New List(Of String)
                    SyncRdA(givenFile, "PES", myLinesFirme, myLinesRemedy)
                Catch ex As Exception
                    eJobSchedulerServer.Log.Error(String.Format("PES:" & ex.Message))
                    'message1 = String.Format("PES:" & ex.Message)
                    ' Throw New Exception(ex.Message)
                End Try
                eJobSchedulerServer.Log.Info(String.Format("# STOP CHECK PRESTAZIONI"))
                GetMemory()


                'Threading.Thread.Sleep(1000)
                eJobSchedulerServer.Log.Info(String.Format("# CHECK PRP"))
                GetMemory()
                Try
                    '  eJobSchedulerServer.Log.Info("LUCA 11")
                    Dim givenFile As String = IO.Path.Combine(mydir.FullName, Now.ToString("yyyyMMddHHmmss") & "-PRP.txt")
                    Run_Process(PRP_CMD_BAAN_EXE, PRP_CMD_BAAN_ARGS, givenFile, CMD_BAAN_ERROR_MAP)
                    SyncRdA(givenFile, "PRP", Nothing, Nothing)

                Catch ex As Exception
                    eJobSchedulerServer.Log.Error(String.Format("PRP:" & ex.Message))
                    'message2 = String.Format("PRP:" & ex.Message)
                End Try
                eJobSchedulerServer.Log.Info(String.Format("# STOP CHECK PRP"))
                GetMemory()

                GC.Collect()
                GC.WaitForPendingFinalizers()
                GC.Collect()
                If myLinesFirme Is Nothing Then
                    myLinesFirme = New List(Of String)
                End If
                If myLinesRemedy Is Nothing Then
                    myLinesRemedy = New List(Of String)
                End If

                eJobSchedulerServer.Log.Info(String.Format("************************************"))
                eJobSchedulerServer.Log.Info(String.Format("***** AFTER BAAN REQUEST *****"))
                Dim countingOK As Integer = 0
                If OKRDA IsNot Nothing AndAlso OKRDA.Count > 0 Then
                    countingOK = OKRDA.Count
                End If
                eJobSchedulerServer.Log.Info(String.Format("*****   RDA TO FINALIZE  *****"))
                eJobSchedulerServer.Log.Info("# RDA to finalize: " & countingOK)
                eJobSchedulerServer.Log.Info("# RDA FIRME: " & myLinesFirme.Count)
                eJobSchedulerServer.Log.Info("# RDA FORZATE REMEDY: " & myLinesRemedy.Count)

                Dim countCancel As Integer = 0
                For Each singleLine As String In myLinesFirme
                    If singleLine.Contains("|eliminato|") Then
                        countCancel += 1
                    End If
                Next
                eJobSchedulerServer.Log.Info("# RDA TO CANCEL: " & countCancel)


                eJobSchedulerServer.Log.Info(String.Format("*****    RDA OWNERS    *****"))
                For Each rdaId As Integer In OKRDA.Keys
                    eJobSchedulerServer.Log.Info("*   - " & OKRDA(rdaId) & " - Owner RDA:" & OKRDAOwner(rdaId))
                Next
                eJobSchedulerServer.Log.Info(String.Format("*****    RDA IN ERROR    *****"))
                Dim countingKO As Integer = 0
                If KORDA IsNot Nothing AndAlso KORDA.Count > 0 Then
                    countingKO = KORDA.Count
                End If
                eJobSchedulerServer.Log.Info("# RDA in error: " & countingKO)
                For Each rdaId As Integer In KORDA.Keys
                    eJobSchedulerServer.Log.Info("*   - " & KORDA(rdaId) & " - Owner RDA:" & KORDAOwner(rdaId))
                Next
                eJobSchedulerServer.Log.Info(String.Format("************************************"))


                Try
                    If OKRDA IsNot Nothing AndAlso OKRDA.Count > 0 Then
                        eJobSchedulerServer.Log.Info(String.Format("# Starting sending mail Purchasing"))
                        For Each rdaId As Integer In OKRDA.Keys
                            NotifyPurchasing(rdaId)
                        Next
                        eJobSchedulerServer.Log.Info(String.Format("# end sending mail Purchasing"))
                    Else
                        eJobSchedulerServer.Log.Info(String.Format("# No mail to send Purchasing"))
                    End If
                Catch ex As Exception
                    eJobSchedulerServer.Log.Error(String.Format("# Purchasing OK Notification Error: " & ex.Message))
                    'message3 = String.Format("# Purchasing OK Notification Error: " & ex.Message)
                End Try

                Try
                    listUserToNotify.Clear()
                    If KORDA IsNot Nothing AndAlso KORDA.Count > 0 Then
                        eJobSchedulerServer.Log.Info(String.Format("# Starting sending mail for ko rda"))
                        For Each rdaid As Integer In KORDA.Keys
                            If Not listUserToNotify.Contains(KORDAOwner(rdaid)) Then
                                listUserToNotify.Add(KORDAOwner(rdaid))
                            End If
                        Next
                        NotifyKoRda(listUserToNotify)
                        eJobSchedulerServer.Log.Info(String.Format("# end sending mail for ko rda"))

                    Else
                        eJobSchedulerServer.Log.Info(String.Format("# no mail to send for ko rda"))
                    End If
                    listUserToNotify = Nothing
                Catch ex As Exception
                    eJobSchedulerServer.Log.Error(String.Format("# Users KO Notification Error: " & ex.Message))
                    'message4 = String.Format("# Users KO Notification Error: " & ex.Message)
                End Try

                Try
                    If OKRDA IsNot Nothing AndAlso OKRDA.Count > 0 Then
                        eJobSchedulerServer.Log.Info(String.Format("# Starting closoure"))
                        For Each rdaId As Integer In OKRDA.Keys
                            myRdA.Load(rdaId)
                            CloseRdA(myRdA)
                        Next
                        eJobSchedulerServer.Log.Info(String.Format("# end rda closoure"))
                    Else
                        eJobSchedulerServer.Log.Info(String.Format("# no rda to close"))
                    End If
                Catch ex As Exception
                    eJobSchedulerServer.Log.Error(String.Format("# Error closing RdA: " & ex.Message))
                    'message5 = String.Format("# Error closing RdA: " & ex.Message)
                End Try

                'GetMemory()

                If myLinesFirme IsNot Nothing AndAlso myLinesFirme.Count > 0 Then
                    eJobSchedulerServer.Log.Info(String.Format("# Starting update from rda baan - history"))

                    If myListOfRDACodesForSignature Is Nothing Then myListOfRDACodesForSignature = New Dictionary(Of String, String)
                    If myListOfRDACodesForSetCancelled Is Nothing Then myListOfRDACodesForSetCancelled = New List(Of String)
                    myListOfRDACodesForSignature.Clear()
                    myListOfRDACodesForSetCancelled.Clear()
                    ' myListOfRDACodesForSignature = New Dictionary(Of String, String)
                    For Each singleLine As String In myLinesFirme

                        If singleLine.EndsWith("||00000000||00000000|00000000|00000000") Then
                            Continue For
                        End If
                        Dim myValues() As String = singleLine.Split("|")
                        If myValues.Length = 11 Then
                            Dim numeroRDA As String = myValues(1)
                            If singleLine.Contains("|eliminato|") Then
                                myListOfRDACodesForSetCancelled.Add(numeroRDA)
                            Else
                                myListOfRDACodesForSignature.Add(numeroRDA, singleLine)
                            End If
                        End If
                    Next
                    If myListOfRDACodesForSignature.Count > 0 Then
                        Dim myQuery As String = "$(Select eId,eplms.GetAttributeValue('RDACode',eId) as RDACODE from eplms.eBusinessData " &
                                                "where eLevel ='CLOSED')"


                        Dim myFind As New eFind(myeConnection)
                        myFind.queryString = String.Format(myQuery, String.Join("','", myListOfRDACodesForSignature.Keys.ToArray))
                        'myFind.selectAttributeListString = "Id"
                        myFind.Prepare(Nothing)
                        Dim myTable As DataTable
                        myFind.Execute(myTable)
                        Dim myRows() As DataRow
                        If myTable IsNot Nothing AndAlso myTable.Rows.Count > 0 Then
                            Try
                                Dim shapes As String = "('" & String.Join("','", myListOfRDACodesForSignature.Keys.ToArray) & "')"
                                myRows = myTable.Select("RDACODE IN " & shapes)
                            Catch ex As Exception

                            End Try
                        End If
                        If myRows IsNot Nothing AndAlso myRows.Length > 0 Then

                            Dim myRDAtoUpdate As New eBusinessData(myeConnection)
                            For Each row As DataRow In myRows
                                Dim singleLine As String = myListOfRDACodesForSignature(row("RDACODE").ToString)
                                Dim myValues() As String = singleLine.Split("|")
                                Dim DataRDA As String = myValues(3)
                                Dim StatoRDA As String = myValues(4)
                                Dim pr As String = myValues(5)
                                Dim prdate As String = myValues(6)
                                Dim mr As String = myValues(7)
                                Dim mrdate As String = myValues(8)
                                Dim metastorm As String = myValues(9)
                                Dim metastormdate As String = myValues(10)
                                If myRDAtoUpdate.Load(Integer.Parse(row("eId").ToString())) = ePLMS_OK Then

                                    If (prdate <> "00000000") Then
                                        myRDAtoUpdate.changeAttribute("getbaan_pr_approver", pr)
                                        myRDAtoUpdate.changeAttribute("getbaan_pr_approvedate", GetCDDate(myeConnection, prdate))
                                    End If
                                    If (mrdate <> "00000000") Then
                                        myRDAtoUpdate.changeAttribute("getbaan_mr_approver", mr)
                                        myRDAtoUpdate.changeAttribute("getbaan_mr_approvedate", GetCDDate(myeConnection, mrdate))
                                    End If
                                    If (metastormdate <> "00000000") Then
                                        myRDAtoUpdate.changeAttribute("getbaan_metastorm_approver", metastorm)
                                        myRDAtoUpdate.changeAttribute("getbaan_metastorm_approvedate", GetCDDate(myeConnection, metastormdate))
                                    End If
                                    If (DataRDA <> "00000000") Then myRDAtoUpdate.changeAttribute("baan_datarda", GetCDDate(myeConnection, DataRDA))
                                    myRDAtoUpdate.changeAttribute("baan_statorda", StatoRDA)
                                End If
                            Next
                        End If
                    End If
                    eJobSchedulerServer.Log.Info(String.Format("# Stop update from rda baan - history"))

                    eJobSchedulerServer.Log.Info(String.Format("# Starting cancel from rda baan - history"))
                    If (Me.myListOfRDACodesForSetCancelled.Count > 0) Then
                        eJobSchedulerServer.Log.Info(String.Format("#RDA to cancel: {0}", myListOfRDACodesForSetCancelled.Count))
                        Dim str14 As String = "$(Select eId  from eplms.eBusinessData where eplms.GetAttributeValue('RDACode',eId) in ('{0}'))"
                        Dim _eFind1 As eFind = New eFind(Me.myeConnection)
                        _eFind1.queryString = (String.Format(str14, String.Join("','", Me.myListOfRDACodesForSetCancelled.ToArray)))
                        _eFind1.Prepare(Nothing)
                        Dim dataTable1 As DataTable = New DataTable
                        _eFind1.Execute(dataTable1)
                        If dataTable1 IsNot Nothing AndAlso dataTable1.Rows.Count > 0 Then
                            Dim eBusinessDatum1 As eBusinessData = New eBusinessData(Me.myeConnection)
                            Try
                                For Each dataRow1 As DataRow In dataTable1.Rows
                                    If (eBusinessDatum1.Load(Integer.Parse(dataRow1("eId").ToString)) = 0) Then
                                        Dim workflow As String = eBusinessDatum1.Workflow
                                        eBusinessDatum1.Reserve()
                                        eBusinessDatum1.Workflow = "WF_CANCEL"
                                        Dim arrayLists As ArrayList = New ArrayList
                                        Dim rc As Integer = eBusinessDatum1.Modify(arrayLists)
                                        eBusinessDatum1.changeAttribute("RdaState", "eliminato")
                                        eBusinessDatum1.Unreserve()
                                        Dim _eWorkflowEngine As eWorkflowEngine = New eWorkflowEngine(Me.myeConnection, eBusinessDatum1.Id)
                                        _eWorkflowEngine.getCurrentWorkflowTask.performTask()
                                        If workflow.ToLower = RdaMultiTrancheController.GetWorkflowMaster Then
                                            RdaMultiTrancheController.CancelSubTranche(eBusinessDatum1)
                                        End If
                                        eBusinessDatum1.changeAttribute("ActionDate", Now.ToString("dd.MM.yyyy"))
                                        eBusinessDatum1.changeAttribute("ActionComment", "Cancelled by BaaN Feedback")

                                        eJobSchedulerServer.Log.Info(String.Format("# cancelled -> {0}", eBusinessDatum1.Name))
                                    Else
                                        eJobSchedulerServer.Log.Info(String.Format("# error loading rda on code -> {0}", dataRow1("eId").ToString))
                                    End If
                                Next

                            Finally

                            End Try

                        End If

                    End If
                    eJobSchedulerServer.Log.Info(String.Format("# Stop cancel from rda baan - history", New Object((0) - 1) {}))



                Else
                    eJobSchedulerServer.Log.Info(String.Format("# No BaaN RDA history in file"))
                End If

                If myLinesRemedy IsNot Nothing AndAlso myLinesRemedy.Count > 0 Then
                    eJobSchedulerServer.Log.Info(String.Format("# Starting force rda remedy"))
                    Dim myRemedy As New eBusinessData(myeConnection)
                    For Each line As String In myLinesRemedy
                        Try
                            Dim content() As String = line.Split("|")
                            Dim myId As String = content(0).Trim
                            Dim myCodiceFornitore As String = content(4).Trim
                            If Not IsNumeric(myId) Then
                                eJobSchedulerServer.Log.Info(String.Format("# error loading rda on id -> {0}, riga -> {1}", myId, line))
                                Continue For
                            End If
                            If String.IsNullOrEmpty(myCodiceFornitore) Then
                                eJobSchedulerServer.Log.Info(String.Format("# error loading supplier code rda on id -> {0},{2}, riga -> {1}", myId, line, myCodiceFornitore))
                                Continue For
                            End If
                            Dim myQuerySupplier As String = "$(SELECT TOP (1) * FROM FORNITORI WHERE (LTRIM(CODE) = '" & myCodiceFornitore & "') and (IsRemedy = 1))"
                            Dim _eFind1 As eFind = New eFind(Me.myeConnection)
                            _eFind1.queryString = myQuerySupplier
                            _eFind1.Prepare(Nothing)
                            Dim dataTable1 As DataTable = New DataTable
                            _eFind1.Execute(dataTable1)
                            If dataTable1 IsNot Nothing AndAlso dataTable1.Rows.Count > 0 Then
                                If (myRemedy.Load(Integer.Parse(myId)) = 0) Then
                                    myRemedy.Reserve()
                                    Dim rc As Integer = 0
                                    Try
                                        rc += myRemedy.changeAttribute("RequestedSupplier", "REMEDY")
                                        rc += myRemedy.changeAttribute("Supplier", dataTable1.Rows(0)("NAME"))
                                        rc += myRemedy.changeAttribute("Amount", content(7))
                                        If content(8).ToLower.Trim = "eur" Or content(8).ToLower.Trim = "" Then
                                            rc += myRemedy.changeAttribute("AmountFormat", "euro")
                                        Else
                                            rc += myRemedy.changeAttribute("AmountFormat", "dollar")
                                        End If
                                        Dim myDate As String = content(3)
                                        myDate = myDate.Substring(4) & myDate.Substring(2, 2) & myDate.Substring(0, 2) & "000000"
                                        myeConnection.Core.stringToDate(myDate, myDate)
                                        rc += myRemedy.changeAttribute("BaanIn_DataErosioneBudget", myDate)
                                    Catch ex As Exception
                                        eJobSchedulerServer.Log.Info(String.Format("# error {0}, riga -> {1},{2}", ex.Message, line, ex.ToString))
                                    End Try
                                    myRemedy.Unreserve()
                                    eJobSchedulerServer.Log.Info(String.Format("# Updated RDA {0}, rc={1}, riga -> {1}", myRemedy.Name, rc, line))

                                End If

                            Else
                                eJobSchedulerServer.Log.Info(String.Format("#supplier with code {0} not found , riga -> {1}", myCodiceFornitore, line))
                                Continue For
                            End If
                        Catch ex As Exception
                            eJobSchedulerServer.Log.Error(String.Format("error {0} , riga -> {1}", ex.Message, line))
                            Continue For
                        End Try
                    Next

                End If
                'If CMD_NOTIFY_ON_EXECUTION Then
                '    Try

                '        eJobSchedulerServer.Log.Info(String.Format("sending mail to ict"))
                '        eUtility.sendICTMail(String.Format("Baan MQ Monitor ended"), eJobSchedulerServer.Log)
                '    Catch ex As Exception
                '        eJobSchedulerServer.Log.Error(String.Format("Error sending ICT mail: " & ex.Message))
                '        message6 = String.Format("Error sending ICT mail" & ex.Message)
                '    End Try
                'End If

                'If message1 <> "" Or message2 <> "" Or message3 <> "" Or message4 <> "" Or message5 <> "" Or message6 <> "" Then

                '    If Not String.IsNullOrEmpty(message1.Trim) Then builder.AppendLine(message1)
                '    If Not String.IsNullOrEmpty(message2.Trim) Then builder.AppendLine(message2)
                '    If Not String.IsNullOrEmpty(message3.Trim) Then builder.AppendLine(message3)
                '    If Not String.IsNullOrEmpty(message4.Trim) Then builder.AppendLine(message4)
                '    If Not String.IsNullOrEmpty(message5.Trim) Then builder.AppendLine(message5)
                '    If Not String.IsNullOrEmpty(message6.Trim) Then builder.AppendLine(message6)
                '    'Throw New Exception(builder.ToString)

                'End If
                'GetMemory()
                'myeConnection.logOut()
                'myeConnection.Dispose()
                'myeConnection = Nothing



            Catch ex As Exception

                eUtility.sendICTMail(String.Format("Error: {0} {1}", ex.Message, ex.ToString()), eJobSchedulerServer.Log)
                eJobSchedulerServer.Log.Error(String.Format("Error. {0} {1}", ex.Message, ex.ToString))
                Dim message As String = String.Format("Error: {0} {1}", ex.Message, ex.ToString)
                'Throw New Exception(message)
            Finally
                GetMemory()

                Try

                    OKRDA.Clear()
                    OKRDAOwner.Clear()
                    KORDA.Clear()
                    KORDAOwner.Clear()
                    listUserToNotify.Clear()
                    myListOfRDACodesForSignature.Clear()
                    myLinesFirme.Clear()
                Catch ex As Exception

                End Try
                Try
                    GC.SuppressFinalize(OKRDA)
                    GC.SuppressFinalize(OKRDAOwner)

                    GC.SuppressFinalize(KORDA)
                    GC.SuppressFinalize(KORDAOwner)

                    GC.SuppressFinalize(listUserToNotify)
                    GC.SuppressFinalize(myListOfRDACodesForSignature)
                    GC.SuppressFinalize(myLinesFirme)
                Catch ex As Exception

                End Try
                'Try
                '    OKRDA = Nothing
                '    OKRDAOwner = Nothing
                '    KORDA = Nothing
                '    KORDAOwner = Nothing
                '    listUserToNotify = Nothing
                'Catch ex As Exception

                'End Try

                GC.Collect()
                GC.WaitForPendingFinalizers()
                GC.Collect()
                eJobSchedulerServer.Stop()

            End Try
            GC.Collect(2, GCCollectionMode.Default)
            GC.Collect(2, GCCollectionMode.Forced)
            GC.Collect(2, GCCollectionMode.Optimized)

        End Sub
        Public Function GetCDDate(ByRef myeConnection As eConnection, ByVal givenDate As String) As String
            If givenDate.Length = 8 Then
                givenDate = String.Format("{0}{1}{2}", givenDate.Substring(4, 4), givenDate.Substring(2, 2), givenDate.Substring(0, 2)) & "000000"
                myeConnection.Core.stringToDate(givenDate, givenDate)
                ' givenDate = String.Format("{0}.{1}.{2}", givenDate.Substring(0, 2), givenDate.Substring(2, 2), givenDate.Substring(4, 4))
            End If
            Return givenDate
        End Function

        Friend Function GetMemory()
            ' Dim after = System.Diagnostics.Process.GetCurrentProcess().PrivateMemorySize64
            'after = after / 1024

            eJobSchedulerServer.Log.Info(String.Format("memory usage {0} ", GC.GetTotalMemory(False)))
        End Function
        Friend Function IsFileLocked(filePath As String) As Boolean
            Try
                Using File.Open(filePath, FileMode.Open)
                End Using
            Catch e As IOException
                Dim errorCode = Marshal.GetHRForException(e) And ((1 << 16) - 1)

                Return errorCode = 32 OrElse errorCode = 33
            End Try

            Return False
        End Function

        Friend Function SyncRdA_BACKUP(givenFile As String, cmdType As String, ByRef myeConnection As eConnection, ByRef okList As List(Of eBusinessData), ByRef koList As List(Of eBusinessData))
            ' Exit Function
            Dim myLines() As String
            Dim Rc As Integer

            'Threading.Thread.Sleep(10000)

            If IO.File.Exists(givenFile) Then


                eJobSchedulerServer.Log.Info(String.Format("Starting sync rda using {0}", IO.Path.GetFileName(givenFile)))

                'Threading.Thread.Sleep(10000)

                myLines = IO.File.ReadAllLines(givenFile)
                If myLines.Length > 0 Then
                    If CMD_NOTIFY_ON_EXECUTION Then
                        eUtility.sendICTMail("eRdA BaaN PARSING FILE " & IO.Path.GetFileName(givenFile), eJobSchedulerServer.Log)
                    End If

                    eJobSchedulerServer.Log.Info(String.Format("Parsing {0} rows", myLines.Length))
                    Dim myCollection As New Generic.Dictionary(Of String, List(Of String))
                    Dim myList As New List(Of String)

                    For Each line As String In myLines
                        line = line.Trim
                        If line.StartsWith("F|") Then
                            line = line.Replace("F|", "")
                            If myCollection.ContainsKey(line) Then
                                myCollection(line) = myList
                            Else
                                myList = New List(Of String)
                                myCollection(line) = myList
                            End If
                        Else
                            myList.Add(line)
                        End If
                    Next
                    myList = Nothing
                    For Each myDataExchangeName As String In myCollection.Keys
                        Dim name As String = myDataExchangeName
                        name = name.Replace("eRDA-" & cmdType & "_", "")
                        name = IO.Path.GetFileNameWithoutExtension(name)
                        eJobSchedulerServer.Log.Info(String.Format("Parsing DataExchange {0}", name))
                        If myRdA Is Nothing Then myRdA = New eBusinessData(myeConnection)

                        Dim myListOfRdA As List(Of String) = myCollection(myDataExchangeName)
                        If myListOfRdA IsNot Nothing Then
                            '    myRdA.Load(0)
                            Dim messages As String = ""
                            Dim myStringBuilder As New StringBuilder
                            Dim BAANCODE As String = ""
                            Dim BAANCODE_STORICO As String = ""
                            If cmdType = "PES" Then
                                For Each line As String In myListOfRdA
                                    Dim tipoRecord As String = line.Split("|")(0)
                                    Dim rdaId As String = line.Split("|")(1)

                                    If myRdA.Id <> rdaId Then
                                        If myRdA.Id > 0 Then
                                            myRdA.changeAttribute("RDACode", BAANCODE)
                                            eJobSchedulerServer.Log.Info(String.Format(" - RDA:{0} -> RDACODE:{1}", myRdA.Name, BAANCODE))
                                            myRdA.changeAttribute("BaanIn_ExchangeMessage", myStringBuilder.ToString.Trim)
                                            If BAANCODE <> "" Then
                                                okList.Add(myRdA)
                                                myRdA.changeAttribute("BaanCodeWriter", myRdA.Owner)
                                                Dim BaanCodeWriteDate As String = Now.ToString("yyyyMMddHHmmss")
                                                myRdA.Connection.Core.stringToDate(BaanCodeWriteDate, BaanCodeWriteDate)
                                                BaanCodeWriteDate = BaanCodeWriteDate.Split(" ")(0)
                                                myRdA.changeAttribute("BaanCodeWriteDate", BaanCodeWriteDate)
                                            Else
                                                If myStringBuilder.ToString <> "" Then
                                                    koList.Add(myRdA)
                                                End If
                                            End If
                                            BAANCODE = ""
                                            myStringBuilder = New StringBuilder
                                        End If
                                        myRdA = New eBusinessData(myeConnection)
                                        If myRdA.Load(rdaId) <> ePLMS_OK Then
                                            eJobSchedulerServer.Log.Error(String.Format("Error reading RDA, DBCODE eId {0}", rdaId))
                                        End If
                                    End If
                                    Select Case tipoRecord
                                        Case "H"
                                            If BAANCODE = "" Then BAANCODE = line.Split("|")(2)
                                        Case "L"
                                            If BAANCODE = "" Then BAANCODE = line.Split("|")(3)
                                    End Select
                                    If BAANCODE = "0" Then
                                        BAANCODE = ""
                                    End If
                                    Dim message As String = line.Split("|")(line.Split("|").Length - 1)
                                    If message <> "" Then
                                        myStringBuilder.AppendLine(message)
                                    End If

                                    'UR10 - Gestione RDA con errori di 'secondo livello'
                                    If myStringBuilder.ToString <> "" Then
                                        BAANCODE = ""
                                    End If
                                    'UR10 - Gestione RDA con errori di 'secondo livello'
                                Next
                            Else
                                For Each line As String In myListOfRdA
                                    Dim tipoRecord As String = line.Split("|")(0)
                                    Dim rdaId As String = line.Split("|")(1)

                                    If myRdA.Id <> rdaId Then
                                        If myRdA.Id > 0 Then

                                            myRdA.changeAttribute("RDACode", BAANCODE)
                                            eJobSchedulerServer.Log.Info(String.Format(" - RDA:{0} -> RDACODE:{1}", myRdA.Name, BAANCODE))
                                            myRdA.changeAttribute("PRP", BAANCODE_STORICO)
                                            'eJobSchedulerServer.Log.Info(String.Format(" - RDA:{0} -> BAANCODE_STORICO:{1}", myRdA.Name, BAANCODE_STORICO))
                                            myRdA.changeAttribute("BaanIn_ExchangeMessage", myStringBuilder.ToString.Trim)
                                            If BAANCODE <> "" Then
                                                okList.Add(myRdA)
                                                myRdA.changeAttribute("BaanCodeWriter", myRdA.Owner)
                                                Dim BaanCodeWriteDate As String = Now.ToString("yyyyMMddHHmmss")
                                                myRdA.Connection.Core.stringToDate(BaanCodeWriteDate, BaanCodeWriteDate)
                                                BaanCodeWriteDate = BaanCodeWriteDate.Split(" ")(0)
                                                myRdA.changeAttribute("BaanCodeWriteDate", BaanCodeWriteDate)

                                            Else
                                                If myStringBuilder.ToString <> "" Then
                                                    koList.Add(myRdA)
                                                End If
                                            End If
                                            BAANCODE = ""
                                            myStringBuilder = New StringBuilder
                                        End If
                                        myRdA = New eBusinessData(myeConnection)
                                        If myRdA.Load(rdaId) <> ePLMS_OK Then
                                            eJobSchedulerServer.Log.Error(String.Format("Error reading RDA, DBCODE eId {0}", rdaId))
                                        End If
                                    End If
                                    Select Case tipoRecord
                                        Case "H"
                                            If BAANCODE = "" Then BAANCODE = line.Split("|")(3)
                                            If BAANCODE_STORICO = "" Then BAANCODE_STORICO = line.Split("|")(4)
                                            'Case "L"
                                            '    If BAANCODE = "" Then BAANCODE = line.Split("|")(3)
                                    End Select
                                    If BAANCODE = "0" Then
                                        BAANCODE = ""
                                    End If
                                    If BAANCODE_STORICO = "0" Then
                                        BAANCODE_STORICO = ""
                                    End If

                                    Dim message As String = line.Split("|")(line.Split("|").Length - 1)
                                    If message <> "" Then
                                        myStringBuilder.AppendLine(message)
                                    End If
                                    'UR10 - Gestione RDA con errori di 'secondo livello'
                                    'If myStringBuilder.ToString <> "" Then
                                    '    BAANCODE = ""
                                    '    BAANCODE_STORICO = ""
                                    'End If
                                    'UR10 - Gestione RDA con errori di 'secondo livello'

                                Next
                            End If

                            If BAANCODE = "0" Then
                                BAANCODE = ""
                            End If
                            If BAANCODE_STORICO = "0" Then
                                BAANCODE_STORICO = ""
                            End If
                            'UR10 - Gestione RDA con errori di 'secondo livello'
                            'If myStringBuilder.ToString <> "" Then
                            '    BAANCODE = ""
                            'End If
                            'UR10 - Gestione RDA con errori di 'secondo livello'
                            'If BAANCODE <> "" Or myStringBuilder.ToString <> "" Then
                            myRdA.changeAttribute("RDACode", BAANCODE)
                            eJobSchedulerServer.Log.Info(String.Format(" - RDA:{0} -> RDACODE:{1}", myRdA.Name, BAANCODE))
                            myRdA.changeAttribute("PRP", BAANCODE_STORICO)
                            'eJobSchedulerServer.Log.Info(String.Format(" - RDA:{0} -> BAANCODE_STORICO:{1}", myRdA.Name, BAANCODE_STORICO))
                            myRdA.changeAttribute("BaanIn_ExchangeMessage", myStringBuilder.ToString.Trim)
                            'If BAANCODE_STORICO <> "" Then

                            'End If
                            If BAANCODE <> "" Then
                                okList.Add(myRdA)
                                myRdA.changeAttribute("BaanCodeWriter", myRdA.Owner)
                                'myRdA.changeAttribute("BaanCodeWriteDate", Now.ToString("dd.MM.yyyy"))
                                Dim BaanCodeWriteDate As String = Now.ToString("yyyyMMddHHmmss")
                                myRdA.Connection.Core.stringToDate(BaanCodeWriteDate, BaanCodeWriteDate)
                                BaanCodeWriteDate = BaanCodeWriteDate.Split(" ")(0)
                                myRdA.changeAttribute("BaanCodeWriteDate", BaanCodeWriteDate)
                            Else
                                If myStringBuilder.ToString <> "" Then
                                    koList.Add(myRdA)
                                End If
                            End If

                            BAANCODE = ""
                            myStringBuilder = New StringBuilder
                            myRdA.Dispose()
                            myRdA = Nothing
                            'End If

                        End If


                    Next


                Else
                    eJobSchedulerServer.Log.Error(String.Format("No rows found in {0}", IO.Path.GetFileName(givenFile)))
                    eJobSchedulerServer.Log.Error(String.Format("file deleting {0}...", IO.Path.GetFileName(givenFile)))
                    Try
                        IO.File.Delete(givenFile)
                    Catch ex As Exception

                    End Try
                End If




            End If
ExitFunction:


        End Function

        Friend Function SyncRdA(givenFile As String, cmdType As String, ByRef giveLinesFirme As List(Of String), ByRef givenLinesRemedy As List(Of String))
            ' Exit Function
            Dim myLines() As String
            Dim myCollection As Generic.Dictionary(Of String, List(Of String))
            Dim dummyList As List(Of String)
            Dim Rc As Integer



            If IO.File.Exists(givenFile) Then


                eJobSchedulerServer.Log.Info(String.Format("Starting sync rda using {0}", IO.Path.GetFileName(givenFile)))

                Threading.Thread.Sleep(5000)

                myLines = IO.File.ReadAllLines(givenFile)
                If myLines.Length > 0 Then

                    If giveLinesFirme IsNot Nothing Then
                        Dim startFile As Boolean = False
                        Dim newLines As New List(Of String)
                        For Each line As String In myLines
                            line = line.Trim
                            If line.ToUpper.StartsWith("START_FILE") Then
                                startFile = True
                            ElseIf line.ToUpper.StartsWith("END_FILE ") Then
                                startFile = False
                            Else
                                If startFile Then
                                    If giveLinesFirme Is Nothing Then
                                        giveLinesFirme = New List(Of String)
                                    End If
                                    giveLinesFirme.Add(line)
                                Else
                                    newLines.Add(line)
                                End If
                            End If
                        Next
                        myLines = newLines.ToArray
                    End If

                    If givenLinesRemedy IsNot Nothing Then
                        Dim startFile As Boolean = False
                        For Each line As String In myLines
                            line = line.Trim
                            If line.ToUpper.StartsWith("INIZIO FILE") Then
                                startFile = True
                            ElseIf line.ToUpper.StartsWith("FINE FILE ") Then
                                startFile = False
                            Else
                                If startFile Then
                                    If givenLinesRemedy Is Nothing Then
                                        givenLinesRemedy = New List(Of String)
                                    End If
                                    givenLinesRemedy.Add(line)
                                End If
                            End If
                        Next
                    End If

                    If CMD_NOTIFY_ON_EXECUTION Then
                        eUtility.sendICTMail("eRdA BaaN PARSING FILE " & IO.Path.GetFileName(givenFile), eJobSchedulerServer.Log)
                    End If

                    eJobSchedulerServer.Log.Info(String.Format("Parsing RDA {0} rows", myLines.Length))
                    If giveLinesFirme IsNot Nothing Then
                        eJobSchedulerServer.Log.Info(String.Format("Parsing FIRME {0} rows", giveLinesFirme.Count))
                    Else
                        eJobSchedulerServer.Log.Info("Parsing FIRME NO rows")
                    End If

                    myCollection = New Generic.Dictionary(Of String, List(Of String))
                    dummyList = New List(Of String)
                    For Each line As String In myLines
                        line = line.Trim
                        If line.StartsWith("F|") Then
                            line = line.Replace("F|", "")
                            If myCollection.ContainsKey(line) Then
                                myCollection(line) = dummyList
                            Else
                                dummyList = New List(Of String)
                                myCollection(line) = dummyList
                            End If
                        Else
                            dummyList.Add(line)
                        End If
                    Next
                    dummyList = Nothing
                    GC.Collect()
                    For Each myDataExchangeName As String In myCollection.Keys
                        Dim name As String = myDataExchangeName
                        name = name.Replace("eRDA-" & cmdType & "_", "")
                        name = IO.Path.GetFileNameWithoutExtension(name)
                        eJobSchedulerServer.Log.Info(String.Format("Parsing DataExchange {0}", name))


                        Dim myListOfRdA As List(Of String) = myCollection(myDataExchangeName)
                        If myListOfRdA IsNot Nothing Then
                            Dim myListItemToManage As Dictionary(Of Integer, MyDataExchange) = GetRdAInLines(myListOfRdA)
                            If myListItemToManage IsNot Nothing Then
                                If myRdA Is Nothing Then myRdA = New eBusinessData(myeConnection)
                                For Each idRdA As Integer In myListItemToManage.Keys

                                    If myRdA.Load(idRdA) <> ePLMS_OK Then
                                        eJobSchedulerServer.Log.Error(String.Format("Error reading RDA, DBCODE eId {0}", idRdA))
                                        Continue For
                                    End If
                                    Dim BAANCODE As String = ExtractValue("BAANCODE", myListItemToManage(idRdA), cmdType)
                                    Dim BAANCODE_STORICO As String = ExtractValue("BAANCODE_STORICO", myListItemToManage(idRdA), cmdType)
                                    Dim MessageError As String = ExtractValue("MESSAGE", myListItemToManage(idRdA), cmdType)

                                    'UR10 - Gestione RDA con errori di 'secondo livello'
                                    If MessageError.ToString <> "" And cmdType = "PES" Then
                                        BAANCODE = ""
                                    End If

                                    'UR10 - Gestione RDA con errori di 'secondo livello'
                                    myRdA.changeAttribute("RDACode", BAANCODE)
                                    myRdA.changeAttribute("BaanIn_ExchangeMessage", MessageError)
                                    eJobSchedulerServer.Log.Info(String.Format(" - RDA:{0} -> RDACODE:{1}", myRdA.Name, BAANCODE))
                                    If cmdType = "PRP" Then
                                        myRdA.changeAttribute("PRP", BAANCODE_STORICO)
                                        eJobSchedulerServer.Log.Info(String.Format(" - RDA:{0} -> PRP:{1}", myRdA.Name, BAANCODE_STORICO))
                                    End If
                                    If BAANCODE <> "" Then
                                        If Not OKRDA.ContainsKey(myRdA.Id) Then
                                            OKRDA.Add(myRdA.Id, myRdA.Name)
                                            OKRDAOwner.Add(myRdA.Id, myRdA.Owner)
                                        Else
                                            OKRDA(myRdA.Id) = myRdA.Name
                                            OKRDAOwner(myRdA.Id) = myRdA.Owner

                                        End If

                                        myRdA.changeAttribute("BaanCodeWriter", myRdA.Owner)
                                        'myRdA.changeAttribute("BaanCodeWriteDate", Now.ToString("dd.MM.yyyy"))
                                        Dim BaanCodeWriteDate As String = Now.ToString("yyyyMMddHHmmss")
                                        myRdA.Connection.Core.stringToDate(BaanCodeWriteDate, BaanCodeWriteDate)
                                        BaanCodeWriteDate = BaanCodeWriteDate.Split(" ")(0)
                                        myRdA.changeAttribute("BaanCodeWriteDate", BaanCodeWriteDate)
                                    Else
                                        If MessageError.ToString <> "" Then
                                            If Not KORDA.ContainsKey(myRdA.Id) Then
                                                KORDA.Add(myRdA.Id, myRdA.Name)
                                                KORDAOwner.Add(myRdA.Id, myRdA.Owner)
                                            Else
                                                KORDA(myRdA.Id) = myRdA.Name
                                                KORDAOwner(myRdA.Id) = myRdA.Owner
                                            End If
                                        End If
                                    End If
                                    Try
                                        myListItemToManage(idRdA).Dispose()
                                    Catch ex As Exception

                                    End Try

                                Next
                            End If
                            Try
                                GC.SuppressFinalize(myListItemToManage)
                                'myListItemToManage = Nothing
                            Catch ex As Exception

                            End Try

                        End If

                        Try
                            GC.SuppressFinalize(myListOfRdA)
                            ' myListOfRdA = Nothing
                        Catch ex As Exception

                        End Try
                    Next
                Else
                    eJobSchedulerServer.Log.Error(String.Format("No rows found in {0}", IO.Path.GetFileName(givenFile)))
                    eJobSchedulerServer.Log.Error(String.Format("file deleting {0}...", IO.Path.GetFileName(givenFile)))
                    Try
                        IO.File.Delete(givenFile)
                    Catch ex As Exception

                    End Try
                End If




            End If
ExitFunction:


        End Function

        Friend Function SyncFirmeRdA(givenLinesFirme As List(Of String))

            If givenLinesFirme IsNot Nothing AndAlso givenLinesFirme.Count > 0 Then



            End If

ExitFunction:


        End Function



        Public Function ExtractValue(givenAttributeName As String, givenData As MyDataExchange, context As String) As String
            Dim retValue As String = ""
            Select Case givenAttributeName.ToUpper
                Case "BAANCODE"
                    If context = "PES" Then
                        retValue = givenData.campoH.Split("|")(2).Trim
                        If retValue = "" Then
                            For Each line As String In givenData.campoL
                                If retValue = "" Then
                                    retValue = line.Split("|")(3).Trim
                                End If

                            Next

                        End If
                    ElseIf context = "PRP" Then
                        retValue = givenData.campoH.Split("|")(3)
                    End If

                    If retValue = "0" Then
                        retValue = ""
                    End If
                Case "BAANCODE_STORICO"
                    If context = "PRP" Then
                        retValue = givenData.campoH.Split("|")(4)
                    End If
                    If retValue = "0" Then
                        retValue = ""
                    End If
                Case "MESSAGE"
                    retValue = givenData.GetMessages
            End Select

            Return retValue
        End Function
        Public Function GetRdAInLines(givenLines As List(Of String)) As Dictionary(Of Integer, MyDataExchange)
            Dim retValue As New Dictionary(Of Integer, MyDataExchange)
            '1a linea e seconda linea sono quelle di testata
            If givenLines.Count > 0 Then
                For idx As Integer = 0 To givenLines.Count - 1
                    Dim myDataExchange As New MyDataExchange
                    Dim currentLine As String = givenLines(idx)
                    Dim idRDA As String = currentLine.Split("|")(1)
                    If IsNumeric(idRDA) Then
                        If Not retValue.ContainsKey(Integer.Parse(idRDA)) Then
                            retValue.Add(Integer.Parse(idRDA), myDataExchange)
                        Else
                            myDataExchange = retValue(Integer.Parse(idRDA))
                        End If

                        Dim firstLetters As String = currentLine.Split("|")(0)
                        Select Case firstLetters.ToUpper.Trim
                            Case "H"
                                myDataExchange.campoH = currentLine
                            Case "TH"
                                myDataExchange.campoTH.Add(currentLine)
                            Case "L"
                                myDataExchange.campoL.Add(currentLine)
                            Case "TL"
                                myDataExchange.campoTL.Add(currentLine)
                        End Select
                        retValue(Integer.Parse(idRDA)) = myDataExchange
                    End If

                Next
            End If

            Return retValue
            'F|eRDA-PES_RdABaaN-DE2017-0000000171.txt
            'H|1003036102|761295||
            'TH|1003036102||
            'TH|1003036102||
            'TH|1003036102||
            'TH|1003036102||
            'TH|1003036102||
            'L|1003036102|1|761295|1||
            'TL|1003036102|1||
            'TL|1003036102|1||
            'TL|1003036102|1||
            'TL|1003036102|1||
            'TL|1003036102|1||
            'F|eRDA-PES_RdABaaN-DE2017-0000000171.txt

        End Function
        Public Class MyDataExchange
            Implements IDisposable

            Public campoH As String
            Public campoTH As List(Of String)
            Public campoL As List(Of String)
            Public campoTL As List(Of String)
            Private _campoMessage As List(Of String)

            Public Sub New()
                campoH = ""
                campoTH = New List(Of String)
                campoL = New List(Of String)
                campoTL = New List(Of String)
                _campoMessage = New List(Of String)
            End Sub

            ReadOnly Property GetMessages() String
                Get
                    Dim stringBuilder As New Text.StringBuilder
                    Dim retValue As String = ""

                    retValue = ExtractMessage(campoH)
                    If retValue <> "" Then stringBuilder.AppendLine(retValue)

                    For Each line As String In campoTH
                        retValue = ExtractMessage(line)
                        If retValue <> "" Then stringBuilder.AppendLine(retValue)
                    Next

                    For Each line As String In campoL
                        retValue = ExtractMessage(line)
                        If retValue <> "" Then stringBuilder.AppendLine(retValue)
                    Next

                    For Each line As String In campoTL
                        retValue = ExtractMessage(line)
                        If retValue <> "" Then stringBuilder.AppendLine(retValue)
                    Next

                    Return stringBuilder.ToString
                End Get

            End Property

            Private Function ExtractMessage(givenField As String) As String
                Dim retValue As String = ""
                If givenField.Contains("|") Then
                    Dim split() As String = givenField.Split("|")
                    retValue = split(split.Length - 1)
                End If
                Return retValue
            End Function

#Region "IDisposable Support"
            Private disposedValue As Boolean ' To detect redundant calls

            ' IDisposable
            Protected Overridable Sub Dispose(disposing As Boolean)
                If Not disposedValue Then
                    If disposing Then
                        campoH = Nothing
                        campoTH = Nothing
                        campoL = Nothing
                        campoTL = Nothing
                        _campoMessage = Nothing
                    End If

                    ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                    ' TODO: set large fields to null.
                End If
                disposedValue = True


            End Sub

            ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
            Protected Overrides Sub Finalize()
                '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
                Dispose(False)
                MyBase.Finalize()
            End Sub

            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
                Dispose(True)
                ' TODO: uncomment the following line if Finalize() is overridden above.
                GC.SuppressFinalize(Me)
            End Sub
#End Region
        End Class
#End Region

        Friend Sub Run_Process(ByRef Process_Name As String, ByRef Process_Arguments As String, ByRef givenFile As String, ByRef CMD_BAAN_ERROR_MAP As String)

            ' Returns True if "Read_Output" argument is False and Process was finished OK
            ' Returns False if ExitCode is not "0"
            ' Returns Nothing if process can't be found or can't be started
            ' Returns "ErrorOutput" or "StandardOutput" (In that priority) if Read_Output argument is set to True.

            Dim Read_Output As Boolean = False
            Dim Process_Hide As Boolean = True
            Dim Process_TimeOut As Integer = 999999999

            Dim currentProcess_Arguments As String = String.Format("{0} > {1}", Process_Arguments, IO.Path.GetFileName(givenFile))
            Dim myBatContent As String = String.Format("""{0}"" {1}", Process_Name, currentProcess_Arguments)
            Dim myFileBat As String = givenFile.Replace(IO.Path.GetExtension(givenFile), ".bat")
            IO.File.WriteAllText(myFileBat, myBatContent)

            Dim ERRORLEVEL As Integer
            Try

                If My_Process Is Nothing Then
                    'eJobSchedulerServer.Log.Error("My_Process")
                    My_Process = New Process
                End If
                If My_Process_Info Is Nothing Then
                    ' eJobSchedulerServer.Log.Error("My_Process_Info")
                    My_Process_Info = New ProcessStartInfo
                End If

                My_Process_Info.FileName = myFileBat ' Process_Name ' Process filename
                My_Process_Info.Arguments = Nothing ' Process_Arguments ' Process arguments
                My_Process_Info.CreateNoWindow = Process_Hide ' Show or hide the process Window
                My_Process_Info.UseShellExecute = True ' Don't use system shell to execute the process
                My_Process_Info.RedirectStandardOutput = Read_Output '  Redirect (1) Output
                My_Process_Info.RedirectStandardError = Read_Output ' Redirect non (1) Output
                My_Process.EnableRaisingEvents = True ' Raise events
                My_Process_Info.WorkingDirectory = IO.Path.GetDirectoryName(givenFile)
                My_Process.StartInfo = My_Process_Info

                eJobSchedulerServer.Log.Info("cmd sended to BAAN")
                eJobSchedulerServer.Log.Info("cmd: " & myBatContent)
                My_Process.Start() ' Run the process NOW

                My_Process.WaitForExit(Process_TimeOut) ' Wait X ms to kill the process (Default value is 999999999 ms which is 277 Hours)

                ERRORLEVEL = My_Process.ExitCode ' Stores the ExitCode of the process
                If Not ERRORLEVEL = 0 Then
                    Dim message As String = String.Format("ExitCode: {0}, Message: {1}", ERRORLEVEL, GetBaanMessageError(ERRORLEVEL, CMD_BAAN_ERROR_MAP))
                    Throw New Exception(message)
                End If ' Return ERRORLEVEL ' Returns the Exitcode if is not 0

                If Read_Output = True Then
                    Dim Process_ErrorOutput As String = My_Process.StandardOutput.ReadToEnd() ' Stores the Error Output (If any)
                    Dim Process_StandardOutput As String = My_Process.StandardOutput.ReadToEnd() ' Stores the Standard Output (If any)
                    ' Return output by priority
                    If Process_ErrorOutput IsNot Nothing Then
                        Throw New Exception(Process_ErrorOutput) '  Return Process_ErrorOutput ' Returns the ErrorOutput (if any)
                    End If

                End If

                My_Process.Close()
                'My_Process.Dispose()
                'My_Process = Nothing
                'Try
                '    GC.SuppressFinalize(My_Process)
                'Catch ex As Exception

                'End Try
                'Try
                '    GC.SuppressFinalize(My_Process_Info)
                'Catch ex As Exception

                'End Try
                'My_Process = Nothing
                'My_Process_Info = Nothing
                GC.WaitForPendingFinalizers()
                GC.Collect(2)
            Catch ex As Exception
                Dim message As String = ex.Message
                Throw New Exception("Error retrieving data from Baan. " & message)
            End Try
        End Sub

        Private Function GetBaanMessageError(givenCode As Integer, CMD_BAAN_ERROR_MAP As String) As String
            Dim myFiles As String = CMD_BAAN_ERROR_MAP
            Dim myLines() As String = IO.File.ReadAllLines(myFiles)
            For Each line As String In myLines
                Dim code As Integer = line.Split(":")(0)
                Dim message As String = line.Split(":")(1)
                If code = givenCode Then
                    Return message
                End If
            Next
            Return ""
        End Function


        Private Function NotifyPurchasing(ByVal givenRDAId As Integer) As Integer

            Dim myBAAN As String = ""
            Dim rc As Integer
            Dim errorMessage As String = ""
            Dim mailingList As String = ""
            Dim myPath As String = System.Configuration.ConfigurationManager.AppSettings("TMP_PATH")


            Dim deploy As String = myPath
            ' Dim givenConnection As eConnection = givenRDA.Connection
            myRdA.Load(givenRDAId)
            myRdA.getAttribute("RDACode", myBAAN)
            myRdA.getAttribute("Purchasing", mailingList)
            If myBAAN = "" Then
                eJobSchedulerServer.Log.Error("Attention! Baan Code Is Not Set yet. RDA:" & myRdA.Name)
                Exit Function
            End If
            If mailingList.Replace(";", "").Trim = "" Then
                eJobSchedulerServer.Log.Error("Attention! Purchasing eMail list Is empty:" & myRdA.Name)
                Exit Function
            End If
            Dim myBody As String = ""
            Dim mysubject As String = ""
            Dim myFiles() As eBusinessDataFile

            FillData(myRdA, myBody, mysubject, myFiles)
            'Dim myMail As New eMail(givenRDA.Connection)
            Dim toList() As String = mailingList.Trim.Split(";".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
            Dim Subject As String = mysubject & " [" & myRdA.Name & "]"
            Dim Body As String = myBody
            Dim myListaFiles As New Generic.List(Of String)

            If myFiles IsNot Nothing AndAlso myFiles.Length > 0 Then

                Try


                    deploy = IO.Path.Combine(myPath, myRdA.Connection.currentUserId)
                    IO.Directory.CreateDirectory(deploy)

                    For Each file As eBusinessDataFile In myFiles

                        Dim retFile As String
                        file.copyOut(retFile)
                        If retFile <> "" Then
                            Dim myWeb As New Net.WebClient
                            myWeb.DownloadFile(retFile, IO.Path.Combine(deploy, file.Name))
                            If IO.File.Exists(IO.Path.Combine(deploy, file.Name)) Then
                                myListaFiles.Add(IO.Path.Combine(deploy, file.Name))
                            End If
                        End If
                    Next


                Catch ex As Exception

                    eJobSchedulerServer.Log.Error("Error preparing mail to purchasing on RDA:" & myRdA.Name & "->" & ex.Message)
                    'Exit Function
                End Try
            End If

            myFiles = Nothing

            Dim mail As New MailMessage()
            mail.Subject = ""
            mail.Attachments.Clear()
            mail.To.Clear()


            mail.From = New MailAddress(myRdA.Connection.ePLMS_SMTP_MAIL_FROM)
            For Each User As String In toList
                If User & "" <> "" Then
                    mail.To.Add(User)
                End If
            Next
            mail.Subject = Subject
            mail.IsBodyHtml = True
            mail.Body = Body
            If myListaFiles IsNot Nothing AndAlso myListaFiles.Count > 0 Then
                For Each attach As String In myListaFiles.ToArray
                    If attach & "" <> "" Then
                        Dim myBytes() As Byte = IO.File.ReadAllBytes(attach)
                        Dim ms As New MemoryStream(myBytes)
                        mail.Attachments.Add(New Net.Mail.Attachment(ms, IO.Path.GetFileName(attach)))
                    End If
                Next
            End If



            Try
                eJobSchedulerServer.Log.Info("mail Purchasing on rda:" & myRdA.Name)
                eJobSchedulerServer.Log.Info("User ->" & mailingList)
                SmtpServer.Send(mail)
                rc = ePLMS_OK
                eJobSchedulerServer.Log.Info("mail Purchasing sent on rda:" & myRdA.Name)
            Catch ex As Exception
                rc = ePLMS_ERROR
                eJobSchedulerServer.Log.Error("Error sending mail Purchasing on RDA " & myRdA.Name & ":" & ex.Message)

            End Try



            Try
                If deploy <> myPath Then
                    If deploy.ToLower.EndsWith(myRdA.Connection.currentUserId) Then
                        IO.Directory.Delete(deploy, True)
                    End If
                End If
            Catch ex As Exception

            End Try


            If rc = ePLMS_OK Then
                myRdA.changeAttribute("PurchasingeMailSent", "TRUE")

            Else
                myRdA.changeAttribute("PurchasingeMailSent", "FALSE")
            End If


            Try
                GC.SuppressFinalize(mail)
                GC.SuppressFinalize(myFiles)
                GC.SuppressFinalize(myListaFiles)
                'myFiles = Nothing
                'myListaFiles = Nothing
                'mail = Nothing
            Catch ex As Exception

            End Try

            GC.Collect(2)
        End Function


        Private Sub FillData(ByRef givenRdA As eBusinessData, ByRef myMailBody As String, ByRef returnSubject As String, ByRef returnFiles() As eBusinessDataFile)


            Dim name As String = ""
            Dim description As String = ""
            Dim myLevel As String = ""
            Dim BAANCode As String = ""
            Dim supplier As String = ""
            Dim BaanCodeWriter As String = ""
            Dim Purchasing As String = ""
            Dim PurchasingeMailSent As String = ""
            Dim duvri As String = ""

            Dim myMail As String = AppDomain.CurrentDomain.BaseDirectory

            Dim myAttributeList As New StringBuilder

            If myeAttribute Is Nothing Then myeAttribute = New eAttribute(myeConnection)
            Dim createUser As String = ""
            Dim myTemplate As String = "<tr><td style='width:150px;bgcolor:#eeeeee'><b>{0}</b></td><td>{1}<td></tr>"
            Dim remedy As String = " "
            Try

                myMail = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.RDA.PurchasingMailAlert") ' IO.Path.Combine(myMail, "Custom\RDA\Workbench\Purchasing.html")
                myMailBody = IO.File.ReadAllText(myMail)


                myAttributeList.Append("<table width='95%'><tr><th align='left' color='black' colspan='2' bgcolor='#000055'><font size='2pt'><b>Dettaglio RDA</b></font></th></tr>")

                If rdaAttributeList IsNot Nothing Then
                    createUser = givenRdA.createUser

                    createUser = createUser.Replace(".", " ")
                    createUser = (System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(createUser))
                    If myMailBody.Contains("[eCreateUser]") Then
                        myMailBody = myMailBody.Replace("[eCreateUser]", "<b>" & createUser & "</b>")
                    End If
                    If myMailBody.Contains("[eCreateDate]") Then
                        myMailBody = myMailBody.Replace("[eCreateDate]", "<b>" & givenRdA.createDate.Split(" ")(0) & "</b>")
                    End If
                    If myMailBody.Contains("[eDescription]") Then
                        myMailBody = myMailBody.Replace("[eDescription]", "<b>" & givenRdA.Description & "</b>")
                    End If
                    For Each edata As eDataListElement In rdaAttributeList
                        givenRdA.getAttribute(edata.Name, edata.Value)
                        If edata.Name = "Purchasing" Then
                            Purchasing = edata.Value
                        ElseIf edata.Name = "PurchasingeMailSent" Then
                            PurchasingeMailSent = edata.Value
                        ElseIf edata.Name = "RDACode" Then
                            BAANCode = edata.Value
                        ElseIf edata.Name = "BaanCodeWriter" Then
                            BaanCodeWriter = edata.Value
                            edata.Value = edata.Value.Replace(".", " ")
                            edata.Value = (System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(edata.Value))
                            BaanCodeWriter = edata.Value
                        ElseIf edata.Name = "Supplier" Then
                            supplier = edata.Value
                        ElseIf edata.Name = "RequestedSupplier" Then
                            remedy = edata.Value
                            If remedy.ToUpper <> "REMEDY" Then
                                remedy = " "
                            Else
                                remedy = " REMEDY "
                            End If
                        End If
                        If edata.Type = "DATE" Then
                            edata.Value = edata.Value.Split(" ")(0)
                        End If
                        If myMailBody.Contains("[" & edata.Name & "]") Then
                            If edata.Name <> "BaanCodeWriter" Then
                                myMailBody = myMailBody.Replace("[" & edata.Name & "]", edata.Value)
                            End If
                        End If
                        If edata.Value <> "" Then
                            myeAttribute.Load(edata.Key)
                            Dim mMessageLabel As String
                            If givenRdA.Connection.labelCatalog.getText(myeAttribute.labelNumber, mMessageLabel) <> ePLMS_OK Then
                                mMessageLabel = edata.Name
                            End If
                            If edata.Name.ToUpper.StartsWith("Purchasing".ToUpper) Then
                                Continue For
                            End If
                            myAttributeList.Append(String.Format(myTemplate, mMessageLabel, edata.Value))
                        End If
                    Next
                End If

                myAttributeList.Append("</table>")

                Dim myListFiles As New Generic.List(Of eBusinessDataFile)
                givenRdA.getBusinessDataFileList(True, returnFiles)
                If returnFiles IsNot Nothing Then
                    For Each file As eBusinessDataFile In returnFiles
                        If file.Description.ToUpper <> "Configurazione RDA".ToUpper Then
                            myListFiles.Add(file)
                        End If
                    Next
                End If
                If myListFiles.Count > 0 Then
                    returnFiles = myListFiles.ToArray
                Else
                    returnFiles = Nothing
                End If

                If myMailBody.Contains("[BaanCodeWriter]") And BaanCodeWriter = "" And BAANCode <> "" Then
                    myMailBody = myMailBody.Replace("[BaanCodeWriter]", createUser)
                    BaanCodeWriter = createUser
                ElseIf myMailBody.Contains("[BaanCodeWriter]") And BaanCodeWriter <> "" And BAANCode <> "" Then
                    myMailBody = myMailBody.Replace("[BaanCodeWriter]", BaanCodeWriter)
                Else
                    myMailBody = myMailBody.Replace("[BaanCodeWriter]", "-------------")
                End If

                myMailBody = myMailBody.Replace("[EBUSINESSDATAATTRIBUTELIST]", myAttributeList.ToString)
                returnSubject = "Emissione RDA" & remedy & "N. " & IIf(BAANCode <> "", BAANCode, "****")

                If BaanCodeWriter.ToUpper = createUser.ToUpper Then
                    myMailBody = myMailBody.Replace("per conto di <b><b>" & createUser & "</b></b>", "")
                End If


                Try
                    GC.SuppressFinalize(myAttributeList)
                    'myAttributeList = Nothing
                Catch ex As Exception

                End Try

                GC.Collect(2)
            Catch ex As Exception
                eJobSchedulerServer.Log.Error("Error sending mail Purchasing on RDA " & givenRdA.Name & ":" & ex.Message)
            End Try

        End Sub


        Private Function NotifyKoRda(ByRef listOfUser As List(Of String)) As Integer



            Dim myPath As String = System.Configuration.ConfigurationManager.AppSettings("TMP_PATH")
            Dim myPerson As New ePerson(myeConnection)

            If listOfUser IsNot Nothing Then
                For Each user As String In listOfUser
                    Dim mailingList As String = ""
                    If myPerson.Load(user) = ePLMS_OK Then
                        mailingList = myPerson.eMail
                    End If
                    If mailingList = "" Then
                        mailingList = myBasicMailinglist
                    End If
                    Dim stringBuilder As New StringBuilder
                    stringBuilder.AppendLine(myMailAlert)
                    For Each rdaId As Integer In KORDA.Keys
                        If KORDAOwner(rdaId) = user Then
                            Dim message As String = ""
                            myRdA.Load(rdaId)
                            myRdA.getAttribute("BaanIn_ExchangeMessage", message)
                            stringBuilder.AppendLine("  - " & myRdA.Name & ":" & message)
                        End If
                    Next

                    Dim mail As New MailMessage()
                    mail.From = New MailAddress(myeConnection.ePLMS_SMTP_MAIL_FROM)
                    For Each UserMail As String In mailingList.Trim.Split(";".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
                        If UserMail & "" <> "" Then
                            mail.To.Add(UserMail)
                        End If
                    Next
                    mail.Subject = subject
                    mail.IsBodyHtml = True
                    mail.Body = stringBuilder.ToString

                    Try
                        eJobSchedulerServer.Log.Info("mail ko on rda")
                        eJobSchedulerServer.Log.Info("User ->" & mailingList)
                        eJobSchedulerServer.Log.Info("RDA ->" & stringBuilder.ToString)
                        SmtpServer.Send(mail)
                        eJobSchedulerServer.Log.Info("mail sent for ko on rda")
                    Catch ex As Exception
                        eJobSchedulerServer.Log.Info("Error sending mail ko on RDA :" & ex.Message)

                    End Try
                    Try
                        GC.SuppressFinalize(mail)
                        mail = Nothing
                    Catch ex As Exception

                    End Try


                Next
            End If

            Try

            Catch ex As Exception

            End Try



        End Function


        Private Function CloseRdA(ByRef givenRda As eBusinessData)
            Dim rc As Integer
            Dim actionPromote As String = ""

            Dim myeCheck As eCheck
            'With givenRda
            Try
                eJobSchedulerServer.Log.Info("Closing " & givenRda.Name & " Level:" & givenRda.Level)
                For idx As Integer = 0 To 10
                    If givenRda.Level.ToUpper = "Under Control".ToUpper Then
                        eJobSchedulerServer.Log.Info("RDA " & givenRda.Name & " in wf 'under control' transaction.Try to reconnect...")
                        Threading.Thread.Sleep(30000)
                        givenRda.Load(givenRda.Id)
                    End If
                Next

                If givenRda.Level.ToUpper = "CONTROLLED" Then
                    eJobSchedulerServer.Log.Info("Close RDA " & givenRda.Name)
                    givenRda.Unreserve()
                    actionPromote = "Approved"

                    Dim myWorkflowEngine As New eWorkflowEngine(myeConnection, givenRda.Id)

                    With myWorkflowEngine

                        Dim myCurrentTask As workflowTask
                        If Not .isWorkflowEnded Then
                            'Retrieve current task
                            myCurrentTask = Nothing
                            myCurrentTask = .getCurrentWorkflowTask
                            If myCurrentTask IsNot Nothing Then
                                If myCurrentTask.getTaskType().Equals(ePLMS_CHECKTASK) Then
                                    Dim myeCheckTask As workflowCheckTask = DirectCast(myCurrentTask, workflowCheckTask)
                                    If myeCheckTask.getUnmarkedCheckTableAssignableByCurrentUser.Count > 0 Then
                                        For Each myeCheck In myeCheckTask.getUnmarkedCheckTableAssignableByCurrentUser.Values
                                            myCurrentTask.SetCurrentTaskParameter(myeCheck.Name & "_action", actionPromote)
                                            myCurrentTask.SetCurrentTaskParameter(myeCheck.Name & "_comment", "Automatic closoure after baan processing")
                                        Next
                                        '.Connection.pushMessageFlag = True
                                        rc = myCurrentTask.performTask()
                                        'ivenRda.Connection.pushMessageFlag = False
                                        If rc <> ePLMS_OK Then
                                            eJobSchedulerServer.Log.Error(myeConnection.errorMessage)
                                        Else
                                            eJobSchedulerServer.Log.Info("RdA Closed")
                                            'givenRda
                                            eJobSchedulerServer.Log.Info("Closing worklfow prefix->" & givenRda.Name)
                                            GetMemory()
                                            System.Threading.Thread.Sleep(500)
                                            RDAWorkflowTaskSuffix(givenRda, myCurrentTask)
                                            System.Threading.Thread.Sleep(500)
                                            GetMemory()
                                            eJobSchedulerServer.Log.Info("Closing worklfow suffix->" & givenRda.Name)
                                            givenRda.Load(givenRda.Id)
                                        End If

                                    Else

                                        eJobSchedulerServer.Log.Info("RDA under other user control")
                                    End If


                                End If
                            Else
                                eJobSchedulerServer.Log.Error("Error retrieve your check visibility, please contact administrator")
                                rc = ePLMS_ERROR
                            End If


                        End If
                    End With
                ElseIf givenRda.Level.ToUpper = "UNDER CONTROL" Then
                    eJobSchedulerServer.Log.Info("RDA under control and force closoure")
                    Dim myWorkflowEngine As New eWorkflowEngine(myeConnection, givenRda.Id)
                    With myWorkflowEngine
                        If Not .isWorkflowEnded Then
                            If myWorkflowEngine.reassignLevel("Closed", "Automatic closoure after baan processing", True) <> ePLMS_OK Then
                                Throw New Exception(myeConnection.errorMessage)
                            End If
                        End If
                    End With
                    myWorkflowEngine = New eWorkflowEngine(myeConnection, givenRda.Id)
                    With myWorkflowEngine
                        Dim myCurrentTask As workflowTask
                        If Not .isWorkflowEnded Then
                            'Retrieve current task
                            myCurrentTask = Nothing
                            myCurrentTask = .getCurrentWorkflowTask
                            If myCurrentTask IsNot Nothing Then
                                myCurrentTask.performTask()
                            End If
                        End If
                    End With
                    givenRda.Load(givenRda.Id)
                End If
            Catch ex As Exception
                eJobSchedulerServer.Log.Error("Error closing rda. " & ex.ToString)
                rc = ePLMS_ERROR
            End Try
            eJobSchedulerServer.Log.Info("Closing result->" & givenRda.Name & " Level:" & givenRda.Level)

            'End With

        End Function


        Public Function RDAWorkflowTaskSuffix_suppressed(ByRef givenBusinessData As eBusinessData, ByRef givenWorkFlowTask As workflowTask) As Integer

            Dim logoutFlag As Boolean = False

            Try
                If givenWorkFlowTask.getTaskType().Equals(ePLMS_CHECKTASK) Then


                    If givenWorkFlowTask.currentTaskParameters IsNot Nothing AndAlso givenBusinessData.businessDataType = "RDA" Then
                        If givenWorkFlowTask.currentTaskParameters.Count > 0 Then

                            If mydummyBD Is Nothing Then mydummyBD = New eBusinessData(myeConnection)
                            With mydummyBD
                                If .Load(givenBusinessData.Id) = ePLMS_OK Then
                                    .Reserve()
                                    Try
                                        .changeAttribute("ActionDate", Now.ToString("dd.MM.yyyy"))
                                        .changeAttribute("ActionOperation", "")
                                        .changeAttribute("ActionComment", "")
                                        For Each key As String In givenWorkFlowTask.currentTaskParameters.Keys
                                            If key.ToLower.EndsWith("_action") Then
                                                .changeAttribute("ActionOperation", givenWorkFlowTask.currentTaskParameters(key))
                                            ElseIf key.ToLower.EndsWith("_comment") Then
                                                .changeAttribute("ActionComment", givenWorkFlowTask.currentTaskParameters(key))
                                            End If
                                        Next
                                    Catch ex As Exception
                                        eJobSchedulerServer.Log.Error(ex.ToString)
                                    End Try

                                    .Unreserve()
                                End If
                            End With

                        End If
                    End If
                End If
            Catch ex1 As Exception
                eJobSchedulerServer.Log.Error(ex1.ToString)
            Finally


            End Try

            Return ePLMS_OK
        End Function


        Public Function RDAWorkflowTaskSuffix(ByRef givenBusinessData As eBusinessData, ByRef givenWorkFlowTask As workflowTask) As Integer

            Dim logoutFlag As Boolean = False

            Try
                If givenWorkFlowTask.getTaskType().Equals(ePLMS_CHECKTASK) Then


                    If givenWorkFlowTask.currentTaskParameters IsNot Nothing AndAlso givenBusinessData.businessDataType = "RDA" Then
                        If givenWorkFlowTask.currentTaskParameters.Count > 0 Then

                            Try
                                givenBusinessData.changeAttribute("ActionDate", Now.ToString("dd.MM.yyyy"))
                                givenBusinessData.changeAttribute("ActionOperation", "")
                                givenBusinessData.changeAttribute("ActionComment", "")
                                For Each key As String In givenWorkFlowTask.currentTaskParameters.Keys
                                    If key.ToLower.EndsWith("_action") Then
                                        givenBusinessData.changeAttribute("ActionOperation", givenWorkFlowTask.currentTaskParameters(key))
                                    ElseIf key.ToLower.EndsWith("_comment") Then
                                        givenBusinessData.changeAttribute("ActionComment", givenWorkFlowTask.currentTaskParameters(key))
                                    End If
                                Next
                            Catch ex As Exception
                                eJobSchedulerServer.Log.Error(ex.ToString)
                            End Try

                            'If mydummyBD Is Nothing Then mydummyBD = New eBusinessData(myeConnection)
                            'With mydummyBD
                            '    If .Load(givenBusinessData.Id) = ePLMS_OK Then
                            '        .Reserve()
                            '        Try
                            '            .changeAttribute("ActionDate", Now.ToString("dd.MM.yyyy"))
                            '            .changeAttribute("ActionOperation", "")
                            '            .changeAttribute("ActionComment", "")
                            '            For Each key As String In givenWorkFlowTask.currentTaskParameters.Keys
                            '                If key.ToLower.EndsWith("_action") Then
                            '                    .changeAttribute("ActionOperation", givenWorkFlowTask.currentTaskParameters(key))
                            '                ElseIf key.ToLower.EndsWith("_comment") Then
                            '                    .changeAttribute("ActionComment", givenWorkFlowTask.currentTaskParameters(key))
                            '                End If
                            '            Next
                            '        Catch ex As Exception
                            '            eJobSchedulerServer.Log.Error(ex.ToString)
                            '        End Try

                            '        .Unreserve()
                            '    End If
                            'End With

                        End If
                    End If
                End If
            Catch ex1 As Exception
                eJobSchedulerServer.Log.Error(ex1.ToString)
            Finally


            End Try

            Return ePLMS_OK
        End Function


#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: dispose managed state (managed objects).
                End If

                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TODO: set large fields to null.
            End If
            disposedValue = True
        End Sub

        ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
        'Protected Overrides Sub Finalize()
        '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            Dispose(True)
            ' TODO: uncomment the following line if Finalize() is overridden above.
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace
