﻿Module Module1

    Sub Main(args() As String)
        Try
            Parallaksis.eJobScheduler.eJobSchedulerServer.Start()
            Dim myCurretnJob As New Parallaksis.eJobScheduler.Jobs.ePLMSRDAConnectionJob()
            myCurretnJob.OpenConnection()
            Try
                Try
                    myCurretnJob.BaaNMQExecute()
                Catch ex As Exception
                    Parallaksis.eJobScheduler.eJobSchedulerServer.Log.Error(ex.ToString)
                End Try
                myCurretnJob.CloseConnection()
            Catch ex As Exception

            End Try

        Catch ex As Exception
            Parallaksis.eJobScheduler.eJobSchedulerServer.Log.Error(String.Format("BAAN.RDA.MQ:" & ex.ToString))
        End Try

    End Sub

End Module
