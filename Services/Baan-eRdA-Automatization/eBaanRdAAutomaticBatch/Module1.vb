﻿Imports eRdAAutomatizationServiceBatch.Parallaksis.eJobScheduler
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.IO

Module Module1
    Dim myLogPath As String = ""
    Sub Main(args() As String)
        'Threading.Thread.Sleep(10000)
        myLogPath = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.ePLMS_LogPath")
        Try
            IO.Directory.CreateDirectory(myLogPath)
        Catch ex As Exception

        End Try
        eJobSchedulerServer.Start(myLogPath)
        eJobSchedulerServer.Log.Info("*/Config Paramenters->")
        For Each arg As String In args
            eJobSchedulerServer.Log.Info(arg)
        Next
        eJobSchedulerServer.Log.Info("<- Config Paramenters/*")

        Dim myJob = New Parallaksis.eJobScheduler.Jobs.ePLMSRDAConnectionJob()
        Try
            myJob.Execute()
        Catch ex As Exception
            eJobSchedulerServer.Log.Error(String.Format("eRdAAutomatizationServiceBatch: '{0}' ", ex.ToString))
        End Try

        eJobSchedulerServer.Stop()
        Environment.Exit(0)
    End Sub


End Module
