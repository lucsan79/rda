﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.Collections
Imports System.IO
Imports System.Net.Mail

Namespace Parallaksis.eJobScheduler.Jobs

    Public Class ePLMSRDAConnectionJob
        Dim _connection As eConnection = Nothing

        Public myLogEntryNameSupplier As String = "BAAN.RDA.MQ.SUPPLIER"
        Public myLogEntryNamePriceList As String = "BAAN.RDA.MQ.SUPPLIER"

        Public ePLMS_HOME As String = ""
        Public ePLMS_LogPath As String = ""
        Public MailAlertPort As String = ""
        Public MailAlertHost As String = ""
        Public MailAlertTimeout As String = ""
        Public SUPPLIER_CMD_EXE As String = ""
        Public SUPPLIER_CMD_ARGS As String = ""
        Public SUPPLIER_NOTIFICATION As String = ""
        Public PRICELIST_CMD_EXE As String = ""
        Public PRICELIST_CMD_ARGS As String = ""
        Public PRICELIST_NOTIFICATION As String = ""
        Public BAAN_ERROR_MAP As String = ""
        Public BAAN_TMP_PATH As String = ""
        Public SmtpServer As SmtpClient
        Dim rxcProcess As Process
        Dim rxcProcessInfo As ProcessStartInfo
        Dim myTmpPath As DirectoryInfo

#Region "START"
        Public Enum taskType
            supplier
            pricelist
        End Enum
        Public Sub Execute()
            Try
                OpenConnection()
                Try
                    PerformSupplierTask()
                    SendMail(taskType.supplier)
                Catch ex As Exception
                    eJobSchedulerServer.Log.Error("PerformSupplierTask:" & ex.Message)
                End Try

                Try
                    PerformPriceListTask()
                    SendMail(taskType.pricelist)
                Catch ex As Exception
                    eJobSchedulerServer.Log.Error("PerformPriceListTask:" & ex.Message)
                End Try

                CloseConnection()
            Catch ex As Exception
                CloseConnection()
                Throw ex
            End Try
        End Sub
#End Region

#Region "tracciato fornitori"
        'CODICE FORNITORE	        CHAR(6)		    tccom020.suno
        'DESCRIZIONE FORNITORE	    CHAR(35)		tccom020.nama
        'INDIRIZZO	                CHAR(30)		tccom020.namc
        'CITTA'	                    CHAR(30)		tccom020.name
        'PROVINCIA	                CHAR(30)		tccom020.namf
        'CAP	                    CHAR(10)		tccom020.pstc
        'NAZIONE	                CHAR(3)		    tccom020.ccty
        'BUYER	                    NUM		        tccom020.ccon
        'IS REMEDY	                NUM	            "Questo campo può assumere i seguenti valori:
        '                                           1: true, se esiste un contratto Remedy con stato libero o attivo
        '                                           2: false, altrimenti"	
        'FORNITORE CONSOLIDATO	    NUM	            "Questo campo può assumere i seguenti valori:
        '                                           1: true, se il fornitore risulta consolidato per l'ente R&D
        '                                           2: false, altrimenti"	
        'DATA INIZIO CONSOLIDAMENTO	DATE	        formato ddmmyyyy	abpur581.sdat
        'DATA FINE CONSOLIDAMENTO	DATE	        formato ddmmyyyy - potrebbe non essere valorizzata	abpur581.edat
#End Region
        Public Sub PerformSupplierTask()
            Dim fileLines() As String
            Dim codicefornitore As String = ""
            Dim ragionesociale As String = ""
            Dim indirizzo As String = ""
            Dim citta As String = ""
            Dim provincia As String = ""
            Dim cap As String = ""
            Dim nazione As String = ""
            Dim buyer As String = ""
            Dim Consolidamento As String = ""
            Dim ConsolidamentoFrom As String = ""
            Dim ConsolidamentoTo As String = ""

            Dim isremedy As String = ""
            Dim searchKEY As String = ""
            Dim update As String = "UPDATE [dbo].[FORNITORI] SET [NAME] = '{1}', [ADDRESS] = '{2}', [CITY] = '{3}', [ZIPCODE] = '{4}', [CODE] = '{5}', [BUYER] = '{6}', [IsRemedy] = {7},[ModifyDate] = GETDATE(),[Consolidamento] = '{8}',[ConsolidamentoFrom] = CAST('{9}' AS DATETIME),[ConsolidamentoTo] = CAST('{10}' AS DATETIME) WHERE ID={0}"
            Dim insert As String = "INSERT INTO [dbo].[FORNITORI] ([NAME],[ADDRESS],[CITY],[ZIPCODE],[CODE],[BUYER],[IsRemedy],[ModifyDate],[Consolidamento],[ConsolidamentoFrom],[ConsolidamentoTo]) VALUES ('{1}','{2}','{3}','{4}','{5}','{6}',{7},GETDATE(),{8},CAST('{9}' AS DATETIME),CAST('{10}' AS DATETIME))"

            Dim executeQuery As String = ""
            Dim givenFile As String = IO.Path.Combine(myTmpPath.FullName, "Supplier-" & Now.ToString("yyyyMMddHHmmss") & ".txt")
            PerformBaaNAction(SUPPLIER_CMD_EXE, SUPPLIER_CMD_ARGS, givenFile)
            If System.IO.File.Exists(givenFile) Then
                fileLines = IO.File.ReadAllLines(givenFile)
                If fileLines.Length > 0 Then
                    Dim supplierTable As DataTable = ExecuteSql("SELECT *,REPLACE(code, ' ', '') as searchkey FROM dbo.FORNITORI")
                    Dim index As Int32 = 0
                    For Each line In fileLines
                        index += 1
                        line = line.Replace("?", "")
                        line = line.Replace("�", " ")
                        Dim lineData() = line.Split("|")
                        If lineData.Length <> 12 Then
                            eJobSchedulerServer.Log.Error("Bad row lenght #" & index & ": " & line)
                            Continue For
                        End If
                        codicefornitore = lineData(0).Trim
                        ragionesociale = lineData(1).Trim.Replace("'", "''")
                        indirizzo = lineData(2).Trim.Replace("'", "''")
                        citta = lineData(3).Trim.Replace("'", "''")
                        provincia = lineData(4).Trim.Replace("'", "''")
                        cap = lineData(5).Trim
                        nazione = lineData(6).Trim
                        buyer = lineData(7).Trim
                        isremedy = lineData(8).Trim
                        Consolidamento = lineData(9).Trim
                        If Not String.IsNullOrEmpty(Consolidamento) Then
                            Consolidamento = IIf(Consolidamento = "1", "1", "0")
                        Else
                            Consolidamento = 0
                        End If

                        ConsolidamentoFrom = lineData(10).Trim
                        If Not String.IsNullOrEmpty(ConsolidamentoFrom) Then
                            Dim datenew As New DateTime(ConsolidamentoFrom.Substring(4, 4), ConsolidamentoFrom.Substring(2, 2), ConsolidamentoFrom.Substring(0, 2))
                            ConsolidamentoFrom = datenew.ToString("yyyyMMdd")
                        End If
                        ConsolidamentoTo = lineData(11).Trim
                        If Not String.IsNullOrEmpty(ConsolidamentoTo) Then
                            Dim datenew As New DateTime(ConsolidamentoTo.Substring(4, 4), ConsolidamentoTo.Substring(2, 2), ConsolidamentoTo.Substring(0, 2))
                            ConsolidamentoTo = datenew.ToString("yyyyMMdd")
                        End If
                        If String.IsNullOrEmpty(ConsolidamentoTo) Then
                            ConsolidamentoTo = "20991231"
                        End If

                        searchKEY = codicefornitore.Replace(" ", "")
                        ragionesociale = ragionesociale & "    cod.forn. [" & searchKEY & "]"
                        If Not String.IsNullOrEmpty(isremedy) Then
                            isremedy = IIf(isremedy = "1", "1", "0")
                        Else
                            isremedy = "0"
                        End If
                        Dim supplierRow() As DataRow = supplierTable.Select("searchkey='" & searchKEY & "'")
                        executeQuery = insert
                        Dim id = ""
                        If supplierRow.Length > 0 Then
                            id = supplierRow(0)("ID")
                            executeQuery = update
                        End If

                        executeQuery = String.Format(executeQuery, id, ragionesociale, indirizzo, citta, cap, codicefornitore, buyer, isremedy, Consolidamento, ConsolidamentoFrom, ConsolidamentoTo)
                        eJobSchedulerServer.Log.Info("Row #" & index & ": " & executeQuery)
                        Try
                            ExecuteSql(executeQuery)
                        Catch ex As Exception
                            eJobSchedulerServer.Log.Error("Row #" & index & ": " & ex.Message)
                        End Try


                    Next
                Else
                    eJobSchedulerServer.Log.Error("No rows to read for supplier list")
                End If
            End If


        End Sub
#Region "tracciato listini"
        'CONTRATTO REMEDY	            NUM	 	        abpur510.reid
        'DESCRIZIONE CONTRATTO	        CHAR(30)	 	abpur510.dsca
        'CODICE FORNITORE	            CHAR(6)	 	    abpur510.suno
        'RAGIONE SOCIALE	            CHAR(35)		tccom020.nama
        'STATO CONTRATTO	            NUM	            "Questo campo potrà assumere i seguenti valori:
        '                                               - 1: Libero
        '                                               - 2: Attivo
        '                                               - 3: Chiuso"	abpur510.stat
        'DATA ATTIVAZIONE CONTRATTO	    DATE	        formato ddmmyyyy	abpur510.sdat
        'DATA SCADENZA CONTRATTO	    DATE	        formato ddmmyyyy - potrebbe non essere valorizzata	abpur510.edat
        'POSIZIONE	                    NUM	 	        abpur512.pono
        'SEQUENZA PREZZO	            NUM		        abpur512.seqn
        'ARTICOLO	                    CHAR(16)	 	abpur512.item
        'DESCRIZIONE ARTICOLO	        CHAR(30)	 	tiitm001.dsca
        'PREZZO	                        DOUBLE	 	    abpur512.pric
        'DIVISA	                        CHAR(3)		    abpur512.ccur
        'DESCRIZIONE DIVISA	            CHAR(30)		tcmcs002.dsca
        'STATO RIGA	                    NUM	            "Questo campo potrà assumere i seguenti valori:
        '                                               - 1: Libero
        '                                               - 2: Attivo
        '                                               - 3: Chiuso"	abpur512.stat
        'DATA INIZIO VALIDITA'	        DATE	        formato ddmmyyyy	abpur512.dati
        'DATA FINE VALIDITA'	        DATE	        formato ddmmyyyy - potrebbe non essere valorizzata	abpur512.datf
#End Region
        Public Sub PerformPriceListTask()

            Dim fileLines() As String
            Dim ID_Contratto As String = ""
            Dim Fornitore As String = ""
            Dim Ragione_Sociale As String = ""
            Dim Stato As String = ""
            Dim Data_Attiviazione As String = ""
            Dim Data_Scadenza As String = ""
            Dim Valore_Massimo_Contratto As String = ""
            Dim Valore_Residuo_Contratto As String = ""
            Dim Pos As String = ""
            Dim Articolo As String = ""
            Dim Descrizione As String = ""
            Dim Prezzo As String = ""
            Dim Nuovo_Prezzo As String = ""
            Dim Divisa As String = ""
            Dim Descrizione_Divisa As String = ""
            Dim Stato_Riga As String = ""
            Dim Seq As String = ""
            Dim Data_Inizio_Validita As String = ""
            Dim Data_Fine_Validita As String = ""
            Dim searchKEY As String = ""
            Dim update As String = "UPDATE      [dbo].[LISTINO_FORNITORI] SET [ID Contratto] = {1}, [Fornitore] = '{2}',[Ragione Sociale] = '{3}',  [Stato] = '{4}',[Data Attiviazione] = '{5}',[Data Scadenza] = '{6}',[Valore Massimo Contratto] = {7},[Valore Residuo Contratto] = {8},  [Pos] = {9},[Articolo] = '{10}',[Descrizione] = '{11}', [Prezzo] = {12},[Nuovo Prezzo] = {13},  [Divisa] = '{14}',  [Descrizione Divisa] = '{15}',  [Stato Riga] = '{16}',  [Seq] = {17},   [Data Inizio Validità] = '{18}',[Data Fine Validità] = '{19}', [ModifyDate] = GETDATE() WHERE ID={0}"
            Dim insert As String = "INSERT INTO [dbo].[LISTINO_FORNITORI]    ([ID Contratto],       [Fornitore],        [Ragione Sociale],          [Stato],        [Data Attiviazione],        [Data Scadenza],        [Valore Massimo Contratto],      [Valore Residuo Contratto],        [Pos],      [Articolo],         [Descrizione],          [Prezzo],       [Nuovo Prezzo],         [Divisa],           [Descrizione Divisa],           [Stato Riga],           [Seq],          [Data Inizio Validità],         [Data Fine Validità], [Descrizione Completa],[ModifyDate]) VALUES({1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','',GETDATE())"

            Dim executeQuery As String = ""
            Dim givenFile As String = IO.Path.Combine(myTmpPath.FullName, "PriceList-" & Now.ToString("yyyyMMddHHmmss") & ".txt")
            PerformBaaNAction(PRICELIST_CMD_EXE, PRICELIST_CMD_ARGS, givenFile)
            If System.IO.File.Exists(givenFile) Then
                fileLines = IO.File.ReadAllLines(givenFile)
                If fileLines.Length > 0 Then
                    Dim priceListTable As DataTable = ExecuteSql("SELECT *,REPLACE(Fornitore, ' ', '') as searchkey FROM dbo.LISTINO_FORNITORI")
                    Dim index As Int32 = 0
                    For Each line In fileLines
                        index += 1
                        Dim lineData() = line.Split("|")

                        If lineData.Length <> 17 Then
                            eJobSchedulerServer.Log.Error("Bad row lenght #" & index & ": " & line)
                            Continue For
                        End If
                        ID_Contratto = lineData(0).Trim
                        Fornitore = lineData(2).Trim
                        Ragione_Sociale = lineData(3).Trim
                        Stato = lineData(4).Trim
                        If Stato = "1" Then
                            Stato = "libero"
                        ElseIf Stato = "2" Then
                            Stato = "attivo"
                        ElseIf Stato = "3" Then
                            Stato = "chiuso"
                        End If

                        Data_Attiviazione = lineData(5).Trim
                        Data_Scadenza = lineData(6).Trim
                        Valore_Massimo_Contratto = 0
                        Valore_Residuo_Contratto = 0
                        Pos = lineData(7).Trim
                        Seq = lineData(8).Trim
                        Articolo = lineData(9).Trim
                        Descrizione = lineData(10).Trim
                        Prezzo = lineData(11).Trim
                        Nuovo_Prezzo = 0
                        Divisa = lineData(12).Trim
                        Descrizione_Divisa = lineData(13).Trim
                        Stato_Riga = lineData(14).Trim
                        If Stato_Riga = "1" Then
                            Stato_Riga = "libero"
                        ElseIf Stato_Riga = "2" Then
                            Stato_Riga = "attivo"
                        ElseIf Stato_Riga = "3" Then
                            Stato_Riga = "chiuso"
                        End If

                        Data_Inizio_Validita = lineData(15).Trim
                        Data_Fine_Validita = lineData(16).Trim

                        searchKEY = Fornitore.Replace(" ", "")
                        If Not String.IsNullOrEmpty(Data_Attiviazione) AndAlso Data_Attiviazione.Length = 8 Then
                            Data_Attiviazione = String.Format("{0}/{1}/{2}", Data_Attiviazione.Substring(0, 2), Data_Attiviazione.Substring(2, 2), Data_Attiviazione.Substring(4, 4))
                        Else
                            Data_Attiviazione = ""
                        End If
                        If Not String.IsNullOrEmpty(Data_Scadenza) AndAlso Data_Scadenza.Length = 8 Then
                            Data_Scadenza = String.Format("{0}/{1}/{2}", Data_Scadenza.Substring(0, 2), Data_Scadenza.Substring(2, 2), Data_Scadenza.Substring(4, 4))
                        Else
                            Data_Scadenza = ""
                        End If
                        If String.IsNullOrEmpty(Data_Inizio_Validita) OrElse Data_Inizio_Validita.Length <> 8 Then
                            eJobSchedulerServer.Log.Error("Bad value for Data_Inizio_Validita #" & index & ": " & line)
                            Continue For
                        Else
                            Data_Inizio_Validita = String.Format("{0}/{1}/{2}", Data_Inizio_Validita.Substring(0, 2), Data_Inizio_Validita.Substring(2, 2), Data_Inizio_Validita.Substring(4, 4))
                        End If

                        If String.IsNullOrEmpty(Data_Fine_Validita) OrElse Data_Fine_Validita.Length <> 8 Then
                            eJobSchedulerServer.Log.Error("Bad value for Data_Fine_Validita #" & index & ": " & line)
                            Continue For
                        Else
                            Data_Fine_Validita = String.Format("{0}/{1}/{2}", Data_Fine_Validita.Substring(0, 2), Data_Fine_Validita.Substring(2, 2), Data_Fine_Validita.Substring(4, 4))
                        End If
                        'Dim priceRow() As DataRow = priceListTable.Select("searchkey='" & searchKEY & "' and [ID Contratto]='" & ID_Contratto & "' and articolo='" & Articolo & "' and Descrizione='" & Descrizione & "' and pos=" & Pos)
                        Dim priceRow() As DataRow = priceListTable.Select("searchkey='" & searchKEY & "' and [ID Contratto]='" & ID_Contratto & "' and articolo='" & Articolo & "'  and pos=" & Pos & " and Seq=" & Seq)
                        executeQuery = insert
                        Dim id = ""
                        If priceRow.Length > 0 Then
                            id = priceRow(0)("ID")
                            executeQuery = update
                        End If
                        executeQuery = String.Format(executeQuery, id, ID_Contratto, Fornitore, Ragione_Sociale, Stato, Data_Attiviazione, Data_Scadenza, Valore_Massimo_Contratto, Valore_Residuo_Contratto, Pos, Articolo, Descrizione, Prezzo, Nuovo_Prezzo, Divisa, Descrizione_Divisa, Stato_Riga, Seq, Data_Inizio_Validita, Data_Fine_Validita)

                        eJobSchedulerServer.Log.Info("Row #" & index & ": " & executeQuery)
                        Try
                            ExecuteSql(executeQuery)
                        Catch ex As Exception
                            eJobSchedulerServer.Log.Error("Row #" & index & ": " & ex.Message)
                        End Try


                    Next
                Else
                    eJobSchedulerServer.Log.Error("No rows to read for supplier list")
                End If
            End If


        End Sub
        Public Sub PerformBaaNAction(ByRef Process_Name As String, ByRef Process_Arguments As String, ByRef givenFile As String)

            Dim Read_Output As Boolean = False
            Dim Process_Hide As Boolean = True
            Dim Process_TimeOut As Integer = 999999999

            Dim currentProcess_Arguments As String = String.Format("{0} > {1}", Process_Arguments, IO.Path.GetFileName(givenFile))
            Dim myBatContent As String = String.Format("""{0}"" {1}", Process_Name, currentProcess_Arguments)
            Dim myFileBat As String = givenFile.Replace(IO.Path.GetExtension(givenFile), ".bat")
            IO.File.WriteAllText(myFileBat, myBatContent)

            Dim ERRORLEVEL As Integer
            Try
                If rxcProcess Is Nothing Then
                    rxcProcess = New Process
                End If
                If rxcProcessInfo Is Nothing Then
                    rxcProcessInfo = New ProcessStartInfo
                End If
                rxcProcess.EnableRaisingEvents = True
                rxcProcessInfo.FileName = myFileBat
                rxcProcessInfo.Arguments = Nothing
                rxcProcessInfo.CreateNoWindow = Process_Hide
                rxcProcessInfo.UseShellExecute = True
                rxcProcessInfo.RedirectStandardOutput = Read_Output
                rxcProcessInfo.RedirectStandardError = Read_Output
                rxcProcessInfo.WorkingDirectory = IO.Path.GetDirectoryName(givenFile)
                rxcProcess.StartInfo = rxcProcessInfo

                eJobSchedulerServer.Log.Info("cmd sended to BAAN")
                eJobSchedulerServer.Log.Info("cmd: " & myBatContent)
                rxcProcess.Start()

                rxcProcess.WaitForExit(Process_TimeOut)

                ERRORLEVEL = rxcProcess.ExitCode
                If Not ERRORLEVEL = 0 Then
                    Dim message As String = String.Format("ExitCode: {0}, Message: {1}", ERRORLEVEL, GetBaanMessageError(ERRORLEVEL))
                    Throw New Exception(message)
                End If

                If Read_Output = True Then
                    Dim Process_ErrorOutput As String = rxcProcess.StandardOutput.ReadToEnd()
                    Dim Process_StandardOutput As String = rxcProcess.StandardOutput.ReadToEnd()
                    If Process_ErrorOutput IsNot Nothing Then
                        Throw New Exception(Process_ErrorOutput)
                    End If
                End If

                rxcProcess.Close()
                GC.WaitForPendingFinalizers()
                GC.Collect(2)
            Catch ex As Exception
                givenFile = ""
                eJobSchedulerServer.Log.Error("Error in " & Process_Name & ":" & ex.Message)
                eJobSchedulerServer.Log.Error("Full description-->")
                eJobSchedulerServer.Log.Error(ex.ToString)
                eJobSchedulerServer.Log.Error("<-- Full description")
            End Try
        End Sub

#Region "CONNECTION / CONFIG"
        Public Sub OpenConnection()
            Dim rc As Integer = 0

            ePLMS_HOME = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.ePLMS_HOME")
            ePLMS_LogPath = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.ePLMS_LogPath")
            MailAlertPort = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Por")
            MailAlertHost = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Host")
            MailAlertTimeout = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Timeout")

            SUPPLIER_CMD_EXE = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ-SUPPLIER.CMD.EXE")
            SUPPLIER_CMD_ARGS = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ-SUPPLIER.CMD.ARGS")
            SUPPLIER_NOTIFICATION = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.SUPPLIER.NOTIFICATION")

            PRICELIST_CMD_EXE = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ-PRICELIST.CMD.EXE")
            PRICELIST_CMD_ARGS = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ-PRICELIST.CMD.ARGS")
            PRICELIST_NOTIFICATION = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.PRICELIST.NOTIFICATION")
            BAAN_ERROR_MAP = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.BAAN_ERROR_MAP")

            BAAN_TMP_PATH = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.ePLMS_TMP_PATH")



            myTmpPath = New DirectoryInfo(BAAN_TMP_PATH)
            If Not myTmpPath.Exists Then
                Throw New Exception("Error! Tmp Path not Found")
            End If
            If System.IO.Directory.Exists(ePLMS_LogPath) Then
                eJobSchedulerServer.Start(ePLMS_LogPath)
            Else
                Throw New Exception("Error! Log Path not Found")
            End If


            If (String.IsNullOrEmpty(SUPPLIER_CMD_EXE) And String.IsNullOrEmpty(SUPPLIER_CMD_EXE)) Or
               (String.IsNullOrEmpty(PRICELIST_CMD_EXE) And String.IsNullOrEmpty(PRICELIST_CMD_ARGS)) Then
                eJobSchedulerServer.Log.Error("Error!Empty parameters to invoke BaaN MQ")
                Throw New Exception("Error!Empty parameters to invoke BaaN MQ")
            End If

            _connection = New eConnection(ePLMS_HOME)
            rc = _connection.internalDbLogIn("eplms")
            If rc <> 0 Then
                Throw New Exception([String].Format("Error in internal login:{0}", _connection.errorMessage))
            End If

            SmtpServer = New SmtpClient()
            SmtpServer.Host = _connection.ePLMS_SMTP_SERVER
            SmtpServer.Port = _connection.ePLMS_SMTP_PORT
        End Sub
        Friend Sub CloseConnection()

            Try
                _connection.logOut()

            Catch ex As Exception

            End Try
            _connection = Nothing
        End Sub
        Private Function GetBaanMessageError(givenCode As Integer) As String
            If IO.File.Exists(BAAN_ERROR_MAP) Then
                Dim myLines() As String = IO.File.ReadAllLines(BAAN_ERROR_MAP)
                For Each line As String In myLines
                    Dim code As Integer = line.Split(":")(0)
                    Dim message As String = line.Split(":")(1)
                    If code = givenCode Then
                        Return message
                    End If
                Next

            End If
            Return ""
        End Function
        Private Function ExecuteSql(givenSql As String) As DataTable
            Dim myTable As New DataTable
            Dim myFind As New eFind(_connection)
            myFind.queryString = "$(" + givenSql + ")"
            myFind.Prepare(Nothing)
            myFind.Execute(myTable)
            Return myTable
        End Function
        Private Sub SendMail(givenTaskType As taskType)
            Dim subject As String = ""
            Dim bodyMail As String = ""
            Dim mailinglist As String = ""

            If givenTaskType = taskType.supplier Then
                subject = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.SUPPLIER.NOTIFICATION_SUBJECT")
                bodyMail = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.SUPPLIER.NOTIFICATION_BODY")
                mailinglist = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.SUPPLIER.NOTIFICATION")
            ElseIf givenTaskType = taskType.pricelist Then
                subject = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.PRICELIST.NOTIFICATION_SUBJECT")
                bodyMail = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.PRICELIST.NOTIFICATION_BODY")
                mailinglist = "" & System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.GETBAANMQ.PRICELIST.NOTIFICATION")

            End If
            If Not String.IsNullOrEmpty(subject) AndAlso Not String.IsNullOrEmpty(bodyMail) Then
                Dim myMail As New eMail(_connection)
                myMail.Subject = subject
                myMail.Body = bodyMail
                Dim toList As New List(Of String)
                If Not String.IsNullOrEmpty(mailinglist) Then
                    Dim myGroup As New eGroup(_connection)
                    If myGroup.Load(mailinglist) = ePLMS_OK Then
                        Dim myUserList As New ArrayList
                        Dim myPersonList As New ArrayList
                        myGroup.getUserList(myUserList)
                        If myUserList IsNot Nothing AndAlso myUserList.Count > 0 Then
                            Dim myUser As New eUser(_connection)
                            For Each edata As eDataListElement In myUserList
                                If myUser.Load(edata.Name) = ePLMS_OK Then
                                    myUser.getPersonList(myPersonList)
                                    If myPersonList IsNot Nothing AndAlso myPersonList.Count > 0 Then
                                        Dim myPerson As New ePerson(_connection)
                                        For Each person As eDataListElement In myPersonList
                                            If myPerson.Load(person.Name) = ePLMS_OK Then
                                                toList.Add(myPerson.eMail)
                                            End If
                                        Next
                                    End If
                                End If
                            Next
                        End If
                    End If

                End If
                If toList.Count > 0 Then
                    myMail.ToList = toList.ToArray
                    myMail.Send()
                End If
            End If


        End Sub

#End Region
    End Class
End Namespace
