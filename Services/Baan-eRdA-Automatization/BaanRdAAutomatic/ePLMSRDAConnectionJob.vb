﻿Imports Quartz
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports Parallaksis.eJobScheduler
Imports Parallaksis.ePLMS33
Imports Parallaksis.ePLMS33.Defs
Imports System.Collections
Imports System.IO
Imports System.Net.Mail

Namespace Parallaksis.eJobScheduler.Jobs

    Public Class ePLMSRDAConnectionJob



        Private Sub sendMail(ByVal message As String)
            Try
                Dim objeto_mail As MailMessage = New MailMessage
                Dim client As SmtpClient = New SmtpClient
                client.Port = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Port")
                client.Host = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Host")
                client.Timeout = 10000
                client.DeliveryMethod = SmtpDeliveryMethod.Network
                client.UseDefaultCredentials = True
                Dim mailFrom As String = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.From")
                objeto_mail.From = New MailAddress(mailFrom)
                Dim myTo As String = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.To")
                For Each emailTo As String In myTo.Split(";".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
                    objeto_mail.To.Add(New MailAddress(emailTo))
                Next
                objeto_mail.Subject = System.Configuration.ConfigurationManager.AppSettings("ePLMS.ePLMSRDAConnectionJob.MailAlert.Subject")
                objeto_mail.Body = message
                client.Send(objeto_mail)
            Catch e As Exception
                eJobSchedulerServer.Log.Error(String.Format(_serviceName & "- Error sending mail:{0}", e.Message))
            End Try

        End Sub

        Public Sub Execute()
            eJobSchedulerServer.Start()
            Dim root As String = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.ePLMS_ROOT")
            Dim pathToView As String = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.BAAN.PathMonitor")
            Dim fileRDABaan As String = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.BAAN.FileMonitor")
            Dim mydir As DirectoryInfo
            Dim BaanList As System.Data.DataTable = Nothing
            Dim lines() As String = Nothing
            Try
                fileRDABaan = ("*" _
                            + (DateTime.Now.ToString("yyyyMMdd") + "*.txt"))
                mydir = New DirectoryInfo(pathToView)
                If mydir.Exists Then
                    Dim myFiles() As FileInfo = mydir.GetFiles(fileRDABaan)
                    If (myFiles.Length > 0) Then
                        'Me.sendMail(String.Format("Job started on: {0}", myFiles(0)))
                        eJobSchedulerServer.Log.Info(String.Format("ePLMSRDAConnectionJob:Monitor Activity Started"))
                        eJobSchedulerServer.Log.Info(String.Format("ePLMSRDAConnectionJob:path scanning file '{0}'", myFiles(0)))
                        lines = System.IO.File.ReadAllLines(myFiles(0).FullName)
                        ImportDataToCD(lines, root)
                        eJobSchedulerServer.Log.Info(String.Format( _serviceName & ": Monitor Activity ended"))
                        'Me.sendMail(String.Format("Monitor Activity ended"))
                    Else
                        'sendMail(String.Format("ePLMSRDAConnectionJob:path '{0}' no file to process ", pathToView));
                        Dim message As String = String.Format("path '{0}' no file to process ", pathToView)
                        eJobSchedulerServer.Log.Error(String.Format(_serviceName & ": path '{0}' no file to process ", pathToView))
                        Throw New Exception(message)
                    End If

                Else
                    'sendMail(String.Format("ePLMSRDAConnectionJob:path '{0}' not found ", pathToView));
                    eJobSchedulerServer.Log.Error(String.Format(_serviceName & ":path '{0}' not found ", pathToView))
                    Dim message As String = String.Format("path '{0}' not found ", pathToView)
                    Throw New Exception(message)
                End If

            Catch ex As Exception
                'sendMail(String.Format("ePLMSRDAConnectionJob:Error: {0} {1}", ex.Message, ex.ToString()));
                eJobSchedulerServer.Log.Error(String.Format(_serviceName & " - Error. {0} {1}", ex.Message, ex.ToString))
                Dim message As String = String.Format("Error: {0} {1}", ex.Message, ex.ToString)
                Throw New Exception(message)
            Finally
                Try
                    If lines IsNot Nothing Then
                        GC.SuppressFinalize(lines)
                    End If
                    If BaanList IsNot Nothing Then
                        GC.SuppressFinalize(BaanList)
                    End If
                    lines = Nothing
                    BaanList = Nothing

                    GC.Collect(2, GCCollectionMode.Forced)
                    GC.Collect(2, GCCollectionMode.Optimized)
                    eJobSchedulerServer.Stop()
                Catch ex As Exception

                End Try

            End Try

        End Sub


        Public Sub ImportDataToCD(ByRef lines As [String](), root As String)
            Dim Rc As Integer
            Dim myeConnection As eConnection = Nothing
            Dim rdaBD As eBusinessData = Nothing
            Dim RDAList As System.Data.DataTable = Nothing
            Dim lineMapping As String = ""
            Dim rdaField As [String]() = getRDAField(lineMapping)
            Dim fieldPerno As String = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.BAAN.RdaCodeAttributeName")
            Dim pernoIdx As Integer = 0
            Dim startIndex As Integer = 4
            Dim myCDRows As System.Data.DataRow() = Nothing
            Try
                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:File mapping: {0}.", fieldPerno))
                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Content: {0}.", lineMapping))
                For idx As Integer = 0 To rdaField.Length - 1
                    If rdaField(idx).ToLower() = fieldPerno.ToLower() Then
                        pernoIdx = idx
                        Exit For
                    End If
                Next
                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Index RDA CODE at: {0}.", pernoIdx))



                If lines IsNot Nothing Then
                    If lines.Length >= startIndex Then
                        myeConnection = New eConnection(root)
                        Rc = myeConnection.internalDbLogIn("eplms")
                        If Rc = 0 Then
                            RDAList = getAllRDA(myeConnection)
                            If RDAList IsNot Nothing Then
                                rdaBD = New eBusinessData(myeConnection)
                                Dim cellCount As Integer = lines(startIndex).Split(";".ToCharArray()).Length
                                If cellCount <> rdaField.Length Then
                                    eJobSchedulerServer.Log.[Error]([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Error on file columns size read {0}, template {1}.", cellCount, rdaField.Length))
                                Else

                                    For idx As Integer = startIndex To lines.Length - 1
                                        If lines(idx).Trim() = "" Then
                                            Continue For
                                        End If

                                        Dim wsCells As String() = lines(idx).Split(";".ToCharArray())
                                        Dim rdaCode As String = wsCells(pernoIdx).ToString()
                                        rdaCode = rdaCode.Trim()

                                        'if (rdaCode == "798760")
                                        '     rdaCode = rdaCode;
                                        If rdaCode <> "" Then
                                            myCDRows = RDAList.[Select]((Convert.ToString("baancode='") & rdaCode) + "'")
                                            If myCDRows.Length > 0 Then
                                                For Each rdaRow As System.Data.DataRow In myCDRows
                                                    Rc = rdaBD.Load(Integer.Parse(rdaRow("eId").ToString()))
                                                    If Rc = 0 Then

                                                        Dim myAttribute As New ArrayList()
                                                        Dim Committed As Double = 0
                                                        Dim Invoiced As Double = 0

                                                        For idy As Integer = 0 To rdaField.Length - 1
                                                            If wsCells(idy) IsNot Nothing Then
                                                                Try
                                                                    Dim currentValue As String = wsCells(idy).ToString()
                                                                    currentValue = currentValue.Trim()
                                                                    If idy <> pernoIdx AndAlso currentValue <> "" AndAlso rdaField(idy) <> "" Then
                                                                        Dim edata As New eDataListElementModule.eDataListElement()
                                                                        edata.Name = rdaField(idy)
                                                                        If edata.Name.Contains("(D)") Then
                                                                            edata.Name = edata.Name.Replace("(D)", "")
                                                                            currentValue = currentValue.Split(" ".ToCharArray())(0)

                                                                            currentValue = currentValue.Replace("/", ".")
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_impegnato" Then
                                                                            Committed += GetDoubleValue(currentValue)
                                                                            ' double.Parse(currentValue);
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            ' currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_ordinato" Then
                                                                            Committed += GetDoubleValue(currentValue)
                                                                            ' double.Parse(currentValue);
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            ' currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_ricevuto" Then
                                                                            Committed += GetDoubleValue(currentValue)
                                                                            'double.Parse(currentValue);
                                                                            Invoiced += GetDoubleValue(currentValue)
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            'currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_liquidato" Then
                                                                            Committed += GetDoubleValue(currentValue)
                                                                            'double.Parse(currentValue);
                                                                            Invoiced += GetDoubleValue(currentValue)
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            'currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        ' Invoiced += GetDoubleValue(currentValue);//double.Parse(currentValue);
                                                                        'currentValue = GetDoubleValue(currentValue).ToString();//currentValue.Replace(",", ".");
                                                                        ' currentValue = currentValue.Replace(",", ".");
                                                                        If edata.Name.ToLower() = "baan_ricevuto" Then
                                                                        End If
                                                                        'Invoiced += GetDoubleValue(currentValue);//double.Parse(currentValue);
                                                                        'currentValue = GetDoubleValue(currentValue).ToString();//currentValue.Replace(",", ".");
                                                                        'currentValue = currentValue.Replace(",", ".");
                                                                        If edata.Name.ToLower() = "baan_liquidato" Then
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_altriconsumi" Then
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            ' currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        edata.Value = currentValue
                                                                        'rdaBD.changeAttribute(rdaField[idy], currentValue);

                                                                        myAttribute.Add(edata)
                                                                    End If
                                                                Catch e1 As Exception

                                                                End Try
                                                            End If
                                                        Next
                                                        If myAttribute.Count > 0 Then
                                                            Dim edata As New eDataListElementModule.eDataListElement()
                                                            edata.Name = "baan_committed"
                                                            edata.Value = Committed.ToString().Replace(",", ".")
                                                            myAttribute.Add(edata)
                                                            edata = New eDataListElementModule.eDataListElement()
                                                            edata.Name = "baan_invoiced"
                                                            edata.Value = Invoiced.ToString().Replace(",", ".")


                                                            myAttribute.Add(edata)

                                                            eJobSchedulerServer.Log.Info([String].Format("{0}", rdaBD.Name))
                                                            For Each myedata As eDataListElementModule.eDataListElement In myAttribute
                                                                eJobSchedulerServer.Log.Info([String].Format("{0}={1}", myedata.Name, myedata.Value))
                                                            Next


                                                            If rdaBD.Modify(myAttribute) = 0 Then


                                                                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Mofify {0}.", rdaBD.Name))
                                                            Else
                                                                eJobSchedulerServer.Log.[Error]([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Error Modify {0} {1}.", rdaBD.Name, myeConnection.errorMessage))


                                                            End If
                                                        End If

                                                        myAttribute = Nothing
                                                        'rdaBD.Unreserve();
                                                        System.Threading.Thread.Sleep(1000)
                                                    Else
                                                        eJobSchedulerServer.Log.[Error]([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Error load {0}.", myeConnection.errorMessage))


                                                    End If

                                                Next
                                            Else
                                                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: row {0} no rda mapping in cd, rda code {1}", idx.ToString(), rdaCode))
                                            End If
                                        Else
                                            eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: row {0} not parsed, rda code empty", idx.ToString()))
                                            Exit For
                                        End If
                                        wsCells = Nothing

                                    Next
                                End If
                            Else
                                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: No RDA in db"))

                            End If
                        Else
                            eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: Error in internal login"))

                        End If
                    Else
                        eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: No baan data to import"))
                    End If
                End If
            Catch ex As Exception
                eJobSchedulerServer.Log.[Error]([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Error. {0} {1}", ex.Message, ex.ToString()))
            Finally
                Try
                    myeConnection.logOut()
                Catch ex1 As Exception
                End Try
                If myCDRows IsNot Nothing Then
                    GC.SuppressFinalize(myCDRows)
                End If
                If RDAList IsNot Nothing Then
                    GC.SuppressFinalize(RDAList)
                End If
                If rdaBD IsNot Nothing Then
                    GC.SuppressFinalize(rdaBD)
                End If
                If myeConnection IsNot Nothing Then
                    GC.SuppressFinalize(myeConnection)
                End If
                RDAList = Nothing
                rdaBD = Nothing
                myeConnection = Nothing
                myCDRows = Nothing
                GC.Collect(2, GCCollectionMode.Forced)


                GC.Collect(2, GCCollectionMode.Optimized)
            End Try

        End Sub

        Public Function GetDoubleValue(ByVal givenValue As String) As Double
            givenValue = givenValue.Trim
            If (givenValue = "") Then
                Return 0
            Else
                givenValue = givenValue.Replace(".", "")
                givenValue = givenValue.Replace(",", ".")
                Try
                    Return Convert.ToDouble(givenValue, System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch e As Exception
                    Return Double.Parse(givenValue)
                End Try

            End If

            Return 0
        End Function

        




     
    End Class
End Namespace
