Imports System.Reflection
Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Text
Imports System.Threading
Imports System.Configuration
Imports eRdAAutomatizationService.Parallaksis.eJobScheduler

Public Class eSender
    Dim myLastName As String = ""
    Private Schedular As Timer

#Region "Service Actions"
    Public Sub StartServer()


        Try
            'Thread.Sleep(15000)
            Parallaksis.eJobScheduler.eJobSchedulerServer.Start(_serviceName & " started")
            Dim myScheduler As String = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.SchedulerType")
            Dim mode As String = myScheduler.Split("(")(0).ToUpper
            Dim myTime As String = myScheduler.Split("(")(1).Replace(")", "")

            eJobSchedulerServer.Log.Info(_serviceName & " mode: " & mode)

            Schedular = New Timer(New TimerCallback(AddressOf ExecuteBaaNRequest))


            'Set the Default Time.
            Dim scheduledTime As DateTime = DateTime.MinValue

            If mode = "DAILY" Then
                'Get the Scheduled Time from AppSettings.
                scheduledTime = DateTime.Parse(myTime)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next day.
                    scheduledTime = scheduledTime.AddDays(1)
                End If
            End If

            If mode.ToUpper() = "INTERVAL" Or mode.ToUpper() = "EACH" Then
                'Get the Interval in Minutes from AppSettings.
                Dim intervalMinutes As Integer = Convert.ToInt32(myTime)

                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddMinutes(intervalMinutes)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddMinutes(intervalMinutes)
                End If
            End If

            Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
            Dim schedule As String = String.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds)
            eJobSchedulerServer.Log.Info(_serviceName & " scheduled to run after: " & schedule)
            'Get the difference in Minutes between the Scheduled and Current Time.
            Dim dueTime As Integer = Convert.ToInt32(timeSpan.TotalMilliseconds)
            'Change the Timer's Due Time.
            Schedular.Change(dueTime, Timeout.Infinite)
            Parallaksis.eJobScheduler.eJobSchedulerServer.Stop()
        Catch ex As Exception
            eJobSchedulerServer.Log.Info("eRdASchedulerServiceError on: " & ex.Message & ex.StackTrace)

            'Stop the Windows Service.
            Using serviceController As New System.ServiceProcess.ServiceController(_serviceName)
                serviceController.[Stop]()
            End Using
        End Try




    End Sub
    Public Sub StopServer()
        Try
            Parallaksis.eJobScheduler.eJobSchedulerServer.Stop()
        Catch ex As Exception

        End Try

    End Sub


#End Region


#Region "EVENTS"

    Private Sub ExecuteBaaNRequest()
        Dim myPath As String = AppDomain.CurrentDomain.BaseDirectory

        Dim prog_name As String = IO.Path.Combine(myPath, "eRdAAutomatizationServiceBatch.exe")
        Parallaksis.eJobScheduler.eJobSchedulerServer.Start("-> start request to baan")

        If Not IO.File.Exists(prog_name) Then
            eJobSchedulerServer.Log.Info("batch file not exists:  " & prog_name)
        Else
            eJobSchedulerServer.Log.Info("check prog in memory")
            If Not String.IsNullOrEmpty(myLastName) Then
                Try
                    Dim P() As Process = Process.GetProcessesByName(myLastName)
                    If P.Length > 0 Then
                        eJobSchedulerServer.Log.Info("force close locked sync BAAN - ERDA on " & myLastName)
                        For Each pp As Process In P
                            pp.Kill()
                        Next
                    End If
                    GC.SuppressFinalize(P)
                    P = Nothing
                Catch ex As Exception

                End Try

            End If
            Try
                Dim _process As New Process()
                _process.StartInfo.FileName = prog_name
                _process.Start()
                myLastName = _process.ProcessName
                Dim myDateStart As DateTime = Now
                eJobSchedulerServer.Log.Info("process " & myLastName & " started at " + myDateStart.ToString())
                While Not _process.HasExited AndAlso _process.Responding
                    If Now.Subtract(myDateStart).TotalMinutes > 300 Then
                        eJobSchedulerServer.Log.Error("timeout kill :" & Now.Subtract(myDateStart).TotalMinutes.ToString())
                        _process.Kill()
                    End If
                End While
            Catch ex As Exception
                eJobSchedulerServer.Log.Error(String.Format(_serviceName & " error {0} ", ex.Message))
                eJobSchedulerServer.Log.Error(ex.ToString)
            End Try
        End If

exitFunction:
        GC.Collect()
        GC.WaitForPendingFinalizers()
        GC.Collect()

        Try
            Parallaksis.eJobScheduler.eJobSchedulerServer.Stop()
        Catch ex As Exception

        End Try
        Me.StartServer()
    End Sub




#End Region


#Region "COMMON"

    Private Sub ReloadParameters()
        Dim myStartPath As String = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
        Dim myFileConfig As String = IO.Path.Combine(myStartPath, "service.config")


    End Sub


#End Region


#Region "costruttore"


    Public Sub New()


    End Sub


#End Region


End Class
