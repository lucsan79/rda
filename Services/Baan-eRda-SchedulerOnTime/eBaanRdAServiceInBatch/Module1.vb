﻿Imports eRdASchedulerServiceData.Parallaksis.eJobScheduler
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.IO

Module Module1
    Dim myLogPath As String = ""
    Sub Main(args() As String)
        'Threading.Thread.Sleep(10000)
        myLogPath = args(6)
        eJobSchedulerServer.Start(myLogPath)
        eJobSchedulerServer.Log.Info("*/Config Paramenters->")
        For Each arg As String In args
            eJobSchedulerServer.Log.Info(arg)
        Next
        eJobSchedulerServer.Log.Info("<- Config Paramenters/*")

        Dim myJob = New Parallaksis.eJobScheduler.Jobs.ePLMSRDAConnectionJob()
        Try
            myJob.Execute(args(0), args(1), args(2), args(3), args(4), args(5), args(7), args(8))
        Catch ex As Exception
            eJobSchedulerServer.Log.Error(String.Format("ePLMSRDAConnectionJob: '{0}' ", ex.ToString))
        End Try

        eJobSchedulerServer.Stop()
        Environment.Exit(0)
    End Sub


End Module
