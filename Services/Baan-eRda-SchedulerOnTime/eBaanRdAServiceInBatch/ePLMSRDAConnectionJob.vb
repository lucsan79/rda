﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.Collections
Imports System.IO
Imports System.Net.Mail

Namespace Parallaksis.eJobScheduler.Jobs

    Public Class ePLMSRDAConnectionJob


        Public Sub Execute(root As String,
                           pathToView As String,
                           fileRDABaan As String,
                           rdaCodeField As String,
                           fileToRead As String,
                           findScript As String,
                           startLine As String,
                           stopLine As String)

            Dim mydir As DirectoryInfo
            Dim BaanList As System.Data.DataTable = Nothing
            Dim lines() As String = Nothing
            eJobSchedulerServer.Log.Info(String.Format("ePLMSRDAConnectionJob:Monitor Activity Started"))
            Try
                fileRDABaan = ("*" _
                            + (DateTime.Now.ToString("yyyyMMdd") + "*.txt"))
                mydir = New DirectoryInfo(pathToView)
                If mydir.Exists Then
                    Dim myFiles() As FileInfo = mydir.GetFiles(fileRDABaan)
                    If (myFiles.Length > 0) Then
                        eJobSchedulerServer.Log.Info(String.Format("ePLMSRDAConnectionJob:path scanning file '{0}'", myFiles(0)))
                        lines = System.IO.File.ReadAllLines(myFiles(0).FullName)
                        If lines.Length > 0 Then

                            Dim myNewLine As New List(Of String)
                            If Integer.Parse(startLine) > 0 Then
                                For idx As Integer = 0 To 3
                                    myNewLine.Add(lines(idx))
                                Next
                            End If
                            For idx As Integer = Integer.Parse(startLine) To Integer.Parse(stopLine)
                                Try
                                    myNewLine.Add(lines(idx))
                                Catch ex As Exception
                                    Exit For
                                End Try

                            Next
                            lines = myNewLine.ToArray
                        End If
                        'Dim myFile As String = IO.Path.Combine("C:\etc\Parallaksis\Collaboration Desktop\.Net Framework 4.0\3.3\masad.erda\monitor\logs", "OUT-" & Now.ToString("yyyyMMddHHmmss") & ".txt")
                        'IO.File.WriteAllLines(myFile, lines)
                        'Threading.Thread.Sleep(50000)
                        ImportDataToCD(lines, root, rdaCodeField, fileToRead, findScript)
                    Else
                        eJobSchedulerServer.Log.Error(String.Format("ePLMSRDAConnectionJob:path '{0}' no file to process ", pathToView))
                    End If
                Else
                    eJobSchedulerServer.Log.Error(String.Format("ePLMSRDAConnectionJob:path '{0}' not found ", pathToView))
                End If

            Catch ex As Exception
                eJobSchedulerServer.Log.Error(String.Format("ePLMSRDAConnectionJob:Error. {0} {1}", ex.Message, ex.ToString))
            Finally
                Try
                    If lines IsNot Nothing Then
                        GC.SuppressFinalize(lines)
                    End If
                    If BaanList IsNot Nothing Then
                        GC.SuppressFinalize(BaanList)
                    End If
                    lines = Nothing
                    BaanList = Nothing

                    GC.Collect(2, GCCollectionMode.Forced)
                    GC.Collect(2, GCCollectionMode.Optimized)

                Catch ex As Exception

                End Try
                eJobSchedulerServer.Log.Info(String.Format("ePLMSRDAConnectionJob:Monitor Activity Ended"))
            End Try

        End Sub


        Public Sub ImportDataToCD(ByRef lines As [String](), root As String, fieldPerno As String, fileToRead As String, findScript As String)
            Dim Rc As Integer
            Dim myeConnection As eConnection = Nothing
            Dim rdaBD As eBusinessData = Nothing
            Dim RDAList As System.Data.DataTable = Nothing
            Dim lineMapping As String = ""
            Dim rdaField As [String]() = getRDAField(lineMapping, fileToRead)

            Dim pernoIdx As Integer = 0
            Dim startIndex As Integer = 4
            Dim myCDRows As System.Data.DataRow() = Nothing
            Dim dateFormat As String = ""

            Try
                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:File mapping: {0}.", fieldPerno))
                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Content: {0}.", lineMapping))
                For idx As Integer = 0 To rdaField.Length - 1
                    If rdaField(idx).ToLower() = fieldPerno.ToLower() Then
                        pernoIdx = idx
                        Exit For
                    End If
                Next
                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Index RDA CODE at: {0}.", pernoIdx))



                If lines IsNot Nothing Then
                    If lines.Length >= startIndex Then
                        myeConnection = New eConnection(root)
                        Rc = myeConnection.internalDbLogIn("eplms")
                        If Rc = 0 Then
                            eEnvConfigurationController.getEntry(myeConnection, "shortDateFormat")
                            RDAList = getAllRDA(myeConnection, findScript)
                            If RDAList IsNot Nothing Then
                                rdaBD = New eBusinessData(myeConnection)
                                Dim cellCount As Integer = lines(startIndex).Split(";".ToCharArray()).Length
                                If cellCount <> rdaField.Length Then
                                    eJobSchedulerServer.Log.[Error]([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Error on file columns size read {0}, template {1}.", cellCount, rdaField.Length))
                                Else

                                    For idx As Integer = startIndex To lines.Length - 1
                                        If lines(idx).Trim() = "" Then
                                            Continue For
                                        End If

                                        Dim wsCells As String() = lines(idx).Split(";".ToCharArray())
                                        Dim rdaCode As String = wsCells(pernoIdx).ToString()
                                        rdaCode = rdaCode.Trim()

                                        'if (rdaCode == "798760")
                                        '     rdaCode = rdaCode;
                                        If rdaCode <> "" Then
                                            myCDRows = RDAList.[Select]((Convert.ToString("baancode='") & rdaCode) + "'")
                                            If myCDRows.Length > 0 Then
                                                For Each rdaRow As System.Data.DataRow In myCDRows
                                                    Rc = rdaBD.Load(Integer.Parse(rdaRow("eId").ToString()))
                                                    If Rc = 0 Then

                                                        Dim myAttribute As New ArrayList()
                                                        Dim Committed As Double = 0
                                                        Dim Invoiced As Double = 0

                                                        For idy As Integer = 0 To rdaField.Length - 1
                                                            If wsCells(idy) IsNot Nothing Then
                                                                Try
                                                                    Dim currentValue As String = wsCells(idy).ToString()
                                                                    currentValue = currentValue.Trim()
                                                                    If idy <> pernoIdx AndAlso currentValue <> "" AndAlso rdaField(idy) <> "" Then
                                                                        Dim edata As New eDataListElementModule.eDataListElement()
                                                                        edata.Name = rdaField(idy)
                                                                        If edata.Name.Contains("(D)") Then
                                                                            edata.Name = edata.Name.Replace("(D)", "")
                                                                            currentValue = currentValue.Split(" ".ToCharArray())(0)
                                                                            If dateFormat.Contains("/") Then
                                                                                currentValue = currentValue.Replace(".", "/")
                                                                            ElseIf dateFormat.Contains(".") Then
                                                                                currentValue = currentValue.Replace("/", ".")
                                                                            End If

                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_impegnato" Then
                                                                            Committed += GetDoubleValue(currentValue)
                                                                            ' double.Parse(currentValue);
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            ' currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_ordinato" Then
                                                                            Committed += GetDoubleValue(currentValue)
                                                                            ' double.Parse(currentValue);
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            ' currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_ricevuto" Then
                                                                            Committed += GetDoubleValue(currentValue)
                                                                            'double.Parse(currentValue);
                                                                            Invoiced += GetDoubleValue(currentValue)
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            'currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_liquidato" Then
                                                                            Committed += GetDoubleValue(currentValue)
                                                                            'double.Parse(currentValue);
                                                                            Invoiced += GetDoubleValue(currentValue)
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            'currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        ' Invoiced += GetDoubleValue(currentValue);//double.Parse(currentValue);
                                                                        'currentValue = GetDoubleValue(currentValue).ToString();//currentValue.Replace(",", ".");
                                                                        ' currentValue = currentValue.Replace(",", ".");
                                                                        If edata.Name.ToLower() = "baan_ricevuto" Then
                                                                        End If
                                                                        'Invoiced += GetDoubleValue(currentValue);//double.Parse(currentValue);
                                                                        'currentValue = GetDoubleValue(currentValue).ToString();//currentValue.Replace(",", ".");
                                                                        'currentValue = currentValue.Replace(",", ".");
                                                                        If edata.Name.ToLower() = "baan_liquidato" Then
                                                                        End If
                                                                        If edata.Name.ToLower() = "baan_altriconsumi" Then
                                                                            currentValue = GetDoubleValue(currentValue).ToString()
                                                                            ' currentValue.Replace(",", ".");
                                                                            currentValue = currentValue.Replace(",", ".")
                                                                        End If
                                                                        edata.Value = currentValue
                                                                        'rdaBD.changeAttribute(rdaField[idy], currentValue);

                                                                        myAttribute.Add(edata)
                                                                    End If
                                                                Catch e1 As Exception

                                                                End Try
                                                            End If
                                                        Next
                                                        If myAttribute.Count > 0 Then
                                                            Dim edata As New eDataListElementModule.eDataListElement()
                                                            edata.Name = "baan_committed"
                                                            edata.Value = Committed.ToString().Replace(",", ".")
                                                            myAttribute.Add(edata)
                                                            edata = New eDataListElementModule.eDataListElement()
                                                            edata.Name = "baan_invoiced"
                                                            edata.Value = Invoiced.ToString().Replace(",", ".")


                                                            myAttribute.Add(edata)

                                                            eJobSchedulerServer.Log.Info([String].Format("{0}", rdaBD.Name))
                                                            For Each myedata As eDataListElementModule.eDataListElement In myAttribute
                                                                eJobSchedulerServer.Log.Info([String].Format("{0}={1}", myedata.Name, myedata.Value))
                                                            Next


                                                            If rdaBD.Modify(myAttribute) = 0 Then


                                                                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Mofify {0}.", rdaBD.Name))
                                                            Else
                                                                eJobSchedulerServer.Log.[Error]([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Error Modify {0} {1}.", rdaBD.Name, myeConnection.errorMessage))


                                                            End If
                                                        End If

                                                        myAttribute = Nothing
                                                        'rdaBD.Unreserve();
                                                        System.Threading.Thread.Sleep(1000)
                                                    Else
                                                        eJobSchedulerServer.Log.[Error]([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Error load {0}.", myeConnection.errorMessage))


                                                    End If

                                                Next
                                            Else
                                                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: row {0} no rda mapping in cd, rda code {1}", idx.ToString(), rdaCode))
                                            End If
                                        Else
                                            eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: row {0} not parsed, rda code empty", idx.ToString()))
                                            Exit For
                                        End If
                                        wsCells = Nothing

                                    Next
                                End If
                            Else
                                eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: No RDA in db"))

                            End If
                        Else
                            eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: Error in internal login"))

                        End If
                    Else
                        eJobSchedulerServer.Log.Info([String].Format("ePLMSRDAConnectionJob.ImportDataToCD: No baan data to import"))
                    End If
                End If
            Catch ex As Exception
                eJobSchedulerServer.Log.[Error]([String].Format("ePLMSRDAConnectionJob.ImportDataToCD:Error. {0} {1}", ex.Message, ex.ToString()))
            Finally
                Try
                    myeConnection.logOut()
                Catch ex1 As Exception
                End Try
                If myCDRows IsNot Nothing Then
                    GC.SuppressFinalize(myCDRows)
                End If
                If RDAList IsNot Nothing Then
                    GC.SuppressFinalize(RDAList)
                End If
                If rdaBD IsNot Nothing Then
                    GC.SuppressFinalize(rdaBD)
                End If
                If myeConnection IsNot Nothing Then
                    GC.SuppressFinalize(myeConnection)
                End If
                RDAList = Nothing
                rdaBD = Nothing
                myeConnection = Nothing
                myCDRows = Nothing
                GC.Collect(2, GCCollectionMode.Forced)


                GC.Collect(2, GCCollectionMode.Optimized)
            End Try

        End Sub

        Public Function GetDoubleValue(ByVal givenValue As String) As Double
            givenValue = givenValue.Trim
            If (givenValue = "") Then
                Return 0
            Else
                givenValue = givenValue.Replace(".", "")
                givenValue = givenValue.Replace(",", ".")
                Try
                    Return Convert.ToDouble(givenValue, System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch e As Exception
                    Return Double.Parse(givenValue)
                End Try

            End If

            Return 0
        End Function

        Public Function getAllRDA(ByRef givenConnection As eConnection, findExecute As String) As System.Data.DataTable
            Dim rc As Integer
            Dim returnTable As System.Data.DataTable = Nothing
            Dim myFindRDA As eFind
            'Dim findExecute As String = 's.AppSettings("ePLMS.ePLMSRDAConnectionJob.findScript")
            Try
                'If System.IO.File.Exists(findExecute) Then
                Dim input() As String = Nothing
                    myFindRDA = New eFind(givenConnection)
                    myFindRDA.queryString = "$(select eId, RDACode as 'baancode' from eplms.rdadb where (RDACode IS NOT NULL and RDACode <>'') )" '(System.IO.File.ReadAllText(findExecute)
                    'myFindRDA.selectAttributeListString = "Id;Name;Project;key1"
                    rc = myFindRDA.Prepare(input)
                    rc = myFindRDA.Execute(returnTable)
                    If (rc = 0) Then
                        eJobSchedulerServer.Log.Info(String.Format("ePLMSRDAConnectionJob.getAllRDA:Found RDA. {0}", returnTable.Rows.Count))
                    Else
                        eJobSchedulerServer.Log.Error(String.Format("ePLMSRDAConnectionJob.getAllRDA:Error. {0}", givenConnection.errorMessage))
                    End If

                ' Else
                'eJobSchedulerServer.Log.Error(String.Format("ePLMSRDAConnectionJob.getAllRDA:Error. script not found"))
                'End If

            Catch ex As Exception
                eJobSchedulerServer.Log.Error(String.Format("ePLMSRDAConnectionJob.getAllRDA:Error. {0} {1}", ex.Message, ex.ToString))
            Finally
                myFindRDA = Nothing
                GC.SuppressFinalize(Me)
            End Try

            Return returnTable
        End Function

        Public Function getRDAField(ByRef mappingLine As String, fileToRead As String) As String()
            Dim returnFields() As String = Nothing
            'Dim fileToRead As String = 's.AppSettings("ePLMS.ePLMSRDAConnectionJob.rdaMappingFields")
            Try
                If System.IO.File.Exists(fileToRead) Then
                    Dim input As String = System.IO.File.ReadAllText(fileToRead)
                    mappingLine = input
                    returnFields = input.Split(";".ToCharArray)
                End If

            Catch ex As Exception
                eJobSchedulerServer.Log.Error(String.Format("ePLMSRDAConnectionJob.getRDAField:Error. {0} {1}", ex.Message, ex.ToString))
            Finally

            End Try

            Return returnFields
        End Function




        Public Sub New()

        End Sub
    End Class
End Namespace
