﻿Imports System.Linq
Imports System.Web
Imports log4net
Imports log4net.Appender
Imports log4net.Config
Imports log4net.Repository
Imports log4net.Core

Imports System.Collections.Specialized
Imports System.Net
Imports System.IO
Imports log4net.Layout
Imports System.Text
Imports log4net.Repository.Hierarchy
Imports System.Reflection
Imports System.Collections.Generic

Namespace Parallaksis.eJobScheduler
    Public NotInheritable Class eJobSchedulerServer
        Private Sub New()
        End Sub
        Shared _log As ILog
        Shared _running As Boolean = False


        Public Shared ReadOnly Property Log() As ILog
            Get
                Return _log
            End Get
        End Property


        Public Shared Sub [Stop]()
            _running = False
            _log.Info("eJobScheduler stopped ")
            ResetLog()

        End Sub

        Public Shared Function isStarted() As Boolean
            Return _running
        End Function
        Public Shared Sub ResetLog()
            _log.Logger.Repository.Shutdown()
            'Try
            '    For Each app As log4net.Appender.IAppender In _log.Logger.Repository.GetAppenders()
            '        app.Close()
            '    Next
            'Catch ex As Exception

            'End Try
        End Sub

        Public Shared Sub Start(givenlogPath As String)


            'Configure logger
            ' Dim pathLog As String = System.Configuration.ConfigurationSettings.AppSettings("ePLMS.ePLMSRDAConnectionJob.ePLMS_LogPath")
            ConfigureFileAppender(Path.Combine(givenlogPath, "eBaanRdAService-Data" + DateTime.Now.ToString("yyyyMMdd") + ".log"))

            _log = LogManager.GetLogger(GetType(eJobSchedulerServer))
            _running = True
            _log.Info("eJobScheduler started ")
            '_log.Info("eJobScheduler started" & Now.ToString)

        End Sub

        Public Shared Sub ConfigureFileAppender(logFile As String)
            Dim fileAppender = GetFileAppender(logFile)
            BasicConfigurator.Configure(fileAppender)
            DirectCast(LogManager.GetRepository(), Hierarchy).Root.Level = Level.Debug
        End Sub

        Private Shared Function GetFileAppender(logFile As String) As IAppender
            Dim layout = New PatternLayout("%date %-5level %logger - %message%newline")
            layout.ActivateOptions()
            ' According to the docs this must be called as soon as any properties have been changed.
            Dim appender = New FileAppender() With { _
                .File = logFile, _
                .Encoding = Encoding.UTF8, _
                .Threshold = Level.Debug, _
                .Layout = layout _
            }

            appender.ActivateOptions()

            Return appender
        End Function


    End Class




End Namespace