Imports System.Reflection

Public Class eSenderService

    Dim myConvertClass As eSender
    'Public Const _serviceName As String = "eRdABaaNDataSynchronization"
    'Private Const _serviceDescription As String = "eRdA BaaN Data Synchronization"

    Protected Overrides Sub OnStart(ByVal args() As String)

        Threading.Thread.Sleep(1000)
        Try
            myConvertClass = New eSender
            myConvertClass.StartServer()
        Catch ex As Exception
            'Dim c As New IO.StreamWriter("c:\temp\error.txt")
            'c.WriteLine(ex.ToString)
            'c.Close()

        End Try
        
    End Sub

    Protected Overrides Sub OnStop()
        If myConvertClass IsNot Nothing Then
            myConvertClass.StopServer()
        End If
        myConvertClass = Nothing
    End Sub

    Shared Sub Main(ByVal args() As String)
        'Dim myConvertClass As eConverter
        ' myConvertClass = New eConverter
        'myConvertClass.StartServer()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' All'interno di uno stesso processo � possibile eseguire pi� servizi di Windows NT.
        ' Per aggiungere un servizio al processo, modificare la riga che segue in modo
        ' da creare un secondo oggetto servizio. Ad esempio,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '

        If (args.Length = 1) Then
            Console.WriteLine("Service Install/Unistall.")
            Console.WriteLine("Params " & args(0).ToUpper())
            Try
                Select Case (args(0).ToUpper)
                    Case "/I"

                        Dim srv As New DirectServiceInstaller()
                        Dim okInstallation As Boolean = srv.InstallService(Assembly.GetExecutingAssembly().Location + " /s", _serviceName, _serviceDescription)
                        If okInstallation Then
                            Console.WriteLine("Service installed.")
                        Else
                            Console.WriteLine("There was a problem with installation.")
                        End If
                        Exit Sub

                    Case "/U"
                        ' TODO stop the service
                        Dim srv As New DirectServiceInstaller()
                        Dim okInstallation As Boolean = srv.UnInstallService(_serviceName)
                        If okInstallation Then
                            Console.WriteLine("Service uninstalled.")
                        Else
                            Console.WriteLine("There was a problem with uninstallation.")
                        End If
                        Exit Sub

                End Select

            Catch ex As Exception
                Console.WriteLine("Error duting operation")
                Console.WriteLine("Message:" & ex.Message)
                Console.WriteLine("Full:" & ex.ToString)
            End Try
        End If
        Try
            ServicesToRun = New System.ServiceProcess.ServiceBase() {New eSenderService}

            System.ServiceProcess.ServiceBase.Run(ServicesToRun)
        Catch ex As Exception

        End Try

    End Sub

End Class
