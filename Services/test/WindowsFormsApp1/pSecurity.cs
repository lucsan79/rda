﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualBasic;
using System.Security.Cryptography;

public class pSecurity : IDisposable
{

    public byte[] key = { 76, 43, 51, 93, 154, 52, 126, 39, 131, 140, 191, 101, 216, 87, 171, 78 };

    public byte[] IV = { 23, 17, 238, 158, 36, 55, 211, 144 };
  
    // [-]--------------------------------------------------------------------[-]
    // |
    // | Class            : pSecurity
    // | Created by       : DrP
    // | Date             : 28 jan 2004
    // | Description      : Metodi e proprietà per pSecurity
    // | Private Methods  :
    // | Friend Methods   :
    // |                    CryptBase64CNV()
    // |                    DecryptBase64CNV()
    // |                    setIV()
    // |                    setKey()
    // | Public Methods   :
    // |
    // | Properties       :
    // |
    // [-]--------------------------------------------------------------------[-]

    // **************************************************************
    // *                              mSystemInfo.ComputerName      *
    // *                C o n s t a n t s                           *
    // *                                                            *
    // **************************************************************

    // **************************************************************
    // *                                                            *
    // *                G l o b a l  V a r i a b l e s              *
    // *                                                            *
    // **************************************************************



    public pSecurity()
    {
    }

    // **************************************************************
    // *                                                            *
    // *    D i s p o s e   V a r s   &   M e t h o d s             *
    // *                                                            *
    // **************************************************************
    public new void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    ~pSecurity()
    {
        Dispose(false);
    }

    protected virtual new void Dispose(bool disposing)
    {
        if (disposing)
        {
        }
    }

    // **************************************************************
    // *                                                            *
    // *                P r i v a t e   M e t h o d s               *
    // *                                                            *
    // **************************************************************


    // **************************************************************
    // *                                                            *
    // *                F r i e n d  M e t h o d s                  *
    // *                                                            *
    // **************************************************************

    // [-]--------------------------------------------------------------------[-]
    // |
    // | Method           : CryptBase64CNV()
    // | Created by       : Oleh Lozynskyy
    // | Date             : 28 jan 2004
    // | Description      : Cripta una stringa                 
    // | Parameters       :
    // | Returns          :stringa in Base64
    // [-]--------------------------------------------------------------------[-]
    internal void CryptBase64CNV(string givenString, ref string returnStringBase64)
    {
        RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
        ICryptoTransform encryptor;
        ASCIIEncoding textConverter = new ASCIIEncoding();
        byte[] toEncrypt;
        MemoryStream msEncrypt = new MemoryStream();
        CryptoStream csEncrypt;
        // crittografia RC2
        encryptor = rc2CSP.CreateEncryptor(key, IV);
        csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
        toEncrypt = textConverter.GetBytes(givenString);
        csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
        csEncrypt.FlushFinalBlock();
        // codifica con Base64

        long arrayLength;
        arrayLength = Convert.ToInt32((4 / (double)3) * msEncrypt.Length);
        if (arrayLength % 4 != 0)
            arrayLength = arrayLength + 4 - arrayLength % 4;

        char[] base64CharArray = new char[Convert.ToInt32(arrayLength) - 1 + 1];


        System.Convert.ToBase64CharArray(msEncrypt.ToArray(), 0, Convert.ToInt32(msEncrypt.Length), base64CharArray, 0);

        returnStringBase64 = new string(base64CharArray); 
    }

    // [-]--------------------------------------------------------------------[-]
    // |
    // | Method           : DecryptBAse64CNV()
    // | Created by       : Oleh Lozynskyy
    // | Date             : 28 jan 2004
    // | Description      : Decripta una stringa                 
    // | Parameters       :
    // | Returns          : 
    // [-]--------------------------------------------------------------------[-]
    internal void DecryptBase64CNV(string givenStringBase64, ref string returnString)
    {
        RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
        ICryptoTransform decryptor;
        ASCIIEncoding textConverter = new ASCIIEncoding();
        byte[] fromEncrypt;
        int idx;
        byte[] givenBytes;

        try
        {
            givenBytes = System.Convert.FromBase64String(givenStringBase64);
        }
        catch (ArgumentNullException exp)
        {
            throw new System.Exception("Base 64 string is null.");
        }
        catch (FormatException exp)
        {
            throw new System.Exception("Base 64 length is not 4 or is " + "not an even multiple of 4.");
        }

        MemoryStream msDecrypt = new MemoryStream(givenBytes);
        CryptoStream csDecrypt;

        decryptor = rc2CSP.CreateDecryptor(key, IV);
        csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
        fromEncrypt = new byte[givenBytes.Length + 1];
        csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

        char[] decrypted = new char[fromEncrypt.Length - 1 + 1];
        for (idx = 0; idx <= fromEncrypt.Length - 1; idx++)
        {
            if (fromEncrypt[idx] == 0)
                decrypted[idx] = System.Convert.ToChar(" ");
            else
                decrypted[idx] = System.Convert.ToChar( fromEncrypt[idx]);
        }
        string decryptedString = new string(decrypted);
        returnString = decryptedString;
    }

    // [-]--------------------------------------------------------------------[-]
    // |
    // | Method           : setIV()
    // | Created by       : DrP
    // | Date             : 28 jan 2004
    // | Description      : Setta l'"IV"                        
    // | Parameters       :
    // | Returns          : 
    // [-]--------------------------------------------------------------------[-]
    internal void setIV(byte[] givenIV)
    {
        IV = givenIV;
    }

    // [-]--------------------------------------------------------------------[-]
    // |
    // | Method           : setKey()
    // | Created by       : DrP
    // | Date             : 28 jan 2004
    // | Description      : Setta la "key"                       
    // | Parameters       :
    // | Returns          : 
    // [-]--------------------------------------------------------------------[-]
    internal void setKey(byte[] givenKey)
    {
        key = givenKey;
    }
}
