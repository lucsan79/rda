﻿using Parallaksis.ePLMSM40;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pSecurity dec = new pSecurity();
            string retValue = "";
            dec.DecryptBase64CNV(textBox1.Text, ref retValue);
            textBox2.Text = retValue;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            eConnection mycon = new eConnection(@"C:\etc\Collaboration\cd4.rda");
            mycon.internalDbLogIn("eplms");

            string givenDate = "05082020";
            givenDate = String.Format("{0}{1}{2}", givenDate.Substring(4, 4), givenDate.Substring(2, 2), givenDate.Substring(0, 2));
            string c = givenDate + "000000";
            mycon.Core.stringToDate(c, ref c);
        }
    }
}
