'
'*------------------------------------------------------------------------------
'*   Copyright (c) 2005 Parallaksis Corporation. All Rights Reserved.
'*
'*   This source code is intended only as a supplement to Collaboration
'*   Desktop Development Tools.  
'*
'*------------------------------------------------------------------------------
' 
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs

Public Class eAdminHooks
        Implements IDisposable

#Region "Class Description"
        '[-]--------------------------------------------------------------------[-]
        ' |
        ' | Class            : eAdminHooks
        ' | Created by       : 
        ' | Date             : 10 dec 2003
        ' | Description      : 
        ' | Private Methods  :
        ' | Public Methods   :
	' |     ---------------------------------------------------------------
	' |     BusinessDataType Hooks
	' |     ---------------------------------------------------------------
	' |       getBusinessDataTypeAttributeListSuffix()
	' |     ---------------------------------------------------------------
	' |     User Hooks
	' |     ---------------------------------------------------------------
	' |       createUserPrefix()
	' |       createUserSuffix()
        ' | Properties       :
        '[-]--------------------------------------------------------------------[-]
#End Region

#Region "Global variables"
        '**************************************************************
        '*                                                            *
        '*                G l o b a l  V a r i a b l e s              *
        '*                                                            *
        '**************************************************************
        Private className                 As String
#End Region

#Region "Properties"
        '**************************************************************
        '*                                                            *
        '*                P r o p e r t i e s                         *
        '*                                                            *
        '**************************************************************
#End Region

#Region "Constructor"
        '**************************************************************
        '*                                                            *
        '*                C o n s t r u c t o r                       *
        '*                                                            *
        '**************************************************************
        Public Sub New()
	    className = "eAdminHooks"
        End Sub
#End Region

#Region "Dispose Vars & Methods"
        '**************************************************************
        '*                                                            *
        '*    D i s p o s e   V a r s   &   M e t h o d s             *
        '*                                                            *
        '**************************************************************
        Public Overloads Sub Dispose() Implements System.IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                ' Disposal was triggered manually by the client.
                ' Call Dispose() on any contained classes.
            End If

            ' Release unmanaged resources.
            ' Set large member variables to Nothing (null).
        End Sub
#End Region

#Region "Private Methods"
        '**************************************************************
        '*                                                            *
        '*                P r i v a t e   M e t h o d s               *
        '*                                                            *
        '**************************************************************
#End Region

#Region "Public Methods"
        '**************************************************************
        '*                                                            *
        '*                P u b l i c  M e t h o d s                  *
        '*                                                            *
        '**************************************************************
#Region "BusinessDataType Hooks"
#Region "getBusinessDataTypeAttributeListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' |
    ' | Method           : getBusinessDataTypeAttributeListSuffix()
    ' | Created by       : 
    ' | Date             : 
    ' | Description      : 
    ' | Parameters       : 
    ' | Returns          : ePLMSErr_OK
    ' |                    ePLMSErr_CONTINUE
    ' |                    ePLMSErr_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataTypeAttributeListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessDataType As String, ByRef givenCriteriaAttributeName As String, ByRef givenCriteriaAttributeValue As String, ByRef givenAttributeList As arrayList, ByRef Rc As Integer) As Integer
        Dim dummyFunctionName     As String

	dummyFunctionName = "getBusinessDataTypeAttributeListSuffix"
	
	givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        Rc = ePLMS_CONTINUE

ExitFunction:
	givenConnection.Logger.handleLocalError(Rc,className,dummyFunctionName)
	givenConnection.Logger.handleError1(Rc,"Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & Rc & ")",0,"")
	givenConnection.Logger.handleFunctionEnd(Rc)

        Return (Rc)
    End Function
#End Region
#End Region

#Region "User Hooks"
#Region "createUserPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' |
    ' | Method           : createUserPrefix()
    ' | Created by       : 
    ' | Date             : 
    ' | Description      : 
    ' | Parameters       : 
    ' | Returns          : ePLMSErr_OK
    ' |                    ePLMSErr_CONTINUE
    ' |                    ePLMSErr_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function createUserPrefix(ByRef givenConnection As eConnection, ByRef givenUser As eUser, ByRef Rc As Integer) As Integer
        Dim dummyFunctionName     As String

	dummyFunctionName = "createUserPrefix"
	
	givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        Rc = ePLMS_CONTINUE

ExitFunction:
	givenConnection.Logger.handleLocalError(Rc,className,dummyFunctionName)
	givenConnection.Logger.handleError1(Rc,"Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & Rc & ")",0,"")
	givenConnection.Logger.handleFunctionEnd(Rc)

        Return (Rc)
    End Function
#End Region

#Region "createUserSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' |
    ' | Method           : createUserSuffix()
    ' | Created by       : 
    ' | Date             : 
    ' | Description      : 
    ' | Parameters       : 
    ' | Returns          : ePLMSErr_OK
    ' |                    ePLMSErr_CONTINUE
    ' |                    ePLMSErr_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function createUserSuffix(ByRef givenConnection As eConnection, ByRef givenUser As eUser, ByRef Rc As Integer) As Integer
        Dim dummyFunctionName     As String
	Dim myeWorkbenchFolder    As New eWorkbenchFolder(givenConnection,givenUser)

	dummyFunctionName = "createUserSuffix"
	
	givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        Rc = ePLMS_CONTINUE

ExitFunction:
	givenConnection.Logger.handleLocalError(Rc,className,dummyFunctionName)
	givenConnection.Logger.handleError1(Rc,"Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & Rc & ")",0,"")
	givenConnection.Logger.handleFunctionEnd(Rc)

        Return (Rc)
    End Function
#End Region
#End Region
#End Region
    End Class
