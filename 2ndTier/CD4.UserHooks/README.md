﻿Jasmin @ 11-dec-2017
=============================
Output is redirected to "bin" folder. In order to make it more independent of "etc" folder structure. 
Original buildoutput path was @"C:\etc\Parallaksis\Collaboration Desktop\.Net Framework 4.0\3.4\Customers\Training\userHooks".

Please note that I am not aware of all dependencies and references on "etc" folder structure, so in order to make it compile, 
some hardcoded modifications need to be performed.