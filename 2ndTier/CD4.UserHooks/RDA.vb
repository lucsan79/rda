﻿Imports System.Globalization
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs


Public Class RDA
    Public Shared ReadOnly ListRootProjectContent As String = "Programs Contets Mngmnt"
    Public Shared ReadOnly ListRootProjectArea As String = "Programs Area Mngmnt"
    Private Shared Sub UpdateTotals(ByRef givenBusinessData As eBusinessData)

    End Sub
    Public Shared Function GetRdAOnRemedy(ByRef givenConnection As eConnection, givenCDC As String, givenSupplier As String, givenYear As String) As DataTable
        Dim myList As New eList(givenConnection)
        Dim startDate As String = ""
        Dim endDate As String = ""
        If myList.Load("REMEDY_RDA_RANGE") <> ePLMS_OK Then
            myList = New eList(givenConnection)
            myList.Name = "REMEDY_RDA_RANGE"
            If myList.Create = ePLMS_OK Then
                myList.addItem("start-date", "03.12")
                myList.addItem("end-date", "03.12")
            End If
        End If

        Dim myQuery As String = "SELECT								 " &
                                "	eId, 							 " &
                                "	Amount, 						 " &
                                "	baan_committed as Committed, 	 " &
                                "	LOWER(Supplier) AS [Supplier] ,	 " &
                                "	CDC								 " &
                                "from 								 " &
                                "	eplms.RDADB						 " &
                                "									 " &
                                "where 								 " &
                                "	eLevel='Closed' and 			 " &
                                "	eWorkflow='WF_RDA_01' and 		 " &
                                "	(CBS = 'R&D Hours' or CBS = 'Work Package')	and 		 " &
                                "	RequestedSupplier= 'REMEDY'		 "
        '" ( eCreateDate >= '" & startDate & "' and eCreateDate <= '" & endDate & "') "

        If Not String.IsNullOrEmpty(givenCDC) Then
            myQuery = myQuery & " and CDC = '" & givenCDC & "' "
        End If
        If Not String.IsNullOrEmpty(givenSupplier) Then
            myQuery = myQuery & " and Supplier = '" & givenSupplier & "' "
        End If
        'myQuery = myQuery & " and (eplms.GetAttributeValue('ActivityEnd',  eplms.eBusinessData.eId) >= '" & startDate & "' and eplms.GetAttributeValue('ActivityEnd',  eplms.eBusinessData.eId) <= '" & endDate & "')"
        myQuery = myQuery & " and (CONVERT(VARCHAR(8), ActivityEnd, 112) like '%" + givenYear + "%') "

        Dim myTable As DataTable = ExecuteSql(myQuery, givenConnection)
        Return myTable

    End Function
    Public Shared Sub CheckRemedyBudget(ByRef givenConnection As eConnection, givenYear As String, givenCDC As String, givenSupplier As String, givenAmount As String)
        If givenYear.Length > 4 Then
            givenYear = givenYear.Substring(0, 4)
        End If
        Dim myConnection = retrieveDBAConnection.getDbaConnection()
        Dim myTableRDA As DataTable = GetRdAOnRemedy(myConnection, givenCDC, givenSupplier, givenYear)
        Dim speso As Double = 0
        If myTableRDA IsNot Nothing AndAlso myTableRDA.Rows.Count > 0 Then
            For Each row As DataRow In myTableRDA.Rows
                Try
                    Dim committed As Double = eUtility.GetDoubleValue(row("Committed").ToString)

                    If (committed > 0) Then
                        speso = speso + committed
                    Else
                        speso = speso + eUtility.GetDoubleValue(row("Amount"))
                    End If
                Catch ex As Exception
                    Dim c As String = ""
                End Try
            Next
            'If IsDBNull(myTableRDA.Rows(0)(0)) Then
            '    speso = 0
            'Else
            '    speso = myTableRDA.Rows(0)(0)
            'End If
        End If
        Dim myQuery As String = "SELECT        ID, NAME, ADDRESS, CITY, ZIPCODE, CODE, BUYER, IsRemedy, Brand, " &
                                                        " (SELECT        BUDGET												   " &
                                                        " FROM            SUPPLIERBUDGETYEAR								   " &
                                                        " WHERE        (SUPPLIERID IN (F.ID)) AND (YEAR = '" & givenYear & "')) AS budget " &
                                                        " FROM            FORNITORI AS F									 " &
                                                        " WHERE        (IsRemedy = 1)	and Name='" & givenSupplier & "'									 " &
                                                        " ORDER BY NAME		"


        Dim myTableSupplier As DataTable = ExecuteSql(myQuery, myConnection) '"Select Budget from dbo.SUPPLIERBUDGETYEAR where Name='" & givenSupplier & "'")
        If myTableRDA IsNot Nothing And myTableSupplier IsNot Nothing Then
            If myTableSupplier.Rows.Count > 0 Then
                Dim myBudget As String = "" & myTableSupplier.Rows(0)("budget")
                For Each item As String In myBudget.Split("|".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
                    If item.ToLower.StartsWith(givenCDC.ToLower) Then
                        Dim allocated As String = item.Split("#")(1)
                        Dim value As Double = eUtility.GetDoubleValue(allocated)
                        Dim residuo As Double = allocated - speso - eUtility.GetDoubleValue(givenAmount)
                        If residuo < 0 Then
                            givenConnection.errorMessage = "Warning! The current RDA may not be finalized because the supplier's budget doesn't cover the Amount"
                            Throw New Exception("Warning! The current RDA may not be finalized because the supplier's budget doesn't cover the Amount")
                        End If
                    End If
                Next
            End If
        End If
    End Sub

    Public Shared Function RDACreatePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataUserDefinedAttributeList As ArrayList) As Integer

        'Gestione Scelta WF
        Dim rdaArea As String = ""
        Dim rdaProject As String = ""

        Dim logoutFlag As Boolean = False
        Dim outWorkflowNAme As String = ""

        Try


            If givenBusinessData.businessDataType = "RDA" Then
                Dim amount As String = ""
                Dim amountFormat As String = "euro"
                Dim piecePrice As String = ""
                Dim piecePriceFormat As String = ""
                Dim rdaMultipleCreation As Boolean = False
                Dim rdaMultiTranche As Boolean = False
                Dim isRemedy As Boolean = False
                Dim Supplier As String = ""
                Dim area As String = ""
                Dim mySpendingCurve As String = ""
                If givenBusinessDataUserDefinedAttributeList IsNot Nothing Then
                    Dim myNeweData As New eDataListElement
                    myNeweData.Name = ""
                    'For idx As Integer = 0 To givenBusinessDataUserDefinedAttributeList.Count - 1
                    '    Dim myData As eDataListElement = givenBusinessDataUserDefinedAttributeList(idx)
                    '    If myData.Name.ToLower = "activityend" Then
                    '        myNeweData = New eDataListElement
                    '        myNeweData.Name = "BaanIn_DataErosioneBudget"
                    '        myNeweData.Value = myData.Value
                    '    End If
                    'Next
                    If myNeweData.Name <> "" Then
                        givenBusinessDataUserDefinedAttributeList.Add(myNeweData)
                    End If
                    For Each edata As eDataListElement In givenBusinessDataUserDefinedAttributeList
                        If edata.Name.ToUpper = "AMOUNT" Then
                            amount = edata.Value
                        ElseIf edata.Name.ToUpper = "PiecePrice".ToUpper Then
                            piecePrice = edata.Value
                        ElseIf edata.Name.ToUpper = "RDASCOPE".ToUpper Then
                            If edata.Value.ToUpper = "MULTIPLE" Then
                                rdaMultipleCreation = True
                            ElseIf edata.Value.ToUpper = "TRANCHES" Then
                                rdaMultiTranche = True
                            End If
                        ElseIf edata.Name.ToUpper = "SupplierRemedy".ToUpper Then
                            Supplier = edata.Value
                            isRemedy = True
                        ElseIf edata.Name.ToUpper = "Supplier".ToUpper Then
                            Supplier = edata.Value
                            isRemedy = False
                        ElseIf edata.Name.ToUpper = "Area".ToUpper Then
                            area = edata.Value
                        ElseIf edata.Name.ToUpper = "rdaw1_spendingcurve".ToUpper Then
                            mySpendingCurve = edata.Value
                        End If
                    Next
                End If


                '1,211.53 $
                '1.211,53 €
                If amount.Trim.Length > 0 Then
                    If amount.Contains("$") Then
                        amountFormat = "dollar"
                        amount = amount.Replace("$", "").Trim
                        ' If IsNumeric(amount) Then
                        amount = amount.Replace(",", "")
                        'End If
                    ElseIf amount.Contains("€") Then
                        amountFormat = "euro"
                        amount = amount.Replace("€", "").Trim
                        ' If IsNumeric(amount) Then
                        amount = amount.Replace(".", "")
                        amount = amount.Replace(",", ".")
                        'End If
                    End If
                End If
                If piecePrice.Trim.Length > 0 Then
                    If piecePrice.Contains("$") Then
                        piecePriceFormat = "dollar"
                        piecePrice = piecePrice.Replace("$", "").Trim
                        'If IsNumeric(piecePrice) Then
                        piecePrice = piecePrice.Replace(",", "")
                        'End If
                    ElseIf piecePrice.Contains("€") Then
                        piecePriceFormat = "euro"
                        piecePrice = piecePrice.Replace("€", "").Trim
                        ' If IsNumeric(piecePrice) Then
                        piecePrice = piecePrice.Replace(".", "")
                        piecePrice = piecePrice.Replace(",", ".")
                        'End If
                    End If
                End If

                If givenBusinessDataUserDefinedAttributeList IsNot Nothing Then
                    For idx As Integer = 0 To givenBusinessDataUserDefinedAttributeList.Count - 1
                        Dim edata As eDataListElement = givenBusinessDataUserDefinedAttributeList(idx)
                        If edata.Name.ToUpper = "Amount".ToUpper Then
                            edata.Value = amount
                            givenBusinessDataUserDefinedAttributeList(idx) = edata
                        ElseIf edata.Name.ToUpper = "PiecePrice".ToUpper Then
                            edata.Value = piecePrice
                            givenBusinessDataUserDefinedAttributeList(idx) = edata
                        ElseIf edata.Name.ToUpper = "AmountFormat".ToUpper Then
                            edata.Value = amountFormat
                            amountFormat = ""
                            givenBusinessDataUserDefinedAttributeList(idx) = edata
                        ElseIf edata.Name.ToUpper = "PiecePriceFormat".ToUpper Then
                            edata.Value = piecePriceFormat
                            piecePriceFormat = ""
                            givenBusinessDataUserDefinedAttributeList(idx) = edata
                        ElseIf edata.Name.ToUpper = "SupplierRemedy".ToUpper Then
                            edata.Name = "Supplier"
                            edata.Value = Supplier
                            givenBusinessDataUserDefinedAttributeList(idx) = edata
                        ElseIf edata.Name.ToUpper = "Contents".ToUpper Then
                            If edata.Value = "N.D." Then
                                edata.Value = ""
                            End If
                            givenBusinessDataUserDefinedAttributeList(idx) = edata
                        End If
                    Next

                End If

                If amountFormat <> "" Then
                    Dim myeData As New eDataListElement
                    myeData.Name = "AmountFormat"
                    myeData.Value = amountFormat
                    givenBusinessDataUserDefinedAttributeList.Add(myeData)
                End If

                If piecePriceFormat <> "" Then
                    Dim myeData As New eDataListElement
                    myeData.Name = "PiecePriceFormat"
                    myeData.Value = piecePriceFormat
                    givenBusinessDataUserDefinedAttributeList.Add(myeData)
                End If


                If isRemedy And Supplier <> "" And area <> "" Then

                    Dim BudgetOK As Boolean = ValidateBudgetOnSupplier(amount, amountFormat, Supplier, area)
                    For idx As Integer = 0 To givenBusinessDataUserDefinedAttributeList.Count - 1
                        Dim edata As eDataListElement = givenBusinessDataUserDefinedAttributeList(idx)
                        If edata.Name.ToUpper = "Supplier".ToUpper Then
                            edata.Value = Supplier
                            givenBusinessDataUserDefinedAttributeList(idx) = edata
                            Exit For
                        End If
                    Next

                    If BudgetOK = False Then
                        givenConnection.messageStack.Clear()
                        givenConnection.errorMessage = "RDA cost exceeds the remaining budget of selected supplier"
                        Return -9999

                    End If
                End If
                If givenBusinessData.Name = "#" Or givenBusinessData.Name = "AUTOMATIC" Then
                    Dim myeSequence As eSequenceGenerator
                    Dim rdaNameProgressNumber As String = "#"


                    If GetSequenceGenerator(retrieveDBAConnection.getDbaConnection, myeSequence, rdaMultipleCreation, rdaMultiTranche) = ePLMS_OK Then
                        If myeSequence.getNext(rdaNameProgressNumber) = ePLMS_OK Then
                            givenBusinessData.Name = rdaNameProgressNumber
                        End If
                    End If

                    If rdaMultipleCreation = True Then
                        givenBusinessData.Workflow = "WF_RDA_02" 'System.Configuration.ConfigurationSettings.AppSettings("WF_RDA_MULTICREATE_MATERIAL").ToString
                    End If

                    If rdaMultiTranche = True Then
                        Dim WF As String = "WF_RDA_03" '& System.Configuration.ConfigurationSettings.AppSettings("WF_RDA_MULTITRANCHES_CODESIGN")
                        givenBusinessData.Workflow = WF
                    End If
                End If
            ElseIf givenBusinessData.businessDataType = "DataExchange" Then
                If givenBusinessData.Name = "#" Or givenBusinessData.Name = "AUTOMATIC" Then
                    Dim myeSequence As eSequenceGenerator
                    Dim rdaBaanDENameProgressNumber As String = "#"

                    If GetSequenceGeneratorBaaN(retrieveDBAConnection.getDbaConnection, myeSequence) = ePLMS_OK Then
                        If myeSequence.getNext(rdaBaanDENameProgressNumber) = ePLMS_OK Then
                            givenBusinessData.Name = rdaBaanDENameProgressNumber
                        End If
                    End If
                End If


            End If

            '[-]------------------------------------------------------------------------------[-]
            ' | Autore:     Luca Sansone
            ' | Scopo:      generazione automatica rdadefinition per il solo ramo prestazioni
            ' |             
            ' | Requisito:  2.2 -> BID_ICT_eRDA.evolutiva.W1_20170801.docx
            If givenBusinessData.businessDataType = "RDA" Then
                AutomaticRDADEFINITION(givenConnection, givenBusinessData, givenBusinessDataUserDefinedAttributeList, ActionType.create)
            End If
            '[-]------------------------------------------------------------------------------[-]




        Finally

        End Try

        Return ePLMS_OK
    End Function
    Public Shared Function RDACreateSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataUserDefinedAttributeList As ArrayList) As Integer
        Dim rc As Integer = 0
        Dim amount As String = ""
        Dim piecePrice As String = ""
        Dim skipTextRDA As Boolean = True

        If givenBusinessData.businessDataType = "RDA" Then
            Dim shortDateFormat = eEnvConfigurationController.getEntry(givenConnection, "shortDateFormat")
            givenBusinessData.Reserve()
            givenBusinessData.changeAttribute("ActionDate", Now.ToString(shortDateFormat))
            givenBusinessData.Unreserve()
        End If

        If givenBusinessData.businessDataType = "RDA" And givenBusinessData.Workflow = "WF_RDA_01" Then
            'Verifico le condizioni del fast track
            skipTextRDA = False
            FastTrackManager.ManageFastTrack(givenBusinessData)
        ElseIf givenBusinessData.businessDataType = "RDA" And givenBusinessData.Workflow = "WF_RDA_01" And givenBusinessData.Key1 <> "" Then

        End If

        '[-]------------------------------------------------------------------------------[-]
        ' | Autore:     Luca Sansone
        ' | Scopo:      gestione RDA per spending curve
        ' |             
        ' | Requisito:  2.2 -> BID_ICT_eRDA.evolutiva.W1_20170801.docx
        If givenBusinessData.businessDataType = "RDA" Then
            Dim cbs As String = ""
            Dim spendingCurve As String = ""
            Dim rdascope As String = ""
            givenBusinessData.getAttribute("CBS", cbs)
            givenBusinessData.getAttribute("rdaw1_spendingcurve", spendingCurve)
            givenBusinessData.getAttribute("RDASCOPE", rdascope)
            If cbs.ToLower = "codesign" And spendingCurve <> "" AndAlso rdascope = "TRANCHES" Then
                Dim myWF As String = "WF_RDA_03"
                If givenBusinessData.Workflow = myWF Then
                    rc = GenerateSpendingCurve(givenConnection, givenBusinessData, spendingCurve)
                End If

            End If


            If cbs.Trim.ToLower = "Materials".ToLower Then
                Dim qnt As String = ""
                Dim PiecePriceFormat As String = ""

                givenBusinessData.getAttribute("PiecePrice", pieceprice)
                givenBusinessData.getAttribute("PiecePriceFormat", PiecePriceFormat)
                givenBusinessData.getAttribute("TotalQuantity", qnt)

                Dim val1 As Double = eUtility.GetDoubleValue(pieceprice)
                Dim val2 As Double = eUtility.GetDoubleValue(qnt)

                Dim myDB As New eBusinessData(retrieveDBAConnection.getDbaConnection)
                myDB.Load(givenBusinessData.Id)
                myDB.changeAttribute("Amount", Math.Round(val1 * val2, 3).ToString.Replace(",", "."))
                myDB.changeAttribute("AmountFormat", PiecePriceFormat)
            End If


        End If
        '[-]------------------------------------------------------------------------------[-]


        '[-]------------------------------------------------------------------------------[-]
        ' | Autore:     Luca Sansone
        ' | Scopo:      generazione automatica text rda per il solo ramo prestazioni
        ' |             
        ' | Requisito:  2.2 -> BID_ICT_eRDA.evolutiva.W1_20170801.docx
        If givenBusinessData.businessDataType = "RDA" And Not skipTextRDA Then
            AutomaticTextRDA(givenConnection, givenBusinessData, ActionType.create)
        End If
        '[-]------------------------------------------------------------------------------[-]



        Return rc

    End Function
    Private Shared Sub GetSupplierInfo(ByRef givenSupplierName As String, ByRef givenCost As String)
        Dim myQeury As String = "SELECT eDescription,eplms.GetSupplierBudget(eId) AS Budget FROM eplms.eBusinessData WHERE (eBusinessDataType = N'Supplier') AND (eName = '" & givenSupplierName & "')"
        Dim myTable As DataTable = ExecuteSql(myQeury, retrieveDBAConnection.getDbaConnection)
        givenCost = ""
        If myTable IsNot Nothing AndAlso myTable.Rows.Count > 0 Then
            givenSupplierName = myTable.Rows(0)("eDescription")
            If myTable.Rows(0)("Budget") IsNot DBNull.Value Then
                givenCost = "" & myTable.Rows(0)("Budget")
            End If
        End If

    End Sub
    Private Shared Function ValidateBudgetOnSupplier(givenBudget As String, givenBudgetFormat As String, ByRef givenSupplier As String, givenArea As String)
        Dim assignedBudget As String = ""
        givenBudget = givenBudget.Replace(",", ".")
        GetSupplierInfo(givenSupplier, assignedBudget)

        Dim myQuery As String = "SELECT        SUM(eplms.GetAttributeCurrencyValue('Amount',eplms.eBusinessDataAttribute.eBusinessDataId)) as Amount  			" &
                            "FROM            eplms.eBusinessDataAttribute INNER JOIN																	 	" &
                            "                         eplms.eAttribute ON eplms.eBusinessDataAttribute.eAttributeId = eplms.eAttribute.eId INNER JOIN		" &
                            "                         eplms.eBusinessData ON eplms.eBusinessDataAttribute.eBusinessDataId = eplms.eBusinessData.eId			" &
                            "WHERE        																													" &
                            "(eplms.eBusinessDataAttribute.eValue = N'" & givenSupplier.ToLower & "') AND 											" &
                            "(eplms.eAttribute.eName = N'Supplier') and																						" &
                            " eplms.eBusinessData.eLevel<>'Cancelled' and 																					" &
                            " eplms.eBusinessData.eWorkflow='WF_RDA_01' and 																				" &
                            " (eplms.GetAttributeValue('CBS',  eplms.eBusinessData.eId) = 'R&D Hours' or eplms.GetAttributeValue('CBS',  eplms.eBusinessData.eId) = 'Work Package') and 	" &
                            " eplms.GetAttributeValue('Area',  eplms.eBusinessData.eId) = '" & givenArea & "' and 											" &
                            " eplms.GetAttributeValue('RequestedSupplier',  eplms.eBusinessData.eId) = 'REMEDY' 											"

        'Dim myQuery As String = "SELECT  " &
        '                       "SUM(eplms.GetAttributeCurrencyValue('Amount',eplms.ebusinessdata.eid)) as Amount " &
        '                       "from eplms.eBusinessData where eBusinessDataType='RDA' and eLevel<>'Cancelled' " &
        '                       "and eWorkflow='WF_RDA_01' " &
        '                       "and eplms.GetAttributeValue('CBS',  eplms.eBusinessData.eId) = 'R&D Hours' " &
        '                       "and eplms.GetAttributeValue('Area',  eplms.eBusinessData.eId) = '" & givenArea & "' " &
        '                       "and eplms.GetAttributeValue('RequestedSupplier',  eplms.eBusinessData.eId) = 'REMEDY' " &
        '                       "and LOWER(eplms.GetAttributeValue('Supplier',  eplms.eBusinessData.eId)) = '" & givenSupplier.ToLower & "'"
        Dim retValue As Boolean = True
        Dim Actual As Double = 0
        'Dim myTable As DataTable = ExecuteSql(myQuery, retrieveDBAConnection.getDbaConnection)
        Dim totaleRdA As DataTable = ExecuteSql(myQuery, retrieveDBAConnection.getDbaConnection)
        myQuery = String.Format("select eplms.CalculateAttributeCurrencyValue('{0}','{1}')", givenBudget, givenBudgetFormat)
        Dim myBudgetTable As DataTable = ExecuteSql(myQuery, retrieveDBAConnection.getDbaConnection)
        If totaleRdA IsNot Nothing AndAlso totaleRdA.Rows.Count = 0 AndAlso totaleRdA.Rows(0)(0) Is DBNull.Value Then
            totaleRdA = Nothing
        Else
            Actual = IIf(totaleRdA.Rows(0)(0) Is DBNull.Value, 0, totaleRdA.Rows(0)(0))
        End If
        If myBudgetTable IsNot Nothing AndAlso myBudgetTable.Rows.Count = 0 Then
            myBudgetTable = Nothing
        End If

        If myBudgetTable IsNot Nothing Then
            Dim currenValue As Double = myBudgetTable.Rows(0)(0)
            If currenValue > 0 Then
                'myQuery = "SELECT eplms.GetSupplierBudget(eId) AS Budget FROM eplms.eBusinessData WHERE (eBusinessDataType = N'Supplier') AND (eDescription = '" & givenSupplier & "')"
                'Dim myTableSupplier As DataTable = ExecuteSql(myQuery, retrieveDBAConnection.getDbaConnection)
                'If myTableSupplier IsNot Nothing AndAlso myTableSupplier.Rows.Count > 0 Then
                'PWTDS#0|VI&V#0|PROTOTYPES#0|BODY#20|CHASSIS#0|POWERTRAIN#0|INTERIORS#0|EE#0|ENGINE SYSTEMS#0|CONCEPT#0|MANAGEMENT#0|MANUFACTURING ENGINEERING#0|QUALITY#0|STYLE#0|PWTGAS#0
                Dim myValues As String = assignedBudget
                For Each item As String In myValues.Split("|")
                    If item.Contains("#") Then
                        Dim myArea As String = item.Split("#")(0)
                        Dim myValue As String = item.Split("#")(1)
                        If myArea.ToLower = givenArea.ToLower Then
                            Dim allBudget As Double = eUtility.GetDoubleValue(myValue)
                            Dim rest As Double = allBudget - currenValue - Actual
                            If rest < 0 Then
                                retValue = False
                            End If
                            Exit For
                        End If
                    End If
                Next
                ' End If
            End If
        End If

        Return retValue
    End Function

    Public Shared Function RDAModifyPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataUserDefinedAttributeList As ArrayList) As Integer

        'Gestione Scelta WF
        Dim rdaArea As String = ""
        Dim rdaProject As String = ""

        Dim logoutFlag As Boolean = False
        Dim outWorkflowNAme As String = ""

        Try
            If givenBusinessData.businessDataType = "RDA" Then
                Dim amount As String = ""
                Dim amountFormat As String = ""
                Dim piecePrice As String = ""
                Dim piecePriceFormat As String = ""
                If givenBusinessDataUserDefinedAttributeList IsNot Nothing Then
                    For Each edata As eDataListElement In givenBusinessDataUserDefinedAttributeList
                        If edata.Name.ToUpper = "AMOUNT" Then
                            amount = edata.Value
                        ElseIf edata.Name.ToUpper = "PiecePrice".ToUpper Then
                            piecePrice = edata.Value
                        End If
                    Next
                End If
                '1,211.53 $
                '1.211,53 €
                If amount.Trim.Length > 0 Then
                    If amount.Contains("$") Then
                        amountFormat = "dollar"
                        amount = amount.Replace("$", "").Trim
                        'If IsNumeric(amount) Then
                        amount = amount.Replace(",", "")
                        'End If
                    ElseIf amount.Contains("€") Then
                        amountFormat = "euro"
                        amount = amount.Replace("€", "").Trim
                        'If IsNumeric(amount) Then
                        amount = amount.Replace(".", "")
                        amount = amount.Replace(",", ".")
                        'End If
                    End If

                    If piecePrice.Trim.Length > 0 Then
                        If piecePrice.Contains("$") Then
                            piecePriceFormat = "dollar"
                            piecePrice = piecePrice.Replace("$", "").Trim
                            'If IsNumeric(piecePrice) Then
                            piecePrice = piecePrice.Replace(",", "")
                            'End If
                        ElseIf piecePrice.Contains("€") Then
                            piecePriceFormat = "euro"
                            piecePrice = piecePrice.Replace("€", "").Trim
                            'If IsNumeric(piecePrice) Then
                            piecePrice = piecePrice.Replace(".", "")
                            piecePrice = piecePrice.Replace(",", ".")
                            'End If
                        End If
                    End If
                    If givenBusinessDataUserDefinedAttributeList IsNot Nothing Then
                        For idx As Integer = 0 To givenBusinessDataUserDefinedAttributeList.Count - 1
                            Dim edata As eDataListElement = givenBusinessDataUserDefinedAttributeList(idx)
                            If edata.Name.ToUpper = "Amount".ToUpper Then
                                edata.Value = amount
                                givenBusinessDataUserDefinedAttributeList(idx) = edata
                            ElseIf edata.Name.ToUpper = "PiecePrice".ToUpper Then
                                edata.Value = piecePrice
                                givenBusinessDataUserDefinedAttributeList(idx) = edata
                            ElseIf edata.Name.ToUpper = "AmountFormat".ToUpper Then
                                edata.Value = amountFormat
                                amountFormat = ""
                                givenBusinessDataUserDefinedAttributeList(idx) = edata
                            ElseIf edata.Name.ToUpper = "PiecePriceFormat".ToUpper Then
                                edata.Value = piecePriceFormat
                                piecePriceFormat = ""
                                givenBusinessDataUserDefinedAttributeList(idx) = edata
                            End If
                        Next
                    End If

                    If amountFormat <> "" Then
                        Dim myeData As New eDataListElement
                        myeData.Name = "AmountFormat"
                        myeData.Value = amountFormat
                        givenBusinessDataUserDefinedAttributeList.Add(myeData)
                    End If

                    If piecePriceFormat <> "" Then
                        Dim myeData As New eDataListElement
                        myeData.Name = "PiecePriceFormat"
                        myeData.Value = piecePriceFormat
                        givenBusinessDataUserDefinedAttributeList.Add(myeData)
                    End If


                End If
            End If
            If givenBusinessData.businessDataType.ToUpper = "L2ROW" Then
                If givenBusinessDataUserDefinedAttributeList IsNot Nothing Then
                    For idx As Integer = 0 To givenBusinessDataUserDefinedAttributeList.Count - 1
                        Dim myeData As eDataListElement = givenBusinessDataUserDefinedAttributeList(idx)
                        If myeData.Name.ToUpper = "ENDDATE" Then
                            If myeData.Value.Contains(" ") Then
                                myeData.Value = myeData.Value & " 23:59:59"
                            ElseIf myeData.Value.Contains(".") Then
                                myeData.Value = myeData.Value & " 23:59:59"
                            ElseIf myeData.Value.Length > 8 And myeData.Value.EndsWith("000000") Then
                                myeData.Value = myeData.Value.Replace("000000", "235959")
                            End If
                            givenBusinessDataUserDefinedAttributeList(idx) = myeData
                        End If
                    Next
                End If
            End If

            '[-]------------------------------------------------------------------------------[-]
            ' | Autore:     Luca Sansone
            ' | Scopo:      generazione automatica rdadefinition per il solo ramo prestazioni
            ' |             
            ' | Requisito:  2.2 -> BID_ICT_eRDA.evolutiva.W1_20170801.docx
            If givenBusinessData.businessDataType = "RDA" Then
                AutomaticRDADEFINITION(givenConnection, givenBusinessData, givenBusinessDataUserDefinedAttributeList, ActionType.modify)
            End If
            '[-]------------------------------------------------------------------------------[-]
        Finally

        End Try

        Return ePLMS_OK
    End Function
    Public Shared Function RDAModifySuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataUserDefinedAttributeList As ArrayList) As Integer
        Dim skipTextRDA As Boolean = True

        If givenBusinessData.businessDataType = "RDA" AndAlso givenBusinessData.Workflow = "WF_RDA_01" Then
            skipTextRDA = False
        End If
        Dim WFSpendingCurve As String = "WF_RDA_03"
        If WFSpendingCurve = "" Then
            WFSpendingCurve = "WF_RDA_03"
        End If

        If givenBusinessData.businessDataType = "RDA" Then
            Dim myCBS As String = ""
            givenBusinessData.getAttribute("CBS", myCBS)
            If myCBS.Trim.ToLower = "Materials".ToLower Then
                Dim qnt As String = ""
                Dim pieceprice As String = ""
                Dim PiecePriceFormat As String = ""

                givenBusinessData.getAttribute("PiecePrice", pieceprice)
                givenBusinessData.getAttribute("PiecePriceFormat", PiecePriceFormat)
                givenBusinessData.getAttribute("TotalQuantity", qnt)

                Dim val1 As Double = eUtility.GetDoubleValue(pieceprice)
                Dim val2 As Double = eUtility.GetDoubleValue(qnt)

                Dim myDB As New eBusinessData(retrieveDBAConnection.getDbaConnection)
                myDB.Load(givenBusinessData.Id)
                myDB.changeAttribute("Amount", Math.Round(val1 * val2, 3).ToString.Replace(",", "."))
                myDB.changeAttribute("AmountFormat", PiecePriceFormat)
            End If
        End If


        If givenBusinessData.businessDataType = "RDA" AndAlso givenBusinessData.Workflow = WFSpendingCurve Then
            UpdateSpendingCurve(givenConnection, givenBusinessData, givenBusinessDataUserDefinedAttributeList)
            skipTextRDA = True
        End If

        If givenBusinessData.Workflow = "WF_CANCEL" Then
            skipTextRDA = True
        End If
        '[-]------------------------------------------------------------------------------[-]
        ' | Autore:     Luca Sansone
        ' | Scopo:      generazione automatica text rda per il solo ramo prestazioni
        ' |             
        ' | Requisito:  2.2 -> BID_ICT_eRDA.evolutiva.W1_20170801.docx
        If givenBusinessData.businessDataType = "RDA" And Not skipTextRDA Then
            AutomaticTextRDA(givenConnection, givenBusinessData, ActionType.modify)
        End If
        '[-]------------------------------------------------------------------------------[-]

        Return ePLMS_OK
    End Function
    Public Shared Function RDAGenerateMailPrefix(ByRef givenConnection As eConnection, ByVal givenBusinessData As eBusinessData, ByVal givenBusinessDataCurrentLevel As String, ByRef givenTextMailSubject As String, ByRef givenTextMailBody As String, ByRef givenMailToList() As String, ByRef returnMailSubject As String, ByRef returnMailBody As String) As Integer

        'Gestione Scelta WF

        Dim logoutFlag As Boolean = False
        Dim myeRoleNotificationTaskList() As eRoleNotificationTaskEntry
        Dim myMailList As New Generic.List(Of String)
        Dim myRole As eRole
        Try
            Dim newLevel As String = ""
            eBusinessData.getAttribute(givenConnection, givenBusinessData.Id, "Level", newLevel)
            If givenBusinessData.businessDataType = "RDA" Then
                myRole = New eRole(retrieveDBAConnection.getDbaConnection)
                Dim myeWorkflow As New eWorkflow(retrieveDBAConnection.getDbaConnection)
                If myeWorkflow.Load(givenBusinessData.Workflow) = ePLMS_OK Then
                    Dim myeTask As New eTask(retrieveDBAConnection.getDbaConnection, myeWorkflow)
                    If givenTextMailSubject.ToLower.Trim.EndsWith("- PR Validation".ToLower) Then
                        GetPRAreaUserMailList(givenBusinessData, retrieveDBAConnection.getDbaConnection, myMailList, "PR")
                        myeWorkflow.getTask("REQUEST_VALIDATE", myeTask)
                        myeTask.getRoleNotificationTaskList(myeRoleNotificationTaskList)
                        If myeRoleNotificationTaskList IsNot Nothing Then
                            For Each role As eRoleNotificationTaskEntry In myeRoleNotificationTaskList
                                If myRole.Load(role.RoleId) = ePLMS_OK Then
                                    If Not myRole.Name.StartsWith("PR_") Then
                                        GetPRAreaUserMailList(givenBusinessData, retrieveDBAConnection.getDbaConnection, myMailList, myRole.Name)
                                    End If
                                End If
                            Next
                        End If

                    ElseIf givenTextMailSubject.ToLower.Trim.EndsWith(" - PR Validation Rejected".ToLower) Then
                        'myeWorkflow.getTask("PR_CHECK_FAILURE", myeTask)
                        'myeTask.getRoleNotificationTaskList(myeRoleNotificationTaskList)
                        'If myeRoleNotificationTaskList IsNot Nothing Then
                        '    For Each role As eRoleNotificationTaskEntry In myeRoleNotificationTaskList
                        '        If myRole.Load(role.RoleId) = ePLMS_OK Then
                        '            If Not myRole.Name.StartsWith("PR_") Then
                        '                GetPRAreaUserMailList(givenBusinessData, myAdminConnection, myMailList, myRole.Name)
                        '            End If
                        '        End If
                        '    Next
                        'End If
                    ElseIf givenTextMailSubject.ToLower.Trim.EndsWith(" - CMS Validation Rejected".ToLower) Then
                        myeWorkflow.getTask("CMS_CHECK_FAILURE", myeTask)
                        GetPRAreaUserMailList(givenBusinessData, retrieveDBAConnection.getDbaConnection, myMailList, "PR")
                        myeTask.getRoleNotificationTaskList(myeRoleNotificationTaskList)
                        Dim myUser As New eUser(givenConnection)
                        If myUser.Load(givenBusinessData.createUser) = ePLMS_OK Then
                            Dim myArrayPerson = New ArrayList
                            myUser.getPersonList(myArrayPerson)
                            If myArrayPerson IsNot Nothing Then
                                Dim myPerson As New ePerson(givenConnection)
                                For Each person As eDataListElement In myArrayPerson
                                    If myPerson.Load(person.Name) = ePLMS_OK Then
                                        If Not myMailList.Contains(myPerson.eMail) Then
                                            myMailList.Add(myPerson.eMail)
                                        End If
                                    End If
                                Next
                            End If
                        End If

                        If myeRoleNotificationTaskList IsNot Nothing Then
                            For Each role As eRoleNotificationTaskEntry In myeRoleNotificationTaskList
                                If myRole.Load(role.RoleId) = ePLMS_OK Then
                                    If Not myRole.Name.StartsWith("PR_") Then
                                        GetPRAreaUserMailList(givenBusinessData, retrieveDBAConnection.getDbaConnection, myMailList, myRole.Name)
                                    End If
                                End If
                            Next
                        End If
                    ElseIf givenTextMailSubject.ToLower.Trim.EndsWith("- PMM CONTROL CHECK".ToLower) OrElse
                        givenTextMailSubject.ToLower.Trim.EndsWith("- PPM CONTROL CHECK".ToLower) Then
                        GetPWTUserMailListManagement(givenBusinessData, givenConnection, givenMailToList, myMailList)
                    ElseIf givenTextMailSubject.ToLower.Trim.EndsWith("- RDA Control Rejected".ToLower) Then
                        'myeWorkflow.getTask("RDA_CONTROL_FEAILURE", myeTask)
                    ElseIf givenTextMailSubject.ToLower.Trim.EndsWith("- RDA Clousure Rejected".ToLower) Then
                        ' myeWorkflow.getTask("CLOUSER_CHECK_FAILED", myeTask)
                    ElseIf givenTextMailSubject.ToLower.Trim.EndsWith("- RDA CLOSED".ToLower) Then

                        GetClosedUserMailList(givenBusinessData, givenConnection, myMailList)

                    End If
                End If

                If myMailList IsNot Nothing AndAlso myMailList.Count > 0 Then
                    givenMailToList = New String(1023) {}
                    For idx As Integer = 0 To myMailList.ToArray.Length - 1
                        givenMailToList(idx) = myMailList.ToArray(idx)
                    Next
                ElseIf givenTextMailSubject.ToLower.Trim.EndsWith("- PR Validation".ToLower) Then
                    myMailList = New List(Of String)
                    Dim myGroup As New eGroup(retrieveDBAConnection.getDbaConnection)
                    If myGroup.Load("RecoveryMail") = ePLMS_OK Then
                        Dim myUsers As New ArrayList
                        myGroup.getUserList(myUsers)
                        If myUsers IsNot Nothing Then
                            Dim myeUser As New eUser(retrieveDBAConnection.getDbaConnection)
                            For Each user As eDataListElement In myUsers
                                If myeUser.Load(user.Name) = ePLMS_OK Then
                                    Dim myPersonsList As New ArrayList
                                    myeUser.getPersonList(myPersonsList)
                                    If myPersonsList IsNot Nothing Then
                                        Dim myPerson As New ePerson(retrieveDBAConnection.getDbaConnection)
                                        For Each person As eDataListElement In myPersonsList
                                            If myPerson.Load(person.Name) = ePLMS_OK Then
                                                If Not myMailList.Contains(myPerson.eMail) And myPerson.eMail <> "" Then
                                                    myMailList.Add(myPerson.eMail)
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            Next
                        End If
                    End If
                    givenMailToList = New String(1023) {}
                    For idx As Integer = 0 To myMailList.ToArray.Length - 1
                        givenMailToList(idx) = myMailList.ToArray(idx)
                    Next
                End If
            End If


            Try

                Dim myTemplate As String = "Function=({0});Class=({1});Message=({2})"
                Dim myMailingTo As String = ""
                If givenMailToList IsNot Nothing AndAlso givenMailToList.Length > 0 Then
                    For Each item As String In givenMailToList
                        If item & "" = "" Then
                            Exit For
                        Else
                            myMailingTo &= item & ";"
                        End If
                    Next
                End If

                Dim myMessage As String = String.Format("RDA:{0}, Subject:{1}, mailto:{2}", givenBusinessData.Name, returnMailSubject, myMailingTo)
                givenConnection.Logger.writeToLog(myMessage, "MailDebugMode")

            Catch ex As Exception

            End Try
        Finally

        End Try

        Return ePLMS_OK
    End Function
    Private Shared Sub GetPRAreaUserMailList(givenBusinessData As eBusinessData, givenConnection As eConnection, ByRef myUserMail As Generic.List(Of String), roleToNotify As String)
        Dim Area As String = ""
        Dim myRole As String = ""
        Dim myUserList As New Generic.List(Of String)
        Dim myArrayPerson As ArrayList
        Dim myUser As New eUser(givenConnection)
        Dim myPerson As New ePerson(givenConnection)
        Dim RDACompetence As String = ""
        Dim rdaCBS As String = ""
        Dim cdc As String = ""



        givenBusinessData.getAttribute("Area", Area)
        givenBusinessData.getAttribute("RDACompetence", RDACompetence)
        givenBusinessData.getAttribute("CBS", rdaCBS)
        givenBusinessData.getAttribute("CDC", cdc)
        If roleToNotify = "PR" Then

            If Not String.IsNullOrEmpty(RDACompetence) Then

                If (RDACompetence.ToUpper = "VEHICLE" Or RDACompetence.ToUpper = "POWERTRAIN") AndAlso (rdaCBS.ToUpper = "R&D HOURS" Or rdaCBS.ToUpper = "WORK PACKAGE") Then

                    Dim mySelectedCDC As String = eUtility.GetCDCRoleNameForSearch(eUtility.DynamicRoleNamePrefix.PlannerOfCDC, cdc)
                    Dim myQuery As String = "SELECT    eplms.eUser.eId  as cdcplannerId,    eplms.eUser.eName AS cdcplanner " &
                                            " From eplms.eProjectRole INNER Join " &
                                            " eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId INNER JOIN " &
                                            " eplms.eProject ON eplms.eProjectRole.eProjectId = eplms.eProject.eId INNER JOIN " &
                                            " eplms.eProjectRoleUser ON eplms.eProjectRole.eId = eplms.eProjectRoleUser.eProjectRoleId INNER JOIN " &
                                            " eplms.eUser ON eplms.eProjectRoleUser.eUserId = eplms.eUser.eId " &
                                            " WHERE        (eplms.eProject.eName = N'RDA') AND (eplms.eRole.eName LIKE N'{0}')"
                    myQuery = String.Format(myQuery, mySelectedCDC)
                    Dim myTable As DataTable = RDA.ExecuteSql(myQuery, givenConnection)
                    If myTable IsNot Nothing AndAlso myTable.Rows.Count > 0 Then
                        For Each row As DataRow In myTable.Rows
                            If Not myUserList.Contains(row("cdcplanner").ToString.Trim.ToLower) Then
                                myUserList.Add(row("cdcplanner").ToString.Trim.ToLower)
                            End If
                        Next
                    End If
                Else
                    Dim myQuery As String = "" &
                       "Select        eplms.eListItem.eValue As RoleName, eListItem_3.eValue As UserName                                    " &
                       "FROM            eplms.eListItem INNER JOIN                                                                                    " &
                       "                         eplms.eListItem As eListItem_1 On eplms.eListItem.eListId = eListItem_1.eValueId INNER JOIN          " &
                       "                         eplms.eListItem As eListItem_2 On eListItem_1.eListId = eListItem_2.eValueId     INNER JOIN          " &
                       "                         eplms.eListItem As eListItem_3 On eplms.eListItem.eValueId = eListItem_3.eListId                     " &
                       "WHERE        (eListItem_2.eValue = 'PROJECT_CDCAPPROVERS') AND (eListItem_1.eValue = '{0}') "


                    If RDACompetence.ToUpper = "TRANSVERSAL" Then
                        myQuery = "SELECT        eplms.eListItem.eValue AS RoleName, eListItem_3.eValue  as ActivityNPI, eListItem_4.eValue AS UserName                                   " &
                            "FROM            eplms.eListItem INNER JOIN                                                                                    " &
                            "                         eplms.eListItem AS eListItem_1 ON eplms.eListItem.eListId = eListItem_1.eValueId INNER JOIN          " &
                            "                         eplms.eListItem AS eListItem_2 ON eListItem_1.eListId = eListItem_2.eValueId     INNER JOIN          " &
                            "                         eplms.eListItem AS eListItem_3 ON eplms.eListItem.eValueId = eListItem_3.eListId INNER JOIN          " &
                            "                         eplms.eListItem AS eListItem_4 ON eListItem_3.eValueId = eListItem_4.eListId                     " &
                            "WHERE        (eListItem_2.eValue = 'PROJECT_CDCAPPROVERS') AND (eListItem_1.eValue = '{0}') "
                    End If

                    myQuery = String.Format(myQuery, givenBusinessData.Project)
                    Dim myTable As DataTable = RDA.ExecuteSql(myQuery, givenConnection)
                    If myTable IsNot Nothing Then

                        cdc = ""
                        Dim ActivityNPI As String = ""
                        givenBusinessData.getAttribute("CDC", cdc)
                        givenBusinessData.getAttribute("ActivityNPI", ActivityNPI)
                        If Not String.IsNullOrEmpty(RDACompetence) Then
                            Dim selectRows() As DataRow
                            Select Case RDACompetence.ToUpper
                                Case "VEHICLE"
                                    myRole = "PRCDC_VEHICLE"
                                    selectRows = myTable.Select("Rolename='" & cdc.ToUpper & "'")
                                Case "POWERTRAIN"
                                    myRole = "PRCDC_POWERTRAIN"
                                    selectRows = myTable.Select("Rolename='POWERTRAIN'")
                                Case "TRANSVERSAL"
                                    myRole = "PRCDC_TRANSVERSAL"
                                    selectRows = myTable.Select("Rolename='" & cdc.ToUpper & "' and ActivityNPI='" & ActivityNPI.ToUpper & "'")

                            End Select
                            If selectRows.Length > 0 Then

                                myUserList.Clear()
                                Select Case RDACompetence.ToUpper
                                    Case "POWERTRAIN"
                                        For Each row As DataRow In selectRows
                                            myUserList.Add(row("UserName"))
                                        Next
                                    Case Else
                                        myUserList.Add(selectRows(0)("UserName"))
                                End Select

                            End If
                        End If
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(Area) Then
                Area = Area.Replace(" ", "_")
                Dim myProject As New eProject(givenConnection)
                myProject.Load(givenBusinessData.Project)
                Dim returnProjectRoleList() As eProjectRole
                myProject.getAllProjectRoleList(returnProjectRoleList)
                For Each myProjectRole As eProjectRole In returnProjectRoleList
                    If myProjectRole.roleName.ToUpper.EndsWith(Area.ToUpper) AndAlso myProjectRole.roleName.ToUpper.StartsWith("PR_") Then
                        myRole = myProjectRole.roleName
                        Dim returnProjectRoleUserList() As eProjectRoleUser
                        myProjectRole.getProjectRoleUserList(returnProjectRoleUserList)

                        If Not returnProjectRoleUserList Is Nothing Then
                            For Each myProjectRoleUser As eProjectRoleUser In returnProjectRoleUserList
                                If Not myUserList.Contains(myProjectRoleUser.userName) Then
                                    myUserList.Add(myProjectRoleUser.userName)
                                End If
                            Next
                        End If
                    End If
                Next
            End If


            If myUserList IsNot Nothing Then
                For Each user As String In myUserList
                    If myUser.Load(user) = ePLMS_OK Then
                        myArrayPerson = New ArrayList
                        myUser.getPersonList(myArrayPerson)
                        If myArrayPerson IsNot Nothing Then
                            For Each person As eDataListElement In myArrayPerson
                                If myPerson.Load(person.Name) = ePLMS_OK Then
                                    If Not myUserMail.Contains(myPerson.eMail) Then
                                        myUserMail.Add(myPerson.eMail)
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next
            End If


        ElseIf roleToNotify.ToLower = "createuser" Then
            If myUser.Load(givenBusinessData.createUser) = ePLMS_OK Then
                myArrayPerson = New ArrayList
                myUser.getPersonList(myArrayPerson)
                If myArrayPerson IsNot Nothing Then
                    For Each person As eDataListElement In myArrayPerson
                        If myPerson.Load(person.Name) = ePLMS_OK Then
                            If Not myUserMail.Contains(myPerson.eMail) Then
                                myUserMail.Add(myPerson.eMail)
                            End If
                        End If
                    Next
                End If
            End If

        End If

    End Sub

    Private Shared Sub GetPWTUserMailListManagement(givenBusinessData As eBusinessData, givenConnection As eConnection, ByRef apiMail() As String, ByRef myUserMail As Generic.List(Of String))
        '''''BYPASS
        'Dim rdaCompetence As String = ""

        'givenBusinessData.getAttribute("RDACompetence", rdaCompetence)

        'Dim myListPWTMail As DataTable = RDA.ExecuteSql("SELECT       eplms.ePerson.eEMail
        '                FROM            eplms.ePerson INNER JOIN
        '                 eplms.eUserPerson ON eplms.ePerson.eId = eplms.eUserPerson.ePersonId INNER JOIN
        '                 eplms.eGroup INNER JOIN
        '                 eplms.eGroupUser ON eplms.eGroup.eId = eplms.eGroupUser.eGroupId ON eplms.eUserPerson.eUserId = eplms.eGroupUser.eUserId WHERE (eplms.eGroup.eName = N'PWT CONTROLLER')", givenConnection)

        'myUserMail.Clear()
        'If myListPWTMail IsNot Nothing Then
        '    For Each mail As String In apiMail
        '        If String.IsNullOrEmpty(mail) Then
        '            Continue For
        '        End If
        '        Dim mailInPWT() As DataRow = myListPWTMail.Select("eEMail='" & mail & "'")
        '        If rdaCompetence.ToUpper = "POWERTRAIN" AndAlso mailInPWT.Length > 0 Then
        '            myUserMail.Add(mail)
        '        ElseIf rdaCompetence.ToUpper <> "POWERTRAIN" AndAlso mailInPWT.Length = 0 Then
        '            myUserMail.Add(mail)
        '        End If
        '    Next
        'End If


    End Sub

    Private Shared Sub GetClosedUserMailList(givenBusinessData As eBusinessData, givenConnection As eConnection, ByRef myUserMail As Generic.List(Of String))
        Dim Area As String = ""
        Dim myRole As String = ""
        Dim myUserList As New Generic.List(Of String)
        Dim myArrayPerson As ArrayList
        Dim myUser As New eUser(givenConnection)
        Dim myPerson As New ePerson(givenConnection)
        Dim myTable As DataTable
        Dim myFind As New eFind(givenConnection)

        myFind.queryString = "SELECT   * " &
                            "FROM eplms.eBusinessDataCheckHistory " &
                            "WHERE(eBusinessDataId = {0})" &
                            "ORDER BY eDate DESC"

        myFind.queryString = String.Format("$(" & myFind.queryString & ")", givenBusinessData.Id)
        myFind.Prepare(Nothing)
        myFind.Execute(myTable)
        If myTable IsNot Nothing AndAlso myTable.Rows.Count > 0 Then

            For Each checkRow As DataRow In myTable.Rows
                If Not myUserList.Contains(checkRow("eUser")) Then
                    myUserList.Add(checkRow("eUser"))
                End If
                'Dim extraUser() As String = getUserGroupOnTask(checkRow("eTaskId"), givenConnection)
                'For Each extra As String In extraUser
                '    If Not myUserList.Contains(extra) Then
                '        myUserList.Add(extra)
                '    End If
                'Next

                'extraUser = getUserOnTask(checkRow("eTaskId"), givenConnection)
                'For Each extra As String In extraUser
                '    If Not myUserList.Contains(extra) Then
                '        myUserList.Add(extra)
                '    End If
                'Next

                If checkRow("eCheck").ToString.ToUpper = "Require Validation".ToUpper Then
                    Exit For
                End If
            Next
            Try
                Dim myGroup As New eGroup(givenConnection)
                If myGroup.Load("SuperVisore") = ePLMS_OK Then
                    Dim myUsers As New ArrayList
                    myGroup.getUserList(myUsers)
                    If myUsers IsNot Nothing Then
                        For Each user As eDataListElement In myUsers
                            If Not myUserList.Contains(user.Name) Then
                                myUserList.Add(user.Name)
                            End If
                        Next
                    End If
                End If
            Catch ex As Exception

            End Try

        End If
        If Not myUserList.Contains(givenBusinessData.createUser) Then
            myUserList.Add(givenBusinessData.createUser)
        End If

        If myUserList IsNot Nothing Then
            For Each user As String In myUserList
                If myUser.Load(user) = ePLMS_OK Then
                    myArrayPerson = New ArrayList
                    myUser.getPersonList(myArrayPerson)
                    If myArrayPerson IsNot Nothing Then
                        For Each person As eDataListElement In myArrayPerson
                            If myPerson.Load(person.Name) = ePLMS_OK Then
                                If Not myUserMail.Contains(myPerson.eMail) Then
                                    myUserMail.Add(myPerson.eMail)
                                End If
                            End If
                        Next
                    End If
                End If
            Next
        End If



    End Sub
    Shared Sub traceEvent(sessionDebugFlag As Boolean?, myeConnection As eConnection, givenFunction As String, givenClass As String, givenMessage As String)
        If sessionDebugFlag.HasValue Then
            If sessionDebugFlag = True Then
                Dim myTemplate As String = "Function=({0});Class=({1});Message=({2})"
                Dim myMessage As String = String.Format(myTemplate, givenFunction, givenClass, givenMessage)
                myeConnection.Logger.writeToLog(myMessage, "DebugMode")
            End If
        End If
    End Sub
    Private Shared Function GetSequenceGenerator(givenConnection As eConnection, ByRef returnSequence As eSequenceGenerator, isForMultiCreation As Boolean, isForMultiTranches As Boolean) As Integer
        Dim rc As Integer

        Dim nameSequence As String = "R" & Now.ToString("yy")
        If isForMultiCreation Then
            nameSequence = "RDAMC" & Now.ToString("yy")
        End If
        If isForMultiTranches Then
            '  nameSequence = "RDACODSC" & Now.ToString("yy")
        End If
        returnSequence = New eSequenceGenerator(givenConnection)
        rc = returnSequence.Load(nameSequence)
        If rc <> ePLMS_OK Then
            returnSequence.Name = nameSequence
            returnSequence.Description = "RDA Sequence for Year " & Now.ToString("yyyy")
            returnSequence.generationType = ePLMSGenerationType_INCREASE
            returnSequence.Context = ePLMSContext_PUBLIC
            returnSequence.Status = ePLMSStatus_ENABLED
            returnSequence.Key1 = ""
            returnSequence.Key2 = ""
            returnSequence.Key3 = ""
            returnSequence.generatedValue = nameSequence & "-00001"
            rc = returnSequence.Create()
        End If

        Return rc
    End Function



#Region "Notification"


#End Region

#Region "BAAN"
    Private Shared Function GetSequenceGeneratorBaaN(givenConnection As eConnection, ByRef returnSequence As eSequenceGenerator) As Integer
        Dim rc As Integer

        Dim nameSequence As String = "RdABaaN-DE" & Now.ToString("yyyy")
        returnSequence = New eSequenceGenerator(givenConnection)
        rc = returnSequence.Load(nameSequence)
        If rc <> ePLMS_OK Then
            returnSequence.Name = nameSequence
            returnSequence.Description = "RDA BaaN Data Exchange Sequence for Year " & Now.ToString("yyyy")
            returnSequence.generationType = ePLMSGenerationType_INCREASE
            returnSequence.Context = ePLMSContext_PUBLIC
            returnSequence.Status = ePLMSStatus_ENABLED
            returnSequence.Key1 = ""
            returnSequence.Key2 = ""
            returnSequence.Key3 = ""
            returnSequence.generatedValue = nameSequence & "-0000000001"
            rc = returnSequence.Create()
        End If

        Return rc
    End Function


    '[-]------------------------------------------------------------------------------[-]
    ' | Autore:     Luca Sansone
    ' | Scopo:      generazione automatica text rda per il solo ramo prestazioni
    ' |             
    ' | Requisito:  2.2 -> BID_ICT_eRDA.evolutiva.W1_20170801.docx
    Public Enum ActionType
        create
        modify
    End Enum
    Private Shared Function AutomaticTextRDA(ByRef givenConnection As eConnection, ByRef givenRdA As eBusinessData, actionType As ActionType)

        Dim myPath As String = IO.Path.Combine(givenConnection.ePLMS_HOME, "env\scripts\BaaN\sync\")
        If Not IO.Directory.Exists(myPath) Then
            Exit Function
        End If
        Dim dataExchange As String = ""
        givenRdA.getAttribute("BaanIn_DataExchange", dataExchange)
        If IsNumeric(dataExchange) Then
            Try
                If Integer.Parse(dataExchange) > 0 Then
                    Exit Function
                End If
            Catch ex As Exception

            End Try
        End If

        If givenRdA.levelPosition = 1000 Then
            Exit Function
        End If
        If givenRdA.Level.ToLower.Trim = "closed" Then
            Exit Function
        End If

        Dim myTemplateFiles() As String = IO.Directory.GetFiles(myPath, "textRDA_*.eplms")
        Dim dummyCBS As String = ""
        Dim selectedTemplateFile As String = ""
        Dim rdaw1_spendingcurve As String = ""
        If myTemplateFiles IsNot Nothing AndAlso myTemplateFiles.Length > 0 Then
            givenRdA.getAttribute("CBS", dummyCBS)
            dummyCBS = dummyCBS.Replace("/", "")
            dummyCBS = dummyCBS.Replace("&", "")
            dummyCBS = dummyCBS.Replace(" ", "")
            For Each file As String In myTemplateFiles
                If IO.Path.GetFileNameWithoutExtension(file).ToLower = String.Format("textrda_{0}", dummyCBS.ToLower) Then
                    selectedTemplateFile = file
                    Exit For
                End If
            Next
        End If


        If IO.File.Exists(selectedTemplateFile) Then
            Dim rdaattributelist As New ArrayList
            Dim myFileContent As String = IO.File.ReadAllText(selectedTemplateFile)
            givenRdA.getAttributeList(rdaattributelist)

            If rdaattributelist IsNot Nothing AndAlso rdaattributelist.Count > 0 Then
                Dim rdaw1_feasibilitystudies As String = ""
                Dim ODMNumber As String = ""
                For Each edata As eDataListElement In rdaattributelist
                    Dim replaceFlag = False
                    If myFileContent.ToLower.Contains("{" & edata.Name.ToLower & "}") Then
                        replaceFlag = True
                    End If
                    If edata.Name = "rdaw1_spendingcurve" Then
                        If myFileContent.Contains("{SPENDING_CURVE") Then
                            replaceFlag = True
                        End If

                    End If
                    If replaceFlag Then
                        If edata.Name.ToUpper = "DUVRI" Then
                            If edata.Value = "TRUE" Then
                                edata.Value = "Attività coperta da DUVRI"
                            ElseIf edata.Value = "FALSE" Then
                                edata.Value = "Attività di natura intellettuale, non necessita di DUVRI"
                            End If
                        ElseIf edata.Name.ToUpper = "ODMNumber".ToUpper Then
                            ODMNumber = "ODM:" & edata.Value
                            Continue For
                        ElseIf edata.Name.ToUpper = "rdaw1_spendingcurve".ToUpper Then
                            rdaw1_spendingcurve = edata.Value
                            Continue For
                        ElseIf edata.Name.ToUpper = "rdaw1_feasibilitystudies".ToUpper Then
                            rdaw1_feasibilitystudies = edata.Value
                            Continue For
                        ElseIf edata.Type = "DATE" Then
                            If edata.Value.Contains(" ") Then
                                edata.Value = edata.Value.Split(" ")(0)
                            ElseIf edata.Value.Length > 8 Then
                                givenConnection.Core.stringToDate(edata.Value, edata.Value)
                                edata.Value = edata.Value.Split(" ")(0)
                            End If
                        End If
                        myFileContent = myFileContent.Replace("{" & edata.Name & "}", edata.Value)
                    End If
                Next

                If rdaw1_feasibilitystudies.ToUpper = "TRUE" Then
                    myFileContent = myFileContent.Replace("[{rdaw1_feasibilitystudies}or{ODMNumber}]", "STUDI/FATTIBILITA")
                Else
                    myFileContent = myFileContent.Replace("[{rdaw1_feasibilitystudies}or{ODMNumber}]", ODMNumber)
                End If


                myFileContent = myFileContent.Replace("{eName}", givenRdA.Name)
                myFileContent = myFileContent.Replace("{eDescription}", givenRdA.Description)
                myFileContent = myFileContent.Replace("{eProject}", givenRdA.Project)
                myFileContent = myFileContent.Replace("{eCreateUser}", givenRdA.createUser)
                If myFileContent.Contains("{SPENDING_CURVE") Then
                    Dim mySpending As String = rdaw1_spendingcurve
                    If mySpending <> "" Then
                        '2nd tranche 50.00% of the total amount € 1.250,00 in date 01/01/2018
                        Dim numero As String = mySpending.Split(" ")(0)
                        Dim cifra As String = ""
                        Dim tf As String = mySpending.Replace("in date", "|").Trim.Split("|")(0)
                        Dim dt As String = mySpending.Replace("in date", "|").Trim.Split("|")(1)
                        If numero.Length > 2 Then
                            cifra = numero.Substring(0, numero.Length - 2)
                        End If
                        tf = tf.Replace(numero & " tranche", "tranche " & cifra & " -")
                        tf = tf.Replace("of the total amount", "of total amount")
                        myFileContent = myFileContent.Replace("{SPENDING_CURVE_RATE}", tf)
                        myFileContent = myFileContent.Replace("{SPENDING_CURVE_DATE}", dt.Split("/")(2).Trim)
                    End If
                End If
                'elimino tutto quello che non è definito
                Dim myRDAType As New eBusinessDataType(givenConnection)
                If myRDAType.Load(givenRdA.businessDataType) = ePLMS_OK Then
                    myRDAType.getAttributeList(rdaattributelist)
                    If rdaattributelist IsNot Nothing AndAlso rdaattributelist.Count > 0 Then
                        For Each edata As eDataListElement In rdaattributelist
                            If myFileContent.ToLower.Contains("{" & edata.Name.ToLower & "}") Then
                                myFileContent = myFileContent.Replace("{" & edata.Name & "}", "")
                            End If
                        Next

                    End If
                End If
            End If
            'If myFileContent.Length > 72 Then
            '    myFileContent = myFileContent.Substring(0, 72)
            'End If


            If actionType = ActionType.create Then givenRdA.Reserve()
            givenRdA.changeAttribute("BaanIn_RigaTesto", myFileContent)
            If actionType = ActionType.create Then givenRdA.Unreserve()



        End If
    End Function

    Private Shared Sub AutomaticRDADEFINITION(ByRef givenConnection As eConnection, ByRef givenRdA As eBusinessData, givenBusinessDataUserDefinedAttributeList As ArrayList, actionType As ActionType)
        Dim dummyCBS As String = Nothing
        Dim dummyRequestedFornitore As String = Nothing
        Dim dummySupplier As String = Nothing
        Dim dummyDescription As String = Nothing
        Dim dummyComponentDescription As String = Nothing
        Dim dummyFeasibilityStudies As String = Nothing
        Dim dummyODM As String = Nothing
        Dim dummystartdate As String = Nothing
        Dim dummyenddate As String = Nothing
        Dim dummyworkpackage As String = Nothing
        Dim dataExchange As String = ""

        givenRdA.getAttribute("BaanIn_DataExchange", dataExchange)
        If IsNumeric(dataExchange) Then
            Try
                If Integer.Parse(dataExchange) > 0 Then
                    Exit Sub
                End If
            Catch ex As Exception

            End Try
        End If
        If givenRdA.levelPosition = 1000 Then
            Exit Sub
        End If
        If givenRdA.Level.ToLower.Trim = "closed" Then
            Exit Sub
        End If
        'If givenRdA.levelPosition >= 400 Then
        '    Exit Sub
        'End If

        Dim RDADefinition As String = "{0}_{1}_{2}_{3}{4}"
        Dim dummyDate As DateTime
        Dim cbs As String = ""
        If givenBusinessDataUserDefinedAttributeList IsNot Nothing AndAlso givenBusinessDataUserDefinedAttributeList.Count > 0 Then
            For Each edata As eDataListElement In givenBusinessDataUserDefinedAttributeList
                Select Case edata.Name
                    Case "CBS"
                        dummyCBS = edata.Value
                    Case "RequestedSupplier"
                        dummyRequestedFornitore = edata.Value
                    Case "Supplier"
                        dummySupplier = edata.Value
                    Case "rdaw1_activitydescription"
                        dummyDescription = edata.Value
                    Case "ComponentDescription"
                        dummyComponentDescription = edata.Value
                    Case "rdaw1_feasibilitystudies"
                        dummyFeasibilityStudies = edata.Value
                    Case "ODMNumber"
                        dummyODM = edata.Value
                    Case "ActivityStart"
                        dummystartdate = edata.Value
                    Case "ActivityEnd"
                        dummyenddate = edata.Value
                    Case "rdaw1_workpackage"
                        dummyworkpackage = edata.Value

                End Select
            Next
        End If
        If actionType = ActionType.modify Then
            If dummyCBS Is Nothing Then givenRdA.getAttribute("CBS", dummyCBS)
            If dummyRequestedFornitore Is Nothing Then givenRdA.getAttribute("RequestedSupplier", dummyRequestedFornitore)
            If dummySupplier Is Nothing Then givenRdA.getAttribute("Supplier", dummySupplier)
            If dummyDescription Is Nothing Then givenRdA.getAttribute("rdaw1_activitydescription", dummyDescription)
            If dummyComponentDescription Is Nothing Then givenRdA.getAttribute("ComponentDescription", dummyComponentDescription)
            If dummyFeasibilityStudies Is Nothing Then givenRdA.getAttribute("rdaw1_feasibilitystudies", dummyFeasibilityStudies)
            If dummyODM Is Nothing Then givenRdA.getAttribute("ODMNumber", dummyODM)
            If dummystartdate Is Nothing Then givenRdA.getAttribute("ActivityStart", dummystartdate)
            If dummyenddate Is Nothing Then givenRdA.getAttribute("ActivityEnd", dummyenddate)
            If dummyworkpackage Is Nothing Then givenRdA.getAttribute("rdaw1_workpackage", dummyworkpackage)

        End If

        dummyCBS = "" & dummyCBS
        dummyRequestedFornitore = "" & dummyRequestedFornitore
        dummySupplier = "" & dummySupplier
        dummyDescription = "" & dummyDescription
        dummyComponentDescription = "" & dummyComponentDescription
        dummyFeasibilityStudies = "" & dummyFeasibilityStudies
        dummyODM = "" & dummyODM
        dummystartdate = "" & dummystartdate
        dummyenddate = "" & dummyenddate
        dummyworkpackage = "" & dummyworkpackage
        cbs = dummyCBS

        Select Case dummyCBS.ToLower
            Case "r&d hours"
                dummyCBS = "RDH"
            Case "codesign"
                dummyCBS = "COD"
            Case "test/tool proto/outsource&facilities"
                dummyCBS = "TES"
            Case "work package"
                dummyCBS = "WOR"
            Case Else
                Exit Sub
        End Select

        Select Case dummyRequestedFornitore.ToLower
            Case "to be sourced"
                dummyRequestedFornitore = IIf(dummyworkpackage = "TRUE", "TBW", "TBS")
            Case "suggested / defined"
                dummyRequestedFornitore = IIf(dummyworkpackage = "TRUE", "SUW", "SUG")
            Case "remedy"
                dummyRequestedFornitore = IIf(dummyworkpackage = "TRUE", "REW", "REM")
        End Select

        If dummyDescription.Length > 12 Then
            dummyDescription = dummyDescription.Substring(0, 12)
        End If
        dummyComponentDescription = GetText(dummyComponentDescription, 11)
        dummyODM = GetText(dummyODM, 4)


        If dummyFeasibilityStudies = "TRUE" Then
            dummyODM = "STUD"
        End If
        If cbs <> "Codesign" Then
            If dummystartdate.Contains(" ") Then
                dummystartdate = dummystartdate.Split(" ")(0)
            ElseIf dummystartdate.Length > 10 Then
                givenConnection.Core.stringToDate(dummystartdate, dummystartdate)
                dummystartdate = dummystartdate.Split(" ")(0)
            End If
            If Not String.IsNullOrEmpty(dummystartdate) Then
                dummystartdate = dummystartdate.Replace("/", ".")
                dummyDate = New DateTime(dummystartdate.Split(".")(2), dummystartdate.Split(".")(1), dummystartdate.Split(".")(0))
                dummystartdate = dummyDate.ToString("MMyy")
            End If
            If dummyenddate.Contains(" ") Then
                dummyenddate = dummyenddate.Split(" ")(0)
            ElseIf dummyenddate.Length > 10 Then
                givenConnection.Core.stringToDate(dummyenddate, dummyenddate)
                dummyenddate = dummyenddate.Split(" ")(0)
            End If
            If Not String.IsNullOrEmpty(dummyenddate) Then
                dummyenddate = dummyenddate.Replace("/", ".")
                dummyDate = New DateTime(dummyenddate.Split(".")(2), dummyenddate.Split(".")(1), dummyenddate.Split(".")(0))
                dummyenddate = dummyDate.ToString("MMyy")
                If dummystartdate <> "" Then
                    dummyenddate = "-" & dummyenddate
                End If
            End If
            RDADefinition = String.Format(RDADefinition, dummyCBS, dummyRequestedFornitore, dummyDescription, dummystartdate, dummyenddate)
        ElseIf cbs = "Codesign" Then
            '"{0}_{1}_{2}_{3}{4}"
            '"$CBS”_”$TIPOFORNITORE”_”$COMPONENTDESCRIPTION”_”$ODMNUMBER”_”$SUPPLIER    
            If dummySupplier <> "" Then
                dummySupplier = GetSupplierCode(givenConnection, dummySupplier)
                dummySupplier = dummySupplier.Trim
                dummySupplier = GetText(dummySupplier, 5)
                dummySupplier = "_" & dummySupplier
            Else
                dummySupplier = "_PPPPP"
            End If
            RDADefinition = String.Format(RDADefinition, dummyCBS, dummyRequestedFornitore, dummyComponentDescription, dummyODM, dummySupplier)
        End If



        givenRdA.Description = RDADefinition
    End Sub
    Public Shared Function GetSupplierCode(ByRef givenConnection As eConnection, givenText As String) As String

        Dim myFindQuery As String = "Select Code,Name from FORNITORI where Name='" & givenText & "'"
        'bypass caratteri sporchi
        Dim supplier As String = PrepareSupplierNameForQuery(givenText)
        myFindQuery = "Select Code,Name from FORNITORI where Name like '" & supplier & "'"

        Dim retValue As String = GetText(givenText, 5)

        Dim myTable As DataTable
        Dim myFind As New eFind(givenConnection)

        myFind.queryString = "$(" & myFindQuery & ")"
        myFind.Prepare(Nothing)
        myFind.Execute(myTable)

        If myTable IsNot Nothing OrElse myTable.Rows.Count > 0 Then
            'retValue = myTable.Rows(0)(0)
            For Each row As DataRow In myTable.Rows
                If row("Name").ToString.ToLower = givenText.ToLower Then
                    retValue = row("Code") '.Rows(0)(0)
                    Exit For
                End If
            Next
        End If
        Return retValue
    End Function
    Public Shared Function PrepareSupplierNameForQuery(givenText As String) As String
        Dim supplier As String = givenText
        supplier = supplier.Replace("""", "%")
        supplier = supplier.Replace("'", "%")
        supplier = supplier.Replace("[", "%")
        supplier = supplier.Replace("]", "%")
        supplier = supplier.Replace(" ", "%")
        supplier = supplier.Replace("%%", "%")
        Return supplier
    End Function
    Public Shared Function GetText(givenText As String, truncateIndex As Integer) As String
        givenText = givenText.Replace(vbCrLf, " ")
        If givenText.Length > truncateIndex Then
            givenText = givenText.Substring(0, truncateIndex)
        End If
        Return givenText
    End Function
    Private Shared Function GenerateSpendingCurve(ByRef givenConnection As eConnection, ByRef givenRdA As eBusinessData, givenSpendingCurve As String) As Integer
        Dim givenAmount As String = ""
        Dim givenFormat As String = ""
        Dim rc As Integer = 0
        Dim spendingTranches() As String = givenSpendingCurve.Split(";".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
        givenRdA.getAttribute("Amount", givenAmount)
        givenRdA.getAttribute("AmountFormat", givenFormat)

        Dim myFormatAmount As String = ""
        If IsNumeric(givenAmount) Then
            If givenFormat = "" OrElse givenFormat = "euro" Then
                myFormatAmount = eUtility.GetDoubleValue(givenAmount).ToString("C", CultureInfo.CreateSpecificCulture("it-IT"))
            Else
                myFormatAmount = eUtility.GetDoubleValue(givenAmount).ToString("C", CultureInfo.CreateSpecificCulture("en-US"))
            End If

        End If
        Dim shortDateFormat = eEnvConfigurationController.getEntry(givenConnection, "shortDateFormat")
        If spendingTranches.Length > 0 Then
            ' Dim myeSequence As eSequenceGenerator
            Dim rdaNameProgressNumber As String = givenRdA.Name
            Dim globalSpendigValues As New Text.StringBuilder

            'If GetSequenceGenerator(retrieveDBAConnection.getDbaConnection, myeSequence, False, False) = ePLMS_OK Then
            '    If myeSequence.getNext(rdaNameProgressNumber) = ePLMS_OK Then
            '        rdaNameProgressNumber = rdaNameProgressNumber
            '    End If
            'End If

            For idx As Integer = 0 To spendingTranches.Length - 1
                Dim tranche As Integer = spendingTranches(idx).Split("-")(0)
                Dim rate As Double = eUtility.GetDoubleValue(spendingTranches(idx).Split("-")(1).Trim)
                Dim invoicedate As String = spendingTranches(idx).Split("-")(2).Replace(" ", "")
                Dim spend As String = String.Format("{0} tranche {1}% of the total amount {2} in date {3}",
                                                    AddOrdinal(tranche), rate, myFormatAmount, invoicedate.Replace(" ", ""))

                Dim spendAdmin As String = String.Format("{0}|{1}|{2}", tranche, rate, invoicedate.Replace(" ", ""))
                Dim dateOfInvoice As DateTime = DateTime.ParseExact(invoicedate, "dd/MM/yyyy", New CultureInfo("en-US"))



                Dim myeSaveAsCriteria As New eSaveasCriteria
                myeSaveAsCriteria.businessDataAttributeFlag = True
                myeSaveAsCriteria.businessDataFileFlag = False
                myeSaveAsCriteria.businessDataRelationFlag = True

                Dim myeBusinessDataNew As New eBusinessData(givenConnection)
                myeBusinessDataNew.businessDataType = givenRdA.businessDataType
                myeBusinessDataNew.Name = rdaNameProgressNumber & "_" & tranche 'myeBusinessDataNew.Name & "_" & dateOfInvoice.ToString("yyMM") & "_" & tranche
                myeBusinessDataNew.Revision = givenRdA.Revision
                myeBusinessDataNew.Description = givenRdA.Description
                myeBusinessDataNew.Project = givenRdA.Project
                'myeBusinessDataNew.Workflow = givenRdA.Workflow
                myeBusinessDataNew.Key1 = givenRdA.Id
                rc = givenRdA.Saveas(myeSaveAsCriteria, myeBusinessDataNew)
                If rc <> ePLMS_OK Then
                    Dim message As String = givenConnection.errorMessage
                    message = "Error generating spending curve splitted rda!" & message
                    givenConnection.errorMessage = message
                    GoTo EXITFUNCTION
                Else
                    globalSpendigValues.AppendLine(spend)
                    myeBusinessDataNew.Reserve()
                    myeBusinessDataNew.changeAttribute("RDASCOPE", "SINGLE")
                    myeBusinessDataNew.changeAttribute("rdaw1_spendingcurve_totalAmount", givenAmount)
                    myeBusinessDataNew.changeAttribute("rdaw1_spendingcurve", spend)
                    myeBusinessDataNew.changeAttribute("rdaw1_spendingcurve_admin", spendAdmin)

                    myeBusinessDataNew.changeAttribute("rdaw1_spendingcurve_master", givenRdA.Id)
                    myeBusinessDataNew.changeAttribute("BaanIn_DataErosioneBudget", dateOfInvoice.ToString(shortDateFormat))
                    Dim myNewAmout As Double = eUtility.GetDoubleValue(givenAmount) * rate / 100
                    myNewAmout = Math.Round(myNewAmout, 2)
                    myeBusinessDataNew.changeAttribute("Amount", myNewAmout.ToString().Replace(",", "."))

                    myeBusinessDataNew.Modify(New ArrayList)
                    myeBusinessDataNew.Unreserve()
                End If
                'End If


            Next
            givenRdA.Reserve()
            givenRdA.changeAttribute("rdaw1_spendingcurve", globalSpendigValues.ToString)
            givenRdA.Unreserve()
        End If

EXITFUNCTION:
        Return rc
    End Function


    Private Shared Function UpdateSpendingCurve(ByRef givenConnection As eConnection, ByRef givenRdA As eBusinessData, givenAttributeList As ArrayList) As Integer
        Dim rc As Integer = 0

        Dim newAmount As String = ""
        Dim newAmountFormat As String = ""
        Dim myFormatAmount As String = ""

        Dim ComponentDescription As String = ""
        Dim RequestedSupplier As String = ""
        Dim Supplier As String = ""
        Dim PartNumber As String = ""
        Dim ODMNumber As String = ""
        Dim rdaw1_feasibilitystudies As String = ""
        Dim OfferReference As String = ""
        Dim OfferDate As String = ""


        givenRdA.getAttribute("Amount", newAmount)
        givenRdA.getAttribute("AmountFormat", newAmountFormat)


        If IsNumeric(newAmount) Then
            If newAmountFormat = "" OrElse newAmountFormat = "euro" Then
                myFormatAmount = eUtility.GetDoubleValue(newAmount).ToString("C", CultureInfo.CreateSpecificCulture("it-IT"))
            Else
                myFormatAmount = eUtility.GetDoubleValue(newAmount).ToString("C", CultureInfo.CreateSpecificCulture("en-US"))
            End If

        End If

        Dim mySql As String = "select eplms.eBusinessdata.eId, eplms.eBusinessdata.eLevel, eplms.eBusinessdata.eLevelPosition, eplms.GetAttributeNumericValue('BaanIn_DataExchange', eId) as BaanIn_DataExchange , eplms.GetAttributeValue('rdaw1_spendingcurve', eId) as SpendingCurve,eplms.GetAttributeValue('rdaw1_spendingcurve_admin', eId) as rdaw1_spendingcurve_admin from eplms.eBusinessData where ebusinessDataType='RdA' and eWorkflow='WF_RDA_01' and eKey1='" & givenRdA.Id & "'"
        Dim retTable As DataTable = ExecuteSql(mySql, givenConnection)
        If retTable IsNot Nothing AndAlso retTable.Rows.Count > 0 Then
            Dim selectPath As String = "eLevelPosition>=400 and BaanIn_DataExchange>0"
            Dim selectRow() As DataRow = retTable.Select(selectPath)
            If selectRow.Length > 0 Then
                Exit Function
            End If
            Dim myRda As New eBusinessData(retrieveDBAConnection.getDbaConnection)
            For Each row As DataRow In retTable.Rows
                If myRda.Load(row("eId")) = ePLMS_OK Then
                    If myRda.isReservable = ePLMS_OK Then
                        RestartWF(myRda.Connection, myRda.Id)
                        If myRda.reserveUser <> "" Then
                            myRda.Unreserve()
                        End If
                        myRda.Reserve()
                        Dim myNewAttribute As New ArrayList
                        For Each edata As eDataListElement In givenAttributeList
                            If "ComponentDescription;RequestedSupplier;Supplier;PartNumber;ODMNumber;rdaw1_feasibilitystudies;OfferReference;OfferDate".ToLower.Contains(edata.Name.ToLower) Then
                                Dim myNewEdata As eDataListElement
                                myNewEdata.Type = edata.Type
                                myNewEdata.Name = edata.Name
                                myNewEdata.Value = edata.Value
                                If edata.Type = "DATE" Then
                                    myNewEdata.Value = edata.Value.Split(" ")(0)
                                    If Not myNewEdata.Value.Contains(".") AndAlso myNewEdata.Value.Length = 14 Then
                                        givenConnection.Core.stringToDate(myNewEdata.Value, myNewEdata.Value)
                                        myNewEdata.Value = myNewEdata.Value.Split(" ")(0)
                                    End If
                                End If
                                myNewAttribute.Add(myNewEdata)
                                '
                            End If
                        Next

                        If myRda.Modify(myNewAttribute) = ePLMS_OK Then
                            Dim oldSpendingCurve As String = ""
                            myRda.getAttribute("rdaw1_spendingcurve_admin", oldSpendingCurve)

                            Dim tranche As Integer = oldSpendingCurve.Split("|")(0)
                            Dim rate As Double = eUtility.GetDoubleValue(oldSpendingCurve.Split("|")(1).Trim)
                            Dim invoicedate As String = oldSpendingCurve.Split("|")(2).Replace(" ", "")

                            Dim spend As String = String.Format("{0} tranche {1}% of the total amount {2} in date {3}",
                                                    AddOrdinal(tranche), rate, myFormatAmount, invoicedate.Replace(" ", ""))


                            Dim spendAdmin As String = String.Format("{0}|{1}|{2}", tranche, rate, invoicedate.Replace(" ", ""))
                            Dim rc1 As Integer = 0
                            rc1 = myRda.changeAttribute("rdaw1_spendingcurve_totalAmount", newAmount)
                            rc1 = myRda.changeAttribute("rdaw1_spendingcurve", spend)
                            Dim myNewAmout As Double = eUtility.GetDoubleValue(newAmount) * rate / 100
                            myNewAmout = Math.Round(myNewAmout, 2)
                            rc1 = myRda.changeAttribute("Amount", myNewAmout.ToString().Replace(",", "."))
                        Else

                        End If
                        myRda.Unreserve()







                    Else
                        Exit Function
                    End If
                End If
            Next
        End If



EXITFUNCTION:
        Return rc
    End Function

    Private Shared Sub RestartWF(myeConnection As eConnection, givenRdaId As Integer)
        Dim myWorkflowEngine As New eWorkflowEngine(myeConnection, givenRdaId)
        With myWorkflowEngine
            If Not .isWorkflowEnded Then
                If myWorkflowEngine.reassignLevel("Working", "Restart workflow for tranche modification", True) <> ePLMS_OK Then
                    Throw New Exception(myeConnection.errorMessage)
                End If
            End If
        End With
        myWorkflowEngine = New eWorkflowEngine(myeConnection, givenRdaId)
        With myWorkflowEngine
            Dim myCurrentTask As workflowTask
            If Not .isWorkflowEnded Then
                'Retrieve current task
                myCurrentTask = Nothing
                myCurrentTask = .getCurrentWorkflowTask
                If myCurrentTask IsNot Nothing Then
                    myCurrentTask.performTask()
                End If
            End If
        End With
    End Sub
    Private Shared Function AddOrdinal(ByVal num As Integer) As String
        Select Case (num Mod 100)
            Case 11 To 13
                Return num.ToString() & "th"
        End Select
        Select Case num Mod 10
            Case 1
                Return num.ToString() & "st"
            Case 2
                Return num.ToString() & "nd"
            Case 3
                Return num.ToString() & "rd"
            Case Else
                Return num.ToString() & "th"
        End Select
    End Function
    '[-]------------------------------------------------------------------------------[-]
#End Region


#Region "DB Methods"
    Public Shared Function GetSqlText(givenScriptPath As String, params() As String) As String
        Dim mySqlString As String = ""
        Dim myPath As String = AppDomain.CurrentDomain.BaseDirectory
        myPath = IO.Path.Combine(myPath, givenScriptPath)
        If IO.File.Exists(myPath) Then
            mySqlString = IO.File.ReadAllText(myPath)
            If params IsNot Nothing Then
                For idx As Integer = 0 To params.Length - 1
                    mySqlString = mySqlString.Replace("@Param" & (idx + 1), params(idx))
                Next
            End If
        End If
        Return mySqlString
    End Function
    Public Shared Function ExecuteSql(givenSql As String, getUserConnection As eConnection) As DataTable
        Dim myTable As New DataTable
        Dim myFind As New eFind(getUserConnection)
        myFind.queryString = "$(" + givenSql + ")"
        myFind.Prepare(Nothing)
        myFind.Execute(myTable)
        Return myTable
    End Function
    Public Shared Function ExecuteQuery(givenConnection As eConnection, givenScriptPath As String, givenParams() As String, Optional removeString As String = "") As DataTable
        Dim mySqlText As String = GetSqlText(givenScriptPath, givenParams)

        If removeString <> "" Then
            mySqlText = mySqlText.Replace(removeString, "")

        End If

        If Not String.IsNullOrEmpty(mySqlText) Then
            Return ExecuteSql(mySqlText, givenConnection)
        End If
        Return New DataTable
    End Function

#End Region



End Class
