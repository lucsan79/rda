'
'*------------------------------------------------------------------------------
'*   Copyright (c) 2005 Parallaksis Corporation. All Rights Reserved.
'*
'*   This source code is intended only As a supplement to Collaboration
'*   Desktop Development Tools.  
'*
'*------------------------------------------------------------------------------
' 
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.IO
Imports System.Data
Imports System.Collections
Imports System.Reflection

Public Class eBusinessDataHooks
    Implements IDisposable

#Region "Class Description"
    '[-]--------------------------------------------------------------------[-]
    ' |
    ' | Class            : eBusinessDataHooks
    ' | Created by       : 
    ' | Date             : 
    ' | Description      : 
    ' | Private Methods  :
    ' | Public Methods   :
    ' |     createPrefix()
    ' |     createSuffix()
    ' |     deletePrefix()
    ' |     deleteSuffix()
    ' |     freezePrefix()
    ' |     freezeSuffix()
    ' |     loadPrefixById()
    ' |     loadPrefix()
    ' |     loadSuffix()
    ' |     modifyPrefix()
    ' |     modifySuffix()
    ' |     renamePrefix()
    ' |     renameSuffix()
    ' |     reservePrefix()
    ' |     reserveSuffix()
    ' |     revisePrefix()
    ' |     reviseSuffix()
    ' |     saveasPrefix()
    ' |     saveasSuffix()
    ' |     unfreezePrefix()
    ' |     unfreezeSuffix()
    ' |     unreservePrefix()
    ' |     unreserveSuffix()
    ' |  Attribute
    ' |     changeAttributePrefix()
    ' |     changeAttributeSuffix()
    ' |     getAttributeListPrefix()
    ' |     getAttributeListSuffix()
    ' |     getAttributeListWithCriteriaPrefix()
    ' |     getAttributeListWithCriteriaSuffix()
    ' |     getAttributePrefix()
    ' |     getAttributeSuffix()
    ' |     removeAttributePrefix()
    ' |     removeAttributeSuffix()
    ' |  BusinessDataFile
    ' |     getCandidateVaultSuffix()
    ' |     addBusinessDataFilePrefix()
    ' |     addBusinessDataFileSuffix()
    ' |     getBusinessDataFileListPrefix()
    ' |     getBusinessDataFileListSuffix()
    ' |     getBusinessDataFilePrefix()
    ' |     getBusinessDataFilePrefix()
    ' |     getBusinessDataFileSuffix()
    ' |     removeBusinessDataFilePrefix()
    ' |     removeBusinessDataFileSuffix()
    ' |  BusinessDataURL
    ' |     addBusinessDataURLPrefix()
    ' |     addBusinessDataURLSuffix()
    ' |     getBusinessDataURLListPrefix()
    ' |     getBusinessDataURLListSuffix()
    ' |     getBusinessDataURLPrefix()
    ' |     getBusinessDataURLPrefix()
    ' |     getBusinessDataURLSuffix()
    ' |     removeBusinessDataURLPrefix()
    ' |     removeBusinessDataURLSuffix()
    ' |  BusinessDataLine
    ' |     addBusinessDataLinePrefix()
    ' |     addBusinessDataLineSuffix()
    ' |     getBusinessDataLineListPrefix()
    ' |     getBusinessDataLineListSuffix()
    ' |     getBusinessDataLinePrefix()
    ' |     getBusinessDataLinePrefix()
    ' |     getBusinessDataLineSuffix()
    ' |     modifyBusinessDataLinePrefix()
    ' |     modifyBusinessDataLineSuffix()
    ' |     removeBusinessDataLinePrefix()
    ' |     removeBusinessDataLineSuffix()
    ' |     swapBusinessDataLinePrefix()
    ' |     swapBusinessDataLineSuffix()
    ' |  BusinessDataRelation
    ' |     addBusinessDataRelationPrefix()
    ' |     addBusinessDataRelationSuffix()
    ' |     getBusinessDataRelationListPrefix()
    ' |     getBusinessDataRelationListSuffix()
    ' |     getBusinessDataRelationPrefix()
    ' |     getBusinessDataRelationSuffix()
    ' |     getParentBusinessDataRelationListPrefix()
    ' |     getParentBusinessDataRelationListSuffix()
    ' |     modifyBusinessDataRelationPrefix()
    ' |     modifyBusinessDataRelationSuffix()
    ' |     removeBusinessDataRelationPrefix()
    ' |     removeBusinessDataRelationSuffix()
    '[-]--------------------------------------------------------------------[-]
#End Region

#Region "Global variables"
    '**************************************************************
    '*                                                            *
    '*                G l o b a l  V a r i a b l e s              *
    '*                                                            *
    '**************************************************************
    Private className As String
#End Region

#Region "Properties"
    '**************************************************************
    '*                                                            *
    '*                P r o p e r t i e s                         *
    '*                                                            *
    '**************************************************************
#End Region

#Region "Constructor"
    '**************************************************************
    '*                                                            *
    '*                C o n s t r u c t o r                       *
    '*                                                            *
    '**************************************************************
    Public Sub New()
        className = "eBusinessDataHooks"
    End Sub
#End Region

#Region "Dispose Vars & Methods"
    '**************************************************************
    '*                                                            *
    '*    D i s p o s e   V a r s   &   M e t h o d s             *
    '*                                                            *
    '**************************************************************
    Public Overloads Sub Dispose() Implements System.IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            ' Disposal was triggered manually by the client.
            ' Call Dispose() on any contained classes.
        End If

        ' Release unmanaged resources.
        ' Set large member variables to Nothing (null).
    End Sub
#End Region

#Region "Private Methods"
    '**************************************************************
    '*                                                            *
    '*                P r i v a t e   M e t h o d s               *
    '*                                                            *
    '**************************************************************
#End Region

#Region "Public Methods"
    '**************************************************************
    '*                                                            *
    '*                P u b l i c  M e t h o d s                  *
    '*                                                            *
    '**************************************************************
#Region "createPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : createPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function createPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "createPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")
        returnCode = ePLMS_CONTINUE
        returnCode = RDA.RDACreatePrefix(givenConnection, givenBusinessData, givenBusinessDataUserDefinedAttributeList)

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "createSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : createSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function createSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "createSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE
        returnCode = RDA.RDACreateSuffix(givenConnection, givenBusinessData, givenBusinessDataUserDefinedAttributeList)
        If givenBusinessData.businessDataType = "RDA" Then
            OLTPHelper.Insert(givenBusinessData)
        End If
ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "deletePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : deletePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function deletePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef forceIfReferencedByWorkbenchFolder As Boolean, ByRef forcforceIfReferencedByWorkbenchFoldereIfReferencedByWorkSpace As Boolean, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "deletePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "deleteSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : deleteSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function deleteSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef forceIfReferencedByWorkbenchFolder As Boolean, ByRef forceIfReferencedByWorkSpace As Boolean, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "deleteSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "freezePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : freezePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function freezePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "freezePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "freezeSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : freezeSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function freezeSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "freezeSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "loadByIdPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : loadByIdPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function loadByIdPrefix(ByRef givenConnection As eConnection, ByRef givenId As Integer, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "loadByIdPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "loadByIdSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : loadByIdSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function loadByIdSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "loadByIdSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")



        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "loadByIdWithReturnedAttributesValueListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : loadByIdWithReturnedAttributesValueListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function loadByIdWithReturnedAttributesValueListPrefix(ByRef givenConnection As eConnection, ByRef givenId As Integer, ByRef returnAttributeValueListDataTable As System.Data.DataTable, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "loadByIdWithReturnedAttributesValueListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "loadByIdWithReturnedAttributesValueListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : loadByIdWithReturnedAttributesValueListSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function loadByIdWithReturnedAttributesValueListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnAttributeValueListDataTable As System.Data.DataTable, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "loadByIdWithReturnedAttributesValueListSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "loadByBusinessDataTypeNameRevisionKeysPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : loadByBusinessDataTypeNameRevisionKeysPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function loadByBusinessDataTypeNameRevisionKeysPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessDataType As String, ByRef givenName As String, ByRef givenRevision As String, ByRef givenKey1 As String, ByRef givenKey2 As String, ByRef givenKey3 As String, ByRef givenBaselineId As Integer, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "loadByBusinessDataTypeNameRevisionKeysPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "loadByBusinessDataTypeNameRevisionKeysSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : loadByBusinessDataTypeNameRevisionKeysSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function loadByBusinessDataTypeNameRevisionKeysSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "loadByBusinessDataTypeNameRevisionKeysSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "loadByBusinessDataTypeNameRevisionKeysWithReturnedAttributesValueListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : loadByBusinessDataTypeNameRevisionKeysWithReturnedAttributesValueListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function loadByBusinessDataTypeNameRevisionKeysWithReturnedAttributesValueListPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessDataType As String, ByRef givenName As String, ByRef givenRevision As String, ByRef givenKey1 As String, ByRef givenKey2 As String, ByRef givenKey3 As String, ByRef givenBaselineId As Integer, ByRef returnAttributeValueListDataTable As System.Data.DataTable, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "loadByBusinessDataTypeNameRevisionKeysWithReturnedAttributesValueListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "loadByBusinessDataTypeNameRevisionKeysWithReturnedAttributesValueListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : loadByBusinessDataTypeNameRevisionKeysWithReturnedAttributesValueListSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function loadByBusinessDataTypeNameRevisionKeysWithReturnedAttributesValueListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnAttributeValueListDataTable As System.Data.DataTable, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "loadByBusinessDataTypeNameRevisionKeysWithReturnedAttributesValueListSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "modifyPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : modifyPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function modifyPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "modifyPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE
        returnCode = RDA.RDAModifyPrefix(givenConnection, givenBusinessData, givenBusinessDataUserDefinedAttributeList)

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "modifySuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : modifySuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function modifySuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "modifySuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE
        returnCode = RDA.RDAModifySuffix(givenConnection, givenBusinessData, givenBusinessDataUserDefinedAttributeList)
        If givenBusinessData.businessDataType = "RDA" Then
            OLTPHelper.Update(givenBusinessData)
        End If


ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "renamePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : renamePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function renamePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenNewName As String, ByRef givenNewRevision As String, ByRef givenNewKey1 As String, ByRef givenNewKey2 As String, ByRef givenNewKey3 As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "renamePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "renameSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : renameSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function renameSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "renameSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "reservePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : reservePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function reservePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "reservePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "reserveSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : reserveSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function reserveSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "reserveSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "revisePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : revisePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function revisePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenReviseCriteria As eSaveasCriteria, ByRef returnBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "revisePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "reviseSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : reviseSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function reviseSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenReviseCriteria As eSaveasCriteria, ByRef returnBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "reviseSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "saveasPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : saveasPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function saveasPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenSaveasCriteria As eSaveasCriteria, ByRef returnBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "saveasPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "saveasSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : saveasSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function saveasSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenSaveasCriteria As eSaveasCriteria, ByRef returnBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "saveasSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE
        If givenBusinessData.businessDataType = "RDA" Then
            OLTPHelper.Insert(returnBusinessData)
        End If
ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "unfreezePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : unfreezePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function unfreezePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "unfreezePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "unfreezeSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : unfreezeSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function unfreezeSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "unfreezeSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "unreservePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : unreservePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function unreservePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "unreservePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "unreserveSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : unreserveSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function unreserveSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "unreserveSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "Attribute"
#Region "changeAttributePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : changeAttributePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function changeAttributePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenAttributeName As String, ByRef givenValue As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "changeAttributePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "changeAttributeSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : changeAttributeSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function changeAttributeSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenAttributeName As String, ByRef givenValue As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "changeAttributeSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE
        If givenBusinessData.businessDataType = "RDA" Then
            If givenAttributeName.ToLower() = "PiecePrice".ToLower() Or givenAttributeName.ToLower() = "TotalQuantity".ToLower() Then
                Dim myCBS As String = ""
                givenBusinessData.getAttribute("CBS", myCBS)
                If myCBS.Trim.ToLower = "Materials".ToLower Then
                    Dim qnt As String = ""
                    Dim pieceprice As String = ""
                    Dim PiecePriceFormat As String = ""

                    givenBusinessData.getAttribute("PiecePrice", pieceprice)
                    givenBusinessData.getAttribute("PiecePriceFormat", PiecePriceFormat)
                    givenBusinessData.getAttribute("TotalQuantity", qnt)

                    Dim val1 As Double = eUtility.GetDoubleValue(pieceprice)
                    Dim val2 As Double = eUtility.GetDoubleValue(qnt)

                    Dim myDB As New eBusinessData(retrieveDBAConnection.getDbaConnection)
                    myDB.Load(givenBusinessData.Id)
                    myDB.changeAttribute("Amount", Math.Round(val1 * val2, 3).ToString.Replace(",", "."))
                    myDB.changeAttribute("AmountFormat", PiecePriceFormat)
                End If
            End If

            OLTPHelper.Update(givenBusinessData)
        End If

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getAttributeListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getAttributeListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getAttributeListPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getAttributeListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getAttributeListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getAttributeListSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getAttributeListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getAttributeListSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getAttributeListWithCriteriaPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getAttributeListWithCriteriaPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getAttributeListWithCriteriaPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenCriteriaAttributeName As String, ByRef givenCriteriaAttributeValue As String, ByVal givenCriteriaProject As String, ByVal givenCriteriaWorkflow As String, ByVal givenCriteriaLevel As String, ByVal givenCriteriaRole As String, ByRef givenAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getAttributeListWithCriteriaPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getAttributeListWithCriteriaSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getAttributeListWithCriteriaSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getAttributeListWithCriteriaSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenCriteriaAttributeName As String, ByRef givenCriteriaAttributeValue As String, ByVal givenCriteriaProject As String, ByVal givenCriteriaWorkflow As String, ByVal givenCriteriaLevel As String, ByVal givenCriteriaRole As String, ByRef givenAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getAttributeListWithCriteriaSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getAttributePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getAttributePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getAttributePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenAttributeName As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getAttributePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getAttributeSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getAttributeSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getAttributeSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenAttributeName As String, ByRef givenValue As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getAttributeSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        If givenBusinessData.businessDataType = "RDA" Then
            If givenAttributeName = "Amount" Then
                Dim myCBS As String = ""
                givenBusinessData.getAttribute("CBS", myCBS)
                If myCBS.Trim.ToLower = "Materials".ToLower Then
                    Dim qnt As String = ""
                    Dim pieceprice As String = ""
                    Dim PiecePriceFormat As String = ""

                    givenBusinessData.getAttribute("PiecePrice", pieceprice)
                    givenBusinessData.getAttribute("PiecePriceFormat", PiecePriceFormat)
                    givenBusinessData.getAttribute("TotalQuantity", qnt)

                    Dim val1 As Double = eUtility.GetDoubleValue(pieceprice)
                    Dim val2 As Double = eUtility.GetDoubleValue(qnt)

                    Dim myDB As New eBusinessData(retrieveDBAConnection.getDbaConnection)
                    myDB.Load(givenBusinessData.Id)
                    myDB.changeAttribute("Amount", Math.Round(val1 * val2, 3).ToString.Replace(",", "."))
                    myDB.changeAttribute("AmountFormat", PiecePriceFormat)

                    givenValue = Math.Round(val1 * val2, 3).ToString.Replace(",", ".")

                End If

            End If
        End If
        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeAttributePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeAttributePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeAttributePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenAttributeName As String, ByRef givenValue As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeAttributePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeAttributeSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeAttributeSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeAttributeSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenAttributeName As String, ByRef givenValue As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeAttributeSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region
#End Region

#Region "BusinessDataFile"
#Region "getCandidateVaultSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getCandidateVaultSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getCandidateVaultSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnVaultName As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getCandidateVaultSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "addBusinessDataFilePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : addBusinessDataFilePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function addBusinessDataFilePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef givenUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "addBusinessDataFilePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "addBusinessDataFileSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : addBusinessDataFileSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function addBusinessDataFileSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef givenUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "addBusinessDataFileSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataFileListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataFileListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataFileListPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataFileListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataFileListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataFileListSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataFileListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFileList() As eBusinessDataFile, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataFileListSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE
        If Not givenConnection.isCurrentUserDba Then
            Dim myOut As New List(Of eBusinessDataFile)
            If givenBusinessDataFileList IsNot Nothing AndAlso givenBusinessDataFileList.Length > 0 Then
                Dim path As String = givenConnection.ePLMS_HOME
                Dim XmlFilename As String = System.IO.Path.Combine(path, "ENV\ePLMSBusinessDataFileVisibility.xml")
                Dim query1 As String = "//ebusinessdatafiletypes/file[@businessdatatype='{0}']"
                If System.IO.File.Exists(XmlFilename) Then

                    Dim xmlDoc As System.Xml.XmlDocument = New System.Xml.XmlDocument
                    xmlDoc.LoadXml(System.IO.File.ReadAllText(XmlFilename).ToLower)
                    Dim myNodeList As System.Xml.XmlNodeList = xmlDoc.SelectNodes(String.Format(query1, givenBusinessData.businessDataType.ToLower))
                    If myNodeList IsNot Nothing AndAlso myNodeList.Count > 0 Then
                        For Each file As eBusinessDataFile In givenBusinessDataFileList
                            Dim exclude As Boolean = False
                            For Each node As Xml.XmlNode In myNodeList
                                If node.Attributes("extensions") IsNot Nothing Then
                                    If node.Attributes("extensions").Value.ToString.Contains(IO.Path.GetExtension(file.Name.ToLower)) Then
                                        exclude = True
                                        Exit For
                                    End If
                                End If
                            Next
                            If Not exclude Then
                                myOut.Add(file)
                            End If
                        Next
                        givenBusinessDataFileList = myOut.ToArray
                    End If
                End If

            End If
        End If

        If givenBusinessData.businessDataType.ToUpper = "RDA" Then
            If Not String.IsNullOrEmpty(givenBusinessData.Key1) Then
                Dim myListOfFiles As New List(Of eBusinessDataFile)
                If givenBusinessDataFileList IsNot Nothing AndAlso givenBusinessDataFileList.Length > 0 Then
                    For Each file As eBusinessDataFile In givenBusinessDataFileList
                        myListOfFiles.Add(file)
                    Next
                End If
                Dim myMaster As New eBusinessData(givenBusinessData.Connection)
                If myMaster.Load(givenBusinessData.Key1) = ePLMS_OK Then
                    Dim masterFiles() As eBusinessDataFile
                    myMaster.getBusinessDataFileList(True, masterFiles)
                    If masterFiles IsNot Nothing AndAlso masterFiles.Length > 0 Then
                        For Each file As eBusinessDataFile In masterFiles
                            myListOfFiles.Add(file)
                        Next
                    End If
                End If
                givenBusinessDataFileList = myListOfFiles.ToArray()
            End If
        End If


ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataFilePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataFilePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataFilePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFilePtr As Object, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataFilePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")
        If Not givenBusinessDataFilePtr Is Nothing Then
            If TypeOf givenBusinessDataFilePtr Is Integer Then
                'businessDataFile Id was given...
            ElseIf TypeOf givenBusinessDataFilePtr Is String Then
                'businessDataFile Name was given...
            End If
        End If

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataFileSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataFileSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataFileSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataFileSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeBusinessDataFilePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeBusinessDataFilePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeBusinessDataFilePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeBusinessDataFilePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeBusinessDataFileSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeBusinessDataFileSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeBusinessDataFileSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeBusinessDataFileSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region
#End Region

#Region "BusinessDataURL"
#Region "addBusinessDataURLPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : addBusinessDataURLPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function addBusinessDataURLPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataURL As eBusinessDataURL, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "addBusinessDataURLPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "addBusinessDataURLSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : addBusinessDataURLSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function addBusinessDataURLSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataURL As eBusinessDataURL, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "addBusinessDataURLSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataURLListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataURLListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataURLListPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataURLListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataURLListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataURLListSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataURLListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataURLList() As eBusinessDataURL, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataURLListSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataURLPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataURLPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataURLPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataURLId As Integer, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataURLPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataURLPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataURLPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataURLPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataURLName As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataURLPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataURLSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataURLSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataURLSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataURL As eBusinessDataURL, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataURLSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeBusinessDataURLPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeBusinessDataURLPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeBusinessDataURLPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataURL As eBusinessDataURL, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeBusinessDataURLPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeBusinessDataURLSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeBusinessDataURLSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeBusinessDataURLSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataURL As eBusinessDataURL, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeBusinessDataURLSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region
#End Region

#Region "BusinessDataLine"
#Region "addBusinessDataLinePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : addBusinessDataLinePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function addBusinessDataLinePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "addBusinessDataLinePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "addBusinessDataLineSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : addBusinessDataLineSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function addBusinessDataLineSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "addBusinessDataLineSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataLineListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataLineListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataLineListPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataLineListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataLineListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataLineListSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataLineListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLineList() As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataLineListSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataLinePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataLinePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataLinePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLineId As Integer, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataLinePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataLineSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataLineSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataLineSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataLineSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "modifyBusinessDataLinePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : modifyBusinessDataLinePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function modifyBusinessDataLinePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "modifyBusinessDataLinePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "modifyBusinessDataLineSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : modifyBusinessDataLineSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function modifyBusinessDataLineSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "modifyBusinessDataLineSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeBusinessDataLinePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeBusinessDataLinePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeBusinessDataLinePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeBusinessDataLinePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeBusinessDataLineSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeBusinessDataLineSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeBusinessDataLineSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeBusinessDataLineSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "swapBusinessDataLinePrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : swapBusinessDataLinePrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function swapBusinessDataLinePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine1 As eBusinessDataLine, ByRef givenBusinessDataLine2 As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "swapBusinessDataLinePrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "swapBusinessDataLineSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : swapBusinessDataLineSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function swapBusinessDataLineSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataLine1 As eBusinessDataLine, ByRef givenBusinessDataLine2 As eBusinessDataLine, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "swapBusinessDataLineSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region
#End Region

#Region "BusinessDataRelation"
#Region "addBusinessDataRelationPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : addBusinessDataRelationPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function addBusinessDataRelationPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelation As eBusinessDataRelation, ByRef givenUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "addBusinessDataRelationPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "addBusinessDataRelationSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : addBusinessDataRelationSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function addBusinessDataRelationSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelation As eBusinessDataRelation, ByRef givenUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "addBusinessDataRelationSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataRelationListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataRelationListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataRelationListPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataRelationListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataRelationListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataRelationListSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataRelationListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelationList() As eBusinessDataRelation, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataRelationListSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataRelationPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataRelationPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataRelationPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelationId As Integer, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataRelationPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getBusinessDataRelationSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getBusinessDataRelationSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getBusinessDataRelationSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelation As eBusinessDataRelation, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getBusinessDataRelationSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getParentBusinessDataRelationListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getParentBusinessDataRelationListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getParentBusinessDataRelationListPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getParentBusinessDataRelationListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "getParentBusinessDataRelationListSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : getParentBusinessDataRelationListSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function getParentBusinessDataRelationListSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenParentBusinessDataRelationList() As eBusinessDataRelation, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "getParentBusinessDataRelationListSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "modifyBusinessDataRelationPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : modifyBusinessDataRelationPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function modifyBusinessDataRelationPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelation As eBusinessDataRelation, ByRef givenUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "modifyBusinessDataRelationPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "modifyBusinessDataRelationSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : modifyBusinessDataRelationSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function modifyBusinessDataRelationSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelation As eBusinessDataRelation, ByRef givenUserDefinedAttributeList As ArrayList, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "modifyBusinessDataRelationSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeBusinessDataRelationPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeBusinessDataRelationPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeBusinessDataRelationPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelation As eBusinessDataRelation, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeBusinessDataRelationPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "removeBusinessDataRelationSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : removeBusinessDataRelationSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function removeBusinessDataRelationSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataRelation As eBusinessDataRelation, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "removeBusinessDataRelationSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region
#End Region
#End Region
End Class
