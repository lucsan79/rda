﻿Imports Parallaksis.ePLMSM40

Public Class OLTPHelper
    Public Shared Function Update(givenBusinessData As eBusinessData)


        Dim updateRDA As String = "EXEC [eplms].[SyncData] @BusinessDataId = " & givenBusinessData.Id
        RDA.ExecuteSql(updateRDA, givenBusinessData.Connection)

    End Function
    Public Shared Function Insert(givenBusinessData As eBusinessData)

        Dim insertRDA As String = "EXEC [eplms].[CreateData] @BusinessDataId = " & givenBusinessData.Id
        RDA.ExecuteSql(insertRDA, givenBusinessData.Connection)

    End Function

End Class
