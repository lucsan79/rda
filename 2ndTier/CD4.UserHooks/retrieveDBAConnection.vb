Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports Microsoft.VisualBasic





Public Class retrieveDBAConnection

    Private Shared myDbaConnection As eConnection

    Public Shared Function getDbaConnection() As eConnection
        Dim myEplmsHome As String = Configuration.ConfigurationSettings.AppSettings("ePLMSConfigPath")

        Dim rc As Integer
        If myDbaConnection Is Nothing Then
            myDbaConnection = New eConnection(myEplmsHome)
            rc = myDbaConnection.internalDbLogIn("eplms")
            If rc <> ePLMS_OK Then
                myDbaConnection = Nothing
            End If
        ElseIf myDbaConnection.isLoggedIn = False Then
            myDbaConnection = New eConnection(myEplmsHome)
            rc = myDbaConnection.internalDbLogIn("eplms")
            If rc <> ePLMS_OK Then
                myDbaConnection = Nothing
            End If
        End If


        Return myDbaConnection


    End Function
End Class
