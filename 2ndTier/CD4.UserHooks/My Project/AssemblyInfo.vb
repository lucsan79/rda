﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("userHooks")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Parallaksis Corp.")> 
<Assembly: AssemblyProduct("userHooks")> 
<Assembly: AssemblyCopyright("Copyright © Parallaksis Corp.")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("fd98032e-dec8-42d5-a99f-60bd6f3459d8")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.3.0.0")> 
