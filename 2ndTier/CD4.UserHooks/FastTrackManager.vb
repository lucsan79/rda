﻿Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs

Public Class FastTrackManager

    Public Shared Function ManageFastTrack(ByRef givenBusinessData As eBusinessData)
        Dim FASTTRACK As String = ""
        Dim area As String = ""
        Dim CBS As String = ""
        Dim matDefinition As String = ""
        If givenBusinessData.Workflow = "WF_RDA_01" Then
            givenBusinessData.getAttribute("FASTTRACK", FASTTRACK)
            If FASTTRACK = "TRUE" And givenBusinessData.Key1 = "" Then
                givenBusinessData.getAttribute("Area", area)
                If area = "VI&V" Then
                    givenBusinessData.getAttribute("CBS", CBS)
                    If CBS = "Materials" Then
                        givenBusinessData.getAttribute("CO_SP", matDefinition)
                        If matDefinition = "CO" Then
                            Dim message As String = ""
                            Dim isFASTTRACK As String = IsRdAForFastTrack(givenBusinessData, message)
                            If isFASTTRACK = "TRUE" Then
                                SetRDAUnderControl(givenBusinessData)
                            Else
                                If message <> "" Then
                                    givenBusinessData.Reserve()
                                    givenBusinessData.changeAttribute("FASTTRACK_KOREASON", message)
                                    givenBusinessData.Unreserve()
                                    'SetRDAWorking(givenBusinessData, message)
                                End If

                            End If

                        End If
                    End If
                End If
            ElseIf FASTTRACK = "TRUE" And givenBusinessData.Key1 <> "" Then
                SetRDAUnderControl(givenBusinessData)
            End If
        End If
    End Function


#Region "PRIVATE"
    Private Shared Function IsRdAForFastTrack(ByRef givenRdA As eBusinessData, ByRef givenMessage As String) As String
        'recupero limiti
        Dim limiteMensile As Double = 0
        Dim limiteRDA As Double = 0
        Dim dummyValue As String = ""
        Dim retValue As String = "FALSE"
        Dim myList As New eList(retrieveDBAConnection.getDbaConnection)
        If myList.Load("Program Spending Limits") = ePLMS_OK Then
            Dim mysubList As List(Of listItemElement)
            myList.getItemList(mysubList)
            If mysubList IsNot Nothing Then
                'recupero valori default
                For Each item As listItemElement In mysubList
                    If item.Type = ePLMSListItemTypeValue Then
                        If item.Value = "monthly spending limit" Then
                            dummyValue = item.Description
                            dummyValue = dummyValue.Split(" ")(0)
                            dummyValue = dummyValue.ToLower.Replace("k€", "")
                            If IsNumeric(dummyValue) Then
                                limiteMensile = eUtility.GetDoubleValue(dummyValue)
                            End If
                        ElseIf item.Value = "rda spending limit" Then
                            dummyValue = item.Description
                            dummyValue = dummyValue.Split(" ")(0)
                            dummyValue = dummyValue.ToLower.Replace("k€", "")
                            If IsNumeric(dummyValue) Then
                                limiteRDA = eUtility.GetDoubleValue(dummyValue)
                            End If
                        End If
                    End If
                Next

                For Each item As listItemElement In mysubList
                    If item.Type = ePLMSListItemTypeList Then
                        If item.Value.Name.ToString.ToLower = givenRdA.Project.ToLower Then
                            Dim mysubSubList As List(Of listItemElement)
                            item.Value.getItemList(mysubSubList)
                            If mysubSubList IsNot Nothing Then
                                For Each item2 As listItemElement In mysubSubList
                                    If item2.Type = ePLMSListItemTypeValue Then
                                        If item2.Value = "monthly spending limit" Then
                                            dummyValue = item2.Description
                                            dummyValue = dummyValue.Split(" ")(0)
                                            dummyValue = dummyValue.ToLower.Replace("k€", "")
                                            If IsNumeric(dummyValue) Then
                                                limiteMensile = eUtility.GetDoubleValue(dummyValue)
                                            End If
                                        ElseIf item2.Value = "rda spending limit" Then
                                            dummyValue = item2.Description
                                            dummyValue = dummyValue.Split(" ")(0)
                                            dummyValue = dummyValue.ToLower.Replace("k€", "")
                                            If IsNumeric(dummyValue) Then
                                                limiteRDA = eUtility.GetDoubleValue(dummyValue)
                                            End If
                                        End If
                                    End If
                                Next
                            End If

                        End If
                    End If
                Next
            End If
        End If

        limiteMensile = limiteMensile * 1000
        limiteRDA = limiteRDA * 1000

        Dim myCost As Double = GetMaterialCost(givenRdA.Id)
        If myCost <= limiteRDA Then
            Dim myCostoTotale As Double = GetMaterialCostOnMonth(givenRdA.Project)
            If myCostoTotale <= limiteMensile Then
                retValue = "TRUE"
            Else
                Dim residuo As Double = limiteMensile - (myCostoTotale - myCost)

                givenMessage = "FAST TRACK REJECTED: Request exceeds Monthly Spending limit (Month Residue." & String.Format("{0:N2} €", residuo) & ")"
                'givenMessage = "FAST TRACK REJECTED: MONTHLY SPENDING LIMIT KO"
            End If
        Else
            Dim residuo As Double = limiteRDA - myCost
            givenMessage = "FAST TRACK REJECTED: Cost exceeds the limit for each Carry Over RdA (Limit." & String.Format("{0:N2} €", limiteRDA) & ")"
            'givenMessage = "FAST TRACK REJECTED: Cost of RdA is greater of the cost of each CO RdA " & String.Format("{0:N2}", residuo)
        End If


        Return retValue
    End Function


    Private Shared Function GetMaterialCost(givenRdAId As Integer) As Double
        Dim myeFind As New eFind(retrieveDBAConnection.getDbaConnection)
        Dim myDataTable As New DataTable
        Dim retValue As Double = 0

        myeFind.queryString = "select eplms.GetAttributeCurrencyValueOnTotal('PiecePrice', 'TotalQuantity', eplms.ebusinessdata.eid) as RDACOST "
        myeFind.queryString = myeFind.queryString & "from eplms.eBusinessData where eplms.eBusinessData.eId={0}"

        myeFind.queryString = String.Format("$(" & myeFind.queryString & ")", givenRdAId)
        myeFind.selectAttributeListString = "id"
        myeFind.Prepare(Nothing)
        myeFind.Execute(myDataTable)

        If myDataTable IsNot Nothing AndAlso myDataTable.Rows.Count = 1 Then
            If myDataTable.Rows(0)(0) IsNot DBNull.Value Then
                retValue = myDataTable.Rows(0)(0)
            End If

        End If


        Return retValue

    End Function

    Private Shared Function GetMaterialCostOnMonth(givenProject As String) As Double
        Dim myeFind As New eFind(retrieveDBAConnection.getDbaConnection)
        Dim myDataTable As New DataTable
        Dim retValue As Double = 0

        myeFind.queryString = ""
        myeFind.queryString = myeFind.queryString & "select sum(eplms.GetAttributeCurrencyValueOnTotal('PiecePrice', 'TotalQuantity', eplms.ebusinessdata.eid)) as RDACOST  "
        myeFind.queryString = myeFind.queryString & "from eplms.eBusinessData where  "
        myeFind.queryString = myeFind.queryString & "eCreateDate like '{0}%' and  "
        myeFind.queryString = myeFind.queryString & "eplms.GetAttributeValue('Area',eId)='VI&V' and "
        myeFind.queryString = myeFind.queryString & "eplms.GetAttributeValue('CBS',eId)='Materials' and "
        myeFind.queryString = myeFind.queryString & "eplms.GetAttributeValue('CO_SP',eId)='CO' and "
        myeFind.queryString = myeFind.queryString & "eplms.GetAttributeValue('FASTTRACK_KOREASON',eId)='' "
        myeFind.queryString = myeFind.queryString & "and eProject='{1}' and eWorkflow='WF_RDA_01'"

        myeFind.queryString = String.Format("$(" & myeFind.queryString & ")", Now.ToString("yyyyMM"), givenProject)
        myeFind.selectAttributeListString = "id"
        myeFind.Prepare(Nothing)
        myeFind.Execute(myDataTable)

        If myDataTable IsNot Nothing AndAlso myDataTable.Rows.Count = 1 Then
            If myDataTable.Rows(0)(0) IsNot DBNull.Value Then
                retValue = myDataTable.Rows(0)(0)
            End If
        End If


        Return retValue

    End Function


    Private Shared Sub SetRDAUnderControl(ByRef givenRDA As eBusinessData)
        Dim newLevelName As String = Nothing
        Dim newReason As String = "FAST TRACK VIV C/O Materials"
        Dim setTaskAction As Boolean = True
        newLevelName = "Under Control"
        setTaskAction = False

        Dim myWorkflowEngine As New eWorkflowEngine(retrieveDBAConnection.getDbaConnection, givenRDA.Id)
        Dim Rc As Integer = myWorkflowEngine.reassignLevel(newLevelName, newReason, setTaskAction)

        Dim myScript As String = String.Format("Delete FROM eplms.eBusinessDataTaskEntry WHERE (eBusinessDataId = {0})", givenRDA.Id)
        ExecuteCommand(retrieveDBAConnection.getDbaConnection, myScript)

        myWorkflowEngine = New eWorkflowEngine(retrieveDBAConnection.getDbaConnection, givenRDA.Id)
        Dim myCurrentTask As workflowTask = myWorkflowEngine.getCurrentWorkflowTask
        myCurrentTask.performTask()
        myWorkflowEngine = New eWorkflowEngine(retrieveDBAConnection.getDbaConnection, givenRDA.Id)
        myCurrentTask = myWorkflowEngine.getCurrentWorkflowTask
        myCurrentTask.performTask()



        'myWorkflowEngine = New eWorkflowEngine(retrieveDBAConnection.getDbaConnection, givenRDA.Id)
        'myCurrentTask = myWorkflowEngine.getCurrentWorkflowTask
        'Dim myReloadRDA As New eBusinessData(retrieveDBAConnection.getDbaConnection)
        'If myReloadRDA.Load(givenRDA.Id) = ePLMS_OK Then
        '    If myReloadRDA.Level.ToUpper = "UNDER CONTROL" Then

        '        retrieveDBAConnection.getDbaConnection.SQLAdapter.openPersistentConnection()
        '    End If
        'End If

    End Sub

    Public Shared Function ExecuteCommand(ByRef givenConnection As eConnection, givenScript As String) As Integer
        Dim myDBConnetion As IDbConnection = Nothing
        Dim myDBErrorNumber As Integer = 0
        Dim myDBErrorMsg As String = Nothing
        Dim myDBResultRecords As Integer = 0
        Dim myDataReader As IDataReader = Nothing
        Dim myCommandSQL As String = givenScript
        Dim Rc As Integer = 0
        Try

            Rc = givenConnection.SQLAdapter.openPersistentConnection(myDBConnetion, myDBErrorNumber, myDBErrorMsg)
            If Rc = ePLMS_OK Then
                Rc = givenConnection.SQLAdapter.ExecuteNonQuery(
                myDBConnetion,
                myCommandSQL,
                myDBResultRecords,
                myDBErrorNumber,
                myDBErrorMsg)
            End If

        Catch ex As Exception

        End Try

        Try
            myDBConnetion.Close()
        Catch ex As Exception
        End Try

        Return Rc
    End Function
    Private Shared Sub SetRDAWorking(ByRef givenRDA As eBusinessData, message As String)
        Dim newLevelName As String = Nothing
        Dim newReason As String = message
        Dim setTaskAction As Boolean = True
        newLevelName = "Working"
        setTaskAction = False

        Dim myWorkflowEngine As New eWorkflowEngine(retrieveDBAConnection.getDbaConnection, givenRDA.Id)
        Dim Rc As Integer = myWorkflowEngine.reassignLevel(newLevelName, newReason, setTaskAction)

        myWorkflowEngine = New eWorkflowEngine(retrieveDBAConnection.getDbaConnection, givenRDA.Id)
        Dim myCurrentTask As workflowTask = myWorkflowEngine.getCurrentWorkflowTask
        myCurrentTask.performTask()
        myWorkflowEngine = New eWorkflowEngine(retrieveDBAConnection.getDbaConnection, givenRDA.Id)
        myCurrentTask = myWorkflowEngine.getCurrentWorkflowTask
        myCurrentTask.performTask()

    End Sub
#End Region

End Class
