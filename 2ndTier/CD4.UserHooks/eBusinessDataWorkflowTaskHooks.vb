'
'*------------------------------------------------------------------------------
'*   Copyright (c) 2005 Parallaksis Corporation. All Rights Reserved.
'*
'*   This source code is intended only As a supplement to Collaboration
'*   Desktop Development Tools.  
'*
'*------------------------------------------------------------------------------
' 
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.IO
Imports System.Data
Imports System.Collections
Imports System.Reflection

Public Class eBusinessDataWorkflowTaskHooks
    Implements IDisposable

#Region "Class Description"
    '[-]--------------------------------------------------------------------[-]
    ' |
    ' | Class            : eUserBusinessDataWorkflowTaskHooks
    ' | Created by       : 
    ' | Date             : 
    ' | Description      : 
    ' | Private Methods  :
    ' | Public Methods   :
    ' |     promotePrefix()
    ' |     promoteSuffix()
    '[-]--------------------------------------------------------------------[-]
#End Region

#Region "Global variables"
    '**************************************************************
    '*                                                            *
    '*                G l o b a l  V a r i a b l e s              *
    '*                                                            *
    '**************************************************************
    Private className As String
    Private _bypassPromotePrefix As Boolean = True
    Private _bypassPromoteSuffix As Boolean = True
#End Region

#Region "Properties"
    '**************************************************************
    '*                                                            *
    '*                P r o p e r t i e s                         *
    '*                                                            *
    '**************************************************************
#End Region

#Region "Constructor"
    '**************************************************************
    '*                                                            *
    '*                C o n s t r u c t o r                       *
    '*                                                            *
    '**************************************************************
    Public Sub New(ByRef givenConnection As Object)
        className = "eUserBusinessDataWorkflowTaskHooks"

        _bypassPromotePrefix = eEnvConfigurationController.getEntry(DirectCast(givenConnection, eConnection), "bypassPromotePrefixHook")
        _bypassPromoteSuffix = eEnvConfigurationController.getEntry(DirectCast(givenConnection, eConnection), "bypassPromoteSuffixHook")


    End Sub
#End Region

#Region "Dispose Vars & Methods"
    '**************************************************************
    '*                                                            *
    '*    D i s p o s e   V a r s   &   M e t h o d s             *
    '*                                                            *
    '**************************************************************
    Public Overloads Sub Dispose() Implements System.IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            ' Disposal was triggered manually by the client.
            ' Call Dispose() on any contained classes.
        End If

        ' Release unmanaged resources.
        ' Set large member variables to Nothing (null).
    End Sub
#End Region

#Region "Public Methods"
    '**************************************************************
    '*                                                            *
    '*                P u b l i c  M e t h o d s                  *
    '*                                                            *
    '**************************************************************

#Region "executeWorkflowTaskPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : executeWorkflowTaskPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function executeWorkflowTaskPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByVal preActionBusinessData As eBusinessData, ByVal givenWorkFlowTask As workflowTask, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "executeWorkflowTaskPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        If givenBusinessData.businessDataType = "RDA" And givenBusinessData.Workflow = "WF_RDA_01" And givenBusinessData.Level.ToUpper <> "CLOSED" Then
            Dim remedy As String = ""
            Dim supplier As String = ""
            Dim amount As String = ""
            Dim CDC As String = ""
            Dim ActivityEnd As String = ""
            givenBusinessData.getAttribute("RequestedSupplier", remedy)
            If remedy.ToUpper = "REMEDY" Then
                givenBusinessData.getAttribute("Supplier", supplier)
                givenBusinessData.getAttribute("amount", amount)
                givenBusinessData.getAttribute("CDC", CDC)
                givenBusinessData.getAttribute("ActivityEnd", ActivityEnd)
                Try
                    If (Not String.IsNullOrEmpty(ActivityEnd)) Then
                        If (ActivityEnd.Contains(" ")) Then
                            givenConnection.Core.dateToString(ActivityEnd, ActivityEnd)
                        End If
                    End If
                    RDA.CheckRemedyBudget(givenConnection, ActivityEnd, CDC, supplier, amount)
                Catch ex As Exception
                    givenConnection.errorMessage = ex.Message
                    Return 90010
                End Try

            End If
        End If
        If givenWorkFlowTask.currentTaskParameters IsNot Nothing Then
            Dim isReject As Boolean = False
            Dim hasReason As Boolean = False
            Dim isCMSCheck As Boolean = False

            For Each taskName As String In givenWorkFlowTask.currentTaskParameters.Keys
                If taskName.ToLower.EndsWith("_action") Then
                    isReject = givenWorkFlowTask.currentTaskParameters(taskName).ToString.ToLower = ePLMSRejectedCheck.ToLower
                End If
                If taskName.ToLower.EndsWith("_comment") Then
                    hasReason = Not String.IsNullOrEmpty(givenWorkFlowTask.currentTaskParameters(taskName))
                End If
            Next
            If isReject AndAlso Not hasReason Then
                Dim myMessage As New messageStackEntry
                myMessage.Code = -101
                myMessage.Text = "Fill a reason for the reject"
                givenConnection.messageStack.Clear()
                givenConnection.messageStack.Push(myMessage)
                givenConnection.errorMessage = "Fill a reason for the reject"
                returnCode = myMessage.Code
                GoTo ExitFunction
                'Throw New Exception("Fill a reason for the reject")
            End If
            If Not isReject Then
                If givenWorkFlowTask.getTaskName.ToUpper() = "CMS CHECK" Then
                    Dim atc As String = ""

                    Dim myCBS As String = ""
                    givenBusinessData.getAttribute("CBS", myCBS)
                    If myCBS.ToLower = "materials" Then
                        givenBusinessData.getAttribute("BaanIn_Dimensione3", atc)
                    Else
                        givenBusinessData.getAttribute("BaanIn_ATC", atc)
                    End If
                    If String.IsNullOrEmpty(atc) Then
                        Dim myMessage As New messageStackEntry
                        myMessage.Code = -101
                        myMessage.Text = "Please, fill a valid ATC"
                        givenConnection.messageStack.Clear()
                        givenConnection.messageStack.Push(myMessage)
                        givenConnection.errorMessage = "Please, fill a valid ATC"
                        returnCode = myMessage.Code
                    End If
                End If
            End If



        End If
        '    returnCode = ePLMS_OK

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "executeWorkflowTaskSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : executeWorkflowTaskSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function executeWorkflowTaskSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByVal preActionBusinessData As eBusinessData, ByVal givenWorkFlowTask As workflowTask, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String


        dummyFunctionName = "executeWorkflowTaskSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")
        If givenBusinessData.businessDataType = "RDA" Then
            Dim myDba As eConnection = retrieveDBAConnection.getDbaConnection()
            If givenWorkFlowTask.currentTaskParameters IsNot Nothing AndAlso givenBusinessData.businessDataType = "RDA" Then
                If givenWorkFlowTask.currentTaskParameters.Count > 0 Then
                    Dim shortDateFormat = eEnvConfigurationController.getEntry(givenConnection, "shortDateFormat")

                    Dim myBD = New eBusinessData(myDba)
                    If myBD.Load(givenBusinessData.Id) = ePLMS_OK Then
                        myBD.Reserve()
                        myBD.changeAttribute("ActionDate", Now.ToString(shortDateFormat))
                        myBD.changeAttribute("ActionOperation", "")
                        myBD.changeAttribute("ActionComment", "")
                        For Each key As String In givenWorkFlowTask.currentTaskParameters.Keys
                            If key.ToLower.EndsWith("_action") Then
                                myBD.changeAttribute("ActionOperation", givenWorkFlowTask.currentTaskParameters(key))
                            ElseIf key.ToLower.EndsWith("_comment") Then
                                myBD.changeAttribute("ActionComment", "" & givenWorkFlowTask.currentTaskParameters(key))
                            End If
                        Next
                        myBD.Unreserve()
                    End If
                End If
            End If



            OLTPHelper.Update(givenBusinessData)
        End If

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function




#End Region

#End Region





    '=======================================================================

End Class
