﻿Public Class RDAConst

    Public Const RDA_ERROR_PROJECT_NODETECTED As String = "Error(CE001). Project was not detected"
    Public Const RDA_ERROR_AREA_NODETECTED As String = "Error(CE002). Area was not detected"
    Public Const RDA_ERROR_PROJECT_AREA_NODETECTED As String = "Error(CE003). Project and Area were not detected"
    Public Const RDA_ERROR_WORKFLOW_MAPPING As String = "Error(CE004). Please contact Administrator, error in workflow request."
    Public Const RDA_ERROR_WORKFLOW_MAPPING_2 As String = "Error(CE005). Please contact Administrator, error in workflow request."

End Class
