'
'*------------------------------------------------------------------------------
'*   Copyright (c) 2005 Parallaksis Corporation. All Rights Reserved.
'*
'*   This source code is intended only as a supplement to Collaboration
'*   Desktop Development Tools.  
'*
'*------------------------------------------------------------------------------
' 
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.IO
Imports System.Data
Imports System.Collections
Imports System.Reflection

    Public Class eBusinessDataFileHooks
        Implements IDisposable

#Region "Class Description"
        '[-]--------------------------------------------------------------------[-]
        ' |
        ' | Class            : eBusinessDataFileHooks
        ' | Created by       : 
        ' | Date             : 
        ' | Description      : 
        ' | Private Methods  :
        ' | Public Methods   :
        ' |     changeAttributeBusinessDataFilePrefix()
        ' |     changeAttributeBusinessDataFileSuffix()
        ' |     checkInBusinessDataFilePrefix()
        ' |     checkInBusinessDataFileSuffix()
        ' |     checkOutBusinessDataFilePrefix()
        ' |     checkOutBusinessDataFileSuffix()
        ' |     copyOutBusinessDataFilePrefix()
        ' |     copyOutBusinessDataFileSuffix()
        ' | Properties       :
        '[-]--------------------------------------------------------------------[-]
#End Region

#Region "Global variables"
        '**************************************************************
        '*                                                            *
        '*                G l o b a l  V a r i a b l e s              *
        '*                                                            *
        '**************************************************************
        Private className                 As String
#End Region

#Region "Properties"
        '**************************************************************
        '*                                                            *
        '*                P r o p e r t i e s                         *
        '*                                                            *
        '**************************************************************
#End Region

#Region "Constructor"
        '**************************************************************
        '*                                                            *
        '*                C o n s t r u c t o r                       *
        '*                                                            *
        '**************************************************************
        Public Sub New()
	    className = "eBusinessDataHooks"
        End Sub
#End Region

#Region "Dispose Vars & Methods"
        '**************************************************************
        '*                                                            *
        '*    D i s p o s e   V a r s   &   M e t h o d s             *
        '*                                                            *
        '**************************************************************
        Public Overloads Sub Dispose() Implements System.IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                ' Disposal was triggered manually by the client.
                ' Call Dispose() on any contained classes.
            End If

            ' Release unmanaged resources.
            ' Set large member variables to Nothing (null).
        End Sub
#End Region

#Region "Private Methods"
        '**************************************************************
        '*                                                            *
        '*                P r i v a t e   M e t h o d s               *
        '*                                                            *
        '**************************************************************
#End Region

#Region "Public Methods"
        '**************************************************************
        '*                                                            *
        '*                P u b l i c  M e t h o d s                  *
        '*                                                            *
        '**************************************************************
#Region "changeAttribute"
#Region "changeAttributeBusinessDataFilePrefix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : changeAttributeBusinessDataFilePrefix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function changeAttributeBusinessDataFilePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef givenAttributeName As String, ByRef givenValue As String, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "changeAttributeBusinessDataFilePrefix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region

#Region "changeAttributeBusinessDataFileSuffix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : changeAttributeBusinessDataFileSuffix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function changeAttributeBusinessDataFileSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef givenAttributeName As String, ByRef givenValue As String, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "changeAttributeBusinessDataFileSuffix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region
#End Region

#Region "checkIn"
#Region "checkInBusinessDataFilePrefix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : checkInBusinessDataFilePrefix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function checkInBusinessDataFilePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "checkInBusinessDataFilePrefix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region

#Region "checkInBusinessDataFileSuffix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : checkInBusinessDataFileSuffix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function checkInBusinessDataFileSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "checkInBusinessDataFileSuffix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region
#End Region

#Region "checkOut"
#Region "checkOutBusinessDataFilePrefix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : checkOutBusinessDataFilePrefix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function checkOutBusinessDataFilePrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "checkOutBusinessDataFilePrefix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region

#Region "checkOutBusinessDataFileSuffix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : checkOutBusinessDataFileSuffix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function checkOutBusinessDataFileSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "checkOutBusinessDataFileSuffix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region
#End Region

#Region "copyOut"
#Region "copyOutBusinessDataFilePrefix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : copyOutBusinessDataFilePrefix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function copyOutBusinessDataFilePrefix(ByRef givenConnection As eConnection, ByVal givenVaultName As String, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "copyOutBusinessDataFilePrefix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region

#Region "copyOutBusinessDataFileSuffix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : copyOutBusinessDataFileSuffix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function copyOutBusinessDataFileSuffix(ByRef givenConnection As eConnection, ByVal givenVaultName As String, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataFile As eBusinessDataFile, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "copyOutBusinessDataFileSuffix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region
#End Region
#End Region
    End Class
