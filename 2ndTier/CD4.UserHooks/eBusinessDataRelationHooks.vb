'
'*------------------------------------------------------------------------------
'*   Copyright (c) 2005 Parallaksis Corporation. All Rights Reserved.
'*
'*   This source code is intended only as a supplement to Collaboration
'*   Desktop Development Tools.  
'*
'*------------------------------------------------------------------------------
' 
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.IO
Imports System.Data
Imports System.Collections
Imports System.Reflection

    Public Class eBusinessDataRelationHooks
        Implements IDisposable

#Region "Class Description"
        '[-]--------------------------------------------------------------------[-]
        ' |
        ' | Class            : eBusinessDataRelationHooks
        ' | Created by       : 
        ' | Date             : 
        ' | Description      : 
        ' | Private Methods  :
        ' | Public Methods   :
        ' |     resolveLatest()
        ' | Properties       :
        '[-]--------------------------------------------------------------------[-]
#End Region

#Region "Global variables"
        '**************************************************************
        '*                                                            *
        '*                G l o b a l  V a r i a b l e s              *
        '*                                                            *
        '**************************************************************
        Private className                 As String
#End Region

#Region "Properties"
        '**************************************************************
        '*                                                            *
        '*                P r o p e r t i e s                         *
        '*                                                            *
        '**************************************************************
#End Region

#Region "Constructor"
        '**************************************************************
        '*                                                            *
        '*                C o n s t r u c t o r                       *
        '*                                                            *
        '**************************************************************
        Public Sub New()
	    className = "eBusinessDataHooks"
        End Sub
#End Region

#Region "Dispose Vars & Methods"
        '**************************************************************
        '*                                                            *
        '*    D i s p o s e   V a r s   &   M e t h o d s             *
        '*                                                            *
        '**************************************************************
        Public Overloads Sub Dispose() Implements System.IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                ' Disposal was triggered manually by the client.
                ' Call Dispose() on any contained classes.
            End If

            ' Release unmanaged resources.
            ' Set large member variables to Nothing (null).
        End Sub
#End Region

#Region "Private Methods"
        '**************************************************************
        '*                                                            *
        '*                P r i v a t e   M e t h o d s               *
        '*                                                            *
        '**************************************************************
#End Region

#Region "Public Methods"
        '**************************************************************
        '*                                                            *
        '*                P u b l i c  M e t h o d s                  *
        '*                                                            *
        '**************************************************************
#Region "resolveLatest"
#Region "resolveLatestSuffix"
        '[-]--------------------------------------------------------------------[-]
	' | Method          : resolveLatestSuffix()
	' | Created by      :
	' | Date            :
	' | Description     :
	' | Parameters      :
	' |
	' | Returns         : ePLMS_OK
	' |                   ePLMS_CONTINUE
	' |                   ePLMS_ERROR
	' |
	'[-]--------------------------------------------------------------------[-]
        Public Function resolveLatestSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessRelation As eBusinessDataRelation, ByRef givenRevisionList() As String, ByRef returnRevision As String, ByRef returnCode As Integer) As Integer
	    Dim dummyFunctionName   As String
	    
	    dummyFunctionName = "resolveLatestSuffix"
	    givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

	    returnRevision = givenRevisionList(0)
	    returnCode = ePLMS_CONTINUE

ExitFunction:
            givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
	    givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
	    givenConnection.Logger.handleFunctionEnd(returnCode)

	    Return(returnCode)
	End Function
#End Region
#End Region
#End Region
    End Class
