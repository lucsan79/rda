﻿Public Class eUtility
    Public Shared Function GetDoubleValue(ByVal givenValue As String) As Double
        If IsNumeric(givenValue) Then
            givenValue = givenValue.ToString.Replace(",", ".")
            Return Convert.ToDouble(givenValue, System.Globalization.CultureInfo.GetCultureInfo("en-US"))
        Else
            Return 0
        End If

    End Function

    Public Shared Function GetCDCRoleNameForSearch(basicRoleName As String, givenCDC As String) As String
        Dim retValue As String = ""
        givenCDC = givenCDC.Replace(" & ", "%")
        givenCDC = givenCDC.Replace("&", "%")
        givenCDC = givenCDC.Replace("_", "%")
        givenCDC = givenCDC.Replace(" ", "%")

        retValue = basicRoleName & givenCDC
        Return retValue
    End Function
    Public Shared Function GetCDCRoleNameForSave(basicRoleName As String, givenCDC As String) As String
        Dim retValue As String = basicRoleName & givenCDC
        retValue = retValue.Replace(" & ", "-")
        givenCDC = givenCDC.Replace("&", "-")
        retValue = retValue.Replace(" ", "_")

        Return retValue
    End Function


    Public Class DynamicRoleNamePrefix
        Public Const UR As String = "UR_"
        Public Const PrOfCDC As String = "PRCDC_"
        Public Const PlannerOfCDC As String = "PL_"
    End Class

End Class
