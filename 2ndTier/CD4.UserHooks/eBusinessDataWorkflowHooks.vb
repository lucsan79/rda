'
'*------------------------------------------------------------------------------
'*   Copyright (c) 2005 Parallaksis Corporation. All Rights Reserved.
'*
'*   This source code is intended only As a supplement to Collaboration
'*   Desktop Development Tools.  
'*
'*------------------------------------------------------------------------------
' 
Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs
Imports System.IO
Imports System.Data
Imports System.Collections
Imports System.Reflection

Public Class eBusinessDataWorkflowHooks
    Implements IDisposable

#Region "Class Description"
    '[-]--------------------------------------------------------------------[-]
    ' |
    ' | Class            : eBusinessDataWorkflowHooks
    ' | Created by       : 
    ' | Date             : 
    ' | Description      : 
    ' | Private Methods  :
    ' | Public Methods   :
    ' |     buildTaskEntryListPrefix
    ' |     executeWorkflowTaskPrefix
    ' |     executeWorkflowTaskSuffix
    ' |     generateHTMLMailSuffix()
    '[-]--------------------------------------------------------------------[-]
#End Region

#Region "Global variables"
    '**************************************************************
    '*                                                            *
    '*                G l o b a l  V a r i a b l e s              *
    '*                                                            *
    '**************************************************************
    Private className As String
#End Region

#Region "Properties"
    '**************************************************************
    '*                                                            *
    '*                P r o p e r t i e s                         *
    '*                                                            *
    '**************************************************************
#End Region

#Region "Constructor"
    '**************************************************************
    '*                                                            *
    '*                C o n s t r u c t o r                       *
    '*                                                            *
    '**************************************************************
    Public Sub New()
        className = "eBusinessDataHooks"
    End Sub
#End Region

#Region "Dispose Vars & Methods"
    '**************************************************************
    '*                                                            *
    '*    D i s p o s e   V a r s   &   M e t h o d s             *
    '*                                                            *
    '**************************************************************
    Public Overloads Sub Dispose() Implements System.IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            ' Disposal was triggered manually by the client.
            ' Call Dispose() on any contained classes.
        End If

        ' Release unmanaged resources.
        ' Set large member variables to Nothing (null).
    End Sub
#End Region

#Region "Private Methods"
    '**************************************************************
    '*                                                            *
    '*                P r i v a t e   M e t h o d s               *
    '*                                                            *
    '**************************************************************
#End Region

#Region "Public Methods"
    '**************************************************************
    '*                                                            *
    '*                P u b l i c  M e t h o d s                  *
    '*                                                            *
    '**************************************************************
#Region "buildTaskEntryListPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : buildTaskEntryListPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function buildTaskEntryListPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByVal givenWorkFlowTask As workflowTask, ByRef givenUserList As Hashtable, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "buildTaskEntryListPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")
        'Attributo Area distrimina il pr
        'progetto -> Area
        'MAtrice
        If givenBusinessData.businessDataType = "RDA" Then

            Dim rdaCompetence As String = ""
            Dim rdaCBS As String = ""
            Dim cdc As String = ""

            givenBusinessData.getAttribute("RDACompetence", rdaCompetence)
            givenBusinessData.getAttribute("CBS", rdaCBS)
            givenBusinessData.getAttribute("CDC", cdc)

            Dim Area As String = ""
            Dim myUserList As New Generic.List(Of String)
            Dim myRole As String = ""
            Dim CBS As String = ""

            If givenWorkFlowTask.getTaskName.Replace(" ", "_") = "PR_CHECK" Then

                'givenUserList.Clear()
                If Not String.IsNullOrEmpty(rdaCompetence) Then

                    If (rdaCompetence.ToUpper = "VEHICLE" Or rdaCompetence.ToUpper = "POWERTRAIN") AndAlso (rdaCBS.ToUpper = "R&D HOURS" Or rdaCBS.ToUpper = "WORK PACKAGE") Then
                        For Each myValue As String In givenUserList.Values
                            myRole = myValue
                            Exit For
                        Next

                        givenUserList.Clear()
                        myUserList.Clear()
                        Dim mySelectedCDC As String = eUtility.GetCDCRoleNameForSearch(eUtility.DynamicRoleNamePrefix.PlannerOfCDC, cdc)
                        Dim myQuery As String = "SELECT    eplms.eUser.eId  as cdcplannerId,    eplms.eUser.eName AS cdcplanner " &
                                            " From eplms.eProjectRole INNER Join " &
                                            " eplms.eRole ON eplms.eProjectRole.eRoleId = eplms.eRole.eId INNER JOIN " &
                                            " eplms.eProject ON eplms.eProjectRole.eProjectId = eplms.eProject.eId INNER JOIN " &
                                            " eplms.eProjectRoleUser ON eplms.eProjectRole.eId = eplms.eProjectRoleUser.eProjectRoleId INNER JOIN " &
                                            " eplms.eUser ON eplms.eProjectRoleUser.eUserId = eplms.eUser.eId " &
                                            " WHERE        (eplms.eProject.eName = N'RDA') AND (eplms.eRole.eName LIKE N'{0}')"
                        myQuery = String.Format(myQuery, mySelectedCDC)
                        Dim myTable As DataTable = RDA.ExecuteSql(myQuery, givenConnection)
                        If myTable IsNot Nothing AndAlso myTable.Rows.Count > 0 Then
                            For Each row As DataRow In myTable.Rows
                                If Not myUserList.Contains(row("cdcplanner").ToString.Trim) Then
                                    myUserList.Add(row("cdcplanner").ToString.Trim)
                                End If
                                givenUserList.Add(row("cdcplanner").ToString.Trim & "," & myRole, myRole)
                            Next
                        End If
                    Else


                        Dim myQuery As String = "" &
                        "SELECT        eplms.eListItem.eValue AS RoleName,  eplms.eUser.eName AS UserName " &
                        "FROM            eplms.eListItem INNER JOIN                                                                                    " &
                        "                         eplms.eListItem AS eListItem_1 ON eplms.eListItem.eListId = eListItem_1.eValueId INNER JOIN          " &
                        "                         eplms.eListItem AS eListItem_2 ON eListItem_1.eListId = eListItem_2.eValueId     INNER JOIN          " &
                        "                         eplms.eListItem AS eListItem_3 ON eplms.eListItem.eValueId = eListItem_3.eListId INNER JOIN          " &
                        " eplms.eUser ON eListItem_3.eValue = eplms.eUser.eName " &
                        " WHERE        (eListItem_2.eValue = 'PROJECT_CDCAPPROVERS') AND (eListItem_1.eValue = '{0}') "


                        If rdaCompetence.ToUpper = "TRANSVERSAL" Then
                            myQuery = "SELECT        eplms.eListItem.eValue AS RoleName, eListItem_3.eValue  as ActivityNPI, eplms.eUser.eName  AS UserName                                   " &
                                    "FROM            eplms.eListItem INNER JOIN                                                                                    " &
                                    "                         eplms.eListItem AS eListItem_1 ON eplms.eListItem.eListId = eListItem_1.eValueId INNER JOIN          " &
                                    "                         eplms.eListItem AS eListItem_2 ON eListItem_1.eListId = eListItem_2.eValueId     INNER JOIN          " &
                                    "                         eplms.eListItem AS eListItem_3 ON eplms.eListItem.eValueId = eListItem_3.eListId INNER JOIN          " &
                                    "                         eplms.eListItem AS eListItem_4 ON eListItem_3.eValueId = eListItem_4.eListId                     " &
                                    " INNER JOIN  eplms.eUser On eListItem_4.eValue = eplms.eUser.eName " &
                                    "WHERE        (eListItem_2.eValue = 'PROJECT_CDCAPPROVERS') AND (eListItem_1.eValue = '{0}') "
                        End If

                        myQuery = String.Format(myQuery, givenBusinessData.Project)
                        Dim myTable As DataTable = RDA.ExecuteSql(myQuery, givenConnection)
                        If myTable IsNot Nothing Then


                            Dim ActivityNPI As String = ""

                            givenBusinessData.getAttribute("ActivityNPI", ActivityNPI)
                            If Not String.IsNullOrEmpty(rdaCompetence) Then
                                Dim selectRows() As DataRow
                                Select Case rdaCompetence.ToUpper
                                    Case "VEHICLE"
                                        myRole = "PRCDC_VEHICLE"
                                        selectRows = myTable.Select("Rolename='" & cdc.ToUpper & "'")
                                    Case "POWERTRAIN"
                                        myRole = "PRCDC_POWERTRAIN"
                                        selectRows = myTable.Select("Rolename='POWERTRAIN'")
                                    Case "TRANSVERSAL"
                                        myRole = "PRCDC_TRANSVERSAL"
                                        selectRows = myTable.Select("Rolename='" & cdc.ToUpper & "' and ActivityNPI='" & ActivityNPI.ToUpper & "'")




                                End Select
                                If selectRows.Length > 0 Then
                                    For Each myValue As String In givenUserList.Values
                                        myRole = myValue
                                        Exit For
                                    Next
                                    givenUserList.Clear()
                                    Select Case rdaCompetence.ToUpper
                                        Case "POWERTRAIN"
                                            For Each row As DataRow In selectRows
                                                myUserList.Add(row("UserName"))
                                                givenUserList.Add(row("UserName").ToString.Trim & "," & myRole, myRole)
                                            Next
                                        Case Else
                                            myUserList.Add(selectRows(0)("UserName"))
                                            givenUserList.Add(selectRows(0)("UserName").ToString.Trim & "," & myRole, myRole)
                                    End Select
                                Else
                                    givenUserList.Clear()
                                End If
                            End If
                        Else
                            givenUserList.Clear()
                        End If
                    End If

                Else

                    givenBusinessData.getAttribute("Area", Area)
                    givenBusinessData.getAttribute("CBS", CBS)

                    If Area <> "" Then
                        Area = Area.Replace(" ", "_")
                        Dim myProject As New eProject(givenConnection)
                        myProject.Load(givenBusinessData.Project)
                        Dim returnProjectRoleList() As eProjectRole
                        myProject.getAllProjectRoleList(returnProjectRoleList)
                        For Each myProjectRole As eProjectRole In returnProjectRoleList
                            If myProjectRole.roleName.ToUpper.EndsWith(Area.ToUpper) AndAlso myProjectRole.roleName.ToUpper.StartsWith("PR_") Then
                                myRole = myProjectRole.roleName
                                Dim returnProjectRoleUserList() As eProjectRoleUser
                                myProjectRole.getProjectRoleUserList(returnProjectRoleUserList)

                                If Not returnProjectRoleUserList Is Nothing Then
                                    For Each myProjectRoleUser As eProjectRoleUser In returnProjectRoleUserList
                                        If Not myUserList.Contains(myProjectRoleUser.userName) Then
                                            myUserList.Add(myProjectRoleUser.userName)
                                        End If
                                    Next
                                End If
                            End If
                        Next

                        For Each myValue As String In givenUserList.Values
                            myRole = myValue
                            Exit For
                        Next

                        givenUserList.Clear()
                        ''Controllo utenza ALL

                        For Each myUser As String In myUserList
                            givenUserList.Add(myUser.Trim & "," & myRole, myRole)
                        Next
                    Else
                        givenUserList.Clear()
                    End If

                End If




            ElseIf givenWorkFlowTask.getTaskName.Replace(" ", "_") = "PPM_CONTROL_CHECK" OrElse
                givenWorkFlowTask.getTaskName.Replace(" ", "_") = "PMP CONTROL CHECK" Then

                Dim myGroup As New eGroup(retrieveDBAConnection.getDbaConnection)
                If myGroup.Load("PWT CONTROLLER") = ePLMS_OK Then
                    Dim myUsers As New ArrayList
                    myGroup.getUserList(myUsers)
                    If rdaCompetence.ToUpper = "POWERTRAIN" Then
                        Dim myUsersPWT As New Hashtable
                        If myUsers IsNot Nothing Then
                            For Each edata As eDataListElement In myUsers
                                For Each ppUser As String In givenUserList.Keys
                                    If ppUser.ToLower.StartsWith(edata.Value.ToLower & ",") Then
                                        If Not myUsersPWT.ContainsKey(ppUser) Then
                                            myUsersPWT.Add(ppUser, givenUserList(ppUser))
                                        End If
                                    End If
                                Next
                            Next
                        End If
                        givenUserList.Clear()
                        givenUserList = myUsersPWT
                    ElseIf rdaCompetence.ToUpper <> "POWERTRAIN" Then
                        Dim myUsersPWT As New Hashtable
                        If myUsers IsNot Nothing Then
                            For Each edata As eDataListElement In myUsers
                                For Each ppUser As String In givenUserList.Keys
                                    If ppUser.ToLower.StartsWith(edata.Value.ToLower & ",") Then
                                        If Not myUsersPWT.ContainsKey(ppUser) Then
                                            myUsersPWT.Add(ppUser, givenUserList(ppUser))
                                        End If
                                    End If
                                Next
                            Next
                        End If
                        For Each key As String In myUsersPWT.Keys
                            givenUserList.Remove(key)
                        Next


                    End If


                End If
                myUserList.Clear()
                For Each key As String In givenUserList.Keys
                    myUserList.Add(key.Split(",")(0))
                Next
            End If
            If myUserList IsNot Nothing AndAlso myUserList.Count > 0 Then
                Dim myTaskEntryQuery As String = "select * from eplms.eBusinessDataTaskEntry where eTaskId={0} and eBusinessDataId={1} and eUser not in ('{2}')"
                myTaskEntryQuery = String.Format(myTaskEntryQuery, givenWorkFlowTask.getTaskId, givenBusinessData.Id, String.Join("','", myUserList.ToArray))
                Dim myOldChecks As DataTable = RDA.ExecuteSql(myTaskEntryQuery, retrieveDBAConnection.getDbaConnection)
                Try
                    If myOldChecks IsNot Nothing AndAlso myOldChecks.Rows.Count > 0 Then
                        myTaskEntryQuery = myTaskEntryQuery.Replace("select * from", "delete ")
                        RDA.ExecuteSql(myTaskEntryQuery, retrieveDBAConnection.getDbaConnection)
                    End If
                Catch ex As Exception

                End Try
            End If



        End If

        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "executeWorkflowTaskPrefix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : executeWorkflowTaskPrefix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function executeWorkflowTaskPrefix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenWorkFlowTask As workflowTask, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "executeWorkflowTaskPrefix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")


        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "executeWorkflowTaskSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : executeWorkflowTaskSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function executeWorkflowTaskSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenWorkFlowTask As workflowTask, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String

        dummyFunctionName = "executeWorkflowTaskSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        OLTPHelper.Update(givenBusinessData)
        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region

#Region "generateHTMLMailSuffix"
    '[-]--------------------------------------------------------------------[-]
    ' | Method          : generateHTMLMailSuffix()
    ' | Created by      :
    ' | Date            :
    ' | Description     :
    ' | Parameters      :
    ' |
    ' | Returns         : ePLMS_OK
    ' |                   ePLMS_CONTINUE
    ' |                   ePLMS_ERROR
    ' |
    '[-]--------------------------------------------------------------------[-]
    Public Function generateHTMLMailSuffix(ByRef givenConnection As eConnection, ByRef givenBusinessData As eBusinessData, ByRef givenBusinessDataCurrentLevel As String, ByRef givenTextMailSubject As String, ByRef givenTextMailBody As String, ByRef givenMailToList() As String, ByRef returnMailSubject As String, ByRef returnMailBody As String, ByRef returnCode As Integer) As Integer
        Dim dummyFunctionName As String
        Dim dummyStringRef1 As String
        Dim dummyStringRef2 As String
        Dim myeWorkflowEngine As eWorkflowEngine
        Dim myeCurrentTask As workflowTask
        Dim mySubjectPrefix As String = ""

        dummyFunctionName = "generateHTMLMailSuffix"
        givenConnection.Logger.handleFunctionBegin(className + "." + dummyFunctionName + "()")

        If givenBusinessData.businessDataType = "RDA" Then
            returnCode = RDA.RDAGenerateMailPrefix(givenConnection, givenBusinessData, givenBusinessDataCurrentLevel, givenTextMailSubject, givenTextMailBody, givenMailToList, returnMailSubject, returnMailBody)
        End If


        returnCode = ePLMS_CONTINUE

ExitFunction:
        givenConnection.Logger.handleLocalError(returnCode, className, dummyFunctionName)
        givenConnection.Logger.handleError1(returnCode, "Function=(" & dummyFunctionName & ");Class=(" & className & ");ReturnCode=(" & returnCode & ")", 0, "")
        givenConnection.Logger.handleFunctionEnd(returnCode)

        Return (returnCode)
    End Function
#End Region
#End Region
End Class
