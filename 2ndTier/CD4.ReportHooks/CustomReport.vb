Imports Parallaksis.ePLMSM40
Imports Parallaksis.ePLMSM40.Defs

Public Enum reportType
    Example1
    Example2
End Enum
Public Class CustomReport
    Public Sub New(ByRef givenConnection As eConnection)
        myeLocalConnection = givenConnection
        RPT_PATH = IO.Path.Combine(givenConnection.ePLMS_HOME, ePLMSEnvPath, "Reports")
    End Sub

    Public Function makeReport(report As String, reportParamenter As Dictionary(Of String, String), ByRef returnReportName As String) As Byte()
        Dim output() As Byte = Nothing
        Select Case report.ToLower
            Case "myexample"
                output = goExampleReport(myeLocalConnection, Convert.ToInt32(reportParamenter("BusinessDataId")), reportParamenter, returnReportName)
        End Select
        Return output
    End Function

    Public Function goExampleReport(ByRef givenConnection As eConnection, ByVal idBusinessdataRoot As Integer, reportParamenter As Dictionary(Of String, String), ByRef returnReportName As String) As Byte()
        Dim returnValue() As Byte = Nothing
        Return returnValue
    End Function

End Class
