Imports Microsoft.VisualBasic
Imports Parallaksis.ePLMSM40
Public Module Common
    Public myeLocalConnection As eConnection
    Public RPT_PATH As String


    Public Function ExecuteSql(givenSql As String, getUserConnection As eConnection) As DataTable
        Dim myTable As New DataTable
        Dim myFind As New eFind(getUserConnection)
        myFind.queryString = "$(" + givenSql + ")"
        myFind.Prepare(Nothing)
        myFind.Execute(myTable)
        Return myTable
    End Function

End Module
