using System;

using Vts.UnitTest;

using MyXaml.Core;
using MyXaml.Core.Exceptions;

namespace MyXamlCoreUnitTests
{
	public class TestClass
	{
		[MyXamlAutoInitialize] private string name;
		[MyXamlAutoInitialize("fullName")] private string name2;

		public string Name
		{
			get {return name;}
		}

		public string FullName
		{
			get {return name2;}
		}

		public TestClass()
		{
			name=null;
			name2=null;
		}
	}

	[TestFixture]
	public class FieldInitializationTests
	{
		protected Parser parser;

		[SetUp]
		public void SetUp()
		{
			parser=new Parser();
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void NullTargetTest()
		{
			parser.InitializeFields(null);
		}

		[Test]
		[ExpectedException(typeof(NoReferenceException))]
		public void NoReferenceTest()
		{
			TestClass tc=new TestClass();
			parser.InitializeFields(tc);
		}

		[Test]
		[ExpectedException(typeof(AutoInitializeException))]
		public void AutoInitializeTypeConversionTest()
		{
			TestClass tc=new TestClass();
			parser.AddReference("name", 1);
			parser.InitializeFields(tc);
		}

		[Test]
		public void AutoInitializeTest()
		{
			TestClass tc=new TestClass();
			parser.AddReference("name", "Marc");
			parser.AddReference("fullName", "Marc Clifton");
			parser.InitializeFields(tc);
			Assertion.Assert(tc.Name=="Marc", "Unexpected result.");
		}
		
		[Test]
		public void AliasTest()
		{
			TestClass tc=new TestClass();
			parser.AddReference("name", "Marc");
			parser.AddReference("fullName", "Marc Clifton");
			parser.InitializeFields(tc);
			Assertion.Assert(tc.FullName=="Marc Clifton", "Unexpected result.");
		}
	}
}