using System;

using Vts.UnitTest;

using MyXaml.Core;
using MyXaml.Core.Exceptions;

namespace MyXamlCoreUnitTests
{
	public class FirstTestClass
	{
		private SecondTestClass secondTestClass;

		public SecondTestClass SecondTestClass
		{
			get {return secondTestClass;}
		}

		public FirstTestClass()
		{
			secondTestClass=new SecondTestClass();
		}
	}

	public class SecondTestClass
	{
		protected string val;

		public string Value
		{
			get {return val;}
		}

		public SecondTestClass()
		{
			val="Marc";
		}
	}

	[TestFixture]
	public class ReferenceTests
	{
		protected Parser parser;

		[SetUp]
		public void SetUp()
		{
			parser=new Parser();
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void AddReferenceNullNameTest()
		{
			parser.AddReference(null, this);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void RemoveReferenceNullNameTest()
		{
			parser.RemoveReference(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void ContainsReferenceNullNameTest()
		{
			parser.ContainsReference(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void GetReferenceNullNameTest()
		{
			parser.GetReference(null);
		}
		
		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void UpdateReferenceNullNameTest()
		{
			parser.UpdateReference(null, this);
		}
		
		[Test]
		[ExpectedException(typeof(ReferenceExistsException))]
		public void AddExistingReferenceTest()
		{
			parser.AddReference("Test", this);
			parser.AddReference("Test", this);
		}
		
		[Test]
		[ExpectedException(typeof(NoReferenceException))]
		public void RemoveMissingReferenceTest()
		{
			parser.RemoveReference("Test");
		}

		[Test]
		public void AddReferenceTest()
		{
			parser.AddReference("Test", this);
			Assertion.Assert(parser.GetReference("Test")==this, "Unexpected result.");
		}

		[Test]
		public void UpdateReferenceTest()
		{
			parser.AddReference("Test", this);
			parser.UpdateReference("Test", parser);
			Assertion.Assert(parser.GetReference("Test")==parser, "Unexpected result.");
		}

		[Test]
		public void RemoveReferenceTest()
		{
			parser.AddReference("Test", this);
			parser.RemoveReference("Test");
			Assertion.Assert(!parser.ContainsReference("Test"), "Unexpected result.");
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void BadSourceCopyTest()
		{
			parser.CopyReferencesFrom(null);
		}

		[Test]
		public void CopyReferencesTest()
		{
			parser.AddReference("Test", this);
			Parser p2=new Parser();
			p2.CopyReferencesFrom(parser);
			Assertion.Assert(p2.ContainsReference("Test"), "Unexpected result.");
		}

		[Test]
		[ExpectedException(typeof(NoReferenceException))]
		public void SimpleResolveFailureTest()
		{
			FirstTestClass ftc=new FirstTestClass();
			parser.AddReference("ftc", ftc);
			parser.ResolveValue("ftc2");
		}

		[Test]
		[ExpectedException(typeof(ImproperComplexReferenceException))]
		public void ComplexResolveFailureTest()
		{
			FirstTestClass ftc=new FirstTestClass();
			parser.AddReference("ftc", ftc);
			parser.ResolveValue("ftc.foobar");
		}

		[Test]
		public void ResolveSimpleValueTest()
		{
			FirstTestClass ftc=new FirstTestClass();
			parser.AddReference("ftc", ftc);
			object obj=parser.ResolveValue("ftc");
			Assertion.Assert(ftc==obj, "Unexpected result.");
		}

		[Test]
		public void ResolveComplexPropertyValueTest()
		{
			FirstTestClass ftc=new FirstTestClass();
			parser.AddReference("ftc", ftc);
			object obj=parser.ResolveValue("ftc.SecondTestClass.Value");
			Assertion.Assert(obj.ToString()=="Marc", "Unexpected result.");
		}

		[Test]
		public void ResolveComplexFieldValueTest()
		{
			FirstTestClass ftc=new FirstTestClass();
			parser.AddReference("ftc", ftc);
			object obj=parser.ResolveValue("ftc.secondTestClass.val");
			Assertion.Assert(obj.ToString()=="Marc", "Unexpected result.");
		}
	}
}
