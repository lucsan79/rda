using System;
using System.Xml;

using Vts.UnitTest;

using MyXaml.Core;
using MyXaml.Core.Exceptions;

namespace ANamespace
{
	public class ATestClass
	{
		protected string name;

		public string Name
		{
			get {return name;}
			set {name=value;}
		}
	}
}

namespace MyXamlCoreUnitTests
{
	public class ATestClass
	{
		protected string name;

		public string Name
		{
			get {return name;}
			set {name=value;}
		}
	}

	public class NoDefaultConstructor
	{
		public NoDefaultConstructor(int foo)
		{
		}
	}

	[TestFixture]
	public class SimpleInstantiationTests
	{
		protected Parser parser;

		[SetUp]
		public void SetUp()
		{
			parser=new Parser();
		}

		[Test]
		[ExpectedException(typeof(XmlException))]
		public void BadXmlTest()
		{
			string badxml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			badxml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			badxml+="<MyXaml>\r\n";												// missing closing tag.
			parser.InstantiateFromString(badxml, "*");
		}

		[Test]
		[ExpectedException(typeof(MissingGraphException))]
		public void MissingGraphTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		[ExpectedException(typeof(GraphNotFoundException))]
		public void GraphNotFoundTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml>\r\n";
			xml+="  <Test Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "Bar");
		}

		[Test]
		[ExpectedException(typeof(NoReferenceException))]
		public void NoReferenceTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\">\r\n";
			xml+="  <Test ref:Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "Foo");
		}

		[Test]
		[ExpectedException(typeof(ImproperNamespaceFormatException))]
		public void ImproperNamespaceTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="xmlns=\"System.Windows.Forms, System.Windows.Forms, Version=1.0.5000.0, Culture=neutral\">\r\n";
			xml+="  <Form Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "Foo");
		}

		[Test]
		[ExpectedException(typeof(MissingAssemblyException))]
		public void MissingAssemblyTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="xmlns=\"FooTest\">\r\n";
			xml+="  <Form Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "Foo");
		}

		[Test]
		public void ShortFormTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <ATestClass Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			object obj=parser.InstantiateFromString(xml, "Foo");
			Assertion.Assert(obj is ATestClass, "Unexpected result.");
		}

		[Test]
		public void MediumFormTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"ANamespace, MyXamlCoreUnitTests\">\r\n";
			xml+="  <ATestClass Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			object obj=parser.InstantiateFromString(xml, "Foo");
			Assertion.Assert(obj is ANamespace.ATestClass, "Unexpected result.");
		}

		[Test]
		public void LongFormTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"ANamespace, MyXamlCoreUnitTests, Version=1.0.0.0, Culture=neutral, PublicKeyToken=\">\r\n";
			xml+="  <ATestClass Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			object obj=parser.InstantiateFromString(xml, "Foo");
			Assertion.Assert(obj is ANamespace.ATestClass, "Unexpected result.");
		}

		[Test]
		[ExpectedException(typeof(UnknownTypeException))]
		public void UnkownTypeTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <MissingClass Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "Foo");
		}

		[Test]
		[ExpectedException(typeof(InstantiationException))]
		public void InstantiationErrorTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <NoDefaultConstructor Name='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "Foo");
		}

		[Test]
		public void WildcardTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <ATestClass/>\r\n";
			xml+="</MyXaml>\r\n";
			object obj=parser.InstantiateFromString(xml, "*");
			Assertion.Assert(obj is ATestClass, "Unexpected result.");

		}

		[Test]
		public void AddToReferenceCollectionTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <ATestClass def:Name='testClass'/>\r\n";
			xml+="</MyXaml>\r\n";
			object obj=parser.InstantiateFromString(xml, "*");
			Assertion.Assert(obj is ATestClass, "Result is not ATestClass.");
			Assertion.Assert(parser.ContainsReference("testClass"), "Expected reference.");
			Assertion.Assert(parser.GetReference("testClass") is ATestClass, "Reference is not ATestClass.");
		}
	}
}
