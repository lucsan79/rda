using System;
using System.Xml;

using Vts.UnitTest;

using MyXaml.Core;
using MyXaml.Core.Exceptions;

namespace MyXamlCoreUnitTests
{
	public class AnotherHandler
	{
		protected bool clicked;

		public bool Clicked
		{
			get {return clicked;}
		}

		public void OnClick(object sender, EventArgs e)
		{
			clicked=true;
		}
	}

	public class AnEventTestClass
	{
		public event EventHandler Click;

		protected string val;
		protected bool clicked;

		public string ReadOnlyValue
		{
			get {return val;}
		}

		public string ReadWriteValue
		{
			get {return val;}
			set {val=value;}
		}

		public bool Clicked
		{
			get {return clicked;}
		}

		public void DoClick()
		{
			Click(null, EventArgs.Empty);
		}

		public void OnClick(object sender, EventArgs e)
		{
			clicked=true;
		}
	}

	[TestFixture]
	public class EventWireupTests
	{
		protected Parser parser;

		[SetUp]
		public void SetUp()
		{
			parser=new Parser();
		}

		[Test]
		[ExpectedException(typeof(ReferencedEventMissingHandlerException))]
		public void MissingHandlerInReferenceTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <AnEventTestClass Click='{AnotherHandler}'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		public void LocalEventTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <AnEventTestClass Click='OnClick'/>\r\n";
			xml+="</MyXaml>\r\n";

			AnEventTestClass tc=(AnEventTestClass)parser.InstantiateFromString(xml, "*");
			tc.DoClick();
			Assertion.Assert(tc.Clicked, "Event not wired up.");
		}

		[Test]
		public void ReferencedEventTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <AnEventTestClass Click='{AnotherHandler.OnClick}'/>\r\n";
			xml+="</MyXaml>\r\n";

			AnotherHandler h=new AnotherHandler();
			parser.AddReference("AnotherHandler", h);

			AnEventTestClass tc=(AnEventTestClass)parser.InstantiateFromString(xml, "*");
			tc.DoClick();
			Assertion.Assert(h.Clicked, "Event not wired up.");
		}

		// Foo Click='A, B, C'
		// late binding events (use markup pre/post to test early/late binding)
		// handler method doesn't match event signature test.
	}
}

