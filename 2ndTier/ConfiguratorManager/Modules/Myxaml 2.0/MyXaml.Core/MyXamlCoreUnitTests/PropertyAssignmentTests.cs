using System;
using System.ComponentModel;
using System.Xml;

using Vts.UnitTest;

using MyXaml.Core;
using MyXaml.Core.Exceptions;

namespace MyXamlCoreUnitTests
{
	public struct AStruct
	{
		private int a;
		private string b;

		public int Num
		{
			get {return a;}
			set {a=value;}
		}

		public string Str
		{
			get {return b;}
			set {b=value;}
		}
	}

	public class Provider : IExtenderProvider
	{
		protected object obj;
		protected string name;
		protected string text;

		public object Object
		{
			get {return obj;}
		}

		public string Name
		{
			get {return name;}
			set {name=value;}
		}

		public string Text
		{
			get {return text;}
		}

		public void SetText(object obj, string text)
		{
			this.obj=obj;
			this.text=text;
		}

		public bool CanExtend(object extendee)
		{
			return extendee is APropertyTestClass;
		}
	}

	public class DynamicClass
	{
		protected string name;
		protected string text;

		public string Name
		{
			get {return name;}
			set {name=value;}
		}

		public string Text
		{
			get {return text;}
			set {text=value;}
		}

		public override string ToString()
		{
			return text;
		}

	}

	public class APropertyTestClass
	{
		protected string name;
		protected string val;
		protected string text;
		protected string[] strArray;
		protected int[] intArray;
		protected AStruct myStruct;

		protected Type myType;

		public string Name
		{
			get {return name;}
			set {name=value;}
		}

		public string ReadOnlyValue
		{
			get {return val;}
		}

		public string ReadWriteValue
		{
			get {return val;}
			set {val=value;}
		}

		public string Text
		{
			get {return text;}
			set {text=value;}
		}

		public string[] StringArray
		{
			get {return strArray;}
			set {strArray=value;}
		}

		public int[] IntArray
		{
			get {return intArray;}
			set {intArray=value;}
		}

		public Type MyType
		{
			get {return myType;}
			set {myType=value;}
		}

		public AStruct MyStruct
		{
			get {return myStruct;}
			set {myStruct=value;}
		}

		public APropertyTestClass()
		{
		}
	}

	[TestFixture]
	public class PropertyAssignmentTests
	{
		protected Parser parser;

		[SetUp]
		public void SetUp()
		{
			parser=new Parser();
		}

		[Test]
		[ExpectedException(typeof(UnknownPropertyException))]
		public void MissingPropertyTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass NoProperty='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		[ExpectedException(typeof(ReadOnlyInstanceNullException))]
		public void ReadOnlyPropertyTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass ReadOnlyValue='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		public void ReadWritePropertyTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass ReadWriteValue='Foo'/>\r\n";
			xml+="</MyXaml>\r\n";
			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.ReadWriteValue=="Foo", "Unexpected result.");
		}

		[Test]
		public void ReferenceTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass ReadWriteValue='{SomeVal}'/>\r\n";
			xml+="</MyXaml>\r\n";

			parser.AddReference("SomeVal", "Marc");
			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.ReadWriteValue=="Marc", "Unexpected result.");
		}

		[Test]
		public void InnerTextTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass>Marc</APropertyTestClass>\r\n";
			xml+="</MyXaml>\r\n";

			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.Text=="Marc", "Unexpected result.");
		}

		[Test]
		[ExpectedException(typeof(ForwardReferenceException))]
		public void EmbeddedReferenceFailureTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass Text='M{text}c'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		public void EmbeddedReferenceTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass Text='M{text}c'/>\r\n";
			xml+="</MyXaml>\r\n";

			parser.AddReference("text", "ar");
			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.Text=="Marc", "Unexpected result.");
		}

		[Test]
		public void ArrayTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass StringArray='a, b, c'/>\r\n";
			xml+="</MyXaml>\r\n";
			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.StringArray.Length==3, "Unexpected result.");
			Assertion.Assert(tc.StringArray[0]=="a", "Unexpected result.");
			Assertion.Assert(tc.StringArray[1]=="b", "Unexpected result.");
			Assertion.Assert(tc.StringArray[2]=="c", "Unexpected result.");
		}

		[Test]
		[ExpectedException(typeof(ArrayConversionException))]
		public void ArrayAssignmentFailureTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass IntArray='a, b, c'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		public void TypeAssignmentTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass MyType='System.Int32'/>\r\n";
			xml+="</MyXaml>\r\n";
			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.MyType==typeof(System.Int32), "Unexpected result.");
		}

		[Test]
		[ExpectedException(typeof(TypeAssignmentException))]
		public void TypeAssignmentFailureTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass MyType='NoType'/>\r\n";
			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		public void LateBindingAssignment()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";
			xml+="  <APropertyTestClass ReadWriteValue='{lateBound.Text}'>\r\n";
			xml+="    <APropertyTestClass def:Name='lateBound' Text='Marc'/>\r\n";
			xml+="  </APropertyTestClass>\r\n";
			xml+="</MyXaml>\r\n";
			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.ReadWriteValue=="Marc", "Unexpected result.");
		}

		[Test]
		public void ReferencePropertyAssignment()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <APropertyTestClass ReadWriteValue='{dynamic;Text=Marc}'>\r\n";
			xml+="    <DynamicClass def:Name='dynamic'/>\r\n";
			xml+="  </APropertyTestClass>\r\n";

			xml+="</MyXaml>\r\n";
			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.ReadWriteValue=="Marc", "Unexpected result.");
		}

		[Test]
		public void ExtenderProviderTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <APropertyTestClass ExtendedProperty='{provider; SetText=Marc}'>\r\n";
			xml+="    <Provider def:Name='provider'/>\r\n";
			xml+="  </APropertyTestClass>\r\n";

			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
			Provider p=(Provider)parser.GetReference("provider");
			Assertion.Assert(p.Object is APropertyTestClass, "Object is unexpected type.");
			Assertion.Assert(p.Text=="Marc", "Text is unexpected value.");
		}

		[Test]
		[ExpectedException(typeof(ExtenderProviderException))]
		public void ExtenderProviderFailTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <DynamicClass ExtendedProperty='{provider; SetText=Marc}'>\r\n";
			xml+="    <Provider def:Name='provider'/>\r\n";
			xml+="  </DynamicClass>\r\n";

			xml+="</MyXaml>\r\n";
			parser.InstantiateFromString(xml, "*");
			Provider p=(Provider)parser.GetReference("provider");
			Assertion.Assert(p.Object is APropertyTestClass, "Object is unexpected type.");
			Assertion.Assert(p.Text=="Marc", "Text is unexpected value.");
		}

		[Test]
		public void StructTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <APropertyTestClass>\r\n";
			xml+="    <MyStruct>\r\n";
			xml+="      <AStruct Num='1' Str='Foo'/>\r\n";
			xml+="    </MyStruct>\r\n";
			xml+="  </APropertyTestClass>\r\n";

			xml+="</MyXaml>\r\n";
			APropertyTestClass tc=(APropertyTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.MyStruct.Num==1, "a not assigned.");
			Assertion.Assert(tc.MyStruct.Str=="Foo", "b not assigned.");
		}
	}
}

