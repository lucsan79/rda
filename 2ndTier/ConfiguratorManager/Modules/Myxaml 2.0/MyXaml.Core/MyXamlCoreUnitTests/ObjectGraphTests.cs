using System;
using System.ComponentModel;
using System.Xml;

using Vts.UnitTest;

using MyXaml.Core;
using MyXaml.Core.Exceptions;

namespace MyXamlCoreUnitTests
{
	public class Item
	{
		protected string text;

		public string Text
		{
			get {return text;}
			set {text=value;}
		}

		public Item()
		{
		}
	}

	public class Item2
	{
		protected string text;

		public string Text
		{
			get {return text;}
			set {text=value;}
		}
	}

	public class ChildTest
	{
	}

	public class ObjectGraphTestClass
	{
		protected string name;
		protected Item[] items;
		protected ChildTest childTest;

		public string Name
		{
			get {return name;}
			set {name=value;}
		}

		public Item[] Items
		{
			get {return items;}
			set {items=value;}
		}

		public ChildTest MyChild
		{
			get {return childTest;}
			set {childTest=value;}
		}
	}

	[TestFixture]
	public class ObjectGraphTests
	{
		protected Parser parser;

		[SetUp]
		public void SetUp()
		{
			parser=new Parser();
		}

		[Test]
		public void PropertyArrayInitializationTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <ObjectGraphTestClass>\r\n";
			xml+="    <Items>\r\n";
			xml+="      <Item Text='a'/>\r\n";
			xml+="      <Item Text='b'/>\r\n";
			xml+="      <Item Text='c'/>\r\n";
			xml+="    </Items>\r\n";
			xml+="  </ObjectGraphTestClass>\r\n";

			xml+="</MyXaml>\r\n";

			ObjectGraphTestClass tc=(ObjectGraphTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.Items[0].Text=="a", "Unexpected result.");
			Assertion.Assert(tc.Items[1].Text=="b", "Unexpected result.");
			Assertion.Assert(tc.Items[2].Text=="c", "Unexpected result.");
		}

		[Test]
		[ExpectedException(typeof(ArrayConversionException))]
		public void PropertyArrayInitializationFailureTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <ObjectGraphTestClass>\r\n";
			xml+="    <Items>\r\n";
			xml+="      <Item2 Text='a'/>\r\n";
			xml+="      <Item2 Text='b'/>\r\n";
			xml+="      <Item2 Text='c'/>\r\n";
			xml+="    </Items>\r\n";
			xml+="  </ObjectGraphTestClass>\r\n";

			xml+="</MyXaml>\r\n";

			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		public void ClassPropertyClassTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <ObjectGraphTestClass>\r\n";
			xml+="    <MyChild>\r\n";
			xml+="      <ChildTest/>\r\n";
			xml+="    </MyChild>\r\n";
			xml+="  </ObjectGraphTestClass>\r\n";

			xml+="</MyXaml>\r\n";

			ObjectGraphTestClass tc=(ObjectGraphTestClass)parser.InstantiateFromString(xml, "*");
			Assertion.Assert(tc.MyChild != null, "Unexpected result.");
		}

		[Test]
		[ExpectedException(typeof(ChildTypeNotPropertyTypeException))]
		public void ClassPropertyNotKindOfTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <ObjectGraphTestClass>\r\n";
			xml+="    <MyChild>\r\n";
			xml+="      <Item/>\r\n";
			xml+="    </MyChild>\r\n";
			xml+="  </ObjectGraphTestClass>\r\n";

			xml+="</MyXaml>\r\n";

			parser.InstantiateFromString(xml, "*");
		}

		[Test]
		[ExpectedException(typeof(ExpectedSingleChildException))]
		public void MultipleChildrenForAPropertyTest()
		{
			string xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			xml+="<!-- (c) 2005 MyXaml  All Rights Reserved  -->\r\n";
			xml+="<MyXaml\r\n";
			xml+="  xmlns:def=\"Definition\"\r\n";
			xml+="  xmlns:ref=\"Reference\"\r\n";
			xml+="  xmlns=\"MyXamlCoreUnitTests\">\r\n";

			xml+="  <ObjectGraphTestClass>\r\n";
			xml+="    <MyChild>\r\n";
			xml+="      <ChildTest/>\r\n";
			xml+="      <ChildTest/>\r\n";
			xml+="    </MyChild>\r\n";
			xml+="  </ObjectGraphTestClass>\r\n";

			xml+="</MyXaml>\r\n";

			parser.InstantiateFromString(xml, "*");
		}

		// ICollection 
		// IList

		// Abstract property
		// Interface implemented by child

	}
}

