/*
 * Copyright (c) 2004, 2005 MyXaml
 * All Rights Reserved
 * 
 * Licensed under the terms of the GNU General Public License
 * http://www.gnu.org/licenses/licenses.html#GPL
*/

using System;
using System.Windows.Forms;

using MyXaml.Core;

namespace MyXaml.WinForms
{
	public class WinFormExtender : IMyXamlExtender
	{
		public WinFormExtender()
		{
		}

		public void Register(Parser parser)
		{
			parser.InstantiateBegin+=new MyXaml.Core.Parser.InstantiateDlgt(OnInstantiateBegin);
			parser.InstantiateEnd+=new MyXaml.Core.Parser.InstantiateDlgt(OnInstantiateEnd);
		}

		public void Unregister(Parser parser)
		{
			parser.InstantiateBegin-=new MyXaml.Core.Parser.InstantiateDlgt(OnInstantiateBegin);
			parser.InstantiateEnd-=new MyXaml.Core.Parser.InstantiateDlgt(OnInstantiateEnd);
		}

		private void OnInstantiateBegin(object parser, InstantiateEventArgs e)
		{
			if (e.Instance is Control)
			{
				((Control)e.Instance).SuspendLayout();
			}
			
			if (e.Instance is Form)
			{
//				PersistWindowState windowState=new PersistWindowState();
//				windowState.Form=((Form)e.Instance);
			}
		}

		private void OnInstantiateEnd(object parser, InstantiateEventArgs e)
		{
			if (e.Instance is Control)
			{
				((Control)e.Instance).ResumeLayout();
			}
		}
	}
}
