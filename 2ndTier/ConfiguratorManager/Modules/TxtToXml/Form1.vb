﻿Imports System.Xml

Public Class Form1

    Dim myPath As String = "C:\Users\Luca\Desktop\configuratore\Utility\confimg\doc.txt"
    Dim myOut As String = "C:\Users\Luca\Desktop\configuratore\Utility\confimg\doc.xml"
    Dim myOut2 As String = "C:\Users\Luca\Desktop\configuratore\Utility\confimg\Famiglia SPC - Presystem SPC.xml"
    Dim myOut3 As String = "C:\Users\Luca\Desktop\configuratore\Utility\confimg\doc-EXT.txt"
    Dim myLists As New Generic.Dictionary(Of Integer, XmlNode)


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click

        'Dim myDataSet As New DataSet
        'myDataSet.ReadXml(myOut2)

        Dim myXMLDoc As New Xml.XmlDocument
        Dim DocRoot As XmlElement
        DocRoot = myXMLDoc.CreateElement("Dialogs")
        myXMLDoc.AppendChild(DocRoot)

        Dim myExt As New IO.StreamWriter(myOut3)
        Dim myReader As New IO.StreamReader(myPath)
        Dim elem As XmlNode = Nothing
        Dim elemCondition As XmlNode = Nothing
        Dim elemFeatures As XmlNode = Nothing
        While (Not myReader.EndOfStream)

            Dim newAtt As XmlAttribute
            Dim myLine As String = myReader.ReadLine
            Dim id As String
            Dim title As String
            Dim lastCreate As String

            myLine = myLine.Trim()
            If myLine.Trim = "" Then
                Continue While

            End If
            If myLine.StartsWith("PC") Then
                elem = Nothing
                elemCondition = Nothing
                elemFeatures = Nothing
                id = myLine.Split("-")(0).Trim
                title = myLine.Replace(id & " - ", "").Trim
                If id = "PC49" Then
                    ' Exit While
                End If

                If myLists.Count = 0 Or id.Length = 4 Then
                    lastCreate = "Answers"
                Else
                    For idx As Integer = id.Length - 1 To 0 Step -1

                        If myLists.ContainsKey(idx) Then
                            If id.Contains(myLists(idx).Attributes("id".ToUpper).Value) Then
                                lastCreate = myLists(idx).Name
                                Exit For
                            End If

                        End If
                    Next
                End If
                If Not myLists.ContainsKey(id.Length) Then
                    myLists.Add(id.Length, Nothing)
                End If
                If lastCreate = "Question" Then
                    elem = myXMLDoc.CreateElement("Answers")
                Else
                    elem = myXMLDoc.CreateElement("Question")
                End If
                elemFeatures = myXMLDoc.CreateElement("Features")
                elemCondition = myXMLDoc.CreateElement("Conditions")

                newAtt = myXMLDoc.CreateAttribute("id".ToUpper)
                newAtt.Value = id
                elem.Attributes.Append(newAtt)

                newAtt = myXMLDoc.CreateAttribute("title".ToUpper)
                newAtt.Value = title
                elem.Attributes.Append(newAtt)
                Dim index As Integer = 0

                For idx As Integer = id.Length - 1 To 0 Step -1
                    If myLists.ContainsKey(idx) Then
                        If id.Contains(myLists(idx).Attributes("id".ToUpper).Value) Then
                            myLists(idx).AppendChild(elem)
                            Exit For
                        End If

                    End If
                Next
                elem.AppendChild(elemFeatures)
                elem.AppendChild(elemCondition)
                If id.Length = 4 Then
                    DocRoot.AppendChild(elem)
                End If
                myLists(id.Length) = elem
            Else
                Dim myArray() As String = myLine.Split(vbTab.ToCharArray, StringSplitOptions.RemoveEmptyEntries)
                'If myArray.Length = 0 Then
                '    myExt.WriteLine(myLine)
                'Else
                '    If myArray(0).Contains("|") Then
                '        If myArray(0).Split("|".ToCharArray, StringSplitOptions.RemoveEmptyEntries).Length > 1 Then
                '            myExt.WriteLine(myLine)
                '        End If
                '    End If
                'End If
                If myLine.Trim.Contains("CP|") Then
                    myExt.WriteLine(myLine)

                    If elemFeatures IsNot Nothing Then
                        Dim tmpNode As XmlNode = myXMLDoc.CreateElement("Feature")
                        Dim tmpAttr As XmlAttribute = myXMLDoc.CreateAttribute("id".ToUpper)
                        Dim idRef As Integer = 1
                        'tmpAttr.Value = elem.Attributes("id").Value & "." & myArray(1)
                        'tmpNode.Attributes.Append(tmpAttr)


                        If idRef > 0 Then

                            tmpAttr = myXMLDoc.CreateAttribute("ftype".ToUpper)
                            tmpAttr.Value = "CP"
                            tmpNode.Attributes.Append(tmpAttr)


                            tmpAttr = myXMLDoc.CreateAttribute("Code".ToUpper)
                            tmpAttr.Value = myArray(idRef)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("UM".ToUpper)
                            tmpAttr.Value = myArray(idRef + 1)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("value".ToUpper)
                            tmpAttr.Value = myArray(idRef + 2)
                            tmpNode.Attributes.Append(tmpAttr)

                            'tmpAttr = myXMLDoc.CreateAttribute("isMandatory")
                            ' tmpAttr.Value = "true"
                            'tmpNode.Attributes.Append(tmpAttr)

                            elemFeatures.AppendChild(tmpNode)
                        End If


                    End If

                ElseIf myLine.Trim.Contains("CA|") Then

                    myExt.WriteLine(myLine)

                    If elemFeatures IsNot Nothing Then
                        Dim tmpNode As XmlNode = myXMLDoc.CreateElement("Feature")
                        Dim tmpAttr As XmlAttribute = myXMLDoc.CreateAttribute("id".ToUpper)
                        Dim idRef As Integer = 1
                        tmpAttr.Value = elem.Attributes("id".ToUpper).Value & "." & myArray(1)
                        'tmpNode.Attributes.Append(tmpAttr)


                        If idRef > 0 Then

                            tmpAttr = myXMLDoc.CreateAttribute("Ftype".ToUpper)
                            tmpAttr.Value = "CA"
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("Code".ToUpper)
                            tmpAttr.Value = myArray(idRef)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("UM".ToUpper)
                            tmpAttr.Value = myArray(idRef + 1)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("minVal".ToUpper)
                            tmpAttr.Value = myArray(idRef + 2)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("maxVal".ToUpper)
                            tmpAttr.Value = myArray(idRef + 3)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("type".ToUpper)
                            tmpAttr.Value = myArray(idRef + 4).Trim
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("isMandatory".ToUpper)
                            tmpAttr.Value = "true"
                            tmpNode.Attributes.Append(tmpAttr)

                            elemFeatures.AppendChild(tmpNode)
                        End If


                    End If

                ElseIf myLine.Trim.Contains("TE|") Then
                    myExt.WriteLine(myLine)

                    If elemFeatures IsNot Nothing Then
                        Dim tmpNode As XmlNode = myXMLDoc.CreateElement("Feature")
                        Dim tmpAttr As XmlAttribute = myXMLDoc.CreateAttribute("id".ToUpper)
                        Dim idRef As Integer = 1
                        tmpAttr.Value = elem.Attributes("id".ToUpper).Value & "." & myArray(1)
                        'tmpNode.Attributes.Append(tmpAttr)


                        If idRef > 0 Then

                            tmpAttr = myXMLDoc.CreateAttribute("Ftype".ToUpper)
                            tmpAttr.Value = "TE"
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("Code".ToUpper)
                            tmpAttr.Value = myArray(idRef)
                            tmpNode.Attributes.Append(tmpAttr)

                            
                            elemFeatures.AppendChild(tmpNode)
                        End If


                    End If

                ElseIf myLine.Trim.Contains("AMM|") Then
                    'If myArray.Length > 6 Then
                    '    myExt.WriteLine(myLine)
                    'End If
                    If elemCondition IsNot Nothing Then
                        Dim tmpNode As XmlNode = myXMLDoc.CreateElement("condition")
                        Dim tmpAttr As XmlAttribute = myXMLDoc.CreateAttribute("id".ToUpper)
                        Dim idRef As Integer = 2
                        tmpAttr.Value = elem.Attributes("id".ToUpper).Value & "." & myArray(1)
                        tmpNode.Attributes.Append(tmpAttr)

                        If myArray(2) <> "CA" And myArray(2) <> "ED" Then
                            idRef = 3
                            If myArray(3) <> "CA" And myArray(3) <> "ED" Then
                                idRef = -1
                            End If
                        End If
                        If idRef > 0 Then

                            tmpAttr = myXMLDoc.CreateAttribute("type".ToUpper)
                            tmpAttr.Value = myArray(idRef)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("refOption".ToUpper)
                            tmpAttr.Value = myArray(idRef + 1)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("refOptionRelation".ToUpper)
                            tmpAttr.Value = myArray(idRef + 2)
                            tmpNode.Attributes.Append(tmpAttr)

                            tmpAttr = myXMLDoc.CreateAttribute("refOperator".ToUpper)
                            If myArray(myArray.Length - 1) <> "AND" And myArray(myArray.Length - 1) <> "OR" Then
                                Throw New Exception("alert")
                            End If
                            tmpAttr.Value = myArray(myArray.Length - 1)
                            tmpNode.Attributes.Append(tmpAttr)

                            elemCondition.AppendChild(tmpNode)
                        End If


                    End If
                    Dim myline2 As String = ""

                ElseIf myLine.Trim.Contains("TE|") Then
                    Dim myline2 As String = ""
                End If

            End If


        End While
        myExt.Close()
        myReader.Close()
        'DocRoot.AppendChild(myLists(4))
        myXMLDoc.Save(myOut)
        MsgBox("FINITO")


    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click

        Dim myXML As String = "C:\Users\Luca\Desktop\configuratore\v1.5\WEB\Dialogs\doc.xml"
        Dim myXML2 As String = "C:\Users\Luca\Desktop\configuratore\v1.5\WEB\Dialogs\doc_out.xml"

        Dim myInput As New IO.StreamReader(myXML)
        Dim myOutput As New IO.StreamWriter(myXML2)
        Dim CurrentId As String = "DG0000000000"

        While (Not myInput.EndOfStream)
            Dim myLine As String = myInput.ReadLine
            Dim id As String = ""
            Dim id2 As String = ""
            For Each myKey As String In myLine.Split(" ")
                If myKey.StartsWith("ID=") Then
                    id = myKey
                    Exit For
                End If
            Next
            If id <> "" Then
                id2 = id.Replace("ID", "CODE")
                id2 = id2 & " ID=""" & CurrentId & """"
                myLine = myLine.Replace(id, id2)


                Dim numstr As String = CurrentId.Substring(2)
                Dim incnum As Integer = Convert.ToInt32(numstr)
                incnum += 1
                Dim newstr As String = incnum.ToString()
                CurrentId = "DG" & newstr.PadLeft(10, "0"c)


            End If
            myOutput.WriteLine(myLine)

        End While
        myInput.Close()
        myOutput.Close()
        ' Dim retValue As String = ""
        'Dim selectedId As String = "-1"
        'Dim parentId As String = "-1"
        'Dim myDialog As Parallaksis.eplms.Configurator.Dialogs
        ''Dim myPath As String = "C:\Users\Luca\Desktop\configuratore\WEB\Dialogs\Famiglia SPC - Presystem SPC.xml"
        'Dim myPath As String = "C:\Users\Luca\Desktop\configuratore\WEB\Dialogs\doc.xml"
        'Dim myDataProvider As Parallaksis.eplms.Configurator.DataProvider
        'Dim mytable As Data.DataTable


        'myDataProvider = New Parallaksis.eplms.Configurator.DataProvider
        'myDataProvider.InitConfiguratorDataByXML(mytable, myPath)

    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Dim myTable As New Data.DataTable("MOTORI-CODICE")
        myTable.Columns.Add("id")
        myTable.Columns.Add("Descrizione")
        myTable.Columns.Add("Peso")
        myTable.Columns.Add("CODICE")

        Dim myRow As DataRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "SOMFY ATP 15/12"
        myRow(2) = "24"
        myRow(3) = "MOT1102"
        myTable.Rows.Add(myRow)


        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "SOMFY ATP 20/12"
        myRow(2) = "30"
        myRow(3) = "MOT1103"
        myTable.Rows.Add(myRow)

        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "SOMFY ATP 30/12"
        myRow(2) = "45"
        myRow(3) = "MOT1104"
        myTable.Rows.Add(myRow)

        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "SOMFY ATP 40/12"
        myRow(2) = "60"
        myRow(3) = "MOT1105"
        myTable.Rows.Add(myRow)

        myTable.WriteXml("C:\Users\Luca\Desktop\configuratore\v1.5\WEB\Dialogs\externalTable\MOTORI.xml")



        myTable = New Data.DataTable("COLORIAV-CODICE")
        myTable.Columns.Add("id")
        myTable.Columns.Add("Descrizione")
        myTable.Columns.Add("CODICE")

        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "GIALLO"
        myRow(2) = "G1"
        myTable.Rows.Add(myRow)


        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "BLUE"
        myRow(2) = "B2"
        myTable.Rows.Add(myRow)

        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "BLACK"
        myRow(2) = "B3"
        myTable.Rows.Add(myRow)

       
        myTable.WriteXml("C:\Users\Luca\Desktop\configuratore\v1.5\WEB\Dialogs\externalTable\COLORIAV.xml")

        myTable = New Data.DataTable("TE_COLORI_ESTR-CODICE")
        myTable.Columns.Add("id")
        myTable.Columns.Add("Descrizione")
        myTable.Columns.Add("CODICE")

        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "GIALLO"
        myRow(2) = "G1"
        myTable.Rows.Add(myRow)


        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "BLUE"
        myRow(2) = "B2"
        myTable.Rows.Add(myRow)

        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "BLACK"
        myRow(2) = "B3"
        myTable.Rows.Add(myRow)

        myTable.WriteXml("C:\Users\Luca\Desktop\configuratore\v1.5\WEB\Dialogs\externalTable\COLORI_ESTR.xml")


        myTable = New Data.DataTable("RAL-CODICE")
        myTable.Columns.Add("id")
        myTable.Columns.Add("Descrizione")
        myTable.Columns.Add("CODICE")

        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "GIALLO"
        myRow(2) = "G1"
        myTable.Rows.Add(myRow)


        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "BLUE"
        myRow(2) = "B2"
        myTable.Rows.Add(myRow)

        myRow = myTable.NewRow
        myRow(0) = myTable.Rows.Count
        myRow(1) = "BLACK"
        myRow(2) = "B3"
        myTable.Rows.Add(myRow)

        myTable.WriteXml("C:\Users\Luca\Desktop\configuratore\v1.5\WEB\Dialogs\externalTable\RAL.xml")
    End Sub
End Class
