using System;
using System.Windows.Forms;

namespace Clifton.Windows.Forms.XmlTree
{
	public interface IXtreeNode
	{
		string Name {get; set;}
        string ID { get; set; }
		IXtreeNode Parent {get; set;}
		int IconIndex { get;}
		int SelectedIconIndex { get;}

		bool AddNode(IXtreeNode parentInstance, string tag,string givenId);
		bool DeleteNode(IXtreeNode parentInstance);
		void AutoDeleteNode(IXtreeNode parentInstance);
		void Select(TreeNode tn);
		bool IsEnabled(string tag, bool defaultState);
		void MoveTo(IXtreeNode newParent, IXtreeNode oldParent, int idx);
		int Index(object obj);
	}
}
