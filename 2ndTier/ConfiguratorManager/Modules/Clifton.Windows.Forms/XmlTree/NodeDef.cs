using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace Clifton.Windows.Forms.XmlTree
{
	public class NodeDef : ISupportInitialize
	{
		protected string name;
		protected string refName;
		protected bool isReadOnly;
		protected string text;
		protected bool isRequired;
		protected string iconFilename;
		private string typeName;
		private Type type;
		protected ImageList imgList;
		protected int imgOffset;
		
		protected List<Popup> parentPopupItems;
		protected List<Popup> popupItems;
		protected List<NodeDef> nodes;
		protected NodeDef parent;
		protected bool recurse;
        
		
		/// <summary>
		/// Gets/sets recurse flag.
		/// </summary>
		public bool Recurse
		{
			get { return recurse; }
			set { recurse = value; }
		}
		
		/// <summary>
		/// Gets/sets parent
		/// </summary>
		public NodeDef Parent
		{
			get { return parent; }
			set { parent = value; }
		}

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public string RefName
		{
			get { return refName; }
			set { refName = value; }
		}

		public bool IsReadOnly
		{
			get { return isReadOnly; }
			set { isReadOnly = value; }
		}

		public string Text
		{
			get { return text; }
			set { text = value; }
		}

		public bool IsRequired
		{
			get { return isRequired; }
			set { isRequired = value; }
		}

		public List<Popup> ParentPopupItems
		{
			get { return parentPopupItems; }
		}

		public List<Popup> PopupItems
		{
			get { return popupItems; }
		}

		public List<NodeDef> Nodes
		{
			get
			{
				if ((parent != null) && (recurse))
				{
					return parent.Nodes;
				}
				else
				{
					return nodes;
				}
			}
		}

		public bool IsRef
		{
			get { return refName != null; }
		}

		public string IconFilename
		{
			get { return iconFilename; }
			set { iconFilename = value; }
		}

		/// <summary>
		/// Gets/sets typeName
		/// </summary>
		public string TypeName
		{
			get { return typeName; }
			set { typeName = value; }
		}

		/// <summary>
		/// Gets/sets type
		/// </summary>
		public Type ImplementingType
		{
			get { return type; }
			set { type = value; }
		}

		public ImageList ImageList
		{
			get { return imgList; }
		}

		public int ImageOffset
		{
			get { return imgOffset; }
			set { imgOffset = value; }
		}

		public NodeDef()
		{
			parentPopupItems = new List<Popup>();
			popupItems = new List<Popup>();
			nodes = new List<NodeDef>();
			imgOffset = -1;
		}

		public virtual void BeginInit()
		{
		}

		public virtual void EndInit()
		{
			imgList = new ImageList();
			type = typeof(PlaceholderInstance);

			if (iconFilename != null)
			{
				string[] icons = iconFilename.Split(',');

				foreach (string ifn in icons)
				{
					string fn=ifn.Trim();
					//Icon icon = new Icon(fn);
                    Image icon = Bitmap.FromFile(fn);
					imgList.Images.Add(fn, icon);
				}
			}

			if (typeName != null)
			{
				type = Type.GetType(typeName);
			}

			foreach (NodeDef child in nodes)
			{
				child.Parent = this;
			}
		}

		public IXtreeNode CreateImplementingType(IXtreeNode parent,String givenFamiglia)
		{
			IXtreeNode inst = null;
			inst = (IXtreeNode)Activator.CreateInstance(ImplementingType);
			inst.Parent = parent;
            //if (parent == null)
            //    inst.ID = givenFamiglia;
            //else
            //{
            //    inst.ID = "";
            //}
			return inst;
            
		}


       
	}
}
