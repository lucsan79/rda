using System;
using System.Windows.Forms;

namespace Clifton.Windows.Forms.XmlTree
{
    public class NodeMenuItem : ToolStripMenuItem
	{
		protected TreeNode tn;
		protected NodeDef parentNode;
		protected NodeDef childNode;
		protected Popup popup;
        protected System.Drawing.Image image;

		public TreeNode TreeNode
		{
			get { return tn; }
		}

		public NodeDef ParentNode
		{
			get { return parentNode; }
		}

		public NodeDef ChildNode
		{
			get { return childNode; }
		}

		public Popup PopupInfo
		{
			get { return popup; }
		}

        public System.Drawing.Image Image
        {
            get { return image; }
            set { image = value; }
        }

		public NodeMenuItem(string text, TreeNode tn, NodeDef parentNode, NodeDef childNode, Popup popup)
			: base(text)
		{
			this.tn = tn;
			this.parentNode = parentNode;
			this.childNode = childNode;
			this.popup = popup;
            
		}
	}
}
