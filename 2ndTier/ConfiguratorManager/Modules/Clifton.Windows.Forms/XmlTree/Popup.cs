using System;

namespace Clifton.Windows.Forms.XmlTree
{
	public class Popup
	{
		protected string text;
		protected bool isAdd;
		protected bool isRemove;
        protected bool isExpand;
        protected bool isCollapse;
		protected bool enabled;
		protected string tag;

		public string Text
		{
			get { return text; }
			set { text = value; }
		}

		public bool IsAdd
		{
			get { return isAdd; }
			set { isAdd = value; }
		}

		public bool IsRemove
		{
			get { return isRemove; }
			set { isRemove = value; }
		}
        public bool IsExpand
        {
            get { return isExpand; }
            set { isExpand = value; }
        }
        public bool IsCollapse
        {
            get { return isCollapse; }
            set { isCollapse = value; }
        }

		public bool Enabled
		{
			get { return enabled; }
			set { enabled = value; }
		}

		public string Tag
		{
			get { return tag; }
			set { tag = value; }
		}

		public Popup()
		{
			enabled = true;
		}
	}
}
