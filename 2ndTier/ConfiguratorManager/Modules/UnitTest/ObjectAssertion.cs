/*
(c) 2005, Marc Clifton
All Rights Reserved
 
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. 

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution. 

Neither the name of the Marc Clifton, "Advanced Unit Test", "AUT", nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Reflection;

namespace Vts.UnitTest
{
	/// <summary>
	/// Provides functionality to assert the state of objects
	/// </summary>
	public abstract class ObjectAssertion
	{
		#region Methods

		#region Compare

		/// <summary>
		/// Compares all public value properties of two objects
		/// </summary>
		/// <param name="expected">object with expected values</param>
		/// <param name="actual">object with actual values</param>
		public static void Compare(object expected, object actual) 
		{
			Compare(expected, actual, 1, false, "");
		}

		/// <summary>
		/// Compares all public value properties of two objects
		/// </summary>
		/// <param name="expected">object with expected values</param>
		/// <param name="actual">object with actual values</param>
		/// <param name="message">a message in case the comparison fails</param>
		public static void Compare(object expected, object actual, string message) 
		{
			Compare(expected, actual, 1, false, message);
		}

		/// <summary>
		/// Compares all public value properties of two objects down to the specified level
		/// </summary>
		/// <param name="expected">object with expected values</param>
		/// <param name="actual">object with actual values</param>
		/// <param name="level">specifies how many recursions on child objects should be processed</param>
		/// <param name="message">a message in case the comparison fails</param>
		public static void Compare(object expected, object actual, int level, string message) 
		{
			Compare(expected, actual, level, false, message);
		}

		/// <summary>
		/// Compares all public properties of two objects up to the specified level
		/// </summary>
		/// <param name="expected">object with expected values</param>
		/// <param name="actual">object with actual values</param>
		/// <param name="level">specifies how many recursions on child objects should be processed</param>
		/// <param name="compareReferences">if <i>true</i> object references have to be equal as well</param>
		/// <param name="message">a message in case the comparison fails</param>
		public static void Compare(object expected, object actual, int level, bool compareReferences, string message) 
		{
			if(expected.GetType() != actual.GetType()) throw new AssertionException("Objects not of same type");

			PropertyInfo[] properties = expected.GetType().GetProperties();
			foreach(PropertyInfo property in properties) 
			{
				object expectedValue = property.GetValue(expected, null);
				object actualValue = property.GetValue(actual, null);

				if(expectedValue == null && actualValue == null) continue;
				
				if(expectedValue is ValueType || expectedValue is string || compareReferences) 
				{
					string msg = string.Format("{3}\nCompare failed at {0}: expected value = {1}, actual value = {2}",
						property.Name, expectedValue, actualValue, message);

					if(expectedValue == null) Assertion.Fail(msg);
					if(actualValue == null) Assertion.Fail(msg);
					if(!expectedValue.Equals(actualValue)) Assertion.Fail(msg);
				} 
				else 
				{
					if(level > 1)
						ObjectAssertion.Compare(expectedValue, actualValue, --level, compareReferences, message);
				}
			}
		}

		#endregion

		#endregion

		#region Fields


		#endregion
	}
}
