/*
(c) 2005, Marc Clifton
All Rights Reserved
 
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. 

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution. 

Neither the name of the Marc Clifton, "Advanced Unit Test", "AUT", nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;

namespace Vts.UnitTest
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class TestFixtureAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class ProcessTestAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class TestFixtureSetUpAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class TestFixtureTearDownAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class TestAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class MinOperationsPerSecondAttribute : Attribute
	{
		private int minOpsPerSecond;

		public int MinOPS
		{
			get {return minOpsPerSecond;}
		}

		public MinOperationsPerSecondAttribute(int n)
		{
			minOpsPerSecond=n;
		}
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class RepeatAttribute : Attribute
	{
		private int repCount;
		private int delayBetweenReps;	//ms

		public int RepeatCount
		{
			get {return repCount;}
		}

		public int RepeatDelay
		{
			get {return delayBetweenReps;}
		}

		public RepeatAttribute(int n)
		{
			repCount=n;
			delayBetweenReps=-1;
		}

		public RepeatAttribute(int n, int d)
		{
			repCount=n;
			delayBetweenReps=d;
		}
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class MaxKMemoryAttribute : Attribute
	{
		private int maxK;

		public int MaxK
		{
			get {return maxK;}
		}

		public MaxKMemoryAttribute(int n)
		{
			maxK=n;
		}
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class SequenceAttribute : Attribute
	{
		private int order;

		public int Order
		{
			get {return order;}
		}

		public SequenceAttribute(int i)
		{
			order=i;
		}
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=true, Inherited=true)]
//	[Serializable]
	public sealed class RequiresAttribute : Attribute
	{
		private string priorTestMethod;

		public string PriorTestMethod
		{
			get {return priorTestMethod;}
		}

		public RequiresAttribute(string methodName)
		{
			priorTestMethod=methodName;
		}
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class SetUpAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class TearDownAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class ExpectedExceptionAttribute : Attribute
	{
		private Type expectedException;
		private string expectedMessage;

		public Type ExceptionType 
		{
			get {return expectedException;}
		}

		public string Message 
		{
			get {return expectedMessage;}
		}

		public ExpectedExceptionAttribute(Type exception, string message)
		{
			expectedException=exception;
			expectedMessage = message;
		}

		public ExpectedExceptionAttribute(Type exception) : this(exception, null)
		{}
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple=true, Inherited=true)]
//	[Serializable]
	public sealed class ReverseProcessExpectedExceptionAttribute : Attribute
	{
		private Type expectedException;

		public Type ExceptionType
		{
			get {return expectedException;}
		}

		public ReverseProcessExpectedExceptionAttribute(Type exception)
		{
			expectedException=exception;
		}
	}

	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple=false, Inherited=true)]
//	[Serializable]
	public sealed class IgnoreAttribute : Attribute
	{
		private string reason;

		public string Reason
		{
			get {return reason;}
		}

		public IgnoreAttribute()
		{
			this.reason = "";
		}

		public IgnoreAttribute(string reason)
		{
			this.reason=reason;
		}
	}

	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple=false, Inherited=true)]
	public sealed class CodePathRangeAttribute : Attribute
	{
		private int start;
		private int stop;

		public int Start
		{
			get {return start;}
		}

		public int Stop
		{
			get {return stop;}
		}

		public CodePathRangeAttribute(int start, int stop)
		{
			this.start=start;
			this.stop=stop;
		}
	}

	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple=false, Inherited=true)]
	public sealed class CodePathsAttribute : Attribute
	{
		private int[] paths;

		public int[] Paths
		{
			get {return paths;}
		}

		public CodePathsAttribute(int[] paths)
		{
			this.paths=paths;
		}
	}

	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple=false, Inherited=true)]
	public sealed class ShouldExecuteCodePathAttribute : Attribute
	{
		private int pathNum;

		public int PathNum
		{
			get {return pathNum;}
		}

		public ShouldExecuteCodePathAttribute(int pathNum)
		{
			this.pathNum=pathNum;
		}
	}
}
