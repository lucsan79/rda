/*
(c) 2005, Marc Clifton
All Rights Reserved
 
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. 

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution. 

Neither the name of the Marc Clifton, "Advanced Unit Test", "AUT", nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Diagnostics;

namespace Vts.UnitTest
{
	public class AssertionException : Exception
	{
		public AssertionException(string message) : base(message)
		{
		}
	}

	public class Assertion
	{
		/// <summary>
		/// Causes a test to fail
		/// </summary>
		public static void Fail() { Fail(""); }
		/// <summary>
		/// Causes a test to fail
		/// </summary>
		/// <param name="message">a message in case of failure</param>
		public static void Fail(string message) 
		{
			throw new AssertionException(message);
		}

		/// <summary>
		/// Calls IsTrue
		/// </summary>
		/// <param name="mustBeTrue"></param>
		/// <param name="message"></param>
		public static void Assert(bool mustBeTrue, string message)
		{
			Assertion.IsTrue(mustBeTrue, message);
		}

		/// <summary>
		/// Asserts that a given expression is true
		/// </summary>
		/// <param name="expression">expression to validate</param>
		public static void IsTrue(bool expression) { IsTrue(expression, ""); }
		/// <summary>
		/// Asserts that a given expression is true
		/// </summary>
		/// <param name="expression">expression to validate</param>
		/// <param name="message">a message in case of failure</param>
		public static void IsTrue(bool expression, string message) 
		{
			if (!expression)
			{
				Trace.Write(message);
				throw(new AssertionException(message));
			}
		}

		/// <summary>
		/// Asserts that a given expression is false
		/// </summary>
		/// <param name="expression">expression to validate</param>
		public static void IsFalse(bool expression) { IsFalse(expression, ""); }
		/// <summary>
		/// Asserts that a given expression is false
		/// </summary>
		/// <param name="expression">expression to validate</param>
		/// <param name="message">a message in case of failure</param>
		public static void IsFalse(bool expression, string message) 
		{
			IsTrue(!expression, message);
		}

		/// <summary>
		/// Asserts that a given object reference is null
		/// </summary>
		/// <param name="obj">object to validate</param>
		public static void IsNull(object obj) { IsNull(obj, ""); }
		/// <summary>
		/// Asserts that a given object reference is null
		/// </summary>
		/// <param name="obj">object to validate</param>
		/// <param name="message">a message in case of failure</param>
		public static void IsNull(object obj, string message) 
		{
			IsTrue(obj == null, message);
		}

		/// <summary>
		/// Asserts that a given object reference is not null
		/// </summary>
		/// <param name="obj">object to validate</param>
		public static void IsNotNull(object obj) { IsNotNull(obj, ""); }
		/// <summary>
		/// Asserts that a given object reference is not null
		/// </summary>
		/// <param name="obj">object to validate</param>
		/// <param name="message">a message in case of failure</param>
		public static void IsNotNull(object obj, string message) 
		{
			IsFalse(obj == null, message);
		}

		/// <summary>
		/// Asserts that two values are equal
		/// </summary>
		/// <param name="expected">expected value</param>
		/// <param name="actual">actual value</param>
		public static void AreEqual(object expected, object actual) { AreEqual(expected, actual, ""); }
		/// <summary>
		/// Asserts that two values are equal.
		/// </summary>
		/// <param name="expected">expected value</param>
		/// <param name="actual">actual value</param>
		/// <param name="message">a message in case of failure</param>
		public static void AreEqual(object expected, object actual, string message) 
		{
			IsTrue(expected.Equals(actual), message);
		}

		/// <summary>
		/// Asserts that two values are not equal.
		/// </summary>
		/// <param name="firstValue">first value</param>
		/// <param name="secondValue">second value</param>
		public static void AreNotEqual(object firstValue, object secondValue) { AreNotEqual(firstValue, secondValue, ""); }
		/// <summary>
		/// Asserts that two values are not equal.
		/// </summary>
		/// <param name="firstValue">first value</param>
		/// <param name="secondValue">second value</param>
		/// <param name="message">a message in case of failure</param>
		public static void AreNotEqual(object firstValue, object secondValue, string message) 
		{
			IsFalse(firstValue.Equals(secondValue), message);
		}
	}
}
