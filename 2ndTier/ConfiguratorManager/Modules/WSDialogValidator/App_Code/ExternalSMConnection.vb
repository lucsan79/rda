﻿Imports Microsoft.VisualBasic
Imports Parallaksis.ePLMS33
Imports Parallaksis.ePLMS33.Defs
Imports System.Net
Imports System.IO
Imports System.Xml


Public Class ExternalSMConnection


    Shared Function StartLogin(ByRef givenPerson As ePerson, ByVal givenUrl As String, ByVal givenUser As String, ByVal givenPass As String) As String
        ' Create a request for the URL. 
        ''' shttp://tceweb.alpac.it/ws/rest/login?login={0}&password={1}&language=it_IT

        Dim retValue As String = ""
        Dim myProxyAddress As String = "" & System.Configuration.ConfigurationSettings.AppSettings("WADL_PROXY")
        givenUrl = String.Format(givenUrl, givenUser, givenPass)
        Dim request As WebRequest = WebRequest.Create(givenUrl)
        ' If required by the server, set the credentials.
        request.Credentials = CredentialCache.DefaultCredentials
        request.Timeout = 3 * 60 * 1000
        If myProxyAddress <> "" Then
            Dim myProxy As New WebProxy()
            ' Obtain the Proxy Prperty of the  Default browser.  
            myProxy.Address = New Uri(myProxyAddress)
            request.Proxy = myProxy
        End If

        ' Get the response.
        Dim response As WebResponse = request.GetResponse()
        ' Display the status.
        ' Get the stream containing content returned by the server.
        Dim dataStream As Stream = response.GetResponseStream()
        ' Open the stream using a StreamReader for easy access.
        Dim reader As New StreamReader(dataStream)
        ' Read the content.

        Dim responseFromServer As String = reader.ReadToEnd()

        ' Clean up the streams and the response.
        reader.Close()
        response.Close()
        ' Display the content.

        '<utente>
        '   <data>2013-02-04T12:06:21.621+01:00</data>
        '   <esito>true</esito>
        '   <token>f18e308b372fc05c50d08528f632f579</token>
        '   <loginName>AGE</loginName>
        '   <password>AGEALPAC</password>
        '</utente>
        Dim xml As New XmlDocument
        xml.LoadXml(responseFromServer)


        Dim token As XmlNodeList = xml.GetElementsByTagName("token")
        Dim errorMessage As String = ""
        Dim operationFlag As Boolean = OperationSuccesfullExecuted(responseFromServer, errorMessage, "Login")

        If operationFlag Then

            Dim today As String = String.Format("{0}{1}{2}", Now.Year, Now.Month, Now.Day)
            givenPerson.URL = token(0).InnerText & "|" & today
            givenPerson.Modify()
            Return givenPerson.URL.Split("|")(0)
        End If

        Return ""

    End Function

    Public Shared Function RemoveProdottoCommessa(ByVal givenToken As String, ByVal givenUrl As String, ByVal givenNRRG As String, ByRef givenMessage As String) As Boolean

        Try



            Dim request As WebRequest = WebRequest.Create(givenUrl)
            Dim myProxyAddress As String = "" & System.Configuration.ConfigurationSettings.AppSettings("WADL_PROXY")
            ' If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials
            request.Timeout = 3 * 60 * 1000
            If myProxyAddress <> "" Then
                Dim myProxy As New WebProxy()
                ' Obtain the Proxy Prperty of the  Default browser.  
                myProxy.Address = New Uri(myProxyAddress)
                request.Proxy = myProxy
            End If

            ' Get the response.
            Dim response As WebResponse = request.GetResponse()
            ' Display the status.
            ' Get the stream containing content returned by the server.
            Dim dataStream As Stream = response.GetResponseStream()
            ' Open the stream using a StreamReader for easy access.
            Dim reader As New StreamReader(dataStream)
            ' Read the content.

            Dim responseFromServer As String = reader.ReadToEnd()
            Dim myMessage As String = ""
            Dim operationFlag As Boolean = OperationSuccesfullExecuted(responseFromServer, myMessage, "Remove Product")
            Return operationFlag

            'If responseFromServer.ToLower.Contains("<esito>false</esito>".ToLower) Then
            '    myMessage = responseFromServer.Substring(responseFromServer.IndexOf("<messaggio>") + "</messaggio>".Length - 1)

            '    myMessage = myMessage.Substring(0, myMessage.IndexOf("</messaggio>"))

            '    myMessage = myMessage.Trim
            '    If myMessage <> "" And myMessage.ToLower <> "null" Then
            '        givenMessage = myMessage
            '        Return False
            '    Else
            '        Return True
            '    End If
            'Else
            '    Return True
            'End If

        Catch ex As Exception
            givenMessage = ex.Message
        End Try
        Return False




    End Function

    Public Shared Sub GetTokenAndUrlRemoveProduct(ByVal myeConnection As eConnection, ByRef returnToken As String, ByRef returnUrl As String, ByVal forceLogin As Boolean, ByVal givenCommessa As eBusinessData)
        Dim myRetValue As Data.DataTable
        Dim myPersonList As New ArrayList
        Dim myUser As eUser
        Dim myToken As String = ""
        Dim userNAme As String = ""
        Dim passwrod As String = ""
        Dim myUrlLogin As String = ""
        Dim myUrlRemoveProduct As String = ""
        Dim myB2B As eBusinessData
        Dim myePerson As ePerson
        Dim retValue As Boolean = False
        Dim returnValues As String = "false"
        Dim lastUpdate As String = ""


        Try
            'SetDBAConnection(myeConnection, myeConnection.ePLMS_HOME, myeConnection)
            myUser = New eUser(myeConnection)
            If myUser.Load(myeConnection.currentUserId) = ePLMS_OK Then
                myUser.getPersonList(myPersonList)
                If myPersonList IsNot Nothing Then
                    myePerson = New ePerson(myeConnection)
                    For Each person As eDataListElement In myPersonList
                        If myePerson.Load(person.Value) = ePLMS_OK Then
                            userNAme = myePerson.Text1
                            passwrod = myePerson.Text2
                            myToken = myePerson.URL.Split("|")(0)
                            Try
                                lastUpdate = myePerson.URL.Split("|")(1)
                            Catch ex As Exception

                            End Try

                            Exit For
                        End If

                    Next
                End If
            End If



            If userNAme <> "" And passwrod <> "" Then
                myB2B = New eBusinessData(myeConnection)
                If myB2B.Load("ExternalConnection", "B2B-Login", "", "", "", "", -1) = ePLMS_OK Then
                    myB2B.getAttribute("url", myUrlLogin)
                    GetUrlForExternalConnection(myeConnection, myB2B, myUrlLogin)
                End If
                If myB2B.Load("ExternalConnection", "B2B-DeleteProduct", "", "", "", "", -1) = ePLMS_OK Then
                    myB2B.getAttribute("url", myUrlRemoveProduct)
                    GetUrlForExternalConnection(myeConnection, myB2B, myUrlRemoveProduct)
                    Dim extraQS As String = ""
                    givenCommessa.getAttribute("NumeroOrdine", extraQS)
                    If extraQS <> "" Then
                        givenCommessa.getAttribute("TDOC", extraQS)
                        extraQS = "&tipodoc=" & extraQS
                    End If
                    If extraQS <> "" Then
                        myUrlRemoveProduct &= extraQS
                    End If
                    givenCommessa = Nothing
                End If

            End If

            Dim today As String = String.Format("{0}{1}{2}", Now.Year, Now.Month, Now.Day)
            If today <> lastUpdate Then
                myToken = ExternalSMConnection.StartLogin(myePerson, myUrlLogin, userNAme, passwrod)
            Else
                If forceLogin Then
                    myToken = ExternalSMConnection.StartLogin(myePerson, myUrlLogin, userNAme, passwrod)
                End If
            End If

            returnToken = myToken
            returnUrl = myUrlRemoveProduct



        Catch ex As Exception
            returnToken = ""
            returnUrl = ""
        Finally
            'CloseDBAConnection(myeConnection, myeConnection)
        End Try



    End Sub
    Public Shared Function GetUrlForExternalConnection(ByVal givenConnection As eConnection, ByVal givenBusinessData As eBusinessData, ByRef givenUrl As String) As Integer
        'Gestione Climapak
        givenBusinessData.getAttribute("url", givenUrl)
        Dim myGroupClimapack As New eGroup(givenConnection)
        Dim myGroupAlapc As New eGroup(givenConnection)
        Dim myUserClimapack As New ArrayList
        If myGroupClimapack.Load("Climapack") = ePLMS_OK And myGroupAlapc.Load("Alpac") = ePLMS_OK Then
            myGroupClimapack.getUserList(myUserClimapack)
            If myUserClimapack IsNot Nothing Then
                For Each edata As eDataListElement In myUserClimapack
                    If edata.Name.ToLower = givenConnection.currentUserName.ToLower Then
                        givenUrl = givenUrl.Replace(myGroupAlapc.Description, myGroupClimapack.Description)
                        Exit For
                    End If
                Next
            End If
        End If
        Return ePLMS_OK

    End Function

    Private Shared Function OperationSuccesfullExecuted(ByVal givenXML As String, ByRef returnMessage As String, ByVal operationType As String) As Boolean
        Dim myXmlDocument As New XmlDocument
        Dim retValue As Boolean = False
        myXmlDocument.LoadXml(givenXML)



        If myXmlDocument.FirstChild.NodeType = XmlNodeType.XmlDeclaration Then
            myXmlDocument.RemoveChild(myXmlDocument.FirstChild)
        End If

        Dim myEsito As XmlNodeList = myXmlDocument.FirstChild.SelectNodes("esito")
        Dim myMessaggio As XmlNodeList = myXmlDocument.FirstChild.SelectNodes("messaggio")
        returnMessage = ""
        If myEsito.Count > 0 Then
            If myEsito(0).InnerText.ToLower = "true" Then
                retValue = True
            Else
                retValue = False
            End If
        End If
        If Not retValue Then
            If myMessaggio.Count > 0 Then
                returnMessage = myMessaggio(0).InnerText.Trim
            End If
            If returnMessage = "" Or returnMessage.ToLower = "null" Then
                returnMessage = "Error in " & operationType
            End If
        End If


        Return retValue
    End Function
End Class
