﻿Imports Microsoft.VisualBasic
Imports Parallaksis.ePLMS33
Imports Parallaksis.ePLMS33.Defs
Imports Parallaksis.ePLMS33.Configurator

Public Class ValidatorEngine


    Public Shared Function UpdateUserDialog(ByVal givenAbacoId As String, ByVal givenAbacoName As String, ByVal givenDialogName As String, ByVal givenCurrentUserName As String, ByVal tipologiaDialogo As String) As Boolean

        Dim myPathToDeploy As String
        Dim dummyFileDialog As String
        Dim dummyFileAbaco As String
        'Dim myConfiguratorManager As ConfiguratorManager
        Dim retValueCompare As Boolean = False
        Dim myExtraVariabili As Dictionary(Of String, String)
        Dim myCodiciK As Dictionary(Of String, Double)
        Dim dirInfo As IO.DirectoryInfo
        Dim myFiles() As IO.FileInfo
        
        myPathToDeploy = System.Configuration.ConfigurationManager.AppSettings("TMP_PATH")
        myPathToDeploy = IO.Path.Combine(myPathToDeploy, givenAbacoId)

        dirInfo = New IO.DirectoryInfo(myPathToDeploy)
        If Not dirInfo.Exists Then
            dirInfo.Create()
        End If
        myFiles = dirInfo.GetFiles()
        If myFiles IsNot Nothing Then
            For Each file As IO.FileInfo In myFiles
                file.Delete()
            Next
        End If

        dummyFileDialog = DeployDialogFiles(myPathToDeploy, givenDialogName)
        dummyFileAbaco = DeployAbocoFile(myPathToDeploy, givenAbacoName)
       
        If dummyFileAbaco <> "" And dummyFileDialog <> "" Then
            myExtraVariabili = New Dictionary(Of String, String)
            myCodiciK = New Dictionary(Of String, Double)

            myExtraVariabili.Add("USER_GROUP", GetExtraVariali(givenCurrentUserName))
            myCodiciK = GetAllKCodes(givenAbacoId)
            If tipologiaDialogo.ToLower.Contains("posa") Then
                'myConfiguratorManager = New ConfiguratorManager(dummyFileDialog, myExtraVariabili, myCodiciK, False, False)
                retValueCompare = ConfiguratorManager.ValidateAllSteps(dummyFileAbaco, dummyFileDialog, myExtraVariabili, myCodiciK, False, False)
            Else

                'myConfiguratorManager = New ConfiguratorManager(dummyFileDialog, myExtraVariabili, myCodiciK, True, True)
                retValueCompare = ConfiguratorManager.ValidateAllSteps(dummyFileAbaco, dummyFileDialog, myExtraVariabili, myCodiciK, True, True)
            End If



            UpdateAbocoFile(dummyFileAbaco, givenAbacoName)


        End If

        Try
            IO.Directory.Delete(myPathToDeploy, True)
        Catch ex As Exception

        End Try
        Return retValueCompare
    End Function

    Public Shared Function UpdateUserDialogAngGetFile(ByVal givenAbacoId As String, ByVal givenAbacoName As String, ByVal givenDialogName As String, ByVal givenCurrentUserName As String, ByVal tipologiaDialogo As String, ByRef returnFileContent() As Byte) As Boolean
        Dim myPathToDeploy As String
        Dim dummyFileDialog As String
        Dim dummyFileAbaco As String

        Dim retValueCompare As Boolean = False
        Dim myExtraVariabili As Dictionary(Of String, String)
        Dim myCodiciK As Dictionary(Of String, Double)
        Dim dirInfo As IO.DirectoryInfo
        Dim myFiles() As IO.FileInfo

        myPathToDeploy = System.Configuration.ConfigurationManager.AppSettings("TMP_PATH")
        myPathToDeploy = IO.Path.Combine(myPathToDeploy, givenAbacoId)

        dirInfo = New IO.DirectoryInfo(myPathToDeploy)
        If Not dirInfo.Exists Then
            dirInfo.Create()
        End If
        myFiles = dirInfo.GetFiles()
        If myFiles IsNot Nothing Then
            For Each file As IO.FileInfo In myFiles
                file.Delete()
            Next
        End If

        dummyFileDialog = DeployDialogFiles(myPathToDeploy, givenDialogName)
        dummyFileAbaco = DeployAbocoFile(myPathToDeploy, givenAbacoName)

        If dummyFileAbaco <> "" And dummyFileDialog <> "" Then
            myExtraVariabili = New Dictionary(Of String, String)
            myCodiciK = New Dictionary(Of String, Double)

            myExtraVariabili.Add("USER_GROUP", GetExtraVariali(givenCurrentUserName))
            myCodiciK = GetAllKCodes(givenAbacoId)

            If tipologiaDialogo.ToLower.Contains("posa") Then
                'myConfiguratorManager = New ConfiguratorManager(dummyFileDialog, myExtraVariabili, myCodiciK, False, False)
                retValueCompare = ConfiguratorManager.ValidateAllSteps(dummyFileAbaco, dummyFileDialog, myExtraVariabili, myCodiciK, False, False)

            Else
                'myConfiguratorManager = New ConfiguratorManager(dummyFileDialog, myExtraVariabili, myCodiciK, True, True)
                retValueCompare = ConfiguratorManager.ValidateAllSteps(dummyFileAbaco, dummyFileDialog, myExtraVariabili, myCodiciK, True, True)

            End If


            returnFileContent = IO.File.ReadAllBytes(dummyFileAbaco)
            UpdateAbocoFile(dummyFileAbaco, givenAbacoName)

        End If

        Try
            IO.Directory.Delete(myPathToDeploy, True)
        Catch ex As Exception

        End Try
        Return retValueCompare
    End Function


    'Public Shared Function ValidateUserDialog(ByVal givenAbacoId As String, ByVal givenAbacoName As String, ByVal givenDialogName As String, ByVal givenCurrentUserName As String) As Boolean

    '    Dim myPathToDeploy As String
    '    Dim dummyFileDialog As String
    '    Dim dummyFileAbaco As String
    '    Dim myConfiguratorManager As ConfiguratorManager
    '    Dim retValueCompare As Boolean = False
    '    Dim myExtraVariabili As Dictionary(Of String, String)
    '    Dim myCodiciK As Dictionary(Of String, Double)
    '    Dim dirInfo As IO.DirectoryInfo
    '    Dim myFiles() As IO.FileInfo

    '    myPathToDeploy = System.Configuration.ConfigurationManager.AppSettings("TMP_PATH")
    '    myPathToDeploy = IO.Path.Combine(myPathToDeploy, givenAbacoId)
    '    dirInfo = New IO.DirectoryInfo(myPathToDeploy)
    '    If Not dirInfo.Exists Then
    '        dirInfo.Create()
    '    End If
    '    myFiles = dirInfo.GetFiles()
    '    If myFiles IsNot Nothing Then
    '        For Each file As IO.FileInfo In myFiles
    '            file.Delete()
    '        Next
    '    End If

    '    dummyFileDialog = DeployDialogFiles(myPathToDeploy, givenDialogName)
    '    dummyFileAbaco = DeployAbocoFile(myPathToDeploy, givenAbacoName)

    '    If dummyFileAbaco <> "" And dummyFileDialog <> "" Then
    '        myExtraVariabili = New Dictionary(Of String, String)
    '        myCodiciK = New Dictionary(Of String, Double)

    '        myExtraVariabili.Add("USER_GROUP", GetExtraVariali(givenCurrentUserName))
    '        myCodiciK = GetAllKCodes(givenAbacoId)

    '        myConfiguratorManager = New ConfiguratorManager
    '        retValueCompare = myConfiguratorManager.ValidateAllSteps(dummyFileAbaco, dummyFileDialog, myExtraVariabili, myCodiciK)

    '    End If

    '    Return retValueCompare

    'End Function


    Public Shared Function WasModifiedDialog(ByVal givenAbacoName As String, ByVal givenDialogName As String) As Boolean

        Dim myeFind As eFind
        Dim dummyTable As Data.DataTable
        Dim dummySQLQuery As String = ""
        Dim dateAbaco As String = ""
        Dim dateDialog As String = ""
        Dim retValue As Boolean = False

        dummySQLQuery = "$(select 'A' + eplms.eBusinessDataFile.eCreateDate from eplms.eBusinessDataFile where eBusinessDataFile.eName='{0}.xml' " & _
                        "UNION " & _
                        "select 'D' + eplms.eBusinessDataFile.eCreateDate from eplms.eBusinessDataFile where eBusinessDataFile.eName='{1}.xml' and eBusinessDataFile.eBusinessDataId in (select eId from eplms.eBusinessData where eBusinessDataType ='Dialogo' and eName='{1}'))"

        dummySQLQuery = String.Format(dummySQLQuery, givenAbacoName, givenDialogName)
        myeFind = New eFind(myeConnection)
        myeFind.queryString = dummySQLQuery
        myeFind.Prepare(Nothing)
        myeFind.Execute(dummyTable)
        If dummyTable IsNot Nothing Then
            If dummyTable.Rows.Count = 2 Then
                dateAbaco = dummyTable.Rows(0)(0).ToString.Substring(1)
                dateDialog = dummyTable.Rows(1)(0).ToString.Substring(1)
            End If
        End If

        If String.Compare(dateAbaco, dateDialog) < 0 Then
            retValue = True
        End If
        Return retValue


    End Function


#Region "UTILITY"

    Private Shared Function DeployDialogFiles(ByVal givenTargetPath As String, ByVal givenDialogName As String) As String

        Dim myeFind As eFind
        Dim dummyTable As Data.DataTable
        Dim dummyRow As Data.DataRow
        Dim dummySQLQuery As String = ""
        Dim retValue As String = ""
        Dim myBasePath As String = ""
        Dim dummyFileFrom As String
        Dim dummyFileTo As String


        dummySQLQuery = "$(select eplms.eBusinessDataFile.eName ,eplms.eBusinessDataFile.eCryptedName  from eplms.eBusinessDataFile where eBusinessDataFile.eBusinessDataId in (select eId from eplms.eBusinessData where eBusinessDataType ='Dialogo' and eName='{0}'))"
        dummySQLQuery = String.Format(dummySQLQuery, givenDialogName)
        myeFind = New eFind(myeConnection)
        myeFind.queryString = dummySQLQuery
        myeFind.Prepare(Nothing)
        myeFind.Execute(dummyTable)

        Try

            If dummyTable IsNot Nothing Then
                myBasePath = System.Configuration.ConfigurationManager.AppSettings("FTP_ADMIN_HOME")

                For Each dummyRow In dummyTable.Rows
                    dummyFileFrom = IO.Path.Combine(myBasePath, dummyRow(1))
                    dummyFileTo = IO.Path.Combine(givenTargetPath, dummyRow(0))
                    If IO.File.Exists(dummyFileFrom) Then
                        IO.File.Copy(dummyFileFrom, dummyFileTo)
                        If IO.File.Exists(dummyFileTo) And IO.Path.GetFileNameWithoutExtension(dummyFileTo).ToUpper = givenDialogName.ToUpper Then
                            retValue = dummyFileTo
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try


        Return retValue

    End Function

    Private Shared Function DeployAbocoFile(ByVal givenTargetPath As String, ByVal givenAbacoName As String) As String

        Dim myeFind As eFind
        Dim dummyTable As Data.DataTable
        Dim dummyRow As Data.DataRow
        Dim dummySQLQuery As String = ""
        Dim retValue As String = ""
        Dim myBasePath As String = ""
        Dim dummyFileFrom As String
        Dim dummyFileTo As String


        dummySQLQuery = "$(select eplms.eBusinessDataFile.eName ,eplms.eBusinessDataFile.eCryptedName  from eplms.eBusinessDataFile where  eBusinessDataFile.eName='{0}.xml')"
        dummySQLQuery = String.Format(dummySQLQuery, givenAbacoName)
        myeFind = New eFind(myeConnection)
        myeFind.queryString = dummySQLQuery
        myeFind.Prepare(Nothing)
        myeFind.Execute(dummyTable)

        Try

            If dummyTable IsNot Nothing Then
                myBasePath = System.Configuration.ConfigurationManager.AppSettings("FTP_DATA_HOME")

                For Each dummyRow In dummyTable.Rows
                    dummyFileFrom = IO.Path.Combine(myBasePath, dummyRow(1))
                    dummyFileTo = IO.Path.Combine(givenTargetPath, dummyRow(0))
                    If IO.File.Exists(dummyFileFrom) Then
                        IO.File.Copy(dummyFileFrom, dummyFileTo)
                        If IO.File.Exists(dummyFileTo) Then
                            retValue = dummyFileTo
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try


        Return retValue

    End Function

    Private Shared Function GetExtraVariali(ByVal currentUserName As String) As String


        Dim myUser As eUser
        Dim retValue As String = "null"
        Dim myArrayGroups As New ArrayList
        Dim myeGroup As New eGroup(myeConnection)


        myUser = New eUser(myeConnection)
        If myUser.Load(currentUserName) = ePLMS_OK Then
            myUser.getWhereReferencedByGroupList(myArrayGroups)
            If myArrayGroups IsNot Nothing Then
                For Each myGroup As eDataListElement In myArrayGroups
                    If myeGroup.Load(myGroup.Name) = ePLMS_OK Then
                        If myeGroup.Description <> "" Then
                            retValue = myeGroup.Name & " - " & myeGroup.Description
                        Else
                            retValue = myeGroup.Name
                        End If
                    End If

                Next
            End If
        End If
        Return retValue

    End Function

    Private Shared Function GetAllKCodes(ByVal givenAbacoId As String) As Generic.Dictionary(Of String, Double)

        Dim myCommessa As eBusinessData
        Dim myRelationList() As eBusinessDataRelation
        Dim qnt As Double
        Dim val As Double
        Dim tmpValue As String
        Dim retValue As New Generic.Dictionary(Of String, Double)
        Dim key1 As String = ""
        Dim listaK As String = ""

        eBusinessData.getAttribute(myeConnection, givenAbacoId, "key1", key1)

        myCommessa = New eBusinessData(myeConnection)

        If myCommessa.Load("Commessa", key1.Split("_")(0), key1.Split("_")(1), "", "", "", -1) = ePLMS_OK Then
            myCommessa.getBusinessDataRelationList(myRelationList)
            If myRelationList IsNot Nothing Then
                For Each relation As eBusinessDataRelation In myRelationList
                    If relation.childBusinessDataBusinessDataType.ToLower <> "abaco" Then
                        Continue For
                    End If
                    relation.businessDataInstance.getAttribute("Quantity", tmpValue)
                    relation.businessDataInstance.getAttribute("CodiciPosa", listaK)
                    If tmpValue <> "" AndAlso IsNumeric(tmpValue) And listaK <> "" Then
                        Dim myKs() As String = listaK.Split("|".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
                        For Each kString As String In myKs
                            kString = kString.Replace("::=", "|")
                            Dim kName As String = kString.Split("|")(0)
                            Dim kValue As String = kString.Split("|")(1)
                            If IsNumeric(kValue) Then
                                Dim myVal As Double = GetDoubleValue(kValue) * GetDoubleValue(tmpValue)
                                If Not retValue.ContainsKey(kName) Then
                                    retValue.Add(kName, 0)
                                End If
                                retValue(kName) = retValue(kName) + myVal
                            End If
                        Next
                    End If
                Next
            End If
        End If
        If retValue.Count = 0 Then
            retValue = Nothing
        End If
        Return retValue
    End Function

    Private Shared Function GetDoubleValue(ByVal givenValue As String) As Double
        If IsNumeric(givenValue) Then
            givenValue = givenValue.ToString.Replace(",", ".")
            Return Convert.ToDouble(givenValue, System.Globalization.CultureInfo.GetCultureInfo("en-US"))
        Else
            Return 0
        End If

    End Function

    Private Shared Function UpdateAbocoFile(ByVal givenFileToReplace As String, ByVal givenAbacoName As String) As Boolean

        Dim myeFind As eFind
        Dim dummyTable As Data.DataTable
        Dim dummyRow As Data.DataRow
        Dim dummySQLQuery As String = ""
        Dim retValue As Boolean = True
        Dim myBasePath As String = ""
        Dim dummyFileTo As String

        dummySQLQuery = "$(select eplms.eBusinessDataFile.eName ,eplms.eBusinessDataFile.eCryptedName  from eplms.eBusinessDataFile where  eBusinessDataFile.eName='{0}.xml')"
        dummySQLQuery = String.Format(dummySQLQuery, givenAbacoName)
        myeFind = New eFind(myeConnection)
        myeFind.queryString = dummySQLQuery
        myeFind.Prepare(Nothing)
        myeFind.Execute(dummyTable)

        ' Dim myLog As New IO.StreamWriter("c:\temp\text.log")
        Try

            If dummyTable IsNot Nothing Then
                myBasePath = System.Configuration.ConfigurationManager.AppSettings("FTP_DATA_HOME")

                For Each dummyRow In dummyTable.Rows
                    dummyFileTo = IO.Path.Combine(myBasePath, dummyRow(1))
                    'myLog.WriteLine(givenFileToReplace & " - " & dummyFileTo)
                    IO.File.Copy(givenFileToReplace, dummyFileTo, True)
                    Exit For
                Next
            End If

        Catch ex As Exception
            retValue = False
        End Try
        ' myLog.Close()

        Return retValue

    End Function


    Public Shared Function ResetPosa(ByVal myCommessaId As String) As Boolean

        Dim sendGestionaleEnabled As Boolean = False
        Dim syncToCadEnabled As Boolean = False
        Dim myCommessa As eBusinessData
        Dim myFilter As eFilter
        Dim rc As Integer
        Dim myAbacoList() As eBusinessDataRelation

        myCommessa = New eBusinessData(myeConnection)
        myFilter = New eFilter(myeConnection)

        myFilter.allowedBusinessDataType = "Abaco"
        'Dim i As Integer = 0
        rc = myCommessa.Load(myCommessaId)
        If rc = ePLMS_OK Then
            rc = myCommessa.getBusinessDataRelationList(myFilter, myAbacoList)
            If rc = ePLMS_OK And myAbacoList IsNot Nothing Then
                For Each abaco As eBusinessDataRelation In myAbacoList
                    Dim abacoPerPosa As String = ""
                    Dim isInGalileo As String = ""
                    abaco.businessDataInstance.getAttribute("tipologiaDialogo", abacoPerPosa)
                    abaco.businessDataInstance.getAttribute("SyncGalileo", isInGalileo)
                    If abaco.businessDataInstance.Level = "Complete" And abacoPerPosa.ToLower.Contains("posa") Then
                        Dim abacoNRRG As String = ""
                        Dim myRemoveProductUrl As String = ""
                        Dim myToken As String = ""
                        Dim NumeroOrdine As String
                        Dim TDOC As String = ""

                        If isInGalileo.ToUpper = "TRUE" Then


                            myCommessa.getAttribute("NumeroOrdine", NumeroOrdine)
                            abaco.businessDataInstance.getAttribute("NRRG", abacoNRRG)
                            ExternalSMConnection.GetTokenAndUrlRemoveProduct(myeConnection, myToken, myRemoveProductUrl, False, myCommessa)

                            If NumeroOrdine = "" Then
                                TDOC = "true"
                                NumeroOrdine = myCommessa.Name
                            Else
                                TDOC = "false"
                            End If
                            myRemoveProductUrl = String.Format(myRemoveProductUrl, myToken, NumeroOrdine, TDOC, abacoNRRG)
                            Dim tmpMessage As String = ""
                            Dim isRemoved As Boolean = ExternalSMConnection.RemoveProdottoCommessa(myToken, myRemoveProductUrl, abacoNRRG, tmpMessage)
                            If isRemoved Then
                                abaco.businessDataInstance.Reserve()
                                abaco.businessDataInstance.changeAttribute("SyncGalileo", "")
                                abaco.businessDataInstance.Unreserve()
                            End If

                        End If
                        'abaco.businessDataInstance.Reserve()
                        ' abaco.businessDataInstance.changeAttribute("SyncGalileo", "CHANGED")
                        ' abaco.businessDataInstance.Unreserve()
                        Dim newLevelName As String = "Created"
                        Dim newReason As String = "Changed by dba"
                        Dim myWorkflowEngine As New eWorkflowEngine(myeConnection, abaco.businessDataInstance.Id)
                        rc = myWorkflowEngine.reassignLevel(newLevelName, newReason, True)

                        Exit For

                    End If

                Next
            End If
        End If
        Return True
    End Function

    Public Shared Function ResetAbaco(ByVal givenAbacoId As Integer, ByVal givenNewLevel As String) As Boolean

        Dim myAbaco As eBusinessData
        Dim rc As Integer
        Dim newReason As String = "Changed by dba"
        Dim myWorkflowEngine As eWorkflowEngine
        Dim retValue As Boolean = False
        myAbaco = New eBusinessData(myeConnection)
        rc = myAbaco.Load(givenAbacoId)
        If rc = ePLMS_OK Then
            myWorkflowEngine = New eWorkflowEngine(myeConnection, myAbaco.Id)
            rc = myWorkflowEngine.reassignLevel(givenNewLevel, newReason, True)
            If rc = ePLMS_OK Then
                retValue = True
            End If
        End If


        Return retValue
    End Function


#End Region

End Class
