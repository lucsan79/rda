﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Parallaksis.ePLMS33.Configurator

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Service
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function UpdateUserDialog(ByVal givenAbacoId As String, ByVal givenAbacoName As String, ByVal givenDialogName As String, ByVal givenCurrentUser As String, ByVal tipologiaDialogo As String) As Boolean
        Return ValidatorEngine.UpdateUserDialog(givenAbacoId, givenAbacoName, givenDialogName, givenCurrentUser, tipologiaDialogo)
    End Function

    <WebMethod()> _
    Public Function UpdateUserDialogAndGetContent(ByVal givenAbacoId As String, ByVal givenAbacoName As String, ByVal givenDialogName As String, ByVal givenCurrentUser As String, ByVal tipologiaDialogo As String, ByRef returnConent() As Byte) As Boolean
        Return ValidatorEngine.UpdateUserDialogAngGetFile(givenAbacoId, givenAbacoName, givenDialogName, givenCurrentUser, tipologiaDialogo, returnConent)
    End Function

    <WebMethod()> _
    Public Function WasDialogAdminUpdated(ByVal givenAbacoId As String, ByVal givenAbacoName As String, ByVal givenDialogName As String, ByVal givenCurrentUser As String) As Boolean
        Return ValidatorEngine.WasModifiedDialog(givenAbacoName, givenDialogName)
    End Function

    <WebMethod()> _
    Public Function ResetPosa(ByVal givenCommessaId As String) As Boolean
        Return ValidatorEngine.ResetPosa(givenCommessaId)
    End Function

    <WebMethod()> _
    Public Function ResetAbaco(ByVal givenAbacoId As Integer, ByVal givenNewLevelName As String) As Boolean
        Return ValidatorEngine.ResetAbaco(givenAbacoId, givenNewLevelName)
    End Function

    '<WebMethod()> _
    'Public Function ValidateDialog(ByVal givenAbacoId As String, ByVal givenAbacoName As String, ByVal givenDialogName As String, ByVal givenCurrentUser As String) As Boolean
    '    Return ValidatorEngine.ValidateUserDialog(givenAbacoId, givenAbacoName, givenDialogName, givenCurrentUser)
    'End Function

    '<WebMethod()> _
    'Public Function test1() As Boolean
    '    Dim f1 As String = "D:\Parallaksis\temp\DialogValidator\3\A000397.xml"
    '    Dim f2 As String = "D:\Parallaksis\temp\DialogValidator\3\DLG-000074.xml"
    '    Dim myConfiguratorManager As New ConfiguratorManager()
    '    myConfiguratorManager.ValidateAllSteps(f1, f2, Nothing, Nothing)
    'End Function

End Class