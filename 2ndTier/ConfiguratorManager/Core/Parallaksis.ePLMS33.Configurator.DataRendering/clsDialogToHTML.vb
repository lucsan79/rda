﻿Imports Parallaksis.ePLMSM40.DataModel

Public Class DialogToHTML

    ''' <summary>
    ''' Used for html in iteraction mode
    ''' </summary>
    ''' <param name="givenDialog"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DialogToHtml1(givenDialog As Dialogs) As String
        Dim retValue As String = ""
        Dim dummyQuestion As DialogsQuestion
        If givenDialog IsNot Nothing Then
            If givenDialog.Questions IsNot Nothing Then
                For Each dummyQuestion In givenDialog.Questions
                    retValue &= QuestionHeader1(dummyQuestion)
                    retValue &= QuestionBody1(dummyQuestion)
                    retValue &= QuestionFooter1(dummyQuestion)

                Next
            End If
        End If


    End Function
    ''' <summary>
    ''' Used for html in summary mode
    ''' </summary>
    ''' <param name="givenDialog"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DialogToHtml2(givenDialog As Dialogs) As String
        Dim retValue As String



    End Function




    Private Shared Function QuestionHeader1(givenQuestion As DialogsQuestion)
        Dim retValue As String = ""

        retValue = String.Format("<p>{0}</p>", givenQuestion.Title)

        Return retValue
    End Function

    Private Shared Function QuestionBody1(givenQuestion As DialogsQuestion)
        Dim retValue As String = ""
        Dim dummyStringBuilder As Text.StringBuilder = Nothing
        Dim dummyAnswer As DialogsQuestionAnswer = Nothing
        If givenQuestion.Answers IsNot Nothing Then
            dummyStringBuilder = New Text.StringBuilder
            dummyStringBuilder.Append("<ul>")
            For Each dummyAnswer In givenQuestion.Answers
                Select Case dummyAnswer.Type
                    Case DialogsQuestionAnswer.AnswerType.Standard
                        dummyStringBuilder.Append(String.Format("<li id='{0}' parent='{1}' {2}>{3}</li>", dummyAnswer.id, dummyAnswer.idParent, IIf(dummyAnswer.IsSelected, "class='selected'", "onclick='liSelection(this);'"), dummyAnswer.title))
                    Case DialogsQuestionAnswer.AnswerType.CA
                        dummyStringBuilder.Append(String.Format("<li id='{0}' parent='{1}' {2}>{3}", dummyAnswer.id, dummyAnswer.idParent, IIf(dummyAnswer.IsSelected, "class='selected'", ""), dummyAnswer.title))
                        dummyStringBuilder.Append(String.Format("&nbsp:&nbsp<input id='txtCA_{0}' parent='{1}' caCode='{2}' type='text' value='{3}' {4}   />", dummyAnswer.id, dummyAnswer.idParent, "", dummyAnswer.TextValue, IIf(dummyAnswer.IsSelected, "disabled='disabled'", "")))
                        dummyStringBuilder.Append(String.Format("&nbsp;<img id='editCA_{0}' src='images/edit.png' width='16' height='16' border='0'  alt='Click Here to edit {1}' align='absmiddle' onclick=""unlock(this, 'txtCA_{0}');"" {2} />", dummyAnswer.id, dummyAnswer.title, IIf(dummyAnswer.IsSelected, "", "style='display:none'")))
                        dummyStringBuilder.Append(String.Format("&nbsp;<img id='saveCA_{0}' src='images/save.png' width='16' height='16' border='0'  alt='Click Here to Update {1}' align='absmiddle' onclick=""lock(this,'txtCA_{0}');"" {2} />", dummyAnswer.id, dummyAnswer.title, IIf(dummyAnswer.IsSelected, "style='display:none'", "")))
                        dummyStringBuilder.Append("</li>")
                    Case DialogsQuestionAnswer.AnswerType.TE
                        dummyStringBuilder.Append(String.Format("<li id='{0}' parent='{1}' {2}>{3}", dummyAnswer.id, dummyAnswer.idParent, IIf(dummyAnswer.IsSelected, "class='selected'", ""), dummyAnswer.title))
                        dummyStringBuilder.Append(String.Format("&nbsp:&nbsp<input id='txtTE_{0}' parent='{1}' teCode='{2}' type='text' value='{3}' readonly   />", dummyAnswer.id, dummyAnswer.idParent, "", dummyAnswer.TextValue))
                        retValue &= String.Format("&nbsp;<input type='button' id='externalTE_{0}' alt='Click to select a value for {1}' value='...' onclick=""openExternal('{0}','{2}');"" disabled='disabled'  />", dummyAnswer.id, dummyAnswer.title, "", IIf(dummyAnswer.IsSelected, "disabled='disabled'", ""))
                        dummyStringBuilder.Append(String.Format("&nbsp;<img id='editTE_{0}' src='images/edit.png' width='16' height='16' border='0'  alt='Click Here to edit {1}' align='absmiddle' onclick=""unlock(this, 'txtTE_{0}');"" {2} />", dummyAnswer.id, dummyAnswer.title, IIf(dummyAnswer.IsSelected, "", "style='display:none'")))
                        dummyStringBuilder.Append(String.Format("&nbsp;<img id='saveTE_{0}' src='images/save.png' width='16' height='16' border='0'  alt='Click Here to Update {1}' align='absmiddle' onclick=""lock(this,'txtTE_{0}');"" {2} />", dummyAnswer.id, dummyAnswer.title, IIf(dummyAnswer.IsSelected, "style='display:none'", "")))
                        dummyStringBuilder.Append("</li>")
                End Select

            Next
            dummyStringBuilder.Append("</ul>")
        End If




        Return retValue
    End Function

    Private Shared Function QuestionFooter1(givenQuestion As DialogsQuestion)
        Dim retValue As String = ""

        retValue = String.Format("</div>")
        Return retValue
    End Function


End Class
