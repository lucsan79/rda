﻿Imports Parallaksis.M40.DataModel.dsConfiguratorDialog
Imports Parallaksis.M40.DataModel

Public Class clsDataModelHelper

    Dim myDialogsDataSet As dsConfiguratorDialog

    Public Sub New(givenDialogSet As dsConfiguratorDialog)
        myDialogsDataSet = givenDialogSet
    End Sub

#Region "PROPERTY"

    Public ReadOnly Property GetCurrentDataModel As dsConfiguratorDialog
        Get
            Return myDialogsDataSet
        End Get
    End Property

    Public ReadOnly Property GetDialogTable(givenTable As TableName) As DataTable
        Get
            Return GetCurrentDataModel.Tables(givenTable.ToString)

        End Get
    End Property

#End Region


#Region "FILL"

    Public Function FillDialog(givenRow As QAOptionRow) As Dialogs
        Dim title As String
        Dim type As String
        Dim dummyDialogQuestion As DialogsQuestion
        Dim dummyDialogQuestionAnswer As DialogsAnswer
        Dim retDialog As New Dialogs
        Dim myTableDialogs As QAOptionDataTable

        Dim lqResult

        If givenRow IsNot Nothing Then
           
            myTableDialogs = myDialogsDataSet.Tables(TableName.QAOption.ToString)

            lqResult = (From qalist In myTableDialogs _
                            Where qalist.parentid = givenRow.id Select qalist)

            title = givenRow.title
            type = givenRow.type



            dummyDialogQuestion = Nothing

            Try


                Select Case givenRow.selectionType.ToUpper
                    Case DialogsQuestion.QuestionType.SingleOptionSelection.ToString.ToUpper
                        dummyDialogQuestion = New DialogsQuestion(DialogsQuestion.QuestionType.SingleOptionSelection)
                    Case DialogsQuestion.QuestionType.MultiOptionSelection.ToString.ToUpper
                        dummyDialogQuestion = New DialogsQuestion(DialogsQuestion.QuestionType.MultiOptionSelection)
                    Case DialogsQuestion.QuestionType.AutomaticFeaturesCompilation.ToString.ToUpper
                        dummyDialogQuestion = New DialogsQuestion(DialogsQuestion.QuestionType.AutomaticFeaturesCompilation)
                    Case Else
                        dummyDialogQuestion = New DialogsQuestion(DialogsQuestion.QuestionType.SingleOptionSelection)
                End Select
                For Each qalist In lqResult
                    If qalist.hasCA Then
                        dummyDialogQuestion = New DialogsQuestion(DialogsQuestion.QuestionType.FeaturesToAcquire)
                        Exit For
                    ElseIf qalist.hasTE Then
                        dummyDialogQuestion = New DialogsQuestion(DialogsQuestion.QuestionType.ExternalReferenceToAcquire)
                        Exit For
                    End If
                Next

                If givenRow.selected Then
                    dummyDialogQuestion.isProcessed = True
                End If
                dummyDialogQuestion.sequenceID = givenRow.number
                dummyDialogQuestion.Id = givenRow.id
                dummyDialogQuestion.IdParent = "_" & givenRow.number
                'dummyDialogQuestion.FullPath = givenRow.path
                dummyDialogQuestion.Title = title

                dummyDialogQuestion.Features = FillFeaturesCP(givenRow)
                dummyDialogQuestion.Conditions = FillCondition(givenRow)

                dummyDialogQuestion.CustomDescription = givenRow.AlternativeDescription
                dummyDialogQuestion.CustomDescriptionIndex = givenRow.AlternativeDescriptionPosition
                dummyDialogQuestion.GrigliaRapida = givenRow.grigliaAutomatica

                For Each qalist As QAOptionRow In lqResult
                    If dummyDialogQuestion.Answers IsNot Nothing And Not qalist.hasCA Then

                        dummyDialogQuestionAnswer = New DialogsAnswer
                        If qalist.selected Then
                            dummyDialogQuestionAnswer.isSelected = True
                        End If
                        dummyDialogQuestionAnswer.id = qalist.id
                        dummyDialogQuestionAnswer.sequenceId = qalist.number
                        dummyDialogQuestionAnswer.idParent = dummyDialogQuestion.Id
                        dummyDialogQuestionAnswer.title = qalist.title
                        dummyDialogQuestionAnswer.codeDigit = qalist.CODE_DIGIT
                        If IsNumeric(qalist.CODE_POSITION) Then
                            dummyDialogQuestionAnswer.codeDigitPosition = Integer.Parse(qalist.CODE_POSITION)
                        End If

                        dummyDialogQuestionAnswer.useForDescription = qalist.DESCRIPTION_CODE
                        dummyDialogQuestionAnswer.features = FillFeaturesCP(qalist)
                        dummyDialogQuestionAnswer.Conditions = FillCondition(qalist)
                        'dummyDialogQuestionAnswer.fullPath = qalist.path
                        dummyDialogQuestionAnswer.hasChild = qalist.hasChild
                        dummyDialogQuestionAnswer.isDefault = qalist.proposto
                        dummyDialogQuestionAnswer.forceChoise = qalist.fisso

                        dummyDialogQuestionAnswer.CustomDescription = qalist.AlternativeDescription
                        dummyDialogQuestionAnswer.CustomDescriptionIndex = qalist.AlternativeDescriptionPosition


                        If Not dummyDialogQuestion.Answers.Contains(dummyDialogQuestionAnswer) Then
                            dummyDialogQuestion.Answers.Add(dummyDialogQuestionAnswer)
                        End If
                    ElseIf dummyDialogQuestion.FeatureToAcquire IsNot Nothing And qalist.hasCA Then
                        FillFeaturesCA(dummyDialogQuestion, qalist)
                    ElseIf dummyDialogQuestion.ExternalReferenceToAcquire IsNot Nothing And qalist.hasTE Then
                        FillFeaturesTE(dummyDialogQuestion, qalist)
                    End If


                Next
            Catch ex As Exception
                dummyDialogQuestion = Nothing
            End Try
            If dummyDialogQuestion IsNot Nothing Then
                If Not retDialog.Questions.Contains(dummyDialogQuestion) Then
                    retDialog.Questions.Add(dummyDialogQuestion)
                End If
            Else
                retDialog.Questions = Nothing

            End If
        End If
        Return retDialog
    End Function

    Private Function FillCondition(givenRow As QAOptionRow) As List(Of DialogsCondition)
        Dim retValue As List(Of DialogsCondition) = Nothing
        Dim tableCondition As QAConditionsDataTable
        Dim tmpCondition As DialogsCondition
        Dim selectsConditions() As QAConditionsRow

        tableCondition = myDialogsDataSet.Tables(TableName.QAConditions.ToString)
        selectsConditions = tableCondition.Select("dtRefNumber='" & givenRow.number & "'")
        If selectsConditions.Length > 0 Then
            retValue = New List(Of DialogsCondition)
            For Each row As QAConditionsRow In selectsConditions
                tmpCondition = New DialogsCondition
                tmpCondition.Id = row.number '..Attributes("id").Value
                tmpCondition.RefType = row.type ' tmpNode.Attributes("type").Value
                tmpCondition.RefOption = row.refCode
                tmpCondition.RefOptionRelation = row.refOptionRelation ' tmpNode.Attributes("refOption").Value
                tmpCondition.RefOperator = row.refOperator
                tmpCondition.refValueMIN = row.minVal
                tmpCondition.refValueMAX = row.maxVal
                retValue.Add(tmpCondition)

                'tmpCondition = New DialogsCondition

            Next
        End If

        Return retValue



        Return retValue
    End Function

    Private Function FillFeaturesCP(ByVal givenRow As QAOptionRow) As List(Of DialogsFeature)
        Dim retValue As List(Of DialogsFeature) = Nothing
        Dim tmpFeature As DialogsFeature
        Dim selectFeatures() As QAFeaturesRow
        Dim tableFeatures As QAFeaturesDataTable

        tableFeatures = myDialogsDataSet.Tables(TableName.QAFeatures.ToString)
        selectFeatures = tableFeatures.Select("dtRefNumber='" & givenRow.number & "'")
        If selectFeatures.Length > 0 Then
            retValue = New List(Of DialogsFeature)
            For Each row As QAFeaturesRow In selectFeatures
                tmpFeature = New DialogsFeature
                tmpFeature.IdTable = row.number
                tmpFeature.Code = row.Code
                tmpFeature.Description = row.Description
                tmpFeature.Value = row.Value
                tmpFeature.ConnectTo = "" & row.connectedTo

                retValue.Add(tmpFeature)
            Next
        End If

        Return retValue

    End Function

    Private Sub FillFeaturesCA(ByRef givenDialogsQuestion As DialogsQuestion, ByVal givenRow As QAOptionRow)
        Dim tmpRow As QAInputRow
        Dim tableInput As QAInputDataTable
        Dim dummyDialogQuestionInput As DialogsInput
        Dim lqResul

        tableInput = GetDialogTable(TableName.QAInput)

        lqResul = From calist In tableInput
                  Where calist.dtRefNumber = givenRow.number
                  Select calist

        For Each tmpRow In lqResul

            dummyDialogQuestionInput = New DialogsInput
            dummyDialogQuestionInput.id = givenRow.id
            dummyDialogQuestionInput.sequenceId = givenRow.number
            dummyDialogQuestionInput.idParent = givenDialogsQuestion.Id
            dummyDialogQuestionInput.title = givenRow.title
            dummyDialogQuestionInput.CA_UM = tmpRow.um
            dummyDialogQuestionInput.CA_Code = tmpRow.code
            dummyDialogQuestionInput.CA_Description = tmpRow.description
            dummyDialogQuestionInput.CA_TextValue = tmpRow.value
            dummyDialogQuestionInput.CA_minValue = tmpRow.minVal
            dummyDialogQuestionInput.CA_maxValue = tmpRow.maxVal
            dummyDialogQuestionInput.CA_IdTable = tmpRow.number
            dummyDialogQuestionInput.CA_InputLength = tmpRow.InputLength
            dummyDialogQuestionInput.CustomDescription = tmpRow.AlternativeDescription
            dummyDialogQuestionInput.CustomDescriptionIndex = tmpRow.AlternativeDescriptionPosition
            If tmpRow.InputType.ToUpper = "N" Then
                dummyDialogQuestionInput.CA_ValueType = DialogsInput.CA_InputType.N.ToString
            ElseIf tmpRow.InputType.ToUpper = "S" Then
                dummyDialogQuestionInput.CA_ValueType = DialogsInput.CA_InputType.S.ToString
            ElseIf tmpRow.InputType.ToUpper = "D" Then
                dummyDialogQuestionInput.CA_ValueType = DialogsInput.CA_InputType.D.ToString
            Else
                dummyDialogQuestionInput.CA_ValueType = tmpRow.InputType



            End If
            If (tmpRow.isMandatory = "") Then
                tmpRow.isMandatory = True
            End If
            dummyDialogQuestionInput.IsMandatory = tmpRow.isMandatory
            If Not givenDialogsQuestion.FeatureToAcquire.Contains(dummyDialogQuestionInput) Then
                givenDialogsQuestion.FeatureToAcquire.Add(dummyDialogQuestionInput)
            End If


        Next



    End Sub

   
    Private Sub FillFeaturesTE(ByRef givenDialogsQuestion As DialogsQuestion, ByVal givenRow As QAOptionRow)
        Dim tmpRow As QAExternalReferenceRow
        Dim tableTE As QAExternalReferenceDataTable
        Dim lqResul
        Dim dummyDialogQuestionExtTable As DialogsExternalTable
        tableTE = GetDialogTable(TableName.QAExternalReference)

        lqResul = From calist In tableTE
                  Where calist.dtRefNumber = givenRow.number
                  Select calist

        For Each tmpRow In lqResul
            dummyDialogQuestionExtTable = New DialogsExternalTable
            dummyDialogQuestionExtTable.id = givenRow.id
            dummyDialogQuestionExtTable.sequenceId = tmpRow.number
            dummyDialogQuestionExtTable.idParent = givenRow.id
            dummyDialogQuestionExtTable.title = tmpRow.code.Replace("TE_", "")
            dummyDialogQuestionExtTable.TE_Code = tmpRow.code
            dummyDialogQuestionExtTable.TE_TextValue = tmpRow.selectedValue
            dummyDialogQuestionExtTable.SearchColumnsToShow = tmpRow.columnsView
            dummyDialogQuestionExtTable.OutputColum = tmpRow.columnOutput
            dummyDialogQuestionExtTable.SearchRowNumber = -1
            dummyDialogQuestionExtTable.CustomDescription = tmpRow.AlternativeDescription
            dummyDialogQuestionExtTable.CustomDescriptionIndex = tmpRow.AlternativeDescriptionPosition

            If IsNumeric(tmpRow.showRowNumber) Then
                dummyDialogQuestionExtTable.SearchRowNumber = Integer.Parse(tmpRow.showRowNumber)
            End If

            If tmpRow.columnNameFilter <> "" Then
                If tmpRow.columnNameFilter.Split(";").Length = tmpRow.columnValueFilter.Split(";").Length Then
                    For idx As Integer = 0 To tmpRow.columnNameFilter.Split(";").Length - 1
                        Dim colName As String = tmpRow.columnNameFilter.Split(";")(idx)
                        Dim colValue As String = tmpRow.columnValueFilter.Split(";")(idx)
                        If colName <> "" And colValue <> "" Then
                            dummyDialogQuestionExtTable.TE_ValueValidate &= colName & ":" & colValue & "|"

                        End If
                    Next
                End If
            End If
            If Not givenDialogsQuestion.ExternalReferenceToAcquire.Contains(dummyDialogQuestionExtTable) Then
                givenDialogsQuestion.ExternalReferenceToAcquire.Add(dummyDialogQuestionExtTable)
            End If
        Next


    End Sub


    Enum TableName
        QAOption
        QAFeatures
        QAExternalReference
        QAInput
        QAConditions
        QASelections
        QAVariabili
    End Enum

    Enum TableRelationsName
        QADialogs_QAFeatures
        QADialogs_QAConditions
        QADialogs_QAInput
        QADialogs_QAExternalReference

    End Enum

    Public Enum optionType
        SingleOptionSelection
        MultiOptionSelection
        AllMultiAnswerSelection
        FeaturesToAcquire
        ExternalReferenceToAcquire
        AutomaticFeaturesCompilation
    End Enum

#End Region

   
End Class
