﻿Public MustInherit Class eDialog

    Private featuresField As List(Of DialogsFeature)

    Private conditionsField As List(Of DialogsCondition)

    Private idField As String = ""

    Private idParentField As String = ""

    Private titleField As String = ""

    Private codeDigitValueField As String = ""

    Private usedForCodeField As Boolean = False

    Private usedForDescriptionField As Boolean = False

    Private idTableField As Integer = -1

    Private fullePathField As String = ""

    Private hasChildField As Boolean = False



    Public Sub New()
        MyBase.New()
        Me.conditionsField = New List(Of DialogsCondition)()
        Me.featuresField = New List(Of DialogsFeature)()
    End Sub

    Public Property hasChild() As Boolean
        Get
            Return Me.hasChildField
        End Get
        Set(value As Boolean)
            Me.hasChildField = value
        End Set
    End Property

    Public Property fullPath() As String
        Get
            Return Me.fullePathField
        End Get
        Set(value As String)
            Me.fullePathField = value
        End Set
    End Property


    Public Property sequenceId() As Integer
        Get
            Return Me.idTableField
        End Get
        Set(value As Integer)
            Me.idTableField = value
        End Set
    End Property

    Public Property features() As List(Of DialogsFeature)
        Get
            Return Me.featuresField
        End Get
        Set(value As List(Of DialogsFeature))
            Me.featuresField = value
        End Set
    End Property


    Public Property conditions() As List(Of DialogsCondition)
        Get
            Return Me.conditionsField
        End Get
        Set(value As List(Of DialogsCondition))
            Me.conditionsField = value
        End Set
    End Property


    Public Property id() As String
        Get
            Return Me.idField
        End Get
        Set(value As String)
            Me.idField = value
        End Set
    End Property

    Public Property idParent() As String
        Get
            Return Me.idParentField
        End Get
        Set(value As String)
            Me.idParentField = value
        End Set
    End Property

    Public Property title() As String
        Get
            Return Me.titleField
        End Get
        Set(value As String)
            Me.titleField = value
        End Set
    End Property

    Public Property usedForCode As Boolean
        Get
            Return Me.usedForCodeField
        End Get
        Set(value As Boolean)
            Me.usedForCodeField = value
        End Set
    End Property

    Public Property usedForDescription As Boolean
        Get
            Return Me.usedForDescriptionField
        End Get
        Set(value As Boolean)
            Me.usedForDescriptionField = value
        End Set
    End Property

    Public Property codeDigit As String
        Get
            Return Me.codeDigitValueField
        End Get
        Set(value As String)
            Me.codeDigitValueField = value
        End Set
    End Property


End Class
