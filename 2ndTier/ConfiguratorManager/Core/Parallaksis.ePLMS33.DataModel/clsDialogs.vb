Imports System
Imports System.Diagnostics
Imports System.Xml.Serialization
Imports System.Collections
Imports System.Xml.Schema
Imports System.ComponentModel
Imports System.Collections.Generic


Public Class Dialogs

#Region "Dialog Property"
    Private questionField As List(Of DialogsQuestion)
    Private errorsField As List(Of String)

#End Region
   

    Public Sub New()
        MyBase.New()
        Me.questionField = New List(Of DialogsQuestion)
        Me.errorsField = New List(Of String)
    End Sub

    Public Property Questions() As List(Of DialogsQuestion)
        Get
            Return Me.questionField
        End Get
        Set(value As List(Of DialogsQuestion))
            Me.questionField = value
        End Set
    End Property

    Public Property ErrorInDialog() As List(Of String)
        Get
            Return Me.errorsField
        End Get
        Set(value As List(Of String))
            Me.errorsField = value
        End Set
    End Property

    Public Function AddQuestionToDialog(givenQuestion As DialogsQuestion) As Boolean

        Dim retValue As Boolean = False
        If Questions Is Nothing Then
            questionField = New List(Of DialogsQuestion)
        End If
        If Not Questions.Contains(givenQuestion) Then
            Questions.Add(givenQuestion)
            retValue = True
        End If
        Return retValue

    End Function

    
End Class

Partial Public Class DialogsQuestion
    Implements ICloneable

   

#Region "VARIABILI"
    Private conditionsField As List(Of DialogsCondition)

    Private featuresField As List(Of DialogsFeature)

    Private answersField As List(Of DialogsAnswer)

    Private inputField As List(Of DialogsInput)

    Private externalTableField As List(Of DialogsExternalTable)

    Private idField As String

    Private idParentField As String

    Private titleField As String

    Private typeField As QuestionType

    Private isProcessedField As Boolean = False

    'Private isMandatoryField As Boolean
    Private idTableField As Integer = -1

    Private indexField As Integer = -1

    Private inMultiSelectionField As Boolean = False

    Private alternativeDescritpion As String = ""

    Private altrnativeDescriptionPosition As String = "-1"

    Private grigliaAutomatica As Boolean = False
#End Region

#Region "COSTRUTTORE"
    Public Sub New(givenTypeQuestion As QuestionType)
        MyBase.New()
        Me.Type = givenTypeQuestion
        If givenTypeQuestion = QuestionType.FeaturesToAcquire Then
            Me.inputField = New List(Of DialogsInput)

        ElseIf givenTypeQuestion = QuestionType.ExternalReferenceToAcquire Then
            Me.externalTableField = New List(Of DialogsExternalTable)
        ElseIf givenTypeQuestion = QuestionType.MultiOptionSelection Or givenTypeQuestion = QuestionType.SingleOptionSelection Or givenTypeQuestion = QuestionType.AutomaticFeaturesCompilation Then
            Me.answersField = New List(Of DialogsAnswer)
        End If


        Me.conditionsField = New List(Of DialogsCondition)
        Me.featuresField = New List(Of DialogsFeature)

    End Sub
#End Region

#Region "PROPERTY"
    Public Function Clone() As Object Implements System.ICloneable.Clone
        Return MyBase.MemberwiseClone()
    End Function


    Public Property IsInMultiSelection() As Boolean
        Get
            Return Me.inMultiSelectionField
        End Get
        Set(value As Boolean)
            Me.inMultiSelectionField = value
        End Set
    End Property

    Public Property isProcessed() As Boolean
        Get
            Return Me.isProcessedField
        End Get
        Set(value As Boolean)
            Me.isProcessedField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlArrayAttribute(Order:=0), _
     System.Xml.Serialization.XmlArrayItemAttribute("conditions", IsNullable:=False)> _
    Public Property Conditions() As List(Of DialogsCondition)
        Get
            Return Me.conditionsField
        End Get
        Set(value As List(Of DialogsCondition))
            Me.conditionsField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlArrayAttribute(Order:=0), _
     System.Xml.Serialization.XmlArrayItemAttribute("featues", IsNullable:=False)> _
    Public Property Features() As List(Of DialogsFeature)
        Get
            Return Me.featuresField
        End Get
        Set(value As List(Of DialogsFeature))
            Me.featuresField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlArrayAttribute(Order:=0), _
    System.Xml.Serialization.XmlArrayItemAttribute("answers", IsNullable:=False)> _
    Public Property Answers() As List(Of DialogsAnswer)
        Get
            Return Me.answersField
        End Get
        Set(value As List(Of DialogsAnswer))
            Me.answersField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlArrayAttribute(Order:=0), _
   System.Xml.Serialization.XmlArrayItemAttribute("answers", IsNullable:=False)> _
    Public Property FeatureToAcquire() As List(Of DialogsInput)
        Get
            Return Me.inputField
        End Get
        Set(value As List(Of DialogsInput))
            Me.inputField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlArrayAttribute(Order:=0), _
  System.Xml.Serialization.XmlArrayItemAttribute("answers", IsNullable:=False)> _
    Public Property ExternalReferenceToAcquire() As List(Of DialogsExternalTable)
        Get
            Return Me.externalTableField
        End Get
        Set(value As List(Of DialogsExternalTable))
            Me.externalTableField = value
        End Set
    End Property

    Public Property Id() As String
        Get
            Return Me.idField
        End Get
        Set(value As String)
            Me.idField = value
        End Set
    End Property

    Public Property IdParent() As String
        Get
            Return Me.idParentField
        End Get
        Set(value As String)
            Me.idParentField = value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return Me.titleField
        End Get
        Set(value As String)
            Me.titleField = value
        End Set
    End Property

    Public Property sequenceID() As Integer
        Get
            Return Me.indexField
        End Get
        Set(value As Integer)
            Me.indexField = value
        End Set
    End Property

    Public Property Type() As QuestionType
        Get
            Return Me.typeField
        End Get
        Set(value As QuestionType)
            Me.typeField = value
        End Set
    End Property

    Public Property CustomDescription() As String
        Get
            Return Me.alternativeDescritpion
        End Get
        Set(ByVal value As String)
            Me.alternativeDescritpion = value
        End Set
    End Property

    Public Property CustomDescriptionIndex() As String
        Get
            Return Me.altrnativeDescriptionPosition
        End Get
        Set(ByVal value As String)
            Me.altrnativeDescriptionPosition = value
        End Set
    End Property

    Public Property GrigliaRapida() As Boolean
        Get
            Return Me.grigliaAutomatica
        End Get
        Set(ByVal value As Boolean)
            Me.grigliaAutomatica = value
        End Set
    End Property

#End Region

#Region "ENUM"
    Public Enum QuestionType
        SingleOptionSelection
        MultiOptionSelection
        FeaturesToAcquire
        ExternalReferenceToAcquire
        AutomaticFeaturesCompilation
    End Enum
#End Region

End Class

Partial Public Class DialogsCondition

#Region "VARIABILI"
    Private idField As String = ""

    Private refOptionField As String = ""

    Private refTypeField As String = ""

    Private refOptionRelationField As String = ""

    Private refOperatorField As String = ""

    Private refValueField As String = ""

    Private refValueMinField As String = ""

    Private refValueMaxField As String = ""

    Private idTableField As Integer = -1
#End Region

#Region "PROPERTY"

    
    Public Property IdTable() As Integer
        Get
            Return Me.idTableField
        End Get
        Set(value As Integer)
            Me.idTableField = value
        End Set
    End Property

    Public Property Id() As String
        Get
            Return Me.idField
        End Get
        Set(value As String)
            Me.idField = value
        End Set
    End Property

    Public Property RefOption() As String
        Get
            Return Me.refOptionField
        End Get
        Set(value As String)
            Me.refOptionField = value
        End Set
    End Property

    Public Property RefOptionRelation() As String
        Get
            Return Me.refOptionRelationField
        End Get
        Set(value As String)
            Me.refOptionRelationField = value
        End Set
    End Property

    Public Property RefType() As String
        Get
            Return Me.refTypeField
        End Get
        Set(value As String)
            Me.refTypeField = value
        End Set
    End Property

    Public Property RefOperator() As String
        Get
            Return Me.refOperatorField
        End Get
        Set(value As String)
            Me.refOperatorField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlTextAttribute()> _
    Public Property refValue() As String
        Get
            Return Me.refValueField
        End Get
        Set(value As String)
            Me.refValueField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlTextAttribute()> _
    Public Property refValueMIN() As String
        Get
            Return Me.refValueMinField
        End Get
        Set(value As String)
            Me.refValueMinField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlTextAttribute()> _
    Public Property refValueMAX() As String
        Get
            Return Me.refValueMaxField
        End Get
        Set(value As String)
            Me.refValueMaxField = value
        End Set
    End Property

#End Region


End Class

Partial Public Class DialogsAnswer

    Private featureField As New List(Of DialogsFeature)

    Private conditionsField As List(Of DialogsCondition)

    Private idField As String = ""

    Private idParentField As String = ""

    Private titleField As String = ""

    Private isSelectedField As Boolean = False

    Private codeDigitValueField As String = ""

    Private codeDigitPositionField As Integer = -1

    Private usedForDescriptionField As String = ""

    Private idTableField As Integer = -1

    Private fullPathField As String = ""

    Private hasChildField As Boolean = False
    Private isDefaultField As Boolean = False

    Private forceChoiseField As Boolean = False

    Private alternativeDescritpion As String = ""

    Private altrnativeDescriptionPosition As String = "-1"

    Public Sub New()
        MyBase.New()
        Me.conditionsField = New List(Of DialogsCondition)()
        Me.featureField = New List(Of DialogsFeature)()

       

    End Sub


    Public Property hasChild() As Boolean
        Get
            Return Me.hasChildField
        End Get
        Set(value As Boolean)
            Me.hasChildField = value
        End Set
    End Property

    Public Property isDefault() As Boolean
        Get
            Return Me.isDefaultField
        End Get
        Set(value As Boolean)
            Me.isDefaultField = value
        End Set
    End Property

    Public Property forceChoise() As Boolean
        Get
            Return Me.forceChoiseField
        End Get
        Set(value As Boolean)
            Me.forceChoiseField = value
        End Set
    End Property
    Public Property sequenceId() As Integer
        Get
            Return Me.idTableField
        End Get
        Set(value As Integer)
            Me.idTableField = value
        End Set
    End Property

    Public Property features() As List(Of DialogsFeature)
        Get
            Return Me.featureField
        End Get
        Set(value As List(Of DialogsFeature))
            Me.featureField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlArrayAttribute(Order:=1), _
     System.Xml.Serialization.XmlArrayItemAttribute("condition", IsNullable:=False)> _
    Public Property Conditions() As List(Of DialogsCondition)
        Get
            Return Me.conditionsField
        End Get
        Set(value As List(Of DialogsCondition))
            Me.conditionsField = value
        End Set
    End Property


    Public Property isSelected() As Boolean
        Get
            Return Me.isSelectedField
        End Get
        Set(value As Boolean)
            Me.isSelectedField = value
        End Set
    End Property

    Public Property id() As String
        Get
            Return Me.idField
        End Get
        Set(value As String)
            Me.idField = value
        End Set
    End Property

    Public Property idParent() As String
        Get
            Return Me.idParentField
        End Get
        Set(value As String)
            Me.idParentField = value
        End Set
    End Property

    Public Property title() As String
        Get
            Return Me.titleField
        End Get
        Set(value As String)
            Me.titleField = value
        End Set
    End Property


    Public Property useForDescription As String
        Get
            Return Me.usedForDescriptionField
        End Get
        Set(value As String)
            Me.usedForDescriptionField = value
        End Set
    End Property

    Public Property codeDigit As String
        Get
            Return Me.codeDigitValueField
        End Get
        Set(value As String)
            Me.codeDigitValueField = value
        End Set
    End Property
    Public Property codeDigitPosition As Integer
        Get
            Return Me.codeDigitPositionField
        End Get
        Set(value As Integer)
            Me.codeDigitPositionField = value
        End Set
    End Property

    Public Property CustomDescription() As String
        Get
            Return Me.alternativeDescritpion
        End Get
        Set(ByVal value As String)
            Me.alternativeDescritpion = value
        End Set
    End Property

    Public Property CustomDescriptionIndex() As String
        Get
            Return Me.altrnativeDescriptionPosition
        End Get
        Set(ByVal value As String)
            Me.altrnativeDescriptionPosition = value
        End Set
    End Property

    Public Enum AnswerType
        CA
        TE
        Standard
    End Enum
End Class

Partial Public Class DialogsInput

    Private featureField As List(Of DialogsFeature)

    Private conditionsField As List(Of DialogsCondition)

    Private idField As String = ""

    Private idParentField As String = ""

    Private titleField As String = ""

    Private codeDigitValueField As String = ""

    Private usedForCodeField As Boolean = False

    Private usedForDescriptionField As Boolean = False

    Private isMandatoryField As Boolean = False

    Private inputLength As String = "50"


    Private CA_IdTableField As Integer = -1

    Private CA_minValueField As String = ""

    Private CA_maxValueFiled As String = ""

    Private CA_CodeField As String = ""

    Private CA_DescriptionField As String = ""

    Private CA_textValueField As String = ""

    Private CA_Field As String = ""

    Private CA_ValidateField As String = ""

    Private idTableField As Integer = -1

    Private CA_grigliaAutomatica As Boolean = False

    Private CA_ValueTypeField As String = CA_InputType.S.ToString

    Private hasChildField As Boolean = False

    Private alternativeDescritpion As String = ""

    Private altrnativeDescriptionPosition As String = "-1"


    Public Sub New()
        MyBase.New()
        Me.conditionsField = New List(Of DialogsCondition)()
        Me.featureField = New List(Of DialogsFeature)()

    End Sub

    Public Property HasChild() As Boolean
        Get
            Return Me.hasChildField
        End Get
        Set(value As Boolean)
            Me.hasChildField = value
        End Set
    End Property

    Public Property sequenceId() As Integer
        Get
            Return Me.idTableField
        End Get
        Set(value As Integer)
            Me.idTableField = value
        End Set
    End Property

    Public Property CA_ValueType() As String
        Get
            Return Me.CA_ValueTypeField
        End Get
        Set(value As String)
            Me.CA_ValueTypeField = value
        End Set
    End Property

   
    Public Property id() As String
        Get
            Return Me.idField
        End Get
        Set(value As String)
            Me.idField = value
        End Set
    End Property

    Public Property idParent() As String
        Get
            Return Me.idParentField
        End Get
        Set(value As String)
            Me.idParentField = value
        End Set
    End Property

    Public Property title() As String
        Get
            Return Me.titleField
        End Get
        Set(value As String)
            Me.titleField = value
        End Set
    End Property


    Public Property CA_TextValue As String
        Get
            Return Me.CA_textValueField
        End Get
        Set(value As String)
            Me.CA_textValueField = value
        End Set
    End Property

    Public Property UsedForCode As Boolean
        Get
            Return Me.usedForCodeField
        End Get
        Set(value As Boolean)
            Me.usedForCodeField = value
        End Set
    End Property

    Public Property UsedForDescription As Boolean
        Get
            Return Me.usedForDescriptionField
        End Get
        Set(value As Boolean)
            Me.usedForDescriptionField = value
        End Set
    End Property

    Public Property CodeDigit As String
        Get
            Return Me.codeDigitValueField
        End Get
        Set(value As String)
            Me.codeDigitValueField = value
        End Set
    End Property

    Public Property IsMandatory() As Boolean
        Get
            Return Me.isMandatoryField
        End Get
        Set(value As Boolean)
            Me.isMandatoryField = value
        End Set
    End Property
    Public Property CA_InputLength() As String
        Get
            Return Me.inputLength
        End Get
        Set(value As String)
            Me.inputLength = value
        End Set
    End Property


    Public Property CA_Code As String
        Get
            Return Me.CA_CodeField
        End Get
        Set(value As String)
            Me.CA_CodeField = value
        End Set
    End Property

    Public Property CA_Description As String
        Get
            Return Me.CA_DescriptionField
        End Get
        Set(value As String)
            Me.CA_DescriptionField = value
        End Set
    End Property


    Public Property CA_UM As String
        Get
            Return Me.CA_Field
        End Get
        Set(value As String)
            Me.CA_Field = value
        End Set
    End Property

    Public Property CA_ValueValidate As String
        Get
            Return Me.CA_ValidateField
        End Get
        Set(value As String)
            Me.CA_ValidateField = value
        End Set
    End Property

    Public Property CA_IdTable As Integer
        Get
            Return Me.CA_IdTableField
        End Get
        Set(value As Integer)
            Me.CA_IdTableField = value
        End Set
    End Property

    Public Property CA_minValue As String
        Get
            Return Me.CA_minValueField
        End Get
        Set(value As String)
            Me.CA_minValueField = value
        End Set
    End Property

    Public Property CA_maxValue As String
        Get
            Return Me.CA_maxValueFiled
        End Get
        Set(value As String)
            Me.CA_maxValueFiled = value
        End Set
    End Property

    Public Property GrigliaAutomatica As Boolean
        Get
            Return Me.CA_grigliaAutomatica
        End Get
        Set(ByVal value As Boolean)
            Me.CA_grigliaAutomatica = value
        End Set
    End Property

    Public Property CustomDescription() As String
        Get
            Return Me.alternativeDescritpion
        End Get
        Set(ByVal value As String)
            Me.alternativeDescritpion = value
        End Set
    End Property

    Public Property CustomDescriptionIndex() As String
        Get
            Return Me.altrnativeDescriptionPosition
        End Get
        Set(ByVal value As String)
            Me.altrnativeDescriptionPosition = value
        End Set
    End Property


    Public Enum CA_InputType
        N
        S
        D
    End Enum

End Class


Partial Public Class DialogsExternalTable

    Private searchParametersField As Generic.Dictionary(Of String, String)

    Private idField As String = ""

    Private idParentField As String = ""

    Private idTableField As String = ""

    Private titleField As String = ""

    Private TE_CodeField As String = ""

    Private TE_DescriptionField As String = ""

    Private TE_textValueField As String = ""

    Private TE_um_Field As String = ""

    Private TE_ValidateField As String = ""

    Private fullePathField As String = ""

    Private hasChildField As Boolean = False

    Private showRowNumber As Integer = 0

    Private columnsToShow As String = ""
    Private columnsToOutput As String = ""
    Private TE_grigliaAutomatica As Boolean = False

    Private alternativeDescritpion As String = ""

    Private altrnativeDescriptionPosition As String = "-1"

    Public Property CustomDescription() As String
        Get
            Return Me.alternativeDescritpion
        End Get
        Set(ByVal value As String)
            Me.alternativeDescritpion = value
        End Set
    End Property

    Public Property CustomDescriptionIndex() As String
        Get
            Return Me.altrnativeDescriptionPosition
        End Get
        Set(ByVal value As String)
            Me.altrnativeDescriptionPosition = value
        End Set
    End Property
    Public Property SearchRowNumber() As Integer
        Get
            Return Me.showRowNumber
        End Get
        Set(value As Integer)
            Me.showRowNumber = value
        End Set
    End Property


    Public Property SearchColumnsToShow() As String
        Get
            Return Me.columnsToShow
        End Get
        Set(value As String)
            Me.columnsToShow = value
        End Set
    End Property

    Public Property OutputColum() As String
        Get
            Return Me.columnsToOutput
        End Get
        Set(value As String)
            Me.columnsToOutput = value
        End Set
    End Property


    Public Property HasChild() As Boolean
        Get
            Return Me.hasChildField
        End Get
        Set(value As Boolean)
            Me.hasChildField = value
        End Set
    End Property


    Public Sub New()
        MyBase.New()
        TE_TextValue = ""
        
    End Sub

    Public Property FullPath() As String
        Get
            Return Me.fullePathField
        End Get
        Set(value As String)
            Me.fullePathField = value
        End Set
    End Property


    Public Property sequenceId() As Integer
        Get
            Return Me.idTableField
        End Get
        Set(value As Integer)
            Me.idTableField = value
        End Set
    End Property
    Public Property id() As String
        Get
            Return Me.idField
        End Get
        Set(value As String)
            Me.idField = value
        End Set
    End Property

    Public Property idParent() As String
        Get
            Return Me.idParentField
        End Get
        Set(value As String)
            Me.idParentField = value
        End Set
    End Property

    Public Property title() As String
        Get
            Return Me.titleField
        End Get
        Set(value As String)
            Me.titleField = value
        End Set
    End Property


    Public Property TE_TextValue As String
        Get
            Return Me.TE_textValueField
        End Get
        Set(value As String)
            Me.TE_textValueField = value
        End Set
    End Property

   
    Public Property TE_Code As String
        Get
            Return Me.TE_CodeField
        End Get
        Set(value As String)
            Me.TE_CodeField = value
        End Set
    End Property

    Public Property TE_Description As String
        Get
            Return Me.TE_DescriptionField
        End Get
        Set(value As String)
            Me.TE_DescriptionField = value
        End Set
    End Property


    Public Property UM As String
        Get
            Return Me.TE_um_Field
        End Get
        Set(value As String)
            Me.TE_um_Field = value
        End Set
    End Property

    Public Property TE_ValueValidate As String
        Get
            Return Me.TE_ValidateField
        End Get
        Set(value As String)
            Me.TE_ValidateField = value
        End Set
    End Property


    Public Property GrigliaAutomatica As Boolean
        Get
            Return Me.TE_grigliaAutomatica
        End Get
        Set(ByVal value As Boolean)
            Me.TE_grigliaAutomatica = value
        End Set
    End Property

   
End Class



Partial Public Class DialogsFeature

    Private idTableField As Integer

    Private typeField As String

    Private codeField As String

    Private descriptionField As String

    Private valueField As String

    Private orginalValueField As String

    Private connectToField As String

    Private target As String

    Public Property IdTable() As Integer
        Get
            Return Me.idTableField
        End Get
        Set(value As Integer)
            Me.idTableField = value
        End Set
    End Property
   
    Public Property Code() As String
        Get
            Return Me.codeField
        End Get
        Set(value As String)
            Me.codeField = value
        End Set
    End Property

    Public Property ConnectTo() As String
        Get
            Return "" & Me.connectToField
        End Get
        Set(ByVal value As String)
            Me.connectToField = "" & value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return Me.descriptionField
        End Get
        Set(value As String)
            Me.descriptionField = value
        End Set
    End Property

    Public Property FeatureTarget() As String
        Get
            Return Me.target
        End Get
        Set(ByVal value As String)
            Me.target = value
        End Set
    End Property

    <System.Xml.Serialization.XmlTextAttribute()> _
    Public Property Value() As String
        Get
            Return Me.valueField
        End Get
        Set(value As String)
            Me.valueField = value
        End Set
    End Property

    <System.Xml.Serialization.XmlTextAttribute()> _
    Public Property OriginalValue() As String
        Get
            Return Me.orginalValueField
        End Get
        Set(ByVal value As String)
            Me.orginalValueField = value
        End Set
    End Property

    Public Sub New()
        Code = ""
        Description = ""
        Value = ""
    End Sub

    
   
End Class






