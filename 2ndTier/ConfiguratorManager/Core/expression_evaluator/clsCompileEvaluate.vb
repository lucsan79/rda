﻿Imports System.Text
Imports Parallaksis.M40.DataModel.dsConfiguratorDialog

Public Class clsCompileEvaluate
    Private mAssembly As System.Reflection.Assembly


    Dim QA As New Generic.Dictionary(Of String, String)
    Dim CA As New Generic.Dictionary(Of String, String)
    Dim TE As New Generic.Dictionary(Of String, String)
    Dim CP As New Generic.Dictionary(Of String, String)
    Dim EXTENALVARS As New Generic.Dictionary(Of String, String)

    Public Enum FunctionType
        F4CA
        F4TE
    End Enum

    Private Function PrepareFunctionVerify(code As String, functionType As FunctionType) As String

        Dim sb As New StringBuilder
        With sb
            .Append("Imports System" & vbCrLf)
            .Append("Imports System.Collections" & vbCrLf)
            .Append("Imports System.Collections.Generic" & vbCrLf)
            .Append("Public Class clsFunctionExecutor")
            .Append(vbCrLf)
            .Append(vbCrLf)
            .Append("Shared QA As Generic.Dictionary(Of String, String)" & vbCrLf)
            .Append("Shared CA As Generic.Dictionary(Of String, String)" & vbCrLf)
            .Append("Shared TE As Generic.Dictionary(Of String, String)" & vbCrLf)
            .Append("Shared CP As Generic.Dictionary(Of String, String)" & vbCrLf)
            .Append("Shared EXTERNALVAR As Generic.Dictionary(Of String, String)" & vbCrLf)
            .Append("    Public Shared Function Verify(givenQA as Generic.Dictionary(Of String, String),givenCA As Generic.Dictionary(Of String, String),givenTE As Generic.Dictionary(Of String, String), givenCP as Generic.Dictionary(Of String, String), givenEXTERNALVAR as Generic.Dictionary(Of String, String)) As String")
            .Append(vbCrLf)
            .Append("       QA=givenQA")
            .Append(vbCrLf)
            .Append("       CA=givenCA")
            .Append(vbCrLf)
            .Append("       TE=givenTE")
            .Append(vbCrLf)
            .Append("       CP=givenCP")
            .Append(vbCrLf)
            .Append("       EXTERNALVAR=givenEXTERNALVAR")
            .Append(vbCrLf)
            .Append(vbCrLf)
            '.Append("       Dim CA As New Generic.Dictionary(Of String, String)")
            '.Append(vbCrLf)
            .Append(code)
            '.Append(vbCrLf)
            '.Append(calist)
            '.Append(vbCrLf)
            '.Append(telist)
            .Append(vbCrLf)
            .Append(vbCrLf)
            .Append("    End Function")
            .Append(vbCrLf)
            .Append(vbTab & "Private Shared Function IsSelectedOption(givenCode As String) As Boolean" & vbCrLf)
            .Append("        Dim retValue As Boolean = False" & vbCrLf)
            .Append("        If QA IsNot Nothing Then" & vbCrLf)
            .Append("            For Each item As String In QA.Keys" & vbCrLf)
            .Append("                If item = (givenCode) Then" & vbCrLf)
            .Append("                    If QA(item).ToLower = ""selected"" Then" & vbCrLf)
            .Append("                        retValue = True" & vbCrLf)
            .Append("                    End If" & vbCrLf)
            .Append("                    Exit For" & vbCrLf)
            .Append("                End If" & vbCrLf)
            .Append("            Next" & vbCrLf)
            .Append("        End If" & vbCrLf)
            .Append("        return retValue" & vbCrLf)
            .Append("    End Function" & vbCrLf)
            .Append(vbCrLf)
            .Append(vbTab & "Private Shared Function IsSelectedInput(givenCode As String) As Boolean" & vbCrLf)
            .Append("        Dim retValue As Boolean = False" & vbCrLf)
            .Append("        If CA IsNot Nothing Then" & vbCrLf)
            .Append("            For Each item As String In CA.Keys" & vbCrLf)
            .Append("                If item = (givenCode) Then" & vbCrLf)
            .Append("                    If CA(item).ToLower <> """" Then" & vbCrLf)
            .Append("                        retValue = True" & vbCrLf)
            .Append("                    End If" & vbCrLf)
            .Append("                    Exit For" & vbCrLf)
            .Append("                End If" & vbCrLf)
            .Append("            Next" & vbCrLf)
            .Append("        End If" & vbCrLf)
            .Append("        return retValue" & vbCrLf)
            .Append("    End Function" & vbCrLf)
            .Append(vbTab & "Private Shared Function IsSelectedExtTable(givenCode As String) As Boolean" & vbCrLf)
            .Append("        Dim retValue As Boolean = False" & vbCrLf)
            .Append("        If TE IsNot Nothing Then" & vbCrLf)
            .Append("            For Each item As String In TE.Keys" & vbCrLf)
            .Append("                If item = (givenCode) Then" & vbCrLf)
            .Append("                    If TE(item).ToLower <> """" Then" & vbCrLf)
            .Append("                        retValue = True" & vbCrLf)
            .Append("                    End If" & vbCrLf)
            .Append("                    Exit For" & vbCrLf)
            .Append("                End If" & vbCrLf)
            .Append("            Next" & vbCrLf)
            .Append("        End If" & vbCrLf)
            .Append("        return retValue" & vbCrLf)
            .Append("    End Function" & vbCrLf)
            .Append(vbTab & "Private Shared Function GetInputValue(givenCode As String) As String" & vbCrLf)
            .Append("        Dim retValue As String = """"" & vbCrLf)
            .Append("        If CA IsNot Nothing Then" & vbCrLf)
            .Append("            For Each item As String In CA.Keys" & vbCrLf)
            .Append("                If item = (givenCode) Then" & vbCrLf)
            .Append("                   retValue = CA(item)" & vbCrLf)
            .Append("                   Exit For" & vbCrLf)
            .Append("                End If" & vbCrLf)
            .Append("            Next" & vbCrLf)
            .Append("        End If" & vbCrLf)
            .Append("        return retValue" & vbCrLf)
            .Append("    End Function" & vbCrLf)
            .Append(vbTab & "Private Shared Function GetExtTableValue(givenCode As String) As String" & vbCrLf)
            .Append("        Dim retValue As String = """"" & vbCrLf)
            .Append("        If TE IsNot Nothing Then" & vbCrLf)
            .Append("            For Each item As String In TE.Keys" & vbCrLf)
            .Append("                If item = (givenCode) Then" & vbCrLf)
            .Append("                   retValue = TE(item)" & vbCrLf)
            .Append("                   Exit For" & vbCrLf)
            .Append("                End If" & vbCrLf)
            .Append("            Next" & vbCrLf)
            .Append("        End If" & vbCrLf)
            .Append("        return retValue" & vbCrLf)
            .Append("    End Function" & vbCrLf)
            .Append(vbCrLf)
            .Append(vbTab & "Private Shared Function GetFeatureValue(givenCode As String) As String" & vbCrLf)
            .Append("        Dim retValue As String = """"" & vbCrLf)
            .Append("        If CP IsNot Nothing Then" & vbCrLf)
            .Append("            For Each item As String In CP.Keys" & vbCrLf)
            .Append("                If item = (givenCode) Then" & vbCrLf)
            .Append("                   retValue = CP(item)" & vbCrLf)
            .Append("                   Exit For" & vbCrLf)
            .Append("                End If" & vbCrLf)
            .Append("            Next" & vbCrLf)
            .Append("        End If" & vbCrLf)
            .Append("        return retValue" & vbCrLf)
            .Append("    End Function" & vbCrLf)
            .Append(vbCrLf)
            .Append(vbTab & "Private Shared Function GetEXTERNALVARValue(givenCode As String) As String" & vbCrLf)
            .Append("        Dim retValue As String = """"" & vbCrLf)
            .Append("        If EXTERNALVAR IsNot Nothing Then" & vbCrLf)
            .Append("            For Each item As String In EXTERNALVAR.Keys" & vbCrLf)
            .Append("                If item = (givenCode) Then" & vbCrLf)
            .Append("                   retValue = EXTERNALVAR(item)" & vbCrLf)
            .Append("                   Exit For" & vbCrLf)
            .Append("                End If" & vbCrLf)
            .Append("            Next" & vbCrLf)
            .Append("        End If" & vbCrLf)
            .Append("        return retValue" & vbCrLf)
            .Append("    End Function" & vbCrLf)
            .Append(vbCrLf)
            .Append("End Class")

        End With

        Return sb.ToString
    End Function


    Private Function CompileCode(givenFunction As String) As Boolean
        Dim provider As Microsoft.VisualBasic.VBCodeProvider
        Dim compiler As System.CodeDom.Compiler.ICodeCompiler
        Dim params As System.CodeDom.Compiler.CompilerParameters
        Dim results As System.CodeDom.Compiler.CompilerResults

        params = New System.CodeDom.Compiler.CompilerParameters
        params.GenerateInMemory = True      'Assembly is created in memory
        params.TreatWarningsAsErrors = False
        params.WarningLevel = 4
        'Put any references you need here - even you own dll's, if you want to use one
        Dim refs() As String = {"System.dll", "Microsoft.VisualBasic.dll"}

        params.ReferencedAssemblies.AddRange(refs)

        Try
            provider = New Microsoft.VisualBasic.VBCodeProvider
            compiler = provider.CreateCompiler
            results = compiler.CompileAssemblyFromSource(params, givenFunction)
        Catch ex As Exception
            'Compile errors don't throw exceptions; you've got some deeper problem...
            MessageBox.Show(ex.Message)
            Exit Function
        End Try

        ''Output errors to the listbox
        'lbErrors.Items.Clear()

        'Dim err As System.CodeDom.Compiler.CompilerError
        'For Each err In results.Errors
        '    lbErrors.Items.Add(String.Format( _
        '        "Line {0}, Col {1}: Error {2} - {3}", _
        '        err.Line, err.Column, err.ErrorNumber, err.ErrorText))
        'Next

        If results.Errors.Count = 0 Then        'No compile errors or warnings...
            mAssembly = results.CompiledAssembly
            Return True
        Else
            mAssembly = Nothing
            Return False
        End If
    End Function


    Public Sub New(ByVal givenTableOption As QAOptionDataTable, ByVal givenTableInput As QAInputDataTable, ByVal givenTableExt As QAExternalReferenceDataTable, ByVal givenTableCP As QAFeaturesDataTable, givenExternalValues As Generic.Dictionary(Of String, String))
        Dim lq1
        Dim myQSB As New StringBuilder
        Dim qaRow As QAOptionRow
        lq1 = From qaItem In givenTableOption.AsEnumerable
              Where Not qaItem.id.Contains(".") And qaItem.level > 0
              Select qaItem

        For Each qaRow In lq1
            Try

                If qaRow.selected Then
                    QA.Add(qaRow.id, "selected")

                Else
                    QA.Add(qaRow.id, "avoid")

                End If
                myQSB.Append(vbCrLf)
            Catch ex As Exception

            End Try

        Next



        lq1 = From qaItem In givenTableOption.AsEnumerable
              Where Not qaItem.id.Contains(".") And qaItem.level > 0 And qaItem.selected
              Select qaItem

        For Each qaRow In lq1
            Dim lq2 = From caItems In givenTableInput
                      Where caItems.dtRefNumber = qaRow.number
                      Select caItems
            Dim value As String = ""
            For Each item As QAInputRow In lq2
                value = item.value
                Dim id As String = item.ID
                If id = "" Then
                    id = item.code
                End If

                If Not CA.ContainsKey(id) Then
                    If qaRow.selected Then
                        CA.Add(id, value)
                    Else
                        CA.Add(id, "")

                    End If
                End If

            Next

        Next


        lq1 = From qaItem In givenTableOption.AsEnumerable
              Where Not qaItem.id.Contains(".") And qaItem.level > 0 And qaItem.selected
              Select qaItem

        For Each qaRow In lq1
            Dim lq2 = From cpItems In givenTableCP Select cpItems
            ' Where cpItems.dtRefNumber > 0'= qaRow.number
            'se cpItems
            Dim value As String = ""
            For Each item As QAFeaturesRow In lq2
                value = item.Value
                Dim id As String = item.ID
                If id = "" Then
                    id = item.Code
                End If
                If Not CP.ContainsKey(id) Then
                    If qaRow.selected Then
                        CP.Add(id, value)
                        '' Else
                        ''    CP.Add(id, "")

                    End If
                End If

            Next

        Next


        lq1 = From qaItem In givenTableOption.AsEnumerable
              Where Not qaItem.id.Contains(".") And qaItem.level > 0 And qaItem.selected
              Select qaItem

        For Each qaRow In lq1
            Dim lq2 = From teItems In givenTableExt
                      Where teItems.dtRefNumber = qaRow.number
                      Select teItems
            Dim value As String = ""
            For Each item As QAExternalReferenceRow In lq2
                value = item.selectedValue
                If Not TE.ContainsKey(item.ID) Then
                    If qaRow.selected Then
                        TE.Add(item.ID, value)
                    Else
                        TE.Add(item.ID, "")
                    End If
                End If

            Next

        Next

        If givenExternalValues IsNot Nothing Then
            For Each key As String In givenExternalValues.Keys
                If Not EXTENALVARS.ContainsKey(key) Then
                    EXTENALVARS.Add(key, givenExternalValues(key))
                End If
            Next
        End If

    End Sub

    Public Function EvaluateCAFunction(givenRule As String) As String
        Dim dummyFunction As String = ""
        Dim retValue As String = ""
        dummyFunction = PrepareFunctionVerify(givenRule, FunctionType.F4CA)
        If CompileCode(dummyFunction) Then
            If mAssembly Is Nothing Then
                MessageBox.Show("Compile first!")
                Return ""
            End If


            Dim scriptType As Type
            Dim instance As Object
            Dim rslt As Object

            Try
                'Get the type from the assembly.  This will allow us access to
                'all the properties and methods.
                scriptType = mAssembly.GetType("clsFunctionExecutor")

                'Set up an array of objects to pass as arguments.
                Dim args() As Object = {QA, CA, TE, CP, EXTENALVARS}

                'And call the static function
                rslt = scriptType.InvokeMember("Verify", _
                    System.Reflection.BindingFlags.InvokeMethod Or _
                    System.Reflection.BindingFlags.Public Or _
                    System.Reflection.BindingFlags.Static, _
                    Nothing, Nothing, args)

                'Return value is an object, cast it back to a string and display
                If Not rslt Is Nothing Then
                    retValue = CType(rslt, String)
                End If
            Catch ex As Exception
                Throw New Exception(ex.ToString)
                'MessageBox.Show(ex.ToString)
            End Try
        End If
        Return retValue
    End Function

End Class
