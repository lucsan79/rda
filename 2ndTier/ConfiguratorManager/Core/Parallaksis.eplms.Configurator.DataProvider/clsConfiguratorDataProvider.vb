﻿Imports System.Xml
Imports Parallaksis.M40.DataModel
Imports Parallaksis.M40.DataModel.clsDataModelHelper

Public Class DataProvider

    Private Shared returnConfiguratorQAData As dsConfiguratorDialog.QAOptionDataTable
    Private Shared returnConfiguratorConditions As dsConfiguratorDialog.QAConditionsDataTable
    Private Shared returnConfiguratorExternal As dsConfiguratorDialog.QAExternalReferenceDataTable
    Private Shared returnConfiguratorInput As dsConfiguratorDialog.QAInputDataTable
    Private Shared returnConfiguratorFeature As dsConfiguratorDialog.QAFeaturesDataTable

    ' Dim io As New IO.StreamWriter("c:\temp\testColl.txt")
#Region "OBJECT DATA PROVIDER"
    Public Sub InitConfiguratorDataByObject(ByRef returnConfiguratorDialogData As DataTable, ByVal givenObject As Object)


    End Sub
#End Region

#Region "XML DATA PROVIDER"
    Public Function InitConfiguratorDataByXML(ByVal givenFileFullPath As String, ByRef returnFamiglia As String, ByRef returnProdotto As String, ByRef returnCodifica As String, ByRef returnCadName As String) As dsConfiguratorDialog
        Dim dummyXmlDoc As XmlDocument
        Dim dummyXmlNode As XmlNode
        Dim dummyXmlNodeList As XmlNodeList
        Dim myeConfiguratorDialog As dsConfiguratorDialog


        myeConfiguratorDialog = New dsConfiguratorDialog
        returnConfiguratorQAData = myeConfiguratorDialog.Tables.Item(TableName.QAOption.ToString)
        returnConfiguratorConditions = myeConfiguratorDialog.Tables.Item(TableName.QAConditions.ToString)
        returnConfiguratorExternal = myeConfiguratorDialog.Tables.Item(TableName.QAExternalReference.ToString)
        returnConfiguratorInput = myeConfiguratorDialog.Tables.Item(TableName.QAInput.ToString)
        returnConfiguratorFeature = myeConfiguratorDialog.Tables.Item(TableName.QAFeatures.ToString)

        If Not System.IO.File.Exists(givenFileFullPath) Then
            Throw New Exception("Configuration file not found")
        End If

        dummyXmlDoc = New XmlDocument
        dummyXmlDoc.Load(givenFileFullPath)

        If dummyXmlDoc.ChildNodes IsNot Nothing Then
            dummyXmlNodeList = dummyXmlDoc.SelectNodes("//Dialogs/Question")
            For Each dummyXmlNode In dummyXmlNodeList
                FillDataByXml("", dummyXmlNode, 0, "")

            Next
        End If
        If dummyXmlDoc.FirstChild IsNot Nothing Then
            If dummyXmlDoc.FirstChild.Attributes("CODIFICA") IsNot Nothing Then
                returnCodifica = dummyXmlDoc.FirstChild.Attributes("CODIFICA").Value
            End If

            If dummyXmlDoc.FirstChild.Attributes("CADASM") IsNot Nothing Then
                returnCadName = dummyXmlDoc.FirstChild.Attributes("CADASM").Value
            End If

            If dummyXmlDoc.FirstChild.Attributes("FAMIGLIA") IsNot Nothing Then
                returnFamiglia = dummyXmlDoc.FirstChild.Attributes("FAMIGLIA").Value
            End If

            If dummyXmlDoc.FirstChild.Attributes("PRODOTTO") IsNot Nothing Then
                returnProdotto = dummyXmlDoc.FirstChild.Attributes("PRODOTTO").Value
            End If
        End If
        ' io.Close()
        Return myeConfiguratorDialog

    End Function

    Public Function InitConfiguratorDataByXML(ByVal givenDataSet As dsConfiguratorDialog) As dsConfiguratorDialog



        returnConfiguratorQAData = givenDataSet.Tables(TableName.QAOption.ToString)
        returnConfiguratorConditions = givenDataSet.Tables(TableName.QAConditions.ToString)
        returnConfiguratorExternal = givenDataSet.Tables(TableName.QAExternalReference.ToString)
        returnConfiguratorInput = givenDataSet.Tables(TableName.QAInput.ToString)
        returnConfiguratorFeature = givenDataSet.Tables(TableName.QAFeatures.ToString)

       
        Return givenDataSet

    End Function


    Public Shared Function SavePersistentDialog(ByVal givenDataSet As dsConfiguratorDialog) As Boolean

        returnConfiguratorQAData = givenDataSet.Tables(TableName.QAOption.ToString)
        returnConfiguratorConditions = givenDataSet.Tables(TableName.QAConditions.ToString)
        returnConfiguratorExternal = givenDataSet.Tables(TableName.QAExternalReference.ToString)
        returnConfiguratorInput = givenDataSet.Tables(TableName.QAInput.ToString)
        returnConfiguratorFeature = givenDataSet.Tables(TableName.QAFeatures.ToString)

        Dim myFileDialog As String = AppDomain.CurrentDomain.BaseDirectory & "tmp\last.xml"
        Dim myFileCADDialog As String = AppDomain.CurrentDomain.BaseDirectory & "tmp\cadParam.xml"
        givenDataSet.WriteXml(myFileDialog)

        Dim lqCad = From qaCAD In returnConfiguratorQAData
                    Where qaCAD.CAD_REF <> ""
                    Select qaCAD

        Dim myCADWR As New IO.StreamWriter(myFileCADDialog)
        If System.IO.File.Exists(myFileCADDialog) Then
            For Each qaCAD As dsConfiguratorDialog.QAOptionRow In lqCad
                Dim stringWR As String = String.Format("{0}:={1}", qaCAD.CAD_REF, qaCAD.CAD_VALUE)
                myCADWR.WriteLine(stringWR)
            Next
        End If
        myCADWR.Close()


        Return True

    End Function

    Private Sub FillDataByXml(ByRef givenParentId As String, ByRef givenXmlNode As XmlNode, givenLevel As Integer, givenParentPath As String)
        Dim dummyRow As dsConfiguratorDialog.QAOptionRow
        Dim dummyRowCA As dsConfiguratorDialog.QAInputRow
        Dim dummyRowTE As dsConfiguratorDialog.QAExternalReferenceRow
        Dim dummyRowCP As dsConfiguratorDialog.QAFeaturesRow
        Dim dummyRowAMM As dsConfiguratorDialog.QAConditionsRow
        Dim featureNode As XmlNode
        Dim tmpNode As XmlNode


        dummyRow = returnConfiguratorQAData.NewQAOptionRow

        If givenXmlNode IsNot Nothing Then
            If givenXmlNode.Name.ToLower = "question" Or givenXmlNode.Name.ToLower = "answers" Then
                dummyRow.number = returnConfiguratorQAData.Count
                dummyRow.parentid = givenParentId
                dummyRow.id = givenXmlNode.Attributes("ID").Value
                dummyRow.type = givenXmlNode.Name.ToLower
                dummyRow.title = givenXmlNode.Attributes("TITLE").Value
                dummyRow.level = givenLevel
                dummyRow.path = givenParentPath & "\" & dummyRow.id

                dummyRow.selectionType = optionType.SingleOptionSelection.ToString
                If givenXmlNode.Attributes("OPTIONTYPE") IsNot Nothing Then
                    Select Case givenXmlNode.Attributes("OPTIONTYPE").Value.ToUpper
                        Case optionType.SingleOptionSelection.ToString.ToUpper
                            dummyRow.selectionType = optionType.SingleOptionSelection.ToString
                        Case optionType.MultiOptionSelection.ToString.ToUpper
                            dummyRow.selectionType = optionType.MultiOptionSelection.ToString
                        Case optionType.AutomaticFeaturesCompilation.ToString.ToUpper
                            dummyRow.selectionType = optionType.AutomaticFeaturesCompilation.ToString
                    End Select
                End If
                If givenXmlNode.Attributes("DEFAULT") IsNot Nothing Then
                    dummyRow.proposto = Boolean.Parse(givenXmlNode.Attributes("DEFAULT").Value)
                End If
                If givenXmlNode.Attributes("FIXED") IsNot Nothing Then
                    dummyRow.fisso = Boolean.Parse(givenXmlNode.Attributes("FIXED").Value)
                End If
                If givenXmlNode.Attributes("CAD_REF") IsNot Nothing Then
                    dummyRow.CAD_REF = (givenXmlNode.Attributes("CAD_REF").Value)
                End If
                If givenXmlNode.Attributes("CAD_VALUE") IsNot Nothing Then
                    dummyRow.CAD_VALUE = (givenXmlNode.Attributes("CAD_VALUE").Value)
                End If
                If givenXmlNode.Attributes("CODE_DIGIT") IsNot Nothing Then
                    dummyRow.CODE_DIGIT = (givenXmlNode.Attributes("CODE_DIGIT").Value)
                End If
                If givenXmlNode.Attributes("CODE_POSITION") IsNot Nothing Then
                    dummyRow.CODE_POSITION = (givenXmlNode.Attributes("CODE_POSITION").Value)
                End If
                If givenXmlNode.Attributes("DESCRIPTION_CODE") IsNot Nothing Then
                    dummyRow.DESCRIPTION_CODE = (givenXmlNode.Attributes("DESCRIPTION_CODE").Value)
                End If

                dummyRow.AlternativeDescription = ""
                dummyRow.AlternativeDescriptionPosition = "-1"

                If givenXmlNode.Attributes("ALTERNATIVEDESCRABACO") IsNot Nothing Then
                    Try
                        dummyRow.AlternativeDescription = (givenXmlNode.Attributes("ALTERNATIVEDESCRABACO").Value)
                    Catch ex As Exception
                        dummyRow.AlternativeDescription = ""
                    End Try
                End If
                If givenXmlNode.Attributes("ALTERNATIVEDESCRABACOPOSITION") IsNot Nothing Then
                    Try
                        dummyRow.AlternativeDescriptionPosition = (givenXmlNode.Attributes("ALTERNATIVEDESCRABACOPOSITION").Value)
                    Catch ex As Exception
                        dummyRow.AlternativeDescriptionPosition = "-1"
                    End Try
                End If

                If givenXmlNode.Attributes("GRIGLIAAUTOMATICA") IsNot Nothing Then
                    Try
                        dummyRow.grigliaAutomatica = (givenXmlNode.Attributes("GRIGLIAAUTOMATICA").Value)
                    Catch ex As Exception
                        dummyRow.grigliaAutomatica = False
                    End Try
                End If



                If givenXmlNode.SelectSingleNode("Features") IsNot Nothing Then
                    featureNode = givenXmlNode.SelectSingleNode("Features")
                End If
                If featureNode IsNot Nothing Then
                    If featureNode.ChildNodes IsNot Nothing Then


                        For Each tmpNode In featureNode.ChildNodes
                            If tmpNode.Attributes("FTYPE") IsNot Nothing Then
                                Select Case tmpNode.Attributes("FTYPE").Value.ToUpper
                                    Case "CP"
                                        dummyRowCP = returnConfiguratorFeature.NewQAFeaturesRow
                                        dummyRowCP.number = returnConfiguratorFeature.Count
                                        dummyRowCP.dtRefNumber = dummyRow.number
                                        dummyRowCP.qaId = dummyRow.id
                                        dummyRowCP.Code = tmpNode.Attributes("CODE").Value.Split("-")(0).Trim
                                        dummyRowCP.Description = tmpNode.Attributes("CODE").Value.Split("-")(1).Trim
                                        If tmpNode.Attributes("ID") IsNot Nothing Then
                                            dummyRowCP.ID = tmpNode.Attributes("ID").Value
                                        End If
                                        If tmpNode.Attributes("CODE").Value.Contains("-") Then
                                            dummyRowCP.Code = tmpNode.Attributes("CODE").Value.Split("-")(0).Trim
                                            dummyRowCP.Description = tmpNode.Attributes("CODE").Value.Split("-")(1).Trim

                                        End If
                                        dummyRowCP.connectedTo = ""
                                        If tmpNode.Attributes("REF_TO") IsNot Nothing Then
                                            dummyRowCP.connectedTo = tmpNode.Attributes("REF_TO").Value
                                        End If
                                        If tmpNode.Attributes("TARGET") IsNot Nothing Then
                                            dummyRowCP.FeatureTarget = tmpNode.Attributes("TARGET").Value
                                        End If
                                        dummyRowCP.UM = tmpNode.Attributes("UM").Value
                                        dummyRowCP.Value = tmpNode.Attributes("VALUE").Value
                                        dummyRowCP.OriginalValue = tmpNode.Attributes("VALUE").Value
                                        dummyRow.hasCP = True
                                        returnConfiguratorFeature.AddQAFeaturesRow(dummyRowCP)
                               
                                    Case "CA"
                                        dummyRowCA = returnConfiguratorInput.NewQAInputRow
                                        If tmpNode.Attributes("ID") IsNot Nothing Then
                                            dummyRowCA.ID = tmpNode.Attributes("ID").Value
                                        End If
                                        dummyRowCA.number = returnConfiguratorInput.Count
                                        dummyRowCA.dtRefNumber = dummyRow.number
                                        dummyRowCA.qaId = dummyRow.id
                                        dummyRowCA.path = givenParentPath & "\" & dummyRow.id

                                        If tmpNode.Attributes("CODE").Value.Contains(" - ") Then
                                            dummyRowCA.code = tmpNode.Attributes("CODE").Value.Split(" - ")(0).Trim
                                            dummyRowCA.description = tmpNode.Attributes("CODE").Value.ToString.Replace(dummyRowCA.code & " - ", "")
                                        Else
                                            dummyRowCA.code = tmpNode.Attributes("CODE").Value
                                            dummyRowCA.description = tmpNode.Attributes("CODE").Value
                                        End If


                                        If tmpNode.Attributes("UM") IsNot Nothing Then
                                            dummyRowCA.um = tmpNode.Attributes("UM").Value
                                        End If
                                        If tmpNode.Attributes("VALUE") IsNot Nothing Then
                                            dummyRowCA.value = tmpNode.Attributes("VALUE").Value
                                        End If
                                        If tmpNode.Attributes("MINVAL") IsNot Nothing Then
                                            dummyRowCA.minVal = tmpNode.Attributes("MINVAL").Value
                                        End If
                                        If tmpNode.Attributes("MAXVAL") IsNot Nothing Then
                                            dummyRowCA.maxVal = tmpNode.Attributes("MAXVAL").Value
                                        End If

                                        If tmpNode.Attributes("TYPE") IsNot Nothing Then
                                            dummyRowCA.InputType = tmpNode.Attributes("TYPE").Value
                                        Else
                                            dummyRowCA.InputType = "S"
                                        End If
                                        If tmpNode.Attributes("ISMANDATORY") IsNot Nothing Then
                                            dummyRowCA.isMandatory = tmpNode.Attributes("ISMANDATORY").Value
                                        Else
                                            dummyRowCA.isMandatory = "true"
                                        End If

                                        If tmpNode.Attributes("VALUELENGTH") IsNot Nothing Then
                                            dummyRowCA.InputLength = tmpNode.Attributes("VALUELENGTH").Value
                                        Else
                                            dummyRowCA.InputLength = "50"
                                        End If

                                        If tmpNode.Attributes("GRIGLIAAUTOMATICA") IsNot Nothing Then
                                            Try
                                                dummyRowCA.grigliaAutomatica = Boolean.Parse(tmpNode.Attributes("GRIGLIAAUTOMATICA").Value)
                                            Catch ex As Exception
                                                dummyRowCA.grigliaAutomatica = False
                                            End Try
                                        End If

                                        dummyRowCA.AlternativeDescription = ""
                                        dummyRowCA.AlternativeDescriptionPosition = "-1"
                                        If tmpNode.Attributes("ALTERNATIVEDESCRABACO") IsNot Nothing Then
                                            Try
                                                dummyRowCA.AlternativeDescription = (tmpNode.Attributes("ALTERNATIVEDESCRABACO").Value)
                                            Catch ex As Exception
                                                dummyRowCA.AlternativeDescription = ""
                                            End Try
                                        End If
                                        If tmpNode.Attributes("ALTERNATIVEDESCRABACOPOSITION") IsNot Nothing Then
                                            Try
                                                dummyRowCA.AlternativeDescriptionPosition = (tmpNode.Attributes("ALTERNATIVEDESCRABACOPOSITION").Value)
                                            Catch ex As Exception
                                                dummyRowCA.AlternativeDescriptionPosition = "-1"
                                            End Try
                                        End If


                                        dummyRow.hasCA = True
                                        returnConfiguratorInput.AddQAInputRow(dummyRowCA)


                                    Case "TE"
                                        dummyRowTE = returnConfiguratorExternal.NewQAExternalReferenceRow
                                        dummyRowTE.number = returnConfiguratorExternal.Count
                                        dummyRowTE.dtRefNumber = dummyRow.number
                                        dummyRowTE.qaId = dummyRow.id
                                        dummyRowTE.code = tmpNode.Attributes("CODE").Value
                                        If tmpNode.Attributes("SHOWROWNUMBER") IsNot Nothing Then
                                            dummyRowTE.showRowNumber = tmpNode.Attributes("SHOWROWNUMBER").Value
                                        End If
                                        If tmpNode.Attributes("COLUMNS") IsNot Nothing Then
                                            dummyRowTE.columnsView = tmpNode.Attributes("COLUMNS").Value
                                        End If
                                        If tmpNode.Attributes("OUTPUT") IsNot Nothing Then
                                            dummyRowTE.columnOutput = tmpNode.Attributes("OUTPUT").Value
                                        End If

                                        If tmpNode.Attributes("ID") IsNot Nothing Then
                                            dummyRowTE.ID = tmpNode.Attributes("ID").Value
                                        End If

                                        If tmpNode.Attributes("GRIGLIAAUTOMATICA") IsNot Nothing Then
                                            Try
                                                dummyRowTE.grigliaAutomatica = Boolean.Parse(tmpNode.Attributes("GRIGLIAAUTOMATICA").Value)
                                            Catch ex As Exception
                                                dummyRowTE.grigliaAutomatica = False
                                            End Try
                                        End If

                                        dummyRowTE.AlternativeDescription = ""
                                        dummyRowTE.AlternativeDescriptionPosition = "-1"
                                        If tmpNode.Attributes("ALTERNATIVEDESCRABACO") IsNot Nothing Then
                                            Try
                                                dummyRowTE.AlternativeDescription = (tmpNode.Attributes("ALTERNATIVEDESCRABACO").Value)
                                            Catch ex As Exception
                                                dummyRowTE.AlternativeDescription = ""
                                            End Try
                                        End If
                                        If tmpNode.Attributes("ALTERNATIVEDESCRABACOPOSITION") IsNot Nothing Then
                                            Try
                                                dummyRowTE.AlternativeDescriptionPosition = (tmpNode.Attributes("ALTERNATIVEDESCRABACOPOSITION").Value)
                                            Catch ex As Exception
                                                dummyRowTE.AlternativeDescriptionPosition = "-1"
                                            End Try
                                        End If


                                        dummyRowTE.selectedValue = ""
                                        dummyRow.hasTE = True
                                        Dim myExternalFilter As XmlNodeList
                                        If tmpNode.InnerXml.ToLower.Contains("ExternalTableFilter".ToLower) Then
                                            For Each childNode As XmlNode In tmpNode.ChildNodes
                                                If childNode.Name.ToLower.Equals("ExternalTableFilter".ToLower) Then
                                                    Try
                                                        dummyRowTE.columnNameFilter &= childNode.Attributes("ColumnName").Value & ";"
                                                    Catch ex As Exception
                                                        dummyRowTE.columnNameFilter &= ";"
                                                    End Try
                                                    Try
                                                        dummyRowTE.columnValueFilter &= childNode.Attributes("FilterValue").Value & ";"
                                                    Catch ex As Exception
                                                        dummyRowTE.columnValueFilter &= ";"
                                                    End Try
                                                    Try
                                                        dummyRowTE.columnOperatorFilter &= childNode.Attributes("FilterOperator").Value & ";"
                                                    Catch ex As Exception
                                                        dummyRowTE.columnOperatorFilter &= ";"
                                                    End Try
                                                End If
                                            Next

                                        End If

                                        returnConfiguratorExternal.AddQAExternalReferenceRow(dummyRowTE)
                                End Select
                            End If
                        Next
                    End If
                End If

                If givenXmlNode.SelectSingleNode("Conditions") IsNot Nothing Then
                    '<condition id="PC0202.1" type="ED" refOption="PC0102 Sviluppo" refOptionRelation="UGUALE" refOperator="OR" />
                    For Each tmpNode In givenXmlNode.SelectSingleNode("Conditions").ChildNodes
                        dummyRowAMM = returnConfiguratorConditions.NewQAConditionsRow
                        dummyRowAMM.number = returnConfiguratorConditions.Count
                        dummyRowAMM.dtRefNumber = dummyRow.number
                        dummyRowAMM.qaId = dummyRow.id
                        dummyRowAMM.sequence = tmpNode.Attributes("ID").Value
                        dummyRowAMM.refCode = tmpNode.Attributes("REFOPTION").Value
                        dummyRowAMM.type = tmpNode.Attributes("TYPE").Value
                        dummyRowAMM.refOptionRelation = tmpNode.Attributes("REFOPTIONRELATION").Value
                        dummyRowAMM.refOperator = tmpNode.Attributes("REFOPERATOR").Value
                        dummyRowAMM.minVal = ""
                        If tmpNode.Attributes("MINVAL") IsNot Nothing Then
                            dummyRowAMM.minVal = tmpNode.Attributes("MINVAL").Value
                        End If

                        dummyRowAMM.maxVal = ""
                        If tmpNode.Attributes("MAXVAL") IsNot Nothing Then
                            dummyRowAMM.maxVal = tmpNode.Attributes("MAXVAL").Value
                        End If

                        dummyRowAMM.value = ""
                        If tmpNode.Attributes("VALUE") IsNot Nothing Then
                            dummyRowAMM.value = tmpNode.Attributes("VALUE").Value
                        End If


                        returnConfiguratorConditions.AddQAConditionsRow(dummyRowAMM)
                    Next

                End If
                If givenXmlNode.SelectSingleNode("Question") IsNot Nothing Or givenXmlNode.SelectSingleNode("Answers") IsNot Nothing Then
                    dummyRow.hasChild = True
                Else
                    dummyRow.hasChild = False
                End If
                ' io.WriteLine(String.Format("{0}-{1}-{2}", dummyRow.level, dummyRow.parentid, dummyRow.id))
                returnConfiguratorQAData.AddQAOptionRow(dummyRow)
                If givenXmlNode.ChildNodes IsNot Nothing Then
                    For Each tmpNode In givenXmlNode.ChildNodes
                        FillDataByXml("" & dummyRow.id, tmpNode, givenLevel + 1, dummyRow.path)
                    Next
                End If

            End If
        End If
    End Sub
#End Region


#Region "XML RULE FILES"
    Public Shared Function GetRule(ByVal givenPath As String, ByVal givenRuleName As String)
        If System.IO.File.Exists(givenPath) Then
            givenPath = System.IO.Path.GetDirectoryName(givenPath)
        End If
        Dim myConfigRulePath As String = IO.Path.Combine(givenPath, givenRuleName & ".vb")
        'AppDomain.CurrentDomain.BaseDirectory & "\Dialogs\Rule\" & givenRuleName & ".ePLMS"
        Dim myIoReader As IO.StreamReader
        Dim code As String = ""
        If System.IO.File.Exists(myConfigRulePath) Then
            myIoReader = New IO.StreamReader(myConfigRulePath)
            While (Not myIoReader.EndOfStream)
                code &= myIoReader.ReadLine & vbCrLf
            End While
            myIoReader.Close()
        End If
        myIoReader = Nothing
        Return code

    End Function


    Public Shared Function GetTEValue(givenCode As String, givenFilter As String) As String
        Dim myConfigTEPath As String = AppDomain.CurrentDomain.BaseDirectory & "\Dialogs\externalTable\" & givenCode & ".xml"
        Dim myTable As DataTable
        Dim myDataSet As DataSet
        Dim code As String = "-EXIT"
        Dim mySelection() As DataRow
        If System.IO.File.Exists(myConfigTEPath) Then
            myDataSet = New DataSet
            myDataSet.ReadXml(myConfigTEPath)
            myTable = myDataSet.Tables(0)
            If myTable IsNot Nothing Then
                Dim query As String = givenFilter
                If query = "" Then
                    query = "*"
                End If
                mySelection = myTable.Select(query)
                If mySelection.Length = 1 Then
                    code = mySelection(0)(myTable.TableName.Split("-")(1))
                End If
               
            End If
        End If

        Return code

    End Function

    Public Shared Function GetTEDataTable(givenCode As String, filterColNames() As String, filterOperators() As String, filterValues() As String, givenColumnsToShow As String, givenMaxRow As Integer) As DataTable


        Dim myService As New DialogAdminService.DialogAdminService()
        Dim myContainer As New System.Net.CookieContainer
        myService.Url = System.Configuration.ConfigurationSettings.AppSettings("eConfigurationService")
        myService.CookieContainer = myContainer

        Dim message As String = ""
        Dim myTable As Data.DataTable
        If myService.login("eplms", "eplms", message) = 0 Then
            myTable = myService.GetExternalTable(givenCode, filterColNames, filterOperators, filterValues, givenColumnsToShow, givenMaxRow, message)
            myService.logout()
        End If

        Return myTable

    End Function
#End Region

End Class


