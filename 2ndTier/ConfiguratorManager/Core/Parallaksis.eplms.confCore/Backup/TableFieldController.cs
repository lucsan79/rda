using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public class TableFieldController : XtreeNodeController
	{
		protected TableFieldDef tableFieldDef;
		protected TableDef tableDef;

		public override string Name
		{
			get { return tableFieldDef.Name; }
			set { tableFieldDef.Name = value; }
		}

		public TableFieldDef TableFieldDef
		{
			get { return tableFieldDef; }
		}

		public override object Item
		{
			get { return tableFieldDef; }
		}

		public TableFieldController()
		{
		}

		public TableFieldController(TableFieldDef tableFieldDef)
		{
			this.tableFieldDef = tableFieldDef;
		}

		public override int Index(object item)
		{
			throw new XmlTreeException("Calling Index for TableFieldController is not permitted.");
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag)
		{
			tableDef = ((TableController)parentInstance).TableDef;
			tableFieldDef = new TableFieldDef();
			tableDef.Fields.Add(tableFieldDef);
			return true;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
			tableDef = ((TableController)parentInstance).TableDef;
			tableDef.Fields.Remove(tableFieldDef);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
			tableDef = ((TableController)parentInstance).TableDef;
			tableDef.Fields.Remove(tableFieldDef);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
			tableDef = ((TableController)parentInstance).TableDef;
			tableDef.Fields.Insert(idx, tableFieldDef);
		}

		public override void Select(TreeNode tn)
		{
			Program.Properties.SelectedObject = tableFieldDef;
		}
	}
}
