using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public class SchemaController : XtreeNodeController
	{
		protected SchemaDef schemaDef;
		
		/// <summary>
		/// Gets/sets schemaService
		/// </summary>
		public SchemaDef SchemaDef
		{
			get { return schemaDef; }
			set { schemaDef = value; }
		}

		public override string Name
		{
			get { return "Schema"; }
			set { ;}
		}

		public override object Item
		{
			get { return schemaDef; }
		}

		public SchemaController()
		{
		}

		public SchemaController(SchemaDef schemaDef)
		{
			this.schemaDef = schemaDef;
		}

		public override int Index(object item)
		{
			throw new XmlTreeException("Calling Index for SchemaController is not permitted.");
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag)
		{
			return false;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
			return false;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
			throw new XmlTreeException("Calling AutoDeleteNode for SchemaController is not permitted.");
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
			throw new XmlTreeException("Calling InsertNode for SchemaController is not permitted.");
		}

		public override void Select(TreeNode tn)
		{
			Program.Properties.SelectedObject = schemaDef;
		}

		public override void MoveTo(IXtreeNode newParent, IXtreeNode oldParent, int idx)
		{
		}
	}
}
