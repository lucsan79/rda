using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using Microsoft.Win32;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms;
using Clifton.Windows.Forms.XmlTree;

using MyXaml.Core;

namespace XTreeDemo
{
	public class Program
	{
		[MyXamlAutoInitialize]
		XTree sdTree = null;

		[MyXamlAutoInitialize]
		PropertyGrid pgProperties = null;

		protected Form form;
		protected string caption;
		protected string schemaFilename;
		protected SchemaDef schemaDef;
		protected TreeNode rootNode;

		protected static PropertyGrid pgProps;

		public static PropertyGrid Properties
		{
			get { return pgProps; }
		}

		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			new Program();
		}

		public Program()
		{
			Parser.AddExtender("MyXaml.WinForms", "MyXaml.WinForms", "WinFormExtender");
			Parser p = new Parser();
			p.AddReference("App", this);
			form = (Form)p.Instantiate("schemaEditor.myxaml", "*");
			p.InitializeFields(this);
			pgProps = pgProperties;
			schemaFilename = String.Empty;
			caption = form.Text;
			form.Shown += new EventHandler(OnShown);
			Application.Run(form);
		}

		protected void OnShown(object sender, EventArgs e)
		{
			CreateRootNode();
		}

		protected void CreateRootNode()
		{
			schemaDef = new SchemaDef();
			SchemaController sc = new SchemaController(schemaDef);
			rootNode = sdTree.AddNode(sc, null);
			pgProperties.SelectedObject = schemaDef;
			sdTree.SelectedNode = sdTree.Nodes[0];
		}

		protected void OnNew(object sender, EventArgs e)
		{
			schemaFilename = String.Empty;
			ClearAll();
			pgProperties.SelectedObject = schemaDef;
			sdTree.SelectedNode = sdTree.Nodes[0];
		}

		protected void OnOpen(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.RestoreDirectory = true;
			ofd.CheckFileExists = true;
			ofd.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
			ofd.Title = "Load Schema";
			DialogResult res = ofd.ShowDialog();

			if (res == DialogResult.OK)
			{
				schemaFilename = ofd.FileName;
				ClearAll();
				Load();
			}
		}

		protected void OnSave(object sender, EventArgs e)
		{
			if (schemaFilename == String.Empty)
			{
				OnSaveAs(sender, e);
			}
			else
			{
				Save();
			}
		}

		protected void OnSaveAs(object sender, EventArgs e)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			sfd.OverwritePrompt = true;
			sfd.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
			sfd.Title = "Save Schema";
			DialogResult res = sfd.ShowDialog();

			if (res == DialogResult.OK)
			{
				schemaFilename = sfd.FileName;
				Save();
				UpdateCaption();
			}
		}

		protected void OnExit(object sender, EventArgs e)
		{
			Application.Exit();
		}

		/// <summary>
		/// Updates the XML node with the name set in the property grid.
		/// </summary>
		protected void OnPropertyValueChanged(object sender, PropertyValueChangedEventArgs e)
		{
			if (e.ChangedItem.Label == "Name")
			{
				sdTree.SelectedNode.Text = e.ChangedItem.Value.ToString();
			}
		}

		protected void UpdateCaption()
		{
			form.Text = caption + " - " + Path.GetFileName(schemaFilename);
		}

		protected void ClearAll()
		{
			sdTree.Clear();
			UpdateCaption();
			CreateRootNode();
		}

		protected void Load()
		{
			XmlSerializer xs = new XmlSerializer(typeof(SchemaDef));
			StreamReader sr = new StreamReader(schemaFilename);
			schemaDef = (SchemaDef)xs.Deserialize(sr);
			((SchemaController)((NodeInstance)rootNode.Tag).Instance).SchemaDef = schemaDef;
			sr.Close();
			PopulateTree();
		}

		protected void Save()
		{
			XmlSerializer xs = new XmlSerializer(typeof(SchemaDef));
			TextWriter tw = new StreamWriter(schemaFilename);
			xs.Serialize(tw, schemaDef);
			tw.Close();
		}

		protected void PopulateTree()
		{
			IXtreeNode controller;
			TreeNode tn;
			sdTree.SuspendLayout();

			foreach (TableDef tableDef in schemaDef.Tables)
			{
				controller = new TableController(tableDef);
				tn = sdTree.AddNode(controller, rootNode);

				foreach (TableFieldDef tfd in tableDef.Fields)
				{
					controller = new TableFieldController(tfd);
					sdTree.AddNode(controller, tn);
				}
			}

			sdTree.CollapseAll();
			sdTree.Nodes[0].Expand();
			sdTree.ResumeLayout();

			pgProperties.SelectedObject = schemaDef;
			sdTree.SelectedNode = sdTree.Nodes[0];
		}
	}
}
