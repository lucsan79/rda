using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public class TableController : XtreeNodeController
	{
		protected TableDef tableDef;
		protected SchemaDef schemaDef;

		public override string Name
		{
			get { return tableDef.Name; }
			set {tableDef.Name=value;}
		}

		public TableDef TableDef
		{
			get { return tableDef; }
		}

		public override object Item
		{
			get { return tableDef; }
		}

		public TableController()
		{
		}

		public TableController(TableDef tableDef)
		{
			this.tableDef = tableDef;
		}

		public override int Index(object item)
		{
			return tableDef.Fields.IndexOf(((TableFieldController)item).TableFieldDef);
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag)
		{
			schemaDef = ((SchemaController)parentInstance).SchemaDef;
			tableDef = new TableDef();
			schemaDef.Tables.Add(tableDef);
			return true;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
			schemaDef = ((SchemaController)parentInstance).SchemaDef;
			schemaDef.Tables.Remove(tableDef);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
			schemaDef = ((SchemaController)parentInstance).SchemaDef;
			schemaDef.Tables.Remove(tableDef);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
			schemaDef = ((SchemaController)parentInstance).SchemaDef;
			schemaDef.Tables.Insert(idx, tableDef);
		}

		public override void Select(TreeNode tn)
		{
			Program.Properties.SelectedObject = tableDef;
		}
	}
}
