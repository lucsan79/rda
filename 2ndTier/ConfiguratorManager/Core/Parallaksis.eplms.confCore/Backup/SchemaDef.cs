using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

/*
 * The SchemaDef is a collection of serialization classes that also support design-time
 * editing of the collections and properties, primarily to be used with the property grid.
 */

namespace XTreeDemo
{
	/// <summary>
	/// Supported field types, used by the schema to define table field.
	/// </summary>
	public enum FieldType
	{
		String,
		Integer,
		Real,
		Money,
		Boolean,
		Date,
		Time,
		Guid,
		DateTime,
	}

	/// <summary>
	/// Supports the declarative instantiation of the table/view schema definition.
	/// </summary>
	[DefaultProperty("Name")]
	public class SchemaDef
	{
		protected List<TableDef> tables;
		protected string name;
		
		/// <summary>
		/// Gets/sets the schema name.
		/// </summary>
		[Category("General")]
		[Description("The schema name.")]
		[DefaultValue("Schema")]
		[Browsable(true)]
		[XmlAttribute("Name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		/// <summary>
		/// Returns the collection of tables in the schema.
		/// </summary>
		[Browsable(false)]
		public List<TableDef> Tables
		{
			get { return tables; }
		}

		/// <summary>
		/// Constructor.  Initializes collections and default name.
		/// </summary>
		public SchemaDef()
		{
			tables = new List<TableDef>();
			name = "Schema";
		}

		public TableDef GetTable(string name)
		{
			TableDef ret=null;

			foreach (TableDef td in tables)
			{
				if (td.Name == name)
				{
					ret = td;
					break;
				}
			}

			return ret;
		}

		/// <summary>
		/// Returns a string array of all the table names in the schema.
		/// </summary>
		/// <returns></returns>
		public string[] GetTableNames()
		{
			string[] strTables = new string[tables.Count];

			for (int i = 0; i < tables.Count; i++)
			{
				strTables[i] = tables[i].Name;
			}

			return strTables;
		}

		/// <summary>
		/// Returns a string array of all the field names for the specified table.
		/// </summary>
		/// <param name="tableName"></param>
		/// <returns></returns>
		public string[] GetFieldNames(string tableName)
		{
			string[] ret = null;

			foreach (TableDef table in tables)
			{
				if (table.Name == tableName)
				{
					ret = new string[table.Fields.Count];

					for (int i = 0; i < table.Fields.Count; i++)
					{
						ret[i] = table.Fields[i].Name;	
					}

					break;
				}
			}

			return ret;
		}
	}

	/// <summary>
	/// Supports the declarative instantiation of the table schema definition.
	/// </summary>
	[DefaultProperty("Name")]
	public class TableDef
	{
		protected string name;
		protected string comments;
		protected List<TableFieldDef> fields;
		
		/// <summary>
		/// Returns the collection of table fields in the table.
		/// </summary>
		[Browsable(false)]
		public List<TableFieldDef> Fields
		{
			get { return fields; }
		}
		
		/// <summary>
		/// Gets/sets the table name.
		/// </summary>
		[Category("General")]
		[Description("The table name, which must correlate to the persistent store name.")]
		[DefaultValue("Table")]
		[Browsable(true)]
		[XmlAttribute("Name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		/// <summary>
		/// Gets/sets the table comments.
		/// </summary>
		[Category("General")]
		[Description("Any comments about this table useful to the schema designers.")]
		[DefaultValue("")]
		[Browsable(true)]
		[XmlAttribute("Comments")]
		public string Comments
		{
			get { return comments; }
			set { comments = value; }
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public TableDef()
		{
			fields = new List<TableFieldDef>();
			name = "Table";
			comments = String.Empty;
		}
	}

	/// <summary>
	/// Suports the declarative instantiation of a table field definition.
	/// </summary>
	[DefaultProperty("Name")]
	public class TableFieldDef
	{
		protected string name;
		protected bool isPK;
		protected string pkTable;
		protected string pkField;
		protected FieldType dataType;
		protected string comments;
		protected bool allowNull;
		protected bool isHashed;
		protected int length;
		protected string defaultValue;
		protected string regexMatch;
		protected string minVal;
		protected string maxVal;

		/// <summary>
		/// Gets/sets maxVal
		/// </summary>
		[Category("Validation")]
		[Description("Enter the allowable maximum value.")]
		[DefaultValue("")]
		[Browsable(true)]
		[XmlAttribute("MaxVal")]
		public string MaxVal
		{
			get { return maxVal; }
			set { maxVal = value; }
		}

		/// <summary>
		/// Gets/sets minVal
		/// </summary>
		[Category("Validation")]
		[Description("Enter the allowable minimum value.")]
		[DefaultValue("")]
		[Browsable(true)]
		[XmlAttribute("MinVal")]
		public string MinVal
		{
			get { return minVal; }
			set { minVal = value; }
		}

		/// <summary>
		/// Gets/sets regexMatch
		/// </summary>
		[Category("Validation")]
		[Description("Enter a regex expression to match the input data against.")]
		[DefaultValue("")]
		[Browsable(true)]
		[XmlAttribute("RegexMatch")]
		public string RegexMatch
		{
			get { return regexMatch; }
			set { regexMatch = value; }
		}
		
		/// <summary>
		/// Gets/sets the field's default value.
		/// </summary>
		[Category("Data")]
		[Description("The default value for the field, used to initialize a new row field.")]
		[DefaultValue("")]
		[Browsable(true)]
		[XmlAttribute("DefaultValue")]
		public string DefaultValue
		{
			get { return defaultValue; }
			set { defaultValue = value; }
		}
		
		/// <summary>
		/// Gets/sets the field's data length.  Only valid with certain data types.
		/// </summary>
		[Category("Data")]
		[Description("If applicable, the length of the field.")]
		[Browsable(true)]
		[XmlAttribute("Length")]
		public int Length
		{
			get { return length; }
			set { length = value; }
		}

		/// <summary>
		/// Gets/sets the field's comments.
		/// </summary>
		[Category("General")]
		[Description("Comments about this field useful to the schema designers.")]
		[DefaultValue("")]
		[Browsable(true)]
		[XmlAttribute("Comments")]
		public string Comments
		{
			get { return comments; }
			set { comments = value; }
		}
		
		/// <summary>
		/// Gets/sets the flag indicating that the field is hashed.  Hashed fields
		/// are persisted using a hash value.  Queries also convert the field value to a hash value.
		/// </summary>
		[Category("Data")]
		[Description("If true, the value is automatically hashed before being persisted.")]
		[DefaultValue(false)]
		[Browsable(true)]
		[XmlAttribute("IsHashed")]
		public bool IsHashed
		{
			get { return isHashed; }
			set { isHashed = value; }
		}
		
		/// <summary>
		/// Gets/sets whether the field allows null values.
		/// </summary>
		[Category("Validation")]
		[Description("If true, DBNull is a permissable value.")]
		[DefaultValue(false)]
		[Browsable(true)]
		[XmlAttribute("AllowNull")]
		public bool AllowNull
		{
			get { return allowNull; }
			set { allowNull = value; }
		}
		
		/// <summary>
		/// Gets/sets the field type.
		/// </summary>
		[Category("Data")]
		[Description("The data type for this field.")]
		[DefaultValue(FieldType.String)]
		[Browsable(true)]
		[XmlAttribute("DataType")]
		public FieldType DataType
		{
			get { return dataType; }
			set { dataType = value; }
		}
		
		/// <summary>
		/// Gets/sets the table that this field references as an FK relationship.
		/// </summary>
		[Category("References")]
		[Description("The primary key table that this field references and to which this field is a foreign key.")]
		[DefaultValue("")]
		[Browsable(true)]
		[XmlAttribute("PKTable")]
		public string PKTable
		{
			get { return pkTable; }
			set { pkTable = value; }
		}

		/// <summary>
		/// Gets/sets the field that this field references as an FK relationship.
		/// </summary>
		[Category("References")]
		[Description("The primary key field that this field references and to which this field is a foreign key.")]
		[DefaultValue("")]
		[Browsable(true)]
		[XmlAttribute("PKField")]
		public string PKField
		{
			get { return pkField; }
			set { pkField = value; }
		}
		
		/// <summary>
		/// Gets/sets a flag indicating that this field is a primary key.
		/// </summary>
		[Category("Data")]
		[Description("If true, this field is a primary key.")]
		[DefaultValue(false)]
		[Browsable(true)]
		[XmlAttribute("IsPK")]
		public bool IsPK
		{
			get { return isPK; }
			set { isPK = value; }
		}

		/// <summary>
		/// Gets/sets field name.  This name corresponds to the persistent store name.
		/// </summary>
		[Category("General")]
		[Description("The name of this field, which must correlate to the persistent store name.")]
		[DefaultValue("Field")]
		[Browsable(true)]
		[XmlAttribute("Name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public TableFieldDef()
		{
			comments = String.Empty;
			dataType = FieldType.String;
			pkTable = String.Empty;
			pkField = String.Empty;
			name = "Field";
			defaultValue = String.Empty;
			minVal = String.Empty;
			maxVal = String.Empty;
			regexMatch = String.Empty;
		}
	}
}
