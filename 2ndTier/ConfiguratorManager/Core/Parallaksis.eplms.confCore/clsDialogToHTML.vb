﻿Imports Parallaksis.M40.DataModel
Imports Parallaksis.M40.DataModel.clsDataModelHelper
Imports System.Xml

Public Class DialogToHTML

    ''' <summary>
    ''' Used for html in iteraction mode
    ''' </summary>
    ''' <param name="givenDialog"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Public Shared Function DialogToHtml1(givenDialog As Dialogs, givenToolTipFile As String) As String
        Dim retValue As String = ""
        Dim dummyQuestion As DialogsQuestion
        Dim dummyStringBuilder As Text.StringBuilder = Nothing

        If givenDialog IsNot Nothing Then
            If givenDialog.Questions IsNot Nothing Then
                dummyStringBuilder = New Text.StringBuilder
                For Each dummyQuestion In givenDialog.Questions
                    QuestionHeader1(dummyStringBuilder, dummyQuestion)
                    QuestionBody1(dummyStringBuilder, dummyQuestion, givenToolTipFile)
                    QuestionFooter1(dummyStringBuilder, dummyQuestion)

                Next
                retValue = dummyStringBuilder.ToString
            End If
        End If
        Return retValue


    End Function

    ''' <summary>
    ''' Used for html in iteraction mode
    ''' </summary>
    ''' <param name="givenDialog"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DialogToHtmlFiltered1(givenDialog As Dialogs, indexToStop As Integer, givenToolTipFile As String) As String
        Dim retValue As String = ""
        Dim dummyQuestion As DialogsQuestion
        Dim dummyStringBuilder As Text.StringBuilder = Nothing

        Try


            If givenDialog IsNot Nothing Then
                If givenDialog.Questions IsNot Nothing Then
                    dummyStringBuilder = New Text.StringBuilder
                    For Each dummyQuestion In givenDialog.Questions

                        If dummyQuestion.sequenceID < indexToStop Then
                            QuestionHeader1(dummyStringBuilder, dummyQuestion)
                            QuestionBody1(dummyStringBuilder, dummyQuestion, givenToolTipFile)
                            QuestionFooter1(dummyStringBuilder, dummyQuestion)
                        Else
                            '' ''If dummyQuestion.Answers IsNot Nothing Then
                            '' ''    For Each ans As DialogsAnswer In dummyQuestion.Answers
                            '' ''        If ans.isSelected Then
                            '' ''            QuestionHeader1(dummyStringBuilder, dummyQuestion)
                            '' ''            QuestionBody1(dummyStringBuilder, dummyQuestion)
                            '' ''            QuestionFooter1(dummyStringBuilder, dummyQuestion)
                            '' ''            Exit For
                            '' ''        End If
                            '' ''    Next
                            '' ''Else
                            '' ''    QuestionHeader1(dummyStringBuilder, dummyQuestion)
                            '' ''    QuestionBody1(dummyStringBuilder, dummyQuestion)
                            '' ''    QuestionFooter1(dummyStringBuilder, dummyQuestion)
                            '' ''    Exit For
                            '' ''    'ElseIf dummyQuestion.FeatureToAcquire IsNot Nothing Then
                            '' ''    '    For Each ans As DialogsInput In dummyQuestion.FeatureToAcquire
                            '' ''    '        If ans..isSelected Then
                            '' ''    '            QuestionHeader1(dummyStringBuilder, dummyQuestion)
                            '' ''    '            QuestionBody1(dummyStringBuilder, dummyQuestion)
                            '' ''    '            QuestionFooter1(dummyStringBuilder, dummyQuestion)
                            '' ''    '            Exit For
                            '' ''    '        End If
                            '' ''    '    Next
                            '' ''End If


                        End If

                    Next
                    retValue = dummyStringBuilder.ToString
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.StackTrace)
        End Try
        Return retValue


    End Function

    Public Shared Function DialogToHtmlFiltered2(givenDialog As Dialogs, indexToStart As Integer, givenToolTipFile As String) As String
        Dim retValue As String = ""
        Dim dummyQuestion As DialogsQuestion
        Dim dummyStringBuilder As Text.StringBuilder = Nothing

        If givenDialog IsNot Nothing Then
            If givenDialog.Questions IsNot Nothing Then
                dummyStringBuilder = New Text.StringBuilder
                For Each dummyQuestion In givenDialog.Questions
                    If dummyQuestion.sequenceID > indexToStart Then
                        QuestionHeader1(dummyStringBuilder, dummyQuestion)
                        QuestionBody1(dummyStringBuilder, dummyQuestion, givenToolTipFile)
                        QuestionFooter1(dummyStringBuilder, dummyQuestion)
                    End If

                Next
                retValue = dummyStringBuilder.ToString
            End If
        End If
        Return retValue


    End Function
    ''' <summary>
    ''' Used for html in summary mode
    ''' </summary>
    ''' <param name="givenDialog"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DialogToHtml2(givenDialog As Dialogs) As String
        Dim retValue As String = ""
        Dim dummyStringBuilder As Text.StringBuilder = Nothing
        Dim dummyQuestion As DialogsQuestion
        If givenDialog IsNot Nothing Then
            If givenDialog.Questions IsNot Nothing Then
                dummyStringBuilder = New Text.StringBuilder
                For Each dummyQuestion In givenDialog.Questions
                    QuestionHeader2(dummyStringBuilder, dummyQuestion)
                    QuestionBody2(dummyStringBuilder, dummyQuestion)
                    QuestionFooter2(dummyStringBuilder, dummyQuestion)

                Next
                retValue = dummyStringBuilder.ToString
            End If
        End If
        Return retValue
    End Function

    '' ''Public Function GetSummary() As String

    '' ''Dim dummyQuestion As DialogsQuestion
    '' ''Dim dummyQuestionAnswers As DialogsQuestionAnswer
    '' ''Dim retValue As String = ""
    ' '' ''Dim lastQuestion As DialogsQuestion
    ' '' ''Dim lastAnswer As DialogsQuestionAnswer
    '' ''Dim dummyFeaturesTE As DialogsFeature
    '' ''    Try


    '' ''        If Questions IsNot Nothing Then
    '' ''            For Each dummyQuestion In Questions
    '' ''                If dummyQuestionAnswers IsNot Nothing Then
    '' ''                    If dummyQuestionAnswers.id <> dummyQuestion.id Then
    '' ''                        retValue &= "<br/>"
    '' ''                        retValue &= String.Format("<b>{0}</b>:", dummyQuestion.title)
    '' ''                    Else

    '' ''                        retValue &= " "
    '' ''                    End If
    '' ''                Else
    '' ''                    retValue &= String.Format("<b>{0}</b>:", dummyQuestion.title)
    '' ''                End If
    '' ''                For Each dummyQuestionAnswers In dummyQuestion.Answers
    '' ''                    dummyFeaturesTE = Nothing
    '' ''                    If dummyQuestionAnswers.Feature IsNot Nothing Then
    '' ''                        For Each dummyFeaturesTE In dummyQuestionAnswers.Feature
    '' ''                            If dummyFeaturesTE.type = DialogsFeature.FeatureType.TE Then
    '' ''                                Exit For
    '' ''                            Else
    '' ''                                dummyFeaturesTE = Nothing
    '' ''                            End If
    '' ''                        Next
    '' ''                    End If
    '' ''                    If dummyQuestionAnswers.IsSelected Then
    '' ''                        retValue &= String.Format("<i>{0}</i>", dummyQuestionAnswers.title)
    '' ''                        If dummyFeaturesTE IsNot Nothing Then
    '' ''                            retValue &= String.Format("&nbsp;<font color='#990033' style='font-weight: bold'>{0}</font>", dummyQuestionAnswers.externalReferenceValue.ToUpper)
    '' ''                        End If

    '' ''                    End If

    '' ''                Next

    '' ''            Next

    '' ''        End If

    '' ''    Catch ex As Exception
    '' ''        ErrorInDialog.Add(ex.Message)
    '' ''        retValue = False
    '' ''    End Try
    '' ''    Return retValue

    '' ''End Function


    Private Shared Sub QuestionHeader1(ByRef givenSTRBuildier As Text.StringBuilder, givenQuestion As DialogsQuestion)

        If givenQuestion.isProcessed Then
            givenSTRBuildier.Append(String.Format("<div><p class='pProcessed'>{0}</p>", givenQuestion.Title))
        Else
            givenSTRBuildier.Append(String.Format("<div><p>{0}</p>", givenQuestion.Title))
        End If


    End Sub


    Private Shared Sub QuestionBody1(ByRef givenSTRBuildier As Text.StringBuilder, givenQuestion As DialogsQuestion, givenToolTipFile As String)
        Dim dummyAnswer As DialogsAnswer = Nothing
        Dim dummyInput As DialogsInput = Nothing
        Dim tmpValidatationFunction As String
        Dim tmplbl As String = ""
        Dim tmptxt As String
        Dim givenInputs As List(Of DialogsInput)
        Dim givenExternalTable As List(Of DialogsExternalTable)
        Dim idToValidate As String = ""
        Dim idManadatory As String = ""
        Dim tmpEditFunction As String
        Dim uniqueIdCA As String = ""
        Dim validatorid As String = ""
        Dim btnTE As String = ""

        Select Case givenQuestion.Type
            Case DialogsQuestion.QuestionType.AutomaticFeaturesCompilation
                If givenQuestion.Features IsNot Nothing Then
                    Dim filled As Boolean = False

                    givenSTRBuildier.Append("<ul class='ulCP'>")
                    For Each feature As DialogsFeature In givenQuestion.Features
                        If feature.Value <> "" AndAlso IsNumeric(feature.Value) Then
                            Dim codieK As String = feature.Code & " (" & feature.Description & ") = " & feature.Value
                            givenSTRBuildier.Append(String.Format("<li class='liCP'>{0}</li>", codieK.Substring(2)))
                            filled = True
                        End If
                    Next
                    If givenQuestion.Features.Count = 0 Or filled = False Then
                        givenSTRBuildier.Append(String.Format("<li class='liErrorCP'>Nessun codice K individuato o valorizzato correttamente</li>"))
                    End If
                    givenSTRBuildier.Append("<ul>")
                End If

            Case DialogsQuestion.QuestionType.SingleOptionSelection

                If givenQuestion.Answers IsNot Nothing Then
                    givenSTRBuildier.Append("<ul>")
                    For Each dummyAnswer In givenQuestion.Answers
                        Dim defaultIcon As String = "<img border='0' class='imgDefault' title='default selection' />"
                        Dim myToolTip As String = GetToolTip(givenToolTipFile, givenQuestion.Title, dummyAnswer.title)
                        If myToolTip <> "" Then
                            myToolTip = "&nbsp;" & myToolTip
                        End If
                        If dummyAnswer.isDefault Then

                            givenSTRBuildier.Append(String.Format("<li id='{0}' parent='{1}' {2} {4} selid='{5}'><table class='liTB'><tr><td>{3}</td><td class='td-icon-help'>{6}</td></tr></table></li>",
                                                                  dummyAnswer.id,
                                                                  dummyAnswer.idParent,
                                                                  IIf(dummyAnswer.isSelected, "class='selected'", "onclick='liSelection(this);'"),
                                                                  dummyAnswer.title & "&nbsp;" & defaultIcon,
                                                                  IIf(givenQuestion.isProcessed, "class='liProcessed'", ""),
                                                                  dummyAnswer.sequenceId,
                                                                  myToolTip))
                        Else
                            givenSTRBuildier.Append(String.Format("<li id='{0}' parent='{1}' {2} {4} selid='{5}'><table class='liTB'><tr><td>{3}</td><td class='td-icon-help'>{6}</td></tr></table></li>",
                                                                  dummyAnswer.id,
                                                                  dummyAnswer.idParent,
                                                                  IIf(dummyAnswer.isSelected, "class='selected'", "onclick='liSelection(this);'"),
                                                                  dummyAnswer.title,
                                                                  IIf(givenQuestion.isProcessed, "class='liProcessed'", ""),
                                                                  dummyAnswer.sequenceId,
                                                                  myToolTip))
                        End If


                    Next
                    givenSTRBuildier.Append("</ul>")
                End If
            Case DialogsQuestion.QuestionType.FeaturesToAcquire
                givenInputs = givenQuestion.FeatureToAcquire
                If givenInputs IsNot Nothing Then
                    givenSTRBuildier.Append("<table class='TableFA'>")
                    For Each input As DialogsInput In givenInputs
                        uniqueIdCA = input.id & "." & input.CA_IdTable
                        If input.IsMandatory Then
                            idManadatory &= uniqueIdCA & ";"
                        End If
                        idToValidate &= uniqueIdCA & ";"
                        tmplbl = ""
                        If input.CA_Description.Trim = "" Then
                            tmplbl = input.title
                        End If
                        Dim dimension As Integer = 0
                        Dim styleW As String = ""
                        Try
                            dimension = Integer.Parse(input.CA_InputLength)
                        Catch ex As Exception
                            dimension = 100
                        End Try
                        If dimension > 100 And dimension < 200 Then
                            styleW = "class=""len100"""
                        ElseIf dimension >= 200 And dimension < 300 Then
                            styleW = "class=""len200"""
                        ElseIf dimension >= 300 Then
                            styleW = "class=""len300"""
                        End If
                        tmplbl &= input.CA_Description & IIf(input.IsMandatory, "<font color='red' title='IS MANDATORY' >*</font>", "")



                        Dim CA_UM As String = ""
                        Select Case input.CA_UM.ToString.ToUpper
                            Case "NA"
                                CA_UM = ""
                            Case "EURO"
                                CA_UM = "&nbsp;€"
                            Case "DOLLAR"
                                CA_UM = "&nbsp;$"
                            Case Else
                                CA_UM = "&nbsp;" & input.CA_UM.ToString
                        End Select

                        If dimension <= 100 Then


                            If Not input.IsMandatory And givenQuestion.isProcessed Then
                                tmptxt = String.Format("<input class='' id=""txtCA_{0}"" number=""{1}"" parent=""{2}"" type=""text"" value=""{3}"" descr=""{6}"" code=""{7}"" {4} selid='{8}' {9} maxlength='{10}' readonly='true'/>{5}", uniqueIdCA, input.sequenceId, input.idParent, input.CA_TextValue, "disabled='disabled'", CA_UM, input.CA_Description, input.CA_Code, input.sequenceId, styleW, dimension)
                            Else
                                tmptxt = String.Format("<input class='' id=""txtCA_{0}"" number=""{1}"" parent=""{2}"" type=""text"" value=""{3}"" descr=""{6}"" code=""{7}"" {4} selid='{8}' {9} maxlength='{10}' readonly='true'/>{5}", uniqueIdCA, input.sequenceId, input.idParent, input.CA_TextValue, IIf(input.CA_TextValue <> "", "disabled='disabled'", ""), CA_UM, input.CA_Description, input.CA_Code, input.sequenceId, styleW, dimension)
                            End If
                            If input.CA_ValueType = "D" Then
                                tmptxt = tmptxt.Replace("class=''", "class=""txtDate""")
                                Dim idRef As String = "txtCA_" & uniqueIdCA
                                Dim calendar As String = ""
                                If givenQuestion.isProcessed Then
                                    calendar = "&nbsp;<IMG height='16' title='Click Here to Pick up the date' class='imgCalendar' align='absMiddle' border='0' onclick=""popUpDate(this,'" & idRef & "','left');"" />"
                                Else
                                    calendar = "&nbsp;<IMG height='16' title='Click Here to Pick up the date' class='imgCalendar' align='absMiddle' border='0' onclick=""popUpDate(this,'" & idRef & "','right');"" />"
                                End If
                                tmptxt = tmptxt & calendar
                            ElseIf input.CA_ValueType = "BOOL" Then
                                tmptxt = tmptxt.Replace("class=''", "class=""txtBool""")
                                Dim idRef As String = "txtCA_" & uniqueIdCA
                                Dim calendar As String = ""
                                Dim myNewHTML As String = "<div style='display:none'>" & tmptxt & "</div>"


                                If Not input.IsMandatory And givenQuestion.isProcessed Then
                                    tmptxt = String.Format("<input id='txtCA_{0}' number='{1}' parent='{2}' type='checkbox' value='{3}' descr=""{6}"" code=""{7}"" {4} selid='{8}' {9} maxlength='{10}' checked />{5}", uniqueIdCA, input.sequenceId, input.idParent, input.CA_TextValue, "disabled='disabled'", CA_UM, input.CA_Description, input.CA_Code, input.sequenceId, styleW, dimension)
                                Else
                                    tmptxt = String.Format("<input id='txtCA_{0}' number='{1}' parent='{2}' type='checkbox' value='{3}' descr=""{6}"" code=""{7}""  {4} selid='{8}' {9} maxlength='{10}' checked />{5}", uniqueIdCA, input.sequenceId, input.idParent, input.CA_TextValue, IIf(input.CA_TextValue <> "", "disabled='disabled'", ""), CA_UM, input.CA_Description, input.CA_Code, input.sequenceId, styleW, dimension)
                                End If
                                If input.CA_TextValue = "" Or input.CA_TextValue = "false" Then
                                    tmptxt = tmptxt.Replace(" checked ", " ")
                                End If
                            ElseIf input.CA_ValueType = "CURRENCY" Then ' "EURO" Or input.CA_ValueType = "DOLLAR" Then
                                tmptxt = tmptxt.Replace("class=''", "class=""txtCurrency""")
                                Dim myRadio As String = ""
                                If input.CA_TextValue.Contains("$") Then
                                    tmptxt = tmptxt.Replace(" readonly='true'", " onfocus='setCurrency(this);'")
                                    myRadio = String.Format("<input type='radio' name='choices' value='€' onclick='changecurrency(""txtCA_{0}"",""€"")' /><img class='imgEuro' />", uniqueIdCA)
                                    myRadio &= String.Format("<input type='radio' name='choices' checked value='$' onclick='changecurrency(""txtCA_{0}"",""$"")' /><img class='imgDollar' />", uniqueIdCA)
                                Else
                                    ' tmptxt = tmptxt.Replace(" readonly='true'", " class='euroinput'")
                                    tmptxt = tmptxt.Replace(" readonly='true'", " onfocus='setCurrency(this);'")
                                    myRadio = String.Format("<input type='radio' name='choices' checked value='€' onclick='changecurrency(""txtCA_{0}"",""€"");'/><img class='imgEuro' />", uniqueIdCA)
                                    myRadio &= String.Format("<input type='radio' name='choices'  value='$' onclick='changecurrency(""txtCA_{0}"",""$"")'/><img class='imgDollar'  />", uniqueIdCA)

                                End If
                                tmptxt &= "&nbsp;" & myRadio

                            Else
                                If input.CA_ValueType = DialogsInput.CA_InputType.N.ToString("F") Then
                                    tmptxt = tmptxt.Replace("class=''", "class=""txtNumber""")
                                Else
                                    tmptxt = tmptxt.Replace("class=''", "")
                                End If

                                tmptxt = tmptxt.Replace(" readonly='true'", "")
                            End If

                        Else
                            If Not input.IsMandatory And givenQuestion.isProcessed Then
                                tmptxt = String.Format("<textarea id=""txtCA_{0}"" number=""{1}"" parent=""{2}"" type=""text"" value=""{3}"" descr=""{6}"" code=""{7}"" {4} selid='{8}' {9} maxlength='{10}'>{3}</textarea>{5}", uniqueIdCA, input.sequenceId, input.idParent, input.CA_TextValue, "disabled='disabled'", CA_UM, input.CA_Description, input.CA_Code, input.sequenceId, styleW, dimension)
                            Else
                                tmptxt = String.Format("<textarea id=""txtCA_{0}"" number=""{1}"" parent=""{2}"" type=""text"" value=""{3}"" descr=""{6}"" code=""{7}"" {4} selid='{8}' {9} maxlength='{10}'>{3}</textarea>{5}", uniqueIdCA, input.sequenceId, input.idParent, input.CA_TextValue, IIf(input.CA_TextValue <> "", "disabled='disabled'", ""), CA_UM, input.CA_Description, input.CA_Code, input.sequenceId, styleW, dimension)
                            End If

                        End If

                        Dim myToolTip As String = GetToolTip(givenToolTipFile, givenQuestion.Title, input.CA_Description)
                        If myToolTip <> "" Then
                            myToolTip = "&nbsp;" & myToolTip
                        End If
                        'tmptxt &= GetCARequiredField(input, "txtCA_" & uniqueIdCA)
                        givenSTRBuildier.Append("<tr>")
                        'givenSTRBuildier.Append(String.Format("<td  style='white-space: nowrap;width:20%'>{0}</td style='width:80%'><td><table class='liTB'><tr><td>{1}</td><td class='td-icon-help'>{2}</td></tr></table></td>", tmplbl, tmptxt, myToolTip))
                        givenSTRBuildier.Append(String.Format("<td  class='labelFirstColumn'>{0}</td style='width:80%'><td><table class='liTB'><tr><td>{1}</td><td class='td-icon-help'>{2}</td></tr></table></td>", tmplbl, tmptxt, myToolTip))
                        givenSTRBuildier.Append("</tr>")
                    Next
                    givenSTRBuildier.Append("<tr>")


                    givenSTRBuildier.Append("</table>")
                End If
            Case DialogsQuestion.QuestionType.ExternalReferenceToAcquire
                givenExternalTable = givenQuestion.ExternalReferenceToAcquire
                If givenExternalTable IsNot Nothing Then
                    givenSTRBuildier.Append("<table style='width :100%;font-size: 8pt;' >")
                    For Each input As DialogsExternalTable In givenExternalTable

                        uniqueIdCA = input.id & "_" & input.sequenceId

                        idManadatory &= uniqueIdCA & ";"

                        idToValidate &= uniqueIdCA & ";"


                        tmplbl = input.title & "<font color='red' title='IS MANDATORY' >*</font>"
                        tmptxt = String.Format("<input id='txtTE_{0}' readonly='readonly' number='{1}' parent='{2}' type='text' value='{3}' descr=""{6}"" code=""{7}"" {4}  selid='{8}'/>{5}", uniqueIdCA, input.sequenceId, input.idParent, input.TE_TextValue, IIf(input.TE_TextValue <> "", "disabled='disabled'", ""), "", input.TE_Description, input.TE_Code, input.sequenceId)
                        If input.SearchRowNumber = -1 Then
                            btnTE = String.Format("<input type='button' value='...' id='btnTE_{0}' onclick=""openExternalReference('{0}','{1}', '{2}','{4}','{5}')"" {3}/>", uniqueIdCA, input.TE_Code, uniqueIdCA, IIf(input.TE_TextValue <> "", "disabled='disabled'", ""), input.SearchColumnsToShow, input.OutputColum)
                        End If
                        givenSTRBuildier.Append("<tr>")
                        givenSTRBuildier.Append(String.Format("<td  class='labelFirstColumn'>{0}</td style='width:80%'><td>{1}&nbsp;{2}</td>", tmplbl, tmptxt, btnTE))
                        givenSTRBuildier.Append("</tr>")

                    Next
                    givenSTRBuildier.Append("<tr>")


                    givenSTRBuildier.Append("</table>")
                End If
            Case DialogsQuestion.QuestionType.MultiOptionSelection
                If givenQuestion.Answers IsNot Nothing Then

                    givenSTRBuildier.Append(String.Format("<ul id='m_{0}' selid='{1}'>", givenQuestion.Id, givenQuestion.sequenceID))
                    For Each dummyAnswer In givenQuestion.Answers
                        If givenQuestion.isProcessed Then
                            givenSTRBuildier.Append(String.Format("<li id='{0}' onclick='liSelectionOnly(this)' disabled='true' parent='{1}' {2} {4} selid='{5}'>{3}</li>", dummyAnswer.id, dummyAnswer.idParent, IIf(dummyAnswer.isSelected, "class='selectedMultiple'", "class='liMultiple'"), dummyAnswer.title, IIf(givenQuestion.isProcessed, "class='liProcessedMultiple'", ""), dummyAnswer.sequenceId))
                        Else
                            givenSTRBuildier.Append(String.Format("<li id='{0}' onclick='liSelectionOnly(this)' parent='{1}' {2} {4} selid='{5}'>{3}</li>", dummyAnswer.id, dummyAnswer.idParent, IIf(dummyAnswer.isSelected, "class='selectedMultiple'", "class='liMultiple'"), dummyAnswer.title, IIf(givenQuestion.isProcessed, "class='liProcessedMultiple'", ""), dummyAnswer.sequenceId))
                        End If

                    Next
                    givenSTRBuildier.Append("</ul>")
                End If

        End Select





    End Sub

    Private Shared Sub QuestionFooter1(ByRef givenSTRBuildier As Text.StringBuilder, givenQuestion As DialogsQuestion)

        Dim idToValidate As String = ""
        Dim idManadatory As String = ""
        Dim tmpValidatationFunction As String = ""
        Dim tmpEditFunction As String = ""
        Dim uniqueIdCA As String = ""
        Dim idNumber As String = ""
        Dim idStrings As String = ""
        Dim hasCATE As Boolean = False

        If givenQuestion.Type = DialogsQuestion.QuestionType.MultiOptionSelection Then
            If givenQuestion.Answers IsNot Nothing Then

                For Each input As DialogsAnswer In givenQuestion.Answers
                    idToValidate &= input.id & ";"
                Next
                tmpEditFunction = String.Format("SetMultiForEditing('{0}','{1}');", givenQuestion.Id, idToValidate)

                tmpValidatationFunction = String.Format("MultiSelectionSendValidationAndUpdate('{0}','{1}');", givenQuestion.Id, idToValidate)
                givenSTRBuildier.Append("<table style='width :100%'><tr>")
                givenSTRBuildier.Append("<td class='lblError' id='lbl_" & givenQuestion.Id & "'></td>")
                givenSTRBuildier.Append("<td align='right' valign='bottom' style='width :40px;'>")
                givenSTRBuildier.Append(String.Format("<a href='#'><img id='editMultiple_{0}' src='images/edit.png' border='0'  title='Click Here to switch in edit mode' onclick=""{1}"" {2} /></a>", givenQuestion.Id, tmpEditFunction, IIf(givenQuestion.isProcessed, "", "style='display:none'")))
                givenSTRBuildier.Append(String.Format("<a href='#'><img id='saveMultiple_{0}' src='images/save.png' border='0'  title='Click Here to Save Selections' onclick=""{2}"" {3} /></a>", givenQuestion.Id, givenQuestion.Title, tmpValidatationFunction, IIf(givenQuestion.isProcessed, "style='display:none'", "")))
                givenSTRBuildier.Append("</td>")
                givenSTRBuildier.Append("</tr></table>")


            End If
        Else
            If givenQuestion.FeatureToAcquire IsNot Nothing Then
                Dim onlyRead As Boolean = False
                For Each input As DialogsInput In givenQuestion.FeatureToAcquire
                    If input.CA_ValueType <> DialogsInput.CA_InputType.N.ToString And input.CA_ValueType <> DialogsInput.CA_InputType.S.ToString Then
                        onlyRead = True
                    End If
                    uniqueIdCA = input.id & "." & input.CA_IdTable
                    If input.IsMandatory Then
                        idManadatory &= uniqueIdCA & ";"
                    End If
                    idToValidate &= uniqueIdCA & ";"
                    If input.CA_ValueType = DialogsInput.CA_InputType.N.ToString Then
                        idNumber &= uniqueIdCA & ";"
                    Else
                        idStrings &= uniqueIdCA & ";"
                    End If
                Next
                onlyRead = False
                tmpValidatationFunction = String.Format("javascript:return CASendValidationAndUpdate('{0}','{1}','{2}','{3}','{4}',this);", givenQuestion.Id, idToValidate, idManadatory, idNumber, idStrings)
                tmpEditFunction = String.Format("javascript:return SetCAForEditing('{0}','{1}',this);", givenQuestion.Id, idToValidate)

                givenSTRBuildier.Append("<table style='width :100%'><tr>")
                givenSTRBuildier.Append("<td class='lblError' id='lbl_" & givenQuestion.Id & "'></td>")
                givenSTRBuildier.Append("<td align='right' valign='bottom' style='width :40px;'>")
                If onlyRead Then
                    givenSTRBuildier.Append(String.Format("<a href='#'><img id='editCA_{0}' class='imgEditCA'  border='0'  title='Click Here to switch in edit mode' onclick=""{1}"" {2} /></a>", givenQuestion.Id, tmpEditFunction, "style='display:none'"))
                Else
                    givenSTRBuildier.Append(String.Format("<a href='#'><img id='editCA_{0}' class='imgEditCA'  border='0'  title='Click Here to switch in edit mode' onclick=""{1}"" {2} /></a>", givenQuestion.Id, tmpEditFunction, IIf(givenQuestion.isProcessed, "", "style='display:none'")))
                End If
                givenSTRBuildier.Append(String.Format("<a href='#'><img id='saveCA_{0}' class='imgSaveCA' border='0'  title='Click Here to Save Values' onclick=""{2}"" {3} /></a>", givenQuestion.Id, givenQuestion.Title, tmpValidatationFunction, IIf(givenQuestion.isProcessed, "style='display:none'", "")))

                givenSTRBuildier.Append("</td>")
                givenSTRBuildier.Append("</tr></table>")
            ElseIf givenQuestion.ExternalReferenceToAcquire IsNot Nothing Then
                For Each input As DialogsExternalTable In givenQuestion.ExternalReferenceToAcquire
                    If input.SearchRowNumber = -1 Then
                        uniqueIdCA = input.id & "_" & input.sequenceId

                        idManadatory &= uniqueIdCA & ";"

                        idToValidate &= uniqueIdCA & ";"
                        tmpValidatationFunction = String.Format("TESendValidationAndUpdate('{0}','{1}','{2}','{3}','{4}');", givenQuestion.Id, idToValidate, idManadatory, idNumber, idStrings)
                        tmpEditFunction = String.Format("SetTEForEditing('{0}','{1}');", givenQuestion.Id, idToValidate)

                        givenSTRBuildier.Append("<table style='width :100%'><tr>")
                        givenSTRBuildier.Append("<td class='lblError' id='lbl_" & givenQuestion.Id & "'></td>")
                        givenSTRBuildier.Append("<td align='right' valign='bottom' style='width :40px;'>")
                        givenSTRBuildier.Append(String.Format("<a href='#'><img id='editTE_{0}' src='images/edit.png' border='0'  title='Click Here to switch in edit mode' onclick=""{1}"" {2} /></a>", givenQuestion.Id, tmpEditFunction, IIf(givenQuestion.isProcessed, "", "style='display:none'")))
                        givenSTRBuildier.Append(String.Format("<a href='#'><img id='saveTE_{0}' src='images/save.png' border='0'  title='Click Here to Save Values' onclick=""{2}"" {3} /></a>", givenQuestion.Id, givenQuestion.Title, tmpValidatationFunction, IIf(givenQuestion.isProcessed, "style='display:none'", "")))
                        givenSTRBuildier.Append("</td>")
                        givenSTRBuildier.Append("</tr></table>")
                    End If

                Next


            End If
        End If

        givenSTRBuildier.Append("</div>")
    End Sub

    Private Shared Sub QuestionHeader2(ByRef givenSTRBuildier As Text.StringBuilder, givenQuestion As DialogsQuestion)

        If Not givenSTRBuildier.ToString.Contains("qid='" & givenQuestion.Id & "'") Then
            If givenSTRBuildier.ToString.Trim <> "" Then
                givenSTRBuildier.Append("<br/>")
            End If
            givenSTRBuildier.Append(String.Format("<b>{0}</b>:", givenQuestion.Title))
        Else
            givenSTRBuildier.Append(" ")
        End If
    End Sub

    Private Shared Sub QuestionBody2(ByRef givenSTRBuildier As Text.StringBuilder, givenQuestion As DialogsQuestion)

        Dim dummyAnswer As DialogsAnswer = Nothing
        Dim dummyInput As DialogsInput = Nothing
        Dim tmpValue As String = ""

        If givenQuestion.Type = DialogsQuestion.QuestionType.SingleOptionSelection Then
            If givenQuestion.Answers IsNot Nothing Then
                For Each dummyAnswer In givenQuestion.Answers
                    If dummyAnswer.isSelected Then
                        givenSTRBuildier.Append(String.Format("<i qid='{1}'>{0}</i>", dummyAnswer.title, dummyAnswer.id))
                        Exit For
                    End If
                Next
            End If
        ElseIf givenQuestion.Type = DialogsQuestion.QuestionType.MultiOptionSelection Then
            If givenQuestion.Answers IsNot Nothing Then
                tmpValue = ""
                For Each dummyAnswer In givenQuestion.Answers
                    If dummyAnswer.isSelected Then
                        If tmpValue <> "" Then
                            tmpValue &= "<br/>"
                            For idx As Integer = 0 To givenQuestion.Title.Length
                                tmpValue &= "&nbsp;"
                            Next
                        End If
                        tmpValue &= String.Format("<i qid='{0}'>{1}</i>", dummyAnswer.idParent, dummyAnswer.title)

                    End If


                Next
                givenSTRBuildier.Append(tmpValue)
            End If


        End If
        If givenQuestion.Answers IsNot Nothing Then

            For Each dummyAnswer In givenQuestion.Answers
                If dummyAnswer.isSelected Then
                    'If dummyAnswer.Type <> DialogsQuestionAnswer.AnswerType.Standard Then
                    '    givenSTRBuildier.Append(String.Format("<i qid='{1}'>{0} - {1}:</i>", dummyAnswer.title, dummyAnswer.CA_TE_Description))
                    '    givenSTRBuildier.Append(String.Format("&nbsp;<font color='#990033' style='font-weight: bold'>{0}</font>", dummyAnswer.CA_TE_TextValue))
                    'Else
                    '    givenSTRBuildier.Append(String.Format("<i qid='{1}'>{0}</i>", dummyAnswer.title, dummyAnswer.id))
                    'End If
                End If


            Next

        End If



    End Sub

    Private Shared Sub QuestionFooter2(ByRef givenSTRBuildier As Text.StringBuilder, givenQuestion As DialogsQuestion)

    End Sub

    Private Shared Function GetToolTip(givenToolTipFile As String, givenQuestionTitle As String, givenDescription As String)
        Dim retValue As String = ""
        Dim myTemplate As String = "<img class='icon-help-img'  title='{0}' onmouseover='telerikTips.showToolTip(this);' src='../icons/tooltip.png' />"
        If IO.File.Exists(givenToolTipFile) Then
            Dim myXmlDocumet As New XmlDocument
            myXmlDocumet.Load(givenToolTipFile)
            Dim myNodes As XmlNodeList = Nothing
            myNodes = myXmlDocumet.SelectNodes("//Config//Title[@text='" & givenQuestionTitle & "']/Options/Option")
            If myNodes.Count > 0 Then
                For Each node As XmlNode In myNodes
                    If node.Attributes("text").Value.ToLower = givenDescription.ToLower Then
                        If node.ChildNodes() IsNot Nothing AndAlso node.ChildNodes.Count > 0 Then
                            retValue = String.Format(myTemplate, node.ChildNodes(0).InnerText)
                            Exit For
                        End If

                    End If
                Next
            End If

        End If

        Return retValue
    End Function


End Class
