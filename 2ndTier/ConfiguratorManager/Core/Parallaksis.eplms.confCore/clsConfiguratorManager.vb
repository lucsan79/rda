﻿Imports Parallaksis.M40.Configurator.DataProvider
Imports Parallaksis.M40.DataModel
Imports System.Xml

Public Class ConfiguratorManager

    Dim myListVariable As Generic.Dictionary(Of String, String)
    Dim myCurrentDialog As Dialogs
    Dim dummyNextDialog As Dialogs
    Dim myConfiguratorStep As ConfiguratorSequenceHelper
    Dim externalVariable As Generic.Dictionary(Of String, String)
    Dim automaticCPCompialtion As Generic.Dictionary(Of String, Double)
    'Dim internalDialogVariable As Generic.Dictionary(Of String, String)
    Dim givenCodifica As String = ""
    Dim givenCadName As String = ""
    Dim givenFamiglia As String = ""
    Dim givenProdotto As String = ""
    Dim givenCodiceProdottoFinito As String = ""
    Dim myConfigPath As String = ""
    Public myPathToolTip As String = ""


#Region "COSTRUTTORE"
    Public Sub New()

    End Sub
    Public Sub New(ByVal givenConfigurationPath As String, ByVal givenInputVariables As Generic.Dictionary(Of String, String), ByVal givenKFeatures As Generic.Dictionary(Of String, Double), ByVal verifyCadReferement As Boolean, ByVal verifyCodify As Boolean, ByVal givenToolTipFile As String)
        Dim myNewDataProvider As New DataProvider.DataProvider
        Dim myQADataSet As dsConfiguratorDialog

        myQADataSet = myNewDataProvider.InitConfiguratorDataByXML(givenConfigurationPath, givenFamiglia, givenProdotto, givenCodifica, givenCadName)

        'If givenKFeatures Is Nothing Then

        If givenCadName = "" And verifyCadReferement Then
            Throw New Exception("Impossibile avviare il configuratore, Riferimento CAD mancante")
        End If
        If givenCodifica = "" And verifyCodify Then
            Throw New Exception("Impossibile avviare il configuratore, Codifica mancante")
        End If

        automaticCPCompialtion = givenKFeatures
        myCurrentDialog = New Dialogs
        externalVariable = New Dictionary(Of String, String)
        myConfigPath = System.IO.Path.GetDirectoryName(givenConfigurationPath)
        myPathToolTip = givenToolTipFile

        'internalDialogVariable = New Dictionary(Of String, String)
        If givenInputVariables IsNot Nothing Then
            externalVariable = givenInputVariables
        End If

        myConfiguratorStep = New ConfiguratorSequenceHelper(myQADataSet, externalVariable, givenCodifica, givenConfigurationPath)
        'myConfiguratorStep.Variables.Add("CL", "1310000001")
    End Sub



    Public Sub New(ByVal givenDataSet As DataSet, ByVal givenConfigPath As String)
        Dim myNewDataProvider As New DataProvider.DataProvider
        Dim myQADataSet As dsConfiguratorDialog
        Dim dummyXmlDoc As XmlDocument
        Dim dummyXmlNode As XmlNode
        Dim dummyXmlNodeList As XmlNodeList

        myQADataSet = myNewDataProvider.InitConfiguratorDataByXML(givenDataSet)

        myCurrentDialog = New Dialogs
        externalVariable = New Dictionary(Of String, String)

        

        If Not System.IO.File.Exists(givenConfigPath) Then
            Throw New Exception("Configuration file not found")
        End If

        dummyXmlDoc = New XmlDocument
        dummyXmlDoc.Load(givenConfigPath)

        ' givenFamiglia, givenProdotto, givenCodifica, givenCadName
        If dummyXmlDoc.FirstChild IsNot Nothing Then
            If dummyXmlDoc.FirstChild.Attributes("CODIFICA") IsNot Nothing Then
                givenCodifica = dummyXmlDoc.FirstChild.Attributes("CODIFICA").Value
            End If

            If dummyXmlDoc.FirstChild.Attributes("CADASM") IsNot Nothing Then
                givenCadName = dummyXmlDoc.FirstChild.Attributes("CADASM").Value
            End If

            If dummyXmlDoc.FirstChild.Attributes("FAMIGLIA") IsNot Nothing Then
                givenFamiglia = dummyXmlDoc.FirstChild.Attributes("FAMIGLIA").Value
            End If

            If dummyXmlDoc.FirstChild.Attributes("PRODOTTO") IsNot Nothing Then
                givenProdotto = dummyXmlDoc.FirstChild.Attributes("PRODOTTO").Value
            End If
        End If

        If givenDataSet.Tables("QAVariabili") IsNot Nothing Then
            For Each row As dsConfiguratorDialog.QAVariabiliRow In givenDataSet.Tables("QAVariabili").Rows
                If row.id = -111 Then
                    givenCodifica = row.value
                Else
                    externalVariable.Add(row.name, row.value)
                End If

            Next
        End If

        myConfigPath = System.IO.Path.GetDirectoryName(givenConfigPath)
        myConfiguratorStep = New ConfiguratorSequenceHelper(myQADataSet, externalVariable, givenCodifica, givenConfigPath)
        '  myConfiguratorStep.Variables.Add("CL", "1310000001")
    End Sub


#End Region

#Region "CODE"
    '10250 : myQATable NOTHING
    '10251 : myConfiguratorStep NOTHING
    '10252 : myCurrentDialog NOTHING
#End Region

#Region "CHECK PROCEDURALI"
    '1. se applico 'isMandatory='false'' ad una domanda questa non verrà presentata

#End Region

#Region "PUBLIC"
    Public Function GetExternalVariables() As Dictionary(Of String, String)
        Return externalVariable
    End Function

    Public Function GetCadName() As String
        Return givenCadName
    End Function
    Public Function GetCodiceProdottoFinito() As String
        Return givenCodifica
    End Function
    Public Function GetCodiceProdotto() As String
        Return givenProdotto
    End Function

    Public Function IsAFunction(ByVal givenVal As String) As Boolean
        If givenVal = "" Then
            Return False
        End If
        If System.IO.Directory.Exists(myConfigPath) Then
            For Each file In IO.Directory.GetFiles(myConfigPath)
                Dim myStringFile As String = (givenVal & ".vb").ToLower
                If System.IO.Path.GetFileName(file).ToLower.Equals(myStringFile) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    Public Function CalculateCadName() As String

        Dim retValue As String = "OK"
        Dim myFunction As String = ""

        If System.IO.Directory.Exists(myConfigPath) Then
            For Each file In IO.Directory.GetFiles(myConfigPath)
                Dim myStringFile As String = (givenCadName & ".vb").ToLower
                If System.IO.Path.GetFileName(file).ToLower.EndsWith(myStringFile) Then
                    myFunction = System.IO.Path.GetFileNameWithoutExtension(file)
                    Exit For
                End If
            Next
        End If
        If myFunction <> "" Then
            Try
                retValue = myConfiguratorStep.CalculateCadName(myFunction)
            Catch ex As Exception
                Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
            End Try
        End If


        Return retValue

    End Function

    Public Function CalculateCadProdotto() As String

        Dim retValue As String = "OK"
        Dim myFunction As String = ""

        If System.IO.Directory.Exists(myConfigPath) Then
            For Each file In IO.Directory.GetFiles(myConfigPath)
                Dim myStringFile As String = (givenProdotto & ".vb").ToLower
                If System.IO.Path.GetFileName(file).ToLower.EndsWith(myStringFile) Then
                    myFunction = System.IO.Path.GetFileNameWithoutExtension(file)
                    Exit For
                End If
            Next
        End If
        If myFunction <> "" Then
            Try
                retValue = myConfiguratorStep.CalculateCadProdotto(myFunction)
            Catch ex As Exception
                Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
            End Try
        End If


        Return retValue

    End Function

    Public Function CalculateCodiceProdottoFinito() As String

        Dim retValue As String = "OK"
        Dim myFunction As String = ""

        If System.IO.Directory.Exists(myConfigPath) Then
            For Each file In IO.Directory.GetFiles(myConfigPath)
                Dim myStringFile As String = (givenCodifica & ".vb").ToLower
                If System.IO.Path.GetFileName(file).ToLower.EndsWith(myStringFile) Then
                    myFunction = System.IO.Path.GetFileNameWithoutExtension(file)
                    Exit For
                End If
            Next
        End If
        If myFunction <> "" Then
            Try
                retValue = myConfiguratorStep.CalculateCodificaProdottoFinito(myFunction)
            Catch ex As Exception
                Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
            End Try
        End If


        Return retValue

    End Function


    Public Function CalculateCPFunction(ByVal givenFunction As String) As String

        Dim retValue As String = "OK"
        Dim myFunction As String = ""

        If System.IO.Directory.Exists(myConfigPath) Then
            For Each file In IO.Directory.GetFiles(myConfigPath)
                Dim myStringFile As String = (givenFunction & ".vb").ToLower
                If System.IO.Path.GetFileName(file).ToLower.EndsWith(myStringFile) Then
                    myFunction = System.IO.Path.GetFileNameWithoutExtension(file)
                    Exit For
                End If
            Next
        End If
        If myFunction <> "" Then
            Try
                retValue = myConfiguratorStep.CalculateCodificaProdottoFinito(myFunction)
            Catch ex As Exception
                Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
            End Try
        End If


        Return retValue

    End Function


    Public Function ValidateCA(ByVal givenCANumber As String, ByVal givenValue As String) As String

        Dim retValue As String = "OK"

        Try
            retValue = myConfiguratorStep.ValidateCA(givenCANumber, givenValue, myConfigPath)
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try

        Return retValue
    End Function

    Public Function GetNextConfiguratorStep(ByVal givenSelectionid As String, ByVal givenSelectedParentId As String, ByVal givenSelectedChildId As String) As Dialogs
        Dim retValue As Dialogs = Nothing

        Try


            retValue = myConfiguratorStep.GetNextDialogQuestion(givenSelectionid, myConfigPath, automaticCPCompialtion)

            dummyNextDialog = retValue
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try

        Return retValue

    End Function

    Public Function GetNextConfiguratorStep2(ByVal givenSelectionid As String, ByVal givenId As String, ByVal givenSelectedParentId As String, ByVal givenSelectedChildId As String) As Dialogs
        Dim retValue As Dialogs = Nothing

        Try


            retValue = myConfiguratorStep.GetNextDialogQuestion2(givenSelectionid, givenId, myConfigPath, automaticCPCompialtion)

            dummyNextDialog = retValue
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try

        Return retValue

    End Function

    Public Function UpdateCurrentQASelection(ByVal givenIdSelection As String, ByVal givenSelectedParentId As String, ByVal givenSelectedChildId As String, ByVal givenInputValue As String) As Dialogs
        Dim retValue As Dialogs = Nothing
        Try
            If givenInputValue <> "" And givenSelectedChildId.ToLower <> "multiple" Then
                If givenInputValue.StartsWith("CA") Then
                    retValue = myConfiguratorStep.UpdateSequenceCA(givenSelectedParentId, givenInputValue, myConfigPath)
                    'c.WriteLine("UpdateSequenceCA givenSelectedParentId:" & givenSelectedParentId & " givenInputValue:" & givenInputValue)
                ElseIf givenInputValue.StartsWith("TE") Then
                    retValue = myConfiguratorStep.UpdateSequenceTE(givenSelectedParentId, givenSelectedChildId, givenInputValue)
                    'c.WriteLine("UpdateSequenceTE givenSelectedParentId:" & givenSelectedParentId & " givenSelectedChildId:" & givenSelectedChildId & " givenInputValue:" & givenInputValue)
                End If

            ElseIf givenSelectedChildId.ToLower = "multiple" Then
                Dim itemSelected() As String = givenInputValue.Split(";".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
                retValue = myConfiguratorStep.UpdateSequenceQA(givenIdSelection, givenSelectedParentId, givenInputValue, myConfigPath)
                '     c.WriteLine("multiple UpdateSequenceQA givenIdSelection:" & givenIdSelection & " givenSelectedParentId:" & givenSelectedParentId & " givenInputValue:" & givenInputValue)
            Else
                retValue = myConfiguratorStep.UpdateSequenceQA(givenIdSelection, givenSelectedParentId, givenSelectedChildId, myConfigPath)
                '    c.WriteLine("UpdateSequenceQA givenIdSelection:" & givenIdSelection & " givenSelectedParentId:" & givenSelectedParentId & " givenSelectedChildId:" & givenSelectedChildId)
            End If
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try
        'c.Close()

        Return retValue

    End Function

    Public Function GetQADialogHTML() As String
        Dim retValue As String = ""

        Try
            retValue = DialogToHTML.DialogToHtml1(myCurrentDialog, myPathToolTip)
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try

        Return retValue
    End Function

    Public Function GetFullQADialogHTML() As String
        Dim retValue As String = ""

        Try
            myCurrentDialog = myConfiguratorStep.GetFullUsedQA(myConfigPath)
            retValue = DialogToHTML.DialogToHtml1(myCurrentDialog, myPathToolTip)
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try

        Return retValue
    End Function

    Public Function GetQADialogHTMLFilter1(ByVal givenStopAt As Integer) As String
        Dim retValue As String = ""

        Try
            myCurrentDialog = myConfiguratorStep.GetFullUsedQA(myConfigPath)
            retValue = DialogToHTML.DialogToHtmlFiltered1(myCurrentDialog, givenStopAt, myPathToolTip)
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try

        Return retValue
    End Function

    Public Function GetQADialogHTMLFilter2(ByVal givenStartAt As Integer) As String
        Dim retValue As String = ""

        Try
            'myCurrentDialog = myConfiguratorStep.GetFullUsedQA(givenStopAt)
            retValue = DialogToHTML.DialogToHtmlFiltered2(myCurrentDialog, givenStartAt, myPathToolTip)
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try

        Return retValue
    End Function

    Public Function GetDialogHTML(ByVal givenDialog As Dialogs) As String
        Dim retValue As String = ""

        Try
            retValue = DialogToHTML.DialogToHtml1(givenDialog, myPathToolTip)
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try

        Return retValue
    End Function


    Public Function GetSummaryDialogHTML() As String
        Dim retValue As String = ""

        Try
            retValue = DialogToHTML.DialogToHtml2(myCurrentDialog)

        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try
        Return retValue
    End Function


    Public Function GetExternalTable(ByVal givenSequenceId As String, ByVal givenTableCode As String) As DataTable
        Dim retValue As DataTable

        Try
            retValue = myConfiguratorStep.GetTEDataTable(givenSequenceId, givenTableCode, myConfigPath)
        Catch ex As Exception
            Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
        End Try
        Return retValue
    End Function

    Public Function SaveDialog() As DataSet
        Return myConfiguratorStep.SaveDialogTables()
    End Function


    Public Function GetFeatureValue(givenCP) As String
        Return myConfiguratorStep.GetCPValue(givenCP)
    End Function

#End Region

#Region "PUBLIC ACCESSO ALLE INFO DELLE TABELLE"
    'Public Function GetCARow(givenIdCA As String, givenParentIdCA As String) As dsConfiguratorDialog.QAInputRow

    '    Dim retValue As dsConfiguratorDialog.QAInputRow
    '    ValidateConnection()

    '    retValue = myConfiguratorStep.GetCARowById(givenIdCA, givenParentIdCA)


    '    Return retValue
    'End Function
#End Region


#Region "SHARED"
    Public Function MergeDialogs(ByVal givenDialogLastVersion As String, ByVal givenCurrentDialog As String, ByVal givenInputVariables As Generic.Dictionary(Of String, String), ByVal givenKFeatures As Generic.Dictionary(Of String, Double)) As Boolean
        Dim myNewDataProvider As New DataProvider.DataProvider
        Dim myQADataSetLast As dsConfiguratorDialog
        Dim myQADataSetActual As dsConfiguratorDialog
        Dim givenFamiglia1, givenProdotto1, givenCodifica1, givenCadName1 As String
        Dim mySequenceHelper As ConfiguratorSequenceHelper

        myQADataSetLast = myNewDataProvider.InitConfiguratorDataByXML(givenDialogLastVersion, givenFamiglia1, givenProdotto1, givenCodifica1, givenCadName1)

        myQADataSetActual = New dsConfiguratorDialog
        myQADataSetActual.ReadXml(givenCurrentDialog)

        If myQADataSetLast IsNot Nothing And myQADataSetActual IsNot Nothing Then
            mySequenceHelper = New ConfiguratorSequenceHelper(myQADataSetLast, givenInputVariables, givenCodifica1, givenDialogLastVersion)
            If mySequenceHelper.MergeDataSet(myQADataSetLast, myQADataSetActual, IO.Path.GetDirectoryName(givenCurrentDialog)) Then
                If myQADataSetLast IsNot Nothing Then
                    IO.File.Delete(givenCurrentDialog)
                    myQADataSetLast.WriteXml(givenCurrentDialog)

                    'ValidateAllSteps(givenCurrentDialog, givenInputVariables, givenKFeatures)
                    Return IO.File.Exists(givenCurrentDialog)
                End If
            End If
        End If

        Return False

    End Function


    Public Shared Function ValidateAllSteps(ByVal givenFileToValidate As String, ByVal dummyFileDialog As String, ByVal givenInputVariables As Generic.Dictionary(Of String, String), ByVal givenKFeatures As Generic.Dictionary(Of String, Double), ByVal verifyCadReferement As Boolean, ByVal verifyCodify As Boolean) As Boolean
        Dim myNewDataProvider As New DataProvider.DataProvider
        Dim myQADataSetActual As dsConfiguratorDialog
        Dim givenFamiglia1, givenProdotto1, givenCodifica1, givenCadName1 As String
        Dim myConfManager As ConfiguratorManager
        Dim myDialog As Dialogs
        Dim esito As Boolean = True
        myQADataSetActual = New dsConfiguratorDialog
        myQADataSetActual.ReadXml(givenFileToValidate)
        Dim myQADataSetDialog As dsConfiguratorDialog

        Dim myC As New IO.StreamWriter("c:\temp\" & Guid.NewGuid.ToString & ".txt")
        Dim myCounter As Integer = 100000
        'myConfManager = New ConfiguratorManager(myQADataSetActual, givenFileToValidate)
        myConfManager = New ConfiguratorManager(dummyFileDialog, givenInputVariables, givenKFeatures, verifyCadReferement, verifyCodify, "")
        If myConfManager IsNot Nothing Then

            myDialog = myConfManager.GetNextConfiguratorStep("", "", "")
            Dim myRisp As New Generic.List(Of String)
            While myDialog IsNot Nothing And myCounter > 0
                myCounter -= 1
                If myDialog.Questions IsNot Nothing Then
                    'CERCHIAMO NEL VECCHIO LA DOMANDA
                    Dim myDummyDialog As Dialogs = ConfiguratorSequenceHelper.GetTempDialogObject(myQADataSetActual, myDialog.Questions(0).Id)
                    If myDummyDialog IsNot Nothing Then

                        If myDialog.Questions IsNot Nothing AndAlso myDialog.Questions.Count > 0 Then
                            If myRisp.Contains(myDialog.Questions(0).sequenceID) Then
                                myC.WriteLine("force exit - " & myDialog.Questions(0).sequenceID)
                                Exit While
                            Else
                                myRisp.Add(myDialog.Questions(0).sequenceID)
                            End If
                        End If

                        If myDialog.Questions(0).Answers IsNot Nothing And myDummyDialog.Questions(0).Answers IsNot Nothing Then
                            Dim selectId As String = ""
                            For Each answer As DialogsAnswer In myDialog.Questions(0).Answers
                                For Each alreadyAnswer As DialogsAnswer In myDummyDialog.Questions(0).Answers
                                    If answer.id = alreadyAnswer.id And alreadyAnswer.isSelected And Not selectId.Contains(answer.id) Then
                                        If selectId <> "" Then
                                            selectId = selectId & ";"
                                        End If
                                        selectId &= answer.id
                                        Exit For
                                    End If
                                Next
                            Next
                            If selectId.Split(";".ToCharArray, StringSplitOptions.RemoveEmptyEntries).Length = 1 Then
                                selectId = selectId.Split(";")(0)
                                Dim seqId As String = myDialog.Questions(0).sequenceID ' selectId.Split("$")(0)
                                Dim objId As String = selectId
                                myConfManager.UpdateCurrentQASelection(seqId, myDialog.Questions(0).Id, objId, "")
                                myC.WriteLine(Now.ToString & " - " & seqId)
                                myDialog = myConfManager.GetNextConfiguratorStep(seqId, myDialog.Questions(0).Id, objId)
                                myC.WriteLine(Now.ToString & " - " & seqId)
                                Continue While
                            ElseIf selectId.Split(";".ToCharArray, StringSplitOptions.RemoveEmptyEntries).Length > 1 Then
                                Dim seqId As String = myDialog.Questions(0).sequenceID
                                Dim objId As String = "multiple"
                                Dim inputValues As String = selectId
                                myConfManager.UpdateCurrentQASelection(seqId, myDialog.Questions(0).Id, objId, inputValues)
                                myC.WriteLine(Now.ToString & " - " & seqId)
                                myDialog = myConfManager.GetNextConfiguratorStep(seqId, myDialog.Questions(0).Id, objId)
                                myC.WriteLine(Now.ToString & " - " & seqId)
                            Else
                                Dim defaultid As String = ""
                                Dim forceid As String = ""
                                For Each answer As DialogsAnswer In myDialog.Questions(0).Answers
                                    If answer.forceChoise Then
                                        forceid = answer.id
                                    ElseIf answer.isDefault Then
                                        defaultid = answer.id
                                    End If
                                Next
                                If defaultid <> "" Or forceid <> "" Then
                                    If forceid <> "" Then
                                        defaultid = forceid
                                    End If
                                    Dim seqId As String = myDialog.Questions(0).sequenceID
                                    myConfManager.UpdateCurrentQASelection(seqId, myDialog.Questions(0).Id, defaultid, "")
                                    myC.WriteLine(Now.ToString & " - " & seqId)
                                    myDialog = myConfManager.GetNextConfiguratorStep(seqId, myDialog.Questions(0).Id, defaultid)
                                    myC.WriteLine(Now.ToString & " - " & seqId)
                                Else
                                    myC.WriteLine(Now.ToString & " - " & myDialog.Questions(0).sequenceID)
                                    myDialog = myConfManager.GetNextConfiguratorStep2(myDialog.Questions(0).sequenceID, myDialog.Questions(0).Id, "", "")
                                    'myC.WriteLine(Now.ToString & " - " & myDialog.Questions(0).sequenceID)
                                    esito = False
                                End If
                            End If

                        ElseIf myDialog.Questions(0).FeatureToAcquire IsNot Nothing And myDummyDialog.Questions(0).FeatureToAcquire IsNot Nothing Then
                            Dim allValued As Boolean = True
                            Dim childId As String = ""
                            For Each input As DialogsInput In myDialog.Questions(0).FeatureToAcquire
                                For Each input2 As DialogsInput In myDummyDialog.Questions(0).FeatureToAcquire
                                    If input.id = input2.id And input.CA_Code = input2.CA_Code Then
                                        input.CA_TextValue = input2.CA_TextValue
                                        Exit For
                                    End If
                                Next

                            Next
                            For Each input As DialogsInput In myDialog.Questions(0).FeatureToAcquire
                                If input.CA_TextValue = "" Then
                                    allValued = False
                                Else
                                    childId = childId & String.Format("CA_{0}.{1}:{2};", input.idParent, input.CA_IdTable, input.CA_TextValue)
                                End If
                            Next

                            If allValued Then
                                myConfManager.UpdateCurrentQASelection(myDialog.Questions(0).sequenceID, myDialog.Questions(0).Id, myDialog.Questions(0).Id, childId)
                                myDialog = myConfManager.GetNextConfiguratorStep(myDialog.Questions(0).sequenceID, myDialog.Questions(0).Id, myDialog.Questions(0).Id)
                            Else
                                myDialog = myConfManager.GetNextConfiguratorStep2(myDialog.Questions(0).sequenceID, myDialog.Questions(0).Id, "", "")
                                esito = False
                            End If

                        ElseIf myDialog.Questions(0).ExternalReferenceToAcquire IsNot Nothing And myDummyDialog.Questions(0).ExternalReferenceToAcquire IsNot Nothing Then

                            If myDialog.Questions(0).ExternalReferenceToAcquire.Count > 0 Then
                                Dim childIdValue As String = ""
                                For Each input As DialogsExternalTable In myDialog.Questions(0).ExternalReferenceToAcquire
                                    For Each input2 As DialogsExternalTable In myDummyDialog.Questions(0).ExternalReferenceToAcquire
                                        input.TE_TextValue = input2.TE_TextValue
                                    Next
                                Next
                                For Each input As DialogsExternalTable In myDialog.Questions(0).ExternalReferenceToAcquire
                                    If input.TE_TextValue <> "" Then

                                        childIdValue = childIdValue & String.Format("TE_{0}_{1}:{2};", input.idParent, input.sequenceId, input.TE_TextValue)
                                        myConfManager.UpdateCurrentQASelection(myDialog.Questions(0).sequenceID, input.idParent, input.idParent, childIdValue)
                                        myDialog = myConfManager.GetNextConfiguratorStep(myDialog.Questions(0).sequenceID, myDialog.Questions(0).Id, myDialog.Questions(0).Id)
                                        Exit For
                                    Else
                                        myDialog = myConfManager.GetNextConfiguratorStep2(myDialog.Questions(0).sequenceID, myDialog.Questions(0).Id, "", "")
                                        esito = False
                                        Exit For
                                    End If

                                Next

                            End If

                        End If
                    Else
                        myDialog = myConfManager.GetNextConfiguratorStep2(myDialog.Questions(0).sequenceID, myDialog.Questions(0).Id, "", "")
                        esito = False
                    End If

                End If
            End While
        End If
        myC.WriteLine("stop" & myCounter)
        myC.Close()

        Dim myDataSet As Data.DataSet = myConfManager.SaveDialog()
        myDataSet.WriteXml(givenFileToValidate)
        Return esito

    End Function



#End Region
End Class
