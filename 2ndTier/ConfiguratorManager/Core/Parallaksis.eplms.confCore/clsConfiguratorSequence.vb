﻿
Imports Parallaksis.M40.Configurator.DataProvider
Imports Parallaksis.M40.Configurator.DataProvider.DataProvider
Imports Parallaksis.M40.DataModel.dsConfiguratorDialog
Imports Parallaksis.M40.DataModel
Imports Parallaksis.M40.DataModel.clsDataModelHelper
Imports System.Text.RegularExpressions

Public Class ConfiguratorSequenceHelper

    Dim myDataModelHelper As clsDataModelHelper
    Dim myListVariable As Generic.Dictionary(Of String, String)
    Dim DIALOGCODE As String = "--------------"
    Dim givenBasePathDeploy As String = ""
    Public Const VALSEP As String = "\[@]"
    Public Const CODSEP As String = "\[#]"
    Public Const DIALOG_TAG_END As String = "<br/>[CONFIGURATION ENDED]|"
    Public Const DIALOG_TAG_NEWLINE As String = "<br/>"
    'Dim myLastSelection As Dialogs

#Region "COSTRUTTORE"

#End Region

    Public Sub New(ByVal givenTableData As dsConfiguratorDialog, ByVal givenVariables As Generic.Dictionary(Of String, String), ByVal givenDialogCodifica As String, ByVal givenBase As String)
        myDataModelHelper = New clsDataModelHelper(givenTableData)
        If givenVariables IsNot Nothing Then
            myListVariable = givenVariables
        Else
            myListVariable = New Generic.Dictionary(Of String, String)
        End If
        DIALOGCODE = givenDialogCodifica
        givenBasePathDeploy = givenBase
        If DIALOGCODE.Trim = "" Then
            Throw New Exception("Codifica non ammissibile")
        End If

    End Sub

#Region "private"

#End Region

    Public Function CalculateCadName(ByVal functionName As String) As String
        Dim myReturnValue As String = ""
        Dim myCode As String = ""

        Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate = Nothing

        myCode = DataProvider.DataProvider.GetRule(givenBasePathDeploy, functionName)
        If myCode <> "" Then
            myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
            myReturnValue = myNewEvalFun.EvaluateCAFunction(myCode)
        End If
        Return myReturnValue
    End Function

    Public Function CalculateCadProdotto(ByVal functionName As String) As String
        Dim myReturnValue As String = ""
        Dim myCode As String = ""

        Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate = Nothing

        myCode = DataProvider.DataProvider.GetRule(givenBasePathDeploy, functionName)
        If myCode <> "" Then
            myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
            myReturnValue = myNewEvalFun.EvaluateCAFunction(myCode)
        End If
        Return myReturnValue
    End Function

    Public Function CalculateCodificaProdottoFinito(ByVal functionName As String) As String
        Dim myReturnValue As String = ""
        Dim myCode As String = ""

        Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate = Nothing

        myCode = DataProvider.DataProvider.GetRule(givenBasePathDeploy, functionName)
        If myCode <> "" Then
            myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
            myReturnValue = myNewEvalFun.EvaluateCAFunction(myCode)
        End If
        Return myReturnValue
    End Function

    Public Function CalculatFunctionValue(ByVal functionName As String) As String
        Dim myReturnValue As String = ""
        Dim myCode As String = ""

        Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate = Nothing

        myCode = DataProvider.DataProvider.GetRule(givenBasePathDeploy, functionName)
        If myCode <> "" Then
            myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
            myReturnValue = myNewEvalFun.EvaluateCAFunction(myCode)
        End If
        Return myReturnValue
    End Function




    Public Property Variables() As Generic.Dictionary(Of String, String)
        Get
            Return myListVariable
        End Get
        Set(ByVal value As Generic.Dictionary(Of String, String))
            myListVariable = value
        End Set
    End Property

    Private Sub SetCAByFunction(ByRef givenDialog As Dialogs)

        If givenDialog.Questions IsNot Nothing Then
            If givenDialog.Questions(0).FeatureToAcquire IsNot Nothing Then

                If givenDialog.Questions(0).FeatureToAcquire.Count > 0 Then
                    Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate
                    For Each input As DialogsInput In givenDialog.Questions(0).FeatureToAcquire
                        If input.CA_ValueType <> DialogsInput.CA_InputType.N.ToString And input.CA_ValueType <> DialogsInput.CA_InputType.S.ToString Then
                            Dim code As String = ""
                            code = DataProvider.DataProvider.GetRule(givenBasePathDeploy, input.CA_ValueType)
                            If myNewEvalFun Is Nothing Then
                                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                            End If
                            If code <> "" Then
                                input.CA_TextValue = myNewEvalFun.EvaluateCAFunction(code)
                            End If



                        End If
                    Next
                    myNewEvalFun = Nothing
                End If

            End If
        End If


    End Sub


    Private Sub SetCPByFunction(ByRef givenDialog As Dialogs)

        If givenDialog.Questions IsNot Nothing Then
            If givenDialog.Questions(0).Features IsNot Nothing Then

                If givenDialog.Questions(0).Features.Count > 0 Then
                    Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate
                    For Each input As DialogsFeature In givenDialog.Questions(0).Features
                        Dim code As String = ""
                        If input.Value <> input.OriginalValue And input.OriginalValue <> "" Then
                            code = DataProvider.DataProvider.GetRule(givenBasePathDeploy, input.OriginalValue)
                        Else
                            code = DataProvider.DataProvider.GetRule(givenBasePathDeploy, input.Value)
                        End If

                        If myNewEvalFun Is Nothing And code <> "" Then
                            myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                            input.Value = myNewEvalFun.EvaluateCAFunction(code)
                        End If
                    Next

                    myNewEvalFun = Nothing
                End If

            End If
        End If


    End Sub


    Private Sub SetTEAutomatic(ByRef givenDialog As Dialogs, myConfigPath As String)
        Dim dummyTable As DataTable
        If givenDialog.Questions IsNot Nothing Then
            If givenDialog.Questions(0).ExternalReferenceToAcquire IsNot Nothing Then

                If givenDialog.Questions(0).ExternalReferenceToAcquire.Count > 0 Then

                    For Each external As DialogsExternalTable In givenDialog.Questions(0).ExternalReferenceToAcquire
                        'If external.TE_TextValue <> "" Then
                        '    Continue For
                        'End If
                        If external.SearchRowNumber = -1 Then

                        Else
                            Dim myCodeVerify As String = external.idParent & "_" & external.sequenceId
                            'dummyTable = GetTEDataTable(external.sequenceId, external.TE_Code, myConfigPath)
                            dummyTable = GetTEDataTable(myCodeVerify, external.TE_Code, myConfigPath)
                            If dummyTable IsNot Nothing Then
                                If dummyTable.Rows IsNot Nothing Then
                                    If dummyTable.Rows.Count >= 1 Then
                                        external.TE_TextValue = dummyTable.Rows(0)(external.OutputColum).ToString
                                    Else
                                        external.TE_TextValue = ""
                                    End If
                                    dummyTable.AcceptChanges()

                                End If
                            End If
                        End If
                        If external.TE_ValueValidate <> "" Then

                        End If
                    Next

                End If

            End If
        End If


    End Sub
#Region "STEP MANAGEMENT"

    Public Function GetNextDialogQuestion(ByVal givenIdSelection As String, ByVal myConfigPath As String, ByVal automaticCPCompialtion As Generic.Dictionary(Of String, Double)) As Dialogs

        Dim myTableOption As QAOptionDataTable
        Dim myTMPTableOption As QAOptionDataTable
        Dim lqResult
        Dim lqResultTMP
        Dim retValue As Dialogs = Nothing
        Dim dummyRow As QAOptionRow = Nothing
        Dim pathNode As String = ""
        Dim canBeProcessedFlag As Boolean = False
        Dim myOutPut As New Generic.List(Of QAOptionRow)
        Dim countProcessed As Integer = 0
        myTableOption = myDataModelHelper.GetDialogTable(TableName.QAOption)
        write(myTableOption)
        If givenIdSelection = "" Then
            givenIdSelection = "0"
        End If
        lqResult = From qalist In myTableOption
                   Where qalist.selected = False And qalist.level = 0 And qalist.number >= Integer.Parse(givenIdSelection)
                   Select qalist Order By qalist.number Ascending


        countProcessed = 0
        Dim indexCount As Integer = 0
        For Each qalist As QAOptionRow In lqResult
            If qalist.selected Then
                Continue For
            End If
            'myTMPTableOption = myDataModelHelper.GetDialogTable(TableName.QAOption)
            'lqResultTMP = From qalistTMP In myTMPTableOption _
            '          Where qalistTMP.id = qalist.id
            '          Select qalistTMP Order By qalistTMP.number Ascending

            'Dim continueFor As Boolean = False
            'For Each qalistTMP As QAOptionRow In lqResultTMP
            '    If qalistTMP.selected = True And qalist.selected = False Then
            '        continueFor = True
            '        Exit For
            '    End If
            'Next
            'If continueFor Then
            '    Continue For
            'End If
            retValue = Nothing
            countProcessed += 1
            retValue = myDataModelHelper.FillDialog(qalist)
            If retValue IsNot Nothing Then
                If Not ValidateDialog(retValue, myTableOption, myConfigPath) Then
                    retValue = Nothing
                    Continue For
                End If
            End If

            If retValue IsNot Nothing Then
                If retValue.Questions IsNot Nothing Then
                    'SetCPByFunction(retValue)
                    SetCAByFunction(retValue)
                    SetTEAutomatic(retValue, myConfigPath)
                    Dim myListDefault As String = ""
                    If retValue.Questions(0).Answers IsNot Nothing Then
                        For idx As Integer = 0 To retValue.Questions(0).Answers.Count - 1
                            If retValue.Questions(0).Answers(idx).forceChoise Then
                                myListDefault &= retValue.Questions(0).Answers(idx).sequenceId & ";"
                            End If

                        Next
                        If myListDefault <> "" Then
                            'For idx As Integer = 0 To retValue.Questions(0).Answers.Count - 1
                            For idx As Integer = retValue.Questions(0).Answers.Count - 1 To 0 Step -1 '0 To retValue.Questions(0).Answers.Count - 1
                                If Not myListDefault.Contains(retValue.Questions(0).Answers(idx).sequenceId & ";") Then
                                    retValue.Questions(0).Answers.RemoveAt(idx)
                                    If idx = retValue.Questions(0).Answers.Count - 1 Then
                                        Exit For
                                    End If
                                    idx = idx - 1
                                End If
                            Next

                        End If
                    End If


                End If
            End If

            If retValue IsNot Nothing Then
                If retValue.Questions IsNot Nothing Then
                    If retValue.Questions(0).Answers IsNot Nothing Then
                        If retValue.Questions(0).Answers.Count = 0 Then
                            If retValue.Questions(0).Type = DialogsQuestion.QuestionType.AutomaticFeaturesCompilation Then
                                UpdateSequenceQAPOSA(retValue.Questions(0).Id, automaticCPCompialtion)
                                retValue = GetNextDialogQuestion(retValue.Questions(0).sequenceID, myConfigPath, automaticCPCompialtion)
                            End If

                        ElseIf retValue.Questions(0).Answers.Count = 1 Then
                            ' If retValue.Questions(0).Answers(0).type = DialogsAnswer.AnswerType.Standard Then
                            givenIdSelection = retValue.Questions(0).Answers(0).sequenceId
                            If retValue.Questions(0).Type = DialogsQuestion.QuestionType.SingleOptionSelection Then
                                'myLastSelection = retValue

                                UpdateSequenceQA(givenIdSelection, retValue.Questions(0).Answers(0).idParent, retValue.Questions(0).Answers(0).id, myConfigPath)
                                ' If givenTimes = 0 Then
                                retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion)
                                'Else
                                '  retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion, givenTimes + 1)
                                ' Exit For
                            End If


                        Else
                            Dim forceCounter As Integer = 0
                            Dim ansToForce As DialogsAnswer
                            For Each ans As DialogsAnswer In retValue.Questions(0).Answers
                                If ans.forceChoise Then
                                    forceCounter += 1
                                    ansToForce = ans
                                End If
                            Next
                            If forceCounter = 1 And ansToForce IsNot Nothing Then
                                UpdateSequenceQA(ansToForce.sequenceId, ansToForce.idParent, ansToForce.id, myConfigPath)

                                retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion)
                            End If
                        End If
                    ElseIf retValue.Questions(0).FeatureToAcquire IsNot Nothing Then
                        If retValue.Questions(0).FeatureToAcquire.Count > 0 Then
                            Dim allValued As Boolean = True
                            Dim childId As String = ""
                            For Each input As DialogsInput In retValue.Questions(0).FeatureToAcquire
                                If input.CA_TextValue = "" Then
                                    allValued = False
                                Else
                                    childId = childId & String.Format("CA_{0}.{1}:{2};", input.idParent, input.CA_IdTable, input.CA_TextValue)
                                End If
                            Next
                            If allValued Then
                                UpdateSequenceCA(retValue.Questions(0).Id, childId, myConfigPath)

                                retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion)
                            End If
                        End If

                    ElseIf retValue.Questions(0).ExternalReferenceToAcquire IsNot Nothing Then
                        If retValue.Questions(0).ExternalReferenceToAcquire.Count > 0 Then
                            Dim childIdValue As String = ""
                            For Each input As DialogsExternalTable In retValue.Questions(0).ExternalReferenceToAcquire
                                If input.TE_TextValue <> "" Then

                                    childIdValue = childIdValue & String.Format("TE_{0}_{1}:{2};", input.idParent, input.sequenceId, input.TE_TextValue)
                                    UpdateSequenceTE(retValue.Questions(0).Id, input.idParent, childIdValue)
                                    retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion)

                                End If

                            Next

                        End If
                    End If

                End If
            End If



            If retValue IsNot Nothing Then
                Exit For
            End If

        Next

        If retValue IsNot Nothing Then
            If retValue.Questions Is Nothing Then
                Dim myDataSet As DataSet = SaveDialogTables()
                DataProvider.DataProvider.SavePersistentDialog(myDataSet)
            End If
        End If
        Return retValue












    End Function

    Public Function GetNextDialogQuestion2(ByVal givenIdSelection As String, ByVal givenId As String, ByVal myConfigPath As String, ByVal automaticCPCompialtion As Generic.Dictionary(Of String, Double)) As Dialogs

        Dim myTableOption As QAOptionDataTable
        Dim myTMPTableOption As QAOptionDataTable
        Dim lqResult
        Dim lqResultTMP
        Dim retValue As Dialogs = Nothing
        Dim dummyRow As QAOptionRow = Nothing
        Dim pathNode As String = ""
        Dim canBeProcessedFlag As Boolean = False
        Dim myOutPut As New Generic.List(Of QAOptionRow)
        Dim countProcessed As Integer = 0
        myTableOption = myDataModelHelper.GetDialogTable(TableName.QAOption)
        write(myTableOption)
        If givenIdSelection = "" Then
            givenIdSelection = "0"
        End If
        lqResult = From qalist In myTableOption
                   Where qalist.level = 0 And qalist.number >= Integer.Parse(givenIdSelection) And qalist.id <> givenId
                   Select qalist Order By qalist.number Ascending


        countProcessed = 0
        Dim indexCount As Integer = 0
        For Each qalist As QAOptionRow In lqResult
            If qalist.selected Then
                Continue For
            End If
            'myTMPTableOption = myDataModelHelper.GetDialogTable(TableName.QAOption)
            'lqResultTMP = From qalistTMP In myTMPTableOption _
            '          Where qalistTMP.id = qalist.id
            '          Select qalistTMP Order By qalistTMP.number Ascending

            'Dim continueFor As Boolean = False
            'For Each qalistTMP As QAOptionRow In lqResultTMP
            '    If qalistTMP.selected = True And qalist.selected = False Then
            '        continueFor = True
            '        Exit For
            '    End If
            'Next
            'If continueFor Then
            '    Continue For
            'End If
            retValue = Nothing
            countProcessed += 1
            retValue = myDataModelHelper.FillDialog(qalist)
            If retValue IsNot Nothing Then
                If Not ValidateDialog(retValue, myTableOption, myConfigPath) Then
                    retValue = Nothing
                    Continue For
                End If
            End If

            If retValue IsNot Nothing Then
                If retValue.Questions IsNot Nothing Then
                    'SetCPByFunction(retValue)
                    SetCAByFunction(retValue)
                    SetTEAutomatic(retValue, myConfigPath)
                    Dim myListDefault As String = ""
                    If retValue.Questions(0).Answers IsNot Nothing Then
                        For idx As Integer = 0 To retValue.Questions(0).Answers.Count - 1
                            If retValue.Questions(0).Answers(idx).forceChoise Then
                                myListDefault &= retValue.Questions(0).Answers(idx).sequenceId & ";"
                            End If

                        Next
                        If myListDefault <> "" Then
                            'For idx As Integer = 0 To retValue.Questions(0).Answers.Count - 1
                            For idx As Integer = retValue.Questions(0).Answers.Count - 1 To 0 Step -1 '0 To retValue.Questions(0).Answers.Count - 1
                                If Not myListDefault.Contains(retValue.Questions(0).Answers(idx).sequenceId & ";") Then
                                    retValue.Questions(0).Answers.RemoveAt(idx)
                                    If idx = retValue.Questions(0).Answers.Count - 1 Then
                                        Exit For
                                    End If
                                    idx = idx - 1
                                End If
                            Next

                        End If
                    End If


                End If
            End If

            If retValue IsNot Nothing Then
                If retValue.Questions IsNot Nothing Then
                    If retValue.Questions(0).Answers IsNot Nothing Then
                        If retValue.Questions(0).Answers.Count = 0 Then
                            If retValue.Questions(0).Type = DialogsQuestion.QuestionType.AutomaticFeaturesCompilation Then
                                UpdateSequenceQAPOSA(retValue.Questions(0).Id, automaticCPCompialtion)
                                retValue = GetNextDialogQuestion(retValue.Questions(0).sequenceID, myConfigPath, automaticCPCompialtion)
                            End If

                        ElseIf retValue.Questions(0).Answers.Count = 1 Then
                            ' If retValue.Questions(0).Answers(0).type = DialogsAnswer.AnswerType.Standard Then
                            givenIdSelection = retValue.Questions(0).Answers(0).sequenceId
                            If retValue.Questions(0).Type = DialogsQuestion.QuestionType.SingleOptionSelection Then
                                'myLastSelection = retValue

                                UpdateSequenceQA(givenIdSelection, retValue.Questions(0).Answers(0).idParent, retValue.Questions(0).Answers(0).id, myConfigPath)
                                ' If givenTimes = 0 Then
                                retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion)
                                'Else
                                '  retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion, givenTimes + 1)
                                ' Exit For
                            End If


                        Else
                            Dim forceCounter As Integer = 0
                            Dim ansToForce As DialogsAnswer
                            For Each ans As DialogsAnswer In retValue.Questions(0).Answers
                                If ans.forceChoise Then
                                    forceCounter += 1
                                    ansToForce = ans
                                End If
                            Next
                            If forceCounter = 1 And ansToForce IsNot Nothing Then
                                UpdateSequenceQA(ansToForce.sequenceId, ansToForce.idParent, ansToForce.id, myConfigPath)

                                retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion)
                            End If
                        End If
                    ElseIf retValue.Questions(0).FeatureToAcquire IsNot Nothing Then
                        If retValue.Questions(0).FeatureToAcquire.Count > 0 Then
                            Dim allValued As Boolean = True
                            Dim childId As String = ""
                            For Each input As DialogsInput In retValue.Questions(0).FeatureToAcquire
                                If input.CA_TextValue = "" Then
                                    allValued = False
                                Else
                                    childId = childId & String.Format("CA_{0}.{1}:{2};", input.idParent, input.CA_IdTable, input.CA_TextValue)
                                End If
                            Next
                            If allValued Then
                                UpdateSequenceCA(retValue.Questions(0).Id, childId, myConfigPath)

                                retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion)
                            End If
                        End If

                    ElseIf retValue.Questions(0).ExternalReferenceToAcquire IsNot Nothing Then
                        If retValue.Questions(0).ExternalReferenceToAcquire.Count > 0 Then
                            Dim childIdValue As String = ""
                            For Each input As DialogsExternalTable In retValue.Questions(0).ExternalReferenceToAcquire
                                If input.TE_TextValue <> "" Then

                                    childIdValue = childIdValue & String.Format("TE_{0}_{1}:{2};", input.idParent, input.sequenceId, input.TE_TextValue)
                                    UpdateSequenceTE(retValue.Questions(0).Id, input.idParent, childIdValue)
                                    retValue = GetNextDialogQuestion(givenIdSelection, myConfigPath, automaticCPCompialtion)

                                End If

                            Next

                        End If
                    End If

                End If
            End If



            If retValue IsNot Nothing Then
                Exit For
            End If

        Next

        If retValue IsNot Nothing Then
            If retValue.Questions Is Nothing Then
                Dim myDataSet As DataSet = SaveDialogTables()
                DataProvider.DataProvider.SavePersistentDialog(myDataSet)
            End If
        End If
        Return retValue












    End Function

    Public Function IsAFunction(givenVal As String, myConfigPath As String) As Boolean

        If System.IO.Directory.Exists(myConfigPath) Then
            For Each file In IO.Directory.GetFiles(myConfigPath)
                Dim myStringFile As String = (givenVal & ".vb").ToLower
                If System.IO.Path.GetFileName(file).ToLower.EndsWith(myStringFile) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    Public Function GetFunctionValue(myConfigPath As String, givenValue As String) As String

        Dim retValue As String = ""
        Dim myFunction As String = ""

        If System.IO.Directory.Exists(myConfigPath) Then
            For Each file In IO.Directory.GetFiles(myConfigPath)
                Dim myStringFile As String = (givenValue & ".vb").ToLower
                If System.IO.Path.GetFileName(file).ToLower.EndsWith(myStringFile) Then
                    myFunction = System.IO.Path.GetFileNameWithoutExtension(file)
                    Exit For
                End If
            Next
        End If
        If myFunction <> "" Then
            Try
                retValue = CalculatFunctionValue(myFunction)
            Catch ex As Exception
                Throw New Exception("Configurator Manager Exception: " & ex.Message, ex.InnerException)
            End Try
        End If


        Return retValue

    End Function

    Public Function GetTEDataTable(ByVal givenIdSelection As String, ByVal givenTableCode As String, myConfigPath As String) As DataTable

        Dim myTableOption As QAExternalReferenceDataTable
        Dim lqResult
        Dim retValue As Dialogs = Nothing
        Dim dummyRow As QAOptionRow = Nothing
        Dim pathNode As String = ""
        Dim canBeProcessedFlag As Boolean = False
        Dim myOutPut As New Generic.List(Of QAOptionRow)
        Dim showCols As String = ""
        Dim outCols As String = ""
        myTableOption = myDataModelHelper.GetDialogTable(TableName.QAExternalReference)

        If givenIdSelection = "" Then
            givenIdSelection = "0"
        End If
        'qalist.number = Integer.Parse(givenIdSelection) And 
        lqResult = From qalist In myTableOption
                   Where qalist.code = givenTableCode
                   Select qalist Order By qalist.number Ascending

        Dim colsName As String = ""
        Dim colsOp As String = ""
        Dim colsValue As String = ""
        Dim myRowNumber As Integer = -1
        For Each tmpRow As QAExternalReferenceRow In lqResult
            Dim codeVerify As String = ""
            codeVerify = tmpRow.qaId & "_" & tmpRow.number
            If codeVerify = givenIdSelection Then
                If tmpRow.columnNameFilter <> "" And tmpRow.columnOperatorFilter <> "" And tmpRow.columnValueFilter <> "" Then
                    For idx As Integer = 0 To tmpRow.columnNameFilter.Split(";").Length - 1
                        Dim colName As String = tmpRow.columnNameFilter.Split(";")(idx)
                        Dim colOperator As String = tmpRow.columnOperatorFilter.Split(";")(idx)
                        Dim colValue As String = tmpRow.columnValueFilter.Split(";")(idx)

                        If colName <> "" And colOperator <> "" And colValue <> "" Then
                            If IsAFunction(colValue, myConfigPath) Then
                                colValue = GetFunctionValue(myConfigPath, colValue)
                            End If
                            colsName = IIf(colsName <> "", colsName & ";" & colName, colName)
                            colsOp = IIf(colsOp <> "", colsOp & ";" & colOperator, colOperator)
                            colsValue = IIf(colsValue <> "", colsValue & ";" & colValue, colValue)
                        End If
                    Next

                End If
                If IsNumeric(tmpRow.showRowNumber) Then
                    myRowNumber = tmpRow.showRowNumber
                Else
                    myRowNumber = -1
                End If

                showCols = tmpRow.columnsView
                outCols = tmpRow.columnOutput
                If (Not showCols.ToUpper().Contains(outCols)) Then
                    showCols = outCols & "," & showCols
                End If
                Exit For
            End If

        Next



        Return DataProvider.DataProvider.GetTEDataTable(givenTableCode, colsName.Split(";"), colsOp.Split(";"), colsValue.Split(";"), showCols, myRowNumber)


        Return Nothing











    End Function


    Public Function GetAllFeatureSelected() As DataTable
        Dim lqResult
        Dim myTableOption As QAOptionDataTable
        Dim myTableCP As QAFeaturesDataTable
        Dim retValue As New DataTable("DIALOG-FEATURES")
        retValue.Columns.Add("CODE")
        retValue.Columns.Add("DESCRIPTION")
        retValue.Columns.Add("UM")
        retValue.Columns.Add("VALUE")

        myTableOption = myDataModelHelper.GetDialogTable(TableName.QAOption)
        myTableCP = myDataModelHelper.GetDialogTable(TableName.QAFeatures)
        lqResult = From qalist In myTableOption
                   Where qalist.selected = True And qalist.level > 0 And qalist.hasCP = True
                   Select qalist

        For Each row As QAOptionRow In lqResult
            Dim lq2 = From cp In myTableCP
                      Where cp.dtRefNumber = row.number
                      Select cp
            For Each tmpCp As QAFeaturesRow In lq2
                Dim tmpRow As DataRow = retValue.NewRow
                tmpRow(0) = tmpCp.Code
                tmpRow(1) = tmpCp.Description
                tmpRow(2) = tmpCp.UM
                tmpRow(3) = tmpCp.Value
                retValue.Rows.Add(tmpRow)
            Next

        Next

        Return retValue
    End Function


    Public Function GetAllCADFeatureSelected() As DataTable
        Dim lqResult
        Dim myTableOption As QAOptionDataTable
        Dim myTableCP As QAFeaturesDataTable
        Dim retValue As New DataTable("DIALOG-CADINPUT")
        retValue.Columns.Add("CAD_REF")
        retValue.Columns.Add("CAD_VALUE")

        myTableOption = myDataModelHelper.GetDialogTable(TableName.QAOption)
        myTableCP = myDataModelHelper.GetDialogTable(TableName.QAFeatures)
        lqResult = From qalist In myTableOption
                   Where qalist.selected = True And qalist.CAD_REF.Trim <> ""
                   Select qalist

        For Each row As QAOptionRow In lqResult

            Dim tmpRow As DataRow = retValue.NewRow
            tmpRow(0) = row.CAD_REF
            tmpRow(1) = row.CAD_VALUE
            retValue.Rows.Add(tmpRow)

        Next

        Return retValue
    End Function


#End Region

    ''#Region "VALIDAZIONI"

    ''    Private Sub ValidateConnection()
    ''        If myQADataSet Is Nothing Then
    ''            Throw New Exception("Connection lost CODE:10250")
    ''        ElseIf myConfiguratorStep Is Nothing Then
    ''            Throw New Exception("Connection lost CODE:10251")
    ''        ElseIf myCurrentDialog Is Nothing Then
    ''            Throw New Exception("Connection lost CODE:10252")
    ''        End If
    ''    End Sub

    ''#End Region
    ''    Public Function GetNextRootStepDialog()
    ''        Dim myTableDialogs As QAOptionDataTable
    ''        Dim lqResult

    ''        myTableDialogs = myDialogsDataSet.Tables(TableName.QAOption.ToString)
    ''        lqResult = From qalist In myTableDialogs.AsEnumerable() Select qalist _
    ''                              Where (qalist.parentid = "" And qalist.processed = False And qalist.avoid = False And qalist.excluded = False) _
    ''                              Order By qalist.level Descending


    ''        For Each qalist In lqResult
    ''            qalist.processed = False
    ''            qalist.avoid = True
    ''        Next


    ''    End Function



    ''    Public Function GetNextDialogQuestion_2(ByRef currentQADialog As Dialogs, givenIdSelection As String, givenParentId As String, givenChildId As String, Optional isRecursive As Boolean = False) As Dialogs

    ''        ' Return GetNextDialogQuestion_2(currentQADialog, givenParentId, givenChildId, isRecursive)
    ''        ''Exit Function

    ''        Dim retValue As Dialogs = Nothing
    ''        Dim parentId As String = ""
    ''        Dim selectedId As String = ""
    ''        Dim dummyQuestion As DialogsQuestion
    ''        Dim dummyQuery As String = ""
    ''        Dim dummySelectRow() As QAOptionRow
    ''        Dim myTableDialogs As QAOptionDataTable

    ''        myTableDialogs = myDialogsDataSet.Tables(TableName.QAOption.ToString)
    ''        If Not isRecursive Then
    ''            myBypassQuestion = New Generic.List(Of String)
    ''        End If


    ''        If givenParentId = "" And givenChildId = "" Then
    ''            retValue = GetNextStepDialogSequence(givenChildId)
    ''        Else
    ''            dummyQuery = String.Format("id='{0}' and parentid='{1}'", givenChildId, givenParentId)
    ''            dummySelectRow = myTableDialogs.Select(dummyQuery)
    ''            If dummySelectRow.Length > 0 Then
    ''                retValue = FillDialog(dummySelectRow(0))
    ''                If retValue IsNot Nothing Then
    ''                    If retValue.Questions Is Nothing Then
    ''                        retValue = GetNextStepDialogSequence(givenChildId)
    ''                    End If
    ''                End If

    ''            End If
    ''        End If


    ''        If retValue IsNot Nothing Then
    ''            If Not ValidateDialog(retValue) Then
    ''                For Each dummyQuestion In retValue.Questions
    ''                    If Not myBypassQuestion.Contains(dummyQuestion.Id) Then
    ''                        myBypassQuestion.Add(dummyQuestion.Id)
    ''                    End If
    ''                Next
    ''                retValue = GetNextDialogQuestion(currentQADialog, parentId, selectedId, True)
    ''            End If
    ''        End If
    ''        If retValue IsNot Nothing Then

    ''            If retValue.Questions IsNot Nothing Then
    ''                For Each dummyQuestion In retValue.Questions
    ''                    If dummyQuestion.Answers IsNot Nothing Then
    ''                        If dummyQuestion.Answers.Count = 1 Then

    ''                            parentId = dummyQuestion.Answers(0).idParent
    ''                            selectedId = dummyQuestion.Answers(0).id
    ''                            givenIdSelection = dummyQuestion.Answers(0).sequenceId
    ''                            If dummyQuestion.Type = DialogsQuestion.QuestionType.SingleAnswerSelection Then
    ''                                UpdateSequenceQA(currentQADialog, givenIdSelection, parentId, selectedId, False)
    ''                                retValue = GetNextDialogQuestion(currentQADialog, parentId, selectedId, isRecursive)
    ''                            End If



    ''                        End If
    ''                    End If
    ''                Next

    ''            End If

    ''        End If

    ''        Return retValue

    ''    End Function
    ''    ''' <summary>
    ''    ''' ' -> a
    ''    ''' a -> b
    ''    ''' a -> c
    ''    ''' b -> d
    ''    ''' b -> e
    ''    ''' d -> f
    ''    ''' a -> b -> d -> f
    ''    ''' </summary>
    ''    ''' <param name="givenRow"></param>
    ''    ''' <param name="givenDialog"></param>
    ''    ''' <returns></returns>
    ''    ''' <remarks></remarks>
    ''    Private Function CanBeProcessed(givenRow As QAOptionRow, ByRef givenDialog As Dialogs) As Boolean
    ''        Dim retValue As Boolean = True
    ''        Dim processedQuestion() As QASelectionsRow
    ''        Dim processedQuestionParent() As QASelectionsRow
    ''        Dim dummyRow() As QAOptionRow

    ''        Dim dummyDialog As Dialogs


    ''        processedQuestion = myTableSelections.Select(String.Format("parentid='{0}'", givenRow.id))
    ''        If processedQuestion.Length > 0 Then
    ''            retValue = False
    ''        Else
    ''            '''''''''ID CORRENTE NON TROVATO NELLE SELEZIONI
    ''            '''''''''SI VA A MONITORARE SE IL PADRE ESISTE
    ''            '''''''''SE IL PADRE E' IN SELEZIONE SI VERIFICA SE PREVEDE RISPOSTA MULTIPLA
    ''            '''''''''ALTRIMENTI SI PASSA ALLO STEP SUCCESSIVO
    ''            processedQuestion = myTableSelections.Select(String.Format("childid like '%{0}%'", givenRow.id))
    ''            processedQuestionParent = myTableSelections.Select(String.Format("parentid='{0}'", givenRow.parentid))
    ''            If processedQuestionParent.Length = 0 And processedQuestion.Length = 0 Then
    ''                dummyDialog = FillDialog(givenRow)
    ''            ElseIf processedQuestion.Length = 0 And processedQuestionParent.Length > 0 Then
    ''                dummyRow = myDialogsDataSet.Tables(TableName.QAOption.ToString).Select("id='" & givenRow.parentid & "'")
    ''                If dummyRow IsNot Nothing Then
    ''                    If dummyRow.Length = 1 Then
    ''                        If dummyRow(0).selectionType Then
    ''                            dummyDialog = FillDialog(givenRow)
    ''                        Else
    ''                            retValue = False
    ''                        End If
    ''                    End If

    ''                End If
    ''            ElseIf processedQuestion.Length > 0 And processedQuestionParent.Length > 0 Then
    ''                dummyRow = myDialogsDataSet.Tables(TableName.QAOption.ToString).Select("parentid='" & givenRow.id & "'")
    ''                If dummyRow.Length = 0 Then
    ''                    retValue = False
    ''                ElseIf dummyRow.Length = 1 Then
    ''                    dummyDialog = FillDialog(givenRow)
    ''                    If ValidateDialog(dummyDialog) Then
    ''                        If dummyDialog.Questions(0).Type = DialogsQuestion.QuestionType.SingleAnswerSelection Then
    ''                            UpdateSequenceQA(dummyDialog, dummyRow(0).number, givenRow.id, dummyRow(0).id, False)
    ''                            dummyDialog = Nothing
    ''                            retValue = False
    ''                        End If
    ''                    End If
    ''                ElseIf dummyRow.Length > 1 Then
    ''                    dummyDialog = FillDialog(givenRow)
    ''                End If


    ''            End If



    ''            If dummyDialog IsNot Nothing Then
    ''                If Not ValidateDialog(dummyDialog) Then
    ''                    retValue = False
    ''                Else
    ''                    givenDialog = dummyDialog
    ''                End If
    ''            Else
    ''                retValue = False
    ''            End If

    ''        End If

    ''        Return retValue

    ''    End Function


    ''    Public Function UpdateSequenceQA(ByRef currentQADialog As Dialogs, givenSelectionId As String, givenParentId As String, givenChildId As String, ByVal isMultiAnswer As Boolean, Optional reloadCurrentModel As Boolean = True) As Dialogs
    ''        Return UpdateSequenceQA_2(currentQADialog, givenSelectionId, givenParentId, givenChildId, isMultiAnswer, reloadCurrentModel)

    ''        Dim myQASelections() As QASelectionsRow
    ''        Dim myQADialogQueryResultParent() As QAOptionRow
    ''        Dim myQADialogQueryResultChild() As QAOptionRow
    ''        Dim query As String
    ''        Dim tmpRow As QASelectionsRow
    ''        Dim rowChild As QAOptionRow
    ''        Dim rowMaster As QAOptionRow
    ''        Dim myListToRemove As List(Of QASelectionsRow)
    ''        Dim tmpConditionsList As Generic.List(Of DialogsCondition)
    ''        Dim tmpCondition As String
    ''        Dim oldId As String = ""
    ''        Dim myTableDialogs As QAOptionDataTable

    ''        myTableDialogs = myDialogsDataSet.Tables(TableName.QAOption.ToString)

    ''        query = String.Format("id='{0}'", givenParentId)
    ''        myQADialogQueryResultParent = myTableDialogs.Select(query)

    ''        If myQADialogQueryResultParent.Length > 0 Then
    ''            rowMaster = myQADialogQueryResultParent(0)
    ''        Else
    ''            rowMaster = Nothing
    ''        End If
    ''        query = String.Format("id='{0}'", givenChildId)
    ''        myQADialogQueryResultChild = myTableDialogs.Select(query)

    ''        If myQADialogQueryResultChild.Length > 0 Then
    ''            rowChild = myQADialogQueryResultChild(0)
    ''        Else
    ''            rowChild = Nothing

    ''        End If


    ''        If rowMaster IsNot Nothing And rowChild IsNot Nothing Then
    ''            query = String.Format("parentid='{0}'", givenParentId)
    ''            myQASelections = myTableSelections.Select(query, "number ASC")
    ''            If myQASelections.Length > 0 Then
    ''                tmpRow = myQASelections(0)
    ''                If tmpRow.childid <> givenChildId And Not isMultiAnswer Then
    ''                    oldId = tmpRow.childid
    ''                    tmpRow.childid = givenChildId
    ''                    tmpRow.number = rowMaster.number
    ''                    tmpRow.pathSelection = "|" & givenParentId & "|" & givenChildId & "|"
    ''                    tmpRow.Variables = rowChild.features
    ''                    tmpRow.condition_ref = rowChild.conditions
    ''                    tmpRow.AcceptChanges()

    ''                    ''DEVO ELIMINARE TUTTE LE SCELTE EFFETTUATE
    ''                    myListToRemove = New List(Of QASelectionsRow)
    ''                    ClearOldQA(oldId, Integer.Parse(tmpRow.number))
    ''                    For Each tmpRow In myTableSelections.Rows
    ''                        If Integer.Parse(tmpRow.number) > Integer.Parse(rowMaster.number) Then
    ''                            tmpCondition = tmpRow.condition_ref
    ''                            If tmpCondition <> "" Then
    ''                                'tmpConditionsList = FillCondition(tmpCondition)
    ''                                'If Not ValidateConditions(tmpConditionsList) Then
    ''                                '    myListToRemove.Add(tmpRow)
    ''                                'End If

    ''                            End If
    ''                        End If
    ''                    Next

    ''                    For Each tmpRow In myListToRemove
    ''                        myTableSelections.Rows.Remove(tmpRow)
    ''                    Next
    ''                Else
    ''                    tmpRow.childid = tmpRow.childid & "|" & givenChildId
    ''                    tmpRow.pathSelection = tmpRow.pathSelection & givenChildId & "|"
    ''                    tmpRow.Variables = tmpRow.Variables & "|" & rowChild.features
    ''                    tmpRow.condition_ref = tmpRow.condition_ref & rowChild.conditions
    ''                    tmpRow.AcceptChanges()
    ''                End If

    ''            Else
    ''                tmpRow = myTableSelections.NewRow
    ''                tmpRow.number = rowMaster("number")
    ''                tmpRow.parentid = givenParentId
    ''                tmpRow.childid = givenChildId
    ''                tmpRow.pathSelection = "|" & givenParentId & "|" & givenChildId & "|"
    ''                ' tmpRow.CA_TE_SelValue = inputValue
    ''                tmpRow.Variables = rowChild.features
    ''                '  tmpRow.Conditions = rowChild.conditions
    ''                myTableSelections.Rows.Add(tmpRow)

    ''            End If
    ''            If reloadCurrentModel Then
    ''                GetFullUsedQA(currentQADialog)
    ''            End If

    ''        End If
    ''        Return currentQADialog
    ''    End Function

    Public Function ValidateCA(ByVal givenCANumnber As Integer, ByVal givenValue As String, ByVal myConfigPath As String) As String
        Dim myTable As QAInputDataTable
        Dim lqResult
        Dim retValue As String
        Dim dummyRow As QAInputRow
        Dim expression As String = ""
        Dim myEval As New ExpressionEvaluator.Evaluator
        Dim message As String = ""
        myTable = myDataModelHelper.GetDialogTable(TableName.QAInput)

        lqResult = From input In myTable
                   Where input.number = givenCANumnber
                   Select input

        For Each item As QAInputRow In lqResult
            dummyRow = item
            Exit For
        Next
        If dummyRow IsNot Nothing Then
            ' UM="MM" MINVAL="140" MAXVAL="800" TYPE="N"
            If dummyRow.InputType = "N" Then
                If Not IsNumeric(givenValue) Then
                    retValue = "Input value for " & dummyRow.description & " is not a number"
                End If
            End If
            If IsNumeric(dummyRow.minVal) And IsNumeric(dummyRow.maxVal) And retValue = "" Then
                expression = String.Format("({0}>={1}) AND ({0}<={2})", givenValue, dummyRow.minVal, dummyRow.maxVal)
                message = String.Format("- {2}: Please enter a value between {0} and {1}", dummyRow.minVal, dummyRow.maxVal, dummyRow.description)
            ElseIf IsNumeric(dummyRow.minVal) And ("" & dummyRow.maxVal) = "" And retValue = "" Then
                expression = String.Format("({0}>={1}", givenValue, dummyRow.minVal)
                message = String.Format("- {1}: Please enter a value greater than {0}", dummyRow.minVal, dummyRow.description)
            ElseIf IsNumeric(dummyRow.maxVal) And ("" & dummyRow.minVal) = "" And retValue = "" Then
                expression = String.Format("{0}<={1}", givenValue, dummyRow.maxVal)
                message = String.Format("- {, dummyRow.description}: Please enter a value less than {0}", dummyRow.maxVal, dummyRow.description)
            Else
                'GESTIONE MIN - MAX COME FUNZIONE
                Dim minValFunction As String = ""
                Dim maxValFunction As String = ""
                If Not IsNumeric(dummyRow.minVal) And IsAFunction(dummyRow.minVal, myConfigPath) Then
                    minValFunction = CalculatFunctionValue(dummyRow.minVal)
                End If
                If Not IsNumeric(dummyRow.maxVal) And IsAFunction(dummyRow.maxVal, myConfigPath) Then
                    maxValFunction = CalculatFunctionValue(dummyRow.maxVal)
                End If
                If IsNumeric(dummyRow.minVal) Then
                    minValFunction = dummyRow.minVal
                End If
                If IsNumeric(dummyRow.maxVal) Then
                    maxValFunction = dummyRow.maxVal
                End If
                If IsNumeric(minValFunction) And IsNumeric(maxValFunction) And retValue = "" Then
                    expression = String.Format("({0}>={1}) AND ({0}<={2})", givenValue, minValFunction, maxValFunction)
                    message = String.Format("- {2}: Please enter a value between {0} and {1}", minValFunction, maxValFunction, dummyRow.description)
                ElseIf IsNumeric(minValFunction) And ("" & maxValFunction) = "" And retValue = "" Then
                    expression = String.Format("({0}>={1}", givenValue, minValFunction)
                    message = String.Format("- {1}: Please enter a value greater than {0}", minValFunction, dummyRow.description)
                ElseIf IsNumeric(maxValFunction) And ("" & minValFunction) = "" And retValue = "" Then
                    expression = String.Format("{0}<={1}", givenValue, maxValFunction)
                    message = String.Format("- {, dummyRow.description}: Please enter a value less than {0}", maxValFunction, dummyRow.description)
                End If
            End If
        End If
        retValue = "OK"
        If expression <> "" Then
            If myEval.Eval(expression).ToString().ToLower = "false" Then
                retValue = "Constraint Violation " & message
            End If
        End If
        Return retValue

    End Function


    Public Function UpdateSequenceCA2(ByVal givenParentId As String, ByVal inputValue As String, givenConfigPath As String) As Dialogs


        Dim myTableDialogs As QAOptionDataTable
        Dim myTableInput As QAInputDataTable
        Dim index As Integer = -1
        myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)
        myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)

        Dim lqResult = From qalist In myTableDialogs
                       Where qalist.id = givenParentId And qalist.level = 0
                       Select qalist

        Dim lqResult2 = From qalist In myTableDialogs
                        Where qalist.parentid = givenParentId
                        Select qalist

        Dim myListCodeChanger As New Generic.List(Of String)
        Dim givenChildId As String

        For Each qalist As QAOptionRow In lqResult
            givenChildId = qalist.id
            index = qalist.number
            qalist.selected = True
            qalist.AcceptChanges()
            Dim childsCA As Boolean = False
            For Each qalist2 As QAOptionRow In lqResult2
                If qalist2.hasCA Then
                    childsCA = True
                    Dim lqResult3 = From input In myTableInput
                                    Where input.dtRefNumber = qalist2.number
                                    Select input
                    For Each tmpInput As QAInputRow In lqResult3
                        For Each code As String In Regex.Split(inputValue, VALSEP) ' inputValue.Split(";")
                            Dim keyRow As String = "CA_" & tmpInput.qaId & "." & tmpInput.number
                            If code.ToUpper.Contains(keyRow.ToUpper) Then
                                Dim dummyValue As String = Regex.Split(code, CODSEP)(1).Trim
                                If tmpInput.value <> dummyValue And tmpInput.value.Trim <> "" Then
                                    If Not myListCodeChanger.Contains(tmpInput.code) Then
                                        myListCodeChanger.Add(tmpInput.code)
                                    End If
                                End If
                                tmpInput.value = dummyValue
                                qalist2.selected = True
                                tmpInput.AcceptChanges()
                            End If
                        Next
                    Next

                    qalist2.AcceptChanges()
                End If
                If qalist2.hasCP Then

                    Dim myList As New List(Of String)
                    UpdateCP(qalist2, myList)
                End If

            Next
            If childsCA And qalist.hasCP Then
                Dim myList As New List(Of String)
                UpdateCP(qalist, myList)
            End If
        Next

        myTableInput.AcceptChanges()
        myTableDialogs.AcceptChanges()

        If index >= 0 Then
            'dobbiamo annullare tutte le domande che non sono più compatibili
            Dim myListCPUpdated As New Generic.List(Of String)
            myTableDialogs.AcceptChanges()
            myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)

            Dim lqResult3 = From qalist3 In myTableDialogs
                            Where qalist3.selected = True And qalist3.number > index And qalist3.id <> givenChildId
                            Select qalist3

            Dim i As Integer = 0
            Dim myNewList As New Generic.List(Of String)
            For Each qalist3 As QAOptionRow In lqResult3
                'Dim file As String = "c:\temp\table" & i.ToString & ".xml"
                'i += 1
                'myTableDialogs.WriteXml(file)
                Dim myDumDialog As Dialogs = myDataModelHelper.FillDialog(qalist3)
                If myDumDialog IsNot Nothing Then
                    If Not ValidateDialog(myDumDialog, myTableDialogs, givenConfigPath) Then
                        'annullo la risposta data
                        qalist3.selected = False
                        qalist3.AcceptChanges()
                        'annullo le CP valorizzate
                        ResetCP(qalist3, myListCPUpdated)
                        myTableDialogs.AcceptChanges()
                        'annullo tutte le risposte scaturite da questa domanda
                        'If qalist2.parentid = "" Then
                        '    RemoveChildSelection(qalist2, myTableDialogs, True)
                        'Else
                        '    RemoveChildSelection(qalist2, myTableDialogs, False)
                        'End If
                        AnnullaRisposta(qalist3, myTableDialogs, myListCPUpdated)
                    Else
                        Try
                            'If myNewList.Contains(myDumDialog.Questions(0).Id) Then
                            '    Continue For
                            'Else
                            '    myNewList.Add(myDumDialog.Questions(0).Id)
                            'End If
                            If myDumDialog.Questions IsNot Nothing Then
                                SetCAByFunction(myDumDialog)
                                SetTEAutomatic(myDumDialog, givenConfigPath)
                            End If
                            If myDumDialog.Questions(0).Answers IsNot Nothing Then
                                If myDumDialog.Questions(0).Answers.Count = 1 Then
                                    qalist3.selected = True
                                    qalist3.AcceptChanges()
                                    'annullo le CP valorizzate
                                    UpdateCP(qalist3, myListCPUpdated)

                                    Dim lqResult4 = From qalist4 In myTableDialogs
                                                    Where qalist4.id = myDumDialog.Questions(0).Answers(0).id
                                                    Select qalist4
                                    For Each qalist4 As QAOptionRow In lqResult4
                                        qalist4.selected = True
                                        qalist4.AcceptChanges()
                                        'annullo le CP valorizzate
                                        UpdateCP(qalist4, myListCPUpdated)

                                    Next
                                    myTableDialogs.AcceptChanges()
                                ElseIf myDumDialog.Questions(0).Answers.Count > 1 Then
                                    Dim childSelected As Boolean = False
                                    For Each answer As DialogsAnswer In myDumDialog.Questions(0).Answers
                                        If answer.isSelected Then
                                            childSelected = True
                                        End If
                                    Next
                                    If Not childSelected Then
                                        qalist3.selected = False
                                        qalist3.AcceptChanges()
                                        'annullo le CP valorizzate
                                        ResetCP(qalist3, myListCPUpdated)
                                        myTableDialogs.AcceptChanges()
                                    End If
                                End If

                            ElseIf myDumDialog.Questions(0).ExternalReferenceToAcquire IsNot Nothing AndAlso myDumDialog.Questions(0).ExternalReferenceToAcquire.Count > 0 Then
                                Dim childIdValue As String = ""
                                For Each input As DialogsExternalTable In myDumDialog.Questions(0).ExternalReferenceToAcquire


                                    childIdValue = childIdValue & String.Format("TE_{0}_{1}:{2};", input.idParent, input.sequenceId, input.TE_TextValue)
                                    UpdateSequenceTE(myDumDialog.Questions(0).Id, input.idParent, childIdValue)

                                Next
                            End If
                        Catch ex As Exception
                            Dim c As String = ""
                            c = ex.ToString
                        End Try


                    End If
                End If
            Next
            myTableDialogs.AcceptChanges()

            myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)


        End If


        myTableInput.AcceptChanges()
        myTableDialogs.AcceptChanges()

    End Function


    Public Function UpdateSequenceCA(ByVal givenParentId As String, ByVal inputValue As String, ByVal givenConfigPath As String) As Dialogs
        Return UpdateSequenceCA2(givenParentId, inputValue, givenConfigPath)

        Dim myTableDialogs As QAOptionDataTable
        Dim myTableInput As QAInputDataTable
        Dim index As Integer = -1
        myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)
        myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)

        Dim lqResult = From qalist In myTableDialogs
                       Where qalist.id = givenParentId And qalist.level = 0
                       Select qalist

        Dim lqResult2 = From qalist In myTableDialogs
                        Where qalist.parentid = givenParentId
                        Select qalist

        Dim myListCodeChanger As New Generic.List(Of String)

        For Each qalist As QAOptionRow In lqResult
            index = qalist.number
            qalist.selected = True
            qalist.AcceptChanges()

            For Each qalist2 As QAOptionRow In lqResult2
                If qalist2.hasCA Then
                    Dim lqResult3 = From input In myTableInput
                                    Where input.dtRefNumber = qalist2.number
                                    Select input
                    For Each tmpInput As QAInputRow In lqResult3
                        For Each code As String In Regex.Split(inputValue, VALSEP)
                            Dim keyRow As String = "CA_" & tmpInput.qaId & "." & tmpInput.number
                            If code.ToUpper.Contains(keyRow) Then
                                Dim dummyValue As String = Regex.Split(code, CODSEP)(1).Trim
                                If tmpInput.value <> dummyValue And tmpInput.value.Trim <> "" Then
                                    If Not myListCodeChanger.Contains(tmpInput.code) Then
                                        myListCodeChanger.Add(tmpInput.code)
                                    End If
                                End If
                                tmpInput.value = dummyValue
                                qalist2.selected = True
                                tmpInput.AcceptChanges()
                            End If
                        Next
                    Next

                    qalist2.AcceptChanges()
                End If

            Next

        Next

        If index >= 0 Then
            lqResult2 = From qalist2 In myTableDialogs
                        Where qalist2.id <> givenParentId And qalist2.parentid <> givenParentId _
                        And qalist2.selected = True And qalist2.number > index
                        Select qalist2

            For Each qalist2 As QAOptionRow In lqResult2
                qalist2.selected = False

                Dim lqInput = From qaInput In myTableInput
                              Where qaInput.dtRefNumber = qalist2.number
                              Select qaInput

                For Each dummyInput As QAInputRow In lqInput
                    dummyInput.value = ""
                    dummyInput.AcceptChanges()
                Next

                qalist2.AcceptChanges()

            Next
        End If

        '' ''Dim minIndex As Integer = Integer.MaxValue
        '' ''If myListCodeChanger.Count > 0 Then

        '' ''    Dim tableCondition As QAConditionsDataTable = myDataModelHelper.GetDialogTable(TableName.QAConditions)
        '' ''    For Each code As String In myListCodeChanger
        '' ''        Dim lqConditions = From qaCond In tableCondition Select qaCond
        '' ''                            Where qaCond.refCode = code And qaCond.dtRefNumber >= index

        '' ''        For Each row As QAConditionsRow In lqConditions
        '' ''            If Not ValidateConditions(row.dtRefNumber, myTableDialogs) Then
        '' ''                If minIndex > row.dtRefNumber Then
        '' ''                    minIndex = row.dtRefNumber
        '' ''                End If
        '' ''            End If
        '' ''        Next


        '' ''    Next
        '' ''    If minIndex < Integer.MaxValue And minIndex > index Then
        '' ''        lqResult2 = From qalist2 In myTableDialogs
        '' ''                     Where qalist2.id <> givenParentId And qalist2.parentid <> givenParentId _
        '' ''                     And qalist2.selected = True And qalist2.number > index
        '' ''                     Select qalist2

        '' ''        For Each qalist2 As QAOptionRow In lqResult2
        '' ''            qalist2.selected = False
        '' ''            qalist2.AcceptChanges()
        '' ''        Next
        '' ''    End If
        '' ''End If
        myTableInput.AcceptChanges()
        myTableDialogs.AcceptChanges()

    End Function

    Private Function ValidateConditions(ByVal givenSeqId As Integer, ByVal myTableDialogs As QAOptionDataTable, givenConfigPath As String)
        Dim lqResul = From qaOption In myTableDialogs
                      Where qaOption.number = givenSeqId
                      Select qaOption

        For Each row As QAOptionRow In lqResul
            Dim dummyDialog As Dialogs = myDataModelHelper.FillDialog(row)
            Return ValidateDialog(dummyDialog, myTableDialogs, givenConfigPath)

        Next

        Return True
    End Function
    Public Function UpdateSequenceTE(ByVal givenParentId As String, ByVal givenChildId As String, ByVal inputValue As String) As Dialogs

        Dim myTableDialogs As QAOptionDataTable
        Dim myTableExtTable As QAExternalReferenceDataTable

        Dim index As Integer = -1
        myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)
        myTableExtTable = myDataModelHelper.GetDialogTable(TableName.QAExternalReference)

        Dim lqResult = From qalist In myTableDialogs
                       Where qalist.id = givenParentId And qalist.level = 0
                       Select qalist

        Dim lqResult2 = From qalist In myTableDialogs
                        Where qalist.parentid = givenParentId
                        Select qalist


        For Each qalist As QAOptionRow In lqResult
            index = qalist.number
            qalist.selected = True
            qalist.AcceptChanges()

            For Each qalist2 As QAOptionRow In lqResult2
                If qalist2.hasTE Then
                    Dim lqResult3 = From input In myTableExtTable
                                    Where input.dtRefNumber = qalist2.number
                                    Select input
                    For Each tmpInput As QAExternalReferenceRow In lqResult3
                        For Each code As String In inputValue.Split(";")
                            If code.ToUpper.Contains("TE_" & tmpInput.qaId) Then
                                tmpInput.selectedValue = code.Split(":")(1).Trim
                                If tmpInput.selectedValue <> "" Then
                                    qalist2.selected = True
                                Else
                                    qalist2.selected = False
                                    qalist.selected = False
                                    qalist.AcceptChanges()

                                End If

                                tmpInput.AcceptChanges()
                            End If
                        Next
                    Next
                    qalist2.AcceptChanges()
                End If

            Next

        Next

        'If index >= 0 Then
        '    lqResult2 = From qalist2 In myTableDialogs
        '              Where qalist2.id <> givenParentId And qalist2.parentid <> givenParentId _
        '              And qalist2.selected = True And qalist2.number > index
        '              Select qalist2

        '    For Each qalist2 As QAOptionRow In lqResult2
        '        qalist2.selected = False

        '        Dim lqInput = From qaInput In myTableExtTable
        '                     Where qaInput.dtRefNumber = qalist2.number
        '                     Select qaInput

        '        For Each dummyInput As QAExternalReferenceRow In lqInput
        '            dummyInput.selectedValue = ""
        '            dummyInput.AcceptChanges()
        '        Next

        '        qalist2.AcceptChanges()

        '    Next
        'End If

        myTableExtTable.AcceptChanges()
        myTableDialogs.AcceptChanges()
    End Function


    Private Function ValidateDialog(ByRef givenDialog As Dialogs, ByRef myTableOption As QAOptionDataTable, givenConfigPath As String)
        Dim retvalue As Boolean = False
        Dim expression As String = ""
        Dim dummyquestion As DialogsQuestion
        Dim dummyAnswer As DialogsAnswer
        Dim dummyCondition As DialogsCondition

        Dim myEval As New ExpressionEvaluator.Evaluator
        Dim ansToRomove As New List(Of DialogsAnswer)
        If givenDialog IsNot Nothing Then
            'VALIDAZIONE DELLE RISPOSTE
            If givenDialog.Questions IsNot Nothing Then
                For Each dummyquestion In givenDialog.Questions
                    If dummyquestion IsNot Nothing Then

                        If dummyquestion.Conditions IsNot Nothing Then
                            For idx As Integer = 0 To dummyquestion.Conditions.Count - 1
                                dummyCondition = dummyquestion.Conditions(idx)

                                If ValidateCondition(dummyquestion.sequenceID, dummyCondition, myTableOption, givenConfigPath) Then
                                    expression &= "(1=1)"
                                Else
                                    expression &= "(1=2)"
                                End If
                                If idx < dummyquestion.Conditions.Count - 1 Then
                                    If dummyCondition.RefOperator = "" Then
                                        dummyCondition.RefOperator = "AND"
                                    End If
                                    expression = expression & " " & dummyCondition.RefOperator & " "
                                End If

                            Next
                            If expression <> "" Then
                                If myEval.Eval(expression).ToString().ToLower = "false" Then
                                    Return False
                                End If
                            End If
                        End If

                        If dummyquestion.Answers IsNot Nothing Then

                            For Each dummyAnswer In dummyquestion.Answers
                                expression = ""
                                If dummyAnswer.Conditions IsNot Nothing Then
                                    For idx As Integer = 0 To dummyAnswer.Conditions.Count - 1
                                        dummyCondition = dummyAnswer.Conditions(idx)

                                        If ValidateCondition(dummyAnswer.sequenceId, dummyCondition, myTableOption, givenConfigPath) Then
                                            expression &= "(1=1)"
                                        Else
                                            expression &= "(1=2)"
                                        End If
                                        If idx < dummyAnswer.Conditions.Count - 1 Then
                                            If dummyCondition.RefOperator = "" Then
                                                dummyCondition.RefOperator = "AND"
                                            End If
                                            expression = expression & " " & dummyCondition.RefOperator & " "
                                        End If

                                    Next
                                    If expression <> "" Then
                                        If myEval.Eval(expression).ToString().ToLower = "false" Then
                                            ansToRomove.Add(dummyAnswer)
                                        End If
                                    End If
                                End If

                            Next
                            For Each dummyAnswer In ansToRomove
                                dummyquestion.Answers.Remove(dummyAnswer)
                            Next
                        End If


                    End If
                Next

                For Each dummyquestion In givenDialog.Questions
                    If dummyquestion.Answers IsNot Nothing Then
                        If dummyquestion.Answers.Count = 0 And ansToRomove.Count > 0 Then
                            Return False
                        End If
                        'ElseIf dummyquestion.ExternalReferenceToAcquire IsNot Nothing Then
                        '    If dummyquestion.ExternalReferenceToAcquire.Count = 0 Then
                        '        Return False
                        '    End If
                        'ElseIf dummyquestion.FeatureToAcquire IsNot Nothing Then
                        '    If dummyquestion.FeatureToAcquire.Count = 0 Then
                        '        Return False
                        '    End If
                    End If
                Next

            End If

        End If

        Return True

    End Function

    Private Function ValidateCondition(ByVal currentId As Integer, ByVal givenCondition As DialogsCondition, ByRef myTableOption As QAOptionDataTable, myConfigPath As String) As Boolean


        Dim retValue As Boolean = True
        Dim idToCheck As String
        Dim myrowselection As New Generic.List(Of QAOptionRow)

        Dim lqResult

        If givenCondition IsNot Nothing Then

            If givenCondition.RefType = "OPTION" Then

                ' myTableOption = myDataModelHelper.GetDialogTable(TableName.QAOption)
                idToCheck = givenCondition.RefOption.Split(" ")(0)

                lqResult = From qalist In myTableOption Select qalist
                           Where qalist.selected = True _
                           And qalist.id = idToCheck And qalist.number <> currentId
                'And qalist.path.Contains(idToCheck)


                For Each qalist In lqResult
                    myrowselection.Add(qalist)
                Next

                'rowSelection = myTableSelections.Select("pathSelection like '%|" & idToCheck & "|%'", "number ASC")
                If givenCondition.RefOptionRelation.ToUpper = "EQUAL" Then
                    If myrowselection.Count > 0 Then
                        retValue = True
                    Else
                        retValue = False
                    End If
                ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_EQUAL" Then
                    If myrowselection.Count > 0 Then
                        retValue = False
                    Else
                        retValue = True
                    End If
                End If
            ElseIf givenCondition.RefType = "INPUT" Then
                Dim myTableInput As QAInputDataTable
                Dim myInputRow As QAInputRow
                myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)

                'idToCheck = givenCondition.RefOption
                idToCheck = givenCondition.RefOption.Split(" ")(0)
                lqResult = From qalist In myTableInput Select qalist
                           Where qalist.ID = idToCheck And qalist.value <> ""

                Dim val As String = ""
                For Each myInputRow In lqResult
                    val = myInputRow.value
                    Exit For
                Next

                If myInputRow IsNot Nothing Then
                    Dim myVal As Double = -1000
                    Dim maxValue As Double = -1000
                    Dim minValue As Double = -1000

                    Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate


                    If IsNumeric(givenCondition.refValueMIN) Then
                        minValue = Double.Parse(givenCondition.refValueMIN)
                    Else
                        If givenCondition.refValueMIN <> "" Then
                            Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
                            myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                            minValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
                        End If
                    End If

                    If IsNumeric(givenCondition.refValueMAX) Then
                        maxValue = Double.Parse(givenCondition.refValueMAX)
                    Else
                        If givenCondition.refValueMAX <> "" Then
                            Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
                            myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                            maxValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
                        End If
                    End If

                    Try
                        myVal = Double.Parse(val)
                    Catch ex As Exception
                        myVal = -1000
                    End Try
                    If givenCondition.RefOptionRelation.ToUpper = "BETWEEN" Then
                        If myVal <> -1000 Then
                            If Double.Parse(myVal) >= Double.Parse(minValue) And Double.Parse(myVal) <= Double.Parse(maxValue) Then
                                retValue = True
                            Else
                                retValue = False
                            End If

                        End If
                    ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_BETWEEN" Then
                        If myVal <> -1000 Then
                            If Double.Parse(myVal) >= Double.Parse(minValue) And Double.Parse(myVal) <= Double.Parse(maxValue) Then
                                retValue = False
                            Else
                                retValue = True
                            End If
                        End If
                    ElseIf givenCondition.RefOptionRelation.ToUpper = "EQUAL" Then
                        If myVal <> -1000 Then
                            If Double.Parse(myVal) = Double.Parse(minValue) Then
                                retValue = True
                            Else
                                retValue = False
                            End If
                        End If
                    End If
                Else
                    retValue = False
                End If
            ElseIf givenCondition.RefType = "FEATURE" Then
                Dim myFeaturesLiest As QAFeaturesDataTable
                Dim myFeatureRw As QAFeaturesRow
                myFeaturesLiest = myDataModelHelper.GetDialogTable(TableName.QAFeatures)

                'idToCheck = givenCondition.RefOption
                idToCheck = givenCondition.RefOption.Split(" ")(0)
                lqResult = From qalist In myFeaturesLiest Select qalist
                           Where qalist.ID = idToCheck And qalist.Value <> ""

                Dim val As String = ""
                For Each myFeatureRw In lqResult
                    val = myFeatureRw.Value
                    Exit For
                Next

                If myFeatureRw IsNot Nothing Then
                    Dim myVal As Double = -1000
                    Dim maxValue As Double = -1000
                    Dim minValue As Double = -1000

                    Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate

                    If Not IsNumeric(givenCondition.refValueMIN) And Not IsNumeric(givenCondition.refValueMAX) Then
                        retValue = False
                        Dim optionVal1 As String = givenCondition.refValueMIN
                        If optionVal1 <> "" Then
                            If IsAFunction(optionVal1, myConfigPath) Then
                                Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, optionVal1)
                                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                                optionVal1 = myNewEvalFun.EvaluateCAFunction(tmpValue)
                            End If
                        End If

                        Dim optionVal2 As String = givenCondition.refValueMAX
                        If optionVal2 <> "" Then
                            If IsAFunction(optionVal2, myConfigPath) Then
                                Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, optionVal2)
                                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                                optionVal2 = myNewEvalFun.EvaluateCAFunction(tmpValue)
                            End If
                        End If

                        If Not IsNumeric(val) Then
                            If IsAFunction(val, myConfigPath) Then
                                Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, val)
                                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                                val = myNewEvalFun.EvaluateCAFunction(tmpValue)
                            End If
                        End If
                        If givenCondition.RefOptionRelation.ToUpper = "EQUAL" Then
                            If optionVal1 <> "" And val.ToLower = optionVal1.ToLower Then
                                retValue = True
                            End If


                            If optionVal2 <> "" And val.ToLower = optionVal2.ToLower Then
                                retValue = True
                            End If
                        ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_EQUAL" Then
                            If optionVal1 <> "" And val.ToLower <> optionVal1.ToLower Then
                                retValue = True
                            End If


                            If optionVal2 <> "" And val.ToLower <> optionVal2.ToLower Then
                                retValue = True
                            End If
                        End If

                    Else
                        If IsNumeric(givenCondition.refValueMIN) Then
                            minValue = Double.Parse(givenCondition.refValueMIN)
                        Else
                            If givenCondition.refValueMIN <> "" Then
                                Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
                                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                                minValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
                            End If
                        End If

                        If IsNumeric(givenCondition.refValueMAX) Then
                            maxValue = Double.Parse(givenCondition.refValueMAX)
                        Else
                            If givenCondition.refValueMAX <> "" Then
                                Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
                                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                                maxValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
                            End If
                        End If

                        Try
                            If Not IsNumeric(val) Then
                                Try
                                    Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, val)
                                    myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                                    val = myNewEvalFun.EvaluateCAFunction(tmpValue)
                                Catch ex As Exception

                                End Try

                            End If
                            myVal = Double.Parse(val)
                        Catch ex As Exception
                            myVal = -1000
                        End Try
                        If givenCondition.RefOptionRelation.ToUpper = "BETWEEN" Then
                            If myVal <> -1000 Then
                                If Double.Parse(myVal) >= Double.Parse(minValue) And Double.Parse(myVal) <= Double.Parse(maxValue) Then
                                    retValue = True
                                Else
                                    retValue = False
                                End If

                            End If
                        ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_BETWEEN" Then
                            If myVal <> -1000 Then
                                If Double.Parse(myVal) >= Double.Parse(minValue) And Double.Parse(myVal) <= Double.Parse(maxValue) Then
                                    retValue = False
                                Else
                                    retValue = True
                                End If
                            End If
                        ElseIf givenCondition.RefOptionRelation.ToUpper = "EQUAL" Then
                            If myVal <> -1000 Then
                                If Double.Parse(myVal) = Double.Parse(minValue) Then
                                    retValue = True
                                Else
                                    retValue = False
                                End If
                            End If
                        ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_EQUAL" Then
                            If myVal <> -1000 Then
                                If Double.Parse(myVal) <> Double.Parse(minValue) Then
                                    retValue = True
                                Else
                                    retValue = False
                                End If
                            End If
                        End If
                    End If



                Else
                    retValue = False
                End If

            ElseIf givenCondition.RefType = "EXTERNAL_TABLES" Then
                Dim myTableExt As QAExternalReferenceDataTable
                Dim myextRow As QAExternalReferenceRow
                Dim myValueRef As String = ""

                myTableExt = myDataModelHelper.GetDialogTable(TableName.QAExternalReference)

                idToCheck = givenCondition.RefOption.Split(" ")(0)

                lqResult = From qalist In myTableExt Select qalist
                           Where qalist.ID = idToCheck
                For Each myextRow In lqResult
                    Exit For
                Next
                retValue = False
                If myextRow IsNot Nothing Then
                    If myextRow.selectedValue.Trim <> "" Then
                        'CONTROLLO
                        myValueRef = givenCondition.refValueMIN
                        If myValueRef.Trim <> "" Then
                            If myValueRef.ToUpper = myextRow.selectedValue.ToUpper Then
                                retValue = True
                            End If
                        Else
                            retValue = True
                        End If

                    End If
                End If

            Else
                If myListVariable IsNot Nothing Then
                    If myListVariable.ContainsKey(givenCondition.RefType) Then
                        Dim val As String = myListVariable(givenCondition.RefType)
                        If givenCondition.RefOptionRelation.ToUpper = "EQUAL" Then
                            If val.ToUpper.Equals(givenCondition.RefOption.ToUpper) Then
                                retValue = True
                            Else
                                retValue = False
                            End If
                        ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_EQUAL" Then
                            If val.ToUpper.Equals(givenCondition.RefOption.ToUpper) Then
                                retValue = False
                            Else
                                retValue = True
                            End If
                        Else
                            Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate
                            Dim minValue As Double = Integer.MinValue
                            Dim maxValue As Double = Integer.MaxValue
                            Dim value As Double = Double.Parse(val)
                            If IsNumeric(givenCondition.refValueMIN) Then
                                minValue = Double.Parse(givenCondition.refValueMIN)
                            Else
                                If givenCondition.refValueMIN <> "" Then
                                    Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
                                    myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                                    minValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
                                End If
                            End If
                            If IsNumeric(givenCondition.refValueMAX) Then
                                maxValue = Double.Parse(givenCondition.refValueMAX)
                            Else
                                If givenCondition.refValueMAX <> "" Then
                                    Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
                                    myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
                                    maxValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
                                End If
                            End If
                            Try
                                val = Double.Parse(val)
                            Catch ex As Exception
                                val = -1000
                            End Try

                            If givenCondition.RefOptionRelation.ToUpper = "BETWEEN" Then
                                If Double.Parse(val) >= Double.Parse(givenCondition.refValueMIN) And Double.Parse(val) <= Double.Parse(givenCondition.refValueMAX) Then
                                    retValue = True
                                Else
                                    retValue = False
                                End If
                            ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_BETWEEN" Then
                                If Double.Parse(val) >= Double.Parse(givenCondition.refValueMIN) And Double.Parse(val) <= Double.Parse(givenCondition.refValueMAX) Then
                                    retValue = False
                                Else
                                    retValue = True
                                End If
                            End If

                        End If
                    End If
                End If
            End If
        End If
        Return retValue
    End Function



    'Private Function ValidateDialog(ByRef givenDialog As Dialogs)
    '    Dim retvalue As Boolean = False
    '    Dim expression As String = ""
    '    Dim dummyquestion As DialogsQuestion
    '    Dim dummyAnswer As DialogsAnswer
    '    Dim dummyCondition As DialogsCondition

    '    Dim myEval As New ExpressionEvaluator.Evaluator
    '    Dim ansToRomove As New List(Of DialogsAnswer)
    '    If givenDialog IsNot Nothing Then
    '        'VALIDAZIONE DELLE RISPOSTE
    '        If givenDialog.Questions IsNot Nothing Then
    '            For Each dummyquestion In givenDialog.Questions
    '                If dummyquestion IsNot Nothing Then

    '                    If dummyquestion.Conditions IsNot Nothing Then
    '                        For idx As Integer = 0 To dummyquestion.Conditions.Count - 1
    '                            dummyCondition = dummyquestion.Conditions(idx)

    '                            If ValidateCondition(dummyquestion.sequenceID, dummyCondition) Then
    '                                expression &= "(1=1)"
    '                            Else
    '                                expression &= "(1=2)"
    '                            End If
    '                            If idx < dummyquestion.Conditions.Count - 1 Then
    '                                If dummyCondition.RefOperator = "" Then
    '                                    dummyCondition.RefOperator = "AND"
    '                                End If
    '                                expression = expression & " " & dummyCondition.RefOperator & " "
    '                            End If

    '                        Next
    '                        If expression <> "" Then
    '                            If myEval.Eval(expression).ToString().ToLower = "false" Then
    '                                Return False
    '                            End If
    '                        End If
    '                    End If

    '                    If dummyquestion.Answers IsNot Nothing Then

    '                        For Each dummyAnswer In dummyquestion.Answers
    '                            expression = ""
    '                            If dummyAnswer.Conditions IsNot Nothing Then
    '                                For idx As Integer = 0 To dummyAnswer.Conditions.Count - 1
    '                                    dummyCondition = dummyAnswer.Conditions(idx)

    '                                    If ValidateCondition(dummyAnswer.sequenceId, dummyCondition) Then
    '                                        expression &= "(1=1)"
    '                                    Else
    '                                        expression &= "(1=2)"
    '                                    End If
    '                                    If idx < dummyAnswer.Conditions.Count - 1 Then
    '                                        If dummyCondition.RefOperator = "" Then
    '                                            dummyCondition.RefOperator = "AND"
    '                                        End If
    '                                        expression = expression & " " & dummyCondition.RefOperator & " "
    '                                    End If

    '                                Next
    '                                If expression <> "" Then
    '                                    If myEval.Eval(expression).ToString().ToLower = "false" Then
    '                                        ansToRomove.Add(dummyAnswer)
    '                                    End If
    '                                End If
    '                            End If

    '                        Next
    '                        For Each dummyAnswer In ansToRomove
    '                            dummyquestion.Answers.Remove(dummyAnswer)
    '                        Next
    '                    End If


    '                End If
    '            Next

    '            For Each dummyquestion In givenDialog.Questions
    '                If dummyquestion.Answers IsNot Nothing Then
    '                    If dummyquestion.Answers.Count = 0 And ansToRomove.Count > 0 Then
    '                        Return False
    '                    End If
    '                    'ElseIf dummyquestion.ExternalReferenceToAcquire IsNot Nothing Then
    '                    '    If dummyquestion.ExternalReferenceToAcquire.Count = 0 Then
    '                    '        Return False
    '                    '    End If
    '                    'ElseIf dummyquestion.FeatureToAcquire IsNot Nothing Then
    '                    '    If dummyquestion.FeatureToAcquire.Count = 0 Then
    '                    '        Return False
    '                    '    End If
    '                End If
    '            Next

    '        End If

    '    End If

    '    Return True

    'End Function

    'Private Function ValidateCondition(ByVal currentId As Integer, ByVal givenCondition As DialogsCondition) As Boolean


    '    Dim retValue As Boolean = True
    '    Dim idToCheck As String
    '    Dim myrowselection As New Generic.List(Of QAOptionRow)

    '    Dim lqResult

    '    If givenCondition IsNot Nothing Then

    '        If givenCondition.RefType = "OPTION" Then
    '            Dim myTableOption As QAOptionDataTable
    '            myTableOption = myDataModelHelper.GetDialogTable(TableName.QAOption)
    '            idToCheck = givenCondition.RefOption.Split(" ")(0)

    '            'lqResult = From qalist In myTableOption Select qalist _
    '            '               Where qalist.selected = True _
    '            '              And qalist.path.Contains(idToCheck)

    '            lqResult = From qalist In myTableOption Select qalist _
    '                          Where qalist.selected = True _
    '                          And qalist.id = idToCheck And qalist.number <> currentId
    '            'And qalist.path.Contains(idToCheck)

    '            For Each qalist In lqResult
    '                myrowselection.Add(qalist)
    '            Next

    '            'rowSelection = myTableSelections.Select("pathSelection like '%|" & idToCheck & "|%'", "number ASC")
    '            If givenCondition.RefOptionRelation.ToUpper = "EQUAL" Then
    '                If myrowselection.Count > 0 Then
    '                    retValue = True
    '                Else
    '                    retValue = False
    '                End If
    '            ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_EQUAL" Then
    '                If myrowselection.Count > 0 Then
    '                    retValue = False
    '                Else
    '                    retValue = True
    '                End If
    '            End If
    '        ElseIf givenCondition.RefType = "INPUT" Then
    '            Dim myTableInput As QAInputDataTable
    '            Dim myInputRow As QAInputRow
    '            myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)

    '            'idToCheck = givenCondition.RefOption
    '            idToCheck = givenCondition.RefOption.Split(" ")(0)
    '            lqResult = From qalist In myTableInput Select qalist _
    '                        Where qalist.ID = idToCheck And qalist.value <> ""

    '            Dim val As String = ""
    '            For Each myInputRow In lqResult
    '                val = myInputRow.value
    '                Exit For
    '            Next

    '            If myInputRow IsNot Nothing Then
    '                Dim myVal As Double = -1000
    '                Dim maxValue As Double = -1000
    '                Dim minValue As Double = -1000

    '                Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate


    '                If IsNumeric(givenCondition.refValueMIN) Then
    '                    minValue = Double.Parse(givenCondition.refValueMIN)
    '                Else
    '                    If givenCondition.refValueMIN <> "" Then
    '                        Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
    '                        myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures))
    '                        minValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
    '                    End If
    '                End If

    '                If IsNumeric(givenCondition.refValueMAX) Then
    '                    maxValue = Double.Parse(givenCondition.refValueMAX)
    '                Else
    '                    If givenCondition.refValueMAX <> "" Then
    '                        Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
    '                        myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures))
    '                        maxValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
    '                    End If
    '                End If

    '                Try
    '                    myVal = Double.Parse(val)
    '                Catch ex As Exception
    '                    myVal = -1000
    '                End Try
    '                If givenCondition.RefOptionRelation.ToUpper = "BETWEEN" Then
    '                    If myVal <> -1000 Then
    '                        If Double.Parse(myVal) >= Double.Parse(minValue) And Double.Parse(myVal) <= Double.Parse(maxValue) Then
    '                            retValue = True
    '                        Else
    '                            retValue = False
    '                        End If

    '                    End If
    '                ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_BETWEEN" Then
    '                    If myVal <> -1000 Then
    '                        If Double.Parse(myVal) >= Double.Parse(minValue) And Double.Parse(myVal) <= Double.Parse(maxValue) Then
    '                            retValue = False
    '                        Else
    '                            retValue = True
    '                        End If
    '                    End If
    '                End If
    '            Else
    '                retValue = False
    '            End If
    '        ElseIf givenCondition.RefType = "EXTERNAL_TABLES" Then
    '            Dim myTableExt As QAExternalReferenceDataTable
    '            Dim myextRow As QAExternalReferenceRow
    '            Dim myValueRef As String = ""

    '            myTableExt = myDataModelHelper.GetDialogTable(TableName.QAExternalReference)

    '            idToCheck = givenCondition.RefOption.Split(" ")(0)

    '            lqResult = From qalist In myTableExt Select qalist _
    '                        Where qalist.ID = idToCheck
    '            For Each myextRow In lqResult
    '                Exit For
    '            Next
    '            retValue = False
    '            If myextRow IsNot Nothing Then
    '                If myextRow.selectedValue.Trim <> "" Then
    '                    'CONTROLLO
    '                    myValueRef = givenCondition.refValueMIN
    '                    If myValueRef.Trim <> "" Then
    '                        If myValueRef.ToUpper = myextRow.selectedValue.ToUpper Then
    '                            retValue = True
    '                        End If
    '                    Else
    '                        retValue = True
    '                    End If

    '                End If
    '            End If

    '        Else
    '            If myListVariable IsNot Nothing Then
    '                If myListVariable.ContainsKey(givenCondition.RefType) Then
    '                    Dim val As String = myListVariable(givenCondition.RefType)
    '                    If givenCondition.RefOptionRelation.ToUpper = "EQUAL" Then
    '                        If val.ToUpper.Equals(givenCondition.RefOption.ToUpper) Then
    '                            retValue = True
    '                        Else
    '                            retValue = False
    '                        End If
    '                    ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_EQUAL" Then
    '                        If val.ToUpper.Equals(givenCondition.RefOption.ToUpper) Then
    '                            retValue = False
    '                        Else
    '                            retValue = True
    '                        End If
    '                    Else
    '                        Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate
    '                        Dim minValue As Double = Integer.MinValue
    '                        Dim maxValue As Double = Integer.MaxValue
    '                        Dim value As Double = Double.Parse(val)
    '                        If IsNumeric(givenCondition.refValueMIN) Then
    '                            minValue = Double.Parse(givenCondition.refValueMIN)
    '                        Else
    '                            If givenCondition.refValueMIN <> "" Then
    '                                Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
    '                                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures))
    '                                minValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
    '                            End If
    '                        End If
    '                        If IsNumeric(givenCondition.refValueMAX) Then
    '                            maxValue = Double.Parse(givenCondition.refValueMAX)
    '                        Else
    '                            If givenCondition.refValueMAX <> "" Then
    '                                Dim tmpValue As String = DataProvider.DataProvider.GetRule(givenBasePathDeploy, givenCondition.refValueMIN)
    '                                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures))
    '                                maxValue = myNewEvalFun.EvaluateCAFunction(tmpValue)
    '                            End If
    '                        End If
    '                        Try
    '                            val = Double.Parse(val)
    '                        Catch ex As Exception
    '                            val = -1000
    '                        End Try

    '                        If givenCondition.RefOptionRelation.ToUpper = "BETWEEN" Then
    '                            If Double.Parse(val) >= Double.Parse(givenCondition.refValueMIN) And Double.Parse(val) <= Double.Parse(givenCondition.refValueMAX) Then
    '                                retValue = True
    '                            Else
    '                                retValue = False
    '                            End If
    '                        ElseIf givenCondition.RefOptionRelation.ToUpper = "NOT_BETWEEN" Then
    '                            If Double.Parse(val) >= Double.Parse(givenCondition.refValueMIN) And Double.Parse(val) <= Double.Parse(givenCondition.refValueMAX) Then
    '                                retValue = False
    '                            Else
    '                                retValue = True
    '                            End If
    '                        End If

    '                    End If
    '                End If
    '            End If
    '        End If
    '    End If
    '    Return retValue
    'End Function



    Public Function GetFullUsedQA(givenConfigPath As String) As Dialogs

        Dim myTableOption As QAOptionDataTable = myDataModelHelper.GetDialogTable(TableName.QAOption)
        Dim lqResult
        Dim retValue As Dialogs = Nothing
        Dim myQuestionInput As DialogsInput
        Dim dummyDialog As Dialogs

        Try
            retValue = New Dialogs
            'And qalist.number < maxLevel
            lqResult = From qalist In myTableOption
                       Where qalist.selected = True And qalist.level = 0
                       Select qalist Order By qalist.number Ascending

            For Each qalist In lqResult


                'myParentRow = GetDialogRowById(dummyRow.parentid)
                'myChildRow = GetDialogRowByIds(dummyRow.childid)
                'If myChildRow Is Nothing Or myParentRow Is Nothing Then
                '    Continue For
                'End If
                dummyDialog = myDataModelHelper.FillDialog(qalist)
                If dummyDialog IsNot Nothing Then
                    If ValidateDialog(dummyDialog, myTableOption, givenConfigPath) Then
                        If dummyDialog.Questions IsNot Nothing Then
                            Dim myListDefault As String = ""
                            If dummyDialog.Questions(0).Answers IsNot Nothing Then

                                For idx As Integer = 0 To dummyDialog.Questions(0).Answers.Count - 1
                                    If dummyDialog.Questions(0).Answers(idx).forceChoise Then
                                        myListDefault &= dummyDialog.Questions(0).Answers(idx).sequenceId & ";"
                                    End If

                                Next
                                If myListDefault <> "" Then
                                    'For idx As Integer = 0 To dummyDialog.Questions(0).Answers.Count - 1
                                    For idx As Integer = dummyDialog.Questions(0).Answers.Count - 1 To 0 Step -1 '0 To retValue.Questions(0).Answers.Count - 1
                                        If Not myListDefault.Contains(dummyDialog.Questions(0).Answers(idx).sequenceId & ";") Then
                                            dummyDialog.Questions(0).Answers.RemoveAt(idx)
                                            If idx = dummyDialog.Questions(0).Answers.Count - 1 Then
                                                Exit For
                                            End If
                                            idx = idx - 1
                                        End If
                                    Next

                                End If
                            End If



                            retValue.Questions.Add(dummyDialog.Questions(0))

                        End If
                    End If
                End If





            Next






        Catch ex As Exception

            retValue = Nothing
        End Try

        Return retValue

    End Function


    Private Sub write(ByVal myTableDialogs As QAOptionDataTable)
        'Exit Sub
        'Dim c As New IO.StreamWriter("c:\temp\luca.txt")
        'c.WriteLine(String.Format("{0}-{1}-{2}-{3}-{4}-{5}", "num", "level", "parentid", "id", "selected", "path"))
        'For Each row As QAOptionRow In myTableDialogs.Rows
        '    c.WriteLine(String.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}", row.number, row.level, row.parentid, row.id, row.selected, row.path, row.title))
        'Next
        'c.Close()
    End Sub



    Private Sub ResetCompatibleSelection(ByRef myTableDialogs As QAOptionDataTable, ByVal givenLevel As Integer, givenConfigPath As String)
        Dim lqResult

        ''DEVO ELIMINARE TUTTE LE SCELTE EFFETTUATE


        lqResult = (From qalist In myTableDialogs
                    Where qalist.number > givenLevel And qalist.selected = True
                    Select qalist)

        Dim tableCondition As QAConditionsDataTable = myDataModelHelper.GetDialogTable(TableName.QAConditions)
        For Each qalist As QAOptionRow In lqResult

            Dim query2 = From cond In tableCondition
                         Where cond.dtRefNumber = qalist.number.ToString
                         Select cond Order By cond.number Descending


            For Each row As QAConditionsRow In query2

                Dim tmpCondition As New DialogsCondition
                tmpCondition.Id = row.number '..Attributes("id").Value
                tmpCondition.RefType = row.type ' tmpNode.Attributes("type").Value
                tmpCondition.RefOption = row.refCode
                tmpCondition.RefOptionRelation = row.refOptionRelation ' tmpNode.Attributes("refOption").Value
                tmpCondition.RefOperator = row.refOperator
                tmpCondition.refValueMIN = row.minVal
                tmpCondition.refValueMAX = row.maxVal
                If Not ValidateCondition(row.qaId, tmpCondition, myTableDialogs, givenConfigPath) Then
                    qalist.selected = False
                    qalist.AcceptChanges()
                End If



            Next
        Next

    End Sub

    Public Function UpdateSequenceQAPOSA(ByVal givenId As String, ByVal automaticCPCompialtion As Generic.Dictionary(Of String, Double)) As Dialogs

        Dim myTableDialogs As QAOptionDataTable
        Dim currentPath As String = ""
        Dim hasChild As Boolean = False
        Dim changedRow As QAOptionRow = Nothing
        Dim myTableInput As QAInputDataTable
        Dim mainQuestion As QAOptionRow = Nothing

        myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)
        myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)
        '''''''''''''''''''''''''''''''
        '''verifichiamo che la risposta non sia stata cambiata

        Dim lqResult = From qalist In myTableDialogs
                       Where qalist.id = givenId And qalist.level = 0
                       Select qalist


        Dim digitCode As String = ""
        Dim digitPosition As String = ""
        For Each qalist As QAOptionRow In lqResult
            mainQuestion = qalist
            qalist.selected = True
            digitCode &= qalist.CODE_DIGIT & "|"
            digitPosition &= qalist.CODE_POSITION & "|"
            UpdateCPPOSA(qalist, automaticCPCompialtion)
            qalist.AcceptChanges()

        Next

        myTableDialogs.AcceptChanges()



        If digitCode <> "" And digitPosition <> "" Then
            Dim tmpValue As String = ""
            For idx As Integer = 0 To digitCode.Split("|").Length - 1
                Try
                    If digitPosition.Split("|")(idx) = "" Or digitCode.Split("|")(idx) = "" Then
                        Continue For
                    End If
                    Dim position As Integer = Integer.Parse(digitPosition.Split("|")(idx))
                    Dim digit As String = digitCode.Split("|")(idx)

                    For idx2 As Integer = 1 To DIALOGCODE.Length
                        If idx2 <> position Then
                            tmpValue &= DIALOGCODE(idx2 - 1).ToString
                        Else
                            tmpValue &= digit
                            If digit.Length > 1 Then
                                idx2 = idx2 + digit.Length - 1
                            End If
                        End If
                    Next
                Catch ex As Exception

                End Try


            Next
            If tmpValue <> "" And tmpValue.Length = DIALOGCODE.Length Then
                DIALOGCODE = tmpValue
            End If

        End If


        myTableDialogs.AcceptChanges()
        myTableInput.AcceptChanges()

        write(myTableDialogs)
    End Function

    Public Function UpdateSequenceQA_2(ByVal givenSelectionId As String, ByVal givenParentId As String, ByVal givenChildId As String, ByVal givenConfigPath As String) As Dialogs

        Dim myTableDialogs As QAOptionDataTable
        Dim currentPath As String = ""
        Dim hasChild As Boolean = False
        Dim changedRow As QAOptionRow = Nothing
        'Dim myTableInput As QAInputDataTable
        Dim mainQuestion As QAOptionRow = Nothing

        myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)
        'myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)


        '''''''''''''''''''''''''''''''
        '''verifichiamo che la risposta non sia stata cambiata

        Dim lqResult = From qalist In myTableDialogs
                       Where qalist.id = givenParentId And qalist.level = 0
                       Select qalist

        Dim lqResult2 = From qalist In myTableDialogs
                        Where qalist.parentid = givenParentId
                        Select qalist

        Dim oldSelections As New Generic.List(Of QAOptionRow)
        Dim newSelections As New Generic.List(Of QAOptionRow)

        For Each qalist2 As QAOptionRow In lqResult2
            If qalist2.selected Then
                oldSelections.Add(qalist2)
            End If
            If givenChildId.Contains(";") And givenChildId.ToUpper.Contains(qalist2.id.ToUpper & ";") Then
                newSelections.Add(qalist2)
            ElseIf qalist2.id = givenChildId Then
                newSelections.Add(qalist2)
            End If
        Next
        For Each qalist As QAOptionRow In lqResult
            mainQuestion = qalist
        Next
        'posso selezionare la risposta
        Dim digitCode As String = ""
        Dim digitPosition As String = ""
        Dim myListCPUpdated As New Generic.List(Of String)
        If newSelections.Count > 0 Then
            If mainQuestion.selected = False Then
                mainQuestion.selected = True
                digitCode &= mainQuestion.CODE_DIGIT & "|"
                digitPosition &= mainQuestion.CODE_POSITION & "|"
                UpdateCP(mainQuestion, myListCPUpdated)
                mainQuestion.AcceptChanges()

            End If
            For Each qalist2 As QAOptionRow In newSelections
                qalist2.selected = True
                digitCode &= qalist2.CODE_DIGIT & "|"
                digitPosition &= qalist2.CODE_POSITION & "|"
                UpdateCP(qalist2, myListCPUpdated)
                qalist2.AcceptChanges()
            Next
        End If



        'se la domanda è a selezione multipla allora passa
        'se la domanda è a selezione singola dobbiamo annullare oldselection
        Dim index As Integer = -1

        For Each qalist As QAOptionRow In lqResult
            mainQuestion = qalist
            index = mainQuestion.number


            If qalist.selectionType <> optionType.MultiOptionSelection.ToString Then
                For Each qalist2 As QAOptionRow In oldSelections
                    'annullo la risposta data
                    qalist2.selected = False
                    qalist2.AcceptChanges()
                    'annullo le CP valorizzate
                    ResetCP(qalist2, myListCPUpdated)
                    'annullo tutte le risposte scaturite da questa domanda

                    'If qalist2.parentid = "" Then
                    '    RemoveChildSelection(qalist2, myTableDialogs, True)
                    'Else
                    '    RemoveChildSelection(qalist2, myTableDialogs, False)
                    'End If

                    AnnullaRisposta(qalist2, myTableDialogs, myListCPUpdated)
                Next
            Else
                For Each qalist2 As QAOptionRow In oldSelections
                    'annullo la risposta data
                    If Not givenChildId.ToUpper.Contains(qalist2.id.ToUpper) Then
                        qalist2.selected = False
                        qalist2.AcceptChanges()
                        'annullo le CP valorizzate
                        ResetCP(qalist2, myListCPUpdated)
                        'annullo tutte le risposte scaturite da questa domanda

                        'If qalist2.parentid = "" Then
                        '    RemoveChildSelection(qalist2, myTableDialogs, True)
                        'Else
                        '    RemoveChildSelection(qalist2, myTableDialogs, False)
                        'End If

                        AnnullaRisposta(qalist2, myTableDialogs, myListCPUpdated)
                    End If

                Next
            End If
        Next
        'dobbiamo annullare tutte le domande che non sono più compatibili

        myTableDialogs.AcceptChanges()
        myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)
        Dim lqResult3
        If Not givenChildId.Contains(";") Then
            lqResult3 = From qalist3 In myTableDialogs
                        Where qalist3.selected = True And qalist3.number > index And qalist3.id <> givenChildId
                        Select qalist3
        Else
            lqResult3 = From qalist3 In myTableDialogs
                        Where qalist3.selected = True And qalist3.number > index And Not givenChildId.Contains(qalist3.id)
                        Select qalist3

        End If

        Dim i As Integer = 0
        Dim myNewList As New Generic.List(Of String)
        For Each qalist3 As QAOptionRow In lqResult3
            'Dim file As String = "c:\temp\table" & i.ToString & ".xml"
            'i += 1
            'myTableDialogs.WriteXml(file)
            Dim myDumDialog As Dialogs = myDataModelHelper.FillDialog(qalist3)
            If myDumDialog IsNot Nothing Then
                If Not ValidateDialog(myDumDialog, myTableDialogs, givenConfigPath) Then
                    'annullo la risposta data
                    qalist3.selected = False
                    qalist3.AcceptChanges()
                    'annullo le CP valorizzate
                    ResetCP(qalist3, myListCPUpdated)
                    myTableDialogs.AcceptChanges()
                    'annullo tutte le risposte scaturite da questa domanda
                    'If qalist2.parentid = "" Then
                    '    RemoveChildSelection(qalist2, myTableDialogs, True)
                    'Else
                    '    RemoveChildSelection(qalist2, myTableDialogs, False)
                    'End If
                    AnnullaRisposta(qalist3, myTableDialogs, myListCPUpdated)
                Else
                    If myNewList.Contains(myDumDialog.Questions(0).Id) Then
                        Continue For
                    Else
                        myNewList.Add(myDumDialog.Questions(0).Id)
                    End If
                    If myDumDialog.Questions IsNot Nothing Then
                        SetCAByFunction(myDumDialog)
                        SetTEAutomatic(myDumDialog, givenConfigPath)
                    End If

                    'se la domanda generale è comunque valida c'è la possibilà
                    'che il numero di domande presentate non sia + valido
                    If myDumDialog.Questions(0).Answers IsNot Nothing Then
                        If myDumDialog.Questions(0).Answers.Count = 1 Then
                            qalist3.selected = True
                            qalist3.AcceptChanges()
                            'annullo le CP valorizzate
                            UpdateCP(qalist3, myListCPUpdated)
                            If Not myDumDialog.Questions(0).Answers(0).isSelected Then
                                UpdateSequenceQA_2(myDumDialog.Questions(0).Answers(0).sequenceId, myDumDialog.Questions(0).Answers(0).idParent, myDumDialog.Questions(0).Answers(0).id, givenConfigPath)
                            End If
                            myTableDialogs.AcceptChanges()
                        ElseIf myDumDialog.Questions(0).Answers.Count > 1 Then
                            Dim childSelected As Boolean = False
                            For Each answer As DialogsAnswer In myDumDialog.Questions(0).Answers
                                If answer.isSelected Then
                                    childSelected = True
                                End If
                            Next
                            If Not childSelected Then
                                qalist3.selected = False
                                qalist3.AcceptChanges()
                                'annullo le CP valorizzate
                                ResetCP(qalist3, myListCPUpdated)
                                myTableDialogs.AcceptChanges()


                            End If
                        End If

                        'ElseIf myDumDialog.Questions(0).ExternalReferenceToAcquire IsNot Nothing Then
                    ElseIf myDumDialog.Questions(0).ExternalReferenceToAcquire IsNot Nothing AndAlso myDumDialog.Questions(0).ExternalReferenceToAcquire.Count > 0 Then
                        Dim childIdValue As String = ""
                        For Each input As DialogsExternalTable In myDumDialog.Questions(0).ExternalReferenceToAcquire
                            'If input.TE_TextValue <> "" Then

                            childIdValue = childIdValue & String.Format("TE_{0}_{1}:{2};", input.idParent, input.sequenceId, input.TE_TextValue)
                            UpdateSequenceTE(myDumDialog.Questions(0).Id, input.idParent, childIdValue)


                            'End If

                        Next
                    End If
                    If myDumDialog.Questions(0).Features IsNot Nothing Then
                        UpdateCP(qalist3, myListCPUpdated)
                    End If


                End If
            End If
        Next
        myTableDialogs.AcceptChanges()

        myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)




        If digitCode <> "" And digitPosition <> "" Then
            Dim tmpValue As String = ""
            For idx As Integer = 0 To digitCode.Split("|").Length - 1
                Try
                    If digitPosition.Split("|")(idx) = "" Or digitCode.Split("|")(idx) = "" Then
                        Continue For
                    End If
                    Dim position As Integer = Integer.Parse(digitPosition.Split("|")(idx))
                    Dim digit As String = digitCode.Split("|")(idx)

                    For idx2 As Integer = 1 To DIALOGCODE.Length
                        If idx2 <> position Then
                            tmpValue &= DIALOGCODE(idx2 - 1).ToString
                        Else
                            tmpValue &= digit
                            If digit.Length > 1 Then
                                idx2 = idx2 + digit.Length - 1
                            End If
                        End If
                    Next
                Catch ex As Exception

                End Try


            Next
            If tmpValue <> "" And tmpValue.Length = DIALOGCODE.Length Then
                DIALOGCODE = tmpValue
            End If

        End If


        myTableDialogs.AcceptChanges()
        ' myTableInput.AcceptChanges()

        write(myTableDialogs)
    End Function

    Public Function AnnullaRisposta(ByRef givenRow As QAOptionRow, ByRef myTableDialogs As QAOptionDataTable, ByVal myListCPUpdated As List(Of String))

        Dim currentPath As String = ""
        Dim hasChild As Boolean = False
        Dim changedRow As QAOptionRow = Nothing
        Dim myTableInput As QAInputDataTable
        Dim myConfigPath As String = givenBasePathDeploy
        Dim givenId As String = givenRow.id

        myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)

        Dim lqResult = From qalist In myTableDialogs
                       Where qalist.parentid = givenId
                       Select qalist

        For Each qalist2 As QAOptionRow In lqResult
            If qalist2.selected Then


                qalist2.selected = False
                qalist2.AcceptChanges()
                'annullo le CP valorizzate
                ResetCP(qalist2, myListCPUpdated)
                'annullo tutte le risposte scaturite da questa domanda

                Dim lqInput = From qaInput In myTableInput
                              Where qaInput.dtRefNumber = qalist2.number
                              Select qaInput

                For Each dummyInput As QAInputRow In lqInput
                    dummyInput.value = ""

                Next
                AnnullaRisposta(qalist2, myTableDialogs, myListCPUpdated)
            End If
        Next

    End Function


    Public Function UpdateSequenceQA(ByVal givenSelectionId As String, ByVal givenParentId As String, ByVal givenChildId As String, ByVal givenConfigPath As String) As Dialogs
        Return UpdateSequenceQA_2(givenSelectionId, givenParentId, givenChildId, givenConfigPath)

        ''Dim myTableDialogs As QAOptionDataTable
        ''Dim currentPath As String = ""
        ''Dim hasChild As Boolean = False
        ''Dim changedRow As QAOptionRow = Nothing
        ''Dim myTableInput As QAInputDataTable


        ''myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)
        ''myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)
        '' '''''''''''''''''''''''''''''''
        '' '''verifichiamo che la risposta non sia stata cambiata

        ''Dim lqResult = From qalist In myTableDialogs
        ''               Where qalist.id = givenParentId And qalist.level = 0
        ''               Select qalist

        ''Dim lqResult2 = From qalist In myTableDialogs
        ''               Where qalist.parentid = givenParentId
        ''               Select qalist


        ''For Each qalist As QAOptionRow In lqResult
        ''    Dim index As Integer = -1



        ''    qalist.selected = True
        ''    qalist.AcceptChanges()
        ''    Dim digitCode As String = ""
        ''    Dim digitPosition As String = ""

        ''    If qalist.selectionType = optionType.SingleOptionSelection.ToString Then
        ''        For Each qalist2 As QAOptionRow In lqResult2
        ''            If qalist2.id = givenChildId Then
        ''                qalist2.selected = True
        ''                digitCode &= qalist2.CODE_DIGIT & "|"
        ''                digitPosition &= qalist2.CODE_POSITION & "|"
        ''            Else
        ''                If qalist2.selected Then
        ''                    index = qalist2.number
        ''                End If
        ''                qalist2.selected = False
        ''            End If
        ''            qalist2.AcceptChanges()
        ''            UpdateCP(qalist2)
        ''            If qalist2.selected = False Then
        ''                RemoveChildSelection(qalist2)
        ''            End If
        ''        Next
        ''    ElseIf qalist.selectionType = optionType.MultiOptionSelection.ToString Then
        ''        For Each qalist2 As QAOptionRow In lqResult2
        ''            If givenChildId.ToUpper.Contains(qalist2.id.ToUpper) Then
        ''                qalist2.selected = True
        ''                digitCode &= qalist2.CODE_DIGIT & "|"
        ''                digitPosition &= qalist2.CODE_POSITION & "|"
        ''            Else
        ''                If qalist2.selected Then
        ''                    index = qalist.number
        ''                End If
        ''                qalist2.selected = False
        ''            End If
        ''            qalist2.AcceptChanges()
        ''            UpdateCP(qalist2)
        ''        Next
        ''    End If
        ''    If index >= 0 Then
        ''        lqResult2 = From qalist2 In myTableDialogs
        ''                  Where qalist2.selected = True And qalist2.number > index
        ''                  Select qalist2

        ''        For Each qalist2 As QAOptionRow In lqResult2

        ''            Dim myDumDialog As Dialogs = myDataModelHelper.FillDialog(qalist2)
        ''            If myDumDialog IsNot Nothing Then
        ''                If Not ValidateDialog(myDumDialog) Then
        ''                    qalist2.selected = False
        ''                    qalist2.AcceptChanges()

        ''                End If
        ''            End If
        ''        Next



        ''        '' ''lqResult2 = From qalist2 In myTableDialogs
        ''        '' ''          Where qalist2.id <> givenParentId And qalist2.parentid <> givenParentId _
        ''        '' ''          And qalist2.selected = True And qalist2.number > index
        ''        '' ''          Select qalist2

        ''        '' ''For Each qalist2 As QAOptionRow In lqResult2

        ''        '' ''    Dim myDumDialog As Dialogs = myDataModelHelper.FillDialog(qalist2)
        ''        '' ''    If myDumDialog IsNot Nothing Then
        ''        '' ''        If ValidateDialog(myDumDialog) Then

        ''        '' ''            'AGGIORNIAMO EVENTUALI NUOVE SELEZIONI
        ''        '' ''            If myDumDialog.Questions IsNot Nothing Then
        ''        '' ''                If myDumDialog.Questions(0).Answers IsNot Nothing Then
        ''        '' ''                    If myDumDialog.Questions(0).Answers.Count = 1 AndAlso myDumDialog.Questions(0).Answers(0).isSelected = False Then
        ''        '' ''                        ' If retValue.Questions(0).Answers(0).type = DialogsAnswer.AnswerType.Standard Then
        ''        '' ''                        If myDumDialog.Questions(0).Type = DialogsQuestion.QuestionType.SingleOptionSelection Then
        ''        '' ''                            'myLastSelection = retValue
        ''        '' ''                            UpdateSequenceQA(myDumDialog.Questions(0).Answers(0).sequenceId, myDumDialog.Questions(0).Answers(0).idParent, myDumDialog.Questions(0).Answers(0).id)
        ''        '' ''                        End If
        ''        '' ''                    Else
        ''        '' ''                        Dim forceCounter As Integer = 0
        ''        '' ''                        Dim ansToForce As DialogsAnswer
        ''        '' ''                        For Each ans As DialogsAnswer In myDumDialog.Questions(0).Answers
        ''        '' ''                            If ans.forceChoise Then
        ''        '' ''                                forceCounter += 1
        ''        '' ''                                ansToForce = ans
        ''        '' ''                            End If
        ''        '' ''                        Next
        ''        '' ''                        If forceCounter = 1 And ansToForce IsNot Nothing Then
        ''        '' ''                            UpdateSequenceQA(ansToForce.sequenceId, ansToForce.idParent, ansToForce.id)
        ''        '' ''                        End If
        ''        '' ''                    End If

        ''        '' ''                End If
        ''        '' ''            End If



        ''        '' ''            Continue For
        ''        '' ''        End If
        ''        '' ''    End If

        ''        '' ''    qalist2.selected = False

        ''        '' ''    Dim lqInput = From qaInput In myTableInput
        ''        '' ''                 Where qaInput.dtRefNumber = qalist2.number
        ''        '' ''                 Select qaInput

        ''        '' ''    For Each dummyInput As QAInputRow In lqInput
        ''        '' ''        dummyInput.value = ""
        ''        '' ''        dummyInput.AcceptChanges()
        ''        '' ''    Next

        ''        '' ''    qalist2.AcceptChanges()

        ''        '' ''Next
        ''    End If


        ''            If digitCode <> "" And digitPosition <> "" Then
        ''                Dim tmpValue As String = ""
        ''                For idx As Integer = 0 To digitCode.Split("|").Length - 1
        ''                    Try
        ''                        If digitPosition.Split("|")(idx) = "" Or digitCode.Split("|")(idx) = "" Then
        ''                            Continue For
        ''                        End If
        ''                        Dim position As Integer = Integer.Parse(digitPosition.Split("|")(idx))
        ''                        Dim digit As String = digitCode.Split("|")(idx)

        ''                        For idx2 As Integer = 1 To DIALOGCODE.Length
        ''                            If idx2 <> position Then
        ''                                tmpValue &= DIALOGCODE(idx2 - 1).ToString
        ''                            Else
        ''                                tmpValue &= digit
        ''                                If digit.Length > 1 Then
        ''                                    idx2 = idx2 + digit.Length - 1
        ''                                End If
        ''                            End If
        ''                        Next
        ''                    Catch ex As Exception

        ''                    End Try


        ''                Next
        ''                If tmpValue <> "" And tmpValue.Length = DIALOGCODE.Length Then
        ''                    DIALOGCODE = tmpValue
        ''                End If

        ''            End If
        ''            Exit For
        ''        Next




        ''        myTableDialogs.AcceptChanges()
        ''        myTableInput.AcceptChanges()

        ''        write(myTableDialogs)
    End Function

    '' '' '' ''Private Sub RemoveChildSelection2(ByRef givenRow As QAOptionRow, ByRef myTableDialogs As QAOptionDataTable, ByVal forceChild As Boolean)

    '' '' '' ''    'Dim myTableDialogs As QAOptionDataTable
    '' '' '' ''    Dim currentPath As String = ""
    '' '' '' ''    Dim hasChild As Boolean = False
    '' '' '' ''    Dim changedRow As QAOptionRow = Nothing
    '' '' '' ''    Dim myTableInput As QAInputDataTable
    '' '' '' ''    Dim myConfigPath As String = givenBasePathDeploy
    '' '' '' ''    Dim givenId As String = givenRow.id

    '' '' '' ''    'myTableDialogs = myDataModelHelper.GetDialogTable(TableName.QAOption)
    '' '' '' ''    myTableInput = myDataModelHelper.GetDialogTable(TableName.QAInput)

    '' '' '' ''    Dim lqResult = From qalist In myTableDialogs
    '' '' '' ''                   Where qalist.parentid = givenId Or qalist.id = givenId
    '' '' '' ''                   Select qalist

    '' '' '' ''    For Each qalist2 As QAOptionRow In lqResult
    '' '' '' ''        If qalist2.selected Then
    '' '' '' ''            Dim myDialog As Dialogs = myDataModelHelper.FillDialog(qalist2)
    '' '' '' ''            If Not ValidateDialog(myDialog, myTableDialogs) Or forceChild Then
    '' '' '' ''                'annullo la risposta data
    '' '' '' ''                qalist2.selected = False
    '' '' '' ''                qalist2.AcceptChanges()
    '' '' '' ''                'annullo le CP valorizzate
    '' '' '' ''                ResetCP(qalist2)
    '' '' '' ''                'annullo tutte le risposte scaturite da questa domanda
    '' '' '' ''                myTableDialogs.AcceptChanges()
    '' '' '' ''                If qalist2.parentid = "" Then
    '' '' '' ''                    RemoveChildSelection(qalist2, myTableDialogs, True)
    '' '' '' ''                Else
    '' '' '' ''                    RemoveChildSelection(qalist2, myTableDialogs, False)
    '' '' '' ''                End If

    '' '' '' ''            End If

    '' '' '' ''        End If
    '' '' '' ''    Next



    '' '' '' ''End Sub
    Private Sub ResetCP(ByVal givenRow As QAOptionRow, ByVal givenSkipFeatures As Generic.List(Of String))
        Dim retValue As List(Of DialogsFeature) = Nothing
        Dim selectFeatures() As QAFeaturesRow
        Dim tableFeatures As QAFeaturesDataTable

        tableFeatures = myDataModelHelper.GetDialogTable(TableName.QAFeatures)
        selectFeatures = tableFeatures.Select("dtRefNumber='" & givenRow.number & "'")
        If selectFeatures.Length > 0 Then

            For Each row As QAFeaturesRow In selectFeatures
                If givenSkipFeatures IsNot Nothing Then
                    If givenSkipFeatures.Contains(row.Code) Then
                        Continue For
                    End If
                End If
                row.Value = ""

                row.AcceptChanges()
            Next

        End If

    End Sub
    Private Function UpdateCPPOSA(ByVal givenRow As QAOptionRow, ByVal givenListCPWithValues As Generic.Dictionary(Of String, Double)) As List(Of DialogsFeature)
        Dim retValue As List(Of DialogsFeature) = Nothing
        Dim tmpFeature As DialogsFeature
        Dim selectFeatures() As QAFeaturesRow
        Dim tableFeatures As QAFeaturesDataTable

        If givenListCPWithValues IsNot Nothing Then


            tableFeatures = myDataModelHelper.GetDialogTable(TableName.QAFeatures)
            selectFeatures = tableFeatures.Select("dtRefNumber='" & givenRow.number & "'")
            If selectFeatures.Length > 0 Then
                For Each row As QAFeaturesRow In selectFeatures
                    If givenListCPWithValues.ContainsKey(row.Code.ToUpper) Then
                        row.Value = givenListCPWithValues(row.Code.ToUpper)
                    Else
                        row.Value = ""
                    End If

                    row.AcceptChanges()
                Next

            End If
        End If
        Return retValue

    End Function

    Private Sub UpdateCP(ByVal givenRow As QAOptionRow, ByRef listCPUpdated As Generic.List(Of String))
        Dim selectFeatures() As QAFeaturesRow
        Dim tableFeatures As QAFeaturesDataTable

        If listCPUpdated Is Nothing Then
            listCPUpdated = New List(Of String)
        End If
        tableFeatures = myDataModelHelper.GetDialogTable(TableName.QAFeatures)


        Dim lqResult = From qalist In tableFeatures
                       Where qalist.dtRefNumber = givenRow.number
                       Select qalist
        Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate

        For Each qalist As QAFeaturesRow In lqResult
            If qalist.Code.ToUpper = "RELOADCP" Then
                ReloadCP(givenRow.number)
                'Exit Sub
            End If

            Dim code As String = ""
            If qalist.Value <> qalist.OriginalValue Then
                code = DataProvider.DataProvider.GetRule(givenBasePathDeploy, qalist.OriginalValue)
            Else
                code = DataProvider.DataProvider.GetRule(givenBasePathDeploy, qalist.Value)
            End If

            If myNewEvalFun Is Nothing And code <> "" Then
                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
            End If
            If myNewEvalFun IsNot Nothing And code <> "" Then
                Try
                    qalist.Value = myNewEvalFun.EvaluateCAFunction(code)
                Catch ex As Exception
                    Throw New Exception(" ThenErrore nella funzione: " & qalist.Value)
                End Try
            End If
            If Not listCPUpdated.Contains(qalist.Code) Then
                listCPUpdated.Add(qalist.Code)
            End If
            qalist.AcceptChanges()
        Next

        myNewEvalFun = Nothing

        tableFeatures.AcceptChanges()

    End Sub
    Private Sub ReloadCP(givenRownumber As Integer)
        Dim selectFeatures() As QAFeaturesRow
        Dim tableFeatures As QAFeaturesDataTable

        tableFeatures = myDataModelHelper.GetDialogTable(TableName.QAFeatures)


        Dim lqResult = From qalist In tableFeatures
                       Where qalist.QAOptionRow.selected = True
                       Select qalist
        Dim myNewEvalFun As ExpressionEvaluator.clsCompileEvaluate

        For Each qalist As QAFeaturesRow In lqResult
            If qalist.dtRefNumber = givenRownumber Then
                Continue For
            End If

            Dim code As String = ""
            If qalist.Value <> qalist.OriginalValue Then
                code = DataProvider.DataProvider.GetRule(givenBasePathDeploy, qalist.OriginalValue)
            Else
                code = DataProvider.DataProvider.GetRule(givenBasePathDeploy, qalist.Value)
            End If

            If myNewEvalFun Is Nothing And code <> "" Then
                myNewEvalFun = New ExpressionEvaluator.clsCompileEvaluate(myDataModelHelper.GetDialogTable(TableName.QAOption), myDataModelHelper.GetDialogTable(TableName.QAInput), myDataModelHelper.GetDialogTable(TableName.QAExternalReference), myDataModelHelper.GetDialogTable(TableName.QAFeatures), myListVariable)
            End If
            If myNewEvalFun IsNot Nothing And code <> "" Then
                Try
                    qalist.Value = myNewEvalFun.EvaluateCAFunction(code)
                Catch ex As Exception
                    Throw New Exception(" ThenErrore nella funzione: " & qalist.Value)
                End Try
            End If

            qalist.AcceptChanges()
        Next
        myNewEvalFun = Nothing
        tableFeatures.AcceptChanges()

    End Sub

    Public Function GetCPValue(givnCPName As String) As String
        Dim tableFeatures As QAFeaturesDataTable

        tableFeatures = myDataModelHelper.GetDialogTable(TableName.QAFeatures)


        Dim lqResult = From qalist In tableFeatures
                       Where qalist.Code = givnCPName
                       Select qalist

        Dim retValue As String = ""

        For Each qalist As QAFeaturesRow In lqResult
            If qalist.QAOptionRow.selected Then
                retValue = qalist.Value
            End If
        Next
        Return retValue
    End Function

    Public Function SaveDialogTables() As DataSet
        Dim myDataSet As dsConfiguratorDialog
        'myDataSet.Relations.Clear()
        'myDataSet.Tables.Clear()
        'myDataSet.Tables.Add(myDataModelHelper.GetDialogTable(TableName.QAConditions).Copy)
        'myDataSet.Tables.Add(myDataModelHelper.GetDialogTable(TableName.QAExternalReference).Copy)
        'myDataSet.Tables.Add(myDataModelHelper.GetDialogTable(TableName.QAFeatures).Copy)
        'myDataSet.Tables.Add(myDataModelHelper.GetDialogTable(TableName.QAInput).Copy)
        'myDataSet.Tables.Add(myDataModelHelper.GetDialogTable(TableName.QAOption).Copy)
        'myDataSet.Tables.Add(myDataModelHelper.GetDialogTable(TableName.QASelections).Copy)

        myDataSet = myDataModelHelper.GetCurrentDataModel.Copy

        Dim myTableVB As New dsConfiguratorDialog.QAVariabiliDataTable

        Dim newRow As QAVariabiliRow = myTableVB.NewRow
        newRow.id = "-111"
        newRow.name = "CODIFICA"
        newRow.value = DIALOGCODE
        myTableVB.Rows.Add(newRow)
        If myListVariable IsNot Nothing Then
            For Each item As String In myListVariable.Keys
                newRow = myTableVB.NewRow
                newRow.id = myTableVB.Rows.Count
                newRow.name = item
                newRow.value = "" & myListVariable(item)
                myTableVB.Rows.Add(newRow)
            Next
        End If

        For idx As Integer = 0 To myDataSet.Tables.Count - 1
            If myDataSet.Tables(idx).TableName.ToLower = TableName.QAVariabili.ToString.ToLower Then
                myDataSet.Tables.RemoveAt(idx)
                Exit For
            End If

        Next

        myDataSet.Tables.Add(myTableVB)

        Return myDataSet
    End Function


    Public Function MergeDataSet(ByRef givenDataSet1 As dsConfiguratorDialog, ByRef givenDataSet2 As dsConfiguratorDialog, ByVal givenPath As String) As Boolean

        Dim optionDataTable1 As QAOptionDataTable
        Dim inputDataTable1 As QAInputDataTable
        Dim externalDataTable1 As QAExternalReferenceDataTable
        Dim variabiliDataTable1 As QAVariabiliDataTable
        Dim myDataModelHelper1 As New clsDataModelHelper(givenDataSet1)
        Dim optionDataTable2 As QAOptionDataTable
        Dim inputDataTable2 As QAInputDataTable
        Dim externalDataTable2 As QAExternalReferenceDataTable
        Dim variabiliDataTable2 As QAVariabiliDataTable
        Dim myDataModelHelper2 As New clsDataModelHelper(givenDataSet2)
        Dim dummyDialog As Dialogs

        Try

            'givenDataSet1.WriteXml("c:\temp\prima.xml")

            optionDataTable1 = myDataModelHelper1.GetDialogTable(clsDataModelHelper.TableName.QAOption)
            inputDataTable1 = myDataModelHelper1.GetDialogTable(clsDataModelHelper.TableName.QAInput)
            externalDataTable1 = myDataModelHelper1.GetDialogTable(clsDataModelHelper.TableName.QAExternalReference)
            variabiliDataTable1 = Nothing

            optionDataTable2 = myDataModelHelper2.GetDialogTable(clsDataModelHelper.TableName.QAOption)
            inputDataTable2 = myDataModelHelper2.GetDialogTable(clsDataModelHelper.TableName.QAInput)
            externalDataTable2 = myDataModelHelper2.GetDialogTable(clsDataModelHelper.TableName.QAExternalReference)
            variabiliDataTable2 = myDataModelHelper2.GetDialogTable(clsDataModelHelper.TableName.QAVariabili)

            Dim lqResult1, lqResult2



            'Selezioniamo tutte le vecchie risposte
            lqResult2 = From qalist2 In optionDataTable2
                        Where qalist2.selected = True
                        Select qalist2

            Dim mySkipId As New Generic.List(Of String)
            For Each qalist2 As QAOptionRow In lqResult2
                lqResult1 = From qalist1 In optionDataTable1 Where qalist1.id = qalist2.id Select qalist1
                For Each qalist1 As QAOptionRow In lqResult1
                    Try
                        dummyDialog = myDataModelHelper1.FillDialog(qalist1)
                        If ValidateDialog(dummyDialog, optionDataTable1, givenPath) And Not mySkipId.Contains(qalist1.parentid) Then
                            SetCAByFunction(dummyDialog)
                            SetTEAutomatic(dummyDialog, givenPath)
                            qalist1.selected = True
                            qalist1.AcceptChanges()
                        Else
                            If Not mySkipId.Contains(qalist1.id.ToUpper) Then
                                mySkipId.Add(qalist1.id.ToUpper)
                            End If
                        End If

                    Catch ex As Exception
                        Dim c As String = ""
                    End Try

                Next
            Next

            'valorizziamo tutti i vecchi INPUT
            lqResult2 = From qalist2 In inputDataTable2 Select qalist2
            For Each qalist2 As QAInputRow In lqResult2
                lqResult1 = From qalist1 In inputDataTable1 Where qalist1.ID = qalist2.ID Select qalist1
                For Each qalist1 As QAInputRow In lqResult1
                    qalist1.value = qalist2.value
                    qalist1.AcceptChanges()
                Next
            Next

            'valorizziamo tutte le vecchie tabelle
            lqResult2 = From qalist2 In externalDataTable2 Select qalist2
            For Each qalist2 As QAExternalReferenceRow In lqResult2
                lqResult1 = From qalist1 In externalDataTable1 Where qalist1.ID = qalist2.ID Select qalist1
                For Each qalist1 As QAExternalReferenceRow In lqResult1
                    qalist1.selectedValue = qalist2.selectedValue
                    qalist1.AcceptChanges()
                Next
            Next




            If variabiliDataTable2 IsNot Nothing Then

                If givenDataSet1.Tables(variabiliDataTable2.TableName) IsNot Nothing Then
                    givenDataSet1.Tables.Remove(variabiliDataTable2.TableName)
                End If

                variabiliDataTable1 = variabiliDataTable2.Copy()

                givenDataSet1.Tables.Add(variabiliDataTable1)

            End If


            'givenDataSet1.WriteXml("c:\temp\dopo.xml")

            Return True
        Catch ex As Exception

        End Try
        Return False

    End Function


    Public Shared Function GetTempDialogObject(givenDataSet1 As dsConfiguratorDialog, givenId As String) As Dialogs

        Dim optionDataTable1 As QAOptionDataTable
        Dim inputDataTable1 As QAInputDataTable
        Dim externalDataTable1 As QAExternalReferenceDataTable
        Dim variabiliDataTable1 As QAVariabiliDataTable
        Dim myDataModelHelper1 As New clsDataModelHelper(givenDataSet1)

        Dim dummyDialog As Dialogs



        'givenDataSet1.WriteXml("c:\temp\prima.xml")

        optionDataTable1 = myDataModelHelper1.GetDialogTable(clsDataModelHelper.TableName.QAOption)
        inputDataTable1 = myDataModelHelper1.GetDialogTable(clsDataModelHelper.TableName.QAInput)
        externalDataTable1 = myDataModelHelper1.GetDialogTable(clsDataModelHelper.TableName.QAExternalReference)
        variabiliDataTable1 = Nothing


        Dim lqResult1

        'Selezioniamo tutte le vecchie risposte
        'lqResult1 = From qalist1 In optionDataTable1
        '                  Where qalist1.selected = True And qalist1.id = givenId And qalist1.level = 0
        '                  Select qalist1
        lqResult1 = From qalist1 In optionDataTable1
                    Where qalist1.id = givenId And qalist1.level = 0
                    Select qalist1

        For Each qalist1 As QAOptionRow In lqResult1

            Return myDataModelHelper1.FillDialog(qalist1)

        Next
        Return Nothing
    End Function
End Class
