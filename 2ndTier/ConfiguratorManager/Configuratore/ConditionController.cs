using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public class ConditionController : XtreeNodeController
	{
        protected ConditionDef conditiondDef;
		//protected QuestionDef questionDef;
        protected OptionDef answerDef;

		public override string Name
		{
			get { return conditiondDef.Name; }
			set { conditiondDef.Name = value; }
		}

        public override string ID
        {
            get { return conditiondDef.ID; }
            set { conditiondDef.ID = value; }

        }

		public ConditionDef ConditionDef
		{
			get { return conditiondDef; }
		}

		public override object Item
		{
			get { return conditiondDef; }
		}

		public ConditionController()
		{
		}

        public ConditionController(ConditionDef tableFieldDef)
		{
			this.conditiondDef = tableFieldDef;
		}

		public override int Index(object item)
		{
			throw new XmlTreeException("Calling Index for TableFieldController is not permitted.");
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag,string givenId)
		{
             if (parentInstance.GetType() == typeof(OptionController))
            {
                answerDef = ((OptionController)parentInstance).OptionDef;
                conditiondDef = new ConditionDef();
                conditiondDef.ID = givenId;
                answerDef.Conditions.Add(conditiondDef);
            }

			
			return true;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
            answerDef = ((OptionController)parentInstance).OptionDef;
            answerDef.Conditions.Remove(conditiondDef);
          
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
            //questionDef = ((QuestionController)parentInstance).QuestionDef;
            //questionDef.Conditions.Remove(conditiondDef);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
            answerDef = ((OptionController)parentInstance).OptionDef;
            answerDef.Conditions.Insert(idx, conditiondDef);
		}

		public override void Select(TreeNode tn)
		{
			Program.Properties.SelectedObject = conditiondDef;
		}


       
        
	}
}
