﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XTreeIIDemo.DialogAdminService;
using System.IO;

namespace XTreeDemo
{
    

    public partial class ImportDialogProperty : Form
    {
        public string famiglia = "";
        public string tipologia = "";
        public string prodotto = "";
        public string codifica = "";
        public string cadref = "";
        DataTable myListaFP = null;

        public ImportDialogProperty()
        {
            InitializeComponent();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Int32 givenId = -1;
            String statusMessage = "";
            Int32 retValue;
            String givenName = "";
            byte[] myContent = null;
            List<String> functionFiles = new List<String>();
            string fileConfFeatureToCAD = "";
            string givenFileName="";
            string exitStatus = "";

           
            if (System.IO.Directory.Exists(txtDirectory.Text) == false)
            {
                MessageBox.Show("Inserire una directory valida", "Error on Directory Field");
                return;
            }
            //////famiglia = txtFamiglia.Text;
            //////prodotto = txtProdotto.Text;
            //////codifica = "" + System.Configuration.ConfigurationSettings.AppSettings["DEFAULT_CODART"].ToString() ;// txtCodifica.Text;
            //////cadref = "";// txtCadRef.Text;

            DialogAdminService myService = new DialogAdminService();
            myService.Url = System.Configuration.ConfigurationSettings.AppSettings["EPLMS_INSTANCE_URL"].ToString();
            System.Net.CookieContainer myContainer;
            myContainer = new System.Net.CookieContainer();
            myService.CookieContainer = myContainer;
           
            if (myService.login("DialogAdmin", "DialogAdmin", ref exitStatus) != 0)
            {
                throw new Exception(exitStatus);
            }
            String[] files = System.IO.Directory.GetFiles(txtDirectory.Text);
            foreach (String singlefile in files)
            {
                if (System.IO.Path.GetExtension(singlefile).ToLower() == ".xml" && givenFileName == "")
                {
                    givenFileName = singlefile;

                }
                else if (System.IO.Path.GetExtension(singlefile).ToLower() == ".vb" )
                {
                    functionFiles.Add(singlefile);
                }
                else if (System.IO.Path.GetFileName(singlefile).ToLower() == "FeaturesToCAD.config".ToLower())
                {
                    fileConfFeatureToCAD = singlefile;
                }

            }
            System.Xml.XmlDocument myDoc = new System.Xml.XmlDocument();
            myDoc.Load(givenFileName);
            codifica = myDoc.FirstChild.Attributes["CODIFICA"].Value;
            prodotto = myDoc.FirstChild.Attributes["PRODOTTO"].Value;
            famiglia = myDoc.FirstChild.Attributes["FAMIGLIA"].Value;
            cadref = myDoc.FirstChild.Attributes["CADASM"].Value;
            if (myDoc.FirstChild.Attributes["TIPOLOGIA"] != null)
                tipologia = myDoc.FirstChild.Attributes["TIPOLOGIA"].Value;
            else
                tipologia = "Dialogo di Configurazione";

            retValue = myService.CreateDialogo(famiglia, prodotto, codifica, cadref, tipologia, ref statusMessage, ref givenId, ref givenName);
            if (retValue != 0)
                throw new Exception(statusMessage);
            else
            {
                
                myContent = File.ReadAllBytes(givenFileName);
                retValue = myService.AddFileDialogo(givenId, myContent, "", "Dialog Configuration", ref statusMessage);
                if (retValue != 0)
                    throw new Exception(statusMessage);
                else
                {
                    foreach (String fileName in functionFiles)
                    {
                        myContent = File.ReadAllBytes(fileName);
                        retValue = myService.AddFileDialogo(givenId, myContent, System.IO.Path.GetFileName(fileName), "Function Configuration", ref statusMessage);
                        if (retValue != 0)
                            throw new Exception(statusMessage);
                    }

                    
                }

                if (fileConfFeatureToCAD != "")
                {
                    myContent = File.ReadAllBytes(fileConfFeatureToCAD);
                    retValue = myService.AddFileDialogo(givenId, myContent, System.IO.Path.GetFileName(fileConfFeatureToCAD), "CAD Features Configuration", ref statusMessage);
                    if (retValue != 0)
                        throw new Exception(statusMessage);
                }

            }


            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void txtFamiglia_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
 
        }

        private void txtCodifica_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);

        }

        

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog myFolder = new FolderBrowserDialog();
            myFolder.RootFolder = Environment.SpecialFolder.MyComputer;
            if (myFolder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtDirectory.Text = myFolder.SelectedPath;
            }
        }

        
    }
}
