using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public class FunctionController : XtreeNodeController
	{
		protected FunctionDef functionDef;
		protected DialogDef dialogDef;
        protected OptionDef answerDef;
        protected String id;

		public override string Name
		{
            get { return functionDef.Name; }
		    set {functionDef.Name=value;}
		}

        public override string ID
        {
            get { return functionDef.ID; }
            set { functionDef.ID = value; }
        }

        public FunctionDef FunctionDef
		{
			get { return functionDef; }
		}

		public override object Item
		{
			get { return functionDef; }
		}

		public FunctionController()
		{
            
		}

        public FunctionController(FunctionDef functionDef)
		{

            this.functionDef = functionDef;
		}

        
		public override int Index(object item)
		{
            return -1;
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag,string givenId)
		{
           
            dialogDef = ((DialogController)parentInstance).DialogDef;
            functionDef = new FunctionDef();
            functionDef.ID = givenId;
            dialogDef.Functions.Add(functionDef);
            //clsListsItems.AddFunItem(givenId, functionDef.Name);
			
			return true;
		}

        

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
			dialogDef = ((DialogController)parentInstance).DialogDef;
            dialogDef.Functions.Remove(functionDef);
           // clsListsItems.RemoveFunItem(functionDef.ID, functionDef.Name);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
			dialogDef = ((DialogController)parentInstance).DialogDef;
            dialogDef.Functions.Remove(functionDef);
            //clsListsItems.RemoveFunItem(functionDef.ID, functionDef.Name);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
			dialogDef = ((DialogController)parentInstance).DialogDef;
            dialogDef.Functions.Insert(idx, functionDef);
		}

		public override void Select(TreeNode tn)
		{
            Program.Properties.SelectedObject = functionDef;
		}

        
	}
}
