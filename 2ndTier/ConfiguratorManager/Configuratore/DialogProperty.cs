﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XTreeIIDemo.DialogAdminService;

namespace XTreeDemo
{
    

    public partial class DialogProperty : Form
    {
        public string famiglia = "";
        public string prodotto = "";
        public string codifica = "";
        public string cadref = "";
        public string tipologiaDialogo = "";
        DataTable myListaFP = null;

        public DialogProperty()
        {
            InitializeComponent();

            
            String retMessage="";
            clsCDInterface myService;

            ToolTip toolTip1 = new ToolTip();
            toolTip1.ShowAlways = true;
            toolTip1.SetToolTip(txtFamiglia, "Codice identificativo della Famiglia del Prodotto");

            toolTip1 = new ToolTip();
            toolTip1.ShowAlways = true;
            toolTip1.SetToolTip(txtProdotto, "Prodotto cui afferisce il Dialogo");
            
            txtFamiglia.Items.Clear();
            txtProdotto.Items.Clear();

            myService = new clsCDInterface();
            
            myListaFP = null;
            myListaFP = myService.GetListaFamigliaProdotti();
            myService = null;
            if (myListaFP != null)
            {
                foreach (DataRow myFam in myListaFP.Rows)
                    if(!txtFamiglia.Items.Contains(myFam[0].ToString()))
                        txtFamiglia.Items.Add(myFam[0].ToString());
            }
            comboBox1.SelectedIndex = 0;
            //////toolTip1 = new ToolTip();
            //////toolTip1.ShowAlways = true;
            //////toolTip1.SetToolTip(txtCodifica, "Codifica del Dialogo / Codice Articolo");

            //////toolTip1 = new ToolTip();
            //////toolTip1.ShowAlways = true;
            //////toolTip1.SetToolTip(txtCadRef, "Nome dell'assembly di riferimento "); 



        }

        private void button2_Click(object sender, EventArgs e)
        {
            famiglia = "";
            prodotto = "";
            codifica = "";
            cadref  = "";
            tipologiaDialogo = "";
           
            //////////if (txtProdotto.Text.Trim() =="" )
            //////////{
            //////////    MessageBox.Show("Inserire un prodotto valido", "Error on Prodotto Field");
            //////////    return;
            //////////}
           

            famiglia = txtFamiglia.Text;
           // prodotto = txtProdotto.Text;
            codifica = "" + System.Configuration.ConfigurationSettings.AppSettings["DEFAULT_CODART"].ToString() ;// txtCodifica.Text;
            cadref = "";// txtCadRef.Text;
            tipologiaDialogo = comboBox1.Text; 
           
          

            this.DialogResult = DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void txtFamiglia_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
 
        }

        private void txtCodifica_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);

        }

        private void txtFamiglia_SelectedIndexChanged(object sender, EventArgs e)
        {
            //gestione collegamento dialoghi alla sola famiglia
            return;
            if (myListaFP != null && txtFamiglia.Text  != "")
            {
                txtProdotto.Items.Clear();
                if (myListaFP.Rows.Count > 0)
                {
                    DataRow[] selectRow = myListaFP.Select("Famiglia='" + txtFamiglia.Text + "'");
                    if (selectRow.Length > 0)
                    {
                       
                        foreach (DataRow myProd in selectRow)
                            txtProdotto.Items.Add(myProd[1]);
                    }
                }
            }
        }

        
    }
}
