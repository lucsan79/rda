﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using XTreeDemo;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
    class ExternalTableController : XtreeNodeController
    {
        protected ExternalTableDef externalTableDef;
        protected OptionDef optionDef;
        
		public override string Name
		{
            get { return externalTableDef.Name; }
            set { externalTableDef.Name = value; }
		}

        public override string ID
        {
            get { return externalTableDef.ID; 
            }
            set { externalTableDef.ID = value; }

        }

        public ExternalTableDef ExternalTableDef
		{
            get { return externalTableDef; }
		}

		public override object Item
		{
            get { return externalTableDef; }
		}

		public ExternalTableController()
		{
		}

        public ExternalTableController(ExternalTableDef newexternalTableDef)
		{
            this.externalTableDef = newexternalTableDef;
		}

		public override int Index(object item)
		{
			throw new XmlTreeException("Calling Index for TableFieldController is not permitted.");
		}

        public override bool AddNode(IXtreeNode parentInstance, string tag,string givenId)
        {
            if (parentInstance.GetType() == typeof(OptionController))
            {
                optionDef = ((OptionController)parentInstance).OptionDef;
                externalTableDef = new ExternalTableDef();
                externalTableDef.ID = givenId;
                optionDef.External_Tables.Add(externalTableDef);
            }
            
            return true;
        }

        public override bool DeleteNode(IXtreeNode parentInstance)
        {
            if (parentInstance.GetType() == typeof(OptionController))
            {
                optionDef = ((OptionController)parentInstance).OptionDef;
                optionDef.External_Tables.Remove(externalTableDef);
            }
            return true;
        }

        public override void AutoDeleteNode(IXtreeNode parentInstance)
        {
            if (parentInstance.GetType() == typeof(OptionController))
            {
                optionDef = ((OptionController)parentInstance).OptionDef;
                optionDef.External_Tables.Remove(externalTableDef);
            }
            
        }

        public override void InsertNode(IXtreeNode parentInstance, int idx)
        {
            if (parentInstance.GetType() == typeof(OptionController))
            {
                optionDef = ((OptionController)parentInstance).OptionDef;
                optionDef.External_Tables.Insert(idx, externalTableDef);
            }
           
        }

        public override void Select(TreeNode tn)
        {
            Program.Properties.SelectedObject = externalTableDef;
        }

      

    }
}
