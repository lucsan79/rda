﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;
using System.Drawing.Design;

// this is the custom type we'll want to edit in the propertygrid
// it's just a class with 1 property, a string, but we want a multiline editor for it
namespace XTreeDemo
{
    public class MultiLineString
    {
        private string mString;

        private string mS;

        public MultiLineString()
        {
        }
        public MultiLineString(string s)
        {
            mString = s;
        }
        public string Value
        {
            get { return mString; }
            set { mString = value; }
        }
        public override string ToString()
        {
            return (string.IsNullOrEmpty(mString) ? "" : "<...>");
        }
    }
}