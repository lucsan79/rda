﻿namespace XTreeDemo
{
    partial class frmCPtoBOM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCPtoBOM));
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Features");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("User Inputs", 3, 3);
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("External Referements", 4, 4);
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Functions");
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.treeCAD = new TreeViewMS.TreeViewMS();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.treeCP = new TreeViewMS.TreeViewMS();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Lista Caratteristiche Dialogo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(464, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Distinta";
            // 
            // treeCAD
            // 
            this.treeCAD.ImageIndex = 0;
            this.treeCAD.ImageList = this.imageList1;
            this.treeCAD.Location = new System.Drawing.Point(467, 36);
            this.treeCAD.Name = "treeCAD";
            this.treeCAD.SelectedImageIndex = 0;
            this.treeCAD.SelectedNodes = ((System.Collections.ArrayList)(resources.GetObject("treeCAD.SelectedNodes")));
            this.treeCAD.Size = new System.Drawing.Size(379, 498);
            this.treeCAD.TabIndex = 3;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "iam.png");
            this.imageList1.Images.SetKeyName(1, "ipt.png");
            this.imageList1.Images.SetKeyName(2, "feature.png");
            this.imageList1.Images.SetKeyName(3, "Input_Definition.png");
            this.imageList1.Images.SetKeyName(4, "External_Table.png");
            this.imageList1.Images.SetKeyName(5, "function.png");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(12, 593);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(792, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Attenzione! Se non viene effettuata alcuna associazione , il topassembly della di" +
                "stinta erediterà solo le iproperites definite nel modello cad";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // treeCP
            // 
            this.treeCP.ImageIndex = 0;
            this.treeCP.ImageList = this.imageList1;
            this.treeCP.Location = new System.Drawing.Point(15, 36);
            this.treeCP.Name = "treeCP";
            treeNode5.ImageKey = "feature.png";
            treeNode5.Name = "CP";
            treeNode5.SelectedImageIndex = 2;
            treeNode5.Text = "Features";
            treeNode6.ImageIndex = 3;
            treeNode6.Name = "Input";
            treeNode6.SelectedImageIndex = 3;
            treeNode6.Text = "User Inputs";
            treeNode7.ImageIndex = 4;
            treeNode7.Name = "tabExt";
            treeNode7.SelectedImageIndex = 4;
            treeNode7.Text = "External Referements";
            treeNode8.ImageKey = "function.png";
            treeNode8.Name = "Functions";
            treeNode8.SelectedImageIndex = 5;
            treeNode8.Text = "Functions";
            this.treeCP.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8});
            this.treeCP.SelectedImageIndex = 0;
            this.treeCP.SelectedNodes = ((System.Collections.ArrayList)(resources.GetObject("treeCP.SelectedNodes")));
            this.treeCP.Size = new System.Drawing.Size(299, 498);
            this.treeCP.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(321, 167);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 39);
            this.button1.TabIndex = 12;
            this.button1.Text = "     Associa a Distinta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(321, 261);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 39);
            this.button2.TabIndex = 13;
            this.button2.Text = "       Rimuovi da Distinta";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(711, 540);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(135, 39);
            this.button3.TabIndex = 14;
            this.button3.Text = "       Salva Associazione";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // frmCPtoBOM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 611);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.treeCP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.treeCAD);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCPtoBOM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Associazione Caratteristiche e Codici a Distinta";
            this.Load += new System.EventHandler(this.frmCPtoBOM_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private TreeViewMS.TreeViewMS treeCAD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ImageList imageList1;
        private TreeViewMS.TreeViewMS treeCP;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}