﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;

namespace XTreeDemo
{
    public class clsCompileEvaluate
    {

	    private System.Reflection.Assembly mAssembly;
        System.CodeDom.Compiler.CompilerResults results = null;

	    System.Collections.Generic.Dictionary<string, string> QA = new System.Collections.Generic.Dictionary<string, string>();
	    System.Collections.Generic.Dictionary<string, string> CA = new System.Collections.Generic.Dictionary<string, string>();
	    System.Collections.Generic.Dictionary<string, string> TE = new System.Collections.Generic.Dictionary<string, string>();
        System.Collections.Generic.Dictionary<string, string> EXTERNALVAR = new System.Collections.Generic.Dictionary<string, string>();
        public enum FunctionType
	    {
		    F4CA,
		    F4TE
	    }
        public String GetErrors()
        {
            String retValue="";
            System.CodeDom.Compiler.CompilerError err; 
            if (results != null)
            { 
                for(int i=0;i<results.Errors.Count;i++)
                {
                    err=results.Errors[i];
                    retValue = retValue + String.Format("Line {0}, Col {1}: Error {2} - {3}\n", err.Line, err.Column, err.ErrorNumber, err.ErrorText);
                }
              
            }
            return retValue;
        }
	    private string PrepareFunctionVerify(string code, FunctionType functionType)
	    {

		    StringBuilder sb = new StringBuilder();
		    var _with1 = sb;
		    _with1.Append("Imports System" + "\n");
		    _with1.Append("Imports System.Collections" + "\n");
		    _with1.Append("Imports System.Collections.Generic" + "\n");
		    _with1.Append("Public Class clsFunctionExecutor");
		    _with1.Append("\n");
		    _with1.Append("\n");
            _with1.Append("Shared QA As Generic.Dictionary(Of String, String)\n");
            _with1.Append("Shared CA As Generic.Dictionary(Of String, String)\n");
            _with1.Append("Shared TE As Generic.Dictionary(Of String, String)\n");
            _with1.Append("Shared CP As Generic.Dictionary(Of String, String)\n");
            _with1.Append("Shared EXTERNALVAR As Generic.Dictionary(Of String, String)\n");
            _with1.Append("    Public Shared Function Verify(givenQA as Generic.Dictionary(Of String, String),givenCA As Generic.Dictionary(Of String, String),givenTE As Generic.Dictionary(Of String, String), givenCP as Generic.Dictionary(Of String, String),givenExternalVar as Generic.Dictionary(Of String, String)) As String");
		    _with1.Append("\n");
            _with1.Append("       QA=givenQA");
		    _with1.Append("\n");
            _with1.Append("       CA=givenCA");
            _with1.Append("\n");
            _with1.Append("       TE=givenTE");
            _with1.Append("\n");
            _with1.Append("       CP=givenCP");
            _with1.Append("\n");
            _with1.Append("       EXTERNALVAR=givenExternalVar");
            _with1.Append("\n");
            _with1.Append("\n");
		    //.Append("       Dim CA As New Generic.Dictionary(Of String, String)")
		    //.Append(vbCrLf)
		    _with1.Append(code);
		    //.Append(vbCrLf)
		    //.Append(calist)
		    //.Append(vbCrLf)
		    //.Append(telist)
		    _with1.Append("\n");
		    _with1.Append("\n");
		    _with1.Append("    End Function");
		    _with1.Append("\n");
            _with1.Append("\tPrivate Shared Function IsSelectedOption(givenCode As String) As Boolean" + "\n");
		    _with1.Append("        Dim retValue As Boolean = False" + "\n");
		    _with1.Append("        If QA IsNot Nothing Then" + "\n");
		    _with1.Append("            For Each item As String In QA.Keys" + "\n");
		    _with1.Append("                If item = (givenCode) Then" + "\n");
		    _with1.Append("                    If QA(item).ToLower = \"selected\" Then" + "\n");
		    _with1.Append("                        retValue = True" + "\n");
		    _with1.Append("                    End If" + "\n");
		    _with1.Append("                    Exit For" + "\n");
		    _with1.Append("                End If" + "\n");
		    _with1.Append("            Next" + "\n");
		    _with1.Append("        End If" + "\n");
		    _with1.Append("        return retValue" + "\n");
		    _with1.Append("    End Function" + "\n");
		    _with1.Append("\n");
            _with1.Append("\tPrivate Shared Function IsSelectedInput(givenCode As String) As Boolean" + "\n");
		    _with1.Append("        Dim retValue As Boolean = False" + "\n");
		    _with1.Append("        If CA IsNot Nothing Then" + "\n");
		    _with1.Append("            For Each item As String In CA.Keys" + "\n");
		    _with1.Append("                If item = (givenCode) Then" + "\n");
		    _with1.Append("                    If CA(item).ToLower <> \"\" Then" + "\n");
		    _with1.Append("                        retValue = True" + "\n");
		    _with1.Append("                    End If" + "\n");
		    _with1.Append("                    Exit For" + "\n");
		    _with1.Append("                End If" + "\n");
		    _with1.Append("            Next" + "\n");
		    _with1.Append("        End If" + "\n");
		    _with1.Append("        return retValue" + "\n");
		    _with1.Append("    End Function" + "\n");
            _with1.Append("\tPrivate Shared Function IsSelectedExtTable(givenCode As String) As Boolean" + "\n");
		    _with1.Append("        Dim retValue As Boolean = False" + "\n");
		    _with1.Append("        If TE IsNot Nothing Then" + "\n");
		    _with1.Append("            For Each item As String In TE.Keys" + "\n");
		    _with1.Append("                If item = (givenCode) Then" + "\n");
		    _with1.Append("                    If TE(item).ToLower <> \"\" Then" + "\n");
		    _with1.Append("                        retValue = True" + "\n");
		    _with1.Append("                    End If" + "\n");
		    _with1.Append("                    Exit For" + "\n");
		    _with1.Append("                End If" + "\n");
		    _with1.Append("            Next" + "\n");
		    _with1.Append("        End If" + "\n");
		    _with1.Append("        return retValue" + "\n");
		    _with1.Append("    End Function" + "\n");
            _with1.Append("\tPrivate Shared Function GetInputValue(givenCode As String) As String" + "\n");
		    _with1.Append("        Dim retValue As String = \"\"" + "\n");
		    _with1.Append("        If CA IsNot Nothing Then" + "\n");
		    _with1.Append("            For Each item As String In CA.Keys" + "\n");
		    _with1.Append("                If item = (givenCode) Then" + "\n");
		    _with1.Append("                   retValue = CA(item)" + "\n");
		    _with1.Append("                   Exit For" + "\n");
		    _with1.Append("                End If" + "\n");
		    _with1.Append("            Next" + "\n");
		    _with1.Append("        End If" + "\n");
		    _with1.Append("        return retValue" + "\n");
		    _with1.Append("    End Function" + "\n");
            _with1.Append("\tPrivate Shared Function GetExtTableValue(givenCode As String) As String" + "\n");
		    _with1.Append("        Dim retValue As String = \"\"" + "\n");
		    _with1.Append("        If TE IsNot Nothing Then" + "\n");
		    _with1.Append("            For Each item As String In TE.Keys" + "\n");
		    _with1.Append("                If item = (givenCode) Then" + "\n");
		    _with1.Append("                   retValue = TE(item)" + "\n");
		    _with1.Append("                   Exit For" + "\n");
		    _with1.Append("                End If" + "\n");
		    _with1.Append("            Next" + "\n");
		    _with1.Append("        End If" + "\n");
		    _with1.Append("        return retValue" + "\n");
		    _with1.Append("    End Function" + "\n");
		    _with1.Append("\n");
            _with1.Append("\tPrivate Shared Function GetFeatureValue(givenCode As String) As String" + "\n");
            _with1.Append("        Dim retValue As String = \"\"" + "\n");
            _with1.Append("        If CP IsNot Nothing Then" + "\n");
            _with1.Append("            For Each item As String In CP.Keys" + "\n");
            _with1.Append("                If item = (givenCode) Then" + "\n");
            _with1.Append("                   retValue = CP(item)" + "\n");
            _with1.Append("                   Exit For" + "\n");
            _with1.Append("                End If" + "\n");
            _with1.Append("            Next" + "\n");
            _with1.Append("        End If" + "\n");
            _with1.Append("        return retValue" + "\n");
            _with1.Append("    End Function" + "\n");
            _with1.Append("\n");
            _with1.Append("\tPrivate Shared Function GetEXTERNALVARValue(givenCode As String) As String" + "\n");
            _with1.Append("        Dim retValue As String = \"\"" + "\n");
            _with1.Append("        If EXTERNALVAR IsNot Nothing Then" + "\n");
            _with1.Append("            For Each item As String In EXTERNALVAR.Keys" + "\n");
            _with1.Append("                If item = (givenCode) Then" + "\n");
            _with1.Append("                   retValue = EXTERNALVAR(item)" + "\n");
            _with1.Append("                   Exit For" + "\n");
            _with1.Append("                End If" + "\n");
            _with1.Append("            Next" + "\n");
            _with1.Append("        End If" + "\n");
            _with1.Append("        return retValue" + "\n");
            _with1.Append("    End Function" + "\n");
            _with1.Append("\n");
            _with1.Append("End Class");

            //System.IO.StreamWriter c = new System.IO.StreamWriter("c:\\temp\\test.vb");
            //c.Write(sb.ToString());
            //c.Close();
		    return sb.ToString();
	    }

	
	    public bool CompileCode(string givenFunction)
	    {
		    bool functionReturnValue = false;
		    Microsoft.VisualBasic.VBCodeProvider provider = null;
		    System.CodeDom.Compiler.ICodeCompiler compiler = null;
		    System.CodeDom.Compiler.CompilerParameters @params = null;

            givenFunction = PrepareFunctionVerify(givenFunction, FunctionType.F4CA);

		    @params = new System.CodeDom.Compiler.CompilerParameters();
		    @params.GenerateInMemory = true;
		    //Assembly is created in memory
		    @params.TreatWarningsAsErrors = false;
		    @params.WarningLevel = 4;
		    //Put any references you need here - even you own dll's, if you want to use one
		    string[] refs = {
			    "System.dll",
			    "Microsoft.VisualBasic.dll"
		    };

		    @params.ReferencedAssemblies.AddRange(refs);

		    try {
			    provider = new Microsoft.VisualBasic.VBCodeProvider();
			    compiler = provider.CreateCompiler();
			    results = compiler.CompileAssemblyFromSource(@params, givenFunction);
		    } catch (Exception ex) {
			    //Compile errors don't throw exceptions; you've got some deeper problem...
			
			    return functionReturnValue;
		    }

		    //'Output errors to the listbox
		    //lbErrors.Items.Clear()

		    //Dim err As System.CodeDom.Compiler.CompilerError
		    //For Each err In results.Errors
		    //    lbErrors.Items.Add(String.Format( _
		    //        "Line {0}, Col {1}: Error {2} - {3}", _
		    //        err.Line, err.Column, err.ErrorNumber, err.ErrorText))
		    //Next

		    //No compile errors or warnings...
		    if (results.Errors.Count == 0) {
			    mAssembly = results.CompiledAssembly;
			    return true;
		    } else {
			    mAssembly = null;
			    return false;
		    }
		    return functionReturnValue;
	    }

       
    }
	
}
