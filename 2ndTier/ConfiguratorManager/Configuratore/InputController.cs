﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using XTreeDemo;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
    public class InputController : XtreeNodeController
    {
        protected InputDef inputDef;
        protected OptionDef optionDef;
       
		public override string Name
		{
            get { return inputDef.Name; }
            set { inputDef.Name = value; }
		}

        public override string ID
        {
            get { return inputDef.ID; }
            set { inputDef.ID= value; }

        }

        public InputDef InputDef
		{
            get { return inputDef; }
		}

		public override object Item
		{
            get { return inputDef; }
		}

		public InputController()
		{
		}

        public InputController(InputDef newInputDef)
		{
            this.inputDef = newInputDef;
		}

		public override int Index(object item)
		{
			throw new XmlTreeException("Calling Index for TableFieldController is not permitted.");
		}

        public override bool AddNode(IXtreeNode parentInstance, string tag,string givenId)
        {
            if (parentInstance.GetType() == typeof(OptionController))
            {
                optionDef = ((OptionController)parentInstance).OptionDef;
                inputDef = new InputDef();
                inputDef.ID= givenId;
                optionDef.Inputs.Add(inputDef);
            }
          
            return true;
        }

        public override bool DeleteNode(IXtreeNode parentInstance)
        {
            if (parentInstance.GetType() == typeof(OptionController))
            {
                optionDef = ((OptionController)parentInstance).OptionDef;
                optionDef.Inputs.Remove(inputDef);
            }
           
            return true;
        }

        public override void AutoDeleteNode(IXtreeNode parentInstance)
        {
            if (parentInstance.GetType() == typeof(OptionController))
            {
                optionDef = ((OptionController)parentInstance).OptionDef;
                optionDef.Inputs.Remove(inputDef);
                
            }
            
        }

        public override void InsertNode(IXtreeNode parentInstance, int idx)
        {
            if (parentInstance.GetType() == typeof(OptionController))
            {
                optionDef = ((OptionController)parentInstance).OptionDef;
                optionDef.Inputs.Insert(idx, inputDef);
            }
           

        }

        public override void Select(TreeNode tn)
        {
            Program.Properties.SelectedObject = inputDef;
        }

        public Boolean Validate()
        {
            Boolean retValue = true;


            return retValue;

        }
       
    }
}
