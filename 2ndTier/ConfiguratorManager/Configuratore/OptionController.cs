using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public class OptionController : XtreeNodeController
	{
		protected OptionDef optionDef;
        protected OptionDef parentOptionDef;
        protected DialogDef parentDialogDef;

		public override string Name
		{
			get { return optionDef.Name; }
			set { optionDef.Name = value; }
		}

        public override string ID
        {
            get { return optionDef.ID; }
            set { optionDef.ID = value; }
           
        }

		public OptionDef OptionDef
		{
            get { optionDef.refreshCodifica(); return optionDef; }
		}

		public override object Item
		{
			get { return optionDef; }
		}

        
		public OptionController()
		{
		}

		public OptionController(OptionDef tableFieldDef)
		{
			this.optionDef = tableFieldDef;
            optionDef.refreshCodifica();
		}

		public override int Index(object item)
		{
			throw new XmlTreeException("Calling Index for TableFieldController is not permitted.");
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag,string givenId)
		{
            if (parentInstance.GetType().Name == "DialogController")
            {
                parentDialogDef = ((DialogController)parentInstance).DialogDef;
                optionDef = new OptionDef();
                optionDef.ID = givenId;
                optionDef.Codifica = parentDialogDef.Codifica; // vtlmrc: set della variabile codifica
                parentDialogDef.Options.Add(optionDef);
            }
            else
            {
                parentOptionDef = ((OptionController)parentInstance).OptionDef;
                optionDef = new OptionDef();
                optionDef.ID = givenId;
                optionDef.Codifica = parentOptionDef.Codifica; // vtlmrc: set della variabile codifica
                parentOptionDef.SubOptions.Add(optionDef);
                parentOptionDef.OptionType = parentOptionDef.OptionType;
            }
            //clsListsItems.AddQAItem(givenId, optionDef.Name);
			return true;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
            String givenId = optionDef.ID;
            String givenName = optionDef.Name;


            if (parentInstance.GetType().Name == "DialogController")
            {

                parentDialogDef = ((DialogController)parentInstance).DialogDef;
                parentDialogDef.Options.Remove(optionDef);
            }
            else
            {
                parentOptionDef = ((OptionController)parentInstance).OptionDef;
                parentOptionDef.SubOptions.Remove(optionDef);
            }
            //clsListsItems.RemoveQAItem(givenId, givenName);
            //parentOptionDef = ((OptionController)parentInstance).OptionDef;
            //parentOptionDef.SubOptions.Remove(optionDef);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
            String givenId = optionDef.ID;
            String givenName = optionDef.Name;

            parentOptionDef = ((OptionController)parentInstance).OptionDef;
            parentOptionDef.SubOptions.Remove(optionDef);

            //clsListsItems.RemoveQAItem(givenId, givenName);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
            parentOptionDef = ((OptionController)parentInstance).OptionDef;
            parentOptionDef.SubOptions.Insert(idx, optionDef);
		}

		public override void Select(TreeNode tn)
		{
			Program.Properties.SelectedObject = optionDef;
		}

        
	}
}
