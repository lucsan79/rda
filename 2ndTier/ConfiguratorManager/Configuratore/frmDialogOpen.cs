﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Clifton.Windows.Forms;


namespace XTreeDemo
{

    public partial class frmDialogOpen : Form
    {
        //RadioCheckedListBox newListBox;
        ListView newListBox;
        XTree currentTree;
        List<String> myListItems;
        clsCDInterface myService = null;
        System.Data.DataTable myListaDialoghi = null;
        public String openedFile = "";
        public List<String> fileFunctions = new List<String>();
        public String fileFeatureToCAD = "";
        public Int32 myeDialogId = -1;

        private class Item
        {
            public string strText;
            public string strValue;
            public override string ToString()
            {
                return this.strText;
            }
        }

        public frmDialogOpen()
        {
            
            String exitStatus="";
            this.FormClosing += new FormClosingEventHandler(DialogClosing);
            InitializeComponent();

            try
            {
                
                //newListBox = new RadioCheckedListBox();
                newListBox = new ListView();
                newListBox.MultiSelect = false;
                newListBox.StateImageList = imageList1;
                //newListBox.View = View.List ;
                newListBox.View = View.Details;
                this.newListBox.Columns.Add("Name");
                this.newListBox.HeaderStyle = ColumnHeaderStyle.None;
                panel1.Controls.Add(newListBox);
                newListBox.Dock = DockStyle.Fill;

                this.newListBox.Columns[0].Width = this.newListBox.Width - 20;


                myService=new clsCDInterface();
                myListaDialoghi = myService.GetListaDialoghi(true);
                if (myListaDialoghi != null)
                {
                    //foreach (DataRow myRow in myListaDialoghi.Rows)
                    for(Int32  idx = myListaDialoghi.Rows.Count-1;idx>=0;idx--)
                    {
                        DataRow myRow = myListaDialoghi.Rows[idx];
                        //Item myItem = new Item();
                       //if(myRow["eLevel"].ToString()=="Public")
                        //    myItem.strText ="(P) " + myRow["eName"] + " - " + myRow["eDescription"] ;
                        //else
                        //    myItem.strText = "(W) " + myRow["eName"] + " - " + myRow["eDescription"];
                        //myItem.strValue = myRow["eId"].ToString();
                        //newListBox.Items.Add(myItem);


                        ListViewItem myItem = new ListViewItem();
                        myItem.Text = myRow["eName"] + " - " + myRow["eDescription"];
                        myItem.Tag = myRow["eId"].ToString();
                        if (myRow["eLevel"].ToString() == "Public")
                        {
                            myItem.ImageIndex = 2;
                            myItem.StateImageIndex = 2;
                        }
                        else
                        {
                            myItem.ImageIndex = 1;
                            myItem.StateImageIndex = 1;
                        }
                            
                        newListBox.Items.Add(myItem);
                    }
                    newListBox.SelectedItems.Clear();
                    //newListBox.SelectedIndex = -1;
                }
                if (newListBox != null)
                    if (newListBox.Items.Count == 0)
                    {

                        MessageBox.Show("There aren't Dialog stored in CD to refer!", "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                        this.Close();
                    }
            }
            catch (Exception ex1) {
                MessageBox.Show(ex1.Message, "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            
            }
           
            
            
        }

        public frmDialogOpen(Object exit)
        {

           



        }


        private void DialogClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (myService != null)
                    myService.Logout();
            }
            catch (Exception ex) { }

            Cursor = Cursors.Default;
        }

        public String SelectedObject()
        {
            if (newListBox != null)
                if (newListBox.SelectedItems.Count  > 0)
                    return newListBox.SelectedItems[0].Tag.ToString();
                else
                    return "";
            else
                return "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Int32 dialogId = -1;
            
            try
            {
                if (newListBox != null)
                {
                    if (newListBox.SelectedItems.Count  == 0)
                          MessageBox.Show("Please select an option!", "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {

                        if (myListaDialoghi != null)
                        {
                            dialogId = -1;
                            dialogId = Int32.Parse(newListBox.SelectedItems[0].Tag.ToString());
                            if (dialogId > 0)
                            {
                                myeDialogId = dialogId;

                                if (myService.GetFileDialog(dialogId, ref openedFile, ref fileFunctions, ref fileFeatureToCAD))
                                {
                                    myService.Logout();
                                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                                    this.Close();

                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex1) { MessageBox.Show(ex1.Message, "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error); };
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            newListBox.SelectedItems.Clear();
            this.Close();
        }

        private void frmDialogOpen_Load(object sender, EventArgs e)
        {

        }


    }


}
