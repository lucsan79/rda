﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using Clifton.Windows.Forms;

namespace XTreeDemo
{
    public class MultiLineStringEditor : UITypeEditor
    {

        private IWindowsFormsEditorService service;
        
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (((context != null) & (context.Instance != null) & (provider != null)))
            {
                service = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
                if (((service != null)))
                {

                    if (context.Instance.GetType().Name == "DialogDef")
                    {
                        DialogDef myDialog = (DialogDef)context.Instance;
                        XTree sdTree = XTreeDemo.Program.GetTree();
                        frmSelectFunction myFunction = new frmSelectFunction(sdTree, "FUNCTION");
                        if (myFunction.ShowDialog() == DialogResult.OK)
                            value = myFunction.SelectedObject();
                        //frmRefOption myRefOption = new frmRefOption(sdTree);
                        //if (myRefOption.DialogResult != DialogResult.Cancel)
                        //    if (myRefOption.ShowDialog() == DialogResult.OK)
                        //    {
                        //        value = myRefOption.SelectedObject();
                        //       // if (value.ToString().Contains(" - "))
                        //       //     value = value.ToString().Substring(0, value.ToString().IndexOf(" - "));
                        //    }

                    }
                    else if (context.Instance.GetType().Name == "FunctionDef")
                    {
                        String id = ((FunctionDef)context.Instance).ID;
                        String content = ((FunctionDef)context.Instance).SourceCode;
                        TextEditorForm funWR = new TextEditorForm(id + ".vb");
                        System.Threading.Thread.Sleep(500);
                        funWR.SetTextValidated(content);
                        Application.DoEvents();
                        if (funWR.ShowDialog() == DialogResult.OK)
                            value = funWR.GetTextValidated();
                    }
                    else if (context.Instance.GetType().Name == "ConditionDef")
                    {
                        ConditionDef myCond = (ConditionDef)context.Instance;
                        if (myCond.Type == null)
                            MessageBox.Show("Please, select first 'ConditionType'");
                        //if (myCond.ReferementType == null)
                        //    MessageBox.Show("Please, select first 'ReferementType'");
                       
                        XTree sdTree = XTreeDemo.Program.GetTree();
                        String type = myCond.Type.ToString();
                        String field = provider.ToString();
                        if (type == "INPUT")
                            type = "INPUTDEF";
                        if (type == "EXTERNAL_TABLES")
                            type = "ExternalTable";
                        if (field.ToUpper().EndsWith("MAXVALUE") || field.ToUpper().EndsWith("MINVALUE"))
                            type = "FUNCTION";

                        frmSelectFunction myFunction = new frmSelectFunction(sdTree, type);
                        if (myFunction.ShowDialog() == DialogResult.OK)
                            value = myFunction.SelectedObject();

                        //frmRefOption myRefOption = new frmRefOption(sdTree , myCond.Type.ToString(),myCond.ID);
                        //if(myRefOption.DialogResult!=DialogResult.Cancel)
                        //    if (myRefOption.ShowDialog() == DialogResult.OK)
                        //        value = myRefOption.SelectedObject();
                           
                        
                    }
                    else if (context.Instance.GetType().Name == "InputDef")
                    {
                        String field = provider.ToString();
                        if (!field.EndsWith("Code") || field.ToUpper().EndsWith("MAXVALUE") || field.ToUpper().EndsWith("MINVALUE"))
                        {
                            XTree sdTree = XTreeDemo.Program.GetTree();
                            frmSelectFunction myFunction = new frmSelectFunction(sdTree, "FUNCTION");
                            if (myFunction.ShowDialog() == DialogResult.OK)
                                value = myFunction.SelectedObject();
                            //frmRefOption myRefOption = new frmRefOption(sdTree);
                            //if (myRefOption.DialogResult != DialogResult.Cancel)
                            //    if (myRefOption.ShowDialog() == DialogResult.OK)
                            //        value = myRefOption.SelectedObject();
                        }
                        else
                        {
                            InputDef myInput = (InputDef)context.Instance;
                            Cursor.Current = Cursors.WaitCursor;
                            
                            frmRefFeautesCD myRefFeature = new frmRefFeautesCD();
                            Cursor.Current = Cursors.Default;
                            if (myRefFeature.DialogResult != DialogResult.Cancel)
                            {
                                if (myRefFeature.ShowDialog() == DialogResult.OK)
                                {
                                    value = myRefFeature.SelectedObject();
                                    string code = value.ToString().Split("|".ToCharArray())[0];
                                    string name = value.ToString().Split("|".ToCharArray())[1];
                                    string val = value.ToString().Split("|".ToCharArray())[2];
                                    myInput.Code = code;
                                    myInput.Name = name;
                                    value = code;
                                    XTree sdTree = XTreeDemo.Program.GetTree();
                                    TreeNode rootNode = sdTree.Nodes[0];
                                    UpdateNode(ref rootNode, myInput.ID, code, name);
                                }
                            }
                        }
                    }
                    else if (context.Instance.GetType().Name == "ExternalTableDef")
                    {
                        XTree sdTree = XTreeDemo.Program.GetTree();
                         ExternalTableDef myTable = (ExternalTableDef)context.Instance;
                         frmQueryDesigner myFilter = new frmQueryDesigner(sdTree,ref myTable);
                         if (myFilter.ShowDialog() == DialogResult.OK)
                         {
                             value = myFilter.GetCurrentFilter();

                             TreeNode rootNode = sdTree.Nodes[0];
                             UpdateNode(ref rootNode, myTable.ID, "", myTable.Name );

                         }
                         ////////frmTableFilters myFilter = new frmTableFilters(sdTree, myTable.listFilter);
                         ////////if (myFilter.DialogResult != DialogResult.Cancel)
                         ////////    if (myFilter.ShowDialog() == DialogResult.OK)
                         ////////    {
                         ////////        value = myFilter.SelectedObject();

                         ////////    }
                    }
                    else if (context.Instance.GetType().Name == "FeatureDef")
                    {
                        FeatureDef myFeat = (FeatureDef)context.Instance;
                        XTree sdTree = XTreeDemo.Program.GetTree();
                        String field = provider.ToString();
                        if(field.EndsWith("Value"))
                        {
                            frmSelectFunction myFunction = new frmSelectFunction(sdTree,"FUNCTION");
                            if (myFunction.ShowDialog() == DialogResult.OK)
                                value = myFunction.SelectedObject();
                            // frmRefOption myRefOption = new frmRefOption(sdTree, "FUNCTION", myFeat.ID.Split(".".ToCharArray())[0]);
                            //if (myRefOption.DialogResult != DialogResult.Cancel)
                            //    if (myRefOption.ShowDialog() == DialogResult.OK)
                            //        value = myRefOption.SelectedObject();
                               
                        }
                        else if (field.EndsWith("Code"))
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            frmRefFeautesCD myRefFeature = new frmRefFeautesCD();
                            Cursor.Current = Cursors.Default;
                            if (myRefFeature.DialogResult != DialogResult.Cancel)
                                if (myRefFeature.ShowDialog() == DialogResult.OK)
                                {
                                    value = myRefFeature.SelectedObject();
                                    string code = value.ToString().Split("|".ToCharArray())[0];
                                    string name = value.ToString().Split("|".ToCharArray())[1];
                                    string val = value.ToString().Split("|".ToCharArray())[2];
                                    myFeat.Code = code;
                                    myFeat.Name = name;
                                    myFeat.Value = val;
                                    value = code;
                                    TreeNode rootNode = sdTree.Nodes[0];
                                    UpdateNode(ref rootNode, myFeat.ID, code, name);
                                }
                        }

                    }
                   
                }
            }
            return value;
        }
        private void UpdateNode(ref TreeNode givenNode, String id,String code, string name)
        {
            if (givenNode.Text.StartsWith(id))
            {
                givenNode.Text = id + " - " + name;
                return;
            }
            else
            {
                if (givenNode.Nodes != null)
                    for (Int32 index = 0; index < givenNode.Nodes.Count ; index++)
                    {
                        TreeNode subNode = givenNode.Nodes[index];
                        UpdateNode(ref subNode, id, code, name);
                    }
            }
        }
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if (((context != null) & (context.Instance != null)))
            {
                // we'll show a modal form
                return UITypeEditorEditStyle.Modal;
            }
            return base.GetEditStyle(context);
        }
    }
}