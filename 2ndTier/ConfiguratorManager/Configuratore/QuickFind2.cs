﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Clifton.Windows.Forms;

namespace XTreeDemo
{
    public partial class QuickFind2 : Form
    {
        public TreeView currentTRee;
        private string selected = "";
        private string searchText = "";
        private TreeNode lastNode = null;
        private List<TreeNode> listSelected = null;
        private Boolean stopSearch = false;

        public QuickFind2()
        {
            listSelected = new List<TreeNode>();
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (searchText == "")
                selected = "";
            else
            { 
                if(this.txtNodeTextSearch.Text!=searchText)
                {
                    selected = "";
                }
            }
            searchText = this.txtNodeTextSearch.Text;
            if (currentTRee != null)
            {
                FindByText();
            }
            stopSearch = false;
        }


        private void FindByText()
        {
            TreeNodeCollection nodes = currentTRee.Nodes;
            if (lastNode != null)
                lastNode.BackColor = Color.White;
            lastNode = null;
            foreach (TreeNode n in nodes)
            {
                FindRecursive(n);
                
                if (stopSearch)
                    return;
            }
            
        }


        private void FindRecursive(TreeNode treeNode)
        {
           
            foreach (TreeNode tn in treeNode.Nodes)
            {
                if (stopSearch)
                    return;
                // if the text properties match, color the item
                if (tn.Text.ToLower().Contains(searchText.ToLower()))
                {
                    if (selected.Contains(tn.Text.ToString()) == false)
                    {
                        
                        currentTRee.SelectedNode = tn;
                        currentTRee.SelectedNode = null;
                        tn.BackColor = Color.Yellow;
                        selected = selected + tn.Text + ";";
                        stopSearch = true;
                        lastNode = tn;
                        try
                        {
                            listSelected.Add(lastNode);
                        }
                        catch(Exception ex)
                        {}
                        
                        return;
                    }

                }
                

                FindRecursive(tn);
            }


        }

        private void QuickFind_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (listSelected != null)
            {
                foreach (TreeNode myNode in listSelected)
                    myNode.BackColor = Color.White;
            }
        }

        
    }
}
