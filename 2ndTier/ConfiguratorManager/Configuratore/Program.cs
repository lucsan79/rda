using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using Microsoft.Win32;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms;
using Clifton.Windows.Forms.XmlTree;

using MyXaml.Core;
using System.Reflection;
using System.Net;
using XTreeIIDemo.DialogAdminService;


namespace XTreeDemo
{
	public class Program
	{


        #region "VARIABILI / PROPERTY"

        

		[MyXamlAutoInitialize]
		private static XTree sdTree = null;

		[MyXamlAutoInitialize]
        PropertyGrid pgProperties = null;

		protected Form form;
		protected string caption;
		protected string schemaFilename;
		protected DialogDef dialogDef;
		protected TreeNode rootNode;
        protected static System.Data.DataTable cdFeatures;
        protected static PropertyGrid pgProps;
        protected Dictionary<string,TreeNode> myCopyArray;
        public Dictionary<string, string> myFeatureToBOM;
        public string fileConfFeatureToCAD = "";
        
        

		public static PropertyGrid Properties
		{
			get { return pgProps; }
		}
        public static System.Data.DataTable CDFeatures
        {
            get { return cdFeatures; }
            set { 
                cdFeatures = value; 
            }
        }
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
			new Program();
		}
        public static XTree GetTree()
        {
            return sdTree;
        }


#endregion

        #region "COSTRUTTORE"

        public Program()
		{
            Int32 rc = -1;
           
            
			Parser.AddExtender("MyXaml.WinForms", "MyXaml.WinForms", "WinFormExtender");
			Parser p = new Parser();
			p.AddReference("App", this);
			form = (Form)p.Instantiate("Config/schemaEditor.myxaml", "*");
			p.InitializeFields(this);
			pgProps = pgProperties;
            schemaFilename = String.Empty;
			caption = form.Text;
			form.Shown += new EventHandler(OnShown);
            form.StartPosition = FormStartPosition.CenterScreen;
            ////////sdTree.AfterSelect += new TreeViewEventHandler(TreeView1_AfterSelect);
            ////////sdTree.BeforeSelect += new TreeViewCancelEventHandler(TreeView1_BeforeSelect);
            sdTree.AllowDrop = false;
            form.Text = "CD Configurator";
            
            form.Icon = new Icon("images/dialog1.ico");
            myFeatureToBOM = new Dictionary<string, string>();
            Application.Run(form);
		}


        #endregion

        #region "OPERAZIONI SUI NODI"
        private Boolean ValidateNode(ref TreeNode givenNode)
        {
            Boolean retValue = true;
            String nodeType = ((NodeInstance)givenNode.Tag).Instance.GetType().Name;
            Boolean isDefault = false;
            Boolean isForceOption = false;
            switch (nodeType)
            {
                case "OptionController":
                    retValue = ((OptionController)((NodeInstance)givenNode.Tag).Instance).OptionDef.Validate();
                    isDefault = ((OptionController)((NodeInstance)givenNode.Tag).Instance).OptionDef.IsDefault;
                    isForceOption = ((OptionController)((NodeInstance)givenNode.Tag).Instance).OptionDef.ForceOption;
                    break;
                case "ConditionController":
                    retValue = ((ConditionController)((NodeInstance)givenNode.Tag).Instance).ConditionDef.Validate();
                    break;
                case "FunctionController":
                    retValue = ((FunctionController)((NodeInstance)givenNode.Tag).Instance).FunctionDef.Validate();
                    break;
                case "InputController":
                    retValue = ((InputController)((NodeInstance)givenNode.Tag).Instance).InputDef.Validate();
                    break;
                case "ExternalTableController":
                    retValue = ((ExternalTableController)((NodeInstance)givenNode.Tag).Instance).ExternalTableDef.Validate();
                    break;
                case "FeatureController":
                    retValue = ((FeatureController)((NodeInstance)givenNode.Tag).Instance).FeatureDef.Validate();
                    break;
                default:
                    break;
            }
            if (retValue)
            {
                givenNode.ForeColor = Color.Black;
                if (isDefault)
                    givenNode.ForeColor = Color.Green;
                else if (isForceOption)
                    givenNode.ForeColor = Color.Blue;
            }
            else
                givenNode.ForeColor = Color.Red;

            return retValue;
        }

        public void MoveUp(TreeNode node)
        {
            TreeNode parent = node.Parent;
            if (parent != null)
            {
                int index = parent.Nodes.IndexOf(node);
                if (index > 0)
                {
                    if (parent.Nodes[index - 1].Name == node.Name)
                    {
                        parent.Nodes.RemoveAt(index);
                        parent.Nodes.Insert(index - 1, node);


                        // bw : add this line to restore the originally selected node as selected
                        node.TreeView.SelectedNode = node;
                       
                        if (node.Name == "Option")
                        {
                            //OptionDef currentOption = ((OptionController)((NodeInstance)node.Tag).Instance).OptionDef;
                            if (parent.Name == "Dialogo")
                            {
                                String nodeId = node.Text.ToString().Split(" - ".ToCharArray())[0].Trim();
                                dialogDef.MoveOption(nodeId ,-1);
                            }
                            else
                            {
                                OptionDef currentOption = ((OptionController)((NodeInstance)node.Tag).Instance).OptionDef;
                                OptionDef parentOption = ((OptionController)((NodeInstance)parent.Tag).Instance).OptionDef;
                                parentOption.MoveSubOption(currentOption.ID, -1);
                            }
                        }
                        if (node.Name == "Feature")
                        {
                            FeatureDef currentFeature = ((FeatureController)((NodeInstance)node.Tag).Instance).FeatureDef;
                            OptionDef parentOption = ((OptionController)((NodeInstance)parent.Tag).Instance).OptionDef;
                            parentOption.MoveFeatures(currentFeature.ID, -1);
                            
                        }
                        if (node.Name == "Condition")
                        {

                            ConditionDef currentFeature = ((ConditionController)((NodeInstance)node.Tag).Instance).ConditionDef;
                            OptionDef parentOption = ((OptionController)((NodeInstance)parent.Tag).Instance).OptionDef;
                            parentOption.MoveCondition(currentFeature.ID, -1);
                            
                        }

                    }
                    else
                        index = 0;
                }
            }
        }

        public void MoveDown(TreeNode node)
        {
            TreeNode parent = node.Parent;
            if (parent != null)
            {
                int index = parent.Nodes.IndexOf(node);
                if (index < parent.Nodes.Count - 1)
                {
                    if (parent.Nodes[index + 1].Name == node.Name)
                    {
                        parent.Nodes.RemoveAt(index);

                        parent.Nodes.Insert(index + 1, node);
                        // bw : add this line to restore the originally selected node as selected
                        node.TreeView.SelectedNode = node;


                        if (node.Name == "Option")
                        {
                            
                            if (parent.Name == "Dialogo")
                            {
                                String nodeId = node.Text.ToString().Split(" - ".ToCharArray())[0].Trim();
                                dialogDef.MoveOption(nodeId, +1);
                            }
                            else
                            {
                                OptionDef currentOption = ((OptionController)((NodeInstance)parent.Tag).Instance).OptionDef;
                                currentOption.MoveSubOption(currentOption.ID, +1);
                            }
                        }
                        if (node.Name == "Feature")
                        {
                            FeatureDef currentFeature = ((FeatureController)((NodeInstance)node.Tag).Instance).FeatureDef;
                            OptionDef parentOption = ((OptionController)((NodeInstance)parent.Tag).Instance).OptionDef;
                            parentOption.MoveFeatures(currentFeature.ID, +1);

                        }
                        if (node.Name == "Condition")
                        {

                            ConditionDef currentFeature = ((ConditionController)((NodeInstance)node.Tag).Instance).ConditionDef;
                            OptionDef parentOption = ((OptionController)((NodeInstance)parent.Tag).Instance).OptionDef;
                            parentOption.MoveCondition(currentFeature.ID, +1);

                        }

                    }
                    else
                        index = parent.Nodes.Count;
                }
            }
        }

        protected void CopyNodes(TreeNode givenNode)
        {
            if (myCopyArray == null)
                myCopyArray = new Dictionary<string, TreeNode>();
            if (!myCopyArray.ContainsKey(givenNode.Text))
            {
                if (givenNode.Name == "InputDef")
                {
                    foreach (String key in myCopyArray.Keys)
                        if (myCopyArray[key].Name == "ExternalTable")
                        {
                            MessageBox.Show("In memoria � gi� presente una tabella esterna e non � possibile incollare una tabella e un input sotto una stessa option!","CD Configurator - COPY", MessageBoxButtons.OK );
                            return;
                        }

                }
                else if (givenNode.Name == "ExternalTable")
                {
                    foreach (String key in myCopyArray.Keys)
                        if (myCopyArray[key].Name == "ExternalTable")
                        {
                            MessageBox.Show("In memoria � gi� presente un riferimento ad una altra tabella esterna!");
                            return;
                        }
                        else if (myCopyArray[key].Name == "InputDef")
                        {
                            MessageBox.Show("In memoria � gi� presente un 'input' e non � possibile incollare una tabella e un input sotto una stessa option!", "CD Configurator - COPY", MessageBoxButtons.OK);
                            return;
                        }


                }
                myCopyArray.Add(givenNode.Text, givenNode);
                MenuStrip myMenu = (MenuStrip)form.Controls["confMenu"];
                if (myMenu != null)
                {
                    myMenu.Items["menuNodePaste"].Enabled = true;
                    myMenu.Items["menuNodeClearCopy"].Enabled = true;
                }
                if(myCopyArray.Count==1)
                    SetStatusLabel("1 oggetto in memoria");
                else
                    SetStatusLabel(myCopyArray.Count.ToString() + " oggetti in memoria");
                    
            }
            
        }
        protected void PasteNodes(TreeNode givenNode)
        {
            IXtreeNode controller=null;

            if (givenNode.Name == "Option")
            {
                OptionDef currentOption = ((OptionController)((NodeInstance)givenNode.Tag).Instance).OptionDef;
                if (myCopyArray != null)
                {
                    foreach (String key in myCopyArray.Keys)
                    {
                        
                        TreeNode tn1 = myCopyArray[key];
                        String newId = sdTree.GetNewId(givenNode, tn1);
                        if (tn1.Name == "Option")
                        {
                            OptionDef tfd = ((OptionController)((NodeInstance)tn1.Tag).Instance).OptionDef;
                            OptionDef copy = (OptionDef)MyCloner.DeepCopy(tfd);
                            copy.Features.Clear();
                            copy.Inputs.Clear();
                            copy.External_Tables.Clear();
                            copy.Conditions.Clear();
                            copy.SubOptions.Clear();
                            copy.ID = newId;
                            controller = new OptionController(copy);
                            currentOption.SubOptions.Add(copy);
                        }
                        else if (tn1.Name == "Feature")
                        {
                            FeatureDef tfd = ((FeatureController)((NodeInstance)tn1.Tag).Instance).FeatureDef;
                            FeatureDef copy = (FeatureDef)MyCloner.DeepCopy(tfd);
                            copy.ID = newId;
                            controller = new FeatureController(copy);
                            currentOption.Features.Add(copy);

                        }
                        else if (tn1.Name == "Condition")
                        {
                            ConditionDef tfd = ((ConditionController)((NodeInstance)tn1.Tag).Instance).ConditionDef;
                            ConditionDef copy = (ConditionDef)MyCloner.DeepCopy(tfd);
                            copy.ID = newId;
                            controller = new ConditionController(copy);
                            currentOption.Conditions.Add(copy);

                        }
                        else if (tn1.Name == "InputDef")
                        {
                            Boolean attach=true;
                            if (givenNode.Level == 1)
                                attach=false;
                            else
                            {
                                foreach (TreeNode subNode in givenNode.Nodes)
                                    if (subNode.Name == "ExternalTable")
                                        attach = false;
                                        
                            }
                            if (attach)
                            {
                                InputDef tfd = ((InputController)((NodeInstance)tn1.Tag).Instance).InputDef;
                                InputDef copy = (InputDef)MyCloner.DeepCopy(tfd);
                                copy.ID = newId;
                                controller = new InputController(copy);
                                currentOption.Inputs.Add(copy);
                            }
                            else
                                controller = null;

                        }
                        else if (tn1.Name == "ExternalTable")
                        {
                            Boolean attach=true;
                            if (givenNode.Level == 1)
                                continue; 
                            else
                            {
                                foreach (TreeNode subNode in givenNode.Nodes)
                                    if (subNode.Name == "ExternalTable" || subNode.Name == "InputDef")
                                        attach = false;

                            }
                            if (attach)
                            {
                                ExternalTableDef tfd = ((ExternalTableController)((NodeInstance)tn1.Tag).Instance).ExternalTableDef;
                                ExternalTableDef copy = (ExternalTableDef)MyCloner.DeepCopy(tfd);
                                copy.ID = newId;
                                controller = new ExternalTableController(copy);
                                currentOption.External_Tables.Add(copy);
                            }
                            else
                                controller = null;
                           

                        }

                        if (controller != null)
                        {

                            tn1 = sdTree.AddNode(controller, givenNode, controller.ID);
                            ValidateNode(ref tn1);
                            //sdTree.AddOptionID(newId, controller.ID);
                            givenNode.Expand();
                        }
 
                    }
                }
                myCopyArray = null;
                MenuStrip myMenu = (MenuStrip)form.Controls["confMenu"];
                if (myMenu != null)
                {
                    myMenu.Items["menuNodePaste"].Enabled = false;
                    myMenu.Items["menuNodeClearCopy"].Enabled = false;

                }
                SetStatusLabel("0 oggetti in memoria");
            }
            
        }


        protected void CreateRootNode(Boolean clearFlag)
        {

            sdTree.Nodes.Clear();
            if (!clearFlag)
            {

                DialogProperty p = new DialogProperty();
                p.famiglia = "RDA";
                p.prodotto = "STD";
                p.codifica = "#";
                p.tipologiaDialogo = "Standard";
                
                //if (p.ShowDialog() == DialogResult.OK)
                //{
                   //Collegamento dialogo alla famiglia if (p.famiglia != "" && p.prodotto != "" && p.codifica != "")
                    //if (p.famiglia != "" && p.codifica != "")
                    //{
                        dialogDef = new DialogDef(p.famiglia, p.prodotto, p.codifica, p.cadref, -1,p.tipologiaDialogo );
                        DialogController sc = new DialogController(dialogDef);
                        rootNode = sdTree.AddNode(sc, null, p.famiglia);
                        pgProperties.SelectedObject = dialogDef;
                        sdTree.SelectedNode = sdTree.Nodes[0];
                        sdTree.famiglia = p.famiglia;
                        sdTree.prodotto = p.prodotto;
                        sdTree.codifica = p.codifica;
                        sdTree.cadref = p.cadref;
                        sdTree.tipologiaDialogo = p.tipologiaDialogo;
                        MenuOperationOnNode(true);
                   // }
                   // else
                   //     MessageBox.Show("Errore! Valori Input non validi per creare un nuovo dialogo", "Error Dialog Configuration");

                //}
            }
            else
            {

                dialogDef = new DialogDef("", "", "", "", -1,"");
                DialogController sc = new DialogController(dialogDef);
                rootNode = sdTree.AddNode(sc, null, "");
                pgProperties.SelectedObject = dialogDef;
                sdTree.SelectedNode = sdTree.Nodes[0];
            }


        }

       

        #endregion

        #region "OPERAZION MENU"
        protected void OnShown(object sender, EventArgs e)
		{
			//CreateRootNode(false);
		}

		protected void OnNew(object sender, EventArgs e)
		{
            try
            {
                form.Text = "CD Configurator *";
                fileConfFeatureToCAD = "";
                MenuOperationOnNode(false);
                schemaFilename = String.Empty;
                ClearAll();
                sdTree.CleanID();
                CreateRootNode(false);
                pgProperties.SelectedObject = dialogDef;
                sdTree.SelectedNode = sdTree.Nodes[0];
                MenuOperationOnNode(true);
            }
            catch (Exception e1) { sdTree.Nodes.Clear(); }
		}


        protected void OnSearch(object sender, EventArgs e)
        {
            try
            {
                form.Text = "CD Configurator *";
                fileConfFeatureToCAD = "";
                MenuOperationOnNode(false);
                schemaFilename = String.Empty;
                ClearAll();
                CreateRootNode(false);
                pgProperties.SelectedObject = dialogDef;
                sdTree.SelectedNode = sdTree.Nodes[0];
                MenuOperationOnNode(true);
            }
            catch (Exception e1) { sdTree.Nodes.Clear(); }
        }

		protected void OnOpen(object sender, EventArgs e)
		{
            
            fileConfFeatureToCAD = "";
            MenuOperationOnNode(false);
            //////frmDialogOpen myOpenDialog1 = new frmDialogOpen(null);
            //////myOpenDialog1.openedFile = @"P:\Documents\Alpac\v1.5\Utility\XTreeII\XTreeIIDemo\bin\Debug\EXPORT\DLG-000016.xml";
            //////myOpenDialog1.fileFunctions = new List<string>();
            //////myOpenDialog1.fileFunctions.Add(@"P:\Documents\Alpac\v1.5\Utility\XTreeII\XTreeIIDemo\bin\Debug\EXPORT\SS-Fn1_QUOTAQ.vb");
            //////myOpenDialog1.fileFunctions.Add(@"P:\Documents\Alpac\v1.5\Utility\XTreeII\XTreeIIDemo\bin\Debug\EXPORT\SS-Fn2_CADNAME.vb");
            //////if (System.IO.File.Exists(myOpenDialog1.openedFile))
            //////{
            //////    schemaFilename = myOpenDialog1.openedFile;
            //////    ClearAll();
            //////    Load(myOpenDialog1.myeDialogId, myOpenDialog1.fileFunctions);
            //////}
            //////return;
            try
            {
                form.Text = "CD Configurator";
                frmDialogOpen myOpenDialog = new frmDialogOpen();
                String baseDownload = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory ,"EXPORT");
                try
                {
                   System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(baseDownload );

                    foreach(System.IO.FileInfo file in directory.GetFiles()) file.Delete();
                    foreach(System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
                }
                catch(Exception ex1){}
                myOpenDialog.openedFile = AppDomain.CurrentDomain.BaseDirectory + "EXPORT\\";
                if (myOpenDialog.ShowDialog() == DialogResult.OK)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    // myOpenDialog.fileFunctions = new List<string>();
                    if (System.IO.File.Exists(myOpenDialog.openedFile))
                    {
                        
                        schemaFilename = myOpenDialog.openedFile;
                        form.Text = "CD Configurator " + System.IO.Path.GetFileNameWithoutExtension(schemaFilename);
                        ClearAll();
                        CreateRootNode(true);
                        Load(myOpenDialog.myeDialogId, myOpenDialog.fileFunctions);
                        fileConfFeatureToCAD = myOpenDialog.fileFeatureToCAD;
                    }
                }
            }
            catch (Exception e1) { };
            MenuOperationOnNode(true);
            Cursor.Current = Cursors.Default;

            
		}

        protected void OnPublish(object sender, EventArgs e)
        {
            if (sdTree == null)
                return;
            if (sdTree.Nodes.Count == 0)
                return;
            clsCDInterface myService = new clsCDInterface();
            if (dialogDef.CollaborationID > 0)
                myService.PromoteDialog(dialogDef);
            else
                MessageBox.Show("Dialogo ancora non registrato nel PLM");

        }
        protected void OnUnPublish(object sender, EventArgs e)
        {
            clsCDInterface myService = new clsCDInterface();
            if (sdTree == null)
                return;
            if (sdTree.Nodes.Count == 0)
                return;
            if (dialogDef.CollaborationID > 0)
                myService.DeomoteDialog(dialogDef);
            else
                MessageBox.Show("Dialogo ancora non registrato nel PLM");
        }
		protected void OnSave(object sender, EventArgs e)
		{
           // Cursor.Current = Cursors.WaitCursor;
            MenuOperationOnNode(false);
            String message = "";
            try
            {

                Save();
                SetStatusLabel("Dialogo Salvato");
            }
            catch (Exception ex) {
                message = ex.Message;
            };

            MenuOperationOnNode(true);
            //Cursor.Current = Cursors.Default;
			
			
			
		}

        protected void OnImport(object sender, EventArgs e)
		{
           // Cursor.Current = Cursors.WaitCursor;
            MenuOperationOnNode(false);
            String message = "";
            try
            {

                ImportDialogProperty myImport = new ImportDialogProperty();
                myImport.StartPosition = FormStartPosition.CenterParent;
                if (myImport.ShowDialog() == DialogResult.OK)
                {
                    SetStatusLabel("Dialogo Importato");
                }
                
            }
            catch (Exception ex) {
                message = ex.Message;
            };

            MenuOperationOnNode(true);
            //Cursor.Current = Cursors.Default;
			
			
			
		}


       

        private void SetStatusLabel(String message)
        {
            StatusStrip myStatus = (StatusStrip)form.Controls["confStatus"];
            if (myStatus != null)
            {
                myStatus.Items["toolStripStatusLabel1"].Text =System.DateTime.Now.ToShortTimeString() + " " + message;
            }

        }
        protected void OnExit(object sender, EventArgs e)
		{
			Application.Exit();
		}
        protected void OnExpandNode(object sender, EventArgs e)
        {
            if (sdTree != null)
            {
                if (sdTree.SelectedNode != null)
                {
                    sdTree.SelectedNode.ExpandAll(); 
                }
            }
        }

        protected void OnCollapseNode(object sender, EventArgs e)
        {
            if (sdTree != null)
            {
                if (sdTree.SelectedNode != null)
                {
                    sdTree.SelectedNode.Collapse(); 
                }
            }
        }

        protected void OnMoveNodeUp(object sender, EventArgs e)
        {
            if (sdTree != null)
            {
                if (sdTree.SelectedNode != null)
                {
                    MoveUp(sdTree.SelectedNode);
                }
            }
        }

        protected void OnMoveNodeDown(object sender, EventArgs e)
        {

            if (sdTree != null)
            {
                if (sdTree.SelectedNode != null)
                {
                    MoveDown(sdTree.SelectedNode);
                }
            }
            
        }

        protected void OnCopyNode(object sender, EventArgs e)
        {
            if (sdTree != null)
            {
                if (sdTree.SelectedNode != null)
                    if(sdTree.SelectedNode.Level>0)
                        CopyNodes(sdTree.SelectedNode);
                
            }
        }

        protected void OnSearchNode(object sender, EventArgs e)
        {
            if (sdTree != null)
            {
                if (sdTree.Nodes.Count > 0)
                {
                    QuickFind myFind = new QuickFind();
                    myFind.currentTRee = sdTree;
                    myFind.Show();
                }

            }
        }

        protected void OnClearCopyNode(object sender, EventArgs e)
        {
            myCopyArray = null;
            MenuStrip myMenu = (MenuStrip)form.Controls["confMenu"];
            if (myMenu != null)
            {
                myMenu.Items["menuNodePaste"].Enabled = false;
                myMenu.Items["menuNodeClearCopy"].Enabled = false;
                
            }
            SetStatusLabel("0 oggetti in memoria");

            
        }

        
        protected void OnPasteNode(object sender, EventArgs e)
        {
            if (sdTree != null)
            {
                if (sdTree.SelectedNode != null)
                {
                    PasteNodes(sdTree.SelectedNode);
                }
            }
        }

        protected void OnLinkCPCAD(object sender, EventArgs e)
        {
            
            if (sdTree != null)
            {

                frmCPtoBOM myCPBOM;
                myCPBOM = new frmCPtoBOM(sdTree, sdTree.famiglia, sdTree.prodotto, fileConfFeatureToCAD);
                DialogResult myResult = myCPBOM.ShowDialog();

                if (myResult == DialogResult.OK)
                { 
                    fileConfFeatureToCAD=myCPBOM.fileConfiguration;
                }

               
            }
        }

        #endregion



        private void UpdateCondition(TreeNode givenNode,string oldValue, string newValue)
        {
            if (givenNode == null)
                if (sdTree != null)
                    if (sdTree.Nodes.Count > 0)
                        givenNode = sdTree.Nodes[0];

            if (givenNode != null)
            {
                if (givenNode.Name.Equals("Condition"))
                {
                    ConditionDef cond = ((ConditionController)((NodeInstance)givenNode.Tag).Instance).ConditionDef;
                    if (cond.ReleatedTo == oldValue)
                    {
                        cond.ReleatedTo = newValue;
                        //givenNode.Tag = cond;
                    }
                }
                if (givenNode.Nodes != null)
                    foreach (TreeNode subNode in givenNode.Nodes)
                        UpdateCondition(subNode, oldValue, newValue);
            }

        }

        
        private void UpdateFunction(TreeNode givenNode, string oldValue, string newValue)
        {
            if (givenNode == null)
                if (sdTree != null)
                    if (sdTree.Nodes.Count > 0)
                        givenNode = sdTree.Nodes[0];

            if (givenNode != null)
            {
                if (givenNode.Name.Equals("Feature"))
                {
                    FeatureDef cond = ((FeatureController)((NodeInstance)givenNode.Tag).Instance).FeatureDef;
                    if (cond.Name == oldValue)
                    {
                        cond.Name = newValue;
                        //givenNode.Tag = cond;
                    }
                }
                if (givenNode.Nodes != null)
                    foreach (TreeNode subNode in givenNode.Nodes)
                        UpdateFunction(subNode, oldValue, newValue);
            }

        }
       
        private void UpdateExtTableFilter(TreeNode givenNode, string oldValue, string newValue)
        {
            ////////if (givenNode == null)
            ////////    if (sdTree != null)
            ////////        if (sdTree.Nodes.Count > 0)
            ////////            givenNode = sdTree.Nodes[0];

            ////////if (givenNode != null)
            ////////{
            ////////    if (givenNode.Name.Equals("ExternalTable"))
            ////////    {
            ////////        ExternalTableDef cond = ((ExternalTableController)((NodeInstance)givenNode.Tag).Instance).ExternalTableDef;
            ////////        if (cond != null)
            ////////        {
            ////////            if (cond.listFilter != null)
            ////////            {
            ////////                for(Int32 index =0;index<cond.listFilter.Count;index++)
            ////////                {
            ////////                    String filterValues =cond.listFilter[index];
            ////////                    String code = filterValues.Split("|".ToCharArray())[2];
                                
            ////////                    if (code.Contains(oldValue))
            ////////                    {
            ////////                        filterValues = filterValues.Replace(code, newValue);
            ////////                        cond.listFilter[index] = filterValues;
            ////////                    }

            ////////                }
                            
            ////////            }
            ////////        }
                  
            ////////    }
            ////////    if (givenNode.Nodes != null)
            ////////        foreach (TreeNode subNode in givenNode.Nodes)
            ////////            UpdateExtTableFilter(subNode, oldValue, newValue);
            ////////}

        }
		/// <summary>
		/// Updates the XML node with the name set in the property grid.
		/// </summary>
		protected void OnPropertyValueChanged(object sender, PropertyValueChangedEventArgs e)
		{
            // vtlmrc: commentati gli if-else-if e sostituiti con uno switch per aumentere la leggibilit�
            
            switch (e.ChangedItem.Label)
            {
                case "Name": 
                    string oldId,oldname,newid,newname="";
                    oldId = sdTree.SelectedNode.Text.Substring(0, sdTree.SelectedNode.Text.IndexOf(" - ")).Trim();
                    oldname = e.OldValue.ToString();
                    newid=oldId;
                    newname=e.ChangedItem.Value.ToString();
                    UpdateCondition(null,oldId + " - " + oldname, newid + " - " + newname);
                    UpdateFunction(null, oldId + " - " + oldname, newid + " - " + newname);
                    UpdateExtTableFilter(null, oldId + " - " + oldname, newid + " - " + newname);

                    if((e.OldValue + "")!="")
				        sdTree.SelectedNode.Text =oldId + " - " + newname;// sdTree.SelectedNode.Text.Replace(e.OldValue.ToString(), e.ChangedItem.Value.ToString());
                    else
                        sdTree.SelectedNode.Text =sdTree.SelectedNode.Text + " - " + e.ChangedItem.Value.ToString();

                    //if (sdTree.SelectedNode.Name.Equals("Option"))
                    //    clsListsItems.UpdateQAItem(newid, newname, oldId, oldname);
                    //else if (sdTree.SelectedNode.Name.Equals("Input"))
                    //    clsListsItems.UpdateCA(newid, newname, oldId, oldname);
                    //if (sdTree.SelectedNode.Name.Equals("Function"))
                    //    clsListsItems.UpdateFunItem(newid, newname, oldId, oldname);
                    break;
                case "ConditionType":
                    if (!e.OldValue.Equals(e.ChangedItem.Value))
                    {
                        GridItem gi;
                        gi = e.ChangedItem;
                        while (gi.Parent != null)
                        {
                            gi = gi.Parent;
                            if (gi.Value != null)
                            {
                                if (gi.Value.GetType().Name == "ConditionDef")
                                {
                                    ConditionDef tmpCondition = (ConditionDef)gi.Value;
                                    tmpCondition.ReleatedTo = "";
                                    ((PropertyGrid)sender).SelectedObject = tmpCondition;
                                }
                            }
                        }
                    }
                    break;
                case "Digit": // vtlmrc: cosa della modifica del campo digit
                    if (e.OldValue == null ||  !e.OldValue.Equals(e.ChangedItem.Value))
                    {

                        PropertyGrid propertyGrid = (PropertyGrid)sender;
                        OptionDef answerDef = (OptionDef)propertyGrid.SelectedObject;
                        answerDef.Digit = e.ChangedItem.Value.ToString();
                        if(answerDef.Digit!="" && answerDef.DigitPosition!="")
                        {
                            answerDef.refreshCodifica();
                            propertyGrid.Refresh();
                        }

                    }
                    break;
                case "DigitPosition": // vtlmrc: cosa della modifica del campo digitPosition
                    if (e.OldValue == null || !e.OldValue.Equals(e.ChangedItem.Value))
                    {
                        int result;
                        if (!int.TryParse(e.ChangedItem.Value.ToString(), out result))
                        {
                            MessageBox.Show("'" + e.ChangedItem.Value.ToString() + "' is Not a valid integer for DigitPosition");
                            PropertyGrid propertyGrid = (PropertyGrid)sender;
                            OptionDef answerDef = (OptionDef)propertyGrid.SelectedObject;
                            answerDef.DigitPosition = e.OldValue.ToString();
                            propertyGrid.Refresh();
                        }
                        else
                        {
                            
                            PropertyGrid propertyGrid = (PropertyGrid)sender;
                            OptionDef answerDef = (OptionDef)propertyGrid.SelectedObject;
                            if (result > answerDef.Codifica.Length)
                            {
                                MessageBox.Show("'" + e.ChangedItem.Value.ToString() + "' is bigger than code length");
                                answerDef.DigitPosition = e.OldValue.ToString();
                            }
                            else
                                answerDef.DigitPosition = e.ChangedItem.Value.ToString();
                            if (answerDef.Digit != "" && answerDef.DigitPosition != "")
                            {
                                answerDef.refreshCodifica();
                                propertyGrid.Refresh();
                            }
                        }
                    }
                    break;
                case "CadPropertyName":
                    {
                        PropertyGrid propertyGrid = (PropertyGrid)sender;
                        OptionDef answerDef = (OptionDef)propertyGrid.SelectedObject;
                        PropertyDescriptor descriptor = TypeDescriptor.GetProperties(answerDef.GetType())["CadPropertyValue"];
                        ReadOnlyAttribute attrib = (ReadOnlyAttribute)descriptor.Attributes[typeof(ReadOnlyAttribute)];
                        FieldInfo isReadOnly = attrib.GetType().GetField("isReadOnly", BindingFlags.NonPublic | BindingFlags.Instance);
                        if (e.ChangedItem.Value.ToString() != "")
                            isReadOnly.SetValue(attrib, false);
                        else
                            isReadOnly.SetValue(attrib, true);
                        if (e.ChangedItem.Value.ToString() == "")
                            answerDef.CadPropertyValue = "";
                        propertyGrid.Refresh();
                        break;
                    }
                case "InputDataType":
                    {
                        PropertyGrid propertyGrid = (PropertyGrid)sender;
                        InputDef answerDef = (InputDef)propertyGrid.SelectedObject;
                        PropertyDescriptor descriptor = TypeDescriptor.GetProperties(answerDef.GetType())["InputFunction"];
                        BrowsableAttribute attrib = (BrowsableAttribute)descriptor.Attributes[typeof(BrowsableAttribute)];
                        FieldInfo isBrowsable = attrib.GetType().GetField("browsable", BindingFlags.NonPublic | BindingFlags.Instance);
                        if (e.ChangedItem.Value.ToString() != "F")
                            isBrowsable.SetValue(attrib, false);
                        else
                            isBrowsable.SetValue(attrib, true);
                        propertyGrid.Refresh();
                        break;
                    }
                case "OptionType":
                    {
                        PropertyGrid propertyGrid = (PropertyGrid)sender;
                        OptionDef answerDef = (OptionDef)propertyGrid.SelectedObject;
                        PropertyDescriptor descriptor = TypeDescriptor.GetProperties(answerDef.GetType())["OptionType"];
                        if (sdTree.tipologiaDialogo == "Dialogo di Configurazione" || sdTree.tipologiaDialogo == "")
                        {
                            if (e.ChangedItem.Value.ToString() == OptionDef.AnswerSelectionType.AutomaticFeaturesCompilation.ToString())
                            {
                                MessageBox.Show("Attenzione, il dialogo di configurazione non prevede la compilazione automatica delle features");
                                answerDef.OptionType=(OptionDef.AnswerSelectionType)(e.OldValue);
                                break;
                            }
                         }
                        break;
                    }
                default:
                    break;
            }
            TreeNode currentNode = sdTree.SelectedNode;
            ValidateNode(ref currentNode);
            if (e.ChangedItem.Value != e.OldValue)
            {
                PropertyGrid propertyGrid = (PropertyGrid)sender;
                ValidateNode((TreeNode)sdTree.SelectedNode,false);
                propertyGrid.Refresh();
            }
		}

        

        /// <summary>
        

		

		protected void ClearAll()
		{
           // sdTree = new XTree();
			sdTree.Clear();
           
			//UpdateCaption();
			//CreateRootNode(true);
		}

        protected void Load(Int32 givenCDId, List<String> fileFunction)
		{
            dialogDef = clsDialogReadWriter.ReadDialog(ref sdTree, schemaFilename, givenCDId);
            if (fileFunction != null)
                foreach (String fileFun in fileFunction)
                    dialogDef = clsDialogReadWriter.ReadDialogFunction(dialogDef, fileFun);
            PopulateTree();
		}

		protected void Save()
		{
            Cursor.Current= Cursors.WaitCursor;
            try
            {
                String filefullName = "";
                List<String> functionFiles = new List<String>();

                if (sdTree != null)
                    if (sdTree.Nodes != null)
                        if (sdTree.Nodes.Count > 0)
                        {
                            dialogDef = ((DialogController)((NodeInstance)sdTree.Nodes[0].Tag).Instance).DialogDef;
                            //if (dialogDef != null)
                            //{
                            //    if (dialogDef.CadRef == "")
                            //    {
                            //        MessageBox.Show("Indicare un Riferimento CAD valido per il dialogo, per procedere al salvataggio");
                            //        return;
                            //    }
                                if (dialogDef.Codifica == "")
                                {
                                    MessageBox.Show("Indicare una Codifica valida per la configurazione, per procedere al salvataggio oppure usare il default '#'");
                                    return;
                                }
                            //    if (dialogDef.Prodotto == "")
                            //    {
                            //        MessageBox.Show("Indicare un Prodotto valido per il dialogo, per procedere al salvataggio");
                            //        return;
                            //    }
                               

                            //}
                            filefullName = clsDialogReadWriter.WriteDialog(dialogDef);
                            functionFiles = clsDialogReadWriter.WriteDialogFunction(dialogDef);
                        }

                if (VerifyAllID() == true)
                {
                    if (System.IO.File.Exists(filefullName))
                    {
                        clsCDInterface myService = new clsCDInterface();
                        Boolean retValue = false;
                        try
                        {
                            if (dialogDef.CollaborationID > 0)
                            {

                                retValue = myService.UpdateDialog(dialogDef, filefullName, functionFiles, fileConfFeatureToCAD);
                            }
                            else
                                retValue = myService.CreateDialog(ref dialogDef, filefullName, functionFiles);

                            form.Text = "CD Configurator " + dialogDef.CollaborationName;
                        }
                        catch (Exception ex1) { MessageBox.Show(ex1.Message, "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                        myService.Logout();
                        myService = null;
                    }
                }
            }
            catch (Exception ex2) { };
            Cursor.Current = Cursors.Default;
           

		}

        
		protected void PopulateTree()
		{
			IXtreeNode controller;
			TreeNode tn;
			sdTree.SuspendLayout();
            sdTree.CleanID();
           // CreateRootNode(false);
           // CreateRootNode(true);
            sdTree.Nodes[0].Text = dialogDef.Famiglia;// +" - " + dialogDef.Prodotto;
            sdTree.famiglia = dialogDef.Famiglia;
            sdTree.prodotto = dialogDef.Prodotto;
            sdTree.tipologiaDialogo = dialogDef.Tipologia;
            IXtreeNode parentInst = null;
           
            foreach (FunctionDef functionDef in dialogDef.Functions)
            {
                
                controller = new FunctionController(functionDef);
                tn = sdTree.AddNode(controller, rootNode, controller.ID);
                string parentId = controller.ID.Substring(controller.ID.IndexOf("-Fn") +3);
                sdTree.AddFunctionID(dialogDef.Famiglia, Int32.Parse(parentId));
                ValidateNode(ref tn);
            }

			foreach (OptionDef tableDef in dialogDef.Options)
			{
                
				controller = new OptionController(tableDef);
                tn = sdTree.AddNode(controller, rootNode, controller.ID);
                ValidateNode(ref tn);
                PopulateTreeNode(tableDef, ref tn);
                sdTree.AddOptionID(dialogDef.Famiglia, controller.ID);

				
			}

            sdTree.CollapseAll();
			sdTree.Nodes[0].Expand();
			sdTree.ResumeLayout();
            //sdTree.ExpandAll();
			pgProperties.SelectedObject = dialogDef;
            sdTree.SelectedNode = sdTree.Nodes[0];
            
		}


        protected void PopulateTreeNode(OptionDef tableDef, ref TreeNode tn)
        {
            IXtreeNode controller;
            TreeNode tn1;
            foreach (ConditionDef tfd in tableDef.Conditions)
            {
                controller = new ConditionController(tfd);
                tn1 = sdTree.AddNode(controller, tn, controller.ID);
                ValidateNode(ref tn1);
                sdTree.AddCondID(tableDef.ID, Int32.Parse(controller.ID.Split(".".ToCharArray())[1]));
            }

            foreach (FeatureDef tfd in tableDef.Features)
            {
                controller = new FeatureController(tfd);
                tn1 = sdTree.AddNode(controller, tn, controller.ID);
                ValidateNode(ref tn1);
                sdTree.AddFeatureID(tableDef.ID, Int32.Parse(controller.ID.Split(".".ToCharArray())[1]));
            }

            //foreach (OptionDef tfd in tableDef.SubOptions)
            //{
            //    controller = new OptionController(tfd);
            //    tn1 = sdTree.AddNode(controller, tn, controller.ID);
            //    PopulateTreeNode(tfd, ref tn1);
            //}
            if (tableDef.Inputs.Count > 0)
            {
                foreach (InputDef tfd in tableDef.Inputs)
                {
                    controller = new InputController(tfd);
                    tn1 = sdTree.AddNode(controller, tn, controller.ID);
                    ValidateNode(ref tn1);
                    String inpId = controller.ID.Replace(tableDef.ID + "-Inp", "");
                    sdTree.AddInputID(tableDef.ID, Int32.Parse(inpId));
                    //PopulateTreeNode(tfd, ref tn1);
                }
            }
            else if (tableDef.External_Tables.Count > 0)
            {
                foreach (ExternalTableDef  tfd in tableDef.External_Tables)
                {
                    controller = new ExternalTableController (tfd);
                    tn1 = sdTree.AddNode(controller, tn, controller.ID);
                    ValidateNode(ref tn1);
                    String inpId = controller.ID.Replace(tableDef.ID + "-Ext", "");
                    sdTree.AddExtTableID(tableDef.ID, Int32.Parse(inpId));
                }
            }
            else if (tableDef.SubOptions.Count > 0)
            {
                foreach (OptionDef tfd in tableDef.SubOptions)
                {
                    controller = new OptionController(tfd);
                    tn1=sdTree.AddNode(controller, tn, controller.ID);
                    ValidateNode(ref tn1);
                    sdTree.AddOptionID(tableDef.ID , controller.ID);

                    PopulateTreeNode(tfd, ref tn1);
                }
            }

        }

	
    
        Boolean ValidateNode(TreeNode givenNode,Boolean recursive)
        {
            Boolean retValue = false;
            if (givenNode == null)
                givenNode = sdTree.Nodes[0];

            if (givenNode != null)
            {
                givenNode.BackColor = Color.Transparent;
                String nodeType = ((NodeInstance)givenNode.Tag).Instance.GetType().Name;

                switch (nodeType)
                {
                    case "DialogController":
                        retValue = DialogController.Validate(((DialogController)((NodeInstance)givenNode.Tag).Instance).DialogDef);
                        break;
                    case "OptionController":
                        retValue = ((OptionController)((NodeInstance)givenNode.Tag).Instance).OptionDef.Validate();
                        break;
                    case "ConditionController":
                        retValue =((ConditionController)((NodeInstance)givenNode.Tag).Instance).ConditionDef.Validate();
                        break;
                    case "FunctionController":
                        retValue = ((FunctionController)((NodeInstance)givenNode.Tag).Instance).FunctionDef.Validate();
                        break;
                    case "InputController":
                        retValue = ((InputController)((NodeInstance)givenNode.Tag).Instance).InputDef.Validate();
                        break;
                    case "ExternalTableController":
                        retValue = ((ExternalTableController)((NodeInstance)givenNode.Tag).Instance).ExternalTableDef.Validate();
                        break;

                    default:
                    break;
                }
               
                
                if(recursive)
                    if(givenNode.Nodes!=null)
                        foreach (TreeNode subNodes in givenNode.Nodes)
                        {
                            ValidateNode(subNodes, recursive);
                        }

            }
            return retValue;

        }


     

        private void MenuOperationOnNode(Boolean enable_disable_flag)
        {
            if (enable_disable_flag)
            {
                if (sdTree != null)
                    if (sdTree.Nodes.Count == 0)
                        enable_disable_flag = false;
            }
            MenuStrip myMenu = (MenuStrip)form.Controls["confMenu"];
            if (myMenu != null)
            {
                myMenu.Items["menuNodeExpand"].Enabled = enable_disable_flag;
                myMenu.Items["menuNodeCollapse"].Enabled = enable_disable_flag;
                myMenu.Items["menuNodeUp"].Enabled = enable_disable_flag;
                myMenu.Items["menuNodeDown"].Enabled = enable_disable_flag;
                myMenu.Items["menuNodeCopy"].Enabled = enable_disable_flag;
                myMenu.Items["menuNodeLinkCPCAD"].Enabled = enable_disable_flag;
                if (!enable_disable_flag)
                {
                    myMenu.Items["menuNodePaste"].Enabled = enable_disable_flag;
                    myMenu.Items["menuNodeClearCopy"].Enabled = enable_disable_flag;
                    myCopyArray = null;
                }

            }
        }



        private Boolean  VerifyAllID()
        {
            TreeNodeCollection nodes = sdTree.Nodes;
            List<String> myListIDToCheck = new List<string>();
            List<String> myListIDToDuplicate = new List<string>();
            foreach (TreeNode n in nodes)
            {
                VerifyAllIDRecursive(n, ref myListIDToCheck, ref myListIDToDuplicate);
            }
            if (myListIDToDuplicate.Count > 0)
            {
                String message = "Attenzione! trovati domande con identico ID\n";
                foreach (String domanda in myListIDToDuplicate)
                    message = message + domanda + "\n";
                MessageBox.Show(message, "Admin Dialog", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }


        private void VerifyAllIDRecursive(TreeNode treeNode, ref List<String> myListIDToCheck, ref List<String> myListIDToDuplicate)
        {

            foreach (TreeNode tn in treeNode.Nodes)
            {
                if (tn.Name == "Option")
                {
                    OptionDef tfd = ((OptionController)((NodeInstance)tn.Tag).Instance).OptionDef;
                    if (myListIDToCheck.Contains(tfd.ID) == true)
                    {
                        if (myListIDToDuplicate.Contains(tn.Text) == false)
                            myListIDToDuplicate.Add(tn.Text);
                    }
                    else
                    {
                        myListIDToCheck.Add(tfd.ID);
                    }
                }

                VerifyAllIDRecursive(tn, ref myListIDToCheck, ref myListIDToDuplicate);
            }


        }

       
    }
}
