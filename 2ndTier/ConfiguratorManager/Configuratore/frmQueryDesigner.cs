﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XTreeIIDemo.DialogAdminService;
using Clifton.Windows.Forms;

namespace XTreeDemo
{
    public partial class frmQueryDesigner : Form
    {
        private Dictionary<string, DataTable> TablesDAta;
        DialogAdminService myService;
        String lastData = "";
        string lastFiltri = "";
        TreeNode currentFilterNode = null;
        XTree currentTree;
        List<String> myListItems = new List<String>();
        String selectedFilter = "";
        ExternalTableDef currentExtTable;

        public string GetCurrentFilter()
        {
            return selectedFilter;
        }
        public frmQueryDesigner(XTree givenTree, ref ExternalTableDef givenExtTable)
        {
            InitializeComponent();
            currentTree = givenTree;
            TablesDAta = new Dictionary<string, DataTable>();
            FillDataSet();
            GetFunList(null);
            currentExtTable = givenExtTable;

            if (_treeTables.Nodes != null)
            {
                foreach (TreeNode myTableNode in _treeTables.Nodes)
                {
                    if (myTableNode.Text == currentExtTable.Name)
                    {
                        _treeTables.SelectedNode = myTableNode;
                        disegnaFiltriTabella();
                        break;
                    }
                }
            }

          
        }

        private void GetFunList(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Function"))
                myListItems.Add(givenNode.Text.Trim());
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetFunList(node);
        }

        public void FillDataSet()
        {
            String retMessage="";
            String exitStatus = "";
            System.Net.CookieContainer myContainer = new System.Net.CookieContainer();
            myService = new DialogAdminService();
            myService.CookieContainer = myContainer;
            myService.Url = System.Configuration.ConfigurationSettings.AppSettings["EPLMS_INSTANCE_URL"].ToString();
           // if (myService.login("DialogAdmin", "DialogAdmin", ref exitStatus) != 0)
            if (myService.login("eplms", "eplms", ref exitStatus) != 0)
            {
                throw new Exception(exitStatus);
            }
            else
            {
                DataSet myDataSet = myService.GetTabelleEsterne(ref retMessage);

                if (myDataSet != null)
                {
                    foreach (DataTable dummyTable in myDataSet.Tables)
                    {
                        TreeNode myNode = new TreeNode();
                        myNode.Text = dummyTable.TableName;
                        myNode.ImageKey = "Table.png";
                        myNode.SelectedImageKey = myNode.ImageKey;
                        myNode.ToolTipText = "Doppio click per selezionare la tabella esterna";
                        _treeTables.Nodes.Add(myNode);
                        
                        foreach (DataColumn dummyCol in dummyTable.Columns)
                        {
                            TreeNode myNodeCol = new TreeNode();
                            myNodeCol.Text = dummyCol.ColumnName;
                            myNodeCol.ImageKey = "Field.png";
                            myNodeCol.SelectedImageKey = myNodeCol.ImageKey;
                            myNode.Nodes.Add(myNodeCol);
                            
                        }
                        

                    }
                    
                }
                //myService.logout();
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            //visualizzaDatiTabellaToolStripMenuItem.Visible = true;
            //disegnaFiltriTabellaToolStripMenuItem.Visible = true;
            Point pt = this._treeTables.PointToClient(Control.MousePosition);
            TreeNode nd = this._treeTables.GetNodeAt(pt);
            if (nd.ImageKey != "Table.png")
            {
                e.Cancel = true;
                //visualizzaDatiTabellaToolStripMenuItem.Visible = false;
                //disegnaFiltriTabellaToolStripMenuItem.Visible = false;
            }
            //else
            //{
            //    IsResulttoolStripMenuItem.Visible = false;
            //}
            _treeTables.SelectedNode = nd;

            
            
        }

        private void visualizzaDatiTabellaToolStripMenuItem_Click(object sender, EventArgs e)
        {

            String TableName =_treeTables.SelectedNode.Text ;
            if (lastData != TableName)
            {
                label1.Text = TableName;
                lastData = TableName;
                if (lastFiltri != lastData)
                {
                    _gridFilter.DataSource = null;
                }
                String exitMessage="";
                DataTable retValue;
                DataTable tableFiltri = null;
                try
                {
                    Cursor = Cursors.WaitCursor ;
                    if (TablesDAta.ContainsKey(TableName))
                        retValue = TablesDAta[TableName];
                    else
                    {
                        retValue = myService.GetTabellaEsternaWithData(TableName, ref exitMessage);
                        TablesDAta.Add(TableName, retValue);
                    }
                    if (retValue != null)
                    {
                        label1.Text = TableName;
                        _grid.DataSource = retValue;
                        _grid.Columns[0].Visible = false;
                    }
                    tabControl1.SelectedIndex = 0;
                    

                }
                catch (Exception ex)
                { }
                Cursor = Cursors.Default ;
            }
            else
                tabControl1.SelectedIndex = 0;
        }

        private void IsResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            String TableName = _treeTables.SelectedNode.Text;
            TreeNode parent = _treeTables.SelectedNode.Parent;
            if (parent != null)
            {
                foreach (TreeNode child in parent.Nodes)
                    child.ForeColor = Color.Black;

                _treeTables.SelectedNode.ForeColor = Color.Blue;
                
            }
           
        }
        



        private void disegnaFiltriTabellaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentFilterNode = _treeTables.SelectedNode;
            if(currentFilterNode.Text==currentExtTable.Name)
                disegnaFiltriTabella();
            else
                disegnaFiltriTabellaEmpty();
        }

        private void disegnaFiltriTabella()
        {
            String TableName = _treeTables.SelectedNode.Text;
            currentFilterNode = _treeTables.SelectedNode;
            Int32 selectNodeIndex = -1;
            String exitMessage = "";
            DataTable retValue;
            DataTable tableFiltri = null;
            if (lastFiltri != TableName)
            {
                comboBox1.Items.Clear();
                label1.Text = TableName;
                try
                {
                    lastFiltri = TableName;
                    if (lastData != lastFiltri )
                    {
                        _grid.DataSource = null;
                    }

                    Cursor = Cursors.WaitCursor;
                    tableFiltri = null;
                    foreach (TreeNode myNode in _treeTables.SelectedNode.Nodes)
                    {
                        comboBox1.Items.Add(myNode.Text);
                        
                    }
                    comboBox1.SelectedIndex = -1;
                    if (currentFilterNode.Tag != null)
                    {
                        tableFiltri = (DataTable)(currentFilterNode.Tag);
                        foreach(DataRow myRow in tableFiltri.Rows)
                            if(myRow["Output"].ToString().ToUpper()=="TRUE")
                                comboBox1.SelectedItem=myRow["Colonna"].ToString();
                    }
                    else
                    {
                        tableFiltri = new DataTable(TableName);
                        tableFiltri.Columns.Add("Colonna");
                        tableFiltri.Columns.Add("Output", typeof(Boolean));
                        tableFiltri.Columns.Add("Show", typeof(Boolean));
                        tableFiltri.Columns.Add("Operatore");
                        tableFiltri.Columns.Add("Function-Filter-AND");
                        //tableFiltri.Columns.Add("Function-Filter-OR");
                        tableFiltri.Columns.Add("SelectTOP", typeof(Boolean));

                        foreach (TreeNode myNode in _treeTables.SelectedNode.Nodes)
                        {
                           
                            DataRow newRow = tableFiltri.NewRow();
                            newRow["Colonna"] = myNode.Text;
                            if(currentExtTable.ColumnCode.ToUpper()==(myNode.Text.ToUpper()))
                                newRow["Output"] = true;
                            else
                                newRow["Output"] = false;
                            if (newRow["Output"].ToString().ToUpper() == "TRUE")
                                comboBox1.SelectedItem = newRow["Colonna"].ToString();
                            if (currentExtTable.ColumnsToShow .ToUpper().Contains(myNode.Text))
                                newRow["Show"] = true;
                            else
                                newRow["Show"] = false;

                            Boolean found = false;
                            if (currentExtTable.listFilter != null)
                            {
                                foreach (String myFiter in currentExtTable.listFilter)
                                {
                                    if (myFiter.StartsWith("(" + myNode.Text))
                                    {
                                        found = true;
                                        String tmpValue = myFiter;
                                        tmpValue = tmpValue.Replace("(", "");
                                        tmpValue = tmpValue.Replace(")", "");
                                        tmpValue = tmpValue.Substring(tmpValue.IndexOf(myNode.Text) + myNode.Text.Length);
                                       // tmpValue = tmpValue.Replace(myNode.Text, "");
                                       
                                        tmpValue = tmpValue.Trim();
                                        newRow["Operatore"]= tmpValue.Split(" ".ToCharArray())[0];
                                        tmpValue = tmpValue.Replace(newRow["Operatore"].ToString(), "").Trim();
                                        if (newRow["Operatore"].ToString().ToUpper() == "LIKE")
                                        {
                                            tmpValue = tmpValue.Substring(1);
                                            tmpValue = tmpValue.Substring(0,tmpValue.Length-1);
                                            newRow["Function-Filter-AND"] = tmpValue;
                                        }
                                        else
                                            newRow["Function-Filter-AND"] = tmpValue;
                                        
                                        break;
                                    }
                                }
                            }
                            newRow["SelectTOP"] = true;
                            if (currentExtTable.ShowRowNumber != "")
                            {
                                if (currentExtTable.ShowRowNumber == "ALL")
                                    newRow["SelectTOP"] = false;
                            }
                            if (!found)
                            {
                                newRow["Operatore"] = "";
                                newRow["Function-Filter-AND"] = "";
                                // newRow["Function-Filter-OR"] = "";
                               
                                
                            }
                            tableFiltri.Rows.Add(newRow);
                        }
                        currentFilterNode.Tag = tableFiltri;
                    }
                    _gridFilter.DataSource = tableFiltri;
                    _gridFilter.Columns["Function-Filter-AND"].Width = 400;
                   // _gridFilter.Columns["Function-Filter-OR"].Width = 200;
                    _gridFilter.Columns["SelectTOP"].Visible = false;
                    //DataSet myData = new DataSet("test");
                    //tableFiltri.TableName = "Filtri";
                    //myData.Tables.Add(tableFiltri);
                    //myData.WriteXml(@"c:\temp\test.xml");
                    FixGridColumns();
                    
                    tabControl1.SelectedIndex = 1;
                    
                }
                catch (Exception ex)
                { }
                Cursor = Cursors.Default;
                if (tableFiltri != null)
                {
                    if (tableFiltri.Rows.Count > 0)
                    {
                        DataRow firstRow= tableFiltri.Rows[0];
                        Boolean isTop1= Boolean.Parse(firstRow["SelectTOP"].ToString());
                        if (isTop1 == true)
                        {
                            radioButton1.Checked = true;
                            radioButton2.Checked = false;
                        }
                        else
                        {
                            radioButton1.Checked = false;
                            radioButton2.Checked = true;
                        }
                    }

                }
            }
               
            else
            tabControl1.SelectedIndex = 1;
        }


        private void disegnaFiltriTabellaEmpty()
        {
            String TableName = _treeTables.SelectedNode.Text;
            currentFilterNode = _treeTables.SelectedNode;

            String exitMessage = "";
            DataTable retValue;
            DataTable tableFiltri = null;
            if (lastFiltri != TableName)
            {
                label1.Text = TableName;
                try
                {
                    comboBox1.Items.Clear();
                    lastFiltri = TableName;
                    if (lastData != lastFiltri)
                    {
                        _grid.DataSource = null;
                    }

                    Cursor = Cursors.WaitCursor;
                    tableFiltri = null;
                    foreach (TreeNode myNode in _treeTables.SelectedNode.Nodes)
                    {
                        comboBox1.Items.Add(myNode.Text);

                    }
                    comboBox1.SelectedIndex = -1;

                    if (currentFilterNode.Tag != null)
                    {
                        tableFiltri = (DataTable)(currentFilterNode.Tag);
                    
                    }
                    else
                    {
                        tableFiltri = new DataTable(TableName);
                        tableFiltri.Columns.Add("Colonna");
                        tableFiltri.Columns.Add("Output", typeof(Boolean));
                        tableFiltri.Columns.Add("Show", typeof(Boolean));
                        tableFiltri.Columns.Add("Operatore");
                        tableFiltri.Columns.Add("Function-Filter-AND");
                        //tableFiltri.Columns.Add("Function-Filter-OR");
                        tableFiltri.Columns.Add("SelectTOP", typeof(Boolean));
                        

                        foreach (TreeNode myNode in _treeTables.SelectedNode.Nodes)
                        {
                          
                            DataRow newRow = tableFiltri.NewRow();
                            newRow["Colonna"] = myNode.Text;
                            newRow["Output"] = false;
                             newRow["SelectTOP"] = true;
                            newRow["Operatore"] = "";
                            newRow["Function-Filter-AND"] = "";
                                // newRow["Function-Filter-OR"] = "";


                            
                            tableFiltri.Rows.Add(newRow);
                        }
                        
                        currentFilterNode.Tag = tableFiltri;
                    }
                    _gridFilter.DataSource = tableFiltri;
                    _gridFilter.Columns["Function-Filter-AND"].Width = 400;
                    // _gridFilter.Columns["Function-Filter-OR"].Width = 200;
                    _gridFilter.Columns["SelectTOP"].Visible = false;
                    FixGridColumns();

                    tabControl1.SelectedIndex = 1;
                }
                catch (Exception ex)
                { }
                Cursor = Cursors.Default;
                if (tableFiltri != null)
                {
                    if (tableFiltri.Rows.Count > 0)
                    {
                        DataRow firstRow = tableFiltri.Rows[0];
                        Boolean isTop1 = Boolean.Parse(firstRow["SelectTOP"].ToString());
                        if (isTop1 == true)
                        {
                            radioButton1.Checked = true;
                            radioButton2.Checked = false;
                        }
                        else
                        {
                            radioButton1.Checked = false;
                            radioButton2.Checked = true;
                        }
                    }

                }
            }

            else
                tabControl1.SelectedIndex = 1;
        }



        private void FixGridColumns()
        {
            int i = 0;
            for (i = 0; i <= this._gridFilter.Columns.Count - 1; i++)
            {
                DataGridViewColumn col = this._gridFilter.Columns[i];
                if (col.Name == "Operatore")
                {
                    DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                    cmb.ValueType = col.ValueType;
                    cmb.Name = col.Name;
                    cmb.DataPropertyName = col.DataPropertyName;
                    cmb.HeaderText = col.HeaderText;
                    cmb.DisplayStyleForCurrentCellOnly = true;
                    cmb.Items.Add(" ");
                    cmb.Items.Add("=");
                    cmb.Items.Add("<>");
                    cmb.Items.Add(">");
                    cmb.Items.Add(">=");
                    cmb.Items.Add("<");
                    cmb.Items.Add("<=");
                    cmb.Items.Add("LIKE");
                    this._gridFilter.Columns.RemoveAt(i);
                    this._gridFilter.Columns.Insert(i, cmb);
                }
                else if (col.Name.Contains("Output"))
                {
                    col.Visible = false;
                }
                else if (col.Name.Contains("Function"))
                {
                    DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                    cmb.ValueType = col.ValueType;
                    cmb.Name = col.Name;
                    cmb.DataPropertyName = col.DataPropertyName;
                    cmb.HeaderText = col.HeaderText;
                    cmb.DisplayStyleForCurrentCellOnly = true;
                    cmb.Width = 300;
                    cmb.Items.Add(" ");
                    foreach (String function in myListItems)
                    {
                        cmb.Items.Add(function.Trim());
                    }

                    this._gridFilter.Columns.RemoveAt(i);
                    this._gridFilter.Columns.Insert(i, cmb);
                }
                if (col.Name.StartsWith("Function"))
                    col.HeaderText = "Filtro";
                else
                    col.HeaderText = col.Name.Replace("-", " ");
               
            }
        }



        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

            DataTable myFilter =(DataTable)currentFilterNode.Tag;
            if (myFilter != null)
            {
                foreach (DataRow myRow in myFilter.Rows)
                {
                    myRow["SelectTOP"] = true;
                    myRow.AcceptChanges();
                }
            }
            currentFilterNode.Tag = myFilter;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            DataTable myFilter = (DataTable)currentFilterNode.Tag;
            if (myFilter != null)
            {
                foreach (DataRow myRow in myFilter.Rows)
                {
                    myRow["SelectTOP"] = false;
                    myRow.AcceptChanges();
                }
            }
            currentFilterNode.Tag = myFilter;
        }

        private void _btnOK_Click(object sender, EventArgs e)
        {
            String query = "";
            String selectNames = "";
            String selectWhere = "";
            String selectTop = "";
            String tmpValueAND = "";
           // String tmpValueOR = "";
            if (currentFilterNode != null)
            {

                DataTable myFilter = (DataTable)currentFilterNode.Tag;
                //////System.IO.StringWriter writer = new System.IO.StringWriter();
                //////myFilter.WriteXml(writer, XmlWriteMode.IgnoreSchema , true);
                //////selectedFilter = writer.ToString();
                //////this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //////this.Close();
                if (myFilter != null)
                {

                    //tableFiltri.Columns.Add("Colonna");
                    //tableFiltri.Columns.Add("Output", typeof(Boolean));
                    //tableFiltri.Columns.Add("Operatore");
                    //tableFiltri.Columns.Add("Function-Filter-AND");
                    //tableFiltri.Columns.Add("Function-Filter-OR");
                    //tableFiltri.Columns.Add("SelectTOP", typeof(Boolean));

                    foreach (DataRow myRow in myFilter.Rows)
                    {
                        if (selectTop == "")
                        {
                            selectTop = myRow["SelectTOP"].ToString();
                            if (selectTop.ToUpper() == "TRUE")
                                selectTop = "1";
                            else
                                selectTop = "ALL";
                        }
                        if (myRow["Output"].ToString().ToUpper() == "TRUE")
                            if (selectNames == "")
                                selectNames = myRow["Colonna"].ToString();
                            else
                                selectNames = selectNames + "," + myRow["Colonna"].ToString();


                        if (myRow["Operatore"].ToString() != "")
                        {


                            if (myRow["Function-Filter-AND"].ToString() != "")
                            {
                                String tmpValue = string.Format("({0} {1} {2})", myRow["Colonna"].ToString(), myRow["Operatore"].ToString(), myRow["Function-Filter-AND"].ToString());
                                if(myRow["Operatore"].ToString().ToUpper()=="LIKE")
                                    tmpValue = string.Format("({0} {1} '{2}')", myRow["Colonna"].ToString(), myRow["Operatore"].ToString(), myRow["Function-Filter-AND"].ToString());
                                else
                                    tmpValue = string.Format("({0} {1} {2})", myRow["Colonna"].ToString(), myRow["Operatore"].ToString(), myRow["Function-Filter-AND"].ToString());
                                    
                                if (tmpValueAND == "")
                                    tmpValueAND = tmpValue;
                                else
                                    tmpValueAND = tmpValueAND + ";" + tmpValue;

                            }

                               

                        }

                        

                    }
                    selectWhere = tmpValueAND;
                }
            }
            if (selectNames != "" && selectTop != "")
            {
                currentExtTable.Name = currentFilterNode.Text;
                currentExtTable.ColumnCode = selectNames;
                currentExtTable.ShowRowNumber = selectTop;
                currentExtTable.listFilter = new List<string>();
                foreach(String myFilter in selectWhere.Split(";".ToCharArray(),StringSplitOptions.RemoveEmptyEntries))
                {
                     currentExtTable.listFilter.Add(myFilter);
                }
                selectedFilter = selectTop + ";" + selectNames + ";" + selectWhere;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                if(selectNames=="")
                {
                    if (MessageBox.Show("Non è stato selezionato nessun output. Uscire comunque?", "CD Configurator", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.DialogResult = System.Windows.Forms.DialogResult.Ignore;
                        this.Close();

                    }
                    
                }
               
               
            }


        }

        private void _btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String query = "";
            String selectNames = "";
            String selectWhere = "";
            String selectTop = "";
            String tmpValueAND = "";
            // String tmpValueOR = "";
            if (currentFilterNode != null)
            {
                if (comboBox1.SelectedIndex == -1)
                {
                    MessageBox.Show("Select an output field", "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
                DataTable myFilter = (DataTable)currentFilterNode.Tag;
                //////System.IO.StringWriter writer = new System.IO.StringWriter();
                //////myFilter.WriteXml(writer, XmlWriteMode.IgnoreSchema , true);
                //////selectedFilter = writer.ToString();
                //////this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //////this.Close();
                if (myFilter != null)
                {

                    //tableFiltri.Columns.Add("Colonna");
                    //tableFiltri.Columns.Add("Output", typeof(Boolean));
                    //tableFiltri.Columns.Add("Operatore");
                    //tableFiltri.Columns.Add("Function-Filter-AND");
                    //tableFiltri.Columns.Add("Function-Filter-OR");
                    //tableFiltri.Columns.Add("SelectTOP", typeof(Boolean));

                    foreach (DataRow myRow in myFilter.Rows)
                    {
                        if (comboBox1.SelectedItem.ToString().ToUpper() == myRow["Colonna"].ToString().ToUpper())
                            myRow["Output"] = true;
                        else
                            myRow["Output"] = false;
                        if (selectTop == "")
                        {
                            selectTop = myRow["SelectTOP"].ToString();
                            if (selectTop.ToUpper() == "TRUE")
                                selectTop = "1";
                            else
                                selectTop = "ALL";
                        }
                        if (myRow["Show"].ToString().ToUpper() == "TRUE")
                            if (selectNames == "")
                                selectNames = myRow["Colonna"].ToString();
                            else
                                selectNames = selectNames + "," + myRow["Colonna"].ToString();


                        if (myRow["Operatore"].ToString() != "")
                        {


                            if (myRow["Function-Filter-AND"].ToString() != "")
                            {
                                String tmpValue = string.Format("({0} {1} {2})", myRow["Colonna"].ToString(), myRow["Operatore"].ToString(), myRow["Function-Filter-AND"].ToString());
                                if (myRow["Operatore"].ToString().ToUpper() == "LIKE")
                                    tmpValue = string.Format("({0} {1} '{2}')", myRow["Colonna"].ToString(), myRow["Operatore"].ToString(), myRow["Function-Filter-AND"].ToString());
                                else
                                    tmpValue = string.Format("({0} {1} {2})", myRow["Colonna"].ToString(), myRow["Operatore"].ToString(), myRow["Function-Filter-AND"].ToString());

                                if (tmpValueAND == "")
                                    tmpValueAND = tmpValue;
                                else
                                    tmpValueAND = tmpValueAND + ";" + tmpValue;

                            }



                        }



                    }
                    selectWhere = tmpValueAND;
                }
            }
            if (selectNames != "" && selectTop != "" && comboBox1.SelectedIndex!=-1)
            {
                
                currentExtTable.Name = currentFilterNode.Text;
                currentExtTable.ColumnCode = comboBox1.SelectedItem.ToString() ;
                currentExtTable.ColumnsToShow = selectNames;
               

                currentExtTable.ShowRowNumber = selectTop;
                currentExtTable.listFilter = new List<string>();
                foreach (String myFilter in selectWhere.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    currentExtTable.listFilter.Add(myFilter);
                }
               
                selectedFilter = selectTop + ";" + selectNames + ";" + selectWhere;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                if (selectNames == "")
                {
                    if (MessageBox.Show("Non è stato selezionato nessuna colonna da visualizzare all'utente. Uscire comunque e usare output come colonna di visualizzazione?", "CD Configurator", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        selectNames = comboBox1.Text;
                        this.DialogResult = System.Windows.Forms.DialogResult.Ignore;
                        this.Close();

                    }

                }


            }
        }

        private void _gridFilter_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            // If a check box cell is clicked, this event handler sets the value
            // of a few other checkboxes in the same row as the clicked cell.
            if (e.RowIndex < 0) return; // row is sometimes negative?
            int ix = e.ColumnIndex;
            if (ix  == _gridFilter.Columns["Output"].Index )
            {
                var row = _gridFilter.Rows[e.RowIndex];

                DataGridViewCheckBoxCell checkCell =
                    (DataGridViewCheckBoxCell)row.Cells[ix];

                bool isChecked = (Boolean)checkCell.Value;
                if (isChecked)
                {
                    // Only turn off other checkboxes if this one is ON. 
                    // It's ok for all of them to be OFF simultaneously.
                    foreach (DataGridViewRow myRow in _gridFilter.Rows)
                    {
                        if (row.Index != myRow.Index)
                            ((DataGridViewCheckBoxCell)row.Cells[ix]).Value = false;
                    }
                    //for (int i = 1; i <= 3; i++)
                    //{
                    //    if (i != ix)
                    //    {
                    //        ((DataGridViewCheckBoxCell)row.Cells[i]).Value = false;
                    //    }
                    //}
                }
                _gridFilter.Invalidate();
            }

        }

        //private void _treeTables_DoubleClick(object sender, EventArgs e)
        //{
        //    String TableName = _treeTables.SelectedNode.Text;
        //    String exitMessage = "";
        //    DataTable retValue;

        //    if (TablesDAta.ContainsKey(TableName))
        //        retValue = TablesDAta[TableName];
        //    else
        //    {
        //        retValue = myService.GetTabellaEsternaWithData(TableName, ref exitMessage);
        //        TablesDAta.Add(TableName, retValue);
        //    }
        //    if (retValue != null)
        //    {
        //        label1.Text = TableName;
        //        _grid.DataSource = retValue;
        //        _grid.Columns[0].Visible = false;
        //    }
        //}
    }
}
