﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Clifton.Windows.Forms;

namespace XTreeDemo
{

    public partial class frmRefOption : Form
    {
        RadioCheckedListBox newListBox;
        XTree currentTree;
        List<String> myListItems;
        private void GetQAList(TreeNode givenNode)
        {
            if(myListItems==null)
                myListItems=new List<String>();
            if(givenNode==null)
                givenNode=currentTree.Nodes[0];

            if (givenNode.Name.Contains("Option"))
                myListItems.Add(givenNode.Text);
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetQAList(node);
        }
        private void GetInputList(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Input"))
                myListItems.Add(givenNode.Text);
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetInputList(node);
        }


        private void GetCPList(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Feature"))
            {
                FeatureDef myF = ((FeatureController)((Clifton.Windows.Forms.XmlTree.NodeInstance)givenNode.Tag).Instance).FeatureDef;
                myListItems.Add(givenNode.Text + " (" +  myF.Code  + ")" );
            }
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetCPList(node);
        }

        private void GetEXTTableList(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("ExternalTable"))
                myListItems.Add(givenNode.Text);
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetEXTTableList(node);
        }

        private void GetFunList(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Function"))
                myListItems.Add(givenNode.Text);
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetFunList(node);
        }

        public frmRefOption(XTree givenTree, string selectionType, string filterExclude)
        {
            InitializeComponent();
            currentTree = givenTree;
            if(currentTree!=null)
            {
                if(currentTree.Nodes.Count>0)
                {
                    filterExclude = filterExclude.Split(".".ToCharArray())[0];
                    newListBox = new RadioCheckedListBox();
                    panel1.Controls.Add(newListBox);
                    newListBox.Dock = DockStyle.Fill;
                    if (selectionType == "OPTION")
                        GetQAList(null);
                    else if (selectionType == "INPUT")
                        GetInputList(null);
                    else if (selectionType == "EXTERNAL_TABLES")
                        GetEXTTableList(null);
                    else if (selectionType == "FEATURES")
                        GetCPList(null);

                    foreach (string item in myListItems)
                    {
                        if(!item.StartsWith(filterExclude))
                            newListBox.Items.Add(item);
                    }
                    groupBox1.Text = "Select an Option";
                    
                }
            }
            if(newListBox!=null)
                if (newListBox.Items.Count == 0)
                {
                    MessageBox.Show("There aren't option to refer!");
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                    this.Close();
                }
        }
        public frmRefOption(XTree givenTree)
        {
            InitializeComponent();
            currentTree = givenTree;
            newListBox = new RadioCheckedListBox();
            panel1.Controls.Add(newListBox);
            newListBox.Dock = DockStyle.Fill;
            GetFunList(null);
            foreach (string item in myListItems)
            {
               newListBox.Items.Add(item);
            }
            groupBox1.Text = "Select a function";
           

            if (newListBox != null)
                if (newListBox.Items.Count == 0)
                {
                    MessageBox.Show("There aren't Function to refer!");
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                    this.Close();
                }
        }

        public String SelectedObject()
        {
            if (newListBox != null)
                return newListBox.SelectedItem.ToString();
            else
                return "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (newListBox != null)
                if (newListBox.SelectedItem == null)
                    MessageBox.Show("Please select an option");
                else
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            newListBox.SelectedIndex = -1;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            newListBox.Items.Clear();
            foreach (string item in myListItems)
            {
                if(txtFilter.Text=="" || txtFilter.Text=="*")
                    newListBox.Items.Add(item);
                else if (item.ToLower().Contains(txtFilter.Text))
                    newListBox.Items.Add(item);
            }
        }


    }


}
