using System;
using System.Collections.Generic;
using System.Xml;

using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public interface IRootController
	{
		void Serialize(XmlNode root);
		DialogDef SchemaDef { get;}
	}

	public interface IController
	{
	}
}
