﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Clifton.Windows.Forms;
using XTreeDemo;
using System.Collections;

namespace XTreeDemo
{


    public partial class frmCPtoBOM : Form
    {
        private String famiglia;
        private String prodotto;
        private List<String> myListItems;
        private List<String> insert;
        XTree currentTree;
        DataSet ds = new DataSet();
        public String fileConfiguration = "";

        public frmCPtoBOM(XTree givenTree, String givenFamiglia, String givenProdotto, String givenCADConfig)
        {
            Cursor = Cursors.WaitCursor;
            String inputKey = "";
            try
            {
                InitializeComponent();
                famiglia = givenFamiglia;
                prodotto = givenProdotto;
                myListItems = new List<string>();
                currentTree = givenTree;
                inputKey = "";
                FillFeatures(null, ref inputKey);
                treeCP.Nodes[0].Expand();
                inputKey = "";
                FillInput(null, ref inputKey);
                treeCP.Nodes[1].Expand();
                inputKey = "";
                FillTable(null, ref inputKey);
                treeCP.Nodes[2].Expand();
                inputKey = "";
                FillFunction(null, ref inputKey);
                treeCP.Nodes[3].Expand();
                FillBOM();
                fileConfiguration = givenCADConfig;
                if (System.IO.File.Exists(fileConfiguration))
                {
                    LoadFeaturesToCAD();
                }
            }
            catch (Exception e) { }
            Cursor = Cursors.Default;


        }

        private void LoadFeaturesToCAD()
        {
           
            System.IO.StreamReader myRead = new System.IO.StreamReader(fileConfiguration);
            try
            {
                while (!myRead.EndOfStream)
                {
                    String line = myRead.ReadLine();
                    line = line.Replace("::=", "|");

                    String cp = line.Split("|".ToCharArray())[1];
                    String cad = line.Split("|".ToCharArray())[0];

                    AssignCPtoCAD(cp, cad);
                }
            }
            catch (Exception e) { };
            myRead.Close();

        }
        private void AssignCPtoCAD(String givenCPCode, String givenCadCode)
        {
            String givenType= givenCPCode.Substring(0,1);
            String match= givenCPCode.Substring(2);

            TreeNode cp = FinCPdNode(treeCP, null, match, givenType);
            if (cp != null)
            {
                TreeNode cad = FinCADNode(treeCAD, null, givenCadCode);
                if (cad != null)
                {
                    TreeNode addNode = new TreeNode();
                    addNode.Text = cp.Text;
                    addNode.Tag = cp.Tag;
                    cad.Nodes.Insert(0, addNode);
                    cad.Expand();
                    addNode.ImageIndex = cp.ImageIndex;
                    addNode.SelectedImageIndex = cp.ImageIndex;
                }

            }
        }


        private TreeNode FinCPdNode(TreeView tvSelection, TreeNode givenNode, string matchText, string givenType)
        {
            TreeNodeCollection myNodes = null;
            if (tvSelection != null)
                myNodes = tvSelection.Nodes;
            else if(givenNode!=null)
                myNodes = givenNode.Nodes;
            if (myNodes != null)
            {
                foreach (TreeNode node in myNodes)
                {
                    Boolean verifyCode = true;
                    if (givenType == "F" && node.ImageIndex != 2)
                        verifyCode=false;
                    if (givenType == "I" && node.ImageIndex != 3)
                        verifyCode=false;
                    if (givenType == "T" && node.ImageIndex != 4)
                        verifyCode = false;
                    if (verifyCode && node.Tag != null)
                    {
                        if (node.Tag.ToString() == matchText)
                            return node;

                    }
                    
                    if(node.Nodes!=null)
                    {
                        TreeNode nodeChild = FinCPdNode(null, node, matchText, givenType);
                        if (nodeChild != null) return nodeChild;
                    }
                    
                }
                
            }
            return (TreeNode)null;
        }

        private TreeNode FinCADNode(TreeView tvSelection, TreeNode givenNode, string matchText)
        {
            TreeNodeCollection myNodes = null;
            if (tvSelection != null)
                myNodes = tvSelection.Nodes;
            else if (givenNode != null)
                myNodes = givenNode.Nodes;
            if (myNodes != null)
            {
                foreach (TreeNode node in myNodes)
                {
                    if (node.Tag != null)
                    {
                        if (node.Name + "->" + node.Tag.ToString() == matchText)
                            return node;
                        
                    }
                    if(node.Nodes!=null)
                    {
                        TreeNode nodeChild = FinCADNode(null, node, matchText);
                        if (nodeChild != null) return nodeChild;
                    }
                }

            }
            return (TreeNode)null;
        }


        


        private void FillFeatures(TreeNode givenNode, ref String insertKey)
        {

            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Feature"))
            {
                FeatureDef myF = ((FeatureController)((Clifton.Windows.Forms.XmlTree.NodeInstance)givenNode.Tag).Instance).FeatureDef;
                TreeNode myNode = new TreeNode();
                if (!insertKey.Contains(myF.Code))
                {
                    myNode.Text = "(" + myF.Code + ") " + givenNode.Text.Split("-".ToCharArray())[1].Trim();
                    myNode.Tag = myF.Code;
                    treeCP.Nodes[0].Nodes.Add(myNode);
                    myNode.ImageIndex = 2;
                    myNode.SelectedImageIndex = 2;
                    insertKey = insertKey + myF.Code;
                }

            }
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        FillFeatures(node, ref insertKey);


        }


        private void FillInput(TreeNode givenNode, ref String insertKey)
        {

            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Input"))
            {
                
                InputDef myF = ((InputController)((Clifton.Windows.Forms.XmlTree.NodeInstance)givenNode.Tag).Instance).InputDef;
                TreeNode myNode = new TreeNode();
                if (!insertKey.Contains(myF.Code))
                {
                    myNode.Text = "(" + myF.Code + ") " + givenNode.Text.Split("-".ToCharArray())[2].Trim();
                    myNode.Tag = myF.Code;
                    treeCP.Nodes[1].Nodes.Add(myNode);
                    myNode.ImageIndex = 3;
                    myNode.SelectedImageIndex = 3;
                    insertKey = insertKey + myF.Code;
                }


            }
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        FillInput(node, ref insertKey);
        }

        private void FillTable(TreeNode givenNode, ref String insertKey)
        {

            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("ExternalTable"))
            {
                ExternalTableDef myF = ((ExternalTableController)((Clifton.Windows.Forms.XmlTree.NodeInstance)givenNode.Tag).Instance).ExternalTableDef;
                TreeNode myNode = new TreeNode();
                if (!insertKey.Contains(myF.Name))
                {
                    myNode.Text = myF.Name;
                    myNode.Tag = myF.Name;
                    treeCP.Nodes[2].Nodes.Add(myNode);
                    myNode.ImageIndex = 4;
                    myNode.SelectedImageIndex = 4;
                }

            }
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        FillTable(node, ref insertKey);
        }


        private void FillFunction(TreeNode givenNode, ref String insertKey)
        {

            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Function"))
            {
                FunctionDef myF = ((FunctionController)((Clifton.Windows.Forms.XmlTree.NodeInstance)givenNode.Tag).Instance).FunctionDef ;
                TreeNode myNode = new TreeNode();
                if (!insertKey.Contains(myF.Name))
                {
                    myNode.Text = myF.Name;
                    myNode.Tag = myF.Name;
                    treeCP.Nodes[3].Nodes.Add(myNode);
                    myNode.ImageIndex = 5;
                    myNode.SelectedImageIndex = 5;
                }

            }
            if (givenNode.Name == "Dialogo")
            {
                if (givenNode.Nodes != null)
                    if (givenNode.Nodes.Count > 0)
                        foreach (TreeNode node in givenNode.Nodes)
                            FillFunction(node, ref insertKey);
            }
        }

        private void FillBOM()
        {
            String retMessage = "";
            clsCDInterface myService;
            DataTable myTableBOM;
            myService = new clsCDInterface();
            myTableBOM = myService.GetDistintaProdotto(famiglia,prodotto);
            if (myTableBOM != null)
            {
                //myTableBOM.WriteXml("c:\\temp\\test.xml");
                //System.IO.StreamWriter d = new System.IO.StreamWriter("c:\\temp\\io.txt");
                //System.IO.StreamWriter d2 = new System.IO.StreamWriter("c:\\temp\\io2.txt");
                //foreach (DataRow i in myTableBOM.Rows)
                //{
                //    d.WriteLine(i["eParentId"] + " - " + i["eId"]);
                //    d2.WriteLine(i["eFileNameParent"] + " - " + i["eFileName"]);
                //}
                //d.Close();
                //d2.Close();
                ds = new DataSet();
                ds.Tables.Add(myTableBOM);

                //add a relationship

               // ds.Relations.Add("rsParentChild", ds.Tables["BOM"].Columns["eId"], ds.Tables["BOM"].Columns["eParentId"]);
                insert = new List<string>();

                foreach (DataRow dr in ds.Tables["BOM"].Rows)
                {

                    if (dr["eParentId"] == DBNull.Value)
                    {

                        TreeNode root = new TreeNode(dr["eCADName"].ToString() + " (" + dr["eFileName"].ToString() + ")");
                        root.Tag = dr["eCADName"].ToString();
                        root.Name = dr["Ref"].ToString();
                        root.ImageIndex = 0;
                        root.SelectedImageIndex = 0;
                        treeCAD.Nodes.Add(root);

                        PopulateTree(dr, ref root);

                    }

                }

                treeCAD.ExpandAll();


            }


        }

        public void PopulateTree(DataRow dr, ref TreeNode pNode)
        {

            DataRow[] myChild = ds.Tables["BOM"].Select("eParentId='" + dr["eId"].ToString() + "'");
            foreach (DataRow row in myChild)
            {
                String f = row["eParentId"] + "-" + row["eId"];
                if (!insert.Contains(f))
                {
                    insert.Add(f);
                    TreeNode cChild = new TreeNode(row["eCADName"].ToString() + " (" + row["eFileName"].ToString() + ")");
                    cChild.Tag = row["eCADName"].ToString();
                    cChild.ImageIndex = 1;
                    cChild.Name = row["Ref"].ToString();
                    cChild.SelectedImageIndex = 1;
                    pNode.ImageIndex = 0;
                    pNode.SelectedImageIndex = 0;
                    pNode.Nodes.Add(cChild);

                    //Recursively build the tree

                    PopulateTree(row, ref cChild);
                }
               

            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            ArrayList myListSelect = treeCP.SelectedNodes;
            ArrayList myListSelectCAD = treeCAD.SelectedNodes;
            if (myListSelect != null && myListSelectCAD != null)
            {
                foreach (TreeNode myNode in myListSelect)
                {
                    if (myNode.Parent == null)
                        continue;
                    foreach (TreeNode myNodeCAD in myListSelectCAD)
                    {
                        if (myNodeCAD.ImageIndex == 0 || myNodeCAD.ImageIndex == 1)
                        {
                            Boolean canAdd = true;
                            Int32 lastIndex = 0;
                            foreach (TreeNode sub in myNodeCAD.Nodes)
                            {
                                if (sub.ImageIndex > 1)
                                {
                                    lastIndex += 1;
                                    if (sub.Text == myNode.Text)
                                        canAdd = false;
                                }
                            }
                            if (canAdd)
                            {
                                TreeNode addNode = new TreeNode();
                                addNode.Text = myNode.Text;
                                addNode.Tag = myNode.Tag;
                                myNodeCAD.Nodes.Insert(lastIndex, addNode);
                                myNodeCAD.Expand();
                                addNode.ImageIndex = myNode.ImageIndex;
                                addNode.SelectedImageIndex = myNode.ImageIndex;

                            }
                        }
                    }
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {

            Dictionary<TreeNode, List<TreeNode>> myListDelete = new Dictionary<TreeNode, List<TreeNode>>();

            ArrayList myListSelectCAD = treeCAD.SelectedNodes;
            if (myListSelectCAD != null)
            {
                foreach (TreeNode myNodeCAD in myListSelectCAD)
                {
                    List<TreeNode> myIds = new List<TreeNode>();
                    TreeNode myParent = myNodeCAD.Parent;
                    if (myListDelete.ContainsKey(myParent))
                        myIds = myListDelete[myParent];
                    else
                        myListDelete.Add(myParent, null);

                    if (!myIds.Contains(myNodeCAD))
                        myIds.Add(myNodeCAD);

                    myListDelete[myParent] = myIds;
                }

            }

            foreach (TreeNode myNodeParent in myListDelete.Keys)
            {
                List<TreeNode> myIds = myListDelete[myNodeParent];
                foreach (TreeNode sumNode in myIds)
                    myNodeParent.Nodes.Remove(sumNode);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            String path = AppDomain.CurrentDomain.BaseDirectory + "EXPORT";
            String fullFilePath = path + "\\" + "FeaturesToCAD.config";
            if (System.IO.Directory.Exists(path))
            {
                try
                {
                    if (System.IO.File.Exists(fullFilePath))
                    {
                        try
                        {
                            System.IO.File.Delete(fullFilePath);
                        }
                        catch (Exception e1) { }
                    }

                }
                catch (Exception e2) { }
            }
            else
                System.IO.Directory.CreateDirectory(path);


            try
            {
                Dictionary<string,string> myDict= new Dictionary<string,string>();
            
                System.IO.StreamWriter myW = new System.IO.StreamWriter(fullFilePath);
                foreach(TreeNode rootNode in treeCAD.Nodes)
                    WriteCPS(rootNode, ref myW);
            
                myW.Close();
                fileConfiguration = fullFilePath;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().Name);
            }

            

           
        }


        private void WriteCPS(TreeNode givenNode, ref System.IO.StreamWriter givenList)
        {
            if (givenNode.Nodes != null)
            {
                foreach (TreeNode subNode in givenNode.Nodes)
                {
                    if (subNode.ImageIndex >=2)
                    {
                        if(subNode.ImageIndex==2)
                            givenList.WriteLine(givenNode.Name.ToUpper() + "->" + givenNode.Text.Split("(".ToCharArray())[0].Trim() + "::=F_" + subNode.Tag.ToString());
                        else if (subNode.ImageIndex == 3)
                            givenList.WriteLine(givenNode.Name.ToUpper() + "->" + givenNode.Text.Split("(".ToCharArray())[0].Trim() + "::=I_" + subNode.Tag.ToString());
                        else if (subNode.ImageIndex == 4)
                            givenList.WriteLine(givenNode.Name.ToUpper() + "->" + givenNode.Text.Split("(".ToCharArray())[0].Trim() + "::=T_" + subNode.Tag.ToString());
                        else if (subNode.ImageIndex == 5)
                            givenList.WriteLine(givenNode.Name.ToUpper() + "->" + givenNode.Text.Split("(".ToCharArray())[0].Trim() + "::=E_" + subNode.Tag.ToString());
                        
                    }
                    else
                        WriteCPS(subNode, ref givenList);
                }
            }
        }

        private void frmCPtoBOM_Load(object sender, EventArgs e)
        {
            if (treeCP != null)
                treeCP.ExpandAll();
            if (treeCAD != null)
                treeCAD.ExpandAll();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }


}
