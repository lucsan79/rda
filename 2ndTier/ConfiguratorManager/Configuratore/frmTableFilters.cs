using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Data.Common;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Timers;
using Clifton.Windows.Forms;

namespace XTreeDemo
{
   

    public partial class frmTableFilters : Form
    {
        private DataTable filterTable;
        private Int32 currentId=-1;
        XTree currentTree;
        List<String> myListItems;
        List<String> myListRow;

        public frmTableFilters(XTree givenTree, List<String> myListRow)
        {
            InitializeComponent();
            currentTree = givenTree;
            filterTable = new DataTable();
            filterTable.Columns.Add("ColumnToFilter");
            filterTable.Columns.Add("DataFilterType");
            filterTable.Columns.Add("DataFilterValue");
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            if (myListRow != null)
            {
                foreach (string row in myListRow)
                {

                    String[] myVal = row.Split("|".ToCharArray());
                    if (myVal.Length == 3)
                    {
                        Int32 currentId = -1;
                        currentId = userDataGridView.Rows.Add();
                        userDataGridView.Rows[currentId].Cells[0].Value = myVal[0];
                        userDataGridView.Rows[currentId].Cells[1].Value = myVal[1];
                        userDataGridView.Rows[currentId].Cells[2].Value = myVal[2];
                        userDataGridView.Rows[currentId].Selected = false;
                    }
                }
                
            }
        
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtColName.Text.Trim()=="")
                {
                    MessageBox.Show("Insert Column Name to Filter", "Dialog Admin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (cmbFType.SelectedIndex == -1)
                {
                    MessageBox.Show("Select Filter type", "Dialog Admin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (cmbFValue.SelectedIndex == -1)
                {
                    MessageBox.Show("Select Filter Value", "Dialog Admin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (userDataGridView.Rows != null)
                {
                    Int32 counter=0;
                    foreach (DataGridViewRow myRow in userDataGridView.Rows)
                    {
                        counter = 0;
                        if(myRow.Cells[0].Value.ToString()==txtColName.Text)
                            counter+=1;
                        if (myRow.Cells[1].Value.ToString() == cmbFType.SelectedItem)
                            counter += 1;
                         if (myRow.Cells[2].Value.ToString() == cmbFValue.SelectedItem)
                            counter += 1;
                        if (counter == 3)
                        {
                            MessageBox.Show("Data already added", "Dialog Admin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                       

                    }
                   
                }
                
                Int32 currentId=-1;
                currentId=userDataGridView.Rows.Add();
                userDataGridView.Rows[currentId].Cells[0].Value = txtColName.Text;
                userDataGridView.Rows[currentId].Cells[1].Value =cmbFType.SelectedItem;
                userDataGridView.Rows[currentId].Cells[2].Value = cmbFValue.SelectedItem;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
                userDataGridView.Rows[currentId].Selected = true;

                //DataRow myRow = filterTable.NewRow();
                //myRow[0] = txtColName.Text;
                //myRow[1] = cmbFType.SelectedText;
                //myRow[2] = cmbFValue.SelectedText;
                //filterTable.Rows.Add(myRow);
                //userDataGridView.DataSource = filterTable;
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

       

       

        private void userDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (userDataGridView.SelectedRows.Count > 0)
                {
                    currentId = userDataGridView.SelectedRows[0].Index;
                    txtColName.Text = userDataGridView.SelectedRows[0].Cells[0].Value.ToString();
                    cmbFType.SelectedItem = userDataGridView.SelectedRows[0].Cells[1].Value;
                    cmbFValue.SelectedItem = userDataGridView.SelectedRows[0].Cells[2].Value;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
                else
                    currentId = -1;
            }
            catch (Exception ex1) { };
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtColName.Text = "";
            cmbFType.SelectedIndex = -1;
            cmbFValue.SelectedIndex = -1;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            currentId = -1;
        }

        public String SelectedObject()
        {
            String retValue = "";
            for (Int32 idx = 0; idx < userDataGridView.Rows.Count; idx++)
            {
                DataGridViewRow row = userDataGridView.Rows[idx];

                retValue += row.Cells[0].Value.ToString() + "|" + row.Cells[1].Value.ToString() + "|" + row.Cells[2].Value.ToString() + ";";
            }
            return retValue;
        }
        private void cmbFType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Option
            //Input
            //Feature
            cmbFValue.Items.Clear();
            if(myListItems!=null)
                myListItems.Clear();
            if (cmbFType.SelectedItem == "Option")
                GetQAList(null);
            else if (cmbFType.SelectedItem == "Input")
                GetInputList(null);
            else if (cmbFType.SelectedItem == "Feature")
                GetFeatures(null);

            foreach (string val in myListItems)
                cmbFValue.Items.Add(val);
        }



        private void GetQAList(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Option"))
                myListItems.Add(givenNode.Text);
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetQAList(node);
        }

        private void GetInputList(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Input"))
                myListItems.Add(givenNode.Text);
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetInputList(node);
        }

        private void GetFeatures(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Feature"))
                myListItems.Add(givenNode.Text);
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetFeatures(node);
        }

        private void GetFunList(TreeNode givenNode)
        {
            if (myListItems == null)
                myListItems = new List<String>();
            if (givenNode == null)
                givenNode = currentTree.Nodes[0];

            if (givenNode.Name.Contains("Function"))
                myListItems.Add(givenNode.Text);
            if (givenNode.Nodes != null)
                if (givenNode.Nodes.Count > 0)
                    foreach (TreeNode node in givenNode.Nodes)
                        GetFunList(node);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {


            try
            {
                txtColName.Text = "";
                cmbFType.SelectedIndex = -1;
                cmbFValue.SelectedIndex = -1;
                userDataGridView.Rows.RemoveAt(currentId);
                

            }
            catch (Exception ex1) { };
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (userDataGridView.Rows.Count > 0)
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            else
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Int32 currentId = -1;
            try
            {
                currentId = userDataGridView.SelectedRows[0].Index;

               
                Int32 counter = 0;
                foreach (DataGridViewRow myRow in userDataGridView.Rows)
                {
                    if (myRow.Index != currentId)
                    {
                        counter = 0;
                        if (myRow.Cells[0].Value.ToString() == txtColName.Text)
                            counter += 1;
                        if (myRow.Cells[1].Value.ToString() == cmbFType.SelectedItem)
                            counter += 1;
                        if (myRow.Cells[2].Value.ToString() == cmbFValue.SelectedItem)
                            counter += 1;
                        if (counter == 3)
                        {
                            MessageBox.Show("Data already exists", "Dialog Admin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }

                }

                

                
                userDataGridView.Rows[currentId].Cells[0].Value = txtColName.Text;
                userDataGridView.Rows[currentId].Cells[1].Value = cmbFType.SelectedItem;
                userDataGridView.Rows[currentId].Cells[2].Value = cmbFValue.SelectedItem;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
                userDataGridView.Rows[currentId].Selected = true;
            }
            catch (Exception ex1) { };
        }
    }
}