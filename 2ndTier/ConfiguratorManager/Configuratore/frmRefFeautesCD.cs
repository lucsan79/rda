﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Clifton.Windows.Forms;
using XTreeIIDemo.DialogAdminService;

namespace XTreeDemo
{

    public partial class frmRefFeautesCD : Form
    {
        
        List<String> myListItems;

        DialogAdminService myService;
        System.Net.CookieContainer myContainer;
        DataTable myTable=null;
        string selectedObject = "";

        public frmRefFeautesCD()
        {
            InitializeComponent();
            groupBox1.Text = "Select a feature";

            String exitStatus = "";
            myContainer = new System.Net.CookieContainer();
            myService = new DialogAdminService();
            myService.Url = System.Configuration.ConfigurationSettings.AppSettings["EPLMS_INSTANCE_URL"].ToString();
            myService.CookieContainer = myContainer;
            if (myService.login("DialogAdmin", "DialogAdmin", ref exitStatus) != 0)
            {
                throw new Exception(exitStatus);
            }
            else
                LoadTable();

        }

        private void LoadTable()
        {
            treeView1.Nodes.Clear();
            String exitMessage="";
            Int32 retValue = 0;
            if (myTable == null)
            {
                myTable = new DataTable("Caratteristiche");
                retValue = myService.GetListFeatures(ref myTable, ref exitMessage);
            }
            if (myTable != null)
            {
                String myFilter = txtFilter.Text.Trim() ;
                if(myFilter=="*")
                    myFilter="";
                foreach (DataRow row in myTable.Rows)
                {
                    if(!row["eName"].ToString().StartsWith("CRT-"))
                    {
                        if (row["eName"].ToString().ToLower().Contains(myFilter.ToLower()) || row["eDescription"].ToString().ToLower().Contains(myFilter.ToLower()))
                        {
                            TreeNode myNode = new TreeNode();
                            myNode.Text = row["eName"] + " - " + row["eDescription"];
                            myNode.Tag = row;
                            myNode.ImageIndex = 1;
                            myNode.SelectedImageIndex = 1;
                            if(row["TIPOVR"].ToString()=="T")
                                myNode.Nodes.Add("....");
                            treeView1.Nodes.Add(myNode);
                        }
                    }
                }
            }
        }


        private void treeview1_AfterCheck(object sender, TreeViewEventArgs e)
        {
           
            if (e.Node.Checked)
            {
                if (e.Node.Nodes.Count > 0)
                {
                    e.Node.Expand();
                    e.Node.Checked = false;
                }
                else
                    DeselectAll(e.Node);
            }
        }
        private void DeselectAll(TreeNode givenNode)
        {
            if (treeView1.Nodes.Count > 0)
            {
                foreach (TreeNode node in treeView1.Nodes)
                {
                    if (node != givenNode)
                    node.Checked = false;
                    
                    if (node.Nodes.Count > 0)
                    {
                        foreach (TreeNode subnode in node.Nodes)
                        {
                            if (subnode != givenNode)
                                subnode.Checked = false;
                        }
                    }
                    
                }
            }
        }
        
        public String SelectedObject()
        {
        //    if (newListBox != null)
        //        return newListBox.SelectedItem.ToString();
        //    else
            return selectedObject;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (treeView1.Nodes.Count > 0)
            {
                foreach (TreeNode node in treeView1.Nodes)
                {
                    if(node.Checked)
                    {
                        DataRow myRow = (DataRow)node.Tag;
                        selectedObject = string.Format("{0}|{1}|{2}", myRow["CDVAVR"].ToString(), myRow["DSVAVR"].ToString(), "");
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        this.Close();
                        return;
                    }
                    
                    if (node.Nodes.Count > 0)
                    {
                        foreach (TreeNode subnode in node.Nodes)
                        {
                            if (subnode.Checked)
                            {
                                DataRow myRow = (DataRow)subnode.Tag;
                                DataRow myRowParent = (DataRow)subnode.Parent.Tag;
                                selectedObject = string.Format("{0}|{1}|{2}", myRowParent["CDVAVR"].ToString(), myRowParent["DSVAVR"].ToString(), myRow["CDVAVR"].ToString());
                                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                                this.Close();
                                return;
                            }
                        }
                    }

                }
            }
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //this.DialogResult = DialogResult.Cancel;
            //newListBox.SelectedIndex = -1;
            //this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LoadTable();
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeNode current = e.Node;
            current.Nodes.Clear();
            if(myTable !=null)
            {
                DataRow myTag =(DataRow) e.Node.Tag;
                DataRow[] myRow = myTable.Select("eKey1='" + myTag["eName"] + "'");
                foreach (DataRow row in myRow)
                {
                    if (row["eName"].ToString().StartsWith("CRT-"))
                    {
                        TreeNode myNode = new TreeNode();
                        myNode.Text = row["CDVAVR"] + " - " + row["eDescription"];
                        myNode.ImageIndex = 0;
                        myNode.SelectedImageIndex = 0;
                        myNode.Tag = row;
                        current.Nodes.Add(myNode);
                       
                    }
                }
            }
        }


    }


}
