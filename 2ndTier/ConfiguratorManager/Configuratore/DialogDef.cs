using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Reflection;
using System.Text;

/*
 * The SchemaDef is a collection of serialization classes that also support design-time
 * editing of the collections and properties, primarily to be used with the property grid.
 */

namespace XTreeDemo
{
	/// <summary>
	/// Supported field types, used by the schema to define table field.
	/// </summary>
	public enum FieldType
	{
		String,
		Integer,
		Real,
		Money,
		Boolean,
		Date,
		Time,
		Guid,
		DateTime,
	}

	/// <summary>
	/// Supports the declarative instantiation of the table/view schema definition.
	/// </summary>
	[DefaultProperty("Name")]
	public class DialogDef
	{
		protected List<OptionDef> options;
        protected List<FunctionDef> functions;
		protected string name;
        protected string famiglia;
        protected string prodotto;
        protected string codifica;
        protected string cadref;
        protected Int32 cdID;
        protected string cdName;
        protected string tipologiaDialogo;

        [Browsable(false)]
        public Int32 CollaborationID
        {
            get { return cdID; }
            set { cdID = value; }
        }

        [Browsable(false)]
        public string CollaborationName
        {
            get { return cdName; }
            set { cdName = value; }
        }


		/// <summary>
		/// Gets/sets the schema name.
		/// </summary>
		[Category("General")]
		[Description("Nome del Dialogo.")]
        [DefaultValue("Dialog")]
		[Browsable(true)]
		[XmlAttribute("Name")]
        [ReadOnly(true)]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

       
        [Category("Dialog Referement")]
        [Description("Famiglia identificativa del Prodotto.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Famiglia")]
        [ReadOnly(true)]

        public string Famiglia
        {
            get { return famiglia; }
            set { famiglia = value; }
        }

        [Category("Dialog Referement")]
        [Description("Tipologia identificativa del dialogo.")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("Tipologia")]
        [ReadOnly(true)]
        public string Tipologia
        {
            get { return tipologiaDialogo; }
            set { tipologiaDialogo = value; }
        }

       
        [Category("Dialog Referement")]
        [Description("Nome del Prodotto.")]
        [DefaultValue("")]
        [Browsable(false)]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string Prodotto
        {
            get { return prodotto ; }
            set { prodotto = value; }
        }

        
        [Category("Dialog Referement")]
        [Description("Codifica / Codice RDA.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Codifica")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string Codifica
        {
            get { return codifica ; }
            set { codifica = value; }
        }

        [Category("Dialog Referement")]
        [Description("Assieme Cad di riferimento.")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("cad")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string CadRef
        {
            get { return cadref ; }
            set { cadref = value; }
        }

     	[Browsable(false)]
		public List<OptionDef> Options
		{
			get { return options; }
		}

       


		[Browsable(false)]
		public List<FunctionDef> Functions
		{
			get { return functions; }
		}


        /// <summary>
        /// Constructor.
        /// </summary>
        public DialogDef(String givenFamiglia, String givenProdotto, String givenCodifica, String givenCadRef, Int32 givenCDID, String givenTipologia)
		{
			options = new List<OptionDef>();
            functions = new List<FunctionDef>();
            //name = "Dialogo";
            famiglia = givenFamiglia;
            prodotto = givenProdotto;
            codifica  = givenCodifica;
            cadref = givenCadRef;
            name =  prodotto ;
            cdID = givenCDID;
            tipologiaDialogo = givenTipologia;
            
		}

        public void MoveOption(String currentOptionId, Int32 delta)
        {
            Int32 oldIndex = -1;
            Int32 newIndex = -1;
            if (options != null)
            {
                for (Int32 i = 0; i < options.Count; i++)
                {
                    if (options[i].ID == currentOptionId)
                    {
                        oldIndex = i;
                        break;
                    }
                }
                if (oldIndex >= 0)
                    newIndex = oldIndex + delta;

            }
            if(oldIndex!=newIndex)
            {
                OptionDef item = options[oldIndex];

                options.RemoveAt(oldIndex);

                options.Insert(newIndex, item);
            }


        }
       
        public DialogDef()
        {
            options = new List<OptionDef>();
            functions = new List<FunctionDef>();
            name = "";
        }

	}

   

    /// <summary>
    /// Supports the declarative instantiation of the table schema definition.
    /// </summary>
    [DefaultProperty("Name")]
    public class FunctionDef
    {
        protected string name;
        protected string warning;
        protected string code="";
        protected string id;
        protected FunctionValueType rType;
        public enum FunctionValueType { N = 1, S, D, BOOL , CURRENCY};

         
        [Category("General")]
        [Description("ID associated to the function.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("ID")]
        [ReadOnly(true)]
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        [Category("General")]
        [Description("Name associated to the function.")]
        [DefaultValue("Function")]
        [Browsable(true)]
        [XmlAttribute("Name")]
        [ReadOnly(false)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        [Category("Details")]
        [Description("VB.NET Code to execute to retrieve values.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Code")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string SourceCode
        {

            get { return code; }
            set { code = value; }
        }


        [Category("Details")]
        [Description("Specify if function return a numeric 'N' or string 'S' value")]
        [DefaultValue(1)]
        [Browsable(false)]
        [ReadOnly(false)]
        [XmlAttribute("ReturnValueType")]
        public FunctionValueType ReturnValueType
        {
            get { return rType; }
            set { rType = value; }
        }

        [Category("Validation")]
        [Description("Error on validation.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Validation")]
        [ReadOnly(true)]
        public string Warning
        {
            get
            {
                warning = warning + "";

                return warning;
            }
            set
            {
                warning = value;

            }

        }



        /// <summary>
        /// Constructor.
        /// </summary>
        public FunctionDef()
        {
            name = "Function";
            
        }

        public Boolean Validate()
        {

            Boolean retValue = true;
            Warning = "";
            if (Name == "" || Name == "Function")
            {
                Warning = "Insert Valid 'Name'";
                retValue = false;
            }
            if (SourceCode == "" )
            {
                if (Warning != "")
                    Warning += "\n";
                Warning += "Insert valid Source Code for Function";
                return false;
            }

            return retValue;

        }

    }


    /// <summary>
    /// Supports the declarative instantiation of the table schema definition.
    /// </summary>
    [DefaultProperty("Name")]
    public class FeatureDef
    {
        protected string name;
        protected string code;
        protected string warning;
        protected string fvalue = "";
        protected string refTo = "";
        protected string id;
        protected UMList um;
        protected CPType target;
        public enum UMList { mm, cm, m, kg, na, euro,dollar, Tipo_LegameA, Tipo_LegameB, Tipo_LegameC, Tipo_LegameD, Tipo_LegameK, Tipo_LegameT }
        public enum CPType { Configurazione, Posa }
        

        [Category("General")]
        [Description("ID associated to the function.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("ID")]
        [ReadOnly(true)]
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        [Category("General")]
        [Description("Name associated to the feature.")]
        [DefaultValue("Function")]
        [Browsable(true)]
        [XmlAttribute("Name")]
        [ReadOnly(false)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        [Category("General")]
        [Description("Codice CP o CODICE Articolo in dialogo di POSA.")]
        [DefaultValue("Function")]
        [Browsable(true)]
        [XmlAttribute("CODE")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        [Category("Referement")]
        [Description("Value identify feature Referement.")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("VALUE")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string ReferTo
        {

            get { return refTo; }
            set { refTo = value; }
        }


        [Category("Feature Value")]
        [Description("Valore della CP o Valore impiego materiale per POSA.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("VALUE")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string Value
        {

            get { return fvalue; }
            set { fvalue = value; }
        }

        [Category("Feature Value")]
        [Description("Unit of measure o Tipologia Legame per la POSA.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("UM")]
        public UMList UM
        {
            get { return um; }
            set { um = value; }
        }


        [Category("Target")]
        [Description("Use feature for Configuration or setting.")]
        [DefaultValue("Configurazione")]
        [Browsable(true)]
        [XmlAttribute("Target")]
        public CPType  FeatureTarget
        {
            get { return target ; }
            set { target = value; }
        }



        [Category("Validation")]
        [Description("Error on validation.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Validation")]
        [ReadOnly(true)]
        public string Warning
        {
            get
            {
                warning = warning + "";

                return warning;
            }
            set
            {
                warning = value;

            }

        }


        public Boolean Validate()
        {

            Boolean retValue = true;
            Warning = "";
            if (Name == "" || Name == "Feature")
            {
                Warning = "Insert Valid 'Name'";
                retValue = false;
            }

            if (Code == "" )
            {
                Warning = "Insert Valid 'Code'";
                retValue = false;
            }

            if (target == CPType.Posa)
            {
                if (!um.ToString().ToLower().StartsWith("Tipo_Legame".ToLower()))
                {
                    Warning = "Please insert 'Tipo Legame' for current Code";
                    retValue = false;
                }

            }
            return retValue;

        }
        
        /// <summary>
        /// Constructor.
        /// </summary>
        public FeatureDef()
        {
            name = "Feature";

        }
    }



    [DefaultProperty("Name")]
    public class ConditionDef
    {
        protected string name="";
        protected string id="";
        protected string warning = "";
        protected ConditionRefType type;
        protected ConditionRefOperatorType refOptionType;
        protected string refOption = "";
        protected ConditionRefRelation refOptionRelation;
        protected string minValue = "";
        protected string maxValue = "";
        protected ConditionRefOperator refOperator ;
        public enum ConditionRefType { OPTION = 1, INPUT, EXTERNAL_TABLES, USER_GROUP, FEATURE };
        public enum ConditionRefRelation { EQUAL = 1, NOT_EQUAL, BETWEEN, NOT_BETWEEN };
        public enum ConditionRefOperator { AND = 1, OR};
        public enum ConditionRefOperatorType { OPTION = 1, VARIABLE };



        [Category("General")]
        [Description("ID associated to the Condition.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("ID")]
        [ReadOnly(true)]
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        [Category("General")]
        [Description("Name associated to the Condition.")]
        [DefaultValue("Function")]
        [Browsable(false)]
        [XmlAttribute("Name")]
        [ReadOnly(false)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        
        [Category("Step 1 Definition")]
        [Description("Condition Referement Type. Value : ANSWER -> Connected to Selected response; INPUT -> Connected to value filled; EXT_REFERMENT -> Connected to value from an External Table")]
        [DefaultValue(ConditionRefType.OPTION)]
        [Browsable(true)]
        [XmlAttribute("TYPE")]
        [ReadOnly(false)]
        public ConditionRefType Type
        {

            get { return type; }
            set { type = value; }
        }


        //[Category("Step 2 Definition")]
        //[Description("Condition Referement Type.")]
        //[Browsable(true)]
        //[XmlAttribute("REFOPTIONTYPE")]
        //[ReadOnly(false)]
        //[DefaultValue(ConditionRefOperatorType.OPTION)]
        //public ConditionRefOperatorType ReferementType
        //{

        //    get { return refOptionType; }
        //    set { refOptionType = value; }
        //}

        [Category("Step 2 Definition")]
        [Description("Condition Referement.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("REFOPTION")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public String ReleatedTo
        {

            get { return "" + refOption; }
            set { 
                refOption = value; }
        }


        [Category("Step 3 Definition")]
        [Description("Type of comparison to be made to validate the condition")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("REFOPERATOR")]
        [ReadOnly(false)]
        public ConditionRefRelation OperationType
        {

            get { return refOptionRelation ; }
            set { refOptionRelation = value; }
        }

        [Category("Step 4 Definition")]
        [Description("MinValue to validate.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("MINVALUE")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string MinValue
        {

            get { return minValue; }
            set { minValue = value; }
        }

        [Category("Step 4 Definition")]
        [Description("MaxValue to validate.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("MAXVALUE")]
        [ReadOnly(false)]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string MaxValue
        {

            get { return maxValue; }
            set { maxValue = value; }
        }


        [Category("Step 5 Definition")]
        [Description("Logical Operator between current Condition and the next.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("REFOPERATOR")]
        [ReadOnly(false)]
        public ConditionRefOperator LogicalOperatorOnNext
        {

            get { return refOperator; }
            set { refOperator = value; }
        }


        [Category("Validation")]
        [Description("Error on validation.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Validation")]
        [ReadOnly(true)]
        public string Warning
        {
            get
            {
                warning = warning + "";

                return warning;
            }
            set
            {
                warning = value;

            }

        }



        /// <summary>
        /// Constructor.
        /// </summary>
        public ConditionDef()
        {
            name = "Condition";
            type = ConditionRefType.OPTION;
            refOptionRelation = ConditionRefRelation.EQUAL;
            refOperator = ConditionRefOperator.AND;
            refOptionType = ConditionRefOperatorType.OPTION;

        }


        public Boolean Validate()
        {

            Boolean retValue = true;
            Warning = "";
            if (Name == "" )
            {
                Warning = "Insert Valid 'Name'";
                retValue = false;
            }
            if (ReleatedTo == "" )
            {
                if (Warning != "")
                    Warning += "\n";
                Warning += "Select Value for for 'ReleatedTo' field";
                retValue = false;
            }


            return retValue;

        }
    }


    [Serializable()]
	[DefaultProperty("Name")]
	public class OptionDef
	{
        protected Boolean hasError=false;
        protected string warning;
        protected string name;
        protected string id;
        protected string comments;
        protected string codifica;  // vtlmrc: aggiunta la proprietÓ codifica
        protected string originalcodifica="";
        protected string digit;  // vtlmrc: aggiunta la proprietÓ digit
        protected string digitPosition;  // vtlmrc: aggiunta la proprietÓ digitPosition
        protected string cadPropertyName;  
        protected string cadPropertyValue;
        protected List<OptionDef> subOptions;
        protected List<ConditionDef> conditions;
        protected List<FeatureDef> features;
        protected List<InputDef> inputs;
        protected List<ExternalTableDef> ext_tables;
        protected AnswerSelectionType qType;
        protected Boolean isDefault;
        protected Boolean forceSelection;
        protected String parentCodifica="";
        public enum AnswerSelectionType { SingleOptionSelection = 1, MultiOptionSelection, AutomaticFeaturesCompilation };
        protected String alternativeDescription="";
        protected Int32  alternativeDescriptionPosition = -1;
        protected Boolean grigliaAutomatica=false;

        [Browsable(false)]
        public String ParentCodifica
        {
            get { 
                if (parentCodifica=="")
                    parentCodifica=originalcodifica;
                return parentCodifica; 
            }
            set { parentCodifica = value; }

        }
        [Browsable(false)]
        public List<OptionDef> SubOptions
        {
            get { return subOptions; }
        }

        [Browsable(false)]
        public List<InputDef> Inputs
        {
            get { return inputs; }
        }

        [Browsable(false)]
        public List<ExternalTableDef> External_Tables
        {
            get { return ext_tables; }
        }

        [Browsable(false)]
        public List<ConditionDef> Conditions
        {
            get { return conditions; }
        }

        [Browsable(false)]
        public List<FeatureDef> Features
        {
            get { return features ; }
        }
        
        
        /// <summary>
        /// Gets/sets minVal
        /// </summary>
        [Category("Griglia Automatica")]
        [Description("abilita per modifica massiva.")]
        [DefaultValue(false)]
        [Browsable(false)]
        [XmlAttribute("GrigliaAutomatica")]
        public Boolean GrigliaAutomatica
        {
            get { return grigliaAutomatica; }
            set { grigliaAutomatica = value; }
        }


        [Category("Validation")]
        [Description("Error on validation.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Validation")]
        [ReadOnly(true)]
        public string Warning
        {
            get {
                warning = warning + "";
               
                return warning;
            }
            set {
                warning = value;
                
            }
       
        }


        [Category("General")]
        [Description("ID associated to the function.")]
        [DefaultValue("Question")]
        [Browsable(true)]
        [XmlAttribute("ID")]
        [ReadOnly(true)]
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        // vtlmrc: getter e setter
        [Category("Dialog Code")]
        [Description("Codifica referement.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Codifica")]
        [ReadOnly(true)]
        public string Codifica
        {
            get { return codifica; }
            set { codifica = value; if (originalcodifica == "")originalcodifica = value; }
        }

       

        // vtlmrc: getter e setter
        [Category("Dialog Code")]
        [Description("Digit referement.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Digit")]
        [ReadOnly(false)]
        public string Digit
        {
            get { return digit; }
            set { digit = value.ToUpper(); }
        }

        // vtlmrc: getter e setter
        [Category("Dialog Code")]
        [Description("Digit position referement. Position start from value '1' ")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("DigitPsition")]
        [ReadOnly(false)]
        public string DigitPosition
        {
            get { return digitPosition; }
            set { if (value == "0")value = "1"; digitPosition = value; }
        }

        [Category("CAD Referement")]
        [Description("Reference to the name of the property in CAD tool.")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("CADPRPNAME")]
        [ReadOnly(false)]
        public string CadPropertyName
        {
            get { return cadPropertyName; }
            set 
            { 
                cadPropertyName = value;
                
            
            }
        }

        [Category("CAD Referement")]
        [Description("Reference to the value of the field releated to the property in CAD tool.")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("CADPRPVALUE")]
        [ReadOnly(true)]
        public string CadPropertyValue
        {
            get { return cadPropertyValue; }
            set { cadPropertyValue = value; }
        }


       

        [Category("Abaco Referement")]
        [Description("Use alternative description to Option Title in Abaco")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("ALTERNATIVEDESCRABACO")]
        [ReadOnly(false)]
        public String AlternativeDescription
        {
            get { return alternativeDescription; }
            set { alternativeDescription = value; }
        }

        [Category("Abaco Referement")]
        [Description("Use alternative description to Option Title in Abaco at Position")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("ALTERNATIVEDESCRABACOPOSITION")]
        [ReadOnly(false)]
        public Int32 AlternativeDescriptionPosition
        {
            get { return alternativeDescriptionPosition; }
            set { alternativeDescriptionPosition = value; }
        }


        public void MoveSubOption(String currentOptionId, Int32 delta)
        {
            Int32 oldIndex = -1;
            Int32 newIndex = -1;
            if (subOptions  != null)
            {
                for (Int32 i = 0; i < subOptions.Count; i++)
                {
                    if (subOptions[i].ID == currentOptionId)
                    {
                        oldIndex = i;
                        break;
                    }
                }
                if (oldIndex >= 0)
                    newIndex = oldIndex + delta;

            }
            if (oldIndex != newIndex)
            {
                OptionDef item = subOptions[oldIndex];

                subOptions.RemoveAt(oldIndex);

                subOptions.Insert(newIndex, item);
            }


        }

        public void MoveCondition(String conditionId,Int32 delta)
        {
            Int32 oldIndex = -1;
            Int32 newIndex = -1;
            if (conditions != null)
            {
                for (Int32 i = 0; i < conditions.Count; i++)
                {
                    if (conditions[i].ID == conditionId)
                    {
                        oldIndex = i;
                        break;
                    }
                }
                if (oldIndex >= 0)
                    newIndex = oldIndex + delta;

            }
            if (oldIndex != newIndex)
            {
                ConditionDef item = conditions[oldIndex];

                conditions.RemoveAt(oldIndex);
                if (conditions.Count < newIndex)
                    conditions.Add(item);
                else
                    conditions.Insert(newIndex, item);
            }

        }

        public void MoveFeatures(String featureId, Int32 delta)
        {
            Int32 oldIndex = -1;
            Int32 newIndex = -1;
            if (features  != null)
            {
                for (Int32 i = 0; i < features.Count; i++)
                {
                    if (features[i].ID == featureId)
                    {
                        oldIndex = i;
                        break;
                    }
                }
                if (oldIndex >= 0)
                    newIndex = oldIndex + delta;

            }
            if (oldIndex != newIndex)
            {
                FeatureDef item = features[oldIndex];

                features.RemoveAt(oldIndex);

                features.Insert(newIndex, item);
            }


        }

        public void refreshCodifica()
        {
            try
            {
                if (digitPosition != "")
                {
                    Int32 index = Int32.Parse(digitPosition);
                    StringBuilder builder;

                    builder = new StringBuilder(ParentCodifica);
                    if (digit != "")
                    {
                        for (int i = 0; i < digit.Length; i++)
                        {
                            builder[Math.Max(0, index - 1)] = digit.ToCharArray()[i];
                            index = index + 1;
                        }
                    }
                    codifica = builder.ToString();

                    if (subOptions != null)
                    {
                        foreach (OptionDef subOpt in subOptions)
                        {
                            subOpt.parentCodifica  = codifica;
                            if (index>0)
                                subOpt.DigitPosition = (index ).ToString() ;
                            subOpt.refreshCodifica();
                        }
                    }
                }
            }
            catch (Exception e) { }
        }

        public Boolean Validate()
        {

            Boolean retValue = true;
            Warning = "";
            if (Name == "" || Name == "Option")
            {
                Warning = "Insert Valid 'Name'";
                retValue= false;
            }
            if (DigitPosition != "" && Digit == "")
            {
                if (Warning != "")
                    Warning += "\n";
                Warning += "Verify 'Digit' for 'DigitPosition' filled";
                retValue= false;
            }
            else if (DigitPosition == "" && Digit != "")
            {
                Warning += "Verify 'DigitPosition' for 'Digit' filled";
                retValue= false;
            }
            
            
            return retValue;

        }



        /// <summary>
        /// Gets/sets field name.  This name corresponds to the persistent store name.
        /// </summary>
        [Category("General")]
		[Description("The name of this field, which must correlate to the persistent store name.")]
		[DefaultValue("Field")]
		[Browsable(true)]
		[XmlAttribute("Name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

        [Category("Selection Type")]
        [Description("Specify if the Option require SubOption, specify if it's necessary to give a response to one or all SubOption.")]
        [DefaultValue(AnswerSelectionType.SingleOptionSelection)]
        [XmlAttribute("OptionType")]
        [Browsable(true)]
        public AnswerSelectionType OptionType
        {
            get { return qType; }
            set {
                qType = value;
                //PropertyDescriptor descriptor = TypeDescriptor.GetProperties(this.GetType())["OptionType"];
                //BrowsableAttribute attrib = (BrowsableAttribute)descriptor.Attributes[typeof(BrowsableAttribute)];
                //FieldInfo isBrowsable = attrib.GetType().GetField("browsable", BindingFlags.NonPublic | BindingFlags.Instance);

                //if (this.subOptions.Count > 0)
                //    isBrowsable.SetValue(attrib, true);
                //else
                //    isBrowsable.SetValue(attrib, false);
            }
        }

        [Category("Option Type")]
        [Description("Specify if the Option is the default selection.")]
        [DefaultValue(false)]
        [XmlAttribute("OptionType")]
        [Browsable(true)]
        public Boolean IsDefault
        {
            get { return isDefault ; }
            set
            {
                isDefault = value;
            }
        }

        [Category("Option Type")]
        [Description("Specify if the system force the Option selection.")]
        [DefaultValue(false)]
        [XmlAttribute("OptionType")]
        [Browsable(true)]
        public Boolean ForceOption
        {
            get { return forceSelection ; }
            set
            {
                forceSelection = value;
            }
        }

		/// <summary>
		/// Constructor.
		/// </summary>
		public OptionDef()
		{
			comments = String.Empty;
            name = "Option";
            digit = "";
            digitPosition = "";
            subOptions = new List<OptionDef>();
            conditions = new List<ConditionDef>();
            features = new List<FeatureDef>();
            inputs = new List<InputDef>();
            ext_tables = new List<ExternalTableDef>();
            qType  = AnswerSelectionType.SingleOptionSelection;
            
		}
	}

  
   
    /// <summary>
    /// Suports the declarative instantiation of a table field definition.
    /// </summary>
    [DefaultProperty("Name")]
    public class InputDef
    {
        protected string name;
        protected string code;
        protected bool  mandatory=true;
        protected string warning;
        protected string minVal;
        protected string maxVal;
        protected string id = "";// vtlmrc
        protected string formula = "";// vtlmrc
        protected UMList um;// vtlmrc
        protected FunctionValueType rType;// vtlmrc
        protected string  vLength="";// vtlmrc
        public enum FunctionValueType { N=1, S , D , BOOL, CURRENCY} //F};// vtlmrc
        public enum UMList { na, euro,dollar, mm, cm, m, kg }
        protected String alternativeDescription = "";
        protected Int32 alternativeDescriptionPosition = -1;
        protected Boolean  grigliaAutomatica = false;

        [Category("General")]
        [Description("The name of this field, which must correlate to the persistent store name.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("NAME")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        [Category("General")]
        [Description("The CODE of this field, which must correlate to the persistent store code.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Code")]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        [Category("General")]
        [Description("Set if Attribute is Mandatory.")]
        [DefaultValue(true)]
        [Browsable(true)]
        [XmlAttribute("Mandatory")]
        public bool  Mandatory
        {
            get { return mandatory ; }
            set { mandatory = value; }
        }

        [Category("General")]
        [Description("The ID of this field, which must correlate to the persistent store ID.")]
        [DefaultValue("")]
        [Browsable(true)]
        [ReadOnly(true)]
        [XmlAttribute("ID")]
        public string ID
        {
            get { return id; }
            set { id = value; }
        }


        // vtlmrc
        [Category("Details")]
        [Description("Formula.")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("Formula")]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string InputFunction
        {
            get { return formula; }
            set { formula = value; }
        }
       
        // vtlmrc
        [Category("Details")]
        [Description("Unit of measure.")]
        [DefaultValue(UMList.na)]
        [Browsable(true)]
        [XmlAttribute("UM")]
        public UMList UM
        {
            get { return um; }
            set { um = value; }
        }
        // vtlmrc
        [Category("Details")]
        [Description("Specify if function return a numeric 'N', string 'S' or date 'D' value")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("InputDataType")]
        public FunctionValueType InputDataType
        {
            get { return rType ; }
            set { rType = value;
            //    if (rType != FunctionValueType.F)
            //         formula = "";
            }
        }

        [Category("Details")]
        [Description("Specify Length of the field")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("InputValueLength")]
        public String InputValueLength
        {
            get { return vLength; }
            set
            {
                vLength = value;
            }
        }
       

        /// <summary>
        /// Gets/sets maxVal
        /// </summary>
        [Category("Data Value Limit")]
        [Description("Enter the allowable maximum value.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("MaxVal")]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string MaxVal
        {
            get { return maxVal; }
            set { maxVal = value; }
        }

        /// <summary>
        /// Gets/sets minVal
        /// </summary>
        [Category("Data Value Limit")]
        [Description("Enter the allowable minimum value.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("MinVal")]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string MinVal
        {
            get { return minVal; }
            set { minVal = value; }
        }

        /// <summary>
        /// Gets/sets minVal
        /// </summary>
        [Category("Griglia Automatica")]
        [Description("abilita per modifica massiva.")]
        [DefaultValue(false)]
        [Browsable(false)]
        [XmlAttribute("GrigliaAutomatica")]
        public Boolean  GrigliaAutomatica
        {
            get { return grigliaAutomatica; }
            set { grigliaAutomatica = value; }
        }


        [Category("Validation")]
        [Description("Error on validation.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Validation")]
        [ReadOnly(true)]
        public string Warning
        {
            get
            {
                warning = warning + "";

                return warning;
            }
            set
            {
                warning = value;

            }

        }


        [Category("Abaco Referement")]
        [Description("Use alternative description to Option Title in Abaco")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("ALTERNATIVEDESCRABACO")]
        [ReadOnly(false)]
        public String AlternativeDescription
        {
            get { return alternativeDescription; }
            set { alternativeDescription = value; }
        }

        [Category("Abaco Referement")]
        [Description("Use alternative description to Option Title in Abaco at Position")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("ALTERNATIVEDESCRABACOPOSITION")]
        [ReadOnly(false)]
        public Int32 AlternativeDescriptionPosition
        {
            get { return alternativeDescriptionPosition; }
            set { alternativeDescriptionPosition = value; }
        }

        

        /// <summary>
        /// Constructor.
        /// </summary>
        public InputDef()
        {
            name = "Input";
            minVal = String.Empty;
            maxVal = String.Empty;
            
        }


        public Boolean Validate()
        {

            Boolean retValue = true;
            Warning = "";
            if (Name == "" || Name == "Input")
            {
                Warning = "Insert Valid 'Name'";
                retValue = false;
            }
            if (Code  == "" )
            {
                Warning = "Insert Valid 'Code'";
                retValue = false;
            }

            //if (InputDataType == FunctionValueType.F && InputFunction.Trim() == "")
            //{
            //    Warning = "Insert Valid 'InputFunction'";
            //    retValue = false;
            //}


            //PropertyDescriptor descriptor = TypeDescriptor.GetProperties(this.GetType())["InputFunction"];
            //BrowsableAttribute attrib = (BrowsableAttribute)descriptor.Attributes[typeof(BrowsableAttribute)];
            //FieldInfo isBrowsable = attrib.GetType().GetField("browsable", BindingFlags.NonPublic | BindingFlags.Instance);

            //if (InputDataType == FunctionValueType.F)
            //    isBrowsable.SetValue(attrib, true);
            //else
            //    isBrowsable.SetValue(attrib, false);


            return retValue;

        }
    }


    /// <summary>
    /// Suports the declarative instantiation of a table field definition.
    /// </summary>
    [DefaultProperty("Name")]
    public class ExternalTableDef
    {
        protected string name;
        public List<String> listFilter = new List<string>();
        protected string warning = "";
        protected string id= "";
        protected string columnCode = "";
        protected string showRowNumber = "";
        protected string columnsToShow = "";
        protected String alternativeDescription = "";
        protected Int32 alternativeDescriptionPosition = -1;
        protected Boolean grigliaAutomatica = false;

        [Browsable(false)]
        public string ColumnCode
        {
            get { return columnCode; }
            set { columnCode = value; }
        }
        [Browsable(false)]
        public string ColumnsToShow
        {
            get { return columnsToShow; }
            set { columnsToShow = value; }
        }
        [Browsable(false)]
        public string ShowRowNumber
        {
            get { return showRowNumber; }
            set { showRowNumber = value; }
        }

        // vtlmrc
        [Category("General")]
        [Description("The ID of External Table Reference.")]
        [DefaultValue("")]
        [Browsable(true)]
        [ReadOnly(true)]
        [XmlAttribute("ID")]
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        // vtlmrc
        [Category("General")]
        [Description("The Table's Name.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Name")]
        [ReadOnly(true)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        // vtlmrc
        [Category("Filters")]
        [Description("The Table Filter.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Name")]
        [Editor(typeof(MultiLineStringEditor), typeof(UITypeEditor))]
        public string Filter
        {
            get {
                if (listFilter.Count > 0)
                    return "Filled";
                else if (showRowNumber != "")
                    return "Filled";
                else
                    return "";
            }
            set {
                //FillFilter(value);
                
            }
        }


        /// <summary>
        /// Gets/sets minVal
        /// </summary>
        [Category("Griglia Automatica")]
        [Description("abilita per modifica massiva.")]
        [DefaultValue(false)]
        [Browsable(false)]
        [XmlAttribute("GrigliaAutomatica")]
        public Boolean GrigliaAutomatica
        {
            get { return grigliaAutomatica; }
            set { grigliaAutomatica = value; }
        }

       
        
        [Category("Validation")]
        [Description("Error on validation.")]
        [DefaultValue("")]
        [Browsable(true)]
        [XmlAttribute("Validation")]
        [ReadOnly(true)]
        public string Warning
        {
            get
            {
                warning = warning + "";

                return warning;
            }
            set
            {
                warning = value;

            }

        }

        [Category("Abaco Referement")]
        [Description("Use alternative description to Option Title in Abaco")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("ALTERNATIVEDESCRABACO")]
        [ReadOnly(false)]
        public String AlternativeDescription
        {
            get { return alternativeDescription; }
            set { alternativeDescription = value; }
        }

        [Category("Abaco Referement")]
        [Description("Use alternative description to Option Title in Abaco at Position")]
        [DefaultValue("")]
        [Browsable(false)]
        [XmlAttribute("ALTERNATIVEDESCRABACOPOSITION")]
        [ReadOnly(false)]
        public Int32 AlternativeDescriptionPosition
        {
            get { return alternativeDescriptionPosition; }
            set { alternativeDescriptionPosition = value; }
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        public ExternalTableDef()
        {
            name = "ExternalTable";
         
        }

        public Boolean Validate()
        {

            Boolean retValue = true;
            Warning = "";
            if (Name == "" || Name == "ExternalTable")
            {
                Warning = "Insert Valid 'Name'";
                retValue = false;
            }


            return retValue;

        }

        public void FillFilter(String value)
        {
           
            String rowNumber = value.Split(";".ToCharArray())[0];
            value = value.Substring(value.IndexOf(";")+1);
            String myColumnOutput = value.Split(";".ToCharArray())[0];
            value = value.Substring(value.IndexOf(";") + 1);
            columnCode = myColumnOutput;
            showRowNumber = rowNumber;
            listFilter.Clear();
            foreach (string val in value.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                if (!listFilter.Contains(val)) listFilter.Add(val);
            }

        }
    }
}
