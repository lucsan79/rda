using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public class FeatureController : XtreeNodeController
	{
		protected FeatureDef featureDef;
		protected DialogDef dialogDef;
        protected OptionDef answerDef;
        protected String id;

		public override string Name
		{
            get { return featureDef.Name; }
		    set {featureDef.Name=value;}
		}

        public override string ID
        {
            get { return featureDef.ID; }
            set { featureDef.ID = value; }
        }

        public FeatureDef FeatureDef
		{
			get { return featureDef; }
		}

		public override object Item
		{
			get { return featureDef; }
		}

		public FeatureController()
		{
           
		}

        public FeatureController(FeatureDef featureDef)
		{

            this.featureDef = featureDef;
		}

        
		public override int Index(object item)
		{
            return -1;
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag,string givenId)
		{

            answerDef = ((OptionController)parentInstance).OptionDef;
            featureDef = new FeatureDef();
            featureDef.ID = givenId;
            answerDef.Features.Add(featureDef);
           
			
			return true;
		}

        

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
            answerDef = ((OptionController)parentInstance).OptionDef;
            answerDef.Features.Remove(featureDef);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
            answerDef = ((OptionController)parentInstance).OptionDef;
            answerDef.Features.Remove(featureDef);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
            answerDef = ((OptionController)parentInstance).OptionDef;
            answerDef.Features.Insert(idx, featureDef);
		}

		public override void Select(TreeNode tn)
		{
            Program.Properties.SelectedObject = featureDef;
		}


        public static Boolean Validate(FeatureDef givenObject)
        {
            Boolean retValue = true;


            return retValue;

        }
	}
}
