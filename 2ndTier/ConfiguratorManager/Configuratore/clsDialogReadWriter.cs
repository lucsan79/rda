﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using Clifton.Windows.Forms;
using System.Windows.Forms;

namespace XTreeDemo
{
    
    static class clsDialogReadWriter
    {
       

        static XmlWriter xmlTextWriter;
        static XmlReader xmlTextReader;
        private static  Dictionary<String, OptionDef> qa;

        public static string WriteDialog(DialogDef givenDialog)
        {

            String folderToCreate = AppDomain.CurrentDomain.BaseDirectory + "EXPORT\\";
            String fileFullName = folderToCreate + givenDialog.Famiglia + "_" + givenDialog.Prodotto + ".xml";
            DirectoryInfo curDir;
            if (!System.IO.Directory.Exists(folderToCreate))
                try
                {
                    curDir = System.IO.Directory.CreateDirectory(folderToCreate);
                }
                catch (Exception e) { }
            if (System.IO.File.Exists(fileFullName))
                try
                {
                    System.IO.File.Delete(fileFullName);
                }
                catch (Exception e) { }

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.OmitXmlDeclaration=true;

            xmlWriterSettings.ConformanceLevel = ConformanceLevel.Fragment;

            xmlWriterSettings.NewLineHandling = NewLineHandling.Entitize;//.NewLineOnAttributes = false;
            xmlWriterSettings.Indent = true;
            xmlTextWriter = null;
            xmlTextWriter = XmlWriter.Create(fileFullName, xmlWriterSettings);
           
            xmlTextWriter.WriteStartElement("Dialogs");
            xmlTextWriter.WriteStartAttribute("CODIFICA");
            if (givenDialog.Codifica.Trim() == "")
                givenDialog.Codifica = "-----------------";
            xmlTextWriter.WriteValue(givenDialog.Codifica);

            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("PRODOTTO");
            xmlTextWriter.WriteValue(givenDialog.Prodotto);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("FAMIGLIA");
            xmlTextWriter.WriteValue(givenDialog.Famiglia);
            xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CADASM");
            //xmlTextWriter.WriteValue(givenDialog.CadRef);
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("TIPOLOGIA");
            //xmlTextWriter.WriteValue(givenDialog.Tipologia );
            //xmlTextWriter.WriteEndAttribute();

            if(givenDialog.Options!=null)
                foreach (OptionDef givenOption in givenDialog.Options)
                {
                    if (givenOption.SubOptions != null && givenOption.SubOptions.Count>0)
                        WriteQuestion(givenOption, givenDialog); 
                    else
                        WriteQuestionAlternative(givenOption, givenDialog); 
                }

            xmlTextWriter.WriteEndElement();

            xmlTextWriter.Close();

            if (System.IO.File.Exists(fileFullName))
                return fileFullName;
            else
                return "";
        }


       

        public static List<String> WriteDialogFunction(DialogDef givenDialog)
        {

            String folderToCreate = AppDomain.CurrentDomain.BaseDirectory + "EXPORT\\";
            List<String> myListFunction = new List<String>();

            if (givenDialog.Functions != null)
            {
                foreach (FunctionDef myFunction in givenDialog.Functions)
                {
                    String fileFullName = folderToCreate + myFunction.ID + " - " + myFunction.Name + ".vb";
                    System.IO.StreamWriter myW = new StreamWriter(fileFullName);
                    myW.WriteLine(myFunction.SourceCode);
                    myW.Close();
                    if (System.IO.File.Exists(fileFullName))
                        myListFunction.Add(fileFullName);
                }
            }

            return myListFunction;
        }


        public static DialogDef ReadDialog(ref XTree sdTree, String fileName,Int32 givenCDId)
        {
            qa = new Dictionary<String, OptionDef>();
            DialogDef myDialog = null;
            String folderToCreate = AppDomain.CurrentDomain.BaseDirectory + "EXPORT\\";
            String fileFullName = fileName;
          

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.IgnoreComments = true;
            xmlReaderSettings.ConformanceLevel = ConformanceLevel.Document;

            XmlDocument doc = new XmlDocument();
            //Load the the document with the last book node.
            XmlTextReader reader = new XmlTextReader(fileFullName);
            reader.Read();
            // load reader
            doc.Load(reader);
            try
            {
                if (doc.ChildNodes != null)
                {
                    foreach (XmlNode givenNode in doc.ChildNodes)
                    {
                        if (givenNode.Name == "Dialogs")
                        {
                            myDialog = ((DialogController)((Clifton.Windows.Forms.XmlTree.NodeInstance)sdTree.Nodes[0].Tag).Instance).DialogDef;
                            myDialog.CollaborationID = givenCDId;
                            if (givenNode.Attributes != null && myDialog != null)
                            {
                                foreach (XmlAttribute att in givenNode.Attributes)
                                {
                                    switch (att.Name)
                                    {
                                        case "CODIFICA":
                                            myDialog.Codifica = att.Value;
                                            break;
                                        case "CADASM":
                                            myDialog.CadRef = att.Value;
                                            break;
                                        case "PRODOTTO":
                                            myDialog.Prodotto = att.Value;
                                            break;
                                        case "FAMIGLIA":
                                            myDialog.Famiglia = att.Value;
                                            break;
                                        case "TIPOLOGIA":
                                            myDialog.Tipologia = att.Value;
                                            break;
                                    }


                                }
                            }
                            if (myDialog.Tipologia == "")
                                myDialog.Tipologia = "Dialogo di Configurazione";

                            if (givenNode.ChildNodes != null)
                                foreach (XmlNode question in givenNode.ChildNodes)
                                {
                                    String id = question.Attributes["ID"].Value;
                                    if (qa.ContainsKey(id))
                                    {
                                        OptionDef myOption = qa[id];
                                        ReadQuestionAnswer(ref myOption, question);
                                    }
                                    else
                                        ReadDialogQuestion(ref myDialog, question);

                                }

                        }

                    }


                }
            }
            catch (Exception e) { MessageBox.Show(e.StackTrace); };

            reader.Close();
            
            return myDialog;

        }


        public static DialogDef ReadDialogFunction(DialogDef myDialog, String fileName)
        {
           
            String folderToCreate = AppDomain.CurrentDomain.BaseDirectory + "EXPORT\\";
            String fileFullName = fileName;
            String contFun = "";

            if (System.IO.File.Exists(fileName))
            {
                System.IO.StreamReader myReader = new StreamReader(fileName);
                contFun = myReader.ReadToEnd();
                myReader.Close();

                //if (contFun != "")
                //{
                    FunctionDef myFunction = new FunctionDef();

                    myFunction.ID = System.IO.Path.GetFileNameWithoutExtension(fileName);

                    String idFunction = myFunction.ID.Substring(0, myFunction.ID.IndexOf(" - "));
                    String nameFunction = myFunction.ID.Replace(idFunction + " - ", "");
                    myFunction.ID = idFunction;
                    myFunction.Name = nameFunction;

                    myFunction.SourceCode = contFun;

                    myDialog.Functions.Add(myFunction);
                //}
            }
            return myDialog;

        }

        public static void ReadDialogQuestion(ref DialogDef myDialog, XmlNode givenQuestion)
        {

            OptionDef optionDef = new OptionDef();
            if (givenQuestion.Attributes != null)
            {
                foreach (XmlAttribute att in givenQuestion.Attributes)
                {
                    switch (att.Name)
                    {
                        case "ID":
                            optionDef.ID = att.Value;
                            break;
                        case "TITLE":
                            optionDef.Name = att.Value;
                            break;
                        case "OPTIONTYPE":
                            optionDef.OptionType = OptionDef.AnswerSelectionType.SingleOptionSelection;
                            if(att.Value=="SingleOptionSelection")
                                optionDef.OptionType = OptionDef.AnswerSelectionType.SingleOptionSelection;
                            else if (att.Value == "MultiOptionSelection")
                                optionDef.OptionType = OptionDef.AnswerSelectionType.MultiOptionSelection;
                            else if (att.Value == "AutomaticFeaturesCompilation")
                                optionDef.OptionType = OptionDef.AnswerSelectionType.AutomaticFeaturesCompilation;
                            break;
                        case "CAD_REF":
                            optionDef.CadPropertyName = att.Value;
                            break;
                        case "CAD_VALUE":
                            optionDef.CadPropertyValue = att.Value;
                            break;
                        case "CODE_DIGIT":
                            optionDef.Digit  = att.Value;
                            break;
                        case "CODE_POSITION":
                            optionDef.DigitPosition = att.Value;
                            break;
                        case "ALTERNATIVEDESCRABACO":
                            optionDef.AlternativeDescription = att.Value;
                            break;
                        case "ALTERNATIVEDESCRABACOPOSITION":
                            optionDef.AlternativeDescriptionPosition = Int32.Parse(att.Value);
                            break;
                        case "GRIGLIAAUTOMATICA":
                            optionDef.GrigliaAutomatica = Boolean.Parse(att.Value);
                            break;
                    }


                }
            }

            optionDef.Codifica = myDialog.Codifica; // vtlmrc: set della variabile codifica
            //CONDITION E FEATURES
            if (givenQuestion.ChildNodes != null)
            {
                foreach (XmlNode conditions in givenQuestion.ChildNodes)
                {
                    if (conditions.Name == "Conditions")
                    {
                        foreach (XmlNode condition in conditions.ChildNodes)
                        {
                            ConditionDef condDef = new ConditionDef();
                            //<condition ID="SPC07014.2" TYPE="CL" REFOPTION="1310000001_1" REFOPTIONRELATION="DIVERSO" MINVAL="16" MAXVAL="1000000" REFOPERATOR="OR" />
                            // <condition ID="DD0101.1" TYPE="ANSWER" REFOPTION="DD02 pasini" REFOPTIONRELATION="EQUAL" REFOPERATOR="AND" />
                            if (condition.Attributes["ID"] != null)
                                condDef.ID = condition.Attributes["ID"].Value;

                            if (condition.Attributes["TYPE"] != null)
                                condDef.Type = (ConditionDef.ConditionRefType)Enum.Parse(typeof(ConditionDef.ConditionRefType), condition.Attributes["TYPE"].Value);

                            if (condition.Attributes["REFOPTIONRELATION"] != null)
                                condDef.OperationType = (ConditionDef.ConditionRefRelation)Enum.Parse(typeof(ConditionDef.ConditionRefRelation), condition.Attributes["REFOPTIONRELATION"].Value);

                            if (condition.Attributes["REFOPERATOR"] != null)
                                condDef.LogicalOperatorOnNext = (ConditionDef.ConditionRefOperator)Enum.Parse(typeof(ConditionDef.ConditionRefOperator), condition.Attributes["REFOPERATOR"].Value);


                            if (condition.Attributes["REFOPTION"] != null)
                                condDef.ReleatedTo =condition.Attributes["REFOPTION"].Value;


                            if (condition.Attributes["MINVAL"] != null)
                                condDef.MinValue = condition.Attributes["MINVAL"].Value;

                            if (condition.Attributes["MAXVAL"] != null)
                                condDef.MaxValue  = condition.Attributes["MAXVAL"].Value;
                            if (condDef.ID.ToString().EndsWith(".0"))
                                continue;

                            optionDef.Conditions.Add(condDef);
                            
                        }
                    }
                }
                ReadFeaturesNodes(ref optionDef, givenQuestion.ChildNodes);
                
                
            }

            //LEGGIAMO LE ANSWER
            if (givenQuestion.ChildNodes != null)
                foreach (XmlNode answer in givenQuestion.ChildNodes)
                    if(answer.Name=="Answers")
                        ReadAnswerQuestion(ref optionDef, answer);

            myDialog.Options.Add(optionDef);


        }

        public static void ReadAnswerQuestion(ref OptionDef myQuestion, XmlNode givenAnswer)
        {
            OptionDef optionDef = new OptionDef();
            if (givenAnswer.Attributes != null)
            {
                foreach (XmlAttribute att in givenAnswer.Attributes)
                {
                    switch (att.Name)
                    {
                        case "ID":
                            optionDef.ID = att.Value;
                            break;
                        case "TITLE":
                            optionDef.Name = att.Value;
                            break;
                        case "OPTIONTYPE":
                            if (att.Value == "SingleOptionSelection")
                                optionDef.OptionType = OptionDef.AnswerSelectionType.SingleOptionSelection;
                            else
                                optionDef.OptionType = OptionDef.AnswerSelectionType.MultiOptionSelection;
                            break;
                        case "CAD_REF":
                            optionDef.CadPropertyName = att.Value;
                            break;
                        case "CAD_VALUE":
                            optionDef.CadPropertyValue = att.Value;
                            break;
                        case "CODE_DIGIT":
                            optionDef.Digit = att.Value;
                            break;
                        case "CODE_POSITION":
                            optionDef.DigitPosition = att.Value;
                            break;
                        case "DEFAULT":
                            optionDef.IsDefault = Boolean.Parse(att.Value);
                            break;
                        case "FIXED":
                            optionDef.ForceOption =Boolean.Parse(att.Value);
                            break;
                        case "ALTERNATIVEDESCRABACO":
                            optionDef.AlternativeDescription = att.Value;
                            break;
                        case "ALTERNATIVEDESCRABACOPOSITION":
                            optionDef.AlternativeDescriptionPosition = Int32.Parse(att.Value);
                            break;
                        case "GRIGLIAAUTOMATICA":
                            optionDef.GrigliaAutomatica = Boolean.Parse(att.Value);
                            break;

                    }


                }
            }

            optionDef.Codifica = myQuestion.Codifica; // vtlmrc: set della variabile codifica

            if (givenAnswer.ChildNodes != null)
            {
                foreach (XmlNode conditions in givenAnswer.ChildNodes)
                {
                    if (conditions.Name == "Conditions")
                    {
                        foreach (XmlNode condition in conditions.ChildNodes)
                        {
                            ConditionDef condDef = new ConditionDef();
                            //<condition ID="SPC07014.2" TYPE="CL" REFOPTION="1310000001_1" REFOPTIONRELATION="DIVERSO" MINVAL="16" MAXVAL="1000000" REFOPERATOR="OR" />
                            // <condition ID="DD0101.1" TYPE="ANSWER" REFOPTION="DD02 pasini" REFOPTIONRELATION="EQUAL" REFOPERATOR="AND" />
                            if (condition.Attributes["ID"] != null)
                                condDef.ID = condition.Attributes["ID"].Value;

                            if (condition.Attributes["TYPE"] != null)
                                condDef.Type = (ConditionDef.ConditionRefType)Enum.Parse(typeof(ConditionDef.ConditionRefType), condition.Attributes["TYPE"].Value);

                            if (condition.Attributes["REFOPTIONRELATION"] != null)
                                condDef.OperationType = (ConditionDef.ConditionRefRelation)Enum.Parse(typeof(ConditionDef.ConditionRefRelation), condition.Attributes["REFOPTIONRELATION"].Value);

                            if (condition.Attributes["REFOPERATOR"] != null)
                                condDef.LogicalOperatorOnNext = (ConditionDef.ConditionRefOperator)Enum.Parse(typeof(ConditionDef.ConditionRefOperator), condition.Attributes["REFOPERATOR"].Value);


                            if (condition.Attributes["REFOPTION"] != null)
                                condDef.ReleatedTo = condition.Attributes["REFOPTION"].Value;


                            if (condition.Attributes["MINVAL"] != null)
                                condDef.MinValue = condition.Attributes["MINVAL"].Value;

                            if (condition.Attributes["MAXVAL"] != null)
                                condDef.MaxValue = condition.Attributes["MAXVAL"].Value;
                            if (condDef.ID.ToString().EndsWith(".0"))
                                continue;
                            optionDef.Conditions.Add(condDef);

                        }
                    }
                }

                ReadFeaturesNodes(ref optionDef, givenAnswer.ChildNodes);
                
            }




            if(!qa.ContainsKey(optionDef.ID))
                qa.Add(optionDef.ID, optionDef);
            if (givenAnswer.ChildNodes != null)
                foreach (XmlNode answer in givenAnswer.ChildNodes)
                    if(answer.Name=="Question")
                        ReadAnswerQuestion(ref optionDef, answer);

            if (myQuestion.ID != optionDef.ID)
                myQuestion.SubOptions.Add(optionDef);
            else
            {
                if (optionDef.Inputs != null)
                    foreach (InputDef myInput in optionDef.Inputs)
                        myQuestion.Inputs.Add(myInput);
                if (optionDef.External_Tables != null)
                    foreach (ExternalTableDef  myInput in optionDef.External_Tables)
                        myQuestion.External_Tables.Add(myInput);
                
            }

        }

        public static void ReadFeaturesNodes(ref OptionDef optionDef,XmlNodeList givenNodes)
        {

            foreach (XmlNode features in givenNodes)
            {
                if (features.Name == "Features")
                {
                    foreach (XmlNode feature in features.ChildNodes)
                    {

                        //<Feature FTYPE="CP" CODE="DOC - Documento" UM=".." VALUE="DOC" />
                        FeatureDef featureDef = null;
                        if (feature.Attributes["FTYPE"].Value == "CP")
                        {
                            featureDef = new FeatureDef();
                            if (feature.Attributes["CODE"] != null)
                            {
                                featureDef.Code = feature.Attributes["CODE"].Value.Substring(0, feature.Attributes["CODE"].Value.IndexOf(" "));
                                featureDef.Name = feature.Attributes["CODE"].Value.Substring(feature.Attributes["CODE"].Value.IndexOf(" ") + 2).Trim();
                            }

                            if (feature.Attributes["ID"] != null)
                            {
                                featureDef.ID = feature.Attributes["ID"].Value;
                            }
                            else
                                featureDef.ID = featureDef.Code;

                            if (feature.Attributes["VALUE"] != null)
                                featureDef.Value = feature.Attributes["VALUE"].Value;

                            if (feature.Attributes["UM"] != null)
                            {
                                if (feature.Attributes["UM"].Value == "..")
                                    feature.Attributes["UM"].Value = "na";
                                featureDef.UM = (FeatureDef.UMList)Enum.Parse(typeof(FeatureDef.UMList), feature.Attributes["UM"].Value);
                            }
                            featureDef.ReferTo="";
                            if (feature.Attributes["REF_TO"] != null)
                            {
                                featureDef.ReferTo = feature.Attributes["REF_TO"].Value;
                            }

                            featureDef.FeatureTarget = FeatureDef.CPType.Configurazione;
                            if (feature.Attributes["TARGET"] != null)
                            {
                                featureDef.FeatureTarget = (FeatureDef.CPType)Enum.Parse(typeof(FeatureDef.CPType), feature.Attributes["TARGET"].Value);
                            }

                            
                            optionDef.Features.Add(featureDef);
                        }

                        if (feature.Attributes["FTYPE"].Value == "CA")
                        {
                            // <Feature FTYPE="CA" CODE="SS010302-Inp1 - Input2" UM="mm" MINVAL="" MAXVAL="" TYPE="0" />
                            InputDef myInput = new InputDef();
                            if (feature.Attributes["CODE"] != null)
                            {
                                myInput.Code  = feature.Attributes["CODE"].Value.Substring(0, feature.Attributes["CODE"].Value.IndexOf(" "));
                                myInput.Name = feature.Attributes["CODE"].Value.Substring(feature.Attributes["CODE"].Value.LastIndexOf("-") + 2).Trim();
                            }

                            if (feature.Attributes["ID"] != null)
                                myInput.ID = feature.Attributes["ID"].Value;
                            else
                                featureDef.ID = featureDef.Code;


                            if (feature.Attributes["MINVAL"] != null)
                                myInput.MinVal = feature.Attributes["MINVAL"].Value;

                            if (feature.Attributes["MAXVAL"] != null)
                                myInput.MaxVal = feature.Attributes["MAXVAL"].Value;

                            if (feature.Attributes["GRIGLIAAUTOMATICA"] != null)
                            {
                                try
                                {
                                    myInput.GrigliaAutomatica = Boolean.Parse(feature.Attributes["GRIGLIAAUTOMATICA"].Value);
                                }
                                catch (Exception e)
                                {
                                    myInput.GrigliaAutomatica = false;
                                }
                            }

                                

                            try
                            {
                                if (feature.Attributes["TYPE"] != null)
                                    myInput.InputDataType = (InputDef.FunctionValueType)Enum.Parse(typeof(InputDef.FunctionValueType), feature.Attributes["TYPE"].Value);
                            }
                            catch (Exception ex) {

                            }
                           
                            if (feature.Attributes["UM"] != null)
                            {
                                if (feature.Attributes["UM"].Value == "..")
                                    feature.Attributes["UM"].Value = "na";
                                myInput.UM = (InputDef.UMList)Enum.Parse(typeof(InputDef.UMList), feature.Attributes["UM"].Value);
                            }

                            
                            try
                            {
                                if (feature.Attributes["ISMANDATORY"] != null)
                                    myInput.Mandatory = (bool)(bool.Parse(feature.Attributes["ISMANDATORY"].Value.ToString()));
                            }
                            catch (Exception ex) {
                                myInput.Mandatory = true;
                            }


                             try
                            {
                                if (feature.Attributes["VALUELENGTH"] != null)
                                    myInput.InputValueLength = feature.Attributes["VALUELENGTH"].Value.ToString();
                            }
                            catch (Exception ex) {
                                myInput.InputValueLength = "50";
                            }



                            if (feature.Attributes["ALTERNATIVEDESCRABACOPOSITION"] != null)
                            {
                                if(feature.Attributes["ALTERNATIVEDESCRABACOPOSITION"].Value=="")
                                    feature.Attributes["ALTERNATIVEDESCRABACOPOSITION"].Value="-1";
                                myInput.AlternativeDescriptionPosition = Int32.Parse(feature.Attributes["ALTERNATIVEDESCRABACOPOSITION"].Value);
                            }
                            if (feature.Attributes["ALTERNATIVEDESCRABACO"] != null)
                            {
                                myInput.AlternativeDescription = feature.Attributes["ALTERNATIVEDESCRABACO"].Value;
                            }

                         

                            optionDef.Inputs.Add(myInput);
                        }

                        if (feature.Attributes["FTYPE"].Value == "TE")
                        {
                            ExternalTableDef myTable = new ExternalTableDef();
                            if (feature.Attributes["CODE"] != null)
                                myTable.Name = feature.Attributes["CODE"].Value;
                            if (feature.Attributes["ID"] != null)
                                myTable.ID = feature.Attributes["ID"].Value;
                            if (feature.Attributes["COLUMNS"] != null)
                                myTable.ColumnsToShow  = feature.Attributes["COLUMNS"].Value;
                            if (feature.Attributes["OUTPUT"] != null)
                                myTable.ColumnCode = feature.Attributes["OUTPUT"].Value;
                            if (feature.Attributes["SHOWROWNUMBER"] != null)
                                myTable.ShowRowNumber = feature.Attributes["SHOWROWNUMBER"].Value;

                            if (feature.Attributes["ALTERNATIVEDESCRABACOPOSITION"] != null)
                            {
                                if (feature.Attributes["ALTERNATIVEDESCRABACOPOSITION"].Value == "")
                                    feature.Attributes["ALTERNATIVEDESCRABACOPOSITION"].Value = "-1";
                                myTable.AlternativeDescriptionPosition = Int32.Parse(feature.Attributes["ALTERNATIVEDESCRABACOPOSITION"].Value);
                            }
                            if (feature.Attributes["ALTERNATIVEDESCRABACO"] != null)
                                myTable.AlternativeDescription = feature.Attributes["ALTERNATIVEDESCRABACO"].Value;





                            if (feature.Attributes["GRIGLIAAUTOMATICA"] != null)
                            {
                                try
                                {
                                    myTable.GrigliaAutomatica = Boolean.Parse(feature.Attributes["GRIGLIAAUTOMATICA"].Value);
                                }
                                catch (Exception e)
                                {
                                    myTable.GrigliaAutomatica = false;
                                }
                            }

                            if (feature.HasChildNodes)
                            {
                                myTable.listFilter = new List<string>();
                                foreach (XmlNode myFilter in feature.ChildNodes)
                                {
                                    String val = "";
                                    //<ExternalTableFilter ColumnName="CDCOLFORN" FilterOperator="=" FilterValue="SPC-Fn1 - Function1" />
                                    if (myFilter.Attributes["ColumnName"] != null && myFilter.Attributes["FilterValue"] != null && myFilter.Attributes["FilterOperator"] != null)
                                    {
                                        val = "(";

                                        val =val +  myFilter.Attributes["ColumnName"].Value.ToString();
                                        val = val + " ";
                                        val = val +  myFilter.Attributes["FilterOperator"].Value.ToString();
                                        val = val + " ";
                                        if (myFilter.Attributes["FilterOperator"].Value.ToString().ToUpper() == "LIKE")
                                            val = val + "'";
                                        
                                        val = val + myFilter.Attributes["FilterValue"].Value.ToString();
                                        
                                        if (myFilter.Attributes["FilterOperator"].Value.ToString().ToUpper() == "LIKE")
                                            val = val + "'";


                                        val = val + ")";
                                        myTable.listFilter.Add(val);
                                    }

                                }
                            }


                            optionDef.External_Tables.Add(myTable);
                        }
                    }
                }
            }

        }

        public static void ReadQuestionAnswer(ref OptionDef myQuestion, XmlNode givenAnswer)
        {

            if (givenAnswer.Attributes != null)
            {
                foreach (XmlAttribute att in givenAnswer.Attributes)
                {
                    switch (att.Name)
                    {
                        case "OPTIONTYPE":
                            if (att.Value == "SingleOptionSelection")
                                myQuestion.OptionType = OptionDef.AnswerSelectionType.SingleOptionSelection;
                            else
                                myQuestion.OptionType = OptionDef.AnswerSelectionType.MultiOptionSelection;
                            break;
                        case "CAD_REF":
                            myQuestion.CadPropertyName = att.Value;
                            break;
                        case "CAD_VALUE":
                            myQuestion.CadPropertyValue = att.Value;
                            break;
                        case "CODE_DIGIT":
                            myQuestion.Digit = att.Value;
                            break;
                        case "CODE_POSITION":
                            myQuestion.DigitPosition = att.Value;
                            break;
                        case "DEFAULT":
                            myQuestion.IsDefault = Boolean.Parse(att.Value);
                            break;
                        case "FIXED":
                            myQuestion.ForceOption = Boolean.Parse(att.Value);
                            break;

                        case "GRIGLIAAUTOMATICA":
                            myQuestion.GrigliaAutomatica  = Boolean.Parse(att.Value);
                            break;


                    }


                }
            }

            if (givenAnswer.ChildNodes != null)
                foreach (XmlNode answer in givenAnswer.ChildNodes)
                    if (answer.Name == "Answers")
                        ReadAnswerQuestion(ref myQuestion, answer);
           

        }
       


        public static void WriteQuestion(OptionDef givenQuestion, Object givenParent)
        {

            xmlTextWriter.WriteStartElement("Question");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenQuestion.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenQuestion.Name);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("OPTIONTYPE");
            xmlTextWriter.WriteValue(givenQuestion.OptionType.ToString());
            xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CAD_REF");
            //xmlTextWriter.WriteValue(givenQuestion.CadPropertyName );
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CAD_VALUE");
            //xmlTextWriter.WriteValue(givenQuestion.CadPropertyValue );
            //xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_DIGIT");
            xmlTextWriter.WriteValue(givenQuestion.Digit );
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_POSITION");
            xmlTextWriter.WriteValue(givenQuestion.DigitPosition );
            xmlTextWriter.WriteEndAttribute();


            //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACOPOSITION");
            //xmlTextWriter.WriteValue(givenQuestion.AlternativeDescriptionPosition.ToString().ToUpper());
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACO");
            //xmlTextWriter.WriteValue(givenQuestion.AlternativeDescription);
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("GRIGLIAAUTOMATICA");
            //xmlTextWriter.WriteValue(givenQuestion.GrigliaAutomatica);
            //xmlTextWriter.WriteEndAttribute();

             
            WriteFeatures(givenQuestion.Features, givenQuestion.Inputs,givenQuestion.External_Tables );


            if(givenParent.GetType().Name=="DialogDef")
                WriteCondition(givenQuestion.Conditions, "", "");
            else
                WriteCondition(givenQuestion.Conditions, givenQuestion.ID, givenQuestion.Name);
            
            if (givenQuestion.SubOptions != null)
                foreach (OptionDef itemA in givenQuestion.SubOptions)
                    WriteAnswer(itemA,false);

            xmlTextWriter.WriteEndElement();

            if (givenQuestion.SubOptions != null)
                foreach (OptionDef itemA in givenQuestion.SubOptions)
                    if (itemA.SubOptions.Count > 0)
                        WriteAnswerOnQuestion(itemA);
                    else if(itemA.External_Tables.Count>0)
                        WriteAnswerOnQuestionExternalTable(itemA);
                    else if (itemA.Inputs.Count > 0)
                        WriteAnswerOnQuestionInputs(itemA);
        }


        public static void WriteQuestionAlternative(OptionDef givenQuestion, Object givenParent)
        {

            xmlTextWriter.WriteStartElement("Question");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenQuestion.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenQuestion.Name);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("OPTIONTYPE");
            xmlTextWriter.WriteValue(givenQuestion.OptionType.ToString());
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_DIGIT");
            xmlTextWriter.WriteValue(givenQuestion.Digit);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_POSITION");
            xmlTextWriter.WriteValue(givenQuestion.DigitPosition);
            xmlTextWriter.WriteEndAttribute();


            WriteFeatures(givenQuestion.Features, null,null);


            
            WriteCondition(givenQuestion.Conditions, "", "");

            WriteAnswer(givenQuestion, true);

            xmlTextWriter.WriteEndElement();

          
        }

       

        public static void WriteAnswer(OptionDef givenAnswer, Boolean includeIE)
        {
            xmlTextWriter.WriteStartElement("Answers");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenAnswer.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenAnswer.Name);
            xmlTextWriter.WriteEndAttribute();
            
            //xmlTextWriter.WriteStartAttribute("CAD_REF");
            //xmlTextWriter.WriteValue(givenAnswer.CadPropertyName);
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CAD_VALUE");
            //xmlTextWriter.WriteValue(givenAnswer.CadPropertyValue);
            //xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_DIGIT");
            xmlTextWriter.WriteValue(givenAnswer.Digit);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_POSITION");
            xmlTextWriter.WriteValue(givenAnswer.DigitPosition);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("DEFAULT");
            xmlTextWriter.WriteValue(givenAnswer.IsDefault);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("FIXED");
            xmlTextWriter.WriteValue(givenAnswer.ForceOption);
            xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACOPOSITION");
            //xmlTextWriter.WriteValue(givenAnswer.AlternativeDescriptionPosition.ToString().ToUpper());
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACO");
            //xmlTextWriter.WriteValue(givenAnswer.AlternativeDescription);
            //xmlTextWriter.WriteEndAttribute();


			//ridondanza CP
            if(includeIE)
                //WriteFeatures(givenAnswer.Features, givenAnswer.Inputs, givenAnswer.External_Tables);
                 WriteFeatures(null, givenAnswer.Inputs, givenAnswer.External_Tables);
            else
                WriteFeatures(givenAnswer.Features, null, null);

            WriteCondition(givenAnswer.Conditions,"","");
           
            xmlTextWriter.WriteEndElement();
          
        }

        public static void WriteAnswerOnQuestion(OptionDef givenAnswer)
        {
            
            xmlTextWriter.WriteStartElement("Question");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenAnswer.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenAnswer.Name);
            xmlTextWriter.WriteEndAttribute();
           
            xmlTextWriter.WriteStartAttribute("OPTIONTYPE");
            xmlTextWriter.WriteValue(givenAnswer.OptionType.ToString());
            xmlTextWriter.WriteEndAttribute();


            WriteFeatures(null,null,null);
            WriteCondition(givenAnswer.Conditions, givenAnswer.ID, givenAnswer.Name);


            if (givenAnswer.SubOptions != null)
                foreach (OptionDef itemQ in givenAnswer.SubOptions)
                {
                    WriteQuestionOnAnswer(itemQ);
                }
            xmlTextWriter.WriteEndElement();


            if (givenAnswer.SubOptions != null)
                foreach (OptionDef itemQ in givenAnswer.SubOptions)
                    if (itemQ.SubOptions != null)
                        if (itemQ.SubOptions.Count > 0)
                            WriteQuestion(itemQ, givenAnswer);
                        else if (itemQ.Inputs.Count > 0)
                            WriteQuestionInput(itemQ);
                        else if (itemQ.External_Tables.Count > 0)
                            WriteQuestionExternalTable(itemQ);
        }

        public static void WriteAnswerOnQuestionInputs(OptionDef givenAnswer)
        {


            xmlTextWriter.WriteStartElement("Question");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenAnswer.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenAnswer.Name);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("OPTIONTYPE");
            xmlTextWriter.WriteValue(givenAnswer.OptionType.ToString());
            xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CAD_REF");
            //xmlTextWriter.WriteValue(givenAnswer.CadPropertyName);
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CAD_VALUE");
            //xmlTextWriter.WriteValue(givenAnswer.CadPropertyValue);
            //xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_DIGIT");
            xmlTextWriter.WriteValue(givenAnswer.Digit);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_POSITION");
            xmlTextWriter.WriteValue(givenAnswer.DigitPosition);
            xmlTextWriter.WriteEndAttribute();


            WriteCondition(givenAnswer.Conditions, givenAnswer.ID, givenAnswer.Name);


            WriteAnswer(givenAnswer, true);

            xmlTextWriter.WriteEndElement();



        }


        public static void WriteAnswerOnQuestionExternalTable(OptionDef givenAnswer)
        {


            xmlTextWriter.WriteStartElement("Question");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenAnswer.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenAnswer.Name);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("OPTIONTYPE");
            xmlTextWriter.WriteValue(givenAnswer.OptionType.ToString());
            xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CAD_REF");
            //xmlTextWriter.WriteValue(givenAnswer.CadPropertyName);
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CAD_VALUE");
            //xmlTextWriter.WriteValue(givenAnswer.CadPropertyValue);
            //xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_DIGIT");
            xmlTextWriter.WriteValue(givenAnswer.Digit);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_POSITION");
            xmlTextWriter.WriteValue(givenAnswer.DigitPosition);
            xmlTextWriter.WriteEndAttribute();


            WriteCondition(givenAnswer.Conditions, givenAnswer.ID, givenAnswer.Name);


            WriteAnswer(givenAnswer,true );

            xmlTextWriter.WriteEndElement();


            
        }

        


        public static void WriteQuestionOnAnswer(OptionDef givenQuestion)
        {
            xmlTextWriter.WriteStartElement("Answers");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenQuestion.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenQuestion.Name);
            xmlTextWriter.WriteEndAttribute();


            //xmlTextWriter.WriteStartAttribute("CAD_REF");
            //xmlTextWriter.WriteValue(givenQuestion.CadPropertyName);
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("CAD_VALUE");
            //xmlTextWriter.WriteValue(givenQuestion.CadPropertyValue);
            //xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_DIGIT");
            xmlTextWriter.WriteValue(givenQuestion.Digit);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("CODE_POSITION");
            xmlTextWriter.WriteValue(givenQuestion.DigitPosition);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("DEFAULT");
            xmlTextWriter.WriteValue(givenQuestion.IsDefault);
            xmlTextWriter.WriteEndAttribute();

            xmlTextWriter.WriteStartAttribute("FIXED");
            xmlTextWriter.WriteValue(givenQuestion.ForceOption);
            xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACOPOSITION");
            //xmlTextWriter.WriteValue(givenQuestion.AlternativeDescriptionPosition.ToString().ToUpper());
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACO");
            //xmlTextWriter.WriteValue(givenQuestion.AlternativeDescription);
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("GRIGLIAAUTOMATICA");
            //xmlTextWriter.WriteValue(givenQuestion.GrigliaAutomatica);
            //xmlTextWriter.WriteEndAttribute();

            WriteFeatures(givenQuestion.Features,null,null);
            WriteCondition(givenQuestion.Conditions, "", "");
            xmlTextWriter.WriteEndElement();
            
            //if(givenQuestion.Inputs.Count>0)
            //    WriteQuestionInput(givenQuestion);
            //else if(givenQuestion.External_Tables.Count>0)
            //    WriteQuestionInput(givenQuestion);
            
        }

        
        public static void WriteQuestionInput(OptionDef givenOption)
        {
            xmlTextWriter.WriteStartElement("Question");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenOption.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenOption.Name);
            xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACOPOSITION");
            //xmlTextWriter.WriteValue(givenOption.AlternativeDescriptionPosition.ToString().ToUpper());
            //xmlTextWriter.WriteEndAttribute();

            //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACO");
            //xmlTextWriter.WriteValue(givenOption.AlternativeDescription);
            //xmlTextWriter.WriteEndAttribute();

            //if (tag.ToUpper() == "QUESTION")
            //{
                ////xmlTextWriter.WriteStartAttribute("OPTIONTYPE");
                ////xmlTextWriter.WriteValue(givenQuestion.QuestionType.ToString());
                ////xmlTextWriter.WriteEndAttribute();
            WriteCondition(givenOption.Conditions, givenOption.ID, givenOption.Name);
            
            //if (tag.ToUpper() == "QUESTION")
            //    if (givenAnswer.Questions != null)
            //        foreach (QuestionDef itemQ in givenAnswer.Questions)
            //            WriteQuestion(itemQ, "Answers");

            if (givenOption.Inputs != null)
            {
                if (givenOption.Inputs.Count > 0)
                {
                    xmlTextWriter.WriteStartElement("Answers");
                    xmlTextWriter.WriteStartAttribute("ID");
                    xmlTextWriter.WriteValue(givenOption.ID);
                    xmlTextWriter.WriteEndAttribute();
                    xmlTextWriter.WriteStartAttribute("TITLE");
                    xmlTextWriter.WriteValue(givenOption.Name);
                    xmlTextWriter.WriteEndAttribute();
                    WriteFeatures(null, givenOption.Inputs, null);//givenQuestion.Inputs,givenQuestion.External_Tables );
                    xmlTextWriter.WriteEndElement();
                }
            }
            
            
          //  WriteCondition(givenOption.Conditions, givenOption.ID,givenOption.Name);
            xmlTextWriter.WriteEndElement();
            
           
        }



        public static void WriteQuestionExternalTable(OptionDef givenOption)
        {
            xmlTextWriter.WriteStartElement("Question");
            xmlTextWriter.WriteStartAttribute("ID");
            xmlTextWriter.WriteValue(givenOption.ID);
            xmlTextWriter.WriteEndAttribute();
            xmlTextWriter.WriteStartAttribute("TITLE");
            xmlTextWriter.WriteValue(givenOption.Name);
            xmlTextWriter.WriteEndAttribute();
           
            WriteCondition(givenOption.Conditions, givenOption.ID, givenOption.Name);

            if (givenOption.External_Tables != null)
            {
                if (givenOption.External_Tables.Count > 0)
                {
                    xmlTextWriter.WriteStartElement("Answers");
                    xmlTextWriter.WriteStartAttribute("ID");
                    xmlTextWriter.WriteValue(givenOption.ID);
                    xmlTextWriter.WriteEndAttribute();
                    xmlTextWriter.WriteStartAttribute("TITLE");
                    xmlTextWriter.WriteValue(givenOption.Name);
                    xmlTextWriter.WriteEndAttribute();

                    WriteFeatures(null, null, givenOption.External_Tables);//givenQuestion.Inputs,givenQuestion.External_Tables );
                    xmlTextWriter.WriteEndElement();
                }
            }
           

            //  WriteCondition(givenOption.Conditions, givenOption.ID,givenOption.Name);
            xmlTextWriter.WriteEndElement();


        }


        

        public static void WriteCondition(List<ConditionDef> givenConditions, String idToForce, String refForce )
        {
           
            xmlTextWriter.WriteStartElement("Conditions");
            if (idToForce != "")
            {
                xmlTextWriter.WriteStartElement("condition");
                xmlTextWriter.WriteStartAttribute("ID");
                xmlTextWriter.WriteValue(idToForce + ".0");
                xmlTextWriter.WriteEndAttribute();
                xmlTextWriter.WriteStartAttribute("TYPE");
                xmlTextWriter.WriteValue(ConditionDef.ConditionRefType.OPTION.ToString().ToUpper());
                xmlTextWriter.WriteEndAttribute();
                xmlTextWriter.WriteStartAttribute("REFOPTION");
                xmlTextWriter.WriteValue(idToForce + " " + refForce);
                xmlTextWriter.WriteEndAttribute();
                xmlTextWriter.WriteStartAttribute("REFOPTIONRELATION");
                xmlTextWriter.WriteValue(ConditionDef.ConditionRefRelation.EQUAL.ToString().ToUpper());
                xmlTextWriter.WriteEndAttribute();
                xmlTextWriter.WriteStartAttribute("REFOPERATOR");
                xmlTextWriter.WriteValue(ConditionDef.ConditionRefOperator.AND.ToString().ToUpper());
                xmlTextWriter.WriteEndAttribute();

                xmlTextWriter.WriteStartAttribute("MINVAL");
                xmlTextWriter.WriteValue("");
                xmlTextWriter.WriteEndAttribute();

                xmlTextWriter.WriteStartAttribute("MAXVAL");
                xmlTextWriter.WriteValue("");
                xmlTextWriter.WriteEndAttribute();

                xmlTextWriter.WriteEndElement();

            }
            else
            {

                if (givenConditions != null)
                {

                    foreach (ConditionDef givenCond in givenConditions)
                    {
                        xmlTextWriter.WriteStartElement("condition");
                        xmlTextWriter.WriteStartAttribute("ID");
                        xmlTextWriter.WriteValue(givenCond.ID);
                        xmlTextWriter.WriteEndAttribute();
                        xmlTextWriter.WriteStartAttribute("TYPE");
                        xmlTextWriter.WriteValue(givenCond.Type.ToString().ToUpper());
                        xmlTextWriter.WriteEndAttribute();
                        xmlTextWriter.WriteStartAttribute("REFOPTION");
                        xmlTextWriter.WriteValue(givenCond.ReleatedTo);
                        xmlTextWriter.WriteEndAttribute();
                        xmlTextWriter.WriteStartAttribute("REFOPTIONRELATION");
                        xmlTextWriter.WriteValue(givenCond.OperationType.ToString().ToUpper());
                        xmlTextWriter.WriteEndAttribute();
                        xmlTextWriter.WriteStartAttribute("REFOPERATOR");
                        xmlTextWriter.WriteValue(givenCond.LogicalOperatorOnNext.ToString().ToUpper());
                        xmlTextWriter.WriteEndAttribute();

                        xmlTextWriter.WriteStartAttribute("MINVAL");
                        xmlTextWriter.WriteValue(givenCond.MinValue);
                        xmlTextWriter.WriteEndAttribute();

                        xmlTextWriter.WriteStartAttribute("MAXVAL");
                        xmlTextWriter.WriteValue(givenCond.MaxValue );
                        xmlTextWriter.WriteEndAttribute();


                        xmlTextWriter.WriteEndElement();


                    }
                }
            }
           
           xmlTextWriter.WriteEndElement();
           
        }


        public static void WriteFeatures(List<FeatureDef> givenFeatures, List<InputDef> givenInputs,List<ExternalTableDef> givenTables)
        {

            xmlTextWriter.WriteStartElement("Features");

            if (givenFeatures != null)
            {
                foreach (FeatureDef givenFeature in givenFeatures)
                {
                    xmlTextWriter.WriteStartElement("Feature");
                    xmlTextWriter.WriteStartAttribute("FTYPE");
                    xmlTextWriter.WriteValue("CP");
                    xmlTextWriter.WriteEndAttribute();

                    
                    xmlTextWriter.WriteStartAttribute("CODE");
                    xmlTextWriter.WriteValue(givenFeature.Code + " - " + givenFeature.Name);
                    xmlTextWriter.WriteEndAttribute();
                    
                    xmlTextWriter.WriteStartAttribute("ID");
                    xmlTextWriter.WriteValue(givenFeature.ID);
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("VALUE");
                    xmlTextWriter.WriteValue(givenFeature.Value);
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("UM");
                    if (givenFeature.UM.ToString()=="")
                        xmlTextWriter.WriteValue("na");
                    else
                        xmlTextWriter.WriteValue(givenFeature.UM.ToString());
                    
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("REF_TO");
                    xmlTextWriter.WriteValue(givenFeature.ReferTo);
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("TARGET");
                    xmlTextWriter.WriteValue(givenFeature.FeatureTarget.ToString() );
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteEndElement();


                }
            }
            
            if (givenInputs!=null)
            {
                foreach (InputDef myInput in givenInputs)
                {
                    //<Feature FTYPE="CA" CODE="001 - SPESSORE MURO" UM="MM" MINVAL="50" MAXVAL="800" TYPE="N" ISMANDATORY="true" />
                    xmlTextWriter.WriteStartElement("Feature");
                    xmlTextWriter.WriteStartAttribute("FTYPE");
                    xmlTextWriter.WriteValue("CA");
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("CODE");
                    xmlTextWriter.WriteValue(myInput.Code + " - " + myInput.Name);
                    xmlTextWriter.WriteEndAttribute();


                    xmlTextWriter.WriteStartAttribute("ID");
                    xmlTextWriter.WriteValue(myInput.ID);
                    xmlTextWriter.WriteEndAttribute();
                    //xmlTextWriter.WriteStartAttribute("VALUE");
                    //xmlTextWriter.WriteValue(myInput.Value);
                    //xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("UM");
                    xmlTextWriter.WriteValue(myInput.UM.ToString());
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("MINVAL");
                    xmlTextWriter.WriteValue(myInput.MinVal);
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("MAXVAL");
                    xmlTextWriter.WriteValue(myInput.MaxVal );
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("TYPE");
                                       
                    if (myInput.InputDataType.ToString() == "F")
                        xmlTextWriter.WriteValue(myInput.InputFunction.ToString());
                    else
                        xmlTextWriter.WriteValue(myInput.InputDataType.ToString());
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("ISMANDATORY");
                    xmlTextWriter.WriteValue(myInput.Mandatory.ToString());
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("VALUELENGTH");
                    xmlTextWriter.WriteValue(myInput.InputValueLength.ToString());
                    xmlTextWriter.WriteEndAttribute();

                    


                    //xmlTextWriter.WriteStartAttribute("GRIGLIAAUTOMATICA");
                    //xmlTextWriter.WriteValue(myInput.GrigliaAutomatica);
                    //xmlTextWriter.WriteEndAttribute();

                    //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACOPOSITION");
                    //xmlTextWriter.WriteValue(myInput.AlternativeDescriptionPosition.ToString().ToUpper());
                    //xmlTextWriter.WriteEndAttribute();

                    //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACO");
                    //xmlTextWriter.WriteValue(myInput.AlternativeDescription);
                    //xmlTextWriter.WriteEndAttribute();



                    xmlTextWriter.WriteEndElement();
                }
            }
            if (givenTables != null)
            {
                //<Feature FTYPE="TE" CODE="TE_SPESSORI" >
                foreach (ExternalTableDef myInput in givenTables)
                {
                    xmlTextWriter.WriteStartElement("Feature");
                    xmlTextWriter.WriteStartAttribute("FTYPE");
                    xmlTextWriter.WriteValue("TE");
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("ID");
                    xmlTextWriter.WriteValue(myInput.ID);
                    xmlTextWriter.WriteEndAttribute();


                    xmlTextWriter.WriteStartAttribute("CODE");
                    xmlTextWriter.WriteValue(myInput.Name);
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("COLUMNS");
                    xmlTextWriter.WriteValue(myInput.ColumnsToShow);
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("OUTPUT");
                    xmlTextWriter.WriteValue(myInput.ColumnCode);
                    xmlTextWriter.WriteEndAttribute();

                    xmlTextWriter.WriteStartAttribute("SHOWROWNUMBER");
                    xmlTextWriter.WriteValue(myInput.ShowRowNumber);
                    xmlTextWriter.WriteEndAttribute();


                    //xmlTextWriter.WriteStartAttribute("GRIGLIAAUTOMATICA");
                    //xmlTextWriter.WriteValue(myInput.GrigliaAutomatica);
                    //xmlTextWriter.WriteEndAttribute();


                    //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACOPOSITION");
                    //xmlTextWriter.WriteValue(myInput.AlternativeDescriptionPosition.ToString().ToUpper());
                    //xmlTextWriter.WriteEndAttribute();

                    //xmlTextWriter.WriteStartAttribute("ALTERNATIVEDESCRABACO");
                    //xmlTextWriter.WriteValue(myInput.AlternativeDescription);
                    //xmlTextWriter.WriteEndAttribute();


                    if (myInput.listFilter != null)
                    {
                        if (myInput.listFilter.Count > 0)
                        {
                            foreach(String myValue in myInput.listFilter)
                            {
                                String myOut = myValue;
                                myOut = myOut.Substring(1);
                                myOut = myOut.Substring(0,myOut.Length-1);

                                String ColName = myOut.Substring(0, myOut.IndexOf(" "));
                                myOut = myOut.Replace(ColName + " ", "");
                                String Operator = myOut.Substring(0, myOut.IndexOf(" "));
                                myOut = myOut.Replace(Operator + " ", "");

                                String Option = myOut;
                                if(Option.StartsWith("'") && Option.EndsWith("'"))
                                {
                                    Option=Option.Substring(1);
                                    Option=Option.Substring(0,Option.LastIndexOf("'"));
                                }
                                if (ColName != "" && Operator != "" && Option != "")
                                {
                                    xmlTextWriter.WriteStartElement("ExternalTableFilter");
                                    xmlTextWriter.WriteStartAttribute("ColumnName");
                                    xmlTextWriter.WriteValue(ColName);
                                    xmlTextWriter.WriteEndAttribute();

                                    xmlTextWriter.WriteStartAttribute("FilterOperator");
                                    xmlTextWriter.WriteValue(Operator);
                                    xmlTextWriter.WriteEndAttribute();

                                    xmlTextWriter.WriteStartAttribute("FilterValue");
                                    xmlTextWriter.WriteValue(Option);
                                    xmlTextWriter.WriteEndAttribute();

                                    xmlTextWriter.WriteEndElement();
                                }
                               
                               
                             
                               
                            }
                           
                        }
                    }
                    




                    
                    xmlTextWriter.WriteEndElement();
                }
            }

            xmlTextWriter.WriteEndElement();

        }
    }
}
