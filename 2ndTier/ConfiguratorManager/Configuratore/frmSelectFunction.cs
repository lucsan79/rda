﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Clifton.Windows.Forms;

namespace XTreeDemo
{
    public partial class frmSelectFunction : Form
    {
        String selectionType="";
        clsCDInterface myService;
        DataTable myGroups;
        QuickFind2 myFind;

        public frmSelectFunction(XTree currentTree,String givenSelection)
        {
            InitializeComponent();
            if (givenSelection.ToUpper().Equals("USER_GROUP"))
            {
                treeView1.Nodes.Clear();
                myService = new clsCDInterface();
                myGroups=myService.GetGroupsList();
                if (myGroups != null)
                {
                    foreach (DataRow myGroup in myGroups.Rows)
                    { 
                        TreeNode myNode = new TreeNode();
                        myNode.Text = myGroup[0].ToString();
                        myNode.Name = givenSelection;
                        myNode.Tag = myNode.Text;
                        if(!myGroup[1].ToString().Equals(""))
                            myNode.Text = myNode.Text + " - " + myGroup[1];
                        myNode.Text=myNode.Text.Trim();
                        myNode.ImageIndex=7;
                        myNode.SelectedImageIndex =7;
                        treeView1.Nodes.Add(myNode);
                    }
                }
            }
            else
                Copy(currentTree, treeView1);
            treeView1.ExpandAll();
            selectionType = givenSelection;
            this.Text = "Select " + selectionType.ToUpper();
  
        }

        public void Copy(XTree treeNodes, TreeView treeInCopy)
        {
            TreeNode newTn;
            foreach (TreeNode tn in treeNodes.Nodes)
            {
                newTn = new TreeNode(tn.Text);
                newTn.ImageIndex = 0;
                newTn.Name = tn.Name;
                
                newTn.SelectedImageIndex = 0;
                CopyChilds(newTn, tn);
                treeInCopy.Nodes.Add(newTn);
            }
        }

        public void CopyChilds(TreeNode parent, TreeNode willCopied)
        {
            TreeNode newTn;
            foreach (TreeNode tn in willCopied.Nodes)
            {
                newTn = new TreeNode(tn.Text);
                newTn.Name = tn.Name;
                newTn.ImageKey=tn.Name.ToLower() + ".png";
                newTn.SelectedImageKey = newTn.ImageKey;
                parent.Nodes.Add(newTn);
                if (tn.Nodes != null)
                    if(tn.Nodes.Count>0)
                    CopyChilds(newTn,tn);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (myFind != null)
                myFind.Close();
            if (treeView1.SelectedNode.Name.ToUpper() == selectionType.ToUpper())
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Select correct type:" + selectionType);
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (myFind != null)
                myFind.Close();
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }


        public String SelectedObject()
        {
            if (treeView1  != null)
                return treeView1.SelectedNode.Text.ToString();
            else
                return "";
        }

        private void expandAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
            
        }

        private void collapseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.CollapseAll();
        }

        private void expandNodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
                treeView1.SelectedNode.Expand();
        }

        private void collapseNodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
                treeView1.SelectedNode.Collapse();
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (myFind == null)
               myFind = new QuickFind2();
            myFind.currentTRee = treeView1;
            myFind.Show();
        }

        private void frmSelectFunction_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (myFind != null)
                myFind.Close();
        }
    }
}
