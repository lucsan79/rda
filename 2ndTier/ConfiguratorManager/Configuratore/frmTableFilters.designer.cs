namespace XTreeDemo
{
    partial class frmTableFilters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.userDataGridView = new System.Windows.Forms.DataGridView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.grpDataManipulate = new System.Windows.Forms.GroupBox();
            this.lblPageNums = new System.Windows.Forms.Label();
            this.lblLoadedTable = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cmbFType = new System.Windows.Forms.ComboBox();
            this.txtColName = new System.Windows.Forms.TextBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.cmbFValue = new System.Windows.Forms.ComboBox();
            this.grpSqlServers = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ColumnToFilter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilterType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilterValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.userDataGridView)).BeginInit();
            this.grpDataManipulate.SuspendLayout();
            this.grpSqlServers.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // userDataGridView
            // 
            this.userDataGridView.AllowUserToAddRows = false;
            this.userDataGridView.AllowUserToOrderColumns = true;
            this.userDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.userDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnToFilter,
            this.FilterType,
            this.FilterValue});
            this.userDataGridView.Location = new System.Drawing.Point(6, 27);
            this.userDataGridView.MultiSelect = false;
            this.userDataGridView.Name = "userDataGridView";
            this.userDataGridView.ReadOnly = true;
            this.userDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userDataGridView.Size = new System.Drawing.Size(474, 256);
            this.userDataGridView.TabIndex = 0;
            this.userDataGridView.SelectionChanged += new System.EventHandler(this.userDataGridView_SelectionChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(6, 181);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(66, 23);
            this.btnAdd.TabIndex = 15;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(220, 181);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 17;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // grpDataManipulate
            // 
            this.grpDataManipulate.Controls.Add(this.lblPageNums);
            this.grpDataManipulate.Controls.Add(this.lblLoadedTable);
            this.grpDataManipulate.Controls.Add(this.userDataGridView);
            this.grpDataManipulate.Location = new System.Drawing.Point(333, 13);
            this.grpDataManipulate.Name = "grpDataManipulate";
            this.grpDataManipulate.Size = new System.Drawing.Size(487, 289);
            this.grpDataManipulate.TabIndex = 6;
            this.grpDataManipulate.TabStop = false;
            this.grpDataManipulate.Text = "Data Filter View";
            // 
            // lblPageNums
            // 
            this.lblPageNums.AutoSize = true;
            this.lblPageNums.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageNums.Location = new System.Drawing.Point(261, 459);
            this.lblPageNums.Name = "lblPageNums";
            this.lblPageNums.Size = new System.Drawing.Size(0, 13);
            this.lblPageNums.TabIndex = 18;
            // 
            // lblLoadedTable
            // 
            this.lblLoadedTable.AutoSize = true;
            this.lblLoadedTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoadedTable.Location = new System.Drawing.Point(7, 16);
            this.lblLoadedTable.Name = "lblLoadedTable";
            this.lblLoadedTable.Size = new System.Drawing.Size(0, 13);
            this.lblLoadedTable.TabIndex = 5;
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(6, 140);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(199, 42);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Jayant D. Kulkarni                            Contact me: jayantdotnet@gmail.com";
            // 
            // cmbFType
            // 
            this.cmbFType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFType.FormattingEnabled = true;
            this.cmbFType.Items.AddRange(new object[] {
            "Option",
            "Input",
            "Feature"});
            this.cmbFType.Location = new System.Drawing.Point(6, 90);
            this.cmbFType.Name = "cmbFType";
            this.cmbFType.Size = new System.Drawing.Size(217, 21);
            this.cmbFType.TabIndex = 1;
            this.cmbFType.SelectedIndexChanged += new System.EventHandler(this.cmbFType_SelectedIndexChanged);
            // 
            // txtColName
            // 
            this.txtColName.Location = new System.Drawing.Point(6, 42);
            this.txtColName.Name = "txtColName";
            this.txtColName.Size = new System.Drawing.Size(217, 20);
            this.txtColName.TabIndex = 2;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(6, 26);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(79, 13);
            this.lbl1.TabIndex = 10;
            this.lbl1.Text = "Column to Filter";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(9, 74);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 11;
            this.lblPassword.Text = "Filter Type";
            // 
            // cmbFValue
            // 
            this.cmbFValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFValue.FormattingEnabled = true;
            this.cmbFValue.Location = new System.Drawing.Point(6, 141);
            this.cmbFValue.Name = "cmbFValue";
            this.cmbFValue.Size = new System.Drawing.Size(217, 21);
            this.cmbFValue.TabIndex = 5;
            // 
            // grpSqlServers
            // 
            this.grpSqlServers.Controls.Add(this.button3);
            this.grpSqlServers.Controls.Add(this.button2);
            this.grpSqlServers.Controls.Add(this.btnUpdate);
            this.grpSqlServers.Controls.Add(this.label1);
            this.grpSqlServers.Controls.Add(this.cmbFValue);
            this.grpSqlServers.Controls.Add(this.lblPassword);
            this.grpSqlServers.Controls.Add(this.btnDelete);
            this.grpSqlServers.Controls.Add(this.lbl1);
            this.grpSqlServers.Controls.Add(this.btnAdd);
            this.grpSqlServers.Controls.Add(this.txtColName);
            this.grpSqlServers.Controls.Add(this.cmbFType);
            this.grpSqlServers.Location = new System.Drawing.Point(12, 14);
            this.grpSqlServers.Name = "grpSqlServers";
            this.grpSqlServers.Size = new System.Drawing.Size(301, 288);
            this.grpSqlServers.TabIndex = 5;
            this.grpSqlServers.TabStop = false;
            this.grpSqlServers.Text = "Data Filter Insert";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(148, 181);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(66, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(78, 181);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(66, 23);
            this.btnUpdate.TabIndex = 18;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Filter Value";
            // 
            // ColumnToFilter
            // 
            this.ColumnToFilter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnToFilter.HeaderText = "Column To Filter";
            this.ColumnToFilter.Name = "ColumnToFilter";
            this.ColumnToFilter.ReadOnly = true;
            this.ColumnToFilter.Width = 150;
            // 
            // FilterType
            // 
            this.FilterType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FilterType.HeaderText = "Filter Type";
            this.FilterType.Name = "FilterType";
            this.FilterType.ReadOnly = true;
            // 
            // FilterValue
            // 
            this.FilterValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FilterValue.HeaderText = "Filter Value";
            this.FilterValue.Name = "FilterValue";
            this.FilterValue.ReadOnly = true;
            this.FilterValue.Width = 180;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(220, 259);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 21;
            this.button3.Text = "Save / Exit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // frmTableFilters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(832, 338);
            this.Controls.Add(this.grpDataManipulate);
            this.Controls.Add(this.grpSqlServers);
            this.MaximizeBox = false;
            this.Name = "frmTableFilters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add, Edit and Delete Filter to External Table";
            ((System.ComponentModel.ISupportInitialize)(this.userDataGridView)).EndInit();
            this.grpDataManipulate.ResumeLayout(false);
            this.grpDataManipulate.PerformLayout();
            this.grpSqlServers.ResumeLayout(false);
            this.grpSqlServers.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.DataGridView userDataGridView;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox grpDataManipulate;
        private System.Windows.Forms.Label lblLoadedTable;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblPageNums;
        private System.Windows.Forms.ComboBox cmbFType;
        private System.Windows.Forms.TextBox txtColName;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.ComboBox cmbFValue;
        private System.Windows.Forms.GroupBox grpSqlServers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnToFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilterType;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilterValue;
        private System.Windows.Forms.Button button3;
    }
}

