﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.IO;


namespace XTreeDemo
{
	/// <summary>
	/// Summary description for frmMain.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{

		# region Declarations 
		private System.Windows.Forms.GroupBox gbMain;
		private System.Windows.Forms.TextBox txtprimary;
        private System.Windows.Forms.Button btnOpenFldrBwsr;
		private System.Windows.Forms.TextBox txtfunctions;
        private System.Windows.Forms.Button btnOpenFileDlg;
        private System.Windows.Forms.Button btnImport;
		string strCSVFile="";
		private System.Windows.Forms.GroupBox gbMainUploadData;
	
        System.Data.Odbc.OdbcDataAdapter obj_oledb_da;
        private bool bolColName = true;
		string strFormat="CSVDelimited";
		private System.Windows.Forms.Label lblFolderPath;
		private System.Windows.Forms.Label lblFilePath;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		# endregion

		# region Constructor 

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		# endregion 

		# region Destructor 

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		# endregion

		#region Windows Form Designer generated code 
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gbMain = new System.Windows.Forms.GroupBox();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.lblFolderPath = new System.Windows.Forms.Label();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnOpenFileDlg = new System.Windows.Forms.Button();
            this.txtfunctions = new System.Windows.Forms.TextBox();
            this.btnOpenFldrBwsr = new System.Windows.Forms.Button();
            this.txtprimary = new System.Windows.Forms.TextBox();
            this.gbMainUploadData = new System.Windows.Forms.GroupBox();
            this.gbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMain
            // 
            this.gbMain.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gbMain.Controls.Add(this.lblFilePath);
            this.gbMain.Controls.Add(this.lblFolderPath);
            this.gbMain.Controls.Add(this.btnImport);
            this.gbMain.Controls.Add(this.btnOpenFileDlg);
            this.gbMain.Controls.Add(this.txtfunctions);
            this.gbMain.Controls.Add(this.btnOpenFldrBwsr);
            this.gbMain.Controls.Add(this.txtprimary);
            this.gbMain.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbMain.Location = new System.Drawing.Point(16, 8);
            this.gbMain.Name = "gbMain";
            this.gbMain.Size = new System.Drawing.Size(603, 416);
            this.gbMain.TabIndex = 0;
            this.gbMain.TabStop = false;
            this.gbMain.Text = "Import Dialog Data";
            // 
            // lblFilePath
            // 
            this.lblFilePath.Location = new System.Drawing.Point(18, 53);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(88, 20);
            this.lblFilePath.TabIndex = 12;
            this.lblFilePath.Text = "Functions (.zip)";
            this.lblFilePath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFolderPath
            // 
            this.lblFolderPath.Location = new System.Drawing.Point(16, 23);
            this.lblFolderPath.Name = "lblFolderPath";
            this.lblFolderPath.Size = new System.Drawing.Size(90, 20);
            this.lblFolderPath.TabIndex = 11;
            this.lblFolderPath.Text = "Primary (.xml)";
            this.lblFolderPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnImport
            // 
            this.btnImport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnImport.Location = new System.Drawing.Point(112, 94);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(392, 23);
            this.btnImport.TabIndex = 4;
            this.btnImport.Text = "Import Data";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnOpenFileDlg
            // 
            this.btnOpenFileDlg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenFileDlg.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOpenFileDlg.Location = new System.Drawing.Point(558, 54);
            this.btnOpenFileDlg.Name = "btnOpenFileDlg";
            this.btnOpenFileDlg.Size = new System.Drawing.Size(24, 23);
            this.btnOpenFileDlg.TabIndex = 3;
            this.btnOpenFileDlg.Click += new System.EventHandler(this.btnOpenFileDlg_Click);
            // 
            // txtfunctions
            // 
            this.txtfunctions.BackColor = System.Drawing.SystemColors.Info;
            this.txtfunctions.Location = new System.Drawing.Point(112, 54);
            this.txtfunctions.Name = "txtfunctions";
            this.txtfunctions.ReadOnly = true;
            this.txtfunctions.Size = new System.Drawing.Size(440, 20);
            this.txtfunctions.TabIndex = 2;
            this.txtfunctions.Text = "...\\file.zip";
            // 
            // btnOpenFldrBwsr
            // 
            this.btnOpenFldrBwsr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenFldrBwsr.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOpenFldrBwsr.Location = new System.Drawing.Point(558, 25);
            this.btnOpenFldrBwsr.Name = "btnOpenFldrBwsr";
            this.btnOpenFldrBwsr.Size = new System.Drawing.Size(24, 23);
            this.btnOpenFldrBwsr.TabIndex = 1;
            this.btnOpenFldrBwsr.Click += new System.EventHandler(this.btnOpenFldrBwsr_Click);
            // 
            // txtprimary
            // 
            this.txtprimary.BackColor = System.Drawing.SystemColors.Info;
            this.txtprimary.Location = new System.Drawing.Point(112, 24);
            this.txtprimary.Name = "txtprimary";
            this.txtprimary.ReadOnly = true;
            this.txtprimary.Size = new System.Drawing.Size(440, 20);
            this.txtprimary.TabIndex = 1;
            this.txtprimary.Text = "..\\file.xml";
            // 
            // gbMainUploadData
            // 
            this.gbMainUploadData.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gbMainUploadData.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbMainUploadData.Location = new System.Drawing.Point(16, 432);
            this.gbMainUploadData.Name = "gbMainUploadData";
            this.gbMainUploadData.Size = new System.Drawing.Size(504, 56);
            this.gbMainUploadData.TabIndex = 1;
            this.gbMainUploadData.TabStop = false;
            this.gbMainUploadData.Text = "Save Data in Table";
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnImport;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(631, 494);
            this.Controls.Add(this.gbMainUploadData);
            this.Controls.Add(this.gbMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import Dialog";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.gbMain.ResumeLayout(false);
            this.gbMain.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		# region Main() Method  
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		
		static void Main() 
		{
			Application.EnableVisualStyles();
			Application.DoEvents();
			Application.Run(new frmMain());
		}
		#endregion

		#region Form Load 
			private void frmMain_Load(object sender, System.EventArgs e)
			{
				
			
			}
		# endregion

		# region Open Folder Browser Button 
		// On click of this button, the FOLDERBROWSERDIALOG opens where user can select the path of the folder 
		// containing .csv files

		private void btnOpenFldrBwsr_Click(object sender, System.EventArgs e)
		{
            OpenFileDialog openFiles;
            openFiles = new OpenFileDialog();
			try
			{
				if(openFiles.ShowDialog()==DialogResult.OK)
				{
                    txtprimary.Text = openFiles.FileName;
					
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{

			}
		}
		# endregion		

		# region Open File Dialog Button 

		// On click of this button, the openfiledialog opens where user can select .csv file  		

		private void btnOpenFileDlg_Click(object sender, System.EventArgs e)
		{
            OpenFileDialog openFiles;
            openFiles = new OpenFileDialog();
            try
            {
                if (openFiles.ShowDialog() == DialogResult.OK)
                {
                    txtfunctions.Text = openFiles.FileName;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
		}
		# endregion

		

		# region Create schema.ini 
		/*Schema.ini File (Text File Driver)

		When the Text driver is used, the format of the text file is determined by using a
		schema information file. The schema information file, which is always named Schema.ini
		and always kept in the same directory as the text data source, provides the IISAM 
		with information about the general format of the file, the column name and data type
		information, and a number of other data characteristics*/

		private void writeSchema()
		{
			try
			{
				FileStream fsOutput = new FileStream(txtprimary.Text+"\\schema.ini",FileMode.Create, FileAccess.Write);
				StreamWriter srOutput = new StreamWriter (fsOutput);
				string s1,s2,s3,s4,s5;
				s1="["+strCSVFile+"]";
				s2="ColNameHeader="+bolColName.ToString();
				s3="Format="+strFormat;
				s4="MaxScanRows=25";
				s5="CharacterSet=OEM";
				srOutput.WriteLine(s1.ToString()+'\n'+s2.ToString()+'\n'+s3.ToString()+'\n'+s4.ToString()+'\n'+s5.ToString());
				srOutput.Close ();
				fsOutput.Close ();					
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{}
		}
		#endregion

		
		# region Button Import
		private void btnImport_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(txtprimary.Text=="")
				{
					MessageBox.Show("The primary file cannot be empty.","Warning");
					return;
				}
				else
				{
                    LoadMyData(txtprimary.Text, txtfunctions.Text);
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{}
		}
		# endregion


        private void LoadMyData(String givenPrimaryFile , String functionFiles)
        {
            System.Xml.XmlDocument myDoc = new System.Xml.XmlDocument();
            myDoc.Load(givenPrimaryFile);
            String fileProduct = "";
            String fileFamily = "";
            
            

            
        }

		#  region Form Closing 
			private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
			{
				try
				{
					Application.Exit();
				}
				catch(Exception ex)
				{
					MessageBox.Show(ex.Message);
				}
				finally
				{}
			}
		# endregion						

		
	}
}
