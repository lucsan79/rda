using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Windows.Forms.XmlTree;

namespace XTreeDemo
{
	public class DialogController : XtreeNodeController
	{
		protected DialogDef dialogDef;
        /// <summary>
		/// Gets/sets schemaService
		/// </summary>
		public DialogDef DialogDef
		{
			get { return dialogDef; }
			set { dialogDef = value; }
		}

		public override string Name
		{
			get { return dialogDef.Name; }
			set { ;}
		}

        public override string ID
        {
            get { return dialogDef.Famiglia; }
            set { ;}
        }

		public override object Item
		{
			get { return dialogDef; }
		}

		public DialogController()
		{
		}

		public DialogController(DialogDef dialogDef)
		{
			this.dialogDef = dialogDef;
		}

		public override int Index(object item)
		{
			throw new XmlTreeException("Calling Index for SchemaController is not permitted.");
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag,string givenId)
		{
            
			return false;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
			return false;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
			throw new XmlTreeException("Calling AutoDeleteNode for SchemaController is not permitted.");
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
            
            throw new XmlTreeException("Calling InsertNode for SchemaController is not permitted.");
		}

		public override void Select(TreeNode tn)
		{
			Program.Properties.SelectedObject = dialogDef;
		}

		public override void MoveTo(IXtreeNode newParent, IXtreeNode oldParent, int idx)
		{
		}

        public static Boolean Validate(DialogDef givenObject)
        {
            Boolean retValue = true;


            return retValue;

        }
	}
}
