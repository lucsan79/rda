﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using XTreeIIDemo.DialogAdminService;
using System.Windows.Forms;
using System.IO;

namespace XTreeDemo
{
    class clsCDInterface
    {
        DialogAdminService myService;
        System.Net.CookieContainer myContainer;

        public clsCDInterface()
        {
            String exitStatus = "";
            myContainer = new System.Net.CookieContainer();
            myService = new DialogAdminService();
            myService.Url = System.Configuration.ConfigurationSettings.AppSettings["EPLMS_INSTANCE_URL"].ToString();
            myService.CookieContainer = myContainer;
            if (myService.login("ConfigAdmin", "eplms", ref exitStatus) != 0)
            {
                throw new Exception(exitStatus);
            }
        }
        public void Logout()
        {
            myService.logout();
        }
        public DataTable GetListaDialoghi(Boolean bypassStatus)
        {
            String exitStatus = "";
            DataTable myListaDialoghi=null;

            if (myService != null)
            {
                if (myService.GetListaDialoghi(ref myListaDialoghi, bypassStatus,ref exitStatus) != 0)
                {
                    MessageBox.Show(exitStatus, "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
                else
                    return myListaDialoghi;
            }
            return null;
           
        }
        public DataTable GetDistintaProdotto(String givenFamiglia,String givenProdotto )
        {
            String exitStatus = "";
            DataTable myListaBom = null;

            if (myService != null)
            {
                if (myService.GetBOM(ref myListaBom,givenFamiglia , givenProdotto, ref exitStatus) != 0)
                {
                    MessageBox.Show(exitStatus, "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
                else
                    return myListaBom;
            }
            return null;

        }


        public DataTable GetListaFamigliaProdotti()
        {
            String exitStatus = "";
            DataTable myListaDialoghi = null;

            if (myService != null)
            {
                if (myService.GetFamiglieProdotti(ref myListaDialoghi, ref exitStatus) != 0)
                {
                    MessageBox.Show(exitStatus, "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
                else
                    return myListaDialoghi;
            }
            return null;


          

        }

        public DataTable GetGroupsList()
        {
            String exitStatus = "";
            DataTable myListaGruppi = new DataTable("Groups");

            if (myService != null)
            {
                if (myService.GetGroupsList(ref myListaGruppi, ref exitStatus) != 0)
                {
                    MessageBox.Show(exitStatus, "CD Configurator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
                else
                    return myListaGruppi;
            }
            return null;

        }


        public Boolean GetFileDialog(Int32 dialogId, ref String openedFile, ref List<String> myListFiles, ref String fileFeatureToCAD)
        {
            String fileName = "";
            byte[] myFileContent = null;
            String statusMessage = "";
            DataTable myTable = null;
            FileStream fileStream;
            myListFiles = new List<string>();

            if (myService.GetFileDialogo(dialogId, ref fileName, ref myFileContent, ref statusMessage) != 0)
            {
                throw new Exception(statusMessage);
            }
            else
            {
                if (fileName != "" && myFileContent != null)
                {
                    openedFile = openedFile + fileName;
                    fileStream = new FileStream(openedFile, FileMode.Create, FileAccess.ReadWrite);
                    fileStream.Write(myFileContent, 0, myFileContent.Length);
                    fileStream.Close();
                }
                if (myService.GetFileFunctionDialogo(dialogId, ref myTable, ref statusMessage) != 0)
                {
                    throw new Exception(statusMessage);
                }
                else
                {
                    if (myTable != null)
                    {
                        foreach (DataRow row in myTable.Rows)
                        {
                            String fileFunName = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(openedFile), row[0].ToString());
                            fileStream = new FileStream(fileFunName, FileMode.Create, FileAccess.ReadWrite);
                            myFileContent = StrToByteArray(row[1].ToString());
                            fileStream.Write(myFileContent, 0, myFileContent.Length);
                            fileStream.Close();
                            myListFiles.Add(fileFunName);
                        }
                    }
                    
                }

                fileFeatureToCAD = "";
                fileName = "";
                myFileContent = null;
                if (myService.GetFileCADConfigDialogo(dialogId, ref fileName, ref myFileContent, ref statusMessage) != 0)
                {
                    throw new Exception(statusMessage);
                }
                else
                {
                    if (fileName != "" && myFileContent != null)
                    {
                        fileFeatureToCAD = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(openedFile), fileName);
                        fileStream = new FileStream(fileFeatureToCAD, FileMode.Create, FileAccess.ReadWrite);
                        fileStream.Write(myFileContent, 0, myFileContent.Length);
                        fileStream.Close();
                    }
                
                }
                return true;


                
            }

            return false;
        }

        public static byte[] StrToByteArray(string str)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetBytes(str);
        }

       

        public Boolean CreateDialog(ref DialogDef givenDialog, string givenFileName, List<String> functionFiles)
        {
            Int32 givenId=-1;
            String statusMessage="";
            Int32 retValue;
            String givenName = "";
            byte[] myContent=null;


            retValue = myService.CreateDialogo(givenDialog.Famiglia, givenDialog.Prodotto, givenDialog.Codifica, givenDialog.CadRef, givenDialog.Tipologia , ref statusMessage, ref givenId, ref givenName);
            if (retValue != 0)
                throw new Exception(statusMessage);
            else
            {
                myContent= File.ReadAllBytes(givenFileName);
                retValue=myService.AddFileDialogo(givenId, myContent,"", "Dialog Configuration",ref statusMessage);
                if (retValue != 0)
                    throw new Exception(statusMessage);
                else
                {
                    foreach (String fileName in functionFiles)
                    {
                        myContent = File.ReadAllBytes(fileName);
                        retValue = myService.AddFileDialogo(givenId, myContent, System.IO.Path.GetFileName(fileName),"function Configuration", ref statusMessage);
                        if (retValue != 0)
                            throw new Exception(statusMessage);
                    }

                    givenDialog.CollaborationID = givenId;
                    givenDialog.CollaborationName = givenName;
                    return true;
                }
            }
        }

        public void PromoteDialog(DialogDef givenDialog)
        {
            Int32 retValue;
            Int32 givenId = -1;
            String statusMessage = "";
            givenId = givenDialog.CollaborationID;

            retValue = myService.SetDialogPublic(givenId,  ref statusMessage);
            if (retValue != 0)
                throw new Exception(statusMessage);
            else
            {
                MessageBox.Show("Dialogo reso disponibile per la configurazione");
            }

            
        }

        public void DeomoteDialog(DialogDef givenDialog)
        {
            Int32 retValue;
            Int32 givenId = -1;
            String statusMessage = "";
            givenId = givenDialog.CollaborationID;

            retValue = myService.SetDialogWorking (givenId,  ref statusMessage);
            if (retValue != 0)
                throw new Exception(statusMessage);
            else
            {
                MessageBox.Show("Dialogo non più disponibile per la configurazione");
            }
        }
        public Boolean UpdateDialog(DialogDef givenDialog, string givenFileName, List<String> functionFiles, String fileFeatureToCAD)
        {
            Int32 givenId = -1;
            String statusMessage = "";
            Int32 retValue;
            byte[] myContent = null;
            givenId = givenDialog.CollaborationID;
            if(!System.IO.File.Exists(givenFileName))
                throw new Exception("File associato al dialogo non individuato");
           
            myContent = File.ReadAllBytes(givenFileName);
           // myService.PrepareToSave(givenId);
            retValue = myService.UpdateFileDialogo(givenId, myContent, "", "Dialog Configuration", ref statusMessage);
            if (retValue != 0)
                throw new Exception(statusMessage);
            else
            {
                myService.RemoveFileFunction(givenId, "Function Configuration", ref statusMessage);
                foreach (String fileName in functionFiles)
                {
                    myContent = File.ReadAllBytes(fileName);
                    retValue = myService.UpdateFileDialogo(givenId, myContent, System.IO.Path.GetFileName(fileName), "Function Configuration", ref statusMessage);
                    if (retValue != 0)
                        throw new Exception(statusMessage);
                }
            }
            if (fileFeatureToCAD.Trim() != "")
                if (!System.IO.File.Exists(fileFeatureToCAD))
                    throw new Exception("File associato features e distinta cad non individuato");
            if (System.IO.File.Exists(fileFeatureToCAD))
            {
                myContent = null;
                myContent = File.ReadAllBytes(fileFeatureToCAD);
                retValue = myService.UpdateFileDialogo(givenId, myContent, System.IO.Path.GetFileName(fileFeatureToCAD), "CAD Features Configuration", ref statusMessage);
                if (retValue != 0)
                    throw new Exception(statusMessage);
            }
          
            return true;
           
        }

    }
}
