﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;

namespace XTreeDemo
{
    /// <summary>
    /// Provides RadioButton-style functionality in a CheckedListBox.
    /// </summary>
    
    public class RadioCheckedListBox : CheckedListBox
    {

        /// <summary>
        /// Indicates whether the check boxes behave like radio buttons.
        /// </summary>

        private bool _radioCheck = true;

        /// <summary>
        /// Gets or sets a value that indicates whether the check boxes behave like radio buttons.
        /// </summary>
        /// <value>
        /// <b>true</b> if only one item can be checked at a time; otherwise, <b>false</b>.
        /// </value>
        [Category("Behavior")]
        [Description("Indicates whether only a single item can be checked or not.")]
        [DefaultValue(true)]
        public bool RadioCheck
        {
            get { return this._radioCheck; }
            set
            {
                if (this._radioCheck != value)
                {
                    this._radioCheck = value;
                    this.OnRadioCheckChanged(EventArgs.Empty);
                }
            }
        }


        /// <summary>
        /// Raised when the <see cref="RadioCheck" /> property value changes.
        /// </summary>
        /// <remarks></remarks>
        public event EventHandler RadioCheckChanged;


        /// <summary>
        /// Raises the <see cref="RadioCheckChanged" /> event.
        /// </summary>
        /// <param name="e">
        /// The data for the event.
        /// </param>
        protected virtual void OnRadioCheckChanged(EventArgs e)
        {
            if (RadioCheckChanged != null)
            {
                RadioCheckChanged(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="ItemCheck" /> event.
        /// </summary>
        /// <param name="e">
        /// The data for the event.
        /// </param>
        /// <remarks>
        /// If an item is being checked, all other items are unchecked.
        /// </remarks>
        protected override void OnItemCheck(System.Windows.Forms.ItemCheckEventArgs e)
        {
            if (this._radioCheck && e.NewValue == CheckState.Checked)
            {
                //An item is being checked so uncheck all others.
                for (int index = 0; index <= this.Items.Count - 1; index++)
                {
                    if (index != e.Index)
                    {
                        this.SetItemChecked(index, false);
                    }
                }
            }

            base.OnItemCheck(e);
        }

    }
}
